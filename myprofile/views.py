from django.shortcuts import render, redirect, HttpResponse, HttpResponseRedirect
from .models import FullprofileLocation, FullProfile
from persons.models import UsersFullProfile, UsersStartupHistory, UsersIndustries, UsersInvestments, UsersProjectHours, UsersHourCosts, UsersCompanyInfos, UsersProjects, UsersFriend
from utils.views import get_profile_name, get_fulllocation_string, clean_hours_title, content_coverphoto_name, \
    get_active_cover, control_file_extention, check_file_size, get_profile_name_str
from projects.models import ProjectsSteps, ProjectsProjectOptions
from mainadmins.models import ImageUploadSize, ProjectOptions
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.decorators.csrf import csrf_protect
import json
from django.utils.translation import ugettext
from django.views.decorators.cache import cache_page,never_cache


# Create your views here.
@cache_page(60*5)
def index(request, id=0):
    if 'user_fullprofile' in request.session:
        if id is 0:
            return redirect("/dashboard/myprofile/" + str(request.session["user_fullprofile"]) + "/")
        else:
            if not id is request.session["user_fullprofile"]:
                return redirect("/dashboard/myprofile/" + str(request.session["user_fullprofile"]) + "/")
            else:
                startup_history = ""
                users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

                # get profile image
                if not "default" in users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = users_fullprofiles.full_profile.linkedin_picture_url

                if get_profile_name(users_fullprofiles.full_profile.last_name,
                                    users_fullprofiles.full_profile.first_name):
                    profile_name = users_fullprofiles.full_profile.first_name
                else:
                    profile_name = users_fullprofiles.full_profile.first_name + " " + users_fullprofiles.full_profile.last_name

                if UsersStartupHistory._isStartupHistoryClear(users_fullprofiles):
                    if request.LANGUAGE_CODE == "tr":
                        startup_history = UsersStartupHistory.select_user_history(
                            users_fullprofiles).startup_history.title
                    else:
                        startup_history = UsersStartupHistory.select_user_history(
                            users_fullprofiles).startup_history.title_eng

                if "entrepreneur" in users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        industry = "Girişimci"
                    else:
                        industry = users_fullprofiles.users.member_type
                else:
                    if len(users_fullprofiles.full_profile.position_title) > 0:
                        industry = users_fullprofiles.full_profile.position_title
                    else:
                        industry=UsersIndustries.select(users_fullprofiles).industries.description
                        # industry = users_fullprofiles.usersındustries_set.get().industries.description

                context = {
                    "profile_image": profile_picture,
                    "profile_name": profile_name,
                    "profile_location": get_fulllocation_string(
                        FullprofileLocation.select(users_fullprofiles.full_profile).location.district,
                        FullprofileLocation.select(users_fullprofiles.full_profile).location.city),
                    "headline": users_fullprofiles.full_profile.head_line,
                    "summary": users_fullprofiles.full_profile.summary,
                    "startup_history": startup_history,
                    "industry": industry,
                    "cover_photo": users_fullprofiles.full_profile.cover_photo,
                    "profile_id": users_fullprofiles.id,
                    "section": "general",
                    "page_title":ugettext("My profile"),
                }

                return render(request, "socialside/myprofile/index.html", context)
    else:
        return redirect("/login/")

@cache_page(60 * 5)
def profileProjects(request, id):
    context_projects = []
    if 'user_fullprofile' in request.session:

        if id is request.session["user_fullprofile"]:
            selected_user_fullprofile = UsersFullProfile.select_user(id)
            projects = UsersProjects.list_all(selected_user_fullprofile)
            locations = FullprofileLocation.select(selected_user_fullprofile.full_profile)

            if not "default" in selected_user_fullprofile.full_profile.picture_url.name:
                profile_picture=selected_user_fullprofile.full_profile.picture_url.url
            else:
                profile_picture=selected_user_fullprofile.full_profile.linkedin_picture_url

            if get_profile_name(selected_user_fullprofile.full_profile.last_name,
                                selected_user_fullprofile.full_profile.first_name):
                profile_name = selected_user_fullprofile.full_profile.first_name
            else:
                profile_name = selected_user_fullprofile.full_profile.first_name + " " + selected_user_fullprofile.full_profile.last_name

            for project in projects:
                if request.LANGUAGE_CODE == "tr":
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[0].steps.step_name
                    except:
                        steps = "Henüz bir proje adımı bulunmuyor."

                    item = {
                        "id": project.projects.id,
                        "project_logo": project.projects.general_infos.project_logo,
                        "title": project.projects.general_infos.title,
                        "steps": steps,
                        "project_locations": locations.location.city + ", " + locations.location.country,
                        "project_options": ProjectsProjectOptions.select_options(project.projects),
                        "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                      ProjectOptions.select("publish-project")),
                        "profile_id": selected_user_fullprofile.id,
                    }
                else:
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[
                            0].steps.step_name_eng
                    except:
                        steps = "No project steps are found yet."

                    item = {
                        "id": project.projects.id,
                        "project_logo": project.projects.general_infos.project_logo,
                        "title": project.projects.general_infos.title,
                        "steps": steps,
                        "project_locations": locations.location.city + ", " + locations.location.country,
                        "project_options": ProjectsProjectOptions.select_options(project.projects),
                        "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                      ProjectOptions.select("publish-project")),
                    }

                context_projects.append(item)

            context = {
                "projects": context_projects,
                "project_count": UsersProjects.projects_count(selected_user_fullprofile),
                "profile_image": profile_picture,
                "headline": selected_user_fullprofile.full_profile.head_line,
                "cover_photo": selected_user_fullprofile.full_profile.cover_photo,
                "industry": UsersIndustries.select(selected_user_fullprofile).industries.description,
                "profile_name": profile_name,
                "profile_location": get_fulllocation_string(
                    FullprofileLocation.select(selected_user_fullprofile.full_profile).location.district,
                    FullprofileLocation.select(selected_user_fullprofile.full_profile).location.city),
                "section": "projects",
                "page_title":ugettext("My projects"),

            }
            return render(request, 'socialside/myprofile/projects.html', context)

        else:
            return redirect("/dashboard/myprofile/" + str(request.session["user_fullprofile"]) + "/")

    else:
        return HttpResponseRedirect("/login/")

@cache_page(60 * 5)
# @never_cache
def get_project_investments(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

        if UsersInvestments._isInvestmentsClear(users_fullprofiles):
            users_investments = UsersInvestments.select_users_investment(users_fullprofiles)
            # print(users_investments.investments.title)
            if "paid-support" in users_investments.investments.title_code:
                # print(users_fullprofiles.usershourcosts_set.get())
                if UsersHourCosts._isUsersHourCostsClear(users_fullprofiles):
                    hour_cost = UsersHourCosts.select_user_hourcost(
                    users_fullprofiles).hour_costs.cost_code + " " + UsersHourCosts.select_user_hourcost(
                    users_fullprofiles).hour_cost_amount
                else:
                    hour_cost=""
            else:
                hour_cost = ""

            if request.LANGUAGE_CODE == "tr":
                invest_type = users_investments.investments.title
            else:
                invest_type = users_investments.investments.title_eng

            if request.LANGUAGE_CODE == "tr":
                project_hours = clean_hours_title(
                    UsersProjectHours.select_user_projecthours(users_fullprofiles).project_hours.title, "saat")
            else:
                project_hours = clean_hours_title(
                    UsersProjectHours.select_user_projecthours(users_fullprofiles).project_hours.title_eng,
                    "hours")

            context = {
                "message_type": "success",
                "message": ugettext("User investments type is successful."),
                "project_hours": project_hours,
                "hour_cost": hour_cost,
                "invest_type": invest_type,
            }
        else:
            context = {
                "message_type": "warning",
                "message": ugettext("User has not yet specified the type of investment."),
            }

        return HttpResponse(json.dumps(context), content_type='application/json')

@cache_page(60 * 5)
def get_experiences(request):
    if 'user_fullprofile' in request.session:
        context = []
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UsersCompanyInfos.isThereExperience(users_fullprofiles):
            for cmp in UsersCompanyInfos.select_all(users_fullprofiles):
                if cmp.company_infos.is_current:
                    item = {"message_type": "success", "name": cmp.company_infos.name,
                            "title": cmp.company_infos.title,
                            'start_date': cmp.company_infos.start_date_month + "." + cmp.company_infos.start_date_year,
                            "end_date": ugettext("Still Working")}
                else:
                    item = {"message_type": "success", "name": cmp.company_infos.name,
                            "title": cmp.company_infos.title, 'is_current': "Not working",
                            'start_date': cmp.company_infos.start_date_month + "." + cmp.company_infos.start_date_year,
                            'end_date': cmp.company_infos.end_date_month + "." + cmp.company_infos.end_date_year}

                context.append(item)
        else:
            item = {"message_type": "warning", "message": ugettext("User has not yet added an experience.")}
            context.append(item)

        return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def upload_cover_img(request):
    if 'user_fullprofile' in request.session:
        pic = request.FILES['coverfile']
        if control_file_extention(pic):
            if check_file_size(pic) > int(ImageUploadSize.select_mb("2,5")):
                context = {
                    'message_type': 'warning',
                    'message': ugettext("Your image have to small than 2.5 mb. Thanks."),
                    'redirect_url': '/dashboard/myprofile/',
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
                selected_user = FullProfile.objects.filter(id=selected_user_fullprofile.full_profile.id).get()

                selected_user.cover_photo = content_coverphoto_name(selected_user.id, pic)
                selected_user.save()

                context = {
                    'message_type': 'success',
                    'message': ugettext("Your cover photo has been changed. Thanks."),
                    'redirect_url': "/dashboard/myprofile/",
                    'cover_img': str(get_active_cover(int(request.session["user_fullprofile"]))),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context = {
                'message_type': 'error',
                'message': ugettext("your image file should be contains this extensions: *.jpg, *.bmp, *.png"),
                'redirect_url': '/dashboard/myprofile/',
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        context = {
            'message_type': 'error',
            'message': ugettext("Your web session seems to be ended. You've to login again. Thanks."),
            'redirect_url': "/login/",
        }
        return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
def get_header_profile_img(request):
    if "user_fullprofile" in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

        if not "default" in users_fullprofiles.full_profile.picture_url.name:
            profile_picture = users_fullprofiles.full_profile.picture_url.url
        else:
            profile_picture = users_fullprofiles.full_profile.linkedin_picture_url



        context = {
            "profile_photo": profile_picture,
        }
        return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def myfriends_list(request):
    if "user_fullprofile" in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            context_users = []
            if UsersFriend.is_there_any_friend(users_fullprofiles):

                for friend in UsersFriend.friend_list(users_fullprofiles)[:6]:

                    if not "default" in friend.to_users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = friend.to_users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = friend.to_users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in friend.to_users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = friend.to_users_fullprofiles.users.member_type
                    else:

                        if len(friend.to_users_fullprofiles.full_profile.position_title) > 10:
                            position_title = friend.to_users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(friend.to_users_fullprofiles).industries.description
                            # position_title = friend.to_users_fullprofiles.usersındustries_set.get().industries.description

                    profile_name = get_profile_name_str(friend.to_users_fullprofiles.full_profile.last_name,
                                                        friend.to_users_fullprofiles.full_profile.first_name)

                    profile_location = profile_location = get_fulllocation_string(
                        friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                        friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country


                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "position_title": position_title,
                        "id": friend.to_users_fullprofiles.id,
                        "profile_location": profile_location
                    }
                    context_users.append(item)

                if len(context_users) > 5:
                    has_next = True
                else:
                    has_next = False

                context = {
                    "message_type": "success",
                    "users": list(context_users)[:5],
                    "has_next": has_next,
                    "page":2,
                }
            elif UsersFriend.is_there_any_friend_toadded(users_fullprofiles):
                for friend in UsersFriend.friend_list(users_fullprofiles)[:6]:

                    if not "default" in friend.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = friend.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = friend.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in friend.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = friend.users_fullprofiles.users.member_type
                    else:

                        if len(friend.users_fullprofiles.full_profile.position_title) > 10:
                            position_title = friend.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(friend.users_fullprofiles).industries.description
                            # position_title = friend.users_fullprofiles.usersındustries_set.get().industries.description

                    profile_name = get_profile_name_str(friend.users_fullprofiles.full_profile.last_name,
                                                        friend.users_fullprofiles.full_profile.first_name)

                    profile_location = profile_location = get_fulllocation_string(
                        friend.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                        friend.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country


                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "position_title": position_title,
                        "id": friend.users_fullprofiles.id,
                        "profile_location": profile_location
                    }
                    context_users.append(item)

                if len(context_users) > 5:
                    has_next = True
                else:
                    has_next = False

                context = {
                    "message_type": "success",
                    "users": list(context_users)[:5],
                    "has_next": has_next,
                    "page":2,
                }
            else:
                context = {
                    "message_type": "warning",
                    "message": ugettext("You don't have any friend to send a message"),
                    "users": list(context_users),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def loadmore_myfriends_list(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            page = request.GET.get("page")
            context_users = []
            if UsersFriend.is_there_any_friend(users_fullprofiles):
                for friend in UsersFriend.friend_list(users_fullprofiles):

                    if not "default" in friend.to_users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = friend.to_users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = friend.to_users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in friend.to_users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = friend.to_users_fullprofiles.users.member_type

                    else:
                        if len(friend.to_users_fullprofiles.full_profile.position_title) > 10:
                            position_title = friend.to_users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(friend.to_users_fullprofiles).industries.description
                            # position_title = friend.to_users_fullprofiles.usersındustries_set.get().industries.description

                    profile_name = get_profile_name_str(friend.to_users_fullprofiles.full_profile.last_name,
                                                        friend.to_users_fullprofiles.full_profile.first_name)

                    profile_location = profile_location = get_fulllocation_string(
                        friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                        friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country


                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "position_title": position_title,
                        "id": friend.to_users_fullprofiles.id,
                        "profile_location": profile_location,
                    }
                    context_users.append(item)
            elif UsersFriend.is_there_any_friend_toadded(users_fullprofiles):
                for friend in UsersFriend.friend_list(users_fullprofiles):

                    if not "default" in friend.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = friend.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = friend.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in friend.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = friend.users_fullprofiles.users.member_type

                    else:
                        if len(friend.users_fullprofiles.full_profile.position_title) > 10:
                            position_title = friend.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(friend.users_fullprofiles).industries.description
                            # position_title = friend.users_fullprofiles.usersındustries_set.get().industries.description

                    profile_name = get_profile_name_str(friend.users_fullprofiles.full_profile.last_name,
                                                        friend.users_fullprofiles.full_profile.first_name)

                    profile_location = profile_location = get_fulllocation_string(
                        friend.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                        friend.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + friend.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country

                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "position_title": position_title,
                        "id": friend.to_users_fullprofiles.id,
                        "profile_location": profile_location,
                    }
                    context_users.append(item)



            paginator = Paginator(context_users, 5)

            try:
                profile_list = paginator.page(page)
            except PageNotAnInteger:
                profile_list = paginator.page(2)
            except EmptyPage:
                profile_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "users": list(profile_list),
                "has_next": profile_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")