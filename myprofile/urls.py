from django.urls import path
from .views import *


app_name="my-profile"

urlpatterns=[
    path('<int:id>/',index,name='myprofile-index'),
    path('projects/<int:id>/',profileProjects,name='projects'),
    path('get-investments/', get_project_investments, name='project-investments'),
    path('get-experiences/', get_experiences, name='profile-experiences'),
    path('upload-cover/', upload_cover_img, name='upload-cover'),
    path('get-profile-photo/', get_header_profile_img, name='get-profile-photo'),
    path('myfriends/', myfriends_list, name='myfriends'),
    path('loadmore-myfriends/', loadmore_myfriends_list, name='loadmore_myfriends'),


]