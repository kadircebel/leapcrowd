from django.db import models

class FullProfile(models.Model):
    first_name = models.CharField(max_length=150, blank=True)
    cover_photo = models.FileField(upload_to="covers/", default="/media/covers/default_cover.jpg")
    last_name = models.CharField(max_length=150, blank=True)
    head_line = models.CharField(max_length=150, blank=True)
    startup_pass = models.CharField(max_length=250, blank=True)
    phone_number = models.CharField(max_length=100, blank=True)
    position_title = models.CharField(max_length=300, blank=True)
    summary = models.TextField(blank=True)
    picture_url = models.FileField(upload_to="profiles/", default="/media/profiles/default.png")
    linkedin_picture_url = models.TextField(blank=True)
    linkedin_profile_url=models.TextField(blank=True)
    created_date = models.DateField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    def __str__(self):
        return ("%s %s" % (self.first_name,self.last_name)).capitalize()

    class Meta:
        managed = True
        db_table = 'full_profile'



    @classmethod
    def register_profile(cls, first_name, last_name, head_line, summary, linkedin_picture_url,linkedin_profile_url):
        profile = cls(first_name=first_name, last_name=last_name, head_line=head_line, summary=summary,
                      linkedin_picture_url=linkedin_picture_url,linkedin_profile_url=linkedin_profile_url)
        profile.save()
        return profile

    @classmethod
    def register_profile_facebook(cls, first_name, last_name, picture_url):
        profile = cls(first_name=first_name, last_name=last_name,linkedin_picture_url=picture_url)
        profile.save()
        return profile

    @classmethod
    def delete_avatar_image(cls,id):
        selected=cls.objects.filter(id=id).get()
        selected.picture_url = "/media/profiles/default.png",
        selected.save()
        return selected

class Locations(models.Model):
    district = models.CharField(max_length=300, blank=True)
    neighborhood = models.CharField(max_length=300)
    city = models.CharField(max_length=300)
    country = models.CharField(max_length=300)
    lat = models.CharField(max_length=500, blank=True)
    lng = models.CharField(max_length=500)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'locations'

    @classmethod
    def create(cls, district, neighborhood, city, country, lat, lng):
        location = cls(district=district, neighborhood=neighborhood, city=city, country=country, lat=lat, lng=lng)
        location.save()
        return location
    @classmethod
    def update(cls,location_id,district, neighborhood, city, country, lat, lng):
        selected=cls.objects.filter(id=location_id).get()
        selected.district=district
        selected.neighborhood=neighborhood
        selected.city=city
        selected.country=country
        selected.lat=lat
        selected.lng=lng
        selected.save()
        return selected


class FullprofileLocation(models.Model):
    full_profile = models.ForeignKey('FullProfile', models.DO_NOTHING)
    # full_profile=models.OneToOneField('FullProfile',on_delete=models.CASCADE)
    location = models.ForeignKey('Locations', models.DO_NOTHING)
    # location=models.OneToOneField('Locations',on_delete=models.CASCADE)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'fullprofile_location'

    @classmethod
    def isThisSameLocation(cls,city,full_profile):
        from utils.views import change_characters
        try:
            if change_characters(cls.objects.filter(full_profile=full_profile).get().location.city)==change_characters(str(city)):
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def create_profileLocation(cls, full_profile, location):
        if not cls.objects.filter(full_profile=full_profile).exists():
            plocation = cls(full_profile=full_profile, location=location)
            plocation.save()
            return plocation
        else:
            selected = cls.objects.filter(full_profile=full_profile).get()
            selected.location = location
            selected.save()
            return selected

    @classmethod
    def _isLocationClear(cls, full_profile):
        if cls.objects.filter(full_profile=full_profile).exists():
            return True
        else:
            return False

    @classmethod
    def select(cls,full_profiles):
        fullprofile_location=cls.objects.filter(full_profile=full_profiles).get()
        return fullprofile_location