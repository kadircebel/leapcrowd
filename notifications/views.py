from django.shortcuts import render, HttpResponse, redirect
from persons.models import UsersFullProfile, UsersPosts, UsersDmProjectsTeamrequests,UsersIndustries
from .models import *
from projects.models import ProjectsProjectOptions
from mainadmins.models import ProjectOptions
from utils.views import get_profile_name_str, remove_html_tags
from django.utils.timesince import timesince
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import ugettext
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
import json
from django.views.decorators.cache import cache_page,never_cache

# Create your views here.

@never_cache
def teamrequests_list(request):
    if 'user_fullprofile' in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        project_list = users_fullprofile.usersprojects_set.all()
        post_list = users_fullprofile.usersposts_set.all()
        general_request = []


        if UsersDmProjectsTeamrequests.is_there_dm_requests(users_fullprofile):
            for dm_request in UsersDmProjectsTeamrequests.list_requests(users_fullprofile)[:5]:
                dm_request.read_this_request_by_id(dm_request.id)

                if not "default" in dm_request.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = dm_request.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = dm_request.users_fullprofiles.full_profile.linkedin_picture_url

                item_dm = {
                    "title":dm_request.users_projects.projects.general_infos.title,
                    "profile_img": profile_picture,
                    "profile_name": get_profile_name_str(dm_request.users_fullprofiles.full_profile.last_name,dm_request.users_fullprofiles.full_profile.first_name),
                    "created_date": dm_request.created_date,
                    "redirect_url": "javascript:void(0);",
                    "is_read": dm_request.is_read,
                    "user_id":dm_request.users_fullprofiles.id,
                    "position_title":"",
                    "content_id":dm_request.users_projects.projects.id,
                    "type":"dm_project",
                }

                general_request.append(item_dm)

        for project in project_list[:5]:
            if ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select("publish-project")) and ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select("send-teamrequest")):
                # arkadaş toplama ve publis proje mi
                if project.usersprojectsteamrequests_set.count() > 0:
                    for team_request in project.usersprojectsteamrequests_set.all().order_by("-created_date")[:5]:
                        #gördüklerini oku
                        team_request.readthis_request(team_request.teamrequest)

                        if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url

                        if "entrepreneur" in team_request.users_fullprofiles.users.member_type:
                            if request.LANGUAGE_CODE == "tr":
                                position_title="Girişimci"
                            else:
                                position_title= team_request.users_fullprofiles.users.member_type
                        else:
                            if len(team_request.users_fullprofiles.full_profile.position_title) > 0:
                                position_title = team_request.users_fullprofiles.full_profile.position_title
                            else:
                                position_title=UsersIndustries.select(team_request.users_fullprofiles).industries.description
                                # position_title = team_request.users_fullprofiles.usersındustries_set.get().industries.description

                        item_project_request = {
                            "title": project.projects.general_infos.title,
                            "profile_img": profile_picture,
                            "profile_name": get_profile_name_str(team_request.users_fullprofiles.full_profile.last_name,
                                                                 team_request.users_fullprofiles.full_profile.first_name),
                            "user_id": team_request.users_fullprofiles.id,
                            "position_title": position_title,
                            "created_date": team_request.created_date,
                            "redirect_url": "/dashboard/myprojects/requests/" + str(team_request.users_projects.id),
                            "content_id": project.id,
                            "is_read": team_request.is_read,
                            "type": "project",
                        }

                        general_request.append(item_project_request)

        if NotificationsUsersTeams.isThere_notification(users_fullprofile):
            for responded in NotificationsUsersTeams.list_all(users_fullprofile)[:5]:
                responded.read_and_update_notification(responded.id)
                if not "default" in responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.linkedin_picture_url

                item_responded={
                    "title":responded.users_teams.teams.users_projects.projects.general_infos.title,
                    "profile_img":profile_picture,
                    "profile_name":get_profile_name_str(responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.last_name,responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.first_name),
                    "user_id":responded.users_teams.teams.users_projects.users_fullprofiles.id,
                    "position_title":"",
                    "content_id":responded.users_teams.teams.users_projects.projects.id,
                    "created_date":responded.created_date,
                    "redirect_url":"/dashboard/myprojects/cofunded/",
                    "is_read":responded.is_read,
                    "type":"answers",
                }

                general_request.append(item_responded)

        for post in post_list.order_by("-id"):
            if post.userspoststeamrequests_set.count() > 0:
                for team_request in post.userspoststeamrequests_set.all()[:5]:
                    team_request.readthis_request(team_request.teamrequest)
                    if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in team_request.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title="Girişimci"
                        else:
                            position_title=team_request.users_fullprofiles.users.member_type
                    else:
                        if len(team_request.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = team_request.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(team_request.users_fullprofiles).industries.description
                            # position_title = team_request.users_fullprofiles.usersındustries_set.get().industries.description

                    item_post_request = {
                        "title": remove_html_tags(post.posts.content_text),
                        "profile_img": profile_picture,
                        "content_id": post.id,
                        "user_id": team_request.users_fullprofiles.id,
                        "profile_name": get_profile_name_str(team_request.users_fullprofiles.full_profile.last_name,
                                                             team_request.users_fullprofiles.full_profile.first_name),
                        "position_title": position_title,
                        "created_date": team_request.created_date,
                        "redirect_url": "/dashboard/myideas/detail/" + str(team_request.users_posts.id),
                        "is_read": team_request.is_read,
                        "type": "post",
                    }
                    general_request.append(item_post_request)

        if len(general_request):
            has_next = True
        else:
            has_next = False

        context_notifications = []

        for general in sorted(general_request, key=lambda item: item["created_date"], reverse=True):
            item = {
                "title": general["title"],
                "id": general["user_id"],
                "profile_image": general["profile_img"],
                "position_title": general["position_title"],
                "profile_name": general["profile_name"],
                "created_date": timesince(general["created_date"]),
                "redirect_url": general["redirect_url"],
                "is_read": general["is_read"],
                "type": general["type"] + "-" + str(general["content_id"]),
                "content_id": general["content_id"],
            }
            context_notifications.append(item)

        if len(context_notifications) < 1:
            context = {
                "teamrequests": context_notifications[:5],
                "has_next": has_next,
                "message":ugettext("You don't have any team request"),
                "page_title":ugettext("Team request notifications")
            }
        else:
            context = {
                "teamrequests": context_notifications[:5],
                "has_next": has_next,
                "page_title": ugettext("Team request notifications")
            }

        return render(request, "socialside/notifications/teamrequests.html", context)

    else:
        redirect("/login/")

@never_cache
def general_notifications(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        general_notifications = []
        context_notifications = []
        users_posts = UsersPosts.post_list(users_fullprofiles)
        page = 1
        if NotificationsAnswers.list_answers(users_fullprofiles).count() > 0:
            for answers in NotificationsAnswers.list_answers(users_fullprofiles).order_by("-created_date"):
                # bu kısımda answer ikonu
                # mesaj - zaman - link

                if not answers.users_answers.users_fullprofiles.id is users_fullprofiles.id:
                    profile_name = get_profile_name_str(answers.users_answers.users_fullprofiles.full_profile.last_name,answers.users_answers.users_fullprofiles.full_profile.first_name)

                    answers.read_this(answers.id)

                    item_answer = {
                        "message": ugettext("wrote an answer to your idea."),
                        "redirect_url": "javascript:void(0);",
                        "created_date": answers.created_date,
                        "profile_name":profile_name,
                        "profile_id":answers.users_answers.users_fullprofiles.id,
                        "title":remove_html_tags(answers.users_answers.users_posts.posts.content_text)[:85] + "...",
                    }
                    general_notifications.append(item_answer)



        if NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles).count() > 0:
            for feedbacks in NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles).order_by(
                    "-created_date"):
                if not feedbacks.users_projectsfeedbacks.users_fullprofiles.id is users_fullprofiles.id:
                    feedbacks.read_this(feedbacks.id)

                    profile_name = get_profile_name_str(
                        feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.last_name,
                        feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.first_name)

                    item_feedbacks = {
                        "profile_name":profile_name,
                        "message": ugettext("wrote a feedback to your project."),
                        "profile_id":feedbacks.users_projectsfeedbacks.users_fullprofiles.id,
                        "redirect_url": "javascript:void(0);",
                        "created_date": feedbacks.created_date,
                        "title":feedbacks.users_projectsfeedbacks.users_projects.projects.general_infos.title,

                    }
                    general_notifications.append(item_feedbacks)

        if users_posts.count() > 0:
            for post in users_posts.all().order_by("-id"):
                if NotificationsSmilarProjects.list_similarprojects(post.posts).count() > 0:
                    for similar_project in NotificationsSmilarProjects.list_similarprojects(post.posts).order_by("-created_date"):

                        similar_project.read_this(similar_project.id)

                        profile_name = get_profile_name_str(similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.last_name,similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.first_name)

                        item_similarproject = {
                            "profile_name":profile_name,
                            "profile_id":similar_project.users_posts_smilarprojects.users_fullprofiles.id,
                            "message": ugettext("wrote a similar project to your idea."),
                            "redirect_url": "javascript:void(0);",
                            "created_date": similar_project.created_date,
                            "title":remove_html_tags(post.posts.content_text)[:85] + "...",
                        }
                        general_notifications.append(item_similarproject)

        for general in sorted(general_notifications, key=lambda item: item["created_date"], reverse=True):
            item = {
                "message": general["message"],
                "created_date": timesince(general["created_date"]),
                "redirect_url": general["redirect_url"],
                "profile_name":general["profile_name"],
                "profile_id":general["profile_id"],
                "title":general["title"],

            }
            context_notifications.append(item)



        paginator = Paginator(context_notifications, 10)

        try:
            notification_list = paginator.page(page)
        except PageNotAnInteger:
            notification_list = paginator.page(2)
        except EmptyPage:
            notification_list = paginator.page(paginator.num_pages)

        page = str(int(page) + 1)

        output_data = {
            "notifications": notification_list,
            "has_next": notification_list.has_next(),
            "page": page,
            "page_title":ugettext("General notifications"),
        }


        return render(request,"socialside/notifications/notifications.html",output_data)
    else:
        redirect("/login/")

@never_cache
def loadmore_general_notifications(request):
    if 'user_fullprofile' in request.session:
        if request.method=="GET":
            page=request.GET.get("page")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            general_notifications = []
            context_notifications = []
            users_posts = UsersPosts.post_list(users_fullprofiles)

            if NotificationsAnswers.list_answers(users_fullprofiles).count() > 0:
                for answers in NotificationsAnswers.list_answers(users_fullprofiles).order_by("-created_date"):
                    # bu kısımda answer ikonu
                    # mesaj - zaman - link
                    if not answers.users_answers.users_fullprofiles.id is users_fullprofiles.id:

                        answers.read_this(answers.id)

                        profile_name = get_profile_name_str(
                            answers.users_answers.users_fullprofiles.full_profile.last_name,
                            answers.users_answers.users_fullprofiles.full_profile.first_name)

                        item_answer = {
                            "message": ugettext("wrote an answer to your idea."),
                            "redirect_url": "javascript:void(0);",
                            "created_date": answers.created_date,
                            "profile_name": profile_name,
                            "profile_id": answers.users_answers.users_fullprofiles.id,
                            "title": remove_html_tags(answers.users_answers.users_posts.posts.content_text)[
                                     :85] + "...",
                        }
                        general_notifications.append(item_answer)

            if NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles).count() > 0:
                for feedbacks in NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles).order_by(
                        "-created_date"):
                    if not feedbacks.users_projectsfeedbacks.users_fullprofiles.id is users_fullprofiles.id:

                        feedbacks.read_this(feedbacks.id)

                        profile_name = get_profile_name_str(
                            feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.last_name,
                            feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.first_name)

                        item_feedbacks = {
                            "profile_name": profile_name,
                            "message": ugettext("wrote a feedback to your project."),
                            "profile_id": feedbacks.users_projectsfeedbacks.users_fullprofiles.id,
                            "redirect_url": "javascript:void(0);",
                            "created_date": feedbacks.created_date,
                            "title": feedbacks.users_projectsfeedbacks.users_projects.projects.general_infos.title,

                        }
                        general_notifications.append(item_feedbacks)

            if users_posts.count() > 0:
                for post in users_posts.all().order_by("-id"):
                    if NotificationsSmilarProjects.list_similarprojects(post.posts).count() > 0:
                        for similar_project in NotificationsSmilarProjects.list_similarprojects(post.posts).order_by("-created_date"):

                            similar_project.read_this(similar_project.id)

                            profile_name = get_profile_name_str(
                                similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.last_name,
                                similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.first_name)

                            item_similarproject = {
                                "profile_name": profile_name,
                                "profile_id": similar_project.users_posts_smilarprojects.users_fullprofiles.id,
                                "message": ugettext("wrote a similar project to your idea."),
                                "redirect_url": "javascript:void(0);",
                                "created_date": similar_project.created_date,
                                "title": remove_html_tags(post.posts.content_text)[:85] + "...",
                            }
                            general_notifications.append(item_similarproject)

            for general in sorted(general_notifications, key=lambda item: item["created_date"], reverse=True):
                item = {
                    "message": general["message"],
                    "created_date": timesince(general["created_date"]),
                    "redirect_url": general["redirect_url"],
                    "profile_name": general["profile_name"],
                    "profile_id": general["profile_id"],
                    "title": general["title"],

                }
                context_notifications.append(item)

            paginator = Paginator(context_notifications, 10)

            try:
                notification_list = paginator.page(page)
            except PageNotAnInteger:
                notification_list = paginator.page(2)
            except EmptyPage:
                notification_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "notifications": list(notification_list),
                "has_next": notification_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data),content_type="application/json")

@never_cache
def loadmore_teamrequestlist(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            page = request.GET.get("page")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            prj_created_date=""
            post_created_date=""
            counter_prj=0
            counter_post=0
            # counter_dm=0
            context_prj_teamrequests = []
            context_post_teamrequests = []
            context_notifications_answered=[]
            context_dm_teamrequests=[]

            project_list = users_fullprofile.usersprojects_set.all()
            post_list = users_fullprofile.usersposts_set.all()
            general_request = []

            if UsersDmProjectsTeamrequests.is_there_dm_requests(users_fullprofile):
                for dm_request in UsersDmProjectsTeamrequests.list_requests(users_fullprofile)[:5]:
                    dm_request.read_this_request_by_id(dm_request.id)

                    item_dm = {
                        "users":list(context_dm_teamrequests),
                        "title": dm_request.users_projects.projects.general_infos.title,
                        "created_date": timesince(dm_request.created_date),
                        "redirect_url": "javascript:void(0);",
                        "content_id": dm_request.users_projects.projects.id,
                        "type": "dm_project",
                    }

                    general_request.append(item_dm)
                    # counter_dm+=1

            if NotificationsUsersTeams.isThere_notification(users_fullprofile):
                for responded in NotificationsUsersTeams.list_all(users_fullprofile)[:5]:
                    responded.read_and_update_notification(responded.id)

                    item_responded = {
                        "users":list(context_notifications_answered),
                        "title": responded.users_teams.teams.users_projects.projects.general_infos.title,
                        "content_id": responded.users_teams.teams.users_projects.projects.id,
                        "created_date":timesince(responded.created_date),
                        "redirect_url": "/dashboard/myprojects/cofunded/",
                        "type": "answers",
                    }

                    general_request.append(item_responded)


            for project in project_list:
                if ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select("publish-project")) and ProjectsProjectOptions.isItOnline(project.projects,ProjectOptions.select("send-teamrequest")):
                    # arkadaş toplama ve publis proje mi
                    if project.usersprojectsteamrequests_set.count() > 0:

                        for team_request in project.usersprojectsteamrequests_set.filter(is_active=True).all().order_by("-created_date"):
                            if counter_prj < 1:
                                prj_created_date=timesince(team_request.created_date)

                            # gördüklerini oku
                            team_request.readthis_request(team_request.teamrequest)

                            if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url

                            else:
                                profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url

                            if "entrepreneur" in team_request.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title = "Girişimci"
                                else:
                                    position_title = team_request.users_fullprofiles.users.member_type
                            else:
                                if len(team_request.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = team_request.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(team_request.users_fullprofiles).industries.description
                                    # position_title = team_request.users_fullprofiles.usersındustries_set.get().industries.description

                            item_project_request = {
                                # "title": project.projects.general_infos.title,
                                "profile_img": profile_picture,
                                "profile_name": get_profile_name_str(
                                    team_request.users_fullprofiles.full_profile.last_name,
                                    team_request.users_fullprofiles.full_profile.first_name),
                                "user_id": team_request.users_fullprofiles.id,
                                "position_title": position_title,
                                # "created_date": timesince(team_request.created_date),
                            }

                            context_prj_teamrequests.append(item_project_request)
                            counter_prj+=1

                        item_prj = {
                            "title": project.projects.general_infos.title,
                            "content_id": project.id,
                            "type": "project",
                            "users": context_prj_teamrequests,
                            "created_date":prj_created_date,
                            "redirect_url": "/dashboard/myprojects/requests/" + str(project.id) + "/",
                        }

                        general_request.append(item_prj)
                        context_prj_teamrequests = []

            for post in post_list:
                if post.userspoststeamrequests_set.count() > 0:
                    for team_request in post.userspoststeamrequests_set.filter(is_active=True).all().order_by("-created_date"):
                        if counter_post < 1:
                            post_created_date=timesince(team_request.created_date)

                        team_request.readthis_request(team_request.teamrequest)

                        if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url

                        if "entrepreneur" in team_request.users_fullprofiles.users.member_type:
                            if request.LANGUAGE_CODE == "tr":
                                position_title = "Girişimci"
                            else:
                                position_title= team_request.users_fullprofiles.users.member_type
                        else:
                            if len(team_request.users_fullprofiles.full_profile.position_title) > 0:
                                position_title = team_request.users_fullprofiles.full_profile.position_title
                            else:
                                position_title=UsersIndustries.select(team_request.users_fullprofiles).industries.description
                                # position_title = team_request.users_fullprofiles.usersındustries_set.get().industries.description

                        item_post_req = {
                            "profile_img": profile_picture,
                            "user_id": team_request.users_fullprofiles.id,
                            "profile_name": get_profile_name_str(team_request.users_fullprofiles.full_profile.last_name,
                                                                 team_request.users_fullprofiles.full_profile.first_name),
                            "position_title": position_title,
                            # "created_date":timesince(team_request.created_date),
                        }
                        context_post_teamrequests.append(item_post_req)
                        counter_post+=1

                    item_post_request = {
                        "title": remove_html_tags(post.posts.content_text)[:85] + "...",
                        "users": context_post_teamrequests,
                        "content_id": post.id,
                        "redirect_url": "/dashboard/myideas/detail/" + str(post.id) + "/",
                        "type": "post",
                        "created_date":post_created_date,
                    }
                    general_request.append(item_post_request)
                    context_post_teamrequests=[]

            context_notifications = []

            for general in sorted(general_request, key=lambda item: item["content_id"], reverse=True):
                # print(general)
                item = {
                    "title": general["title"],
                    "id": general["content_id"],
                    "users":list(general["users"]),
                    "created_date": general["created_date"],
                    "redirect_url": general["redirect_url"],
                    "type": general["type"] + "-" + str(general["content_id"]),
                }
                context_notifications.append(item)


            paginator = Paginator(context_notifications, 5)

            try:
                notification_list = paginator.page(page)
            except PageNotAnInteger:
                notification_list = paginator.page(2)
            except EmptyPage:
                notification_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "teamrequests": list(notification_list),
                "has_next": notification_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
def notification_count(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        answers_count = NotificationsAnswers.answers_count(users_fullprofiles)
        feedbacks_count = NotificationsProjectsFeedbacks.feedback_count(users_fullprofiles)
        similar_projects_count = NotificationsSmilarProjects.similar_count(users_fullprofiles)

        context = {
            "notifcations_count": (answers_count + feedbacks_count + similar_projects_count),
        }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def get_general_notifications(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        general_notifications = []
        context_notifications = []
        users_posts = UsersPosts.post_list(users_fullprofiles)


        page = 1
        if NotificationsAnswers.list_answers(users_fullprofiles).count() > 0:

            for answers in NotificationsAnswers.list_answers(users_fullprofiles).order_by("-created_date"):
                # bu kısımda answer ikonu
                # mesaj - zaman - link
                if answers.users_answers.users_fullprofiles.id != users_fullprofiles.id:

                    profile_name = get_profile_name_str(answers.users_answers.users_fullprofiles.full_profile.last_name,answers.users_answers.users_fullprofiles.full_profile.first_name)

                    if request.LANGUAGE_CODE == "tr":
                        message = profile_name + "<br/>fikrinize bir cevap yazdı."
                    else:
                        message = profile_name + "<br/>wrote an answer to your idea."
                    item_answer = {
                        "icon": "fa fa-comments",
                        "message": message,
                        "redirect_url": "javascript:void(0);",
                        "created_date": answers.created_date,
                        "is_read": answers.is_read,
                        "color": "bg-success",
                        "type":"answer",
                        "id":answers.users_answers.id,
                        "profile_name":profile_name,
                    }
                    general_notifications.append(item_answer)

        if NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles).count() > 0:
            for feedbacks in NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles).order_by(
                    "-created_date"):
                if feedbacks.users_projectsfeedbacks.users_fullprofiles.id != users_fullprofiles.id:

                    profile_name = get_profile_name_str(
                        feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.last_name,
                        feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.first_name)

                    if request.LANGUAGE_CODE == "tr":
                        message = profile_name + "<br/>projenize bir geri bildirim yazdı."
                    else:
                        message = profile_name + "<br/>wrote a feedback to your project."

                    item_feedbacks = {
                        "icon": "fa fa-pencil",
                        "message": message,
                        "redirect_url": "/dashboard/myprojects/requests/" + str(feedbacks.users_projectsfeedbacks.users_projects.id) + "/",
                        "created_date": feedbacks.created_date,
                        "is_read": feedbacks.is_read,
                        "color": "bg-warning",
                        "type":"feedback",
                        "id":"",
                        "profile_name":profile_name,
                    }
                    general_notifications.append(item_feedbacks)

        if users_posts.count() > 0:
            for post in users_posts.all().order_by("-id"):
                if NotificationsSmilarProjects.list_similarprojects(post.posts).count() > 0:
                    for similar_project in NotificationsSmilarProjects.list_similarprojects(post.posts).order_by(
                            "-created_date"):
                        profile_name = get_profile_name_str(
                            similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.last_name,
                            similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.first_name)

                        if request.LANGUAGE_CODE == "tr":
                            message = profile_name + "<br/>fikrinize benzer bir proje yazdı."
                        else:
                            message = profile_name + "<br/>wrote a similar project to your idea."

                        item_similarproject = {
                            "icon": "fa fa-pencil-square",
                            "message": message,
                            "redirect_url": "/dashboard/myideas/detail/" + str(post.id) + "/",
                            "created_date": similar_project.created_date,
                            "is_read": similar_project.is_read,
                            "color": "bg-info",
                            "type":"similar_project",
                            "id":"",
                            "profile_name":profile_name,
                        }
                        general_notifications.append(item_similarproject)

        for general in sorted(general_notifications, key=lambda item: item["created_date"], reverse=True):
            item = {
                "message": general["message"],
                "created_date": timesince(general["created_date"]),
                "redirect_url": general["redirect_url"],
                "is_read": general["is_read"],
                "icon": general["icon"],
                "color": general["color"],
                "link_type":general["type"],
                "id":general["id"],
                "profile_name":general["profile_name"],
            }
            context_notifications.append(item)

        paginator = Paginator(context_notifications, 4)



        try:
            notification_list = paginator.page(page)
        except PageNotAnInteger:
            notification_list = paginator.page(2)
        except EmptyPage:
            notification_list = paginator.page(paginator.num_pages)

        page = str(int(page) + 1)

        output_data = {
            "notifications": list(notification_list),
            "has_next": notification_list.has_next(),
            "page": page,
        }

        return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
def loadmore_get_general_notifications(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            page = request.GET.get("page")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            general_notifications = []
            context_notifications = []
            users_posts = UsersPosts.post_list(users_fullprofiles)

            if NotificationsAnswers.list_answers(users_fullprofiles).count() > 0:
                for answers in NotificationsAnswers.list_answers(users_fullprofiles):
                    # bu kısımda answer ikonu
                    # mesaj - zaman - link
                    if not answers.users_answers.users_fullprofiles.id is users_fullprofiles.id:
                        profile_name = get_profile_name_str(
                            answers.users_answers.users_fullprofiles.full_profile.last_name,
                            answers.users_answers.users_fullprofiles.full_profile.first_name)
                        if request.LANGUAGE_CODE == "tr":
                            message = profile_name + "<br/>fikrinize bir cevap yazdı."
                        else:
                            message = profile_name + "<br/>wrote an answer to your idea."
                        item_answer = {
                            "icon": "fa fa-comments",
                            "message": message,
                            "redirect_url": "javascript:void(0);",
                            "created_date": answers.created_date,
                            "is_read": answers.is_read,
                            "color": "bg-success",
                            "type":"answer",
                            "id":answers.users_answers.id,
                            "profile_name":profile_name,
                        }
                        general_notifications.append(item_answer)

            if NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles).count() > 0:
                for feedbacks in NotificationsProjectsFeedbacks.list_feedbacks(users_fullprofiles):
                    if not feedbacks.users_projectsfeedbacks.users_fullprofiles.id is users_fullprofiles.id:

                        profile_name = get_profile_name_str(
                            feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.last_name,
                            feedbacks.users_projectsfeedbacks.users_fullprofiles.full_profile.first_name)

                        if request.LANGUAGE_CODE == "tr":
                            message = profile_name + "<br/>projenize bir geri bildirim yazdı."
                        else:
                            message = profile_name + "<br/>wrote a feedback to your project."

                        item_feedbacks = {
                            "icon": "fa fa-pencil",
                            "message": message,
                            "redirect_url": "/dashboard/myprojects/requests/" + str(feedbacks.users_projectsfeedbacks.users_projects.id) + "/",
                            "created_date": feedbacks.created_date,
                            "is_read": feedbacks.is_read,
                            "color": "bg-warning",
                            "type":"feedback",
                            "id":"",
                            "profile_name":profile_name,
                        }
                        general_notifications.append(item_feedbacks)

            if users_posts.count() > 0:
                for post in users_posts.all().order_by("-id"):
                    if NotificationsSmilarProjects.list_similarprojects(post.posts).count() > 0:
                        for similar_project in NotificationsSmilarProjects.list_similarprojects(post.posts):
                            profile_name = get_profile_name_str(
                                similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.last_name,
                                similar_project.users_posts_smilarprojects.users_fullprofiles.full_profile.first_name)

                            if request.LANGUAGE_CODE == "tr":
                                message = profile_name + "<br/>fikrinize benzer bir proje yazdı."
                            else:
                                message = profile_name + "<br/>wrote a similar project to your idea."

                            item_similarproject = {
                                "icon": "fa fa-pencil-square",
                                "message": message,
                                "redirect_url": "/dashboard/myideas/detail/" + str(post.id) + "/",
                                "created_date": similar_project.created_date,
                                "is_read": similar_project.is_read,
                                "color": "bg-info",
                                "type":"similar_project",
                                "id":"",
                                "profile_name":profile_name,
                            }
                            general_notifications.append(item_similarproject)

            for general in sorted(general_notifications, key=lambda item: item["created_date"], reverse=True):
                item = {
                    "message": general["message"],
                    "created_date": timesince(general["created_date"]),
                    "redirect_url": general["redirect_url"],
                    "is_read": general["is_read"],
                    "icon": general["icon"],
                    "color": general["color"],
                    "link_type":general["type"],
                    "id":general["id"],
                    "profile_name":general["profile_name"],
                }
                context_notifications.append(item)


            paginator = Paginator(context_notifications, 4)

            try:
                notification_list = paginator.page(page)
            except PageNotAnInteger:
                notification_list = paginator.page(2)
            except EmptyPage:
                notification_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "notifications": list(notification_list),
                "has_next": notification_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")
