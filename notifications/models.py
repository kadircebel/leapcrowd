from django.db import models


class NotificationsAnswers(models.Model):
    id = models.BigAutoField(primary_key=True)
    to_users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    # şu kullanıcının şu posta yazdığı cevap - şu kullanıcının şu postuna verilen cevaba karşılık yazdığı cevap
    users_answers = models.ForeignKey("persons.UsersAnswers", models.DO_NOTHING, blank=True)
    is_read = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    readed_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'notifications_answers'

    @classmethod
    def read_this(cls,id):
        from datetime import datetime
        try:
            if not cls.objects.filter(id=id).get().is_read is True:
                selected = cls.objects.filter(id=id).get()
                selected.is_read = True
                selected.readed_date = datetime.now()
                selected.save()
                return selected
        except:
            pass

    @classmethod
    def read_this_by_usersanswers(cls,users_answers):
        from datetime import datetime
        if cls.objects.filter(users_answers=users_answers).get().is_read is False:
            selected=cls.objects.filter(users_answers=users_answers).get()
            selected.is_read=True
            selected.readed_date=datetime.now()
            selected.save()
            return selected



    @classmethod
    def create_notification(cls, users_answers,to_users_fullprofiles):
        new_record = cls(users_answers=users_answers,to_users_fullprofiles=to_users_fullprofiles)
        new_record.save()
        return new_record

    @classmethod
    def answers_count(cls, users_fullprofiles):
        try:
            if cls.objects.filter(to_users_fullprofiles=users_fullprofiles,is_read=False).count() > 0:
                return cls.objects.filter(to_users_fullprofiles=users_fullprofiles,is_read=False).count()
            else:
                return 0
        except:
            return 0

    @classmethod
    def list_answers(cls,users_fullprofiles):
        #şu posta atılan cevaplar
        return cls.objects.filter(to_users_fullprofiles=users_fullprofiles).all().order_by("-id")

    # @classmethod #şu cevaba atılan cevaplar
    # def list_answers_answers(cls,users_fullprofiles):
    #     return cls.objects.filter(users_answers__usersanswersanswers__users_fullprofiles=users_fullprofiles).all().order_by("-id")






class NotificationsProjectsFeedbacks(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_projectsfeedbacks = models.ForeignKey('persons.UsersProjectsFeedbacks', models.DO_NOTHING, blank=True)
    is_read = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    readed_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'notifications_feedbacks'

    # @classmethod
    # def read_feedbacks(cls,users_projects_feedbacks):
    #     from datetime import datetime
    #     try:
    #         for fdbck in cls.objects.filter(readed_date__isnull=True).all():
    #             fdbck.
    #             pass
    #     except:
    #         pass

    @classmethod
    def read_this(cls,id):
        from datetime import datetime
        try:
            if not cls.objects.filter(id=id).get().is_read is True:
                selected = cls.objects.filter(id=id).get()
                selected.is_read = True
                selected.readed_date = datetime.now()
                selected.save()
                return selected
        except:
            pass



    @classmethod
    def create_notification(cls, users_projectsfeedbacks):
        new_record = cls(users_projectsfeedbacks=users_projectsfeedbacks)
        new_record.save()
        return new_record

    @classmethod
    def feedback_count(cls, users_fullprofiles):
        try:
            if cls.objects.filter(users_projectsfeedbacks__users_projects__users_fullprofiles=users_fullprofiles,is_read=False).count()>0:
                return cls.objects.filter(users_projectsfeedbacks__users_projects__users_fullprofiles=users_fullprofiles,is_read=False).count()
            else:
                return 0
        except:
            return 0

    @classmethod
    def list_feedbacks(cls,users_fullprofiles):
        return cls.objects.filter(users_projectsfeedbacks__users_projects__users_fullprofiles=users_fullprofiles).all().order_by("-id")



class NotificationsSmilarProjects(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_posts_smilarprojects = models.ForeignKey('persons.UsersPostsSmilarprojects', models.DO_NOTHING, blank=True)
    is_read = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    readed_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'notifications_similarprojects'

    @classmethod
    def read_this(cls,id):
        from datetime import datetime
        try:
            if not cls.objects.filter(id=id).get().is_read is True:
                selected = cls.objects.filter(id=id).get()
                selected.is_read = True
                selected.readed_date = datetime.now()
                selected.save()
                return selected
        except:
            pass



    @classmethod
    def create_notification(cls, users_post_smilarprojects):
        new_record = cls(users_posts_smilarprojects=users_post_smilarprojects)
        new_record.save()
        return new_record

    @classmethod
    def similar_count(cls, users_fullprofiles):
        counter=0
        from persons.models import UsersPostsSmilarprojects,UsersPosts
        users_posts=UsersPosts.post_list(users_fullprofiles)

        for post in users_posts:
            for users_post_smilars in UsersPostsSmilarprojects.select_by_posts(post.posts):
                if cls.objects.filter(users_posts_smilarprojects=users_post_smilars,is_read=False).exists():
                    counter+=1
        return counter

    @classmethod
    def list_similarprojects(cls,posts):
        return cls.objects.filter(users_posts_smilarprojects__posts_smilarprojects__posts=posts).all().order_by("-id")

class NotificationsUsersTeams(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_teams = models.ForeignKey('persons.UsersTeams', models.DO_NOTHING, blank=True)
    is_read = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    readed_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table="notifications_users_teams"
        managed=True

    @classmethod
    def list_all(cls,users_fullprofiles):
        try:
            if cls.objects.filter(users_teams__users_fullprofiles=users_fullprofiles).exists():
                return cls.objects.filter(users_teams__users_fullprofiles=users_fullprofiles).all().order_by("-id")
        except:
            pass

    @classmethod
    def read_and_update_notification(cls,id):
        from datetime import datetime
        selected=cls.objects.filter(id=id).get()
        selected.is_read=True
        selected.readed_date=datetime.now()
        selected.save()
        return selected

    @classmethod
    def create_notification(cls,users_teams):
        new_record=cls(users_teams=users_teams)
        new_record.save()
        return new_record

    @classmethod
    def isThere_notification_by_project(cls,users_fullprofiles,users_projects):
        try:
            if cls.objects.filter(users_teams__users_fullprofiles=users_fullprofiles,users_teams__teams__users_projects=users_projects).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def isThere_notification(cls,users_fullprofiles):
        try:
            if cls.objects.filter(users_teams__users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def count_responded_teamrequests(cls,users_fullprofiles):
        from teams.models import Teams
        try:
            if cls.objects.filter(users_teams__users_fullprofiles=users_fullprofiles, is_read=False).count() > 0:
                if Teams.is_project_owner(users_fullprofiles) is False:
                    return cls.objects.filter(users_teams__users_fullprofiles=users_fullprofiles,is_read=False).count()
            else:
                return 0
        except:
            return 0

    @classmethod
    def funded_projects_count(cls,users_fullprofiles):
        return cls.objects.filter(users_teams__users_fullprofiles=users_fullprofiles).count()


