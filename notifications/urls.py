from django.urls import path
from .views import *



app_name="notifications"

urlpatterns=[
    path('teamrequests/', teamrequests_list, name='teamrequests_list'),
    path('notification-count/', notification_count, name='notification_count'),
    path('general-notifications/', get_general_notifications, name='general_notifications'),
    path('general/', general_notifications, name='general'),
    path('loadmore-general/', loadmore_general_notifications, name='loadmore_general'),

    path('loadmore-gnrl-notifications/', loadmore_get_general_notifications, name='loadmore_gnrl_notifications'),
    path('loadmore-teamrequestlist/', loadmore_teamrequestlist, name='loadmore_teamrequestlist'),
]

