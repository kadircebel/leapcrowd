from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.conf import settings
from persons.models import UsersFullProfile, UsersPosts, UsersPostsTeamrequests, UsersProjects, UsersTeams, UsersMembershipTermsType,UsersIndustries
from projects.models import GeneralInfos, Projects, ProjectsSteps, ProjectUsageTypes, ProjectPointsScores
from utils.views import remove_html_tags, remodel_typerlink, clean_hours_title, responded_teamrequest_mail, select_teammembers_array,get_person_profile_name,match_friends_in_system,is_it_same_location,send_signals_to_projects_owner,get_profile_name_str
from mainadmins.models import UsageTypes, PointScores, Steps
from memberships.models import MembershipTermsType
from teams.models import Teams
from notifications.models import NotificationsUsersTeams
import json, uuid,requests
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import ugettext
from django.utils.timesince import timesince
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.decorators.cache import cache_page,never_cache


@never_cache
def index(request):
    if 'user_fullprofile' in request.session:
        user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        context_posts = []

        if UsersPosts.isThere_post_by_user(user_fullprofile):
            users_posts = UsersPosts.post_list(user_fullprofile)
            loop_counter = 0
            for users_post in users_posts:
                loop_counter += 1
                if not "default" in users_post.users_fullprofiles.full_profile.picture_url.name:
                    profile_img = users_post.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_img = users_post.users_fullprofiles.full_profile.linkedin_picture_url

                item_post = {
                    "profile_img": profile_img,
                    "post_content": remove_html_tags(users_post.posts.content_text),
                    # "post_number":loop_counter,
                    "redirect_url": "/dashboard/myideas/detail/" + str(users_post.id) + "/",
                    "teamrequest_count": users_post.userspoststeamrequests_set.filter(is_active=True).count(),
                    "smilarproject_count": users_post.posts.postssmilarprojects_set.count(),
                }

                context_posts.append(item_post)

            if users_posts.count() > 6:
                has_next = True
            else:
                has_next = False

            context = {
                "posts": context_posts[:6],
                "message_type": "success",
                "has_next": has_next,
                "page_title":ugettext("My ideas"),
            }

        else:
            context = {
                "posts": context_posts,
                "message_types": "warning",
                "has_next": False,
                "page_title": ugettext("My ideas"),
            }

        # users_posts=
        return render(request, "socialside/myideas/index.html", context)
    else:
        return HttpResponseRedirect("/login/")

@never_cache
def loadmore_similarprojects(request, id):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            page = request.POST.get("page")
            context_smilar = []
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_posts = UsersPosts.select_this(id)

            for smilarproject in users_posts.posts.postssmilarprojects_set.all().order_by("-id"):

                if not "default" in smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.picture_url.name:
                    profile_img = smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_img = smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.linkedin_picture_url

                item_smilar = {
                    "profile_img": profile_img,
                    "similar_content": smilarproject.describe,
                    "similar_content_link": remodel_typerlink(smilarproject.link_address),
                    "created_date": timesince(smilarproject.created_date),
                    "profile_id": smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.id,
                    # "profile_name": get_profile_name(
                    #     smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.last_name.lower(),
                    #     smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.first_name.lower()),
                    "profile_name":get_person_profile_name(users_fullprofiles.id,smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.id),

                }
                context_smilar.append(item_smilar)

            paginator = Paginator(context_smilar, 6)

            try:
                similar_project_list = paginator.page(page)
            except PageNotAnInteger:
                similar_project_list = paginator.page(2)
            except EmptyPage:
                similar_project_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "similar_projects": list(similar_project_list),
                "has_next": similar_project_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

# @cache_page(60*5)
@never_cache
def detail(request, id=0):
    if 'user_fullprofile' in request.session:
        if id is 0:
            return HttpResponseRedirect("/dashboard/myideas/")
        else:
            context_smilar = []
            user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            # gelen id=users_post_id
            if UsersPosts.is_there(id):
                users_posts = UsersPosts.select_this(id)
                # bu aşamada smilar projects'i çekeceğim
                for smilarproject in users_posts.posts.postssmilarprojects_set.all().order_by("-id")[:6]:

                    if not "default" in smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.picture_url.name:
                        profile_img = smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.picture_url.url

                    else:
                        profile_img = smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.linkedin_picture_url

                    item_smilar = {
                        "profile_img": profile_img,
                        "similar_content": smilarproject.describe,
                        "similar_content_link": remodel_typerlink(smilarproject.link_address),
                        "created_date": timesince(smilarproject.created_date),
                        "profile_id": smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.id,
                        # "profile_name": get_profile_name(
                        #     smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.last_name,
                        #     smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.full_profile.first_name),
                        "profile_name":get_person_profile_name(user_fullprofile.id,smilarproject.userspostssmilarprojects_set.get().users_fullprofiles.id),

                    }
                    context_smilar.append(item_smilar)

                if users_posts.posts.postssmilarprojects_set.count() > 6:
                    has_next = True
                    message = ""
                else:
                    has_next = False
                    message = ugettext("There is no other similar project information at the moment.")

                context = {
                    "similar_projects": context_smilar,
                    "has_next": has_next,
                    "id": id,
                    "message": message,
                    "message_type": "success",
                    "post_content": remove_html_tags(user_fullprofile.usersposts_set.filter(id=id).get().posts.content_text),
                    "page_title":ugettext("My idea detail")
                }
            else:
                context = {
                    "similar_projects": context_smilar,
                    "message_type": "warning",
                    "message": ugettext("You have not received any similar project yet."),
                    "id": id,
                    "post_content": remove_html_tags(
                        user_fullprofile.usersposts_set.filter(id=id).get().posts.content_text),
                    "page_title": ugettext("My idea detail")
                }

            return render(request, 'socialside/myideas/myideas_detail.html', context)
    else:
        return HttpResponseRedirect("/login/")

@never_cache
@csrf_protect
def teamrequest(request, id):
    if 'user_fullprofile' in request.session:
        context_request = []
        if request.method == "POST":
            users_posts = UsersPosts.select_this(id)
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            page = request.POST.get("page")
            selected_requests = UsersPostsTeamrequests.select_requests(users_posts)

            if selected_requests.count() > 0:
                for requests in selected_requests:

                    if requests.is_read is False:
                        requests.readthis_request(requests.teamrequest)

                    if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title=requests.users_fullprofiles.users.member_type
                    else:
                        if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = requests.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                            # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                    if request.LANGUAGE_CODE == "tr":
                        support_type = requests.teamrequest.users_request_investments.investments.title
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title, "saat")
                    else:
                        support_type = requests.teamrequest.users_request_investments.investments.title_eng
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title_eng, "hour")

                    if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                        try:
                            hour_cost = requests.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + requests.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                        except:
                            hour_cost = ""

                    else:
                        hour_cost = ""

                    item_request = {
                        "profile_img": profile_img,
                        # "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                        #                                      requests.users_fullprofiles.full_profile.first_name),
                        "profile_name":get_person_profile_name(users_fullprofiles.id,requests.users_fullprofiles.id),
                        "position_title": position_title,
                        "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                        "support_type": support_type,
                        "hour_cost": hour_cost,
                        "hour_info": hour_info,
                        "id": requests.users_fullprofiles.id,
                    }

                    context_request.append(item_request)

                # print(context_request)
                paginator = Paginator(context_request, 6)

                try:
                    request_list = paginator.page(page)
                except PageNotAnInteger:
                    request_list = paginator.page(1)
                except EmptyPage:
                    request_list = paginator.page(paginator.num_pages)

                if len(page) > 0:
                    page = str(int(page) + 1)

                output_data = {
                    # "message_type":"success",
                    "requests": list(request_list),
                    "has_next": request_list.has_next(),
                    "page": page,
                    "message_type": "success",
                }
            else:
                output_data = {
                    "message_type": "error",
                    "has_next": False,
                    "message": ugettext("You have not received a team request yet."),
                }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@cache_page(60 * 10)
@csrf_protect
def undecided_requests(request, id):
    if 'user_fullprofile' in request.session:
        context_request = []
        if request.method == "POST":
            users_posts = UsersPosts.select_this(id)
            user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            page = request.POST.get("page")
            selected_requests = UsersPostsTeamrequests.undediced_requests(users_posts)

            if selected_requests.count() > 0:

                for requests in selected_requests:
                    if requests.is_read is False:
                        requests.readthis_request(requests.teamrequest)
                    if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title="Girişimci"
                        else:
                            position_title = requests.users_fullprofiles.users.member_type
                    else:
                        if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = requests.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                            # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                    if request.LANGUAGE_CODE == "tr":
                        support_type = requests.teamrequest.users_request_investments.investments.title
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title, "saat")
                    else:
                        support_type = requests.teamrequest.users_request_investments.investments.title_eng
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title_eng, "hour")

                    if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                        try:
                            hour_cost = requests.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + "" + requests.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                        except:
                            hour_cost = ""
                    else:
                        hour_cost = ""

                    item_request = {
                        "profile_img": profile_img,
                        # "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                        #                                      requests.users_fullprofiles.full_profile.first_name),
                        "profile_name":get_person_profile_name(user_fullprofile.id,requests.users_fullprofiles.id),
                        "position_title": position_title,
                        "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                        "support_type": support_type,
                        "hour_cost": hour_cost,
                        "hour_info": hour_info,
                        "id": requests.users_fullprofiles.id,
                    }

                    context_request.append(item_request)

                # print(context_request)
                paginator = Paginator(context_request, 6)

                try:
                    request_list = paginator.page(page)
                except PageNotAnInteger:
                    request_list = paginator.page(1)
                except EmptyPage:
                    request_list = paginator.page(paginator.num_pages)

                if len(page) > 0:
                    page = str(int(page) + 1)

                if "beginner" in user_fullprofile.users.users_membershiptypes.name_code:
                    if request.LANGUAGE_CODE == "tr":
                        message=str(len(context_request)) + " adet kararsız takım isteğiniz mevcut. Takım isteklerini görebilmeniz için ücretli üyelik paketlerinden birine sahip olmalısınız."
                    else:
                        message="You have " + str(len(context_request)) + " undecided requests. You should benefit our paid membership if you want to see these requests."

                    output_data = {
                        "message_type": "success",
                        "membership_type":"beginner",
                        "message": message,
                        "has_next": False,
                    }
                else:
                    output_data = {
                        "message_type": "success",
                        "requests": list(request_list),
                        "has_next": request_list.has_next(),
                        "page": page,
                    }
            else:
                output_data = {
                    "message_type": "error",
                    "has_next": False,
                    "message": ugettext("You have not an undecided team request yet."),
                }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
@csrf_protect
def filter_teamrequests(request, id):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            filter_option = request.POST.get("filter_choice")
            # print(filter_option)
            page = request.POST.get("page")
            # users_posts = UsersPosts.select_this(id)
            context_request = []
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            selected_requests = UsersPostsTeamrequests.select_requests(
                UsersPosts.select_this_byuser(id, users_fullprofile))

            if selected_requests.count() > 0:
                for requests in selected_requests:
                    if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = requests.users_fullprofiles.users.member_type
                    else:
                        if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = requests.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                            # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                    if request.LANGUAGE_CODE == "tr":
                        support_type = requests.teamrequest.users_request_investments.investments.title
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title,
                            "saat")
                    else:
                        support_type = requests.teamrequest.users_request_investments.investments.title_eng
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title_eng, "hour")

                    if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                        try:
                            hour_cost = requests.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + "" + requests.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                        except:
                            hour_cost = ""
                    else:
                        hour_cost = ""

                    if filter_option in requests.teamrequest.users_request_investments.investments.title_code:
                        item_request = {
                            "profile_img": profile_img,
                            # "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                            #                                      requests.users_fullprofiles.full_profile.first_name),
                            "profile_name":get_person_profile_name(users_fullprofile.id,requests.users_fullprofiles.id),
                            "position_title": position_title,
                            "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                            "support_type": support_type,
                            "hour_cost": hour_cost,
                            "hour_info": hour_info,
                            "id": requests.users_fullprofiles.id,
                        }

                        context_request.append(item_request)
                    elif "show-all" in filter_option:
                        item_request = {
                            "profile_img": profile_img,
                            # "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                            #                                      requests.users_fullprofiles.full_profile.first_name),
                            "profile_name":get_person_profile_name(users_fullprofile.id,requests.users_fullprofiles.id),
                            "position_title": position_title,
                            "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                            "support_type": support_type,
                            "hour_cost": hour_cost,
                            "hour_info": hour_info,
                            "id": requests.users_fullprofiles.id,
                        }

                        context_request.append(item_request)

            paginator = Paginator(context_request, 6)

            try:
                requests_list = paginator.page(page)
            except PageNotAnInteger:
                requests_list = paginator.page(1)
            except EmptyPage:
                requests_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "requests": list(requests_list),
                "has_next": requests_list.has_next(),
                "page": page,
                "message_type": "success",
            }

        else:
            context = {
                "message_type": "warning",
            }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def response_profiles(request):
    if request.method == "POST":
        users_fullprofile_list = request.POST.getlist("users[]")
        users_projects_id = request.POST.get("id")
        # print(users_fullprofile_list)
        user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        team_person_count=0
        extra_person_count=0
        context_users = []
        # owner_city = change_characters(user_fullprofile.full_profile.fullprofilelocation_set.get().location.city)
        team_person_limit = MembershipTermsType.select_terms_type(user_fullprofile.users.users_membershiptypes.name_code, "person-to-team").limit_count
        extra_person_limit = MembershipTermsType.select_terms_type(user_fullprofile.users.users_membershiptypes.name_code, "person-to-team-location").limit_count
        # print("team person limit : ",team_person_limit)
        # print("extra person limit : ",extra_person_limit)
        # seçilen bir proje var ise bu projeye kullanıcı seçebiliyor muyuz? buna bakmalıyız

        if "0" == users_projects_id or users_projects_id is None:
            # yeni açılacak proje için kullanıcılar seçilecek
            # yeni proje açmak için hakkımız var mı üyelik paketimizde
            if UsersMembershipTermsType.isOver_limitsize(user_fullprofile, "add-project",user_fullprofile.users.users_membershiptypes.name_code) == False:
                # proje ekleme limitimiz var
                team_person_count = 0
                extra_person_count = 0

                for user_item in select_teammembers_array(users_fullprofile_list,user_fullprofile.id,team_person_limit, extra_person_limit):
                    if team_person_limit == -1:
                        requested_user = UsersFullProfile.select_user(user_item)

                        if not "default" in requested_user.full_profile.picture_url.name:
                            profile_img = requested_user.full_profile.picture_url.url
                        else:
                            profile_img = requested_user.full_profile.linkedin_picture_url

                        item_user = {
                            "profile_img": profile_img,
                            # "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                            #                                      requested_user.full_profile.first_name),
                            "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                        }
                        context_users.append(item_user)
                    else:
                        if is_it_same_location(user_fullprofile.id,UsersFullProfile.select_user(user_item).id) == False:
                            # farklı konumda
                            if team_person_count < team_person_limit:
                                requested_user = UsersFullProfile.select_user(user_item)

                                if not "default" in requested_user.full_profile.picture_url.name:
                                    profile_img = requested_user.full_profile.picture_url.url
                                else:
                                    profile_img = requested_user.full_profile.linkedin_picture_url

                                item_user = {
                                    "profile_img": profile_img,
                                    "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                                }
                                context_users.append(item_user)
                                team_person_count += 1

                        else:
                            # aynı konumda
                            if extra_person_count < extra_person_limit:
                                requested_user = UsersFullProfile.select_user(user_item)

                                if not "default" in requested_user.full_profile.picture_url.name:
                                    profile_img = requested_user.full_profile.picture_url.url
                                else:
                                    profile_img = requested_user.full_profile.linkedin_picture_url

                                item_user = {
                                    "profile_img": profile_img,
                                    # "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                                    #                                      requested_user.full_profile.first_name),
                                    "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                                }
                                context_users.append(item_user)
                                extra_person_count += 1
                            elif team_person_count + extra_person_count < team_person_limit:
                                requested_user = UsersFullProfile.select_user(user_item)

                                if not "default" in requested_user.full_profile.picture_url.name:
                                    profile_img = requested_user.full_profile.picture_url.url
                                else:
                                    profile_img = requested_user.full_profile.linkedin_picture_url

                                item_user = {
                                    "profile_img": profile_img,
                                    # "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                                    #                                      requested_user.full_profile.first_name),
                                    "profile_name": get_person_profile_name(user_fullprofile.id, requested_user.id),
                                }
                                context_users.append(item_user)
                                team_person_count += 1

                sub_context = {
                    "message_type": "success",
                    "message": "",
                }

            else:
                # proje ekleme limitimiz yok
                sub_context = {
                    "message_type": "error",
                    "message": ugettext("You can't open a new project in this membership. You should benefit our paid membership if you want to create a new project"),
                }

        else:
            # var olan bir proje için kullanıcılar seçilecek
            # bu kısımda da projeye kullanıcı ekleyebilir miyiz ona bakmalıyız
            if UsersProjects.select_by_id(users_projects_id, user_fullprofile):
                # proje bize ait
                selected_users_projects = UsersProjects.select_by_id(users_projects_id, user_fullprofile)
                if Teams.is_there_team_by_project(selected_users_projects):
                    # projemizde mevcut bir takım var
                    selected_team = Teams.select_team(selected_users_projects)
                    team_person_count = (selected_team.usersteams_set.count() - 1)
                    extra_person_count = UsersTeams.my_location_teammembers_count(selected_team.users_projects,user_fullprofile.id)

                    for user_item in select_teammembers_array(users_fullprofile_list,user_fullprofile.id,team_person_limit, extra_person_limit):
                        if team_person_limit == -1:
                            requested_user = UsersFullProfile.select_user(user_item)

                            if not "default" in requested_user.full_profile.picture_url.name:
                                profile_img = requested_user.full_profile.picture_url.url
                            else:
                                profile_img = requested_user.full_profile.linkedin_picture_url

                            item_user = {
                                "profile_img": profile_img,
                                "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                            }
                            context_users.append(item_user)
                        else:
                            if is_it_same_location(user_fullprofile.id,UsersFullProfile.select_user(user_item).id) == False:
                                # kullanıcı benim lokasyonumdan farklı
                                if team_person_count < team_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),

                                    }
                                    context_users.append(item_user)
                                    team_person_count+=1

                            else:
                                # kullanıcı benim lokasyonum ile aynı
                                if extra_person_count < extra_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                                    }
                                    context_users.append(item_user)
                                    extra_person_count+=1
                                elif team_person_count + extra_person_count < team_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_person_profile_name(user_fullprofile.id, requested_user.id),
                                    }
                                    context_users.append(item_user)
                                    team_person_count += 1

                    sub_context = {
                        "message_type": "success",
                        "message": "",
                    }

                else:
                    # projemizde mevcut bir takım yok. - lik defa kuracağız
                    extra_person_count = 0
                    team_person_count = 0
                    for user_item in select_teammembers_array(users_fullprofile_list,user_fullprofile.id,team_person_limit, extra_person_limit):
                        if team_person_limit == -1:
                            requested_user = UsersFullProfile.select_user(user_item)

                            if not "default" in requested_user.full_profile.picture_url.name:
                                profile_img = requested_user.full_profile.picture_url.url
                            else:
                                profile_img = requested_user.full_profile.linkedin_picture_url

                            item_user = {
                                "profile_img": profile_img,
                                # "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                                #                                      requested_user.full_profile.first_name),
                                "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                            }
                            context_users.append(item_user)
                        else:
                            if is_it_same_location(user_fullprofile.id,UsersFullProfile.select_user(user_item).id) == False:
                                # farklı konumda
                                if team_person_count < team_person_limit:

                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                                    }
                                    context_users.append(item_user)
                                    team_person_count+=1

                            else:
                                # aynı konumda
                                if extra_person_count < extra_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name":get_person_profile_name(user_fullprofile.id,requested_user.id),
                                    }
                                    context_users.append(item_user)
                                    extra_person_count+=1

                                elif team_person_count < team_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_person_profile_name(user_fullprofile.id, requested_user.id),
                                    }
                                    context_users.append(item_user)
                                    team_person_count += 1




                    sub_context = {
                        "message_type": "success",
                        "message": "",
                    }

            else:
                # proje bize ait değil hatası
                sub_context={
                    "message_type":"error",
                    "message": ugettext("This project doesn't belong to you."),
                }

        if len(context_users) == 0:

            if team_person_count < team_person_limit:
                if request.LANGUAGE_CODE == "tr":
                    message_type = "warning"
                    message = "Konumunuz dışından " + str(team_person_limit) + " adet kişi ekleyebilirsiniz  veya üyelik paketinizi yükselterek daha fazla kişi ekleyebilirsiniz."
                else:
                    message = "You can add " + str(team_person_limit) + " person/people from outside your location. Or you should benefit our paid membership if you want to include more team member.",
                    message_type = "warning"
            elif extra_person_count < extra_person_limit:
                if request.LANGUAGE_CODE == "tr":
                    message_type = "error",
                    message = "Mevcut projenize konumunuzdan sadece " + str(extra_person_limit) + " adet kişi ekleyebilirsiniz. Daha fazla kişi eklemek isterseniz eğer mevcut üyelik paketini yükseltmeniz gerekmektedir."
                else:
                    message = "You can't include a new team member for this project from your outside your location. You can add " + str(extra_person_limit) + " person/people from your location. Or you should benefit our paid membership if you want to include more team member.",
                    message_type = "error"

            else:
                if team_person_count == team_person_limit:
                    message_type = "error"
                    if request.LANGUAGE_CODE == "tr":
                        message = "Mevcut projenize daha fazla kişiyi takıma ekleyemezsiniz. Daha fazla kişi eklemek için mevcut üyelik paketinizi yükseltmelisiniz."
                    else:
                        message = "You can't include a new team member for your project. You should benefit our paid membership if you want to include more team member."

                elif extra_person_count == extra_person_limit:
                    message_type = "error"
                    if request.LANGUAGE_CODE == "tr":
                        message = "Mevcut projenize daha fazla kişiyi takıma ekleyemezsiniz. Daha fazla kişi eklemek için mevcut üyelik paketinizi yükseltmelisiniz."
                    else:
                        message = "You can't include a new team member for your project. You should benefit our paid membership if you want to include more team member."
                else:
                    message_type = "warning"
                    if request.LANGUAGE_CODE == "tr":
                        message = "Takımınız için en az bir kişi seçmelisiniz."
                    else:
                        message = "You should choose at least one person for your team."


            context = {
                "users": context_users,
                "sub_context": sub_context,
                # "message":ugettext("You should choose at least one person for your team.")
                "message":message,
            }
        else:
            context = {
                "users": context_users,
                "sub_context": sub_context,
                "message":"",
            }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_response_to_teamrequests(request, id):
    if request.method == "POST":
        users_fullprofile_list = request.POST.getlist("users[]")
        users_project_id = request.POST.get("id") # users_posts id
        # print("post_id : ", users_project_id)
        # eğer post'tan gelen bir teklif ise users_post_id üzerinden Post tablosuna ulaşacağız

        user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        users_posts = UsersPosts.select_this(id)
        # owner_city = change_characters(user_fullprofile.full_profile.fullprofilelocation_set.get().location.city)
        # membership değerlerine bakılacak proje açma hakkı var mı, vs.

        context_mail=[]

        team_person_limit = MembershipTermsType.select_terms_type(user_fullprofile.users.users_membershiptypes.name_code, "person-to-team").limit_count
        extra_person_limit = MembershipTermsType.select_terms_type(user_fullprofile.users.users_membershiptypes.name_code, "person-to-team-location").limit_count


        if "0" is users_project_id or users_project_id is None:
            # print("yeni proje")
            if UsersMembershipTermsType.isOver_limitsize(user_fullprofile, "add-project",user_fullprofile.users.users_membershiptypes.name_code) is False:
                #yeni proje açma hakkım var
                title = uuid.uuid4().hex[:6].upper()
                describe = remove_html_tags(users_posts.posts.content_text)
                context_mail = []
                created_general = GeneralInfos.create(title, describe, "", users_project_id)
                created_project = Projects.create_generals(created_general, users_project_id)
                created_user_project = UsersProjects.create(user_fullprofile, created_project,users_posts.posts)

                UsersMembershipTermsType.create(user_fullprofile, "add-project",user_fullprofile.users.users_membershiptypes.name_code)

                ProjectUsageTypes.create(created_project, UsageTypes.select(5))
                ProjectPointsScores.create(created_project, PointScores.select("usage-type"))
                ProjectsSteps.create(created_project, Steps.select("idea"))

                # burda membership aktive edilecek. Eğer beginner ise; 1 den fazla proje olmayacak
                # proje ekleme aşamasında membership_terms_type tablosu etkilenecek ve ordan limit çekilecek
                if request.LANGUAGE_CODE == "tr":
                    team_title = title + " takımı"
                else:
                    team_title = title + " team"

                created_team = Teams.create(team_title, "", created_user_project)
                UsersTeams.create(user_fullprofile, created_team)

                extra_person_count = 0
                team_person_count = 0

                for user_item in select_teammembers_array(users_fullprofile_list,user_fullprofile.id,team_person_limit, extra_person_limit):

                    if team_person_limit == -1:
                        created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),
                                                                created_team)
                        # UsersFriend.create(user_fullprofile, UsersFullProfile.select_user(user_item))
                        NotificationsUsersTeams.create_notification(created_users_teams)
                        UsersPostsTeamrequests.confirm_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                        item_mail = {
                            "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                            "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                            "id": UsersFullProfile.select_user(user_item).id,
                        }
                        context_mail.append(item_mail)

                        # **************** WORKER KODLARI ********************
                        notification_message_tr = get_profile_name_str(
                            user_fullprofile.full_profile.last_name,
                            user_fullprofile.full_profile.first_name) + ", fikrine göndermiş olduğunuz  takım isteğini yanıtladı."

                        notification_message_en = get_profile_name_str(
                            user_fullprofile.full_profile.last_name,
                            user_fullprofile.full_profile.first_name) + ", replied your team request for his/her idea."

                        payload = {
                            "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                            "include_player_ids": send_signals_to_projects_owner(
                                UsersFullProfile.select_user(user_item).id),
                            "contents": {"en": notification_message_en, "tr": notification_message_tr}
                        }

                        requests.post("https://onesignal.com/api/v1/notifications",
                                      headers=settings.ONESIGNAL_HEADER,
                                      data=json.dumps(payload))

                        # **************** WORKER KODLARI ********************

                        team_person_count+=1
                    else:
                        if is_it_same_location(user_fullprofile.id,UsersFullProfile.select_user(user_item).id) == False:
                            # farklı konumda
                            if team_person_count < team_person_limit:
                                created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)
                                # UsersFriend.create(user_fullprofile, UsersFullProfile.select_user(user_item))
                                NotificationsUsersTeams.create_notification(created_users_teams)
                                UsersPostsTeamrequests.confirm_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                item_mail = {
                                    "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                    "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                    "id": UsersFullProfile.select_user(user_item).id,
                                }
                                context_mail.append(item_mail)
                                team_person_count += 1

                        else:
                            # aynı konumda
                            if extra_person_count < extra_person_limit:
                                created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),
                                                                        created_team)

                                NotificationsUsersTeams.create_notification(created_users_teams)
                                UsersPostsTeamrequests.confirm_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                item_mail = {
                                    "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                    "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                    "id": UsersFullProfile.select_user(user_item).id,
                                }
                                context_mail.append(item_mail)
                                extra_person_count += 1
                            elif team_person_count + extra_person_count < team_person_limit:
                                created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),
                                                                        created_team)

                                NotificationsUsersTeams.create_notification(created_users_teams)
                                UsersPostsTeamrequests.confirm_teamrequest(UsersFullProfile.select_user(user_item),
                                                                           users_posts)
                                item_mail = {
                                    "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                    "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                    "id": UsersFullProfile.select_user(user_item).id,
                                }
                                context_mail.append(item_mail)
                                team_person_count += 1

                if extra_person_count > 0 or team_person_count > 0:
                    ProjectsSteps.create(created_user_project.projects, Steps.select("teammates"))

                if responded_teamrequest_mail(context_mail,user_fullprofile.id,created_user_project.projects.general_infos.title):
                    message = ugettext("You have successfully responded to team requests. You have responded to team requests for the created new project. You can edit your project from <u>my projects</u> section.")
                    match_friends_in_system(created_team.users_projects)
                else:
                    message = ugettext("You have successfully responded to team requests but there was a problem with sendin email to team members. You have responded to team requests for the created new project. You can edit your project from <u>my projects</u> section.")

                sub_context = {
                    "message_type": "success",
                    "message": message,
                }

            else:
                # proje açma limitimiz yok
                sub_context = {
                    "message_type": "error",
                    "message": ugettext("You can't open a new project in this membership. You should benefit our paid membership if you want to create a new project"),
                }

        else:
            # seçtiğimiz proje var olan bir proje ise
            if UsersProjects.select_by_id(users_project_id, user_fullprofile):
                # proje bize ait
                selected_users_projects = UsersProjects.select_by_id(users_project_id, user_fullprofile)
                # bu kısımda var olan bir projeye bağladığımız post'u atayacağız
                selected_users_projects.connect_post_to_project(user_fullprofile,selected_users_projects.projects,users_posts.posts)

                if Teams.is_there_team_by_project(selected_users_projects):
                    # projeye ait mevcut bir takım var
                    selected_team = Teams.select_team(selected_users_projects)
                    team_person_count = (selected_team.usersteams_set.count() - 1)
                    extra_person_count = UsersTeams.my_location_teammembers_count(selected_users_projects,user_fullprofile.id)
                    for user_item in select_teammembers_array(users_fullprofile_list,user_fullprofile.id,team_person_limit, extra_person_limit):
                        # teklif veren kullanıcılar
                        if team_person_limit == -1:
                            created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item), selected_team)
                            # UsersFriend.create(user_fullprofile, UsersFullProfile.select_user(user_item))
                            NotificationsUsersTeams.create_notification(created_users_teams)
                            UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                            item_mail = {
                                "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                "id": UsersFullProfile.select_user(user_item).id,
                            }
                            context_mail.append(item_mail)

                            # **************** WORKER KODLARI ********************
                            notification_message_tr = get_profile_name_str(
                                user_fullprofile.full_profile.last_name,
                                user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                            notification_message_en = get_profile_name_str(
                                user_fullprofile.full_profile.last_name,
                                user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                            payload = {
                                "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                "include_player_ids": send_signals_to_projects_owner(
                                    UsersFullProfile.select_user(user_item).id),
                                "contents": {"en": notification_message_en, "tr": notification_message_tr}
                            }

                            requests.post("https://onesignal.com/api/v1/notifications",
                                          headers=settings.ONESIGNAL_HEADER,
                                          data=json.dumps(payload))

                            # **************** WORKER KODLARI ********************

                            team_person_count += 1
                        else:
                            if is_it_same_location(user_fullprofile.id,UsersFullProfile.select_user(user_item).id) == False:
                                # benimle farklı konumda
                                if team_person_count < team_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),selected_team)
                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id": UsersFullProfile.select_user(user_item).id,
                                    }
                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    team_person_count += 1

                            else:
                                # benimle aynı konumda
                                if extra_person_count < extra_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),selected_team)
                                    # UsersFriend.create(user_fullprofile, UsersFullProfile.select_user(user_item))
                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id":UsersFullProfile.select_user(user_item).id,
                                    }
                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    extra_person_count += 1
                                elif team_person_count + extra_person_count < team_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),selected_team)

                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id": UsersFullProfile.select_user(user_item).id,
                                    }
                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    team_person_limit += 1


                    ProjectsSteps.create(selected_users_projects.projects, Steps.select("teammates"))

                    if responded_teamrequest_mail(context_mail,user_fullprofile.id,selected_users_projects.projects.general_infos.title):
                        message = ugettext(
                            "You have successfully responded to team requests. You have responded to team requests for the created new project. You can edit your project from <u>my projects</u> section.")
                        match_friends_in_system(selected_users_projects)
                    else:
                        message = ugettext(
                            "You have successfully responded to team requests but there was a problem with sending email to team members. You have responded to team requests for the created new project. You can edit your project from <u>my projects</u> section.")



                    sub_context = {
                        "message_type": "success",
                        "message": message,
                    }

                else:
                    # projeye ait mevcut bir takım yok - ilk defa kurulacak
                    if request.LANGUAGE_CODE == "tr":
                        team_title = selected_users_projects.projects.general_infos.title.capitalize() + " takımı"
                    else:
                        team_title = selected_users_projects.projects.general_infos.title.capitalize() + " team"

                    created_team = Teams.create(team_title, "", selected_users_projects)
                    UsersTeams.create(user_fullprofile, created_team)
                    extra_person_count = UsersTeams.my_location_teammembers_count(created_team.users_projects,user_fullprofile.id)
                    team_person_count = (created_team.usersteams_set.count() - 1)

                    for user_item in select_teammembers_array(users_fullprofile_list,user_fullprofile.id,team_person_limit, extra_person_limit):
                        if team_person_limit == -1:
                            created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),
                                                                    created_team)
                            # UsersFriend.create(user_fullprofile, UsersFullProfile.select_user(user_item))
                            NotificationsUsersTeams.create_notification(created_users_teams)
                            UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                            item_mail = {
                                "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                "id": UsersFullProfile.select_user(user_item).id,
                            }

                            context_mail.append(item_mail)

                            # **************** WORKER KODLARI ********************
                            notification_message_tr = get_profile_name_str(
                                user_fullprofile.full_profile.last_name,
                                user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                            notification_message_en = get_profile_name_str(
                                user_fullprofile.full_profile.last_name,
                                user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                            payload = {
                                "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                "include_player_ids": send_signals_to_projects_owner(
                                    UsersFullProfile.select_user(user_item).id),
                                "contents": {"en": notification_message_en, "tr": notification_message_tr}
                            }

                            requests.post("https://onesignal.com/api/v1/notifications",
                                          headers=settings.ONESIGNAL_HEADER,
                                          data=json.dumps(payload))

                            # **************** WORKER KODLARI ********************

                            team_person_count += 1
                        else:
                            if is_it_same_location(user_fullprofile.id,UsersFullProfile.select_user(user_item).id) == False:
                                if team_person_count < team_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)
                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id": UsersFullProfile.select_user(user_item).id,
                                    }

                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    team_person_count += 1

                            else:
                                if extra_person_count < extra_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)

                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id": UsersFullProfile.select_user(user_item).id,
                                    }

                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    extra_person_count += 1
                                elif team_person_count + extra_person_count < team_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)

                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersPostsTeamrequests.response_teamrequest(UsersFullProfile.select_user(user_item),users_posts)
                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id": UsersFullProfile.select_user(user_item).id,
                                    }

                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        user_fullprofile.full_profile.last_name,
                                        user_fullprofile.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    team_person_limit += 1

                    ProjectsSteps.create(selected_users_projects.projects, Steps.select("teammates"))

                    if responded_teamrequest_mail(context_mail,user_fullprofile.id,selected_users_projects.projects.general_infos.title):
                        message = ugettext("You have successfully responded to team requests. You have responded to team requests for the created new project. You can edit your project from <u>my projects</u> section.")
                        match_friends_in_system(created_team.users_projects)
                    else:
                        message = ugettext("You have successfully responded to team requests but there was a problem with sending email to team members. You have responded to team requests for the created new project. You can edit your project from <u>my projects</u> section.")

                    sub_context = {
                        "message_type": "success",
                        "message": message,
                    }


            else:
                # proje bize ait değil
                sub_context = {
                    "message_type": "error",
                    "message": ugettext("This project doesn't belong to you."),
                }

        context={
            "sub_context":sub_context,
        }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def create_or_update_project(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_projects = UsersProjects.list_all(user_fullprofile)
            # proje yayında olabilir veya olmayabilir
            context_projects = []

            if UsersMembershipTermsType.isOver_limitsize(user_fullprofile, "add-project",user_fullprofile.users.users_membershiptypes.name_code) == True:
                message = ugettext(
                    "You can't create a new project in this membership package. You should benefit our paid membership if you want to create a new project.")
            else:
                message = ""

            if users_projects.count() > 0:
                for project_item in users_projects:
                    item = {
                        "title": project_item.projects.general_infos.title,
                        "id": project_item.id,  # users_projects_id
                    }
                    context_projects.append(item)

                context = {
                    "users_projects": context_projects,
                    "has_created_new_one": UsersMembershipTermsType.isOver_limitsize(user_fullprofile, "add-project",user_fullprofile.users.users_membershiptypes.name_code),
                    # "message_type":"success",
                }
            else:
                context = {
                    "users_projects": context_projects,
                    "has_created_new_one": UsersMembershipTermsType.isOver_limitsize(user_fullprofile, "add-project",user_fullprofile.users.users_membershiptypes.name_code),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def selected_person_location_for_team(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_post_id = request.POST.get("id")
            user=request.POST.get("user")
            selected_userpost=UsersPosts.select_this(users_post_id)
            selected_user=UsersFullProfile.select_user(user)

            if is_it_same_location(selected_userpost.users_fullprofiles.id,selected_user.id):
                is_it=True
            else:
                is_it=False

            context={
                # "location_name":change_characters(selected_user.full_profile.fullprofilelocation_set.get().location.city),
                "is_it":is_it
            }

            return HttpResponse(json.dumps(context),content_type="application/json")

@never_cache
@csrf_protect
def selected_person_for_team(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            selected_users_list = request.POST.getlist("users[]")
            users_post_id = request.POST.get("id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            # owner_city = change_characters(users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city)
            same_user_counter=0


            for member in selected_users_list:
                # print(member)
                if is_it_same_location(users_fullprofiles.id,UsersFullProfile.select_user(member).id):
                    same_user_counter+=1

            context={
                "location_counter":same_user_counter
            }

            return HttpResponse(json.dumps(context), content_type="application/json")
