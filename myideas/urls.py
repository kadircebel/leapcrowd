from django.urls import path
from .views import *

app_name="myideas"

urlpatterns=[
    path('',index,name='index'),
    path('detail/<int:id>/',detail,name='detail'),
    path('loadmore-similarprojects/<int:id>/', loadmore_similarprojects, name='loadmore_smilarprojects'),
    path('teamrequest/<int:id>/', teamrequest, name='teamrequest'),
    path('undecided-requests/<int:id>/', undecided_requests, name='undecided_teamrequest'),
    path('filter-teamrequests/<int:id>/', filter_teamrequests, name='filter_teamrequest'),
    path('response-profiles/', response_profiles, name='response_profiles'),
    path('response-teamrequests/<int:id>/', send_response_to_teamrequests, name='response_requests'),
    path('project-situation/', create_or_update_project, name='project_situation'),
    path('selected-person/', selected_person_for_team, name='selected_person_for_team'),
    path('selected-person-location/', selected_person_location_for_team, name='selected_person_location_for_team'),

]