from django.contrib import admin
from .models import *
# Register your models here.

class PagesAdmin(admin.ModelAdmin):
    list_display = ["title","title_eng","created_date","is_active"]

    class Meta:
        model=Pages

admin.site.register(Pages,PagesAdmin)