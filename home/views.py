from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
import uuid, json, requests
from linkedin_infos.models import CompanyInfos, Industries
from persons.models import Users, UsersCompanyInfos, UsersFullProfile, UsersInvestments, UsersIndustries, \
    UsersProjectHours, UsersTeamEmailRequests, UsersPointScores
from myprofile.models import *
from mainadmins.models import Investments, ProjectHours, ComplainedUsers, TeamEmailRequests, PointScores
from memberships.models import MembershipTypes
from utils.views import download_image_from_social_account, forgetten_password_mail, create_user_password, \
    create_crypto_name, team_request_mail, send_message_to_new_member, get_community_list,send_message_to_leapcrowd
from django.utils.translation import ugettext
from home.models import Pages
from django.views.decorators.cache import cache_page, never_cache


# Create your views here.

@never_cache
def contact_form(request):
    if request.method == "POST":
        name=request.POST.get("name")
        email=request.POST.get("email")
        subject=request.POST.get("subject")
        message=request.POST.get("message")

        if send_message_to_leapcrowd(subject,email,name,message) is True:
            if request.LANGUAGE_CODE == "tr":
                message="Mesajınız başarılı bir şekilde bize ulaşmıştır."
            else:
                message = "Your message has successfully reached us."

            context = {
                "message_type": "success",
                "message":message,
            }
        else:
            if request.LANGUAGE_CODE == "tr":
                message = "Mesajınızın gönderimi sırasında bir hata ile karşılaşıldı. Bize doğrudan eposta gönderebilirsiniz."
            else:
                message = "A problem accurred while sendin your message. You can directly send email to us."

            context = {
                "message_type": "error",
                "message":message,
            }



        return HttpResponse(json.dumps(context),content_type="application/json")


# @cache_page(60*15)
@never_cache
def index(request):
    teamrequest_pass = request.GET.get("teampass")
    # session var mı?
    if 'user_fullprofile' in request.session:
        # session haricinde takım istek kodum var mı?
        if TeamEmailRequests.isThereInvitationCode(teamrequest_pass):
            del request.session["user_fullprofile"]
            context = {
                'community':get_community_list(request.LANGUAGE_CODE),
                'isThisTeamRequest': True,
                'message_type': 'success',
                'request_code': teamrequest_pass,
                'message': ugettext(
                    "Your session will be terminate. Because you're about to be a new team member. That means you have to re-sign our platform."),
            }
            return render(request, 'frontside/home/index.html', context)
        else:
            # session'ım var fakat takım istek kodum yok ise
            # ==> normal kayıtlı kullanıcı isem
            user_id = request.session['user_fullprofile']
            if UsersFullProfile._userIsActive(user_id):
                # session'ım var ve bloklu değilsem
                context = {
                    'community': get_community_list(request.LANGUAGE_CODE),
                    'isThisTeamRequest': False,
                    'message_type': 'success',
                    'message': ugettext("Your session is still active. You can enter your dashboard."),
                    'UsersFullProfile': UsersFullProfile.select_user(user_id),
                }
                return render(request, 'frontside/home/index.html', context)
            else:
                # session'ım var fakat ve bloklu isem
                context = {
                    'community': get_community_list(request.LANGUAGE_CODE),
                    'isThisTeamRequest': False,
                    'message_type': 'warning',
                    'message': ugettext("Your session is blocked. You can contact with us for this issue."),
                }
                return render(request, 'frontside/home/index.html', context)

                # bu kısımda kişi sayısı ve dashboard butonu koyacağız
    else:
        # session sürem bittiyse => takım isteğim var mı ona bakacağız
        if TeamEmailRequests.isThereInvitationCode(teamrequest_pass):
            context = {
                'community': get_community_list(request.LANGUAGE_CODE),
                'isThisTeamRequest': True,
                'message_type': 'success',
                'request_code': teamrequest_pass,
                'message': ugettext("Your invitation code still active. You can register as a team member."),
            }
            return render(request, 'frontside/home/index.html', context)
        else:
            # takım isteğim yok ise sayfayı normal göstereceğiz.
            context = {
                'community': get_community_list(request.LANGUAGE_CODE),
                'isThisTeamRequest': False,
                'message_type': 'success',
                'message': ugettext("You can register."),

            }
            return render(request, 'frontside/home/index.html', context)


@never_cache
@csrf_protect
def change_password(request):
    rp = request.GET.get("rp")
    user_id = request.GET.get("user")
    users_fullprofile = UsersFullProfile.selectUsersFullProfileByUserId(user_id)
    if rp is None and user_id is None:
        return HttpResponseRedirect("/")
    else:
        if not "default" in users_fullprofile.full_profile.picture_url.name:
            profile_image = users_fullprofile.full_profile.picture_url.url
        else:
            profile_image = users_fullprofile.full_profile.linkedin_picture_url

        context = {
            'message': ugettext("Change you password"),
            'user_firstname': users_fullprofile.full_profile.first_name,
            'user_lastname': users_fullprofile.full_profile.last_name,
            'profile_img': profile_image,
        }
        return render(request, 'frontside/change_password.html', context)


@never_cache
@csrf_protect
def change_pass_ajax(request):
    if request.method == "POST":
        repass = request.POST.get("repass")
        rp = request.POST.get("querypass")
        user_id = request.POST.get("user_id")
        # rdnpss_crypt = create_user_password(rp)

        if Users._isPasswordChangeRequest(rp, user_id):
            Users.change_password(user_id, repass)
            users_fullprofile = UsersFullProfile.selectUsersFullProfileByUserId(user_id)
            request.session['user_fullprofile'] = users_fullprofile.id

            context = {
                'message_type': 'success',
                'redirect_url': '/dashboard/',
                'message': ugettext("Your password has changed. Now, you're redirecting to dashboard. Thanks."),
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context = {
                'message_type': 'error',
                'redirect_url': '/',
                'message': ugettext("You are canceled changing your password. Thanks."),
                'button_label': ugettext("Got it! Go to homepage."),
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        context = {
            'message_type': 'success',
            'redirect_url': '/dashboard/',
            'message': ugettext("Change your password."),
        }
        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def forgot_password_view(request):
    if request.method == "POST":
        email = request.POST.get("email")
        if UsersFullProfile.is_there_user(email):
            # mesaj çıkar ve şifre sıfırlama bağlantı sayfasının linkini gönder
            users_fullprofile = UsersFullProfile.get_user_byemail(email)
            randompass = uuid.uuid4().hex[:6].upper()

            # if request.is_secure():
            #     host_address = "https://" + request.get_host() + "/"
            # else:
            #     host_address = "http://" + request.get_host() + "/"

            # anlık pass oluşturuldu ve querystringde yollandı
            Users.create_randompass(randompass, users_fullprofile.users_id)

            if forgetten_password_mail(users_fullprofile.users.email_address, randompass, "https://www.leapcrowd.co"):
                # email gönderilip hata alınmadıysa posta kutusunu kontrol ediniz mesajı çıkaralım
                context = {
                    'message_type': 'success',
                    'redirect_url': '/dashboard/',
                    'message': ugettext("You can check your mailbox. Thanks."),
                    'button_label': ugettext("Got it!"),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # email gönderiminde hata alındı ise hata mesajı çıkar
                context = {
                    'message_type': 'error',
                    'redirect_url': '/dashboard/',
                    'message': ugettext("There was a problem sending e-mail. Please, contact us. Thanks."),
                    'button_label': ugettext(" Got it! Contact us now"),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context = {
                'message_type': 'warning',
                'message': ugettext("This email address seems to be not registered. Thanks."),
                'redirect_url': '/',
                'button_label': ugettext(" Got it! Register Now"),
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        context = {
            'message': 'Enter your e-mail address, please.',
        }
        return render(request, 'frontside/forgot_password.html', context)


@never_cache
@csrf_protect
def login_view(request):
    # del request.session["user_fullprofile"]
    if not "user_fullprofile" in request.session:
        # session yok ise form ile giriş yap
        if request.method == "POST":
            email = request.POST.get("email")
            user_pass = request.POST.get("password")

            # kullanıcı formundan gelen bilgileri kontrol ettim
            # if UsersFullProfile.objects.filter(users__email_address=email):
            if UsersFullProfile._emailAndPassIsOk(email, user_pass):
                # giriş başarılı ve session oluştur
                if ComplainedUsers._isBlocked(email):
                    # eğer email hesabı sistem tarafından bloklanmış ise
                    # uyarı mesajı çıkar ve yönlendirme
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # eğer giriş yapılan email adresi kayıtlı ise ve bloklanmamışsa
                    # giriş yap
                    users_fullprofile = UsersFullProfile.get_user_byemail(email)

                    request.session['user_fullprofile'] = users_fullprofile.id
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext("You've successfully logged in. Thanks. "),
                        'message_type': 'success',
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # giriş başarısız ve hata mesajı çıkar
                context = {
                    'redirect_url': '/login/',
                    'message': ugettext("Your email address or password is wrong. Please try again."),
                    'message_type': 'warning',
                    'button_label': ugettext(" Got it! Try again. "),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
        else:

            context = {
                'redirect_url': '/',
                'message': ugettext("You're not a member yet. You've to be a member for log in. "),
                'message_type': 'warning',
                'button_label': ugettext(" Got it! Register now"),
            }
            return render(request, 'frontside/login.html', context)
            # return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        # print("buraya geliyor")
        if request.method == "POST":
            email = request.POST.get("email")
            user_pass = request.POST.get("password")

            # kullanıcı formundan gelen bilgileri kontrol ettim
            # if UsersFullProfile.objects.filter(users__email_address=email):
            if UsersFullProfile._emailAndPassIsOk(email, user_pass):
                # giriş başarılı ve session oluştur
                if ComplainedUsers._isBlocked(email):
                    # eğer email hesabı sistem tarafından bloklanmış ise
                    # uyarı mesajı çıkar ve yönlendirme
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # eğer giriş yapılan email adresi kayıtlı ise ve bloklanmamışsa
                    # giriş yap
                    users_fullprofile = UsersFullProfile.get_user_byemail(email)

                    request.session['user_fullprofile'] = users_fullprofile.id
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext("You've successfully logged in. Thanks. "),
                        'message_type': 'success',
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # giriş başarısız ve hata mesajı çıkar
                context = {
                    'redirect_url': '/login/',
                    'message': ugettext("Your email address or password is wrong. Please try again."),
                    'message_type': 'warning',
                    'button_label': ugettext(" Got it! Try again. "),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')

        else:
            if UsersFullProfile.is_there_user_by_cookie(request.session['user_fullprofile']) is False:
                context = {
                    'redirect_url': '/',
                    'message': "",
                    'message_type': 'error',
                    'button_label': ugettext(" Got it!"),
                }
                return render(request, 'frontside/login.html', context)
                # return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                users_fullprofile_id = request.session['user_fullprofile']
                users_fullprofile = UsersFullProfile.select_user(users_fullprofile_id)
                if ComplainedUsers._isBlocked(users_fullprofile.users.email_address):
                    # eğer email hesabı sistem tarafından bloklanmış ise
                    # uyarı mesajı çıkar ve yönlendirme
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # eğer giriş yapılan email adresi kayıtlı ise ve bloklanmamışsa
                    # giriş yap
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext("You've successfully logged in. Thanks. "),
                        'message_type': 'success',
                    }
                    return redirect('/dashboard/')
                    # return render(request, 'frontside/login.html', context)


@never_cache
@csrf_protect
def login_with_social_account(request):
    if not "user_fullprofile" in request.session:
        # session yoksa giriş yap
        if request.method == "POST":
            email = request.POST.get("email")
            if UsersFullProfile.is_there_user(email):
                if ComplainedUsers._isBlocked(email):
                    # eğer email hesabı sistem tarafından bloklanmış ise
                    # uyarı mesajı çıkar ve yönlendirme
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # eğer giriş yapılan email adresi kayıtlı ise ve bloklanmamışsa
                    # giriş yap
                    users_fullprofile = UsersFullProfile.get_user_byemail(email)
                    request.session['user_fullprofile'] = users_fullprofile.id
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext("You've successfully logged in. Thanks. "),
                        'message_type': 'success',
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # giriş yapılan email kayıtlı değilse
                context = {
                    'redirect_url': '/',
                    'message': ugettext("You're not a member yet. You've to be a member before log in. "),
                    'message_type': 'warning',
                    'button_label': ugettext(" Got it! Register now"),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        # session var ise direk dashboard sayfasına yönlendir
        # users_fullprofile_id = request.session['user_fullprofile']
        if request.method == "POST":
            email = request.POST.get("email")
            users_fullprofile = UsersFullProfile.get_user_byemail(email)

            if ComplainedUsers._isBlocked(users_fullprofile.users.email_address):
                # eğer email hesabı sistem tarafından bloklanmış ise
                # uyarı mesajı çıkar ve yönlendirme
                context = {
                    'redirect_url': '/',
                    'message': ugettext("Your email address has been blocked by the system."),
                    'message_type': 'error',
                    'button_label': ugettext(" Got it! Contact us now"),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # eğer giriş yapılan email adresi kayıtlı ise ve bloklanmamışsa
                # giriş yap
                request.session['user_fullprofile'] = users_fullprofile.id
                context = {
                    'redirect_url': '/dashboard/',
                    'message': ugettext("You've successfully logged in. Thanks. "),
                    'message_type': 'success',
                }
                return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def register_view(request):
    if "user_fullprofile" in request.session:
        # üye yim bu kısım da yönlendirme yapılacak
        return redirect("/dashboard/")
    else:
        # üye değilim
        context = {
            "project_hours": ProjectHours.select_list_all(request.LANGUAGE_CODE),
            "investments": Investments.select_list_all(request.LANGUAGE_CODE),
        }
        return render(request, "frontside/register.html", context)


@never_cache
@csrf_protect
def register_linkedin(request):
    # eğer session cookie'm yok ise
    if not 'user_fullprofile' in request.session:
        # email adresim kayıtlı mı değil mi?
        if request.method == "POST":
            email = request.POST.get("email")
            if UsersFullProfile.is_there_user(email):
                # email adresimle daha önceden kayıt olduysam
                # beni üye yapma - direk mesaj çıkar ve dashboard'a gitmem için link koy
                if ComplainedUsers._isBlocked(email):
                    # böyle bir kullanıcı var ve email adresi blok yemişmi
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),

                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # böyle bir kullanıcı var ve sistemden blok yememiş ise giriş yap
                    # users_fullprofile = UsersFullProfile.get_user_byemail(email)
                    # request.session['user_fullprofile'] = users_fullprofile.id
                    context = {
                        'redirect_url': '/login/',
                        'message': ugettext("You're already a member this platform. You can enter dashboard. Thanks. "),
                        'message_type': 'warning',
                        'button_label': ugettext(" Got it! Sign in my account."),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # email adresim yok ise beni kayıt yap
                # session oluştur
                firstName = request.POST.get("firstName")
                lastName = request.POST.get("lastName")
                industry = request.POST.get("industry")
                # picture_url = request.POST.get("picture")
                summary = request.POST.get("summary")
                headline = request.POST.get("headline")
                memberType = request.POST.get("memberType")
                city = request.POST.get("city")
                country = request.POST.get("country")
                district = request.POST.get("district")
                neighborhood = request.POST.get("neighborhood")
                linkedinProfileUrl = request.POST.get("linkedinProfileUrl")
                linkedinPictureUrl = request.POST.get("picture")
                invest_id = request.POST.get("invest_type")
                project_hours = request.POST.get("project_hours")
                # positions = json.loads(request.POST.get("positions"))
                positions = request.POST.get("positions")
                lat_positions = request.POST.get("lat")
                lng_positions = request.POST.get("lng")
                is_active = True

                if len(linkedinPictureUrl) < 1:
                    linkedinPictureUrl = "/media/profiles/default.png"

                fullprofile = FullProfile.register_profile(firstName, lastName, headline, summary, linkedinPictureUrl,
                                                           linkedinProfileUrl)

                location = Locations.create(district, neighborhood, city, country, lat_positions, lng_positions)

                # fullprofile_locations
                FullprofileLocation.create_profileLocation(fullprofile, location)
                # create random pass
                randompass = uuid.uuid4().hex[:6].upper()
                rdnpss_crypt = create_user_password(randompass)
                # create random pass
                user = Users.create(email, MembershipTypes.select("beginner"), email, memberType,
                                    is_active, rdnpss_crypt)
                users_fullprofile = UsersFullProfile.create(user, fullprofile, create_crypto_name())

                industry_alone = Industries.create("-", "-", industry)
                UsersIndustries.create(users_fullprofile, industry_alone)

                selected_invest = Investments.select_invest_type(invest_id)
                selected_projecthours = ProjectHours.select_project_hours(project_hours)
                # invest kısmını belirledik
                UsersInvestments.create(selected_invest, users_fullprofile)
                # create project_house
                UsersProjectHours.create(users_fullprofile, selected_projecthours)
                try:
                    for company in json.loads(positions):
                        comp_info = CompanyInfos.create(company["company"]["name"], company['title'],
                                                        company['location']['name'], company['startDate']['month'],
                                                        company['startDate']['year'], company['isCurrent'], "", "")
                        # users_companyinfos koyulacak
                        UsersCompanyInfos.create(users_fullprofile, comp_info)
                except:
                    pass

                # session oluşturuldu
                request.session['user_fullprofile'] = users_fullprofile.id

                # create points
                # location points
                UsersPointScores.create(users_fullprofile, PointScores.select("location"))
                # investment points
                UsersPointScores.create(users_fullprofile, PointScores.select("support-type"))
                # project-hours
                UsersPointScores.create(users_fullprofile, PointScores.select("hours"))
                # summary
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-summary"))
                # profile-image
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-image"))
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-headline"))

                if 'tech-team' in memberType:
                    # download_image_from_social_account(linkedinPictureUrl,users_fullprofile.id)
                    context = {
                        'redirect_url': '/dashboard/',
                        'message_one': ugettext("Your team requests sended."),
                        'message_two': ugettext(
                            "You're successfully registered. You are currently being directed to your dashboard. Thanks."),
                        'usersfullprofile_id': users_fullprofile.id,

                        'message_type': 'success',
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    download_image_from_social_account(linkedinPictureUrl, users_fullprofile.id)
                    # burdayım
                    send_message_to_new_member(users_fullprofile.id, request.LANGUAGE_CODE)
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext(
                            "You're successfully registered. You are currently being directed to your dashboard. Thanks."),
                        'message_type': 'success',
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context = {
                'redirect_url': '/',
                'message': ugettext("Please log in to go to the your dashboard. Thanks"),
                'message_type': 'warning',
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        # eğer cookiem var ise
        if request.method == "POST":
            email = request.POST.get("email")
            # cookiem var fakat cookie'si olan email adresim kayıtlı mı?
            if UsersFullProfile.is_there_user(email):
                if ComplainedUsers._isBlocked(email):
                    # kayıtlı ve bloklu mu?
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # kayıtlı ve bloklu değilse
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext("You're already a member this platform. You can enter dashboard. Thanks. "),
                        'message_type': 'warning',
                        'button_label': ugettext(" Got it! Sign in my account."),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # cookiem var fakat email adresim kayıtlı değil ise
                # kayıt işlemini gerçekleştir
                firstName = request.POST.get("firstName")
                lastName = request.POST.get("lastName")
                industry = request.POST.get("industry")
                summary = request.POST.get("summary")
                headline = request.POST.get("headline")
                memberType = request.POST.get("memberType")
                city = request.POST.get("city")
                country = request.POST.get("country")
                district = request.POST.get("district")
                neighborhood = request.POST.get("neighborhood")
                linkedinProfileUrl = request.POST.get("linkedinProfileUrl")
                linkedinPictureUrl = request.POST.get("picture")
                invest_id = request.POST.get("invest_type")
                project_hours = request.POST.get("project_hours")
                # positions = json.loads(request.POST.get("positions"))
                positions = request.POST.get("positions")
                lat_positions = request.POST.get("lat")
                lng_positions = request.POST.get("lng")
                is_active = True
                if len(linkedinPictureUrl) < 1:
                    linkedinPictureUrl = "/media/profiles/default.png"

                fullprofile = FullProfile.register_profile(firstName, lastName, headline, summary,
                                                           linkedinPictureUrl, linkedinProfileUrl)

                location = Locations.create(district, neighborhood, city, country, lat_positions, lng_positions)
                # fullprofile_locations
                FullprofileLocation.create_profileLocation(fullprofile, location)
                # create random pass
                randompass = uuid.uuid4().hex[:6].upper()
                rdnpss_crypt = create_user_password(randompass)
                # create random pass
                user = Users.create(email, MembershipTypes.select("beginner"), email, memberType,
                                    is_active, rdnpss_crypt)
                users_fullprofile = UsersFullProfile.create(user, fullprofile, create_crypto_name())

                industry_alone = Industries.create("-", "-", industry)
                UsersIndustries.create(users_fullprofile, industry_alone)

                selected_invest = Investments.select_invest_type(invest_id)
                selected_projecthours = ProjectHours.select_project_hours(project_hours)
                # invest kısmını belirledik
                UsersInvestments.create(selected_invest, users_fullprofile)
                # create project_house
                UsersProjectHours.create(users_fullprofile, selected_projecthours)
                try:
                    for company in json.loads(positions):
                        comp_info = CompanyInfos.create(company["company"]["name"], company['title'],
                                                        company['location']['name'], company['startDate']['month'],
                                                        company['startDate']['year'], company['isCurrent'], "", "")
                        # users_companyinfos koyulacak
                        UsersCompanyInfos.create(users_fullprofile, comp_info)
                except:
                    pass
                request.session['user_fullprofile'] = users_fullprofile.id

                # create points
                # location points
                UsersPointScores.create(users_fullprofile, PointScores.select("location"))
                # investment points
                UsersPointScores.create(users_fullprofile, PointScores.select("support-type"))
                # project-hours
                UsersPointScores.create(users_fullprofile, PointScores.select("hours"))
                # summary
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-summary"))
                # profile-image
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-image"))
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-headline"))

                # eğer takım isteği yollandıysa
                if 'tech-team' in memberType:
                    download_image_from_social_account(linkedinPictureUrl, users_fullprofile.id)
                    context = {
                        'redirect_url': '/dashboard/',
                        'message_one': ugettext("Your team requests sended."),
                        'message_two': ugettext(
                            "You're successfully registered. You are currently being directed to your dashboard. Thanks."),
                        'usersfullprofile_id': users_fullprofile.id,

                        'message_type': 'success',
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    download_image_from_social_account(linkedinPictureUrl, users_fullprofile.id)
                    send_message_to_new_member(users_fullprofile.id, request.LANGUAGE_CODE)
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext(
                            "You're successfully registered. You are currently being directed to your dashboard. Thanks."),
                        'message_type': 'success',
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def register_facebook(request):
    # eğer session cookie'm yok ise
    if not 'user_fullprofile' in request.session:
        # email adresim kayıtlı mı değil mi?
        if request.method == "POST":
            email = request.POST.get("email")
            if UsersFullProfile.is_there_user(email):
                # email adresimle daha önceden kayıt olduysam
                # beni üye yapma - direk mesaj çıkar ve dashboard'a gitmem için link koy
                if ComplainedUsers._isBlocked(email):
                    # böyle bir kullanıcı var ve email adresi blok yemişmi
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # böyle bir kullanıcı var ve sistemden blok yememiş ise giriş yap
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext("You're already a member this platform. You can enter dashboard. Thanks. "),
                        'message_type': 'warning',
                        'button_label': ugettext(" Got it! Sign in my account."),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # email adresim yok ise beni kayıt yap
                # session oluştur
                firstName = request.POST.get("firstName")
                lastName = request.POST.get("lastName")
                picture_url = request.POST.get("picture")
                memberType = request.POST.get("memberType")
                city = request.POST.get("city")
                country = request.POST.get("country")
                district = request.POST.get("district")
                neighborhood = request.POST.get("neighborhood")
                invest_id = request.POST.get("invest_type")
                project_hours = request.POST.get("project_hours")
                lat_positions = request.POST.get("lat")
                lng_positions = request.POST.get("lng")
                is_active = True
                if len(picture_url) < 1:
                    picture_url = "/media/profiles/default.png"

                fullprofile = FullProfile.register_profile_facebook(firstName, lastName, picture_url)

                location = Locations.create(district, neighborhood, city, country, lat_positions, lng_positions)
                # fullprofile_locations
                FullprofileLocation.create_profileLocation(fullprofile, location)
                # create random pass
                randompass = uuid.uuid4().hex[:6].upper()
                rdnpss_crypt = create_user_password(randompass)
                # create random pass
                user = Users.create(email, MembershipTypes.select("beginner"), email, memberType,
                                    is_active, rdnpss_crypt)

                # industry_alone = Industries.create("-", "-", industry)
                # UsersIndustries.create(user, industry_alone)
                users_fullprofile = UsersFullProfile.create(user, fullprofile, create_crypto_name())
                selected_invest = Investments.select_invest_type(invest_id)
                selected_projecthours = ProjectHours.select_project_hours(project_hours)
                # invest kısmını belirledik
                UsersInvestments.create(selected_invest, users_fullprofile)
                # create project_house
                UsersProjectHours.create(users_fullprofile, selected_projecthours)
                # session oluşturuldu
                request.session['user_fullprofile'] = users_fullprofile.id

                # create points
                # location points
                UsersPointScores.create(users_fullprofile, PointScores.select("location"))
                # investment points
                UsersPointScores.create(users_fullprofile, PointScores.select("support-type"))
                # project-hours
                UsersPointScores.create(users_fullprofile, PointScores.select("hours"))
                # summary - facebook tarafında yok
                # UsersPointScores.create(users_fullprofile, PointScores.select("profile-summary"))
                # profile-image
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-image"))

                # kullanıcı oluşturulduktan sonra email atma işlemi olacak;
                download_image_from_social_account(picture_url, users_fullprofile.id)
                send_message_to_new_member(users_fullprofile.id, request.LANGUAGE_CODE)
                context = {
                    'redirect_url': '/dashboard/',
                    'message': ugettext(
                        "You're successfully registered. You are currently being directed to your dashboard. Thanks."),
                    'message_type': 'success',
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context = {
                'redirect_url': '/',
                'message': ugettext("Please log in to go to the your dashboard. Thanks"),
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        # eğer cookiem var ise
        if request.method == "POST":
            email = request.POST.get("email")
            # cookiem var fakat cookie'si olan email adresim kayıtlı mı?
            if UsersFullProfile.is_there_user(email):
                if ComplainedUsers._isBlocked(email):
                    # kayıtlı ve bloklu mu?
                    context = {
                        'redirect_url': '/',
                        'message': ugettext("Your email address has been blocked by the system."),
                        'message_type': 'error',
                        'button_label': ugettext(" Got it! Contact us now"),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # kayıtlı ve bloklu değilse
                    context = {
                        'redirect_url': '/dashboard/',
                        'message': ugettext("You're already a member this platform. You can enter dashboard. Thanks. "),
                        'message_type': 'warning',
                        'button_label': ugettext(" Got it! Sign in my account."),
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                # cookiem var fakat email adresim kayıtlı değil ise
                # kayıt işlemini gerçekleştir
                firstName = request.POST.get("firstName")
                lastName = request.POST.get("lastName")
                picture_url = request.POST.get("picture")
                memberType = request.POST.get("memberType")
                city = request.POST.get("city")
                country = request.POST.get("country")
                district = request.POST.get("district")
                neighborhood = request.POST.get("neighborhood")
                invest_id = request.POST.get("invest_type")
                project_hours = request.POST.get("project_hours")
                lat_positions = request.POST.get("lat")
                lng_positions = request.POST.get("lng")
                is_active = True
                if len(picture_url) < 1:
                    picture_url = "/media/profiles/default.png"

                fullprofile = FullProfile.register_profile_facebook(firstName, lastName, picture_url)

                location = Locations.create(district, neighborhood, city, country, lat_positions, lng_positions)
                # fullprofile_locations
                FullprofileLocation.create_profileLocation(fullprofile, location)
                # create random pass
                randompass = uuid.uuid4().hex[:6].upper()
                rdnpss_crypt = create_user_password(randompass)
                # create random pass
                user = Users.create(email, MembershipTypes.select("beginner"), email, memberType,
                                    is_active, rdnpss_crypt)
                # industry_alone = Industries.create("-", "-", industry)
                # UsersIndustries.create(user, industry_alone)
                users_fullprofile = UsersFullProfile.create(user, fullprofile)
                selected_invest = Investments.select_invest_type(invest_id)
                selected_projecthours = ProjectHours.select_project_hours(project_hours)
                # invest kısmını belirledik
                UsersInvestments.create(selected_invest, users_fullprofile)
                # create project_house
                UsersProjectHours.create(users_fullprofile, selected_projecthours)

                # session oluşturuldu
                request.session['user_fullprofile'] = users_fullprofile.id

                # create points
                # location points
                UsersPointScores.create(users_fullprofile, PointScores.select("location"))
                # investment points
                UsersPointScores.create(users_fullprofile, PointScores.select("support-type"))
                # project-hours
                UsersPointScores.create(users_fullprofile, PointScores.select("hours"))
                # summary - facebook tarafında yok
                # UsersPointScores.create(users_fullprofile, PointScores.select("profile-summary"))
                # profile-image
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-image"))
                download_image_from_social_account(picture_url, users_fullprofile.id)
                send_message_to_new_member(users_fullprofile.id, request.LANGUAGE_CODE)
                # kullanıcı oluşturulduktan sonra email atma işlemi olacak;
                context = {
                    'redirect_url': '/dashboard/',
                    'message': ugettext(
                        "You're successfully registered. You are currently being directed to your dashboard. Thanks."),
                    'message_type': 'success',
                }
                return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def sendTeamRequestAndRegister(request):
    # try:
    if request.method == "POST":
        usersfullprofile_id = request.POST.get("usersfullprofile_id")
        if UsersTeamEmailRequests._isThereInvitation(usersfullprofile_id):
            context = {
                'redirect_url': "/",
                'message_type': 'success',
                'message': ugettext(
                    "You're already sent these requests. You can try to send these requests again on your dashboard"),
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            emailaddressList = json.loads(request.POST.get("emailList"))
            request_pass = uuid.uuid4().hex[:6].upper()
            user_fullprofile = UsersFullProfile.select_user(usersfullprofile_id)
            is_active = True
            for i in range(len(emailaddressList)):
                if not UsersFullProfile.objects.filter(users__email_address=emailaddressList[i]).exists():
                    team_request = TeamEmailRequests()
                    team_request.request_password = request_pass
                    team_request.request_email = emailaddressList[i]
                    team_request.is_active = is_active
                    team_request.save()

                    u_teamrequest = UsersTeamEmailRequests()
                    u_teamrequest.users_fullprofiles = user_fullprofile
                    u_teamrequest.team_requests = team_request
                    u_teamrequest.is_active = is_active
                    u_teamrequest.save()

                # email yollama işi
                if team_request_mail(emailaddressList, user_fullprofile.id, request_pass):
                    context = {
                        'message_type': 'success',
                        'message_one': ugettext("Your team requests sended."),
                        'message_two': ugettext(
                            "You're successfully registered. You are currently being directed to your dashboard. Thanks."),
                        'redirect_url': '/dashboard/',
                        'message': "",
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    context = {
                        'message_type': 'error',
                        'message_one': ugettext("There was a problem sending e-mail."),
                        'message_two': ugettext(
                            "Please, contact us. Thanks."),
                        'redirect_url': '/',
                        'message': "",
                    }
                    return HttpResponse(json.dumps(context), content_type='application/json')


@cache_page(60 * 30)
def privacy_policies(request):
    if request.method == "GET":
        select_main_page = Pages.select_main_page("privacy-policy")
        context_sub_pages = []

        if request.LANGUAGE_CODE == "tr":
            page_title = select_main_page.title
            page_content = select_main_page.content_text

            for sub_page in Pages.select_sub_pages(select_main_page):
                item = {
                    "title": sub_page.title,
                    "page_content": sub_page.content_text
                }
                context_sub_pages.append(item)

        else:
            page_title = select_main_page.title_eng
            page_content = select_main_page.content_text_eng

            for sub_page in Pages.select_sub_pages(select_main_page):
                item = {
                    "title": sub_page.title_eng,
                    "page_content": sub_page.content_text_eng
                }
                context_sub_pages.append(item)

        context = {
            "title": page_title,
            "page_content": page_content,
            "sub_page": context_sub_pages,
            "created_date": str(select_main_page.created_date.date()),
            'page_title': ugettext("Privacy policy")
        }

        return HttpResponse(json.dumps(context), content_type="application/json")
        # return render(request,"frontside/home/privacy_policy.html",context)


@cache_page(60 * 30)
# @never_cache
def terms_of_service(request):
    if request.method == "GET":
        select_main_page = Pages.select_main_page("terms-of-service")
        context_sub_pages = []

        if request.LANGUAGE_CODE == "tr":
            page_title = select_main_page.title
            page_content = select_main_page.content_text

            for sub_page in Pages.select_sub_pages(select_main_page):
                item = {
                    "title": sub_page.title,
                    "page_content": sub_page.content_text
                }
                context_sub_pages.append(item)

        else:
            page_title = select_main_page.title_eng
            page_content = select_main_page.content_text_eng

            for sub_page in Pages.select_sub_pages(select_main_page):
                item = {
                    "title": sub_page.title_eng,
                    "page_content": sub_page.content_text_eng
                }
                context_sub_pages.append(item)

        context = {
            "title": page_title,
            "page_content": page_content,
            "sub_page": context_sub_pages,
            'page_title': ugettext("Terms of service")
        }

        return HttpResponse(json.dumps(context), content_type="application/json")