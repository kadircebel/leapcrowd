from django.db import models

# Create your models here.

class Pages(models.Model):
    id=models.BigAutoField(primary_key=True)
    created_date = models.DateTimeField(auto_now_add=True)
    content_text = models.TextField(blank=True)
    content_text_eng=models.TextField(blank=True)
    title = models.CharField(max_length=500, blank=True)
    title_eng = models.CharField(max_length=500, blank=True)
    is_active=models.BooleanField(default=True)
    parent_page=models.ForeignKey("Pages",models.DO_NOTHING,blank=True,null=True)
    title_type=models.CharField(max_length=500, blank=True)

    class Meta:
        managed=True
        db_table="pages"

    def __str__(self):
        return '{}'.format(self.title)

    @classmethod
    def select_main_page(cls,title_slug):
        selected=cls.objects.filter(title_type=title_slug).get()
        return selected

    @classmethod
    def select_sub_pages(cls,parent_page):
        if cls.objects.filter(parent_page=parent_page).count() > 0:
            return cls.objects.filter(parent_page=parent_page).all().order_by("id")
        else:
            return False