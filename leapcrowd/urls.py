"""leapcrowd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.views.generic import TemplateView

from home.views import index,register_linkedin,register_facebook,sendTeamRequestAndRegister,login_view,forgot_password_view,login_with_social_account,change_password,change_pass_ajax,privacy_policies,terms_of_service,register_view,contact_form
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',index,name='home'),
    path('privacy-policy/',privacy_policies,name='privacy_policy'),
    path('terms-of-service/',terms_of_service,name='terms_of_service'),
    path('register_linkedin/',register_linkedin,name='register_linkedin'),
    path('register_facebook/',register_facebook,name='register_facebook'),
    path('recover-password/',forgot_password_view,name='home'),
    path('rechange-password/',change_pass_ajax,name='home'),
    path('change-password/',change_password,name='home'),
    path('contact-form/',contact_form,name='home'),
    path('login/',login_view,name='home'),
    path('register/',register_view,name="home"),
    path('login-social/',login_with_social_account,name='home'),
    path('buildteam/',sendTeamRequestAndRegister,name='home'),
    path('dashboard/',include('dashboard.urls'),name='dashboard'),
    path('dashboard/notifications/',include('notifications.urls'),name='notifications'),
    path('dashboard/ideas/',include('ideas.urls'),name='ideas'),
    #myprofile olan kısım profiles kısmına taşınacak
    path('dashboard/profiles/',include('persons.urls'),name='persons'),
    path('dashboard/projects/',include('projects.urls'),name='projects'),
    path('dashboard/myideas/',include('myideas.urls'),name='myideas'),
    path('dashboard/myprojects/',include('myprojects.urls'),name='myprojects'),
    path('dashboard/myteams/',include('myteams.urls'),name='my-teams'),
    #bu kısım ilerlendikçe düzeltilecek
    path('dashboard/myprofile/',include('myprofile.urls'),name='my-profile'),
    path('dashboard/updates/',include('updates.urls'),name='updates'),
    path('dashboard/faq/',include('faq.urls'),name='faqs'),
    path('dashboard/main/',include('mainadmins.urls'),name='mainadmins'),
    #linkedin_infos
    path('dashboard/skills/',include('linkedin_infos.urls'),name='linkedin-infos'),

    path('dashboard/messages/',include('users_messagebox.urls'),name='messages'),
    path('dashboard/memberships/',include('memberships.urls'),name='memberships'),

    path('manifest.json', (TemplateView.as_view(template_name="socialside/manifest.json", content_type='application/json', )), name='manifest.json'),
    path('OneSignalSDKWorker.js', (TemplateView.as_view(template_name="socialside/OneSignalSDKWorker.js", content_type='application/javascript', )), name='OneSignalSDKWorker.js'),
    path('OneSignalSDKUpdaterWorker.js', (TemplateView.as_view(template_name="socialside/OneSignalSDKUpdaterWorker.js", content_type='application/javascript', )), name='OneSignalSDKUpdaterWorker.js'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
