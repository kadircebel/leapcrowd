from django.db import models


class Teams(models.Model):
    team_title = models.CharField(max_length=250, blank=True)
    users_projects=models.ForeignKey('persons.UsersProjects',models.DO_NOTHING,blank=True)
    team_content = models.TextField(blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.NullBooleanField(default=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'teams'

    @classmethod
    def is_there_team_by_project(cls,users_projects):
        try:
            if cls.objects.filter(users_projects=users_projects).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def create(cls,team_title,team_content,users_projects):
        try:
            if not cls.objects.filter(users_projects=users_projects).exists():
                new_record = cls(team_title=team_title, team_content=team_content,users_projects=users_projects)
                new_record.save()
                return new_record
            else:
                return cls.objects.filter(users_projects=users_projects).get()

        except:
            new_record=cls(team_title=team_title,team_content=team_content,users_projects=users_projects)
            new_record.save()
            return new_record

    @classmethod
    def is_project_owner(cls,users_fullprofiles):
        try:
            if cls.objects.filter(users_projects__users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def teammates_by_usersprojects(cls,users_projects):
        return cls.objects.filter(users_projects=users_projects).get()

    @classmethod
    def select_team(cls,users_projects):
        return cls.objects.filter(users_projects=users_projects).get()






class AlterUsersTeams(models.Model):
    teams = models.ForeignKey('Teams', models.DO_NOTHING, blank=True)
    # users = models.ForeignKey('persons.Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    request_text = models.TextField(blank=True)
    is_request = models.NullBooleanField()
    request_date = models.DateField(auto_created=True)
    is_response = models.NullBooleanField()
    response_date = models.DateField(auto_now_add=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'alter_users_teams'