/**
 * Created by kadircebel on 28.03.2018.
 */
var place_text = "";

function hasNumber(myString) {
    var withNoDigits = myString.replace(/[0-9]/g, '');
    myString = myString.replace(/\d+/g, '');

    return myString;
}

function splitThis(addresstext) {
    var address = [];

    var str = addresstext;
    var res = str.split(',');
    for (i = 0; i < res.length; i++) {

        address.push(hasNumber(res[i]).trim());
    }

    return address;
}

function saveLocations(data) {
    var country = "";
    var city = "";
    var district = "";
    var neighborhood = "";
    var citybox = [];
    var newAddressText = [];

    if (data.length === 2) {
        country = data[1];
        if (data[0].indexOf('/') > -1) {
            var res = data[0].split('/');
            for (i = 0; i < res.length; i++) {
                citybox.push(hasNumber(res[i]).trim());
            }
            district = citybox[0];
            city = citybox[1];
            neighborhood = "-";
        }
        else {
            city = data[0];
            district = "-";
            neighborhood = "-";
        }


    }
    else if (data.length === 3) {
        country = data[2];
        if (data[1].indexOf('/') > -1) {
            var res = data[1].split('/');
            for (i = 0; i < res.length; i++) {
                citybox.push(hasNumber(res[i]).trim());
            }
            district = citybox[0];
            city = citybox[1];
            neighborhood = data[0];
        }
        else {
            city = data[1];
            district = data[0];
            neighborhood = "-";
        }

    }
    else if (data.length === 4) {
        country = data[3];
        if (data[2].indexOf('/') > -1) {
            var res = data[2].split('/');
            for (i = 0; i < res.length; i++) {
                citybox.push(hasNumber(res[i]).trim());
            }
            district = citybox[0];
            city = citybox[1];
            neighborhood = data[1];
        }
        else {
            city = data[2];
            district = data[1];
            neighborhood = data[0];
        }
    }
    else {
        country = data[0];
        city = "-";
        district = "-";
        neighborhood = "-";
    }
    citybox = [];

    newAddressText.push(country);
    newAddressText.push(city);
    newAddressText.push(district);
    newAddressText.push(neighborhood);



    return newAddressText;
}

function googlestarterfunc() {
    if($("#profil1").find("#location-place-profile").length > 0)
    {
       initAutocomplete_profile();
    }
}

function initAutocomplete_profile() {
    //for entrepreneur section
    var myaddress = [];
    var input = document.getElementById('location-place-profile');
    var options = {
        types: ['(cities)'],
        placeIdOnly: true
    };
    var searchBox = new google.maps.places.SearchBox(input, options);
    //var myAddress = [];
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': places[0].formatted_address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                //results[0].geometry.location.lat(), results[0].geometry.location.lng()
                $("#lat-position-profile").val(results[0].geometry.location.lat());
                $("#lng-position-profile").val(results[0].geometry.location.lng());

            }
        });

        place_text = places[0]['formatted_address'];
        myaddress = splitThis(place_text);


        var nAddress = saveLocations(myaddress);

        $("#city-name-profile").val(nAddress[1]);
        $("#district-name-profile").val(nAddress[2]);
        $("#neighborhood-name-profile").val(places[3]);
        $("#country-name-profile").val(nAddress[0]);
        $("#place-name-profile").val(place_text);

        if (places.length === 0) {
            return;
        }
        myaddress = [];
    });


}

