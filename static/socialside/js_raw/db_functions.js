/**
 * Created by kadircebel on 10.06.2018.
 */
function addPositions(x, wrapper, positionTitleStr, companyTitleStr, startYearTitle, endYearTitle, errorText, positionTitlePlaceholder, companyTitlePlaceholder, startYearPlaceholder, endYearPlaceholder, stillThereStr) {
    var htmlString = "";
    var firstRow = "";
    var secondRow = "";
    var thirdRow = "";

    firstRow = '<div class="row"><div class="col-md-12 col-12">' +
        '<div class="form-group">' +
        '<label for="compnay-name-' + x + '" class="c-title">' + companyTitleStr + '</label>' +
        '<input class="form-control company-name" type="text" name="company-name-' + x + '" placeholder="' + companyTitlePlaceholder + '">' +
        '<div class="invalid-feedback">' + errorText + '</div></div></div></div>';

    firstRow += '<div class="row"><div class="col-md-12 col-12">' +
        '<div class="form-group">' +
        '<label for="position-title-' + x + '" class="p-title">' + positionTitleStr + '</label>' +
        '<input class="form-control position-title" type="text" name="position-title-' + x + '" placeholder="' + positionTitlePlaceholder + '">' +
        '<div class="invalid-feedback">' + errorText + '</div></div>' +
        '<div class="card positions-result-container"><div class="media-list media-list-hover media-list-divided scrollable ps-container ps-theme-default positions-result border ps-active-y"></div></div>'
        + '</div></div>';

    secondRow = '<div class="row"><div class="col-md-6 col-6">' +
        '<div class="form-group">' +
        '<label for="start-year-' + x + '" class="s-year">' + startYearTitle + '</label>' +
        '<input class="form-control start-year" type="text" name="start-year-' + x + '" data-provide="datepicker" placeholder="' + startYearPlaceholder + '" />' +
        '<div class="invalid-feedback">' + errorText + '</div></div></div>';

    secondRow += '<div class="col-md-6 col-6 end-year-container-' + x + '">' +
        '<div class="form-group">' +
        '<label for="end-year-' + x + '" class="e-year">' + endYearTitle + '</label>' +
        '<input class="form-control end-year" type="text" name="end-year-' + x + '" data-provide="datepicker" placeholder="' + endYearPlaceholder + '" />' +
        '<div class="invalid-feedback">' + errorText + '</div></div></div></div>';

    thirdRow = '<div class="row"><div class="col-md-12 col-12">' +
        '<label class="custom-control custom-checkbox still-there">' +
        '<input type="checkbox" class="custom-control-input" data-endyear-title="end-year-container-' + x + '">' +
        '<span class="custom-control-indicator"></span>' +
        '<span class="custom-control-description">' + stillThereStr + '</span></label></div></div>';

    htmlString = '<div class="main-div another-added">' + firstRow + secondRow + thirdRow + '<hr /></div>';

    $(wrapper).append(htmlString);

    positionTitleStr = "";
    companyTitleStr = "";
    startYearTitle = "";
    endYearTitle = "";
    errorText = "";
    positionTitlePlaceholder = "";
    companyTitlePlaceholder = "";
    startYearPlaceholder = "";
    endYearPlaceholder = "";
    stillThereStr = "";
    htmlString = "";
    firstRow = "";
    secondRow = "";
    thirdRow = "";

}

function changePasswordSubmit(repass, user_id, querypass) {
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $("#main-loading").show();
            },
            url: '/rechange-password/',
            data: {'repass': repass, 'user_id': user_id, 'querypass': querypass},
            success: function (data) {
                if (data.message_type === "error") {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="mailto:kadir@leapcrowd.co" class="btn btn-lg btn-round     btn-danger px-6 mt-5" ><i class="fa fa-warning"></i> ' + data.button_label + '</a>');
                }
                else if (data.message_type === "success") {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000);
                }
                else {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="' + data.redirect_url + '" class="btn btn-lg btn-round     btn-warning px-6 mt-5" ><i class="fa fa-warning"></i> ' + data.button_label + '</a>');
                }
                $("#hide-loading").click(function () {
                    $("#main-loading").hide();
                    $("#main-loading").html('');
                })

            },
            error: function (data) {

            }
        }
    );
}

function removeExperiencesLatestAdded() {
    if ($("#experiences-container").find(".main-div").length > 0) {
        $("#experiences-container").each(function () {
            $(this).find("input").val('');
        })
    }
}

function showSkills() {


    $.getJSON("/dashboard/profiles/get-skills/?id=" + id_return(), function (data) {
        $("#skill-result").html('');
        $.each(data, function (key, value) {
            allSkills.push(value.id);
            $("#skill-result").append('<button data-value-id="' + value.id + '" class="btn btn-label btn-outline-danger remove-skill delete-skill btn-round mx-2 my-3 fs-12" type="button"><label><i class="ti-close"></i></label>' + value.title + '</button>');
        });
    });
}

function showExperiences() {
    $.getJSON("/dashboard/profiles/all-experiences/", function (data) {

        $("#experiences-list").html('');
        var start_date = "";
        var end_date = "";
        if (data.length > 0) {
            $("#saved-experiences").show();

            $.each(data, function (key, value) {
                if (value.start_month.trim().length > 0 && value.start_year.trim().length > 0) {
                    if (value.start_month.trim().length === 1 && value.start_month.trim() !== "-") {
                        start_date = "0" + value.start_month + "/" + value.start_year;
                    }
                    else {
                        start_date = value.start_month + "/" + value.start_year;
                    }
                }
                else {
                    start_date = " - " + '<i class="ti-infinite"></i>';
                }
                if (value.end_month.trim().length > 0 && value.end_year.trim().length > 0 && value.end_month.trim() !== "-") {
                    if (value.end_month.trim().length === 1) {
                        end_date = " - " + "0" + value.end_month + "/" + value.end_year;
                    }
                    else {
                        end_date = " - " + value.end_month + "/" + value.end_year;
                    }

                }
                else {
                    end_date = " - " + '<i class="ti-infinite"></i>';
                }

                $("#experiences-list").append('<div class="media mt-0 pl-0">' +
                    '<div class="media-body">' +
                    '<h5 class="card-title pl-0 pt-0 pb-1" style="text-transform: capitalize;">' + value.name + '</h5>' +
                    '<p class="float-left" style="text-transform: capitalize;">' + value.title + '</p>' +
                    '<time class="float-right">' + start_date + end_date + '</time>' +
                    '<div class="btn-spacer mt-5 text-right">' +
                    '<a class="btn btn-xs btn-w-xs btn-outline btn-primary edit-experience" data-id="' + value.id + '" href="#profil2">' + value.edit_btn + '</a>' +
                    '<a class="btn btn-xs btn-w-xs btn-outline btn-danger delete-experience" data-id="' + value.id + '" href="javascript:void(0);">' + value.delete_btn + '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
            });

        }
        else {
            $("#saved-experiences").hide();
        }

    }).done(function () {
        $(".delete-experience").on("click", function () {
            var id = $(this).attr("data-id");
            $.getJSON("/dashboard/profiles/delete-experience/?id=" + id, function (data) {
                showExperiences();
                getUserPoints();
            });
        });

        $(".edit-experience").on("click", function () {
            var id = $(this).attr("data-id");
            $.getJSON("/dashboard/profiles/edit-experience/?id=" + id, function (data) {
                fillExperiences(data.name, data.title, data.start_date, data.end_date, data.is_current, id);
                $(document).find("#experience-save").attr("data-situation", "update");
                $(document).find("#experience-save").attr("data-id", id);
            });


        });

    });
}

function fillExperiences(name, title, start_date, end_date, is_current, users_companyinfos_id) {
    $("#experiences-container").find(".company-name").val(name);
    $("#experiences-container").find(".position-title").val(title);
    $("#experiences-container").find(".start-year").val(start_date);
    switch (is_current) {
        case true:
            isStillThere = true;
            $("#experiences-container").find(".still-there").find(".custom-control-input").attr("checked", "checked");
            $("#experiences-container").find(".end-year-container").fadeOut();
            break;
        case false:
            isStillThere = false;
            $("#experiences-container").find(".end-year-container").fadeIn();
            $("#experiences-container").find(".end-year").val(end_date);
            break;
    }
}

function positionSearch() {
    $(".position-title").unbind("keyup").bind("keydown", function () {
        var getFirst = $(this);
        $(this).parent().next().find(".positions-result").html('');
        var searchText = $(this).val();
        $(getFirst).parent().next().find(".positions-result").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');

        if (searchText.length >= 3 && searchText.trim() !== "" && searchText.trim() !== " ") {
            var searchUrl = "/dashboard/main/job-titles/?keyword=" + searchText;
            $(getFirst).parent().next().find(".positions-result").html('');
            $(getFirst).parent().next().closest(".positions-result-container").show();

            $.getJSON(searchUrl, function (data) {
                $.each(JSON.parse(data), function (key, value) {
                    $(getFirst).parent().next().find(".positions-result").append('<a class="media media-single position-item" href="javascript:void(0);" data-item-id="' + value.id + '" data-item-value="' + value.title + '"><span class="title">' + value.title + '</span></a>');
                });
            }).done(function () {
                $(".position-item").click(function () {
                    $(getFirst).val($(this).data("item-value"));
                    $(getFirst).parent().next().find(".positions-result").html('');
                    $(getFirst).parent().next().hide();
                })
            });
        }
        else {
            if (searchText.trim().length > 0 && searchText.trim() !== "") {
                $(getFirst).parent().next().find(".positions-result").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
            }
            else {
                $(getFirst).parent().next().find(".positions-result").html('');
                $(getFirst).parent().next().hide();
            }
        }

    })
}

var allSkills = [];
function skillAddToProfile() {
    $("#skills").unbind("keyup").bind("keydown", function () {
        $("#skill-list").html('');
        $("#skill-container").show();
        var searchText = $(this).val();
        $("#skill-list").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');

        if (searchText.length >= 2 && searchText.trim() !== "" && searchText.trim() !== " ") {
            var searchUrl = "/dashboard/skills/get-skills/?keyword=" + searchText;
            $("#skill-list").html('');
            $.getJSON(searchUrl, function (data) {
                $.each(JSON.parse(data), function (key, value) {

                    if ($.inArray(this.id, allSkills) === -1) {
                        $("#skill-list").append('<a class="media media-single select-item" href="javascript:void(0);" data-item-id="' + this.id + '" data-item-value="' + this.title + '"><span class="title">' + this.title + '</span><span class="badge"><i class="fa fa-plus"></i></span></a>');
                    }
                    else {
                        $("#skill-list").append('<a class="media media-single select-item" href="javascript:void(0);" data-item-id="' + this.id + '" data-item-value="' + this.title + '"><span class="title">' + this.title + '</span><span class="badge"><i class="fa fa-check text-success"></i></span></a>');
                    }

                });
            }).done(function () {
                $(".select-item").unbind("click").on("click",function () {

                    if (allSkills.length > 15 && $.inArray($(this).data("item-id"), allSkills) === -1) {
                        toastMessage(messageStr("skill-message"), messageStr("ok-message"), "", "warning");
                    }
                    else {

                        if ($.inArray($(this).data("item-id"), allSkills) === -1) {
                            allSkills.push($(this).data("item-id"));

                            $("#skill-result").append('<button data-value-id="' + $(this).data("item-id") + '" class="btn btn-label btn-outline-danger remove-skill btn-round mx-2 my-3 fs-12" type="button"><label><i class="ti-close"></i></label>' + $(this).data("item-value") + '</button>');

                            $(this).find(".fa").removeClass("fa-plus").addClass("fa-check").addClass("text-success");
                        }
                        else {
                            allSkills.splice(allSkills.indexOf($(this).data("item-id")), 1);
                            $(this).find(".fa").removeClass("fa-check").addClass("fa-plus").removeClass("text-success");
                            $(document).find("#skill-result").find(".remove-skill").filter('[data-value-id=' + $(this).data("item-id") + ']').remove();

                        }
                    }
                });
            });
        }
        else {
            if (searchText.trim().length > 0 && searchText.trim() !== "") {
                $("#skill-list").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
            }
            else {
                $("#skill-list").html('');
                $("#skill-container").hide();
            }

        }

    });

    $(document).find("#skill-result").on("click", ".remove-skill", function () {
        var id = $(this).data("value-id");
        if ($.inArray($(this).data("value-id"), allSkills) > -1) {
            allSkills.splice(allSkills.indexOf($(this).data("value-id")), 1);
            $(this).remove();

            $.getJSON("/dashboard/profiles/remove-skill/?id=" + id, function (data) {
                switch (data.message_type) {
                    case "success":
                        //message-message_button_label-message_situation
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        getUserPoints();
                        break;
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                }
            });

        }
    });


}

function saveSkills(skills) {

    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".preloader").fadeIn();
            },
            url: '/dashboard/profiles/save-skills/',
            data: {'list': JSON.stringify(skills)},
            success: function (data) {
                $(".preloader").fadeOut();
                switch (data.message_type) {
                    case "success":
                        getUserPoints();
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        removeExperiencesLatestAdded();
                        break;
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                }
            },
            error: function (data) {

            }
        }
    );
}

function generalInformationsSave() {
    var summaryStr = $("#summary").val();
    var headlineStr = $("#headline").val();
    var cityName = $("#city-name-profile").val();
    var countryName = $("#country-name-profile").val();
    var neighborhoodName = $("#neighborhood-name-profile").val();
    var districtName = $("#district-name-profile").val();
    var latPositions = $("#lat-position-profile").val();
    var lngPositions = $("#lng-position-profile").val();
    var positionTitle = $("#position-titles").val();

    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".preloader").fadeIn();
            },
            url: '/dashboard/profiles/general-info/',
            data: {
                'summary': summaryStr,
                'city': cityName,
                'country': countryName,
                'district': districtName,
                'neighborhood': neighborhoodName,
                'lat': latPositions,
                'lng': lngPositions,
                'headline': headlineStr,
                'position-title': positionTitle
            },
            success: function (data) {
                $(".preloader").fadeOut();
                if (data.message_type === "error") {
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                }
                else if (data.message_type === "success") {
                    if (data.message.length > 0) {
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                    }
                    document.location.reload();
                }
                else {
                    toastMessage(data.message, messageStr("ok-message"), "", "warning");
                }
            },
            error: function (data) {

            }
        }
    );
}

function upload_profile_img() {
    $("#file").change(function () {
        var file_data = $('#file').prop('files')[0];
        var form_data = new FormData($('#upload-img').get(0));
        form_data.append('file', file_data);
        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".image-messages").hide();
                $(".image-messages").html("");
                $(".preloader").show();
            },
            url: '/dashboard/profiles/upload-image/',
            method: 'post',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function (data) {
                $(".preloader").fadeOut();
                switch (data.message_type) {
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                    case "success":
                        $(".image-messages").show();
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        $(".avatar-xl").attr("src", data.profile_img);
                        getUserPoints();
                        header_profil_img();
                        break;
                    case "warning":
                        $(".image-messages").show();
                        toastMessage(data.message, messageStr("ok-message"), "", "warning");
                        break;
                }
            }
        });
    });

}

function header_profil_img() {
    $.getJSON("/dashboard/myprofile/get-profile-photo/", function (data) {
        $("#profile-header-img").attr("src", data.profile_photo);
        $("#profile-header-img-mobile").attr("src", data.profile_photo);
    });
}

function getProjectPoints() {
    var project_id = $("#project_id").val();

    $.getJSON("/dashboard/myprojects/get-project-points/?id=" + project_id, function (data) {
        $("#project-points").find(".point-value").html("%" + data.score);
        var pointText = $("#project-points").find(".point-text").text();

        if (data.score <= 26) {

            if ($("#project-points").hasClass("bg-warning")) {
                $("#project-points").removeClass('bg-warning').addClass('bg-danger');
            }
            else if ($("#project-points").hasClass("bg-success")) {
                $("#project-points").removeClass('bg-success').addClass('bg-danger');
            }
            else {
                $("#project-points").addClass('bg-danger');
            }

            if (pointText.indexOf("COMPLETED") > -1) {
                $("#project-points").find(".point-text").html("NOT COMPLETED");
            }
            else if (pointText.indexOf("TAMAMLANDI") > -1) {
                $("#project-points").find(".point-text").html("TAMAMLANMADI");
            }

        }
        else if (data.score > 26 && data.score < 100) {
            if ($("#project-points").hasClass("bg-danger")) {
                $("#project-points").removeClass('bg-danger').addClass('bg-warning');
            }
            else if ($("#project-points").hasClass("bg-success")) {
                $("#project-points").removeClass('bg-success').addClass('bg-warning');
            }
            else {
                $("#project-points").addClass('bg-warning');
            }
            if (pointText.indexOf("COMPLETED") > -1) {
                $("#project-points").find(".point-text").html("NOT COMPLETED");
            }
            else if (pointText.indexOf("TAMAMLANDI") > -1) {
                $("#project-points").find(".point-text").html("TAMAMLANMADI");
            }
        }
        else {
            if ($("#project-points").hasClass("bg-danger")) {
                $("#project-points").removeClass('bg-danger').addClass('bg-success');
            }
            else if ($("#project-points").hasClass("bg-warning")) {

                $("#project-points").removeClass('bg-warning').addClass('bg-success');
            }
            else {
                $("#project-points").addClass('bg-success');

            }

            if (pointText.indexOf("NOT COMPLETED") > -1) {
                $("#project-points").find(".point-text").html("COMPLETED");
            }
            else if (pointText.indexOf("TAMAMLANMADI") > -1) {
                $("#project-points").find(".point-text").html("TAMAMLANDI");
            }
        }
    });
}

//bu kısmı düzelt
function getUserPoints() {
    $.getJSON("/dashboard/profiles/user-points/", function (data) {
        $("#user-points").find(".point-value").html("%" + data.score);
        var pointText = $("#user-points").find(".point-text").text();
        if (data.score <= 26) {
            if ($("#user-points").hasClass("bg-warning")) {
                $("#user-points").removeClass('bg-warning').addClass('bg-danger');
            }
            else if ($("#user-points").hasClass("bg-success")) {
                $("#user-points").removeClass('bg-success').addClass('bg-danger');
            }
            else {
                $("#user-points").addClass('bg-danger');
            }

            if (pointText.indexOf("COMPLETED") > -1) {
                $("#user-points").find(".point-text").html("NOT COMPLETED");
            }
            else if (pointText.indexOf("TAMAMLANDI") > -1) {
                $("#user-points").find(".point-text").html("TAMAMLANMADI");
            }

        }
        else if (data.score > 26 && data.score < 100) {

            if ($("#user-points").hasClass("bg-danger")) {
                $("#user-points").removeClass('bg-danger').addClass('bg-warning');
            }
            else if ($("#user-points").hasClass("bg-success")) {
                $("#user-points").removeClass('bg-success').addClass('bg-warning');
            }
            else {
                $("#user-points").addClass('bg-warning');
            }
            if (pointText.indexOf("COMPLETED") > -1) {
                $("#user-points").find(".point-text").html("NOT COMPLETED");
            }
            else if (pointText.indexOf("TAMAMLANDI") > -1) {
                $("#user-points").find(".point-text").html("TAMAMLANMADI");
            }
        }
        else {
            if ($("#user-points").hasClass("bg-danger")) {
                $("#user-points").removeClass('bg-danger').addClass('bg-success');
            }
            else if ($("#user-points").hasClass("bg-warning")) {
                $("#user-points").removeClass('bg-warning').addClass('bg-success');
            }
            else {
                $("#user-points").addClass('bg-success');
            }

            if (pointText.indexOf("NOT COMPLETED") > -1) {
                $("#user-points").find(".point-text").html("COMPLETED");
            }
            else if (pointText.indexOf("TAMAMLANMADI") > -1) {
                $("#user-points").find(".point-text").html("TAMAMLANDI");
            }

        }
    });

}

var exlist = function experienceSave() {
    jsonArray = [];
    $("#experiences-container").find(".main-div").each(function () {
        item = {};
        if ($(this).find(".company-name").val().trim() !== "" && $(this).find(".company-name").val().trim().length > 0) {
            $(this).find(".company-name").next().closest(".invalid-feedback").removeClass("i-am-your-father").fadeOut();
            item["company-title"] = $(this).find(".company-name").val();
        }
        else {
            $(this).find(".company-name").next().closest(".invalid-feedback").addClass("i-am-your-father").fadeIn();
        }

        if ($(this).find(".position-title").val().trim() !== "" && $(this).find(".position-title").val().trim().length > 0) {
            $(this).find(".position-title").next().closest(".invalid-feedback").removeClass("i-am-your-father").fadeOut();
            item["position"] = $(this).find(".position-title").val();
        }
        else {
            $(this).find(".position-title").next().closest(".invalid-feedback").addClass("i-am-your-father").fadeIn();
        }


        if ($(this).find(".start-year").val().trim() !== "" && $(this).find(".start-year").val().trim().length > 0) {
            $(this).find(".start-year").next().closest(".invalid-feedback").removeClass("i-am-your-father").fadeOut();

            var startYearSplit = $(this).find(".start-year").val().split("/");
            if (startYearSplit.length > 2) {
                item["start-month"] = startYearSplit[0];
                item["start-year"] = startYearSplit[2];
            }
            else {
                item["start-month"] = startYearSplit[0];
                item["start-year"] = startYearSplit[1];
            }


        }
        else {
            $(this).find(".start-year").next().closest(".invalid-feedback").addClass("i-am-your-father").fadeIn();
        }

        if ($(this).find('input[type="checkbox"]').is(':checked')) {
            item["is-current"] = true;
            item["end-month"] = "-";
            item["end-year"] = "-";
        }
        else {
            item["is-current"] = false;
            if ($(this).find(".end-year").val() !== "" && $(this).find(".end-year").val().length > 0) {
                $(this).find(".end-year").next().closest(".invalid-feedback").removeClass("i-am-your-father").fadeOut();
                var endYearSplit = $(this).find(".end-year").val().split("/");
                if (endYearSplit > 2) {
                    item["end-month"] = endYearSplit[0];
                    item["end-year"] = endYearSplit[2];
                }
                else {
                    item["end-month"] = endYearSplit[0];
                    item["end-year"] = endYearSplit[1];
                }


            }
            else {
                $(this).find(".end-year").next().closest(".invalid-feedback").addClass("i-am-your-father").fadeIn();
            }
        }

        if ($(this).find(".invalid-feedback").hasClass("i-am-your-father") === false) {


            jsonArray.push(item);
        }

    });

    return jsonArray;
};

function requestChangePass(emailaddress) {
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $("#main-loading").show();
            },
            url: '/recover-password/',
            data: {'email': emailaddress},
            success: function (data) {
                if (data.message_type === "error") {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="mailto:kadir@leapcrowd.co" class="btn btn-lg btn-round     btn-danger px-6 mt-5" ><i class="fa fa-warning"></i> ' + data.button_label + '</a>');
                }
                else if (data.message_type === "success") {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="javascript:void(0);" id="hide-loading" class="btn btn-lg btn-round     btn-warning px-6 mt-5" ><i class="fa fa-warning"></i> ' + data.button_label + '</a>');

                }
                else {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="' + data.redirect_url + '" class="btn btn-lg btn-round     btn-warning px-6 mt-5" ><i class="fa fa-warning"></i> ' + data.button_label + '</a>');
                }
                $("#hide-loading").click(function () {
                    $("#main-loading").fadeOut(500);
                    $("#main-loading").html('');
                })

            },
            error: function (data) {

            }
        }
    );
}

function getQerystringByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function loginForm(emailaddress, password) {
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $("#main-loading").show();
            },
            url: '/login/',
            data: {'email': emailaddress, 'password': password},
            success: function (data) {
                if (data.message_type === "error") {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="mailto:kadir@leapcrowd.co" class="btn btn-lg btn-round     btn-danger px-6 mt-5" ><i class="fa fa-warning"></i> ' + data.button_label + '</a>');
                }
                else if (data.message_type === "success") {
                    //burda ayrıca profil resmini almalıyız
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    setTimeout(function () {
                        window.location.href = data.redirect_url;
                    }, 2000)
                }
                else {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="' + data.redirect_url + '" class="btn btn-lg btn-round     btn-warning px-6 mt-5" ><i class="fa fa-warning"></i> ' + data.button_label + '</a>');
                }

            },
            error: function (data) {

            }
        }
    );
}

function selectInvestments() {
    var lang = $("#language").val();
    $(document).find("#project-supply").html('');
    $.getJSON("/dashboard/main/select-invest/", function (data) {
        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":
                    $(document).find("#project-supply").append('<option value="' + value.id + '">' + value.title + '</option>');
                    break;
                default:
                    $(document).find("#project-supply").append('<option value="' + value.id + '">' + value.title_eng + '</option>');
                    break;
            }

        })
    }).done(function () {
        $.getJSON("/dashboard/profiles/selected-invest/", function (data) {
            switch (data.message_type) {
                case "success":
                    $(document).find("#project-supply")[0].selectedIndex = data.id;
                    if (data.id === 3) {
                        $("#project-hours-container").fadeIn();
                        $.getJSON("/dashboard/profiles/selected-hourcost/", function (cost_data) {
                            $("#project-hour-cost").val(cost_data.id);
                        })
                    }
                    break;
                default:
                    $(document).find("#project-supply")[0].selectedIndex = data.id;
                    break;
            }
        });
    });
}

function selectProjectHours() {
    var lang = $("#language").val();
    $(document).find("#project-hour").html('');
    $.getJSON("/dashboard/main/select-hours/", function (data) {
        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":
                    $(document).find("#project-hour").append('<option value="' + value.id + '">' + value.title + '</option>');
                    break;
                default:
                    $(document).find("#project-hour").append('<option value="' + value.id + '">' + value.title_eng + '</option>');
                    break;
            }

        })
    }).done(function () {
        $.getJSON("/dashboard/profiles/selected-hours/", function (data) {
            switch (data.message_type) {
                case "success":
                    $(document).find("#project-hour")[0].selectedIndex = data.id;
                    break;
                default:
                    $(document).find("#project-hour")[0].selectedIndex = data.id;
                    break;
            }
        })
    })
}


function setup_project_investments(selected_post, page_type) {
    var selectBoxHtml = '';
    var selectItems = '';
    var lang = $("#language").val();

    $.getJSON("/dashboard/main/select-invest/", function (data) {
        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":
                    selectItems += '<option value="' + value.id + '">' + value.title + '</option>';
                    break;
                default:
                    selectItems += '<option value="' + value.id + '">' + value.title_eng + '</option>';
                    break;
            }

            selectBoxHtml = '<select data-provide="selectpicker" class="my-2 mx-2" name="select-investments" id="select-investments">' + selectItems + '</select>';

        });

        $(document).find("#modal-team-request").find(".set-support").after(selectBoxHtml);

    }).done(function () {

        $("#select-investments").on("change", function () {
            select_investments($(this).val(), selected_post, page_type);
        });

    });


}

function select_investments(selected_value, selected_post, page_type) {
    switch (page_type) {
        case "post-page":
            $.getJSON("/dashboard/ideas/save-support-types/?id=" + selected_value + "&post_id=" + selected_post, function (data) {

                if (data.message_type === "success") {
                    $("#select-investments").parent().remove();
                    $(document).find("#modal-team-request").find(".set-support").show();
                }

            }).done(function (data) {
                if (data.message_type === "success") {
                    $(document).find("#modal-team-request").find(".selected-investment").text(data.investment);

                    $(document).find("#modal-team-request").find(".selected-investment").attr("data-investment", data.data_investment);
                }
                else {
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                }

            });
            break;
        case "project-page":
            $.getJSON("/dashboard/projects/update-investments/?id=" + selected_value + "&project_id=" + selected_post, function (data) {

                if (data.message_type === "success") {
                    $("#select-investments").parent().remove();
                    $(document).find("#modal-team-request").find(".set-support").show();
                }

            }).done(function (data) {
                if (data.message_type === "success") {
                    $(document).find("#modal-team-request").find(".selected-investment").text(data.investment);

                    $(document).find("#modal-team-request").find(".selected-investment").attr("data-investment", data.data_investment);
                }
                else {
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                }

            });

            break;
    }


}

function get_project_supports(post_id) {
    $.getJSON("/dashboard/ideas/project-supports/?post=" + post_id, function (data) {

    }).done(function (data) {
        if (data.message_type === "success") {
            $(document).find("#modal-team-request").find(".selected-investment").text(data.selected_investment);
            $(document).find("#modal-team-request").find(".selected-investment").attr("data-investment", data.data_investments);

            $(document).find("#modal-team-request").find(".selected-hour").text(data.selected_project_hours);
            $(document).find("#modal-team-request").find(".selected-hour").attr("data-hour", data.data_project_hours)
        }
    });
}

function setup_project_hours(selected_post, page_type) {
    var selectBoxHtml = '';
    var selectItems = '';
    var lang = $("#language").val();

    $.getJSON("/dashboard/main/select-hours/", function (data) {

        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":

                    selectItems += '<option value="' + value.id + '">' + value.title + '</option>';
                    break;
                default:

                    selectItems += '<option value="' + value.id + '">' + value.title_eng + '</option>';
                    break;
            }

            selectBoxHtml = '<select data-provide="selectpicker" class="my-2 mx-2" name="select-hours" id="select-hours">' + selectItems + '</select>';

        });

        $(document).find("#modal-team-request").find(".set-hours").after(selectBoxHtml);


    }).done(function (data) {

        $("#select-hours").on("change", function () {
            select_project_hours($(this).val(), selected_post, page_type);
        });

    });

}

function select_project_hours(selected_value, selected_post, page_type) {

    switch (page_type) {
        case "post-page":
            $.getJSON("/dashboard/ideas/save-project-hours/?id=" + selected_value + "&post_id=" + selected_post, function (data) {
                if (data.message_type === "success") {
                    $("#select-hours").parent().remove();
                    $(document).find("#modal-team-request").find(".set-hours").show();
                }


            }).done(function (data) {
                if (data.message_type === "success") {
                    $(document).find("#modal-team-request").find(".selected-hour").text(data.selected_project_hours);
                    $(document).find("#modal-team-request").find(".selected-hour").attr("data-hour", data.data_project_hours);
                }
                else {
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                }
            });
            break;
        case "project-page":
            $.getJSON("/dashboard/projects/update-projecthours/?id=" + selected_value + "&project_id=" + selected_post, function (data) {
                if (data.message_type === "success") {
                    $("#select-hours").parent().remove();
                    $(document).find("#modal-team-request").find(".set-hours").show();
                }
            }).done(function (data) {
                if (data.message_type === "success") {
                    $(document).find("#modal-team-request").find(".selected-hour").text(data.selected_project_hours);
                    $(document).find("#modal-team-request").find(".selected-hour").attr("data-hour", data.data_project_hours);
                }
                else {
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                }
            });
            break;
    }

}

function selectStartupHistory() {
    var lang = $("#language").val();
    $(document).find("#startup-history").html('');
    $.getJSON("/dashboard/main/select-startuphistory/", function (data) {
        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":
                    $(document).find("#startup-history").append('<option value="' + value.id + '">' + value.title + '</option>');
                    break;
                default:
                    $(document).find("#startup-history").append('<option value="' + value.id + '">' + value.title_eng + '</option>');
                    break;
            }

        })
    }).done(function () {
        $.getJSON("/dashboard/profiles/selected-history/", function (data) {
            switch (data.message_type) {
                case "success":
                    $(document).find("#startup-history")[0].selectedIndex = data.id;
                    break;
                case "warning":
                    $(document).find("#startup-history")[0].selectedIndex = data.id;
                    break;
            }

        })
    })
}

function saveBasics(supportType, hour_cost, project_hour, startup_history) {
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".preloader").show();
            },
            url: '/dashboard/profiles/save-basics/',
            data: {
                'support_type': supportType,
                'project_hour': project_hour,
                'hour_cost': hour_cost,
                'startup_history': startup_history
            },
            success: function (data) {
                $(".preloader").hide();
                switch (data.message_type) {
                    case "success":
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        break;
                    case "warning":
                        toastMessage(data.message, messageStr("ok-message"), "", "warning");
                        break;
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                    default:
                        break;
                }
            },
            error: function (data) {

            }
        }
    );
}

function supportType() {
    $("#project-supply").change(function () {
        if ($(this)[0].selectedIndex === 3) {
            $("#project-hours-container").fadeIn();
        }
        else {
            $("#project-hours-container").fadeOut();
        }
    });
}

function get_user_skills() {
    $("#profile-skills").fadeIn();
    $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('');
    $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
    var lang_code = $("#language").val();
    $.getJSON("/dashboard/profiles/get-skills/?id=" + id_return(), function (data) {
        if (data.length > 0) {
            $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('');
            $.each(data, function (index, value) {
                $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").append('<span class="badge label label-info">' + value.title + '</span>');
            });
        }
        else {
            $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('');
            if (lang_code === "tr") {
                $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('<p class="text-center">Henüz bir beceri girilmemiş.</p>');
            }
            else {
                $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('<p class="text-center">There is a no skill yet.</p>');
            }
        }
    })
}

function get_profile_investments() {
    var lang_code = $("#language").val();
    $.getJSON("/dashboard/myprofile/get-investments/", function (data) {
        $("#invest-type").fadeIn();
        $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div></li>');
        $("#invest-type").find(".card-body").find("ul").html('');

        switch (data.message_type) {
            case "success":
                if (lang_code === "tr") {
                    $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.project_hours + '</div><small>Saat/Hafta</small></li>');
                    if (data.hour_cost.length > 0) {
                        $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.hour_cost + '</div><small>Saatlik Ücret</small></li>');
                        $("#invest-type").find(".card-body").find("ul").append('<li><div class="fs-18">' + data.invest_type + '</div><small>Yatırım Türü</small></li>');
                    }
                    else {
                        $("#invest-type").find(".card-body").find("ul").append('<li ><div class="fs-18">' + data.invest_type + '</div><small>Yatırım Türü</small></li>');
                    }
                }
                else {
                    $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.project_hours + '</div><small>Hour/Week</small></li>');
                    if (data.hour_cost.length > 0) {
                        $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.project_hours + '</div><small>Hourly Rate</small></li>');
                        $("#invest-type").find(".card-body").find("ul").append('<li><div class="fs-18">' + data.invest_type + '</div><small>Invest Type</small></li>');
                    }
                    else {
                        $("#invest-type").find(".card-body").find("ul").append('<li ><div class="fs-18">' + data.invest_type + '</div><small>Invest Type</small></li>');
                    }
                }
                break;
            case "warning":
                $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><p>' + data.message + '</p></li>');
                break;
        }


    })
}

function get_user_level() {
    $.getJSON("/dashboard/profiles/user-points/", function (data) {
        $("#profile-level").find(".progress-bar").attr("aria-valuenow", data.score);
        $("#profile-level").find(".progress-bar").css({"width": data.score + "%"});
        $("#profile-level").find(".progress-bar").find(".level-text").html(data.score);
        if (data.score <= 26) {
            if ($("#profile-level").find(".progress-bar").hasClass("bg-warning")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-warning").addClass("bg-danger")
            }
            else if ($("#profile-level").find(".progress-bar").hasClass("bg-success")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-success").addClass("bg-danger")
            }
        }
        else if (data.score > 26 && data.score < 100) {
            if ($("#profile-level").find(".progress-bar").hasClass("bg-danger")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-danger").addClass("bg-warning")
            }
            else if ($("#profile-level").find(".progress-bar").hasClass("bg-success")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-success").addClass("bg-warning")
            }
        }
        else {
            if ($("#profile-level").find(".progress-bar").hasClass("bg-danger")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-danger").addClass("bg-success")
            }
            else if ($("#profile-level").find(".progress-bar").hasClass("bg-warning")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-warning").addClass("bg-success")
            }
        }
    });
}

function get_profile_experiences() {
    $.getJSON("/dashboard/myprofile/get-experiences/", function (data) {
        if (data.length > 0) {
            $("#experiences-header").fadeIn();
            $("#experiences-list").css({"display": "flex"});
            $("#experiences-list").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
            $("#experiences-list").html('');
            $.each(data, function (index, value) {
                switch (value.message_type) {
                    case "success":
                        $("#experiences-list").append('<div class="col-md-6 col-12 col-sm-12">' +
                            '<a href="javascript:void(0);" class="card card-body card-shadowed text-gray" title="' + value.name + '">' +
                            '<h6 class="text-uppercase text-gray fs-13">' + orj_str(value.name, 36) + '</h6>' +
                            '<div class="flexbox mt-2">' +
                            '<span class="fs-13 pt-2">' + value.title + '</span>' +
                            '<span class="fa fa-building-o text-secondary fs-35"></span>' +
                            '</div>' +
                            '<div class="text-left">' +
                            value.start_date + " - " + value.end_date +
                            '</div>' +
                            '</a>' +
                            '</div>');
                        break;
                    case "warning":
                        $("#experiences-list").append('<div class="col-md-12 col-12 col-sm-12"><p class="text-center">' + value.message + '</p></div>');
                        break;
                }

            })
        }
    });
}

function change_cover_photo() {
    $("#upload-cover-file").change(function () {
        var file_data = $('#upload-cover-file').prop('files')[0];
        var form_data = new FormData($('#upload-cover').get(0));
        form_data.append('file', file_data);
        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }

                $(".preloader").show();
            },
            url: '/dashboard/myprofile/upload-cover/',
            method: 'post',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function (data) {
                $(".preloader").fadeOut();
                switch (data.message_type) {
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                    case "success":
                        $("#bg-img").css({
                            'background-image': 'url(' + data.cover_img + ')'
                        });
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        break;
                    case "warning":
                        toastMessage(data.message, messageStr("ok-message"), "", "warning");
                        break;
                }
            }
        });
    });
}

function logout_btn() {
    $.getJSON("/dashboard/profiles/logout/", function (data) {
        switch (data.message_type) {
            case "success":
                window.location.href = data.redirect_url;
                break;
            case "error":
                toastMessage(data.message, messageStr("ok-message"), "", "danger");
                break;
        }
    })
}

function get_update_count() {
    $.getJSON("/dashboard/updates/update-count/", function (data) {
        $.each(data, function (index, value) {
            if (value.update_count > 0) {
                $("#vue-sidebar").find("ul").find(".update-item").append('<span class="badge badge-pill badge-primary">' + value.update_count + '</span>');
            }
            else {
                if ($("#vue-sidebar").find("ul").find(".update-item").find(".badge").length > 0) {
                    $("#vue-sidebar").find("ul").find(".update-item").find(".badge").remove();
                }
            }

        })
    })
}

function get_update_list() {
    $.getJSON("/dashboard/updates/update-list/", function (data) {
            var count_data = data.length;
            var before_id = 0;
            var titleHtml = "";
            var contentHtml = "";
            $.each(data, function (index, value) {
                if (before_id !== value.id) {
                    contentHtml = "";
                    titleHtml = '<h5><strong>' + value.version + '</strong><br><small class="text-capitalize">' + value.date + '</small></h5>';
                    contentHtml = ' - ' + value.title + ' : ' + value.title_content + "<br/>";
                }
                else {
                    contentHtml += ' - ' + value.title + ' : ' + value.title_content + "<br/>";
                }
                before_id = value.id;
            });

            if (count_data > 1) {
                $("#update-list").append('<div>' + titleHtml + '<pre>' + contentHtml + '</pre><hr/>' + '</div>');
            }
            else {
                $("#update-list").append('<div>' + titleHtml + '<pre>' + contentHtml + '</pre>' + '</div>');
            }
        }
    ).done(function (data) {

    });
}

function save_project_generalinformations() {
    var summary = "";
    var sector = 0;
    var websiteUrl = $("#project-website").val();
    var project_id = 0;
    var project_name = "";
    var error = true;

    if ($("#project_id").val().trim() !== 0 || $("#project_id").val().trim() !== "") {
        project_id = $("#project_id").val().trim();
    }

    if ($("#project-name").val().trim().length > 0) {
        project_name = $("#project-name").val().trim();
        error = false;
        $("#project-name").next().closest(".invalid-feedback").fadeOut();

        if ($("#project-summary").val().trim() !== "" && error === false) {
            summary = $("#project-summary").val().trim();
            error = false;
            $("#project-summary").next().closest(".invalid-feedback").fadeOut();

            if ($("#project-sector")[0].selectedIndex !== 0 && error === false) {
                sector = $("#project-sector").val();
                error = false;
                $("#project-sector").next().closest(".invalid-feedback").fadeOut();

                if (usageTypes.length > 1 && error === false) {
                    error = false;
                    $("#usage-types").next().closest(".invalid-feedback").fadeOut();
                }
                else {
                    $("#usage-types").next().closest(".invalid-feedback").fadeIn();
                    error = true;
                }
            }
            else {
                $("#project-sector").next().closest(".invalid-feedback").fadeIn();
                error = true;
            }
        }
        else {
            $("#project-summary").next().closest(".invalid-feedback").fadeIn();
            error = true;
        }
    }
    else {
        $("#project-name").next().closest(".invalid-feedback").fadeIn();
        error = true;
    }


    if (error !== true) {
        $(".invalid-feedback").fadeOut();
        $.ajax({
                type: 'POST',
                beforeSend: function (xhr, settings) {
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        // Only send the token to relative URLs i.e. locally.
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                    $(".preloader").fadeIn();
                },
                url: '/dashboard/myprojects/general-infos/',
                data: {
                    'summary': summary,
                    'sector': sector,
                    'usage_types': JSON.stringify(usageTypes),
                    'website': websiteUrl,
                    'project_id': project_id,
                    'title': project_name
                },
                success: function (data) {
                    $(".preloader").fadeOut();
                    switch (data.message_type) {
                        case "error":
                            toastMessage(data.message, messageStr("ok-message"), "", "danger");
                            break;
                        case "success":
                            toastMessage(data.message, messageStr("ok-message"), "", "success");
                            $("#project_id").val(data.project_id);
                            getProjectPoints();
                            break;
                        case "warning":
                            toastMessage(data.message, messageStr("ok-message"), "", "warning");
                            break;
                        default:
                            break;
                    }
                },
                error: function (data) {

                }
            }
        );
    }


}

function save_project_businessModel() {
    var target_group = "";
    var revenue_model = "";
    var project_id = 0;
    var error = true;

    if ($("#project_id").val().trim() !== 0 || $("#project_id").val().trim() !== "") {
        project_id = $("#project_id").val().trim();
    }

    if ($("#target-group").val().trim() !== "") {
        target_group = $("#target-group").val().trim();
        error = false;
        $("#target-group").next().closest(".invalid-feedback").fadeOut();
    }
    else {
        $("#target-group").next().closest(".invalid-feedback").fadeIn();
        error = true;
    }

    if ($("#revenue-model").val().trim() !== "") {
        revenue_model = $("#revenue-model").val().trim();
        error = false;
        $("#revenue-model").next().closest(".invalid-feedback").fadeOut();
    }
    else {
        $("#revenue-model").next().closest(".invalid-feedback").fadeIn();
        error = true;
    }

    if (error !== true) {
        $(".invalid-feedback").fadeOut();
        $.ajax({
                type: 'POST',
                beforeSend: function (xhr, settings) {
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        // Only send the token to relative URLs i.e. locally.
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                    $(".preloader").fadeIn();
                },
                url: '/dashboard/myprojects/business-model/',
                data: {
                    'target-group': target_group,
                    'revenue-model': revenue_model,
                    'project_id': project_id
                },
                success: function (data) {
                    $(".preloader").fadeOut();
                    switch (data.message_type) {
                        case "error":
                            toastMessage(data.message, messageStr("ok-message"), "", "danger");
                            break;
                        case "success":
                            toastMessage(data.message, messageStr("ok-message"), "", "success");
                            $("#project_id").val(data.project_id);
                            getProjectPoints();
                            break;
                        case "warning":
                            toastMessage(data.message, messageStr("ok-message"), "", "warning");
                            break;
                        default:
                            break;
                    }
                },
                error: function (data) {

                }
            }
        );
    }

}

function save_project_requirementsModel() {
    var existing_requirements = "";
    var needed_requirements = "";
    var project_id = 0;
    var error = true;

    if ($("#project_id").val().trim() !== 0 || $("#project_id").val().trim() !== "") {
        project_id = $("#project_id").val().trim();
    }
    if ($("#existing-requirements").val().trim() !== "") {
        existing_requirements = $("#existing-requirements").val().trim();
        error = false;
        $("#existing-requirements").next().next().closest(".invalid-feedback").fadeOut();
    }
    else {
        $("#existing-requirements").next().next().closest(".invalid-feedback").fadeIn();
        error = true;
    }

    if ($("#needed-requirements").val().trim() !== "") {
        needed_requirements = $("#needed-requirements").val().trim();
        error = false;
        $("#needed-requirements").next().next().closest(".invalid-feedback").fadeOut();
    }
    else {
        $("#needed-requirements").next().next().closest(".invalid-feedback").fadeIn();
        error = true;
    }

    if (error !== true) {
        $(".invalid-feedback").fadeOut();
        $.ajax({
                type: 'POST',
                beforeSend: function (xhr, settings) {
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        // Only send the token to relative URLs i.e. locally.
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                    $(".preloader").fadeIn();
                },
                url: '/dashboard/myprojects/requirements/',
                data: {
                    'existing-requirements': existing_requirements,
                    'needed-requirements': needed_requirements,
                    'project_id': project_id
                },
                success: function (data) {
                    $(".preloader").fadeOut();
                    switch (data.message_type) {
                        case "error":
                            toastMessage(data.message, messageStr("ok-message"), "", "danger");
                            break;
                        case "success":
                            toastMessage(data.message, messageStr("ok-message"), "", "success");
                            $("#project_id").val(data.project_id);
                            getProjectPoints();
                            break;
                        case "warning":
                            toastMessage(data.message, messageStr("ok-message"), "", "warning");
                            break;
                        default:
                            break;
                    }
                },
                error: function (data) {

                }
            }
        );
    }

}

function save_project_competitorsModel() {
    var competitors = "";
    var project_id = 0;
    var error = true;

    if ($("#project_id").val().trim() !== 0 || $("#project_id").val().trim() !== "") {
        project_id = $("#project_id").val().trim();
    }
    if ($("#competitors").val().trim() !== "") {
        competitors = $("#competitors").val().trim();
        error = false;
        $("#competitors").next().closest(".invalid-feedback").fadeOut();
    }
    else {
        $("#competitors").next().closest(".invalid-feedback").fadeIn();
        error = true;
    }

    if (error !== true) {
        $(".invalid-feedback").fadeOut();
        $.ajax({
                type: 'POST',
                beforeSend: function (xhr, settings) {
                    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                        // Only send the token to relative URLs i.e. locally.
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                    }
                    $(".preloader").fadeIn();
                },
                url: '/dashboard/myprojects/competitors/',
                data: {
                    'competitor': competitors,
                    'project_id': project_id
                },
                success: function (data) {
                    $(".preloader").fadeOut();
                    switch (data.message_type) {
                        case "error":
                            toastMessage(data.message, messageStr("ok-message"), "", "danger");
                            break;
                        case "success":
                            toastMessage(data.message, messageStr("ok-message"), "", "success");
                            $("#project_id").val(data.project_id);
                            getProjectPoints();
                            break;
                        case "warning":
                            toastMessage(data.message, messageStr("ok-message"), "", "warning");
                            break;
                        default:
                            break;
                    }
                },
                error: function (data) {

                }
            }
        );
    }

}

function get_projectAreas() {

    $.getJSON("/dashboard/main/select-project-areas/", function (data) {

        var lang = $("#language").val();
        $(document).find("#project-sector").html('');
        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":
                    $(document).find("#project-sector").append('<option value="' + value.id + '">' + value.title + '</option>');
                    break;
                default:
                    $(document).find("#project-sector").append('<option value="' + value.id + '">' + value.title_eng + '</option>');
                    break;
            }
        })
    }).done(function () {
        var project_id = $("#project_id").val();

        $.getJSON("/dashboard/myprojects/selected-project-areas/?id=" + project_id, function (data) {
            $(document).find("#project-sector")[0].selectedIndex = data.id;
        });
    });
    //bu bölümün done kısmı olacak
}

var usageTypes = [];
function get_projectUsageTypes() {
    $.getJSON("/dashboard/main/select-project-types/", function (data) {
        var lang = $("#language").val();
        $(document).find("#usage-types").html('');
        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":
                    $("#usage-types").append('<label class="custom-control custom-checkbox project-usage-type">' +
                        '<input type="checkbox" class="custom-control-input" value="' + value.id + '">' +
                        '<span class="custom-control-indicator"></span>' +
                        '<span class="custom-control-description">' + value.title + '</span>' +
                        '</label>');
                    break;
                default:
                    $("#usage-types").append('<label class="custom-control custom-checkbox project-usage-type">' +
                        '<input type="checkbox" class="custom-control-input" value="' + value.id + '">' +
                        '<span class="custom-control-indicator"></span>' +
                        '<span class="custom-control-description">' + value.title_eng + '</span>' +
                        '</label>');
                    break;
            }
        })
    }).done(function () {
        $("#usage-types").on("change", '.project-usage-type :checkbox', function () {
            if ($(this).is(":checked")) {
                usageTypes.push($(this).val());
            }
            else {
                usageTypes.splice(usageTypes.indexOf($(this).val()), 1);
            }
        });

        var project_id = $("#project_id").val();
        $.getJSON("/dashboard/myprojects/selected-project-types/?id=" + project_id, function (data) {
            $.each(data, function (index, value) {
                usageTypes.push(value.id);
            });
            usageTypes.forEach(function (element) {
                $("#usage-types").find(".project-usage-type").each(function () {
                    if ($(this).find("input[type='checkbox']").val().indexOf(element) > -1) {
                        $(this).find("input[type='checkbox']").prop("checked", true);
                    }
                });
            });

        });

    });

}

function upload_project_logo() {
    var project_id = id_return();
    // console.log(project_id);
    $("#file-project").unbind("change").change(function () {

        var file_data = $('#file-project').prop('files')[0];
        var form_data = new FormData($('#upload-project-img').get(0));
        form_data.append('file', file_data);
        form_data.append('project_id', project_id);
        console.log(form_data);
        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".preloader").fadeIn();
            },
            url: '/dashboard/myprojects/upload-project-logo/',
            method: 'post',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function (data) {
                $(".preloader").fadeOut();
                switch (data.message_type) {
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                    case "success":
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        $(".project-logo").attr("src", data.project_logo);
                        $("#project_id").val(data.project_id);
                        getProjectPoints();
                        //bu kısımda yüklenen proje logosunu değiştireceksin. +
                        break;
                    case "warning":
                        toastMessage(data.message, messageStr("ok-message"), "", "warning");
                        break;
                }

            }
        });
    });

}

function removeProfileAvatar(){
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".preloader").fadeIn();
            },
            url: '/dashboard/profiles/delete-avatar/',
            data: {
                'id': id_return()
            },
            success: function (data) {
                $(".preloader").fadeOut();
                switch (data.message_type) {
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                    case "success":
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                    $("#upload-img").find(".avatar-xl").attr("src", data.default_avatar);
                        break;
                    default:
                        break;
                }
            },
            error: function (data) {

            }
        }
    );
}

function removeProjectLogo() {

    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".preloader").fadeIn();
            },
            url: '/dashboard/myprojects/delete-project-logo/',
            data: {
                'project_id': id_return()
            },
            success: function (data) {
                $(".preloader").fadeOut();
                switch (data.message_type) {
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;
                    case "success":
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        $(".project-logo").attr("src", data.default_logo);
                        break;
                    default:
                        break;
                }
            },
            error: function (data) {

            }
        }
    );


}

function get_business_stages() {
    var project_id = $("#project_id").val();
    var lang_code = $("#language").val();
    $.getJSON("/dashboard/myprojects/project-business-stage/?project_id=" + project_id, function (data) {
        var inputHtml = "";
        $("#business-stages").html('');
        $.each(data, function (index, value) {
            switch (value.name) {
                case "idea":
                    if (value.situation === "disabled") {
                        if (lang_code === "tr") {
                            inputHtml = '<label class="fs-16 fw-400 mb-4">Proje Fikri</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml = '<label class="fs-16 fw-400 mb-4">Idea</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                    }
                    else {
                        if (lang_code === "tr") {
                            inputHtml = '<label class="fs-16 fw-400 mb-4">Proje Fikri</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" checked disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml = '<label class="fs-16 fw-400 mb-4">Idea</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" checked disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                    }
                    break;
                case "business-model":
                    if (value.situation === "disabled") {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">İş Planı</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Business Plan</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                    }
                    else {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">İş Planı</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" checked disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Business Plan</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" checked disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                    }
                    break;
                case "teammates":
                    if (value.situation === "disabled") {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Takım Arkadaşları</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Teammates</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }

                    }
                    else {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Takım Arkadaşları</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" checked disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Teammates</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" checked disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                    }
                    break;

                case "mvp-product":
                    if (value.situation === "disabled" && value.is_open === false) {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">MVP Ürün</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">MVP Product</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }

                    }
                    else {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">MVP Ürün</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" id="mvp-product" data-color="#465161" />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">MVP Product</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" id="mvp-product" data-color="#465161" />' +
                                '</div>' +
                                '<hr>';
                        }
                    }
                    break;

                case "paying-customers":
                    if (value.situation === "disabled" && value.is_open === false) {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Ödeyen Müşteriler</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Paying Customers</label>' +
                                '<div class="form-group">' +
                                '<input type="checkbox" data-provide="switchery" data-color="#465161" disabled />' +
                                '</div>' +
                                '<hr>';
                        }

                    }
                    else {
                        if (lang_code === "tr") {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Ödeyen Müşteriler</label>' +
                                '<div class="form-group customers">' +
                                '<input type="checkbox" data-provide="switchery" id="paying-customers" data-color="#465161" />' +
                                '</div>' +
                                '<hr>';
                        }
                        else {
                            inputHtml += '<label class="fs-16 fw-400 mb-4">Paying Customers</label>' +
                                '<div class="form-group customers">' +
                                '<input type="checkbox" data-provide="switchery" id="paying-customers" data-color="#465161" />' +
                                '</div>' +
                                '<hr>';
                        }
                    }

                    break;

                default:
                    break;
            }
        });

        $("#business-stages").append(inputHtml);

    }).done(function () {
        //bu kısımda takım arkadaşları - mvp project kaydetme işini yapmalıyız.
        $(document).on("change", '#mvp-product', function () {
            var option_value = $(this).attr("id");
            var project_id = $("#project_id").val();
            active_step(option_value, project_id);

            if ($("#paying-customers").is(':checked')) {

                $("#paying-customers").prop("checked", false);

                $(".customers").find("span.switchery").css({
                    "box-shadow": "rgb(223, 223, 223) 0px 0px 0px 0px inset",
                    "border-color": "rgb(223, 223, 223)",
                    "background-color": "rgb(255, 255, 255)",
                    "transition": "border 0.5s, box-shadow 0.5s"
                });
                $(".customers").find("span.switchery").find("small").css({
                    "left": "0px",
                    "transition": "background-color 0.5s, left 0.25s"
                });
            }

        });

        $(document).on("change", '#paying-customers', function () {
            var option_value = $(this).attr("id");
            var project_id = $("#project_id").val();
            active_step(option_value, project_id);
        });

    })
}

function active_step(valueStr, project_id) {
    $.getJSON("/dashboard/myprojects/active-step/?name=" + valueStr + "&id=" + project_id, function (data) {
        $.each(data, function (index, value) {
            switch (value.message_type) {
                case "success":
                    toastMessage(value.message, messageStr("ok-message"), "", "success");
                    break;
                case "warning":
                    toastMessage(value.message, messageStr("ok-message"), "", "warning");
                    break;
                case "error":
                    toastMessage(value.message, messageStr("ok-message"), "", "danger");
                    break;
            }
        })
    });
}

function set_project_options(value, situation) {
    var project_id = $("#project_id").val();
    $.getJSON("/dashboard/myprojects/change-project-options/?value=" + value + "&project_id=" + project_id + "&situation=" + situation, function (data) {
        switch (data.message_type) {
            case "success":
                toastMessage(data.message, messageStr("ok-message"), "", "success");
                break;
            case "warning":
                toastMessage(data.message, messageStr("ok-message"), "", "warning");
                break;
            case "error":
                toastMessage(data.message, messageStr("ok-message"), "", "danger");
                break;
            default:
                break;
        }
    }).done(function (data) {

        switch (data.message_type) {
            case "success":
                if (data.value_str.indexOf("give-feedback") > -1 || data.value_str.indexOf("send-teamrequest") > -1) {
                    if (data.trigger_this === true) {
                        $("#publish-project").trigger("click");
                    }
                }
                break;
            default:
                break;
        }
    });
}

function get_project_options(value_options) {
    var project_id = $("#project_id").val();
    $.getJSON("/dashboard/myprojects/get-project-options/?value=" + value_options + "&project_id=" + project_id, function (data) {
        $.each(data, function (index, value) {
            if (value_options === value.title_code) {
                $("#" + value_options).find("input[type='checkbox']").attr("data-situation", value.situation);
                $("#" + value_options).find("input[type='checkbox']").prop("checked", value.situation);
            }
        });
    });
}

function project_steps() {
    var project_id = $("#project-steps").attr("data-project-id");
    var counter = 0;
    $.getJSON("/dashboard/projects/project-steps/?project_id=" + project_id, function (data) {
        $.each(data, function (index, value) {
            $("#project-steps").find("li").each(function () {
                if ($(this).hasClass(value.code)) {
                    if (value.situation.indexOf("processing") > -1) {
                        if (counter <= 0) {
                            counter++;
                            $(this).addClass(value.situation);
                        }
                    }
                    else {
                        $(this).addClass(value.situation);
                    }
                }
            });
        });
    });
}

function project_teammates() {
    var htmlStr = "";
    $.getJSON("/dashboard/projects/project-teammates/?project_id=" + id_return(), function (data) {
        if (data.team_mates.length > 0) {
            $.each(data.team_mates, function (index, value) {
                htmlStr += project_person(value.profile_name, value.profile_id, value.profile_image);
            });
            $("#project-persons").html('');
            $("#project-persons").append(htmlStr);
        }
        else {
            $("#project-persons").html('');
            $("#project-persons").append('<p class="result-message">' + messageStr("project-teammates") + '</p>');
        }

    }).done(function (data) {
        $(document).find(".profile-popup").click(function () {
            $("#qv-user-details").find(".quickview-body").scrollTop(0);
            $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
            var id = $(this).attr("data-id");
            $("#qv-user-details").attr("data-user-id", id);
            var modal_body = $("#qv-user-details").find(".quickview-body");
            user_popup(id, modal_body);
        });
    });


}

var position_title_list = function positionTitleSearch(value_title) {
    $.getJSON("/dashboard/profiles/position-title/?title=" + value_title, function (data) {
        $("#position-title-list").find(".media-list").html('');
        if (value_title.trim().length > 2 && value_title.trim() != "" && value_title.trim() != " ") {

            $("#position-title-list").fadeIn();
            $("#position-title-list").find(".positions-result").fadeIn();
            $("#position-title-list").find(".media-list").html('');
            $("#position-title-list").find(".media-list").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
            $("#position-title-list").find(".media-list").html('');


            $.each(JSON.parse(data), function (index, value) {

                $("#position-title-list").find(".media-list").append('<a class="media media-single ptitle-item" href="javascript:void(0);" data-item-id="' + this.id + '" data-item-value="' + this.title + '"><span class="title">' + this.title + '</span></a>');
            });
        }
        else {
            $("#position-title-list").find(".media-list").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
        }

    }).done(function () {

        $(".ptitle-item").click(function () {

            $("#position-titles").val($(this).find(".title").text());
            $("#position-title-list").find(".media-list").html('');
            $("#position-title-list").fadeOut();
        });

    });
};

function user_popup(users_id, body_object) {
    $.getJSON("/dashboard/profiles/users-popup/?id=" + users_id, function (data) {
        $("#qv-user-details").find(".quickview-body").append('<div class="preloader"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');

        $.each(data, function (index, value) {

            switch (index) {
                case "profile_picture":
                    body_object.find(".easypie-data").find("img").attr("src", value);
                    break;
                case "profile_points":
                    $(body_object).find(".profile-points").data("percent", value);
                    $(body_object).find(".profile-points").data('easyPieChart').update(value);
                    break;
                case "profile_name":
                    $(body_object).find(".profile-name").text(value);
                    break;
                case "position_title":
                    $(body_object).find(".position-title").text(value);
                    break;
                case "profile_location":
                    $(body_object).find(".location").text(value);
                    break;
                case "companies":
                    if (value[0].position_title.length > 0) {
                        row = value[0].position_title + '<br/><small>' + value[0].company_name + '</small>';
                        $(body_object).find(".company").html('');
                        $(body_object).find(".company").html(row);
                    }
                    else {
                        row = value[0].company_name;
                        $(body_object).find(".company").html('');
                        $(body_object).find(".company").html(row);
                    }


                    break;
                case "skills":
                    var i = 0;
                    $(body_object).find(".bootstrap-tagsinput").html('');
                    while (i < value.length) {
                        $(body_object).find(".bootstrap-tagsinput").append('<span class="badge label label-info fs-12">' + value[i].title + '</span>');
                        i++;
                    }
                    if (value.length > 5) {
                        $(body_object).find(".bootstrap-tagsinput").append('<span class="badge label label-info fs-12">...</span>');
                    }

                    break;
                case "invest_type":
                    $(body_object).find(".invest-type").text(value);
                    break;
                case "hour_cost":
                    if (value.length > 0) {
                        $(body_object).find(".hour-costs").show();
                        $(body_object).find(".hour-costs").find("p").text(value);
                    }
                    else {
                        $(body_object).find(".hour-costs").hide();
                    }
                    break;
                case "startup_history":
                    $(body_object).find(".startup-history").text(value);
                    break;
                case "projects":
                    if (value[0].points !== "") {
                        $(body_object).find(".project-title").text(value[0].title);
                        $(body_object).find(".project-title").attr("href", value[0].project_url);
                        $(body_object).find(".project-point").attr("aria-valuenow", value[0].points);
                        $(body_object).find(".project-point").addClass(point_css(value[0].points));
                        $(body_object).find(".project-point").css({
                            "width": value[0].points + "%"
                        });
                        $(body_object).find(".project-point").find("strong").text(value[0].points);
                        $(body_object).find(".project-progress").show();
                    }
                    else {
                        $(body_object).find(".project-title").text(value[0].title);
                        $(body_object).find(".project-progress").hide();
                    }
                    break;
                case "profile_cover":
                    $(body_object).find(".bg-img").css({
                        "background-image": 'url(' + value + ')'
                    });
                    break;
                case "membership_type":
                    if (value[0].message_type === "success") {
                        $("#qv-user-details").find(".member-detail").attr("href", value[0].url);
                    }
                    else {
                        $("#qv-user-details").find(".member-detail").attr("href", "");
                    }
                    break;
                default:
                    break;
            }
        });

    }).done(function (data) {

        $("#qv-user-details").find(".quickview-body").find(".preloader").remove();
        if (data["membership_type"][0].message_type === "warning") {
            $(document).find("#core-js").attr("data-provide", "sweetalert");
            $(document).find(".member-detail").on("click", function () {
                sweet_message(data["membership_type"][0].message);
            });
        }
        else {
            $(document).find("#core-js").data("provide", "");
            $(document).find(".member-detail").click(function () {
                window.location.href = data["membership_type"][0].url;
            });
        }

        // $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");

    });

}

function get_profile_level() {
    $.getJSON("/dashboard/profiles/profile-points/?user_id=" + id_return(), function (data) {
        $("#profile-level").find(".progress-bar").attr("aria-valuenow", data.score);
        $("#profile-level").find(".progress-bar").css({"width": data.score + "%"});
        $("#profile-level").find(".progress-bar").find(".level-text").html(data.score);
        if (data.score <= 26) {
            if ($("#profile-level").find(".progress-bar").hasClass("bg-warning")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-warning").addClass("bg-danger")
            }
            else if ($("#profile-level").find(".progress-bar").hasClass("bg-success")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-success").addClass("bg-danger")
            }
        }
        else if (data.score > 26 && data.score < 100) {
            if ($("#profile-level").find(".progress-bar").hasClass("bg-danger")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-danger").addClass("bg-warning")
            }
            else if ($("#profile-level").find(".progress-bar").hasClass("bg-success")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-success").addClass("bg-warning")
            }
        }
        else {
            if ($("#profile-level").find(".progress-bar").hasClass("bg-danger")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-danger").addClass("bg-success")
            }
            else if ($("#profile-level").find(".progress-bar").hasClass("bg-warning")) {
                $("#profile-level").find(".progress-bar").removeClass("bg-warning").addClass("bg-success")
            }
        }
    });
}

function profile_experiences() {
    $.getJSON("/dashboard/profiles/profile-experiences/?user_id=" + id_return(), function (data) {

        if (data.length > 0) {
            $("#profile-experiences-header").fadeIn();
            $("#profile-experiences-list").css({"display": "flex"});
            $("#profile-experiences-list").append('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
            $("#profile-experiences-list").html('');
            $.each(data, function (index, value) {
                switch (value.message_type) {
                    case "success":
                        $("#profile-experiences-list").append('<div class="col-md-6 col-12 col-sm-12">' +
                            '<a href="javascript:void(0);" class="card card-body card-shadowed text-gray" title="' + value.name + '">' +
                            '<h6 class="text-uppercase text-gray fs-13">' + orj_str(value.name, 36) + '</h6>' +
                            '<div class="flexbox mt-2">' +
                            '<span class="fs-13 pt-2">' + value.title + '</span>' +
                            '<span class="fa fa-building-o text-secondary fs-35"></span>' +
                            '</div>' +
                            '<div class="text-left">' +
                            value.start_date + " - " + value.end_date +
                            '</div>' +
                            '</a>' +
                            '</div>');
                        break;
                    case "warning":
                        $("#profile-experiences-list").html('');
                        $("#profile-experiences-header").hide();

                        break;
                }

            })
        }
    });
}

function profile_investments() {
    var lang_code = $("#language").val();

    $.getJSON("/dashboard/profiles/profile-investments/?user_id=" + id_return(), function (data) {
        $("#invest-type").fadeIn();
        $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div></li>');
        $("#invest-type").find(".card-body").find("ul").html('');

        switch (data.message_type) {
            case "success":
                if (lang_code === "tr") {
                    $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.project_hours + '</div><small>Saat/Hafta</small></li>');
                    if (data.hour_cost.length > 0) {
                        $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.hour_cost + '</div><small>Saatlik Ücret</small></li>');
                        $("#invest-type").find(".card-body").find("ul").append('<li><div class="fs-18">' + data.invest_type + '</div><small>Yatırım Türü</small></li>');
                    }
                    else {
                        $("#invest-type").find(".card-body").find("ul").append('<li ><div class="fs-18">' + data.invest_type + '</div><small>Yatırım Türü</small></li>');
                    }
                }
                else {
                    $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.project_hours + '</div><small>Hour/Week</small></li>');
                    if (data.hour_cost.length > 0) {
                        $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><div class="fs-18">' + data.project_hours + '</div><small>Hourly Rate</small></li>');
                        $("#invest-type").find(".card-body").find("ul").append('<li><div class="fs-18">' + data.invest_type + '</div><small>Invest Type</small></li>');
                    }
                    else {
                        $("#invest-type").find(".card-body").find("ul").append('<li ><div class="fs-18">' + data.invest_type + '</div><small>Invest Type</small></li>');
                    }
                }
                break;
            case "warning":
                $("#invest-type").find(".card-body").find("ul").append('<li class="br-1 botder-light"><p>' + data.message + '</p></li>');
                break;
        }


    })
}

function get_profile_skills() {
    $("#profile-skills").fadeIn();
    $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('');
    $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('<div class="card-loading reveal"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');
    var lang_code = $("#language").val();


    $.getJSON("/dashboard/profiles/get-skills/?id=" + id_return(), function (data) {
        if (data.length > 0) {
            $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('');
            $.each(data, function (index, value) {
                $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").append('<span class="badge label label-info">' + value.title + '</span>');
            });
        }
        else {
            $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('');
            if (lang_code === "tr") {
                $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('<p class="text-center">Henüz bir beceri girilmemiş.</p>');
            }
            else {
                $("#profile-skills").find(".card-body").find(".bootstrap-tagsinput").html('<p class="text-center">There is a no skill yet.</p>');
            }
        }
    })
}

function share_quick_idea() {
    //#quick-idea
    var ideaTxt = $("#quick-idea").val().trim();
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".preloader").fadeIn();
            },
            url: '/dashboard/quick-idea/',
            data: {
                'idea-text': ideaTxt
            },
            success: function (data) {
                $(".preloader").fadeOut();
                switch (data.message_type) {
                    case "success":
                        toastMessage(data.message, messageStr("ok-message"), "", "success");
                        $("#quick-idea").val('');
                        break;
                    case "error":
                        toastMessage(data.message, messageStr("ok-message"), "", "danger");
                        break;

                    case "warning":
                        toastMessage(data.message, messageStr("ok-message"), "", "warning");
                        break;
                    default:
                        break;
                }
            },
            error: function (data) {
                $("#quick-idea").val('');
                $(".preloader").fadeOut();
            }
        }
    );
}

function filter_ideas_list(city, country, unanswered, all) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $("#users-ideas-list").html('');
            $("#load-more-ideas").find(".result-message").remove();
            $("#load-more-ideas").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/ideas/filter-ideas/',
        method: 'post',
        data: {
            "city": city,
            "country": country,
            "unanswered": unanswered,
            "all": all
        },
        success: function (data) {

            if (data.posts.length > 0) {
                $("#load-more-ideas").find("button").show();
                $.each(data.posts, function (index, value) {
                    $("#users-ideas-list").append(ideas_list_item(value.post_id, value.profile_picture, value.profile_username, value.created_date, value.position_title, value.post_content, value.like_count, value.teamrequest_count, value.smilarproject_count, value.answer_count, value.profile_id));
                });
            }
            else {
                $("#users-ideas-list").append('<p class="result-message text-center mx-auto">' + messageStr('there-is-no-item') + '</p>');
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                $("#load-more-ideas").find(".spinner-circle-shadow").remove();
                var data_json = $.parseJSON(jqXHR.responseText);

                if (data_json.has_next) {
                    $("#load-more-ideas").find("button").attr("data-page", data_json.page);
                }
                else {
                    $("#load-more-ideas").find("button").before('<p class="text-center result-message">' + messageStr('list-result-message') + '</p>');
                    $("#load-more-ideas").find("button").hide();
                }

                $(document).find(".post-item").on("click", function () {
                    var post = $(this).attr("data-post");
                    ideas_popup(post);
                    get_post_answers(post, "popup-page");
                });

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });
            }
        }
    });
}

function ideas_popup(u_post_id) {

    $.getJSON("/dashboard/ideas/get-post/?id=" + u_post_id, function (data) {
        $("#qv-post-detail").find(".quickview-body").scrollTop(0);
        $("#qv-post-detail").find(".quickview-body").perfectScrollbar("update");

        $("#qv-post-detail").find(".quickview-body").append('<div class="preloader"><svg class="spinner-circle-material-svg" viewBox="0 0 50 50"><circle class="circle" cx="25" cy="25" r="20"></circle></svg></div>');


    }).done(function (data) {
        $("#qv-post-detail").find(".quickview-body").find(".preloader").remove();
        $(document).find("#qv-post-detail").find("#post-detail").html('');
        $(document).find("#qv-post-detail").find("#post-detail").attr("data-post", data.post_id);
        $(document).find("#qv-post-detail").find("#post-detail").attr("data-name", data.profile_username);
        $(document).find("#qv-post-detail").find("#post-detail").append(post_contents(data.profile_image, data.profile_username, data.created_date, data.profile_position_title, data.post_content, data.like_count, data.answer_count, data.is_edit));
        $(document).find("#qv-post-detail").find("#post-smilarprojects").find(".counter-text").text(data.smilarproject_count);
        $(document).find("#qv-post-detail").find("#post-teamrequests").find(".counter-text").text(data.teamrequest_count);

        mainAndSubAnswerBtnContainer();

        //karakter sınırlaması

    });

}

function write_answerToMainPost(post_id, answer_text, page_type) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            // $("#load-more-ideas").find(".spinner-circle").fadeIn();
        },
        url: '/dashboard/ideas/write-answer/',
        method: 'post',
        data: {
            "post": post_id,
            "answer": answer_text
        },
        success: function (data) {

            switch (data.message_type) {
                case "success":
                    if (page_type.indexOf("popup-page") > -1) {
                        if ($("#post-detail").find("#post-answers").length > 0) {
                            $("#post-detail").find("#post-answers").find(".media").first().before(post_main_answers(data.user_answer.profile_image, data.user_answer.profile_username, data.user_answer.created_date, data.user_answer.position_title, data.user_answer.answer_content, data.user_answer.like_count, data.user_answer.answer_count, data.user_answer.users_answers_id, ""));
                        }
                        else {
                            $("#post-detail").find(".card-body").after(main_answer_container(post_main_answers(data.user_answer.profile_image, data.user_answer.profile_username, data.user_answer.created_date, data.user_answer.position_title, data.user_answer.answer_content, data.user_answer.like_count, data.user_answer.answer_count, data.user_answer.users_answers_id, "")));
                        }
                    }
                    else {
                        if ($("#post-detail-page").find("#post-answers-detail").length > 0) {
                            $("#post-detail-page").find("#post-answers-detail").find(".media").first().before(post_main_answers(data.user_answer.profile_image, data.user_answer.profile_username, data.user_answer.created_date, data.user_answer.position_title, data.user_answer.answer_content, data.user_answer.like_count, data.user_answer.answer_count, data.user_answer.users_answers_id, ""));
                        }
                        else {
                            $("#post-detail-page").find(".card-body").after(main_answer_container(post_main_answers(data.user_answer.profile_image, data.user_answer.profile_username, data.user_answer.created_date, data.user_answer.position_title, data.user_answer.answer_content, data.user_answer.like_count, data.user_answer.answer_count, data.user_answer.users_answers_id, "")));
                        }
                    }
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
                case "warning":
                    toastMessage(data_json.message, messageStr("ok-message"), "", "warning");
                    break;
                default:
                    break;
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                mainAndSubAnswerBtnContainer();

            }

        }
    });
}

//bu kısım bütün cevapları almak için
function get_post_answers(post_id, page_type) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: '/dashboard/ideas/post-answers/',
        method: 'post',
        data: {
            "post": post_id
        },
        success: function (data) {
            switch (data.message_type) {
                case "success":
                    var sub_htmlStr = "";
                    var main_htmlStr = "";
                    var m_htmlStr = "";

                    $.each(data.main_answers, function (index, value) {


                        if (value.sub_answers.length > 0) {

                            $.each(value.sub_answers, function (index_sub, value_sub) {


                                sub_htmlStr += post_sub_answers(value_sub.profile_image, value_sub.profile_name, value_sub.created_date, value_sub.position_title, value_sub.answer_content, value_sub.like_count, value_sub.answers_count, value_sub.sub_answer_id, value_sub.profile_id, page_type, value_sub.is_edit, value_sub.ans_ans_id);
                            });

                            main_htmlStr += post_main_answers(value.profile_image, value.profile_name, value.created_date, value.position_title, value.answer_content, value.like_count, value.answers_count, value.answer_id, sub_htmlStr, value.profile_id, page_type, value.is_edit);
                            m_htmlStr = main_answer_container(main_htmlStr);
                            sub_htmlStr = "";
                        }
                        else {
                            main_htmlStr += post_main_answers(value.profile_image, value.profile_name, value.created_date, value.position_title, value.answer_content, value.like_count, value.answers_count, value.answer_id, sub_htmlStr, value.profile_id, page_type, value.is_edit);
                            m_htmlStr = main_answer_container(main_htmlStr);
                        }
                    });

                    if (page_type.indexOf("detail-page") > -1) {
                        $("#post-detail-page").append(m_htmlStr);
                    }
                    else {
                        $("#post-detail").append(m_htmlStr);
                    }


                    break;
                case "warning":

                    break;
                case "error":

                    break;
                default:
                    break;
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                var load_more_btn_html = "";
                var load_more_main_btn_html = "";

                $.each(data_json.main_answers, function (index, value) {

                    if (value.sub_has_next === true) {
                        //alt cevaplar için
                        load_more_btn_html = '<div class="media">' +
                            '<div class="media-body text-left">' +
                            '<p class="text-lighter"><button class="fs-13 fs-13 btn btn-pure btn-secondary text-light fw-400 load-more-subanswers" data-page="2"><i class="fa fa-mail-forward mr-1 text-light"></i> ' + button_text("more-load") + '</button></p></div></div>';

                    }

                });

                if (data_json.has_next === true) {
                    load_more_main_btn_html = '<div class="media">' +
                        '<div class="media-body text-center">' +
                        '<p class="text-lighter"><button class="fs-13 fs-13 btn btn-pure btn-secondary text-light fw-400 load-more-mainanswers" data-page="2"><i class="fa fa-mail-forward mr-1 text-light"></i> ' + button_text("more-load") + '</button></p></div></div>';

                }


                if (page_type.indexOf("detail-page") > -1) {
                    $("#post-answers-detail").find(".media-body").find(".media:last-child").after(load_more_btn_html);
                    $("#post-answers-detail").find(".main-answers:last-child").after(load_more_main_btn_html);


                    $("#post-answers-detail").find(".load-more-subanswers").on("click", function () {
                        var answer_id = $(this).closest(".main-answers").attr("data-answers");
                        var page = $(this).attr("data-page");
                        more_load_sub_answers(answer_id, page, "detail-page");
                    });

                    $("#post-answers-detail").find(".load-more-mainanswers").on("click", function () {
                        var post_id = $("#post-detail-page").attr("data-post");
                        var page = $(this).attr("data-page");
                        more_load_main_answers(post_id, page, "detail-page");
                    });
                }
                else {

                    $("#post-answers").find(".media-body").find(".media:last-child").after(load_more_btn_html);
                    $("#post-answers").find(".main-answers:last-child").after(load_more_main_btn_html);


                    $("#post-answers").find(".load-more-subanswers").on("click", function () {
                        var answer_id = $(this).closest(".main-answers").attr("data-answers");
                        var page = $(this).attr("data-page");
                        more_load_sub_answers(answer_id, page, "popup-page");
                    });

                    $("#post-answers").find(".load-more-mainanswers").on("click", function () {
                        var post_id = $("#post-detail").attr("data-post");
                        var page = $(this).attr("data-page");
                        more_load_main_answers(post_id, page, "popup-page");
                    });

                    mainAndSubAnswerBtnContainer();

                }


            }

        }
    });
}

function write_answerToAnswer(answer_id, answer_content, page_type) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            // $("#load-more-ideas").find(".spinner-circle").fadeIn();
        },
        url: '/dashboard/ideas/write-answer-to/',
        method: 'post',
        data: {
            "answer": answer_id,
            "answer-content": answer_content
        },
        success: function (data) {

            switch (data.message_type) {
                case "success":
                    var sub_html_str = post_sub_answers(data.user_answer.profile_image, data.user_answer.profile_username, data.user_answer.created_date, data.user_answer.position_title, data.user_answer.answer_content, data.user_answer.like_count, data.user_answer.answer_count, data.user_answer.users_answers_id);

                    if (page_type.indexOf("detail-page") > -1) {

                        $("#post-answers-detail").find(".media").each(function () {
                            if ($(this).data("answers") === data.user_answer.selected_answer_id) {
                                $(this).find(".post-buttons").first().find(".sub-answer-btn").html('<i class="fa fa-comments-o pr-1"></i>' + data.user_answer.selected_answer_count);
                                $(this).find(".post-buttons").first().after(sub_html_str);
                                return false;
                            }
                        });

                        if ($("#post-answers-detail").find(".media-body").find(".media").length > 4) {

                            $(document).find("#post-answers-detail").find(".load-more-subanswers").on("click", function () {
                                var answer_id = $("#post-answers").find(".media-body").find(".media").first().attr("data-answers");
                                var page = $(this).attr("data-page");

                                more_load_sub_answers(answer_id, page, page_type);
                            });
                        }


                    }
                    else {
                        $("#post-answers").find(".media").each(function () {
                            if ($(this).data("answers") === data.user_answer.selected_answer_id) {

                                $(this).find(".post-buttons").first().find(".sub-answer-btn").html('<i class="fa fa-comments-o pr-1"></i>' + data.user_answer.selected_answer_count);
                                $(this).find(".post-buttons").first().after(sub_html_str);
                                return false;
                            }
                        });

                        if ($("#post-answers").find(".media-body").find(".media").length > 4) {

                            $(document).find("#post-answers").find(".load-more-subanswers").on("click", function () {
                                var answer_id = $("#post-answers").find(".media-body").find(".media").first().attr("data-answers");
                                var page = $(this).attr("data-page");

                                more_load_sub_answers(answer_id, page, page_type);
                            });
                        }
                    }


                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
                default:
                    break;
            }
        },
        complete: function (jqXHR, status) {


        }
    });

}

function mainAndSubAnswerBtnContainer() {

    if ($(document).find("#qv-post-detail").find("#post-detail").find(".main-answer-btn").length > 0) {
        $(document).find("#qv-post-detail").find("#post-detail").find(".main-answer-btn").click(function () {
            var p_user = $(document).find("#qv-post-detail").find("#post-detail").attr("data-name");
            // $("#answer-content").summernote('code', '');
            $("#modal-answer").find(".modal-title").find("span").text(p_user);
            $("#modal-answer").find(".send-answer").attr("data-btn-role", "main-answer");

            $("#modal-answer").find(".send-answer").attr("data-answer", "");
        });
    }

    if ($("#post-detail-page").find(".main-answer-btn").length > 0) {
        $("#post-detail-page").find(".main-answer-btn").click(function () {
            var p_user = $("#post-detail-page").attr("data-name");
            $("#modal-answer").find(".modal-title").find("span").text(p_user);
            $("#modal-answer").find(".send-answer").attr("data-btn-role", "main-answer");
            $("#modal-answer").find(".send-answer").attr("data-answer", "");
        })
    }

    if ($(document).find("#qv-post-detail").find("#post-detail").find(".sub-answer-btn").length > 0) {
        $(document).find("#qv-post-detail").find("#post-detail").find(".sub-answer-btn").click(function () {


            var p_user = $(this).closest(".media").attr("data-name");
            var answer_id = $(this).closest(".media").attr("data-answers");

            answer_text = $("#answer-content").val();

            $("#modal-answer").find(".modal-title").find("span").text(p_user);
            $("#modal-answer").find(".send-answer").attr("data-btn-role", "sub-answer");
            $("#modal-answer").find(".send-answer").attr("data-answer", answer_id);


        });
    }

    if ($("#post-detail-page").find(".sub-answer-btn").length > 0) {
        $("#post-detail-page").find(".sub-answer-btn").click(function () {
            var p_user = $(this).closest(".media").attr("data-name");
            var answer_id = $(this).closest(".media").attr("data-answers");
            answer_text = $("#answer-content").val();

            $("#modal-answer").find(".modal-title").find("span").text(p_user);
            $("#modal-answer").find(".send-answer").attr("data-btn-role", "sub-answer");
            $("#modal-answer").find(".send-answer").attr("data-answer", answer_id);
        });
    }

    $(document).find("#qv-post-detail").find("#post-detail").find(".post-buttons").first().find(".dropdown").find(".edit-this").unbind("click").on("click", function () {
        var id = $("#post-detail").attr("data-post");
        edit_main_post(id);
    });

    $(document).find("#qv-post-detail").find("#post-detail").find("#post-answers").find(".main-answers").each(function () {
        $(this).first().find(".post-buttons").find(".edit-this").unbind("click").click(function () {
            var answer_id = $(this).parent().parent().parent().parent().parent().attr("data-answers");

            edit_main_answer(answer_id);
        });

        $(this).find(".media").find(".post-buttons").find(".edit-this").unbind("click").click(function () {
            var ans_ans_id = $(this).parent().parent().parent().parent().parent().attr("data-id");
            edit_sub_answer(ans_ans_id);
        });

    });


}

function edit_sub_answer(id) {
    $.getJSON("/dashboard/ideas/edit-get-subanswer/?id=" + id, function (data) {
        $("#edit-content").summernote('code', data.answer_text);
    }).done(function (data) {
        $("#modal-edit").find(".save-edit-btn").unbind("click").on("click", function () {
            var answer_content = $("#edit-content").val();
            edit_save_sub_answer(id, answer_content);
        });
    });
}

function edit_save_sub_answer(id, content_text) {
    $.post("/dashboard/ideas/edit-save-subanswer/", {
        "id": id,
        "content_text": content_text
    }, function (data) {
        $("body").find(".preloader").fadeIn();
    }).done(function (data) {
        $("body").find(".preloader").fadeOut();
        switch (data.message_type) {
            case "success":
                toastMessage(data.message, messageStr("ok-message"), "", "success");
                sub_answers_update_content(id, content_text);
                break;
            case "error":
                toastMessage(data.message, messageStr("ok-message"), "", "danger");
                break;
        }
    });

}

function edit_main_answer(id) {

    $.getJSON("/dashboard/ideas/edit-getanswer/?id=" + id, function (data) {
        $("#edit-content").summernote('code', data.answer_text);
    }).done(function (data) {
        $("#modal-edit").find(".save-edit-btn").unbind("click").on("click", function () {
            var answer_content = $("#edit-content").val();
            edit_save_mainanswer(id, answer_content);
        });
    });
}

function edit_save_mainanswer(id, content_text) {

    $.post("/dashboard/ideas/edit-mainanswer/", {
        "id": id,
        "content_text": content_text
    }, function (data) {
        $("body").find(".preloader").fadeIn();
    }).done(function (data) {
        $("body").find(".preloader").fadeOut();
        switch (data.message_type) {
            case "success":
                toastMessage(data.message, messageStr("ok-message"), "", "success");
                main_answers_update_content(id, content_text);
                break;
            case "error":
                toastMessage(data.message, messageStr("ok-message"), "", "danger");
                break;
        }
    });

}

function sub_answers_update_content(id, content_text) {
    $(document).find("#qv-post-detail").find("#post-detail").find("#post-answers").find(".main-answers").each(function () {
        $(this).find(".media-body").find(".media").each(function () {
            if ($(this).attr("data-id") === id) {

                $(this).find(".media-body").find(".content-container").html('');
                $(this).find(".media-body").find(".content-container").append(content_text);
            }
        });

    });
}

function main_answers_update_content(id, content_text) {
    $(document).find("#qv-post-detail").find("#post-detail").find("#post-answers").find(".main-answers").each(function () {

        if ($(this).first().attr("data-answers") === id) {
            $(this).find(".content-container").first().html('');
            $(this).find(".content-container").first().append(content_text);
        }
    });
}

function edit_main_post(id) {

    $.getJSON("/dashboard/ideas/edit-getpost/?id=" + id, function (data) {

        $("#edit-content").summernote('code', data.post_content);

    }).done(function (data) {
        $("#modal-edit").find(".save-edit-btn").unbind("click").on("click", function () {
            var post_content = $("#edit-content").val();
            edit_save_mainpost(id, content_html(post_content));
        });
    });
}

function edit_save_mainpost(id, content_text) {
    $.post("/dashboard/ideas/edit-mainpost/", {
        "id": id,
        "content_text": content_text
    }, function (data) {
        $("body").find(".preloader").fadeIn();
    }).done(function (data) {
        $("body").find(".preloader").fadeOut();
        switch (data.message_type) {
            case "success":
                toastMessage(data.message, messageStr("ok-message"), "", "success");
                $(document).find("#qv-post-detail").find("#post-detail").find(".card-body").find(".content-container").html('');
                $(document).find("#qv-post-detail").find("#post-detail").find(".card-body").find(".content-container").append(content_text);
                break;
            case "error":
                toastMessage(data.message, messageStr("ok-message"), "", "danger");
                break;
        }
    });


}


function add_smilar_project(post_id, link_text, describe_text) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            // $("#load-more-ideas").find(".spinner-circle").fadeIn();
        },
        url: '/dashboard/ideas/post-smilar-project/',
        method: 'post',
        data: {
            "post": post_id,
            "link_text": link_text,
            "describe_text": describe_text
        },
        success: function (data) {
            switch (data.message_type) {
                case "success":
                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    break;
                case "warning":
                    toastMessage(data.message, messageStr("ok-message"), "", "warning");
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");

                    break;
                default:
                    break;
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                $("#post-smilarprojects").find(".counter-text").text(data_json.smilar_project_count);
            }
        }
    });
}

function ideas_label_infos(post_id, buttonType, page_type) {
    if (page_type.indexOf("post-page") > -1) {
        $.getJSON("/dashboard/ideas/user-infos/?post_id=" + post_id, function (data) {
            switch (buttonType) {
                case "similar-project":
                    $("#modal-smilar-project").find(".current-user-img").find("img").attr("src", data.current_user_profile_img);
                    $("#modal-smilar-project").find(".current-user").text(data.current_user_profilename);
                    $("#modal-smilar-project").find(".current-user-position").text(data.current_user_position_title);
                    $("#modal-smilar-project").find(".request-user-img").find("img").attr("src", data.requested_user_profile_img);
                    $("#modal-smilar-project").find(".request-user").text(data.requested_user_profilename);
                    $("#modal-smilar-project").find(".request-user-position").text(data.requested_user_position_title);
                    break;
                case "team-request":
                    $("#modal-team-request").find(".current-user-img").find("img").attr("src", data.current_user_profile_img);
                    $("#modal-team-request").find(".current-user").text(data.current_user_profilename);
                    $("#modal-team-request").find(".current-user-position").text(data.current_user_position_title);
                    $("#modal-team-request").find(".request-user-img").find("img").attr("src", data.requested_user_profile_img);
                    $("#modal-team-request").find(".request-user").text(data.requested_user_profilename);
                    $("#modal-team-request").find(".request-user-position").text(data.requested_user_position_title);
                    break;
            }


        }).done(function (data) {

        });
    }
    else {
        $.getJSON("/dashboard/projects/user-infos/?project_id=" + post_id, function (data) {

        }).done(function (data) {
            switch (buttonType) {
                case "send-feedback":
                    $("#modal-feedback").find(".current-user-img").find("img").attr("src", data.current_user_profile_img);
                    $("#modal-feedback").find(".current-user").text(data.current_user_profilename);
                    $("#modal-feedback").find(".current-user-position").text(data.current_user_position_title);
                    $("#modal-feedback").find(".request-user-img").find("img").attr("src", data.requested_user_profile_img);
                    $("#modal-feedback").find(".request-user").text(data.requested_user_profilename);
                    $("#modal-feedback").find(".request-user-position").text(data.requested_user_position_title);
                    break;
                case "send-teamrequest":
                    $("#modal-team-request").find(".current-user-img").find("img").attr("src", data.current_user_profile_img);
                    $("#modal-team-request").find(".current-user").text(data.current_user_profilename);
                    $("#modal-team-request").find(".current-user-position").text(data.current_user_position_title);
                    $("#modal-team-request").find(".request-user-img").find("img").attr("src", data.requested_user_profile_img);
                    $("#modal-team-request").find(".request-user").text(data.requested_user_profilename);
                    $("#modal-team-request").find(".request-user-position").text(data.requested_user_position_title);
                    break;
            }

        });
    }
}

function send_user_teamrequest(selected_post, page_type) {
    var url = '';
    switch (page_type) {
        case "post-page":
            url = '/dashboard/ideas/send-teamrequest/';
            break;
        case "project-page":
            url = '/dashboard/projects/send-teamrequest/';
            break;
    }
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: url,
        method: 'post',
        data: {
            "post": selected_post
        },
        success: function (data) {
            switch (data.message_type) {
                case "success":
                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    break;
                case "warning":
                    toastMessage(data.message, messageStr("ok-message"), "", "warning");
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");

                    break;
                default:
                    break;
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                if (page_type === "post-page") {
                    $("#post-teamrequests").find(".counter-text").text(data_json.teamrequest_count);
                }

            }
        }
    });
}

function ideas_detail(selected_post) {
    document.location.href = "/dashboard/ideas/detail/" + selected_post;
}

function share_idea(idea_txt) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/dashboard/ideas/share-idea/',
        method: 'post',
        data: {
            "idea": idea_txt
        },
        success: function (data) {
            switch (data.message_type) {
                case "success":
                    toastMessage(data.message, messageStr("ok-message"), data.redirect_url, "success");
                    break;
                case "warning":
                    toastMessage(data.message, messageStr("ok-message"), data.redirect_url, "warning");
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
                default:
                    break;
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {

            }
        }
    });

}

/*Team request*/
function get_teamrequests() {
    $.getJSON("/dashboard/notification-teamrequest/", function (data) {
        $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".notification-content").html('');
        if (data.notifications.length > 0) {

            $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".notification-content").append('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');
        }
        else {
            $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".topbar-btn").removeClass("has-new");
        }

    }).done(function (data) {
        if (data.notifications.length > 0) {
            $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".notification-content").find(".spinner-linear").remove();
            $.each(data.notifications, function (index, value) {
                $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".notification-content").append(notification_item_team(value.profile_image, value.profile_name, value.created_date, value.message, value.is_read, value.redirect_url, value.type, value.id));
            });

            dm_request_answer();

            if (data.has_next) {
                loadmore_teamrequest(data.page);
            }


        }
    });
}

function dm_request_answer() {
    $(".notification-teamrequest").find(".media-list").find(".item-dm").on("click", function () {
        var dm_id = $(this).attr("data-id");
        requested_dm_project_info(dm_id).done(function (data) {
            dm_sweet_popup(dm_id, dm_project_item(data.project_title, data.project_content, data.project_address, data.project_logo, data.project_location, data.project_point), true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), messageStr("request-successful-text-title"), messageStr("request-successful-text-content"), messageStr("request-cancel-message-title"), messageStr("request-cancel-message-text-content"));
        });

    });
}

function requested_dm_project_info(id) {
    return $.getJSON("/dashboard/profiles/requested-dm-project/?id=" + id, function (data) {

    });

}

function requests_count() {
    $.getJSON("/dashboard/teamrequest-count/", function (data) {
        if (data.request_count > 0) {
            $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".topbar-btn").addClass("has-new");
        }
        else {
            $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".topbar-btn").removeClass("has-new");
        }
    })
}

/*Team request*/

function filter_projects(prj_option, prj_stage) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#projects-list").html('');
            $("#load-more-projects").find(".list-result-message").find("p").remove();
            $("#load-more-projects").find(".list-result-message").before('<div class="spinner-circle-shadow mx-auto"></div>');

        },
        url: '/dashboard/projects/filter-projects/',
        method: 'post',
        data: {
            "selected_option": prj_option,
            "selected_stage": prj_stage
        },
        success: function (data) {
            if ($(data.project_list).length > 0) {
                var listHtml = '';
                $.each(data.project_list, function (index, value) {
                    switch (value.message_type) {
                        case "success":
                            listHtml += project_item_html(value.project_name, value.project_logo, value.project_location, value.project_id, value.is_teamrequest, value.is_feedback, value.business_stage);
                            break;
                        case "error":
                            break;
                    }
                });
                $("#projects-list").append(listHtml);
            }
            else {
                $("#load-more-projects").find(".list-result-message").append('<p>' + messageStr('project-no-item') + '</p>');
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                $("#load-more-projects").find(".spinner-circle-shadow").remove();
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-projects").find(".list-result-message").find("button").show();
                        break;
                    case false:
                        $("#load-more-projects").find(".list-result-message").append('<p>' + messageStr('project-result-message') + '</p>');
                        $("#load-more-projects").find(".list-result-message").find("button").hide();
                        break;
                }

            }
        }
    });
}

function get_users_supports(projects_id) {
    $.getJSON("/dashboard/projects/users-supports/?projects_id=" + projects_id, function (data) {

    }).done(function (data) {

        $("#modal-team-request").find(".selected-hour").attr("data-hour", data.data_project_hours);
        $("#modal-team-request").find(".selected-hour").text(data.project_hours);

        $("#modal-team-request").find(".selected-investment").attr("data-investment", data.data_support_type);
        $("#modal-team-request").find(".selected-investment").text(data.support_type);
    });
}

function send_feedbackToProject(strContent, project_id) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: '/dashboard/projects/send-feedback/',
        method: 'post',
        data: {
            "project_id": project_id,
            "feedback": strContent
        },
        success: function (data) {

            switch (data.message_type) {

                case "success":
                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    break;
                case "warning":
                    toastMessage(data.message, messageStr("ok-message"), "", "warning");
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
            }

        }
    });
}

function project_undecided_teamrequests(id) {
    //users_projects_id
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#undecided-requests").find(".alert").remove();
            $("#undecided-requests").find(".undecided-list").html('');
            $("#load-more-undecided").find(".result-message").remove();
            $("#undecided-requests").find(".undecided-list").html('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/myprojects/undecided-requests/' + id + "/",
        method: 'post',
        data: {
            "id": id
        },
        success: function (data) {
            if (data.membership_type === "beginner") {
                $("#undecided-requests").find(".undecided-list").html('');
                $("#load-more-undecided").find("button").before('<p class="text-center result-message">' + data.message + '</p>');
            }
            else {
                if (data.requests.length > 0) {
                    var requestHtmlStr = "";
                    $.each(data.requests, function (index, value) {
                        requestHtmlStr += teamrequest_item(value.profile_img, value.profile_name, value.id, value.position_title, value.support_type, value.hour_info, value.hour_cost, value.location);
                    });

                    $("#undecided-requests").find(".undecided-list").html('');
                    $("#undecided-requests").find(".undecided-list").append(requestHtmlStr);
                    $("#undecided-requests").find(".undecided-list").before(alertBox(data.message, "info"));
                }
                else {
                    $("#undecided-requests").find(".undecided-list").html('');

                    $("#load-more-undecided").find("button").before('<p class="text-center result-message">' + data.message + '</p>');
                    $("#load-more-undecided").find("button").hide();

                }
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                switch (data_json.has_next) {
                    case true:
                        $("#load-more-undecided").show();
                        // $("#load-more-undecided").find(".result-message").remove();
                        $("#load-more-undecided").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        // $("#load-more-undecided").find(".result-message").remove();
                        $("#load-more-undecided").find("button").hide();
                        // $("#load-more-undecided").find("button").before('<p class="text-center result-message">' + data_json.message + '</p>');
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

            }
        }
    });
}
//burdayım
function myideas_undecided_requests(id, page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#loadmore-ideas-undecided").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
            $("#loadmore-ideas-undecided").find(".result-message").remove();
        },
        url: '/dashboard/myideas/undecided-requests/' + id + "/",
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {
            if (data.message_type === "success") {

                if (data.membership_type === "beginner") {
                    $("#loadmore-ideas-undecided").find("button").before('<p class="text-center result-message">' + data.message + '</p>');
                }
                else {
                    var requestHtmlStr = "";
                    $.each(data.requests, function (index, value) {
                        requestHtmlStr += teamrequest_item(value.profile_img, value.profile_name, value.id, value.position_title, value.support_type, value.hour_info, value.hour_cost, value.location);

                    });

                    $("#tab-undecided").find(".undecided-list").append(requestHtmlStr);
                }


            }
            // else {
            //     $("#tab-undecided").find(".undecided-list").html('');
            //     $("#tab-undecided").find(".undecided-list").append('<div class="col-md-12 col-lg-12">' + messageStr('request-no-item') + '</div>');
            // }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                $("#loadmore-ideas-undecided").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-ideas-undecided").find(".result-message").remove();
                        $("#loadmore-ideas-undecided").find("button").show();
                        $("#loadmore-ideas-undecided").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-ideas-undecided").find("button").before('<p class="text-center result-message">' + messageStr('request-no-item') + '</p>');
                        $("#loadmore-ideas-undecided").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

            }
        }
    });

}

function myideas_teamrequest(id, page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $("#loadmore-ideasrequest").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/myideas/teamrequest/' + id + "/",
        method: 'post',
        data: {
            // "id": id,
            "page": page
        },
        success: function (data) {

            if (data.message_type) {
                var requestHtmlStr = "";
                $.each(data.requests, function (index, value) {
                    requestHtmlStr += teamrequest_item(value.profile_img, value.profile_name, value.id, value.position_title, value.support_type, value.hour_info, value.hour_cost, value.location);
                });

                $("#tab-team").find(".requests-list").append(requestHtmlStr);
            }
            // else {
            //     $("#tab-team").find(".requests-list").html('');
            //     $("#loadmore-ideasrequest").find(".spinner-circle-shadow").remove();
            //     $("#tab-team").find(".requests-list").append('<div class="col-md-12 col-lg-12">' + alertBox(data.message, "danger") + '</div>');
            // }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                $("#loadmore-ideasrequest").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-ideasrequest").find(".result-message").remove();
                        $("#loadmore-ideasrequest").find("button").show();
                        $("#loadmore-ideasrequest").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-ideasrequest").find("button").before('<p class="text-center result-message">' + messageStr("request-no-item") + '</p>');
                        $("#loadmore-ideasrequest").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                myideas_requests();
            }
        }
    });
}

function myproject_feedbacks_teamrequests(page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            if (page !== "") {
                $("#loadmore-mp-feedbacks").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            else {
                $("#tab-feedbacks").find(".project-list").append('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            $("#loadmore-mp-feedbacks").find(".result-message").remove();


        },
        url: '/dashboard/myprojects/teamrequests/',
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {

            if (data.project_list.length > 0) {
                var requestHtmlStr = "";
                $.each(data.project_list, function (index, value) {
                    requestHtmlStr += project_request_item_html(value.project_name, value.project_logo, value.project_locations, value.id, value.is_teamrequest, value.is_feedback, value.steps, value.is_online, "teamrequest");

                });
                $("#tab-feedbacks").find(".project-list").find(".spinner-circle-shadow").remove();
                $("#tab-feedbacks").find(".project-list").append(requestHtmlStr);
            }
            // else {
            //     $("#tab-feedbacks").find(".project-list").html('');
            //     $("#tab-feedbacks").find(".project-list").append('<div class="col-md-12 col-lg-12">' + alertBox(data.message, "danger") + '</div>');
            // }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                $("#loadmore-mp-feedbacks").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-mp-feedbacks").find(".result-message").remove();
                        $("#loadmore-mp-feedbacks").find("button").show();
                        $("#loadmore-mp-feedbacks").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-mp-feedbacks").find("button").before('<p class="text-center result-message">' + messageStr("myprojects-no-item") + '</p>');
                        $("#loadmore-mp-feedbacks").find("button").hide();
                        break;
                }


            }
        }
    });
}

function project_feedbacks(id, page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            if (page !== "") {
                $("#loadmore-feedbacks").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            else {
                $("#feedbacks").find(".feedback-list").append('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            $("#loadmore-feedbacks").find(".result-message").remove();
        },
        url: '/dashboard/myprojects/feedbacks/' + id + "/",
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {

            if (data.feedbacks.length > 0) {
                var requestHtmlStr = "";

                $.each(data.feedbacks, function (index, value) {
                    requestHtmlStr += feedback_item(value.profile_img, value.profile_id, value.profile_name, value.created_date, value.content_text);

                });

                $("#feedbacks").find(".feedback-list").find(".spinner-circle-shadow").remove();

                if (page !== 1) {
                    $("#feedbacks").find(".feedback-list").find(".timeline").append(requestHtmlStr);
                }
                else {
                    $("#feedbacks").find(".feedback-list").append(feedback_main_item(requestHtmlStr));
                }

            }
            else {
                $("#feedbacks").find(".feedback-list").html('');
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                $("#loadmore-feedbacks").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-feedbacks").find(".result-message").remove();
                        $("#loadmore-feedbacks").find("button").show();
                        $("#loadmore-feedbacks").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-feedbacks").find(".result-message").remove();
                        $("#loadmore-feedbacks").find("button").before('<p class="text-center result-message">' + messageStr("myproject-no-feedback") + '</p>');
                        $("#loadmore-feedbacks").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

            }
        }
    });
}

function filter_prj_teamrequests(id, page, choice_str) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $("#load-more-requests").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
            $("#load-more-requests").find(".result-message").remove();
            $("#users-requests").find(".project-requests-lists").find(".result-message").remove();
            $("#users-requests").find(".project-requests-lists").html('');
        },
        url: '/dashboard/myprojects/filter-teamrequests/' + id + "/",
        method: 'post',
        data: {
            "filter_choice": choice_str,
            "page": page
        },
        success: function (data) {
            if (data.requests.length > 0) {
                var requestHtmlStr = "";
                $.each(data.requests, function (index, value) {
                    requestHtmlStr += teamrequest_item(value.profile_img, value.profile_name, value.id, value.position_title, value.support_type, value.hour_info, value.hour_cost, value.location);
                });
                $("#users-requests").find(".project-requests-lists").append(requestHtmlStr);
            }
            else {
                $("#users-requests").find(".project-requests-lists").html('');
                $("#users-requests").find(".project-requests-lists").append('<div class="col-md-12 col-lg-12"><p class="text-center result-message">' + messageStr("myproject-filter-no-item") + '</p></div>');
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                $("#load-more-requests").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-requests").find(".result-message").remove();
                        $("#load-more-requests").find("button").show();
                        $("#load-more-requests").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#load-more-requests").find("button").before('<p class="text-center result-message">' + messageStr("myproject-filter-no-item") + '</p>');
                        $("#load-more-requests").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                myprj_requests();


            }
        }
    });
}

function filter_ideas_teamrequests(id, page, choice_str) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $("#loadmore-ideasrequest").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
            $("#loadmore-ideasrequest").find(".result-message").remove();
            $("#tab-team").find(".requests-list").html('');
        },
        url: '/dashboard/myideas/filter-teamrequests/' + id + "/",
        method: 'post',
        data: {
            "filter_choice": choice_str,
            "page": page
        },
        success: function (data) {

            if (data.requests.length > 0) {
                var requestHtmlStr = "";
                $.each(data.requests, function (index, value) {
                    requestHtmlStr += teamrequest_item(value.profile_img, value.profile_name, value.id, value.position_title, value.support_type, value.hour_info, value.hour_cost, value.location);
                });
                $("#tab-team").find(".requests-list").append(requestHtmlStr);
            }
            // else {
            //     $("#tab-team").find(".requests-list").html('');
            //     $("#tab-team").find(".requests-list").append('<div class="col-md-12 col-lg-12">' + messageStr("myproject-filter-no-item") + '</div>');
            // }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                $("#loadmore-ideasrequest").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-ideasrequest").find(".result-message").remove();
                        $("#loadmore-ideasrequest").find("button").show();
                        $("#loadmore-ideasrequest").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-ideasrequest").find("button").before('<p class="text-center result-message">' + messageStr("myproject-filter-no-item") + '</p>');
                        $("#loadmore-ideasrequest").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                myideas_requests();


            }
        }
    });
}

function answers_ideas_teamrequests(selected_option, requests) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: "/dashboard/myideas/response-teamrequests/",
        method: 'post',
        data: {
            "project_id": selected_option,
            "users": requests
        },
        success: function (data) {

        }

    });

}
//user location
// function member_locationCallBack(callbackData){
//     console.log(callbackData);
//     $(document).find("#tab-team").find("#current-city").val(callbackData.location_name);
// }

var location_info = function selected_person_location(user_id, page_type, project_id) {
    var url = "";
    var locationStr = "";

    if (page_type === "project") {
        url = "/dashboard/myprojects/selected-person-location/";
    }
    else {
        url = "/dashboard/myideas/selected-person-location/";
    }

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: url,
        async: false,
        method: 'post',
        data: {
            "id": project_id,
            "user": user_id
        },
        success: function (data) {
            // console.log(data);
            // $(document).find("#tab-team").find("#current-city").val(data.location_name);
            locationStr = data.is_it;
        }
        // success:member_locationCallBack

    });
    return locationStr;
};


var same_members = function same_location_count(user_list, page_type, project_id) {
    var url = "";
    var same_counter = 0;
    if (page_type === "project") {
        url = "/dashboard/myprojects/selected-person/";
    }
    else {
        url = "/dashboard/myideas/selected-person/";
    }

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: url,
        method: 'post',
        async: false,
        data: {
            "id": project_id,
            "users": user_list
        },
        success: function (data) {

            same_counter = data.location_counter;
        }

    });
    return same_counter;
};

function user_membeships() {
    return $.getJSON("/dashboard/profiles/membership-types/", function (data) {

    });
}


function create_project_or_update(myideas_requests, id) {
    var project_item = "";
    var main_container = "";
    var new_project_item = '<label class="custom-control custom-control-dark b-1 border-light p-2 custom-radio">' +
        '<input type="radio" class="custom-control-input" name="radio1" data-id="0">' +
        '<span class="custom-control-indicator"></span>' +
        '<span class="custom-control-description">' + messageStr("new-project-item") + '</span>' +
        '</label>';
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: "/dashboard/myideas/project-situation/",
        method: 'post',
        data: {},
        success: function (data) {

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                if (data_json.users_projects.length > 0) {
                    $.each(data_json.users_projects, function (index, value) {
                        project_item += project_list(value.title, value.id);
                    });


                    if (data_json.has_created_new_one === false) {
                        main_container += '<div class="custom-controls-stacked">' + project_item + new_project_item + '</div>';
                    }
                    else {
                        main_container += '<div class="custom-controls-stacked">' + project_item + '</div>';
                    }

                    main_container += '<button class="btn btn-w-md btn-outline btn-success my-2">' + messageStr("select-project-item") + '</button>';

                    //bu kısımda proje listelerini kullanıcıya sunuyoruz. içerde seçim yaptırmamız lazım
                    if (myideas_requests.length > 0) {
                        sweet_popup("<h3 class='card-title b-0'>" + messageStr("request-question-title") + "<br/><br/>" + "<span class='fs-14'>" + messageStr("ideas-question-title") + "</span></h3>", main_container, myideas_requests, "question", true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), messageStr("request-successful-text-title"), messageStr("request-successful-text-content"), messageStr("request-cancel-message-title"), messageStr("request-cancel-message-text-content"), id, "ideas-page");
                    }

                }
                else {
                    //bu kısımda proje olmayacağı için direk kişi listesi gösterilecek
                    if (myideas_requests.length > 0) {
                        user_profiles_item_ideas(myideas_requests, id);
                    }

                }
            }
        }
    });
}

function answers_teamrequests(id, requests, page_type, post_id) {
    var urlStr = "";
    if (page_type === "project-page") {
        urlStr = "/dashboard/myprojects/response-teamrequests/";
    }
    else {
        urlStr = "/dashboard/myideas/response-teamrequests/" + post_id + "/";
    }

    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("body").find(".preloader").fadeIn();
        },
        url: urlStr,
        method: 'post',
        data: {
            "users": requests,
            "id": id
        },
        success: function (data) {

        }
    });

}

//bu kısımda kullanıcı listesi gösteriliyor
function second_stageIdeas_users_profiles(myrequests, id) {

    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: '/dashboard/myideas/response-profiles/',
        method: 'post',
        data: {
            "users": myrequests,
            "id": id
        },
        success: function (data) {

        }

    });
}

function user_profiles_item(myprj_requests, id) {
    var user_item_html = "";
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: '/dashboard/myprojects/response-profiles/',
        method: 'post',
        data: {
            "users": myprj_requests,
            "id": id
        },
        success: function (data) {

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                if (data_json.users.length > 0) {
                    $.each(data_json.users, function (index, value) {
                        user_item_html += avatar_list_str(value.profile_img, value.profile_name);
                    });

                    sweet_popup("<h3 class='card-title b-0'>" + messageStr("request-question-title") + "</h3>", user_item_html, myprj_requests, "question", true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), messageStr("request-successful-text-title"), messageStr("request-successful-text-content"), messageStr("request-cancel-message-title"), messageStr("request-cancel-message-text-content"), id, "project-page");
                }
                else {
                    sweet_popup("<h3 class='card-title b-0'>" + messageStr("request-question-title") + "</h3>", user_item_html, myprj_requests, data_json.message_type, true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), data_json.message, messageStr("request-successful-text-content"), messageStr("request-cancel-message-title"), messageStr("request-cancel-message-text-content"), id, "project-page");
                }

            }
        }
    });
}

function user_profiles_item_ideas(myideas_requests, id) {
    var user_item_html = "";
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: '/dashboard/myideas/response-profiles/',
        method: 'post',
        data: {
            "users": myideas_requests
        },
        success: function (data) {

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                if (data_json.users.length > 0) {
                    $.each(data_json.users, function (index, value) {
                        user_item_html += avatar_list_str(value.profile_img, value.profile_name);
                    });


                    //proje listesini burada yollayabiliriz
                    sweet_popup("<h3 class='card-title b-0'>" + messageStr("request-question-title") + "</h3>", user_item_html, myprj_requests, "question", true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), messageStr("request-successful-text-title"), messageStr("request-successful-text-content"), messageStr("request-cancel-message-title"), messageStr("request-cancel-message-text-content"), id, "ideas-page");

                }
            }
        }
    });
}

function fix_menu_btn_types(id, page, data_title, page_type) {
    switch (data_title) {
        case "send-requests":
            if (page_type === "project-page") {
                user_profiles_item(all_myprj_requests, id);
            }
            else {
                create_project_or_update(all_myideas_requests, id); //post_id
            }
            break;
        default:

            if (page_type === "project-page") {
                if ($("#users-requests").find(".alert-info").length > 0) {
                    $("#users-requests").find(".alert-info").last().remove();
                }
                if ($("#users-requests").find(".alert-danger").length > 0) {
                    $("#users-requests").find(".alert-danger").last().remove();
                }
                all_myprj_requests.splice(0, all_myprj_requests.length);
                filter_prj_teamrequests(id, page, data_title);
            }
            else {
                if ($("#tab-team").find(".alert-info").length > 0) {
                    $("#tab-team").find(".alert-info").last().remove();
                }
                if ($("#tab-team").find(".alert-danger").length > 0) {
                    $("#tab-team").find(".alert-danger").last().remove();
                }
                all_myideas_requests.splice(0, all_myideas_requests.length);
                filter_ideas_teamrequests(id, page, data_title);
            }

            break;

    }
}

function newmessages_count() {
    $.getJSON("/dashboard/messages/notification-count/", function (data) {
        if (data.messages_count > 0) {
            $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".topbar-btn").addClass("has-new");
        }
        else {
            $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".topbar-btn").removeClass("has-new");
        }
    });
}

function notifications_count() {
    $.getJSON("/dashboard/notifications/notification-count/", function (data) {

        if (data.notifcations_count > 0) {
            $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".topbar-btn").addClass("has-new");

        }
        else {

            $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".topbar-btn").removeClass("has-new");
        }
    })
}


function get_messages() {

    if ($(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".spinner-linear").length === 0) {
        $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".dropdown-footer").before('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');
    }
    $.getJSON("/dashboard/messages/notification-list/", function (data) {
    }).done(function (data) {

        $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".spinner-linear").remove();
        $.each(data.messages, function (index, value) {

            $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".message-notification-content").append(message_ntf_item(value.sender_name, value.profile_image, value.sended_date, value.message_content, value.id, value.is_read));
        });

        if (data.has_next) {
            loadmore_getmessages(data.page);
        }

    });
}

function get_general_notifications() {
    $.getJSON("/dashboard/notifications/general-notifications/", function (data) {
        // $("#general-notifications").find(".notification-content").html('');
        $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").html('');
    }).done(function (data) {

        $.each(data.notifications, function (index, value) {
            $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").append(notification_item(value.redirect_url, value.icon, value.message, value.created_date, value.is_read, value.color, value.link_type, value.profile_name, value.id));
        });

        $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").each(function () {
            $(this).find(".item-answer").unbind("click").on("click", function () {
                $("#quick-answer-content").summernote('code', '');
                var answer_id = $(this).attr("data-answers");
                $("#modal-quick-answer").attr("data-answers", answer_id);
                quick_answer(answer_id).done(function (data) {
                    $("#modal-quick-answer").find(".spinner-circle-shadow").remove();

                    $("#modal-quick-answer").find("hr").before(quick_answer_item(data.profile_name, data.created_date, data.profile_image, data.answer_content, data.like_count, data.answer_count));
                });
            });
        });

        if (data.has_next) {
            loadmore_get_general_notifications(data.page);
        }
    });
}

var quick_answer = function get_quick_answer(answer_id) {
    return $.getJSON("/dashboard/ideas/get-answer/?id=" + answer_id, function (data) {
        $("#modal-quick-answer").find(".media").remove();
        $("#modal-quick-answer").find("hr").before('<div class="spinner-circle-shadow mx-auto"></div>');
    });
};

function write_quick_answer(answer_id, answer_content) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("body").find(".preloader").fadeIn();
        },
        url: '/dashboard/ideas/write-quick-answers/',
        method: 'post',
        data: {
            "answer": answer_id,
            "answer-content": answer_content
        },
        success: function (data) {

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                $("body").find(".preloader").fadeOut();
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.message_type) {
                    case "success":
                        toastMessage(data_json.message, messageStr("ok-message"), "", "success");
                        $("#quick-answer-content").summernote('code', '');
                        break;
                    case "error":
                        toastMessage(data_json.message, messageStr("ok-message"), "", "danger");
                        break;
                    default:
                        toastMessage(data_json.message, messageStr("ok-message"), "", "warning");
                        break;
                }
            }


        }
    });
}

function fundedProjectsCount() {
    $.getJSON("/dashboard/myprojects/funded-projects-count/", function (data) {

        $("#myproject-nav").find(".nav-tabs").find(".nav-item").each(function () {
            if ($(this).index() === 2) {
                $(this).find(".nav-link").find("small").text('(' + data.funded_count + ')');
            }
        })
    });
}

function myproject_list(page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            if (page !== "") {
                $("#loadmore-myprojects").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            else {
                $("#tab-projects").find(".project-list").append('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            $("#loadmore-myprojects").find(".result-message").remove();

        },
        url: '/dashboard/myprojects/myprojects-list/',
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {

            if (data.project_list.length > 0) {
                var requestHtmlStr = "";
                $.each(data.project_list, function (index, value) {
                    requestHtmlStr += project_request_item_html(value.project_name, value.project_logo, value.project_locations, value.id, value.is_teamrequest, value.is_feedback, value.steps, value.isOnline, "project");
                });

                $("#tab-projects").find(".project-list").find(".spinner-circle-shadow").remove();
                $("#tab-projects").find(".project-list").append(requestHtmlStr);
            }
            // else {
            //     $("#tab-feedbacks").find(".project-list").html('');
            //     $("#tab-feedbacks").find(".project-list").append('<div class="col-md-12 col-lg-12">' + alertBox(data.message, "danger") + '</div>');
            // }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                $("#loadmore-myprojects").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-myprojects").find(".result-message").remove();
                        $("#loadmore-myprojects").find("button").show();
                        $("#loadmore-myprojects").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-myprojects").find("button").before('<p class="text-center result-message">' + messageStr("myprojects-no-item") + '</p>');
                        $("#loadmore-myprojects").find("button").hide();
                        break;
                }

                $(document).find("#loadmore-myprojects").find("button").on("click", function () {
                    var page = $(this).attr("data-page");
                    myproject_list(page);
                });

            }
        }
    });
}

function coFunded_projects(page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            if (page !== "") {
                $("#loadmore-cofunded").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            else {
                $("#tab-cofunded").find(".project-list").append('<div class="spinner-circle-shadow mx-auto"></div>');
            }
            $("#loadmore-cofunded").find(".result-message").remove();

        },
        url: '/dashboard/myprojects/cofunded-projects-list/',
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {

            if (data.project_list.length > 0) {
                var requestHtmlStr = "";
                $.each(data.project_list, function (index, value) {
                    requestHtmlStr += project_request_item_html(value.project_name, value.project_logo, value.project_locations, value.id, value.is_teamrequest, value.is_feedback, value.steps, value.isOnline, "cofunded");
                });

                $("#tab-cofunded").find(".project-list").find(".spinner-circle-shadow").remove();
                $("#tab-cofunded").find(".project-list").append(requestHtmlStr);
            }
            // else {
            //     $("#tab-cofunded").find(".project-list").html('');
            //     $("#tab-cofunded").find(".project-list").append('<div class="col-md-12 col-lg-12">' + messageStr("cofunded-no-project") + '</div>');
            // }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                $("#loadmore-cofunded").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-cofunded").find(".result-message").remove();
                        $("#loadmore-cofunded").find("button").show();
                        $("#loadmore-cofunded").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-cofunded").find("button").before('<p class="text-center result-message">' + messageStr("cofunded-no-project") + '</p>');
                        $("#loadmore-cofunded").find("button").hide();
                        break;
                }
                $(document).find("#loadmore-cofunded").find("button").on("click", function () {
                    var page = $(this).attr("data-page");
                    coFunded_projects(page);
                });
            }
        }
    });
}

var investment_choice = "";
function filter_options_profiles() {
    var selectBoxHtml = '';
    var selectItems = '';
    var lang = $("#language").val();

    $.getJSON("/dashboard/main/select-invest/", function (data) {
        $.each(JSON.parse(data), function (index, value) {
            switch (lang) {
                case "tr":
                    selectItems += '<option value="' + value.id + '">' + value.title + '</option>';
                    break;
                default:
                    selectItems += '<option value="' + value.id + '">' + value.title_eng + '</option>';
                    break;
            }

            selectBoxHtml = '<select data-provide="selectpicker" data-width="100%" class="my-2 mx-2" name="select-investments-prf" id="select-investments-prf">' + selectItems + '</select>';

        });
        $(document).find("#filter-profiles").find(".input-group").html('');
        $(document).find("#filter-profiles").find(".input-group").html(selectBoxHtml);

    }).done(function (data) {

        $(document).find("#select-investments-prf").on("change", function () {
            investment_choice = $(this).val();
        });

        // $(document).find("#filter-profiles-btn").on("click", function () {
        //     var page = $(this).attr("data-page");
        //     filter_profiles(choice_str, investment_choice, page);
        // });
    });
}

function filter_profiles(filter_choice, investment_choice, page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#load-more-container").find(".result-message").remove();
            $("#load-more-container").find("button").hide();
            $("#person-list").html('');
            $("#person-list").html('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/profiles/filter-profiles/',
        method: 'post',
        data: {
            "page": page,
            "filter_option": filter_choice,
            "investment_option": investment_choice
        },
        success: function (data) {

            if (data.profiles.length > 0) {
                var profile_html = "";
                $.each(data.profiles, function (index, value) {
                    profile_html += list_html(value.id, value.profile_picture, value.profile_name, value.position_title, value.profile_location, value.profile_point, value.member_type);
                });
                $("#person-list").html('');
                $("#person-list").append(profile_html);
            }
            else {
                $("#person-list").html('');
                //mesaj ekle sonra
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-container").find(".spinner-circle-shadow").remove();
                        $("#load-more-container").find("button").show();
                        $("#load-more-container").find("button").attr("data-page", data_json.page);
                        break;
                    case false:

                        $("#load-more-container").find(".spinner-circle-shadow").remove();
                        $("#load-more-container").find("button").before('<p class="result-message">' + messageStr('list-result-message') + '</p>');
                        $("#load-more-container").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                $(document).find("#person-list").find(".card").each(function () {
                    $(this).find(".send-team-request").unbind("click").on("click", function () {
                        var profile_id = $(this).parent().parent().parent().find(".profile-popup").attr("data-id");

                        general_send_teamrequest_to_person(profile_id);
                    });

                    $(this).find(".send-dm-message").unbind("click").on("click", function () {
                        var id = $(this).parent().parent().parent().find(".profile-popup").attr("data-id");
                        send_dm_message(" ", true, false, true, button_text("send-message"), button_text("cancel-btn"), id);
                    });
                });

                $("#load-more-container").find("button").unbind("click").on("click", function () {
                    var btn_page = $(this).attr("data-page");
                    more_load(filter_choice, investment_choice, btn_page);
                });

            }
        }
    });
}

function dashboard_near_ideas() {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#near-ideas").find(".media-list").html("");
            $("#near-ideas").find(".media-list").append('<div class="spinner-circle-shadow  mx-auto my-140"></div>');

        },
        url: '/dashboard/near-ideas/',
        method: 'post',
        data: {},
        success: function (data) {

            $("#near-ideas").find(".spinner-circle-shadow").remove();
        },
        complete: function (jqXHR, status) {

            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                var htmlStr = "";
                if (data_json.length > 0) {
                    $.each(data_json, function (index, value) {
                        htmlStr += near_ideas_item(value.profile_picture, value.created_date, value.profile_username, value.post_content, value.post_id, value.profile_location, value.member_type);
                    });

                    $("#near-ideas").find(".media-list").append(htmlStr);
                }
                else {
                    $("#near-ideas").find(".media-list").append('<p class="text-center result-message pt-100 px-4">' + messageStr("near-person-message") + '</p>');
                }


                $(document).find(".post-item").on("click", function () {
                    var data_target = $(this).attr("data-target");
                    var data_toggle = $(this).attr("data-toggle");

                    var post = $(this).attr("data-post");
                    if (data_target.length > 0 && data_toggle.length > 0) {
                        ideas_popup(post);
                        get_post_answers(post, "popup-page");
                    }
                    else {
                        ideas_detail(post);
                    }

                });

            }
        }
    });
}

function dashboard_near_members() {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#near-persons").find(".media-list").find(".result-message").remove();
            $("#near-persons").find(".media-list").append('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/near-members/',
        method: 'post',
        data: {},
        success: function (data) {
            $("#near-persons").find(".media-list").find(".spinner-circle-shadow").remove();

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                var htmlStr = "";

                if (data_json.profiles.length > 0) {
                    $.each(data_json.profiles, function (index, value) {
                        htmlStr += near_person_item(value.profile_name, value.profile_picture, value.position_title, value.id, value.member_type);
                    });
                    $("#near-persons").find(".media-list").append(htmlStr);
                }
                else {
                    $("#near-persons").find(".media-list").append('<p class="text-center result-message pt-100 px-4">' + messageStr("near-person-message") + '</p>');
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                if (data_json.has_next) {
                    loadmore_dashboard_near_members(data_json.page, data_json.has_next);
                }
                send_teamrequest_to_near_persons();
                send_dm_message_to_near_persons();
            }
        }
    });
}

function general_send_teamrequest_to_person(user_id) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: '/dashboard/profiles/dm-teamrequest/',
        method: 'post',
        data: {
            "user_id": user_id
        },
        success: function (data) {

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.message_type) {
                    case "success":
                        special_sweet_popup(dm_user_info(data_json.profile_address, data_json.profile_point, data_json.profile_image, data_json.profile_name, data_json.profile_location, data_json.position_title), user_id, true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), messageStr("request-successful-text-title"), messageStr("dm-request-successfull-text-content"), messageStr("request-cancel-message-title"), messageStr("dm-request-cancel-message-text-content"));
                        break;
                    case "warning":
                        classic_special_sweet_popup("", '<p class="fw-300 lh-15 text-light fs-16">' + data_json.message + '</p>', data_json.message_type, false, false, messageStr("db-warning-confirm-text"), "");
                        break;
                }

            }
        }
    });
}

function send_dm_message_to_near_persons(){
    $("#near-persons").find(".media-list").find(".media-single").find(".send-message").on("click",function(){
        var user_id = $(this).parent().parent().find(".profile-popup").attr("data-id");
        send_dm_message(" ", true, false, true, button_text("send-message"), button_text("cancel-btn"), user_id);
    });
}


function send_teamrequest_to_near_persons() {
    $("#near-persons").find(".media-list").find(".media-single").find(".send-team-request").on("click", function () {

        var user_id = $(this).parent().parent().find(".profile-popup").attr("data-id");
        $.ajax({
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            },
            url: '/dashboard/profiles/dm-teamrequest/',
            method: 'post',
            data: {
                "user_id": user_id
            },
            success: function (data) {

            },
            complete: function (jqXHR, status) {
                if (status === "success") {
                    var data_json = $.parseJSON(jqXHR.responseText);
                    switch (data_json.message_type) {
                        case "success":
                            special_sweet_popup(dm_user_info(data_json.profile_address, data_json.profile_point, data_json.profile_image, data_json.profile_name, data_json.profile_location, data_json.position_title), user_id, true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), messageStr("request-successful-text-title"), messageStr("dm-request-successfull-text-content"), messageStr("request-cancel-message-title"), messageStr("dm-request-cancel-message-text-content"));
                            break;
                        case "warning":
                            classic_special_sweet_popup("", '<p class="fw-300 lh-15 text-light fs-16">' + data_json.message + '</p>', data_json.message_type, false, false, messageStr("db-warning-confirm-text"), "");
                            break;
                    }

                }
            }
        });


    });
}

function dm_message_userinfo(id) {
    return $.getJSON("/dashboard/profiles/dmmessage-person-info/?id=" + id, function (data) {
        $(".messenger-list").append('<div class="spinner-circle-shadow mx-auto"></div>');
    });
}
function dm_message_send(id, body_text) {

    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("body").find(".preloader").fadeIn();
        },
        url: '/dashboard/messages/send-message/',
        method: 'post',
        data: {
            "id": id,
            "body_text": body_text
        },
        success: function (data) {

        }
    });
}


function dm__select_projects() {
    return $.getJSON("/dashboard/profiles/dm-projects/", function (data) {
    });
}

/*dm modal window*/
function dm_setup_project_investments() {
    return $.getJSON("/dashboard/main/select-invest/", function (data) {
    });
}

function dm_select_investments(id, selected_value) {
    return $.getJSON("/dashboard/profiles/save-investments/?id=" + id + "&selected_value=" + selected_value, function (data) {


    });
}

function dm_select_project_hours(id, selected_value) {
    return $.getJSON("/dashboard/profiles/save-projecthours/?id=" + id + "&selected_value=" + selected_value, function (data) {

    });

}

function dm_get_project_supports(id) {
    return $.getJSON("/dashboard/profiles/dm-support-type/?id=" + id, function (data) {
    });
}

function dm_setup_project_hours() {
    return $.getJSON("/dashboard/main/select-hours/", function (data) {
    });
}

function dm_send_teamrequest(id, prj_id) {

    return $.getJSON("/dashboard/profiles/dm-send-request/?id=" + id + "&project=" + prj_id, function (data) {

    });
}

function second_stage_for_requested_person(requested_person) {
    return $.getJSON("/dashboard/profiles/dm-requested-person/?id=" + requested_person, function (data) {

    });
}

function send_answer_to_dm_request(id, investment, project_hour) {
    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("body").find(".preloader").fadeIn();
        },
        url: '/dashboard/profiles/send-answert-dm/',
        method: 'post',
        data: {
            "id": id,
            "investment": investment,
            "project_hour": project_hour
        },
        success: function (data) {

        }
    });


}

/*dm modal window*/

function myfriends() {
    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/dashboard/myprofile/myfriends/',
        method: 'post',
        data: {},
        success: function (data) {

        }
    });
}

function send_reply_message(body_text) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("body").find(".preloader").fadeIn();
        },
        url: '/dashboard/messages/send-reply/',
        method: 'post',
        data: {
            "message": id_return(),
            "body_text": body_text
        },
        success: function (data) {

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                $("body").find(".preloader").fadeOut();
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.message_type) {
                    case "success":
                        $("html").animate({scrollTop: 0}, 360);
                        $("#detail-msg-content").find(".card").first().before(message_detail_item(data_json.profile_name, data_json.profile_image, data_json.sended_date, data_json.message_content));
                        toastMessage(data_json.message, messageStr("ok-message"), "", "success");
                        $(document).find("#reply-form").find("#reply-content").summernote('code', '');
                        break;
                    case "error":
                        toastMessage(data_json.message, messageStr("ok-message"), "", "danger");
                        break;
                }

            }
        }
    });
}

function send_archive_messages(messages) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/dashboard/messages/send-archive/',
        method: 'post',
        data: {
            "messages": messages
        },
        success: function (data) {

            switch (data.message_type) {
                case "success":
                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    $(document).find("#messages-list").html('');
                    loadmore_inbox(1);
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
            }
        }
    });

}

function send_trash_messages(messages) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/dashboard/messages/send-trash/',
        method: 'post',
        data: {
            "messages": messages
        },
        success: function (data) {

            switch (data.message_type) {
                case "success":
                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    $(document).find("#messages-list").html('');
                    loadmore_inbox(1);
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
            }
        }
    });

}

function send_delete_messages(messages) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/dashboard/messages/send-delete/',
        method: 'post',
        data: {
            "messages": messages
        },
        success: function (data) {
            switch (data.message_type) {
                case "success":
                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    $(document).find("#messages-list").html('');
                    loadmore_inbox(1);
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
            }
        }
    });

}

function selected_delete_member(id) {
    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: '/dashboard/myteams/selected-member/',
        method: 'post',
        data: {
            "id": id
        },
        success: function (data) {

        }
    });
}

function delete_member_from_team(id) {
    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("body").find(".preloader").fadeIn();
        },
        url: '/dashboard/myteams/delete-member/',
        method: 'post',
        data: {
            "id": id,
            "project": id_return()
        },
        success: function (data) {

        }
    });
}

function delete_dm_request(id) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("body").find(".preloader").fadeIn();
        },
        url: '/dashboard/myteams/delete-dm/',
        method: 'post',
        data: {
            "id": id
        },
        success: function (data) {
            $("body").find(".preloader").fadeOut();
            switch (data.message_type) {
                case "success":
                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                    $(document).find("#my-requests").find(".dm-requests-table").find("tbody").html('');
                    myteam_mydm_requests(1, id_return());
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;

            }
        }

    });
}

function check_send_dm_message(id) {
    return $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

        },
        url: '/dashboard/messages/dm-person-info/',
        method: 'post',
        data: {
            "id": id
        },
        success: function (data) {

        }
    });
}