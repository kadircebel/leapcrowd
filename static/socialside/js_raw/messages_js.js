/**
 * Created by kadircebel on 12.05.2018.
 */
var messageList = function toastMessage() {
    var lang_code = $(document).find("#language").val();
    messageArray = [];
    message = {};
    switch (lang_code) {
        case "tr":
            message["similar-project-send"]="Websitesi adresi veya açıklama giriniz.";
            message["skill-message"] = "En fazla 20 adet beceri ekleyebilirsiniz.";
            message["ok-message"]="Tamam";
            message["list-result-message"]="Kişi listesi sonu veya üyelik paketiniz boyunca bu kadar kişiyi listeleyebilirsiniz.";
            message["there-is-no-item"]="Henüz fikir paylaşan kimse bulunmuyor.";
            message["project-result-message"]="Proje listesi sonu veya üyelik paketiniz boyunca bu kadar projeyi listeleyebilirsiniz.";
            message["project-item-feedback"]="Geri Bildirim Topluyor";
            message["project-no-item"]="Bu Filtrelemeye göre proje bulunamadı.";
            message["project-item-teamrequest"]="Takım İstekleri Topluyor";
            message["project-stage"]="Proje Aşaması";
            message["project-detail-linktext"]="Detay";
            message["project-option-linktext"]="Ayarlar";
            message["feedback-content-less"]="Bu kadar kısa bir geribildirimin maalesef kimseye yararı yok.";
            message["feedback-content-atleast"]="Hiçbir içerik girmediğiniz için geribildiriminizi yollayamadık";
            message["request-hour-text"]="sa./hafta";
            message["currency-type"]="₺";
            message["currency-hour"]="saat";
            message["request-no-item"]="Şu anda başka bir takım isteği bulunmuyor.";
            message["similar-projects"]="Şu anda başka bir benzer proje örneği bulunmuyor.";
            message["myprojects-no-item"]="Şu anda yayınlanmış olan başka bir projeniz bulunmuyor.";
            message["myproject-no-feedback"]="Şu anda mevcut proje için bir geri bildiriminiz bulunmuyor.";
            message["myproject-filter-no-item"]="Seçilen filtrelemede başka bir istek bulunmuyor";
            message["request-question-title"]="Seçilen kişilere takım isteği gönderilecek.";
            message["ideas-question-title"]="Bu fikrinizin bağlı olduğu projeyi seçiniz. Projeniz yok ise <u>yeni proje</u> seçeneğini seçiniz.";
            message["request-confirm-btn-text"]="Evet, gönder!";
            message["request-cancel-btn-text"]="Hayır, gönderme!";
            message["request-successful-text-title"]="Başarıyla gönderildi.";
            message["request-successful-text-content"]="Takım istekleriniz başarılı bir şekilde yanıtlandı.";
            message["dm-request-successfull-text-content"]="Takım isteğiniz başarılı bir şekilde iletildi.";
            message["request-cancel-message-title"]="İşlem iptal edildi.";
            message["request-cancel-message-text-content"]="Seçilen kişilere gönderim olmadı.";
            message["dm-request-cancel-message-text-content"]="Seçilen kişiye gönderim olmadı.";
            message["new-project-item"]="Yeni proje";
            message["select-project-item"]="İleri";
            message["team-ntfc-project-title"]="Aşağıda belirtilen projenize takım isteği aldınız.";
            message["team-ntfc-post-title"]="Aşağıda belirtilen fikrinize takım isteği aldınız.";
            message["nomore-notification"]="Tüm bildirimlerinizi görüntülüyorsunuz.";
            message["is-online"]="Yayında";
            message["draft"]="Taslak";
            message["cofunded-no-project"]="Henüz başka bir projede ortak olarak görev almıyorsunuz.";
            message["near-person-message"]="Şuan size yakın birileri bulunmuyor. Fikrinizi özet olarak paylaşıp, platform üzerinden gelecek olan takım isteklerini değerlendirmeniz daha mantıklı bir yol gibi görünüyor veya bütün bunlar ile uğraşmak istemiyorsanız ücretli üyeliklerimizden faydalanabilirsiniz.";
            message["project-teammates"]="Projede çalışan bir takım henüz bulunmuyor.";
            message["dashboard-teamrequest-btn"]="Takım İsteği Gönder";
            message["dashboard-message-btn"]="Mesaj Gönder";
            message["dm-request-question-title"]="Seçilen kişiye takım isteği gönderilecek.";
            message["dm-teamrequest-modal-title"]="Size bir projeye katılmanız için takım isteği gönderildi.";
            message["dm-button-text"]="Şartlarını Ayarla";
            message["user-supports"]="Destek Türü";
            message["user-hours"]="Saat/Hafta";
            message["option-text"]="Ayarla";
            message["dm-support-header-title"]="Destek Türü Seçenekleri";
            message["dm-warning-send-teamrequest-title"]="Gönderme İşlemi Başarısız";
            message["db-warning-confirm-text"]="Anladım";
            message["dm-add-btn"]="Bir proje seç";
            message["dm-project-select-title"]="Takım isteği gönderilecek projeyi seçiniz.";
            message["select-teamrequest-limit"]="Bulunduğunuz üyelikte konumunuz dışından başka bir seçim yapamazsınız.";
            message["send-message-title"]="Kime mesaj göndermek istiyorsunuz?";
            message["new-message-subject"]="Konu başlığı yazınız.";
            message["new-message-subject-title"]="Konu:";
            message["new-message-focus-text"]="Gönderilecek mesaj içeriğini giriniz...";
            message["myteam-result-message"]="Takımınızdaki tüm kişileri görüntülüyorsunuz.";
            message["delete-member-title"]="Emin misiniz?";
            message["delete-member-content"]="Bu kullanıcıyı takımdan çıkarmak üzeresiniz.";
            message["option-yes"]="Evet, eminim.";
            message["option-no"]="Hayır, iptal et.";
            message["delete-message-fail-title"]="İşlem Başarısız.";
            message["delete-message-success-title"]="İşlem Başarılı.";
            break;

        case "en":
            message["there-is-no-item"]="There isn't shares ideas yet.";
            message["delete-message-fail-title"]="İşlem Başarısız.";
            message["delete-message-success-title"]="İşlem Başarılı.";
            message["option-no"]="No, cancel it.";
            message["option-yes"]="Yes, I'm sure.";
            message["delete-member-title"]="Are you sure?";
            message["delete-member-content"]="You are about to remove this user from the team.";
            message["myteam-result-message"]="You are viewing all the members in your team.";
            message["new-message-subject-title"]="Subject:";
            message["new-message-subject"]="Subject...";
            message["new-message-focus-text"]="Enter a text...";
            message["send-message-title"]="Who do you want to send a message to?";
            message["select-teamrequest-limit"]="You can't select another person who is located outside your location in this membership package";
            message["dm-project-select-title"]="Select a project to which team requests will be sent.";
            message["dm-add-btn"]="Select a project";
            message["db-warning-confirm-text"]="Ok";
            message["dm-warning-send-teamrequest-title"]="Sending has failed.";
            message["dm-support-header-title"]="Support Type Options";
            message["option-text"]="Set";
            message["dm-button-text"]="Set Conditions";
            message["dm-teamrequest-modal-title"]="There is a team request sent for you to join the project.";
            message["dm-request-successfull-text-content"]="Your team request had been successfully sent.";
            message["dm-request-question-title"]="Team request will send to the selected person.";
            message["dashboard-message-btn"]="Send Message";
            message["dashboard-teamrequest-btn"]="Send Team Request";
            message["project-teammates"]="There is not a team exists in the project.";
            message["near-person-message"]="There are no people near you at the moment. Fikrinizi özet olarak paylaşıp, It seems more logical that you share your idea briefly and evaluate the team requests coming from the platform. Or you can benefit from our paid memberships.";
            message["cofunded-no-project"]="You are not a partner in another project yet.";
            message["draft"]="Draft";
            message["is-online"]="Online";
            message["nomore-notification"]="You are viewing all your notifications.";
            message["team-ntfc-project-title"]="You have received a team request to the following project.";
            message["team-ntfc-post-title"]="You have received a team request to the following idea.";
            message["select-project-item"]="Next";
            message["new-project-item"]="New project";
            message["request-cancel-message-text-content"]="Team requests have been canceled.";
            message["dm-request-cancel-message-text-content"]="Team request have been canceled.";
            message["request-cancel-message-title"]="It's canceled.";
            message["request-successful-text-content"]="Team requests were successfully answered.";
            message["request-successful-text-title"]="Successfully sent";
            message["request-cancel-btn-text"]="No, cancel it!";
            message["request-confirm-btn-text"]="Yes, send!";
            message["request-question-title"]="Team requests will send to the selected people.";
            message["ideas-question-title"]="Select the project that represents your idea. If there is no project for this idea, you should select <u>new project</u> option.";
            message["similar-project-send"]="You have to write website address or describe.";
            message["skill-message"] = "You can not add more than 20 skills.";
            message["ok-message"]="Okey";
            message["list-result-message"]="End of the list or you have to sign up paid membership.";
            message["project-result-message"]="End of the list or you have to sign up paid membership.";
            message["project-item-feedback"]="Collecting Feedbacks";
            message["project-item-teamrequest"]="Collecting Team Requests";
            message["project-stage"]="Business Stage";
            message["project-no-item"]="There is no project on this options.";
            message["project-detail-linktext"]="Detail";
            message["project-option-linktext"]="Options";
            message["feedback-content-less"]="It is not beneficial which so short feedback to no one. You must enter at least 112 characters.";
            message["feedback-content-atleast"]="We can't send your feedback because it's empty.";
            message["request-hour-text"]="h/w";
            message["currency-type"]="$";
            message["currency-hour"]="hour";
            message["user-supports"]="Support Type";
            message["user-hours"]="Hour/Week";
            message["request-no-item"]="There are no other team requests at the moment.";
            message["similar-projects"]="There is no other similar project information at the moment.";
            message["myprojects-no-item"]="You haven't any other published project right now.";
            message["myproject-no-feedback"]="You don't have any feedback on this project.";
            message["myproject-filter-no-item"]="There is no other team request in selected option.";

            break;
    }

    messageArray.push(message);
    return messageArray;
};

var messageStr = function getMessage(messageType) {
    var mesg = "";
    messageList().forEach(function (element) {
        mesg = element[messageType];
    });
    return mesg;
};

var button_text=function(button_slug){
    var lang_code = $(document).find("#language").val();
    var btn_str="";
    switch (button_slug) {
        case "send-team-request":
            if (lang_code==="tr"){
                btn_str="Takım İsteği Gönder";
            }
            else{
                btn_str="Send Team Request";
            }
            break;
        case "send-message":
            if(lang_code==="tr"){
                btn_str="Mesaj Gönder";
            }
            else{
                btn_str="Send Message";
            }
            break;
        case "connect":
            if(lang_code==="tr"){
                btn_str="Bağlan";
            }
            else{
                btn_str="Connect";
            }
            break;
        case "page-title":
            if(lang_code==="tr"){
                btn_str="Sayfa";
            }
            else{
                btn_str="Page";
            }
            break;
        case "more-load":
            if(lang_code==="tr"){
                btn_str="Daha Fazla";
            }
            else{
                btn_str="Load More";
            }
            break;
        case "post-save":
            if(lang_code==="tr"){
                btn_str="Kaydet";
            }
            else{
                btn_str="Save";
            }
            break;
        case "post-follow":
            if(lang_code==="tr"){
                btn_str="Takip Et";
            }
            else{
                btn_str="Follow";
            }
            break;
        case "post-complain":
            if(lang_code==="tr"){
                btn_str="Şikayet Et";
            }
            else{
                btn_str="Report";
            }
            break;

        case "post-edit":
            if(lang_code==="tr"){
                btn_str="Düzenle";
            }
            else{
                btn_str="Edit";
            }
            break;
        case "cancel-btn":
            if(lang_code==="tr"){
                btn_str="İptal Et";
            }
            else{
                btn_str="Cancel";
            }
            break;
        case "detail-btn":
            if(lang_code==="tr"){
                btn_str="Detay";
            }
            else{
                btn_str="Detail";
            }
            break;
        case "delete-member":
            if(lang_code==="tr"){
                btn_str="Sil";
            }
            else{
                btn_str="Delete";
            }
            break;
    }

    return btn_str;
};

function toastMessage(msg,action_title,action_url,action_color) {
    if (action_url.trim().length<1){
        action_url="javascript:void(0);";
    }

    app.toast(msg, {
        actionTitle: action_title,
        actionUrl: action_url,
        actionColor: action_color
    });
}

var member_type_html=function(member_type_str){
    switch (member_type_str){
        case "technical":
            return 'badge-dark m-0';
            break;
        case "entrepreneur":
            return 'badge-success';
            break;
        case "team":
            return 'badge-danger';
            break;
        default:
            return '';
            break;
    }
};

var member_type_text=function(member_type_str){
    var lang_code=$("#language").val();
    var member_txt="";
    switch (member_type_str){
        case "technical":
            if(lang_code==="tr"){
                member_txt='teknik kişi';
            }
            else{
                member_txt='technical';
            }
            break;
        case "entrepreneur":
            if(lang_code==="tr"){
                member_txt='girişimci';
            }
            else{
                member_txt='entrepreneur';
            }
            break;
        case "team":
            if(lang_code==="tr"){
                member_txt='takım lideri';
            }
            else{
                member_txt='team leader';
            }
            break;
        default:
            member_txt="";

            break;
    }
    return member_txt;
};

function sweet_message(message_str) {
    swal({
        type: 'error',
        title: 'Oops...',
        // text: message_str
        html: '<div class="media media-body"><p class="fw-300 text-light font-roboto fs-16">' + message_str + '</p></div>'
        // footer: '<a href>Why do I have this issue?</a>'
    })
}