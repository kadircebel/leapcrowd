/**
 * Created by kadircebel on 10.06.2018.
 */

function isEmailValid(emailAdress) {
    var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');

    return EMAIL_REGEXP.test(emailAdress);
}

var point_css = function progress_bar_css(score_value) {
    var css_value = "bg-danger";
    if (score_value <= 26) {
        css_value = "bg-danger";
    }
    else if (score_value > 26 && score_value < 100) {
        css_value = "bg-warning";
    }
    else {
        css_value = "bg-success";
    }
    return css_value;
};

var list_html = function (profile_id, profile_picture, profile_name, position_title, profile_location, profile_point, profile_member_type) {
    var html_str = '<div class="card b-1 hover-shadow-2 mb-20">' +
        '<div class="media card-body profile-popup" data-id="' + profile_id + '">' +
        '<div class="media-left pr-2 mr-0 mt-1">' +
        '<a href="#qv-user-details" data-toggle="quickview">' +
        '<img class="avatar avatar-xl" src="' + profile_picture + '" alt="...">' +
        '</a>' +
        '</div>' +
        '<div class="media-body">' +
        '<div class="mb-0">' +
        '<a href="#qv-user-details" data-toggle="quickview"><span ' +
        'class="fs-16 pr-16">' + profile_name + '</span></a>' +
        '</div>' +
        '<small class="text-fade fs-13 fw-300 text-capitalize">' + position_title + '</small>' +
        '<p>' +
        '<small class="text-lighter">' + profile_location + '</small>' +
        '</p>' +
        '</div>' +
        '<div class="media-right text-right d-none d-md-block">' +
        '<div data-provide="easypie" data-percent="' + profile_point + '" data-size="70" data-line-width="2">' +
        '<span class="easypie-data fs-14">' + profile_point + '%</span>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<footer class="card-footer flexbox align-items-center">' +
        '<div class="media media-single m-0 p-0"><span class="badge badge-ring ' + member_type_html(profile_member_type) + ' m-0"></span>' +
        '<small class="ml-1 mr-1 pl-1 pr-1 text-capitalize">' + member_type_text(profile_member_type) + '</small>' +
        '</div>' +
        '<div class="card-hover-show">' +
        '<a class="btn btn-xs fs-10 btn-bold btn-dark ml-1 send-team-request" href="javascript:void(0);">' + button_text('send-team-request') + '</a>' +
        '<a class="btn btn-xs fs-10 btn-bold btn-secondary ml-1 send-dm-message" href="javascript:void(0);" data-toggle="modal" data-target="#modal-contact">' + button_text('send-message') + '</a>' +
        '</div>' +
        '</footer>' +
        '</div>';

    return html_str;
};

function countCharacter(inputIdName, smallLength, bigLength) {
    var textarea = document.getElementById(inputIdName);
    textarea.addEventListener("input", function () {
        var maxlength = this.getAttribute("maxlength"); //150
        var currentLength = this.value.length;
        if (currentLength === maxlength) {
            $(textarea).next().closest(".lefted-character").html(maxlength - currentLength);
        }
        else {
            if (currentLength > smallLength && currentLength <= bigLength) {
                // $(textarea).next().closest(".lefted-character").removeClass("btn-danger").addClass("btn-secondary");
                $(textarea).next().closest(".lefted-character").show();
                $(textarea).next().closest(".lefted-character").html(maxlength - currentLength);

            }
            else if (currentLength > bigLength) {
                // $(textarea).next().closest(".lefted-character").removeClass("btn-secondary").addClass("btn-danger");
                $(textarea).next().closest(".lefted-character").show();
                $(textarea).next().closest(".lefted-character").html(maxlength - currentLength);
            }
            else {
                $(textarea).next().closest(".lefted-character").fadeOut();
                $(textarea).next().closest(".lefted-character").html('');
            }
        }
    });
}

var orj_str = function clear_character(str, character_count) {
    var sub_str = "";
    if (str.length > character_count) {
        sub_str = str.substring(0, 36);
        return sub_str;
    }
    else {
        return str;
    }
};

function projectNavMenu() {
    var selectedItem = $("#project-nav-menu").find(".nav-tabs").find(".nav-item").index();

    switch (selectedItem) {
        case 0:
            get_projectAreas();
            get_projectUsageTypes();
            upload_project_logo();
            projectNavMenuClick();
            break;
        case 1:

            break;
        case 2:

            break;
        case 3:
            break;
        case 4:

            break;
        case 5:

            break;

        default:

            break;
    }
}

function profileNavMenu() {
    var selectedItem = $("#profile-right-menu").find(".nav-tabs").find(".nav-item").index();
    switch (selectedItem) {
        case 0:
            upload_profile_img();
            //bu kısma daha sonra bak
            // countCharacter("summary", 300, 600);
            // countCharacter("project-summary", 300, 600);

            $(document).find("#tag-type").attr('data-provide', 'autofill tags');
            $("#saved-experiences").hide();
            profileNavMenuClick();
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:

            break;
        default:
            break;
    }
}


var similarproject = function similar_item(profile_img, profile_id, created_date, similar_content, similar_content_hyperlink, profile_name) {
    var html = '<li class="timeline-block">' +
        '<div class="timeline-point">' +
        '<a href="#qv-user-details" data-toggle="quickview" class="avatar profile-popup"                               data-id="' + profile_id + '">' + '<img src="' + profile_img + '" alt="' + profile_name + '">' +
        '</a>' +
        '</div>' +
        '<div class="timeline-content">' +
        '<time>' + created_date + '</time>' +
        '<p>' + similar_content + '</p>' +
        '<p>' + similar_content_hyperlink + '</p>' +
        '<hr>' +
        '</div>' +
        '</li>';

    return html;

};


var alertBox = function alert_box(messageStr, alert_color) {
    var html = '<div class="alert alert-' + alert_color + ' alert-dismissible fade show mb-35 fw-400 fs-12 pr-35" role="alert">' +
        '<button type="button" class="close cursor-pointer" data-dismiss="alert" aria-label="Close">' +
        '<span aria-hidden="true">×</span>' +
        '</button>' + messageStr +
        '</div>';

    return html;
};

function resize_ReEdit() {
    $(window).resize(function () {

        if ($(this).width() <= 812) {

            if (location.pathname.indexOf("/myideas/detail/") > -1) {
                $(".post-item").attr("data-target", "");
                $(".post-item").attr("data-toggle", "");
            }
            else {
                $("#users-ideas-list").find(".post-item").each(function () {
                    //bu kısımda id yi alıp ayrı bir sayfa açacağım
                    $(this).find(".card").find(".card-body").attr("data-target", "");
                    $(this).find(".card").find(".card-body").attr("data-toggle", "");

                });
            }


        }
        else {

            if (location.pathname.indexOf("/myideas/detail/") > -1) {
                $(".post-item").attr("data-target", "#qv-post-detail");
                $(".post-item").attr("data-toggle", "quickview");
                ideas_popup($(".post-item").attr("data-post"));
            }
            else if (location.pathname.indexOf("/ideas") > -1){
                $("#users-ideas-list").find(".post-item").each(function () {
                    $(this).find(".card").find(".card-body").attr("data-target", "#qv-post-detail");
                    $(this).find(".card").find(".card-body").attr("data-toggle", "quickview");
                    ideas_popup($(this).find(".card").find(".card-body").attr("data-post"));
                });
            }


        }

    });
}

function resizeAndEdit() {
    if ($(window).width() < 812) {

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(".post-item").attr("data-target", "");
            $(".post-item").attr("data-toggle", "");

        }
        else if (location.pathname.indexOf("/ideas/") > -1) {
            $("#users-ideas-list").find(".post-item").each(function () {
                //#qv-post-detail
                $(this).find(".card").find(".card-body").attr("data-target", "");
                //quickview
                $(this).find(".card").find(".card-body").attr("data-toggle", "");
            });
        }


    }
    resize_ReEdit();
}




var isHTML = RegExp.prototype.test.bind(/(<([^>]+)>)/i);

var content_html_render = function (html_str) {
    var outerHtml = "";
    $.each(html_str, function (index, value) {

        if ($(value).find("iframe").length > 0) {

            $(value).find("iframe").addClass("pr-5 py-3 pl-0");
            outerHtml += "<div class='embed-responsive embed-responsive-16by9'>" + value.innerHTML + "</div>";

        }
        else if ($(value).find("img").length > 0) {

            $(value).find("img").addClass("figure-img img-fluid");
            outerHtml += "<p class='py-2'>" + value.innerHTML + "</p>";
        }
        else {
            if ($(value).text() === "") {
                outerHtml += "";
            }
            else {
                outerHtml += '<p class="py-2">' + value.innerHTML + '</p>';
            }
        }
    });

    return outerHtml;

};

var content_html = function content_html(page_content) {
    var htmlStr = "";
    if (isHTML(page_content)) {

        htmlStr = $.parseHTML(page_content);
        htmlStr = content_html_render(htmlStr);
    }
    else {

        htmlStr = '<p class="py-2">' + page_content + '</p>';
    }

    return htmlStr;

};

var ideas_list_item = function ideasListItem(post_id, profile_picture, profile_username, created_date, position_title, post_content, like_count, teamrequest_count, smilarproject_count, answer_count, profile_id) {
    var html_str = '<div class="col-md-12 col-lg-12 post-item" data-post="' + post_id + '">' +
        '<div class="card card-shadowed ">' +
        '<div class="media bb-1 border-fade profile-popup" data-id="' + profile_id + '">' +
        '<a href="#qv-user-details" data-toggle="quickview"><img class="avatar avatar-lg" src="' + profile_picture + '" alt="..."></a>' +
        '<div class="media-body">' +
        '<p>' +
        '<a href="#qv-user-details" class="hover-primary" data-toggle="quickview"><strong class="fs-14">' + profile_username + '</strong></a>' +
        '<time class="float-right text-lighter" datetime="' + created_date + '">' + created_date + '</time>' +
        '</p>' +
        '<p class="text-capitalize">' +
        '<small>' + position_title + '</small>' +
        '</p>' +
        '</div>' +
        '</div>' +
        '<div class="card-body bb-1 border-fade cursor-pointer" data-target="#qv-post-detail"                 data-toggle="quickview" ' + content_html(post_content) +
        '<div class="gap-items-4 mt-2">' +
        '<i class="fa fa-heart mr-1"></i> ' + like_count +
        '<i class="fa fa-user-plus mr-1"></i> ' + teamrequest_count +
        '<i class="fa fa-newspaper-o mr-1"></i> ' + smilarproject_count +
        '<i class="fa fa-comments-o mr-1"></i> ' + answer_count +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    return html_str;
};


var project_option = "";
var project_stage = "";

$(document).find("#filter-projects").find(".custom-radio").each(function () {
    $(this).find("input[type='radio']").change(function () {
        project_option = $(this).val();
    });
});

$(document).find("#business-stage-options").change(function () {
    project_stage = $(this).val();
});

var project_item_html = function prj_item(prj_name, prj_logo, prj_location, prj_id, is_teamrequest, is_feedback, buss_stage) {
    var feedbackHtml = "";
    var teamrequestHtml = "";
    if (is_feedback === true) {
        feedbackHtml = '<span class="badge badge-info text-uppercase no-radius ls-1 opacity-75">' +
            messageStr("project-item-feedback") + '</span>';
    }
    else {
        feedbackHtml = '';
    }
    if (is_teamrequest === true) {
        teamrequestHtml = '<span class="badge badge-info text-uppercase no-radius ls-1 opacity-75">' +
            messageStr("project-item-teamrequest") + '</span>';
    }
    else {
        teamrequestHtml = '';
    }

    var html = '<div class="col-md-12 col-lg-12"><div class="card b-1 hover-shadow-2 mb-20">' +
        '<a class="media card-body cursor-default" href="javascript:void(0);">' +
        '<div class="media-left pr-12">' +
        '<img class="avatar avatar-xl no-radius" src="' + prj_logo + '" alt="...">' +
        '</div>' +
        '<div class="media-body">' +
        '<div class="mb-2">' +
        '<span class="fs-20 pr-16 text-capitalize">' + prj_name + '</span>' +
        '</div>' +
        feedbackHtml + " " +
        teamrequestHtml +
        '</div>' +
        '<div class="media-right text-right d-none d-md-block">' +
        '<p class="text-fade">' +
        '<small><i class="fa fa-map-marker pr-1 text-capitalize"></i> ' + prj_location + '</small>' +
        '</p>' +
        '</div>' +
        '</a>' +
        '<footer class="card-footer flexbox align-items-center">' +
        '<div>' +
        '<strong>' + messageStr("project-stage") + ':</strong>' +
        '<span>' + buss_stage + '</span>' +
        '</div>' +
        '<div class="card-hover-show">' +
        '<a class="btn btn-xs fs-10 btn-bold btn-dark" href="/dashboard/projects/detail/' + prj_id + '/">' + messageStr("project-detail-linktext") + '</a>' +
        '</div>' +
        '</footer>' +
        '</div></div>';

    return html;
};

var project_request_item_html = function prj_rqst_item(prj_name, prj_logo, prj_location, prj_id, is_teamrequest, is_feedback, buss_stage, is_online, url_type) {
    var feedbackHtml = "";
    var teamrequestHtml = "";
    var isOnlineHtml = "";
    var urlStr = "";
    var btnText = "";

    switch (url_type) {
        case "project":
            urlStr = '/dashboard/myprojects/edit/' + prj_id + '/';
            btnText = messageStr("project-option-linktext");
            break;
        case "teamrequest":
            urlStr = '/dashboard/myprojects/requests/' + prj_id + '/';
            btnText = messageStr("project-detail-linktext");
            break;
        case "cofunded":
            urlStr = '/dashboard/projects/detail/' + prj_id + '/';
            btnText = messageStr("project-detail-linktext");
            break;
    }


    if (is_feedback === true) {
        feedbackHtml = '<span class="badge badge-info text-uppercase no-radius ls-1 opacity-75">' +
            messageStr("project-item-feedback") + '</span>';
    }
    else {
        feedbackHtml = '';
    }
    if (is_teamrequest === true) {
        teamrequestHtml = '<span class="badge badge-info text-uppercase no-radius ls-1 opacity-75">' +
            messageStr("project-item-teamrequest") + '</span>';
    }
    else {
        teamrequestHtml = '';
    }

    if (is_online === true) {
        isOnlineHtml = '<span class="badge badge-success">' + messageStr("is-online") + '</span>';
    }
    else {
        isOnlineHtml = '<span class="badge badge-danger">' + messageStr("draft") + '</span>';
    }

    var html = '<div class="card b-1 hover-shadow-2 mb-20">' +
        '<a class="media card-body cursor-default" href="javascript:void(0);">' +
        '<div class="media-left pr-12">' +
        '<img class="avatar avatar-xl no-radius" src="' + prj_logo + '" alt="...">' +
        '</div>' +
        '<div class="media-body">' +
        '<div class="mb-2">' +
        '<span class="fs-20 pr-16 text-capitalize">' + prj_name + '</span>' +
        '</div>' +
        feedbackHtml + " " +
        teamrequestHtml +
        '</div>' +
        '<div class="media-right text-right d-none d-md-block">' +
        '<p class="text-fade">' +
        '<small><i class="fa fa-map-marker pr-1 text-capitalize"></i> ' + prj_location + '</small>' +
        '</p>' + isOnlineHtml +
        '</div>' +
        '</a>' +
        '<footer class="card-footer flexbox align-items-center">' +
        '<div>' +
        '<strong>' + messageStr("project-stage") + ':</strong>' +
        '<span>' + buss_stage + '</span>' +
        '</div>' +
        '<div class="card-hover-show">' +
        '<a class="btn btn-xs fs-10 btn-bold btn-dark" href="' + urlStr + '">' + btnText + '</a>' +
        '</div>' +
        '</footer>' +
        '</div>';

    return html;
};


var city_str = "";
var country_str = "";
var unanswered_str = "";
var all_str = "";

$(document).find("#filter-ideas").find("label").each(function () {
    $(this).find("input[type='checkbox']").change(function () {
        var selected_option = $(this).data("option");

        switch (selected_option) {
            case "city":
                if ($(this).is(":checked")) {

                    city_str = $(this).val();
                    all_str = "";
                    country_str = "";

                    $(document).find("#filter-ideas").find("label:nth-child(2)").find("input[type='checkbox']").prop("checked", false);
                    $(document).find("#filter-ideas").find("label:last-child").find("input[type='checkbox']").prop("checked", false);
                }
                else {
                    city_str = "";
                }

                break;
            case "country":
                if ($(this).is(':checked')) {
                    country_str = $(this).val();
                    $(document).find("#filter-ideas").find("label:first-child").find("input[type='checkbox']").prop("checked", false);
                    $(document).find("#filter-ideas").find("label:last-child").find("input[type='checkbox']").prop("checked", false);
                    // filter_options["city"]="";
                    city_str = "";
                    all_str = "";
                }
                else {
                    country_str = "";
                }
                break;

            case "unanswered":
                if ($(this).is(':checked')) {
                    unanswered_str = $(this).val();
                    $(document).find("#filter-ideas").find("label:last-child").find("input[type='checkbox']").prop("checked", false);
                }
                else {
                    unanswered_str = "";
                }
                break;
            case "all":
                if ($(this).is(':checked')) {
                    all_str = $(this).val();
                    unanswered_str = "";
                    country_str = "";
                    city_str = "";
                    $(document).find("#filter-ideas").find("label").find("input[type='checkbox']").prop("checked", false);
                    $(document).find("#filter-ideas").find("label:last-child").find("input[type='checkbox']").prop("checked", true);
                }
                else {
                    all_str = "";
                }
                break;
            default:
                break;
        }

    });
});


var post_contents = function header_post(profile_image, profile_username, created_date, position_title, post_content, like_count, answer_count, is_edit) {
    var html = '<div class="media bb-1 border-fade">' +
        '<a class="avatar avatar-lg" href="javascript:void(0);">' +
        '<img src="' + profile_image + '" alt="' + profile_username + '">' +
        '</a>' +
        '<div class="media-body">' +
        '<p>' +
        '<a href="javascript:void(0);" class="text-capitalize"><strong class="fs-14">' + profile_username + '</strong></a>' +
        '<time class="float-right text-lighter" datetime="' + created_date + '">' + created_date + '</time>' +
        '</p><p class="text-capitalize">' +
        '<small>' + position_title + '</small>' +
        '</p></div></div>' +
        '<div class="card-body bb-1 border-fade">' + '<div class="content-container">' + content_html(post_content) + '</div>' + post_buttons(like_count, answer_count, "main-answer", is_edit) + '</div>';

    return html;
};

var post_buttons = function post_button_goup(like_count, answer_count, answer_type, is_edit) {
    var answerClassName = "";
    // var edit_html_btn="";
    var html = "";
    switch (answer_type) {
        case "main-answer":
            answerClassName = "main-answer-btn";
            break;
        case "sub-answer":
            answerClassName = "sub-answer-btn";
            break;
        default:
            break;
    }

    switch (is_edit) {
        case true:
            html = '<div class="flexbox align-items-center mt-2 post-buttons">' +
                '<div class="gap-items-4 fs">' +
                '<a class="text-lighter hover-danger like-this" href="javascript:void(0);">' +
                '<i class="fa fa-heart pr-1"></i> ' + like_count +
                '</a>' +
                '<a class="text-lighter hover-dark ' + answerClassName + '" href="javascript:void(0);" data-toggle="modal" data-target="#modal-answer">' +
                '<i class="fa fa-comments-o pr-1"></i> ' + answer_count +
                '</a>' +
                '</div>' +

                '<div class="dropdown">' +
                '<a data-toggle="dropdown" href="javascript:void(0);" aria-expanded="false"><i class="ti-more-alt rotate-90 text-muted"></i></a>' +
                '<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">' +
                '<a class="dropdown-item edit-this" href="javascript:void(0);" data-toggle="modal" data-target="#modal-edit"><i class="fa fa-edit"></i>' + button_text("post-edit") + '</a>' +

                '<a class="dropdown-item text-fadest" href="javascript:void(0);"><i class="fa fa-bookmark-o"></i> ' + button_text("post-save") + '</a>' +

                '<a class="dropdown-item text-fadest" href="javascript:void(0);"><i class="fa fa-check-circle-o"></i> ' + button_text("post-follow") + '</a>' +

                '<div class="dropdown-divider"></div>' +
                '<a class="dropdown-item text-fadest" href="javascript:void(0);"><i class="fa fa-times-circle-o"></i> ' + button_text("post-complain") + '</a>' +
                '</div>' +
                '</div></div>';


            break;
        case false:
            html = '<div class="flexbox align-items-center mt-2 post-buttons">' +
                '<div class="gap-items-4 fs">' +
                '<a class="text-lighter hover-danger" href="javascript:void(0);">' +
                '<i class="fa fa-heart pr-1"></i> ' + like_count +
                '</a>' +
                '<a class="text-lighter hover-dark ' + answerClassName + '" href="javascript:void(0);" data-toggle="modal" data-target="#modal-answer">' +
                '<i class="fa fa-comments-o pr-1"></i> ' + answer_count +
                '</a>' +
                '</div>' +

                '<div class="dropdown">' +
                '<a data-toggle="dropdown" href="javascript:void(0);" aria-expanded="false"><i class="ti-more-alt rotate-90 text-muted"></i></a>' +
                '<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end">' +
                '<a class="dropdown-item text-fadest" href="javascript:void(0);"><i class="fa fa-bookmark-o"></i> ' + button_text("post-save") + '</a>' +
                '<a class="dropdown-item text-fadest" href="javascript:void(0);"><i class="fa fa-check-circle-o"></i> ' + button_text("post-follow") + '</a>' +
                '<div class="dropdown-divider"></div>' +
                '<a class="dropdown-item text-fadest" href="javascript:void(0);"><i class="fa fa-times-circle-o"></i> ' + button_text("post-complain") + '</a>' +
                '</div>' +
                '</div></div>';
            break;
    }


    return html;

};

var post_main_answers = function (profile_image, profile_username, created_date, position_title, post_content, like_count, answer_count, users_answers_id, subHtmlStr, profile_id, page_type, is_edit) {
    var html_answers = '';

    if (page_type === "detail-page") {

        html_answers = '<div class="media main-answers" data-answers="' + users_answers_id + '" data-name="' + profile_username + '">' +
            '<a class="avatar avatar-lg profile-popup" data-id="' + profile_id + '" href="#qv-user-details" data-toggle="quickview">' +
            '<img src="' + profile_image + '" alt="' + profile_username + '"/>' +
            '</a>' +
            '<div class="media-body">' +
            '<p>' +
            '<a href="#qv-user-details" data-toggle="quickview" data-id="' + profile_id + '" class="pr-1 hover-primary text-capitalize profile-popup"><strong>' + profile_username + '</strong></a>' +
            '<time class="float-right text-fade" datetime="' + created_date + '">' + created_date + '</time>' +
            '</p>' +
            '<p class="text-capitalize">' +
            '<small>' + position_title + '</small>' +
            '</p>' + '<div class="content-container">' + content_html(post_content) + '</div>' + post_buttons(like_count, answer_count, "sub-answer", is_edit)
            + subHtmlStr +
            '</div>' +
            '</div>';
    }
    else {
        html_answers = '<div class="media main-answers" data-answers="' + users_answers_id + '" data-name="' + profile_username + '">' +
            '<a class="avatar avatar-lg" href="javascript:void(0);">' +
            '<img src="' + profile_image + '" alt="' + profile_username + '"/>' +
            '</a>' +
            '<div class="media-body">' +
            '<p>' +
            '<a href="javascript:void(0);" class="pr-1 hover-primary text-capitalize"><strong>' + profile_username + '</strong></a>' +
            '<time class="float-right text-fade" datetime="' + created_date + '">' + created_date + '</time>' +
            '</p>' +
            '<p class="text-capitalize">' +
            '<small>' + position_title + '</small>' +
            '</p>' + '<div class="content-container">' + content_html(post_content) + '</div>' + post_buttons(like_count, answer_count, "sub-answer", is_edit)
            + subHtmlStr +
            '</div>' +
            '</div>';
    }


    return html_answers;
};

var main_answer_container = function main_answers_container(html_str) {

    var html = '<div class="media-list media-list-divided bg-lighter" id="post-answers">' + html_str + '</div>';
    return html;


};

var post_sub_answers = function (profile_img, profile_username, created_date, position_title, post_content, like_count, answer_count, users_answers_s_id, profile_id, page_type, is_edit, id) {
    //******** BU KISIMDA EĞER CEVABA CEVAP VERİLDİYSE ONA GÖRE DOLU VEYA BOŞ DÖNDÜRMELİYİZ.
    var html = '';
    if (page_type === "detail-page") {
        html = '<div class="media px-0" data-id="' + id + '" data-answers="' + users_answers_s_id + '" data-name="' + profile_username + '">' +
            '<a class="avatar avatar-lg profile-popup" href="#qv-user-details" data-toggle="quickview" data-id="' + profile_id + '">' +

            '<img src="' + profile_img + '" alt="' + profile_username + '">' +
            '</a>' +
            '<div class="media-body">' +

            '<p>' +
            '<a href="#qv-user-details" data-toggle="quickview"  data-id="' + profile_id + '" class="hover-primary text-capitalize profile-popup"><strong>' + profile_username + '</strong></a>' +
            '<time class="float-right text-fade" datetime="' + created_date + '">' + created_date + '</time>' +
            '</p>' +
            '<p>' +
            '<small class="text-capitalize">' + position_title + '</small>' +
            '</p>' +
            '<div class="content-container">' +
            content_html(post_content) + '</div>' + post_buttons(like_count, answer_count, "sub-answer", is_edit) +
            '</div>' +
            '</div>';
    }
    else {
        html = '<div class="media px-0" data-id="' + id + '" data-answers="' + users_answers_s_id + '" data-name="' + profile_username + '">' +
            '<a class="avatar avatar-lg" href="javascript:void(0);">' +
            '<img src="' + profile_img + '" alt="' + profile_username + '">' +
            '</a>' +
            '<div class="media-body">' +
            '<p>' +
            '<a href="profile.html" class="hover-primary text-capitalize"><strong>' + profile_username + '</strong></a>' +
            '<time class="float-right text-fade" datetime="' + created_date + '">' + created_date + '</time>' +
            '</p>' +
            '<p>' +
            '<small class="text-capitalize">' + position_title + '</small>' +
            '</p>' + '<div class="content-container">' +
            content_html(post_content) + '</div>' + post_buttons(like_count, answer_count, "sub-answer", is_edit) +
            '</div>' +
            '</div>';
    }


    return html;
};

var notification_item_team = function item_team(profile_img, profile_name, created_date, message_str, is_read, redirect_url, request_type, id) {
    var html = '';
    var typeStr = '';
    var is_new = 'media-new1';


    if (request_type === "dm-teamrequest") {
        typeStr = "item-dm";
    }
    else {
        typeStr = "";
    }

    if (is_read === false) {
        is_new = 'media-new';

    }
    else {
        is_new = 'media-new1';
    }

    html = '<a class="media item-post ' + is_new + ' ' + typeStr + '" href="' + redirect_url + '" data-id="' + id + '">' +
        '<span class="avatar mt-2">' +
        '<img src="' + profile_img + '" alt="' + profile_name + '">' +
        '</span>' +
        '<div class="media-body">' +
        '<p class="text-capitalize"><strong>' + profile_name + '</strong>' +
        '<time class="float-right">' + created_date + '</time>' +
        '</p>' +
        '<p class="fs-12">' + message_str + '</p>' +
        '</div>' +
        '</a>';

    return html;
};


function formatAsMoney(mnt) {
    mnt -= 0;
    mnt = (Math.round(mnt * 100)) / 100;
    return (mnt === Math.floor(mnt)) ? mnt + '.00'
        : ( (mnt * 10 === Math.floor(mnt * 10)) ?
            mnt + '0' : ( (mnt * 100 === Math.floor(mnt * 100)) ? mnt + '0' : mnt)    );
}

var teamrequest_item = function teamrequest_itm(profile_img, profile_name, profile_id, position_title, support_type, hour_info, hour_cost, location) {

    var html = "";

    if (hour_cost.length > 0) {

        html = '<div class="col-md-6 col-lg-4 col-sm-6 col-12">' +
            '<div class="card">' +
            '<div class="flexbox align-items-center px-20 pt-20">' +
            '<label class="custom-control custom-checkbox" >' +
            '<input type="checkbox" class="custom-control-input" value="' + profile_id + '" >' +
            '<span class="custom-control-indicator"></span>' +
            '<span class="custom-control-description"></span>' +
            '</label>' +
            '</div>' +
            '<div class="card-body text-center pt-1 pb-20">' +
            '<a href="#qv-user-details" class="profile-popup" data-id="' + profile_id + '" data-toggle="quickview">' +
            '<img class="avatar avatar-xxl" src="' + profile_img + '">' +
            '</a>' +
            '<h5 class="mt-2 mb-0"><a class="hover-primary profile-popup text-capitalize" data-id="' + profile_id + '" href="#qv-user-details" data-toggle="quickview">' + profile_name + '</a></h5>' +
            '<span class="text-capitalize">' + position_title + '</span>' +
            '<div class="my-15">' +
            '<span class="bg-lighter d-inline-block text-center px-3">' + hour_info + ' ' + messageStr('request-hour-text') + '  </span>' +
            '</div>' +
            '<span class="bg-lighter d-inline-block text-center px-3">' + support_type + '</span>' +
            '</div>' +
            '<footer class="card-footer flexbox">' +
            '<div>' +
            '<i class="fa fa-map-marker pr-1"></i>' +
            '<span>' + location + '</span>' +
            '</div>' +
            '<div><i class="fa fa-money pr-1"></i>' +
            '<span>' + hour_cost + '/' + messageStr("currency-hour") + '</span>' +
            '</div>' +
            '</footer>' +
            '</div>' +
            '</div>';
    }
    else {
        html = '<div class="col-md-6 col-lg-4 col-sm-6 col-12">' +
            '<div class="card">' +
            '<div class="flexbox align-items-center px-20 pt-20">' +
            '<label class="custom-control custom-checkbox">' +
            '<input type="checkbox" class="custom-control-input" value="' + profile_id + '">' +
            '<span class="custom-control-indicator"></span>' +
            '<span class="custom-control-description"></span>' +
            '</label>' +
            '</div>' +
            '<div class="card-body text-center pt-1 pb-20">' +
            '<a href="#qv-user-details" class="profile-popup" data-id="' + profile_id + '" data-toggle="quickview">' +
            '<img class="avatar avatar-xxl" src="' + profile_img + '">' +
            '</a>' +
            '<h5 class="mt-2 mb-0"><a class="hover-primary profile-popup text-capitalize" data-id="' + profile_id + '" href="#qv-user-details" data-toggle="quickview">' + profile_name + '</a></h5>' +
            '<span class="text-capitalize">' + position_title + '</span>' +
            '<div class="my-15">' +
            '<span class="bg-lighter d-inline-block text-center px-3">' + hour_info + ' ' + messageStr('request-hour-text') + '  </span>' +
            '</div>' +
            '<span class="bg-lighter d-inline-block text-center px-3">' + support_type + '</span>' +
            '</div>' +
            '<footer class="card-footer flexbox">' +
            '<div>' +
            '<i class="fa fa-map-marker pr-1"></i>' +
            '<span>' + location + '</span>' +
            '</div>' +
            '</footer>' +
            '</div>' +
            '</div>';
    }


    return html;
};


var feedback_main_item = function feedback_main(list_item) {
    var mainHtml = '<div class="card card-body card-shadowed "><ol class="timeline timeline-activity timeline-sm timeline-content-right w-100 py-20 pr-20">' + list_item + '</ol></div>';
    return mainHtml;
};

var feedback_item = function feedback_list_item(profile_img, profile_id, profile_name, created_date, content_text) {

    var html = '<li class="timeline-block">' +
        '<div class="timeline-point">' +
        '<a href="#qv-user-details" data-toggle="quickview" class="avatar profile-popup" data-id="' + profile_id + '">' +
        '<img src="' + profile_img + '" alt="' + profile_name + '">' +
        '</a>' +
        '</div>' +
        '<div class="timeline-content">' +
        '<time>' + created_date + '</time>' +
        '<p>' + content_text + '</p>' +
        '<hr>' +
        '</div>' +
        '</li>';

    return html;

};

var all_myprj_requests = [];
function myprj_requests() {
    var team_person_limit = 0;
    var more_person_limit = 0;
    var my_location = "";


    user_membeships().done(function (data) {

        $(document).find("#users-requests").find(".project-requests-lists").before(alertBox(data.message, "info"));
        team_person_limit = data.team_person;
        more_person_limit = data.more_person;
        // my_location = data.my_location;

    });

    $(document).find("#users-requests").find(".project-requests-lists").find("div").each(function () {
        $(this).find(".card").find(".custom-checkbox :checkbox").change(function () {

            if ($(this).is(":checked")) {
                if (all_myprj_requests.length < team_person_limit || team_person_limit === -1) {
                    if ($.inArray($(this).val(), all_myprj_requests) === -1) {
                        all_myprj_requests.push($(this).val());
                    }
                }
                else {
                    if (location_info($(this).val(), "project", id_return())) {

                        if (same_members(all_myprj_requests, "project", id_return()) < more_person_limit) {
                            if ($.inArray($(this).val(), all_myprj_requests) === -1) {
                                all_myprj_requests.push($(this).val());
                            }
                        }
                        else {
                            $(this).prop("checked", false);
                        }
                    }
                    else {
                        $(this).prop("checked", false);
                    }


                }
            }
            else {
                all_myprj_requests.splice(all_myprj_requests.indexOf($(this).val()), 1);

            }

        });
    });

}

var all_myideas_requests = [];

function myideas_requests() {
    var team_person_limit = 0;
    var more_person_limit = 0;
    // var my_location = "";

    user_membeships().done(function (data) {
        $(document).find("#tab-team").find(".requests-list").before(alertBox(data.message, "info"));
        team_person_limit = data.team_person;
        more_person_limit = data.more_person;
        // my_location = data.my_location;
    });

    $(document).find("#tab-team").find(".requests-list").find("div").each(function () {
        $(this).find(".card").find(".custom-checkbox :checkbox").unbind("change").change(function () {

            if ($(this).is(":checked")) {
                //seçildi
                if (all_myideas_requests.length < team_person_limit || team_person_limit === -1) {
                    if ($.inArray($(this).val(), all_myideas_requests) === -1) {
                        all_myideas_requests.push($(this).val());
                    }
                }
                else {
                    if (location_info($(this).val(), "post", id_return())) {

                        if (same_members(all_myideas_requests, "post", id_return()) < more_person_limit) {
                            if ($.inArray($(this).val(), all_myideas_requests) === -1) {
                                all_myideas_requests.push($(this).val());
                            }
                        }
                        else {
                            $(this).prop("checked", false);
                        }
                    }
                    else {
                        $(this).prop("checked", false);
                    }


                }
            }
            else {
                all_myideas_requests.splice(all_myideas_requests.indexOf($(this).val()), 1);
            }
        });

    });
}


var choice_str = "";
$(document).find("#filter-profiles").find(".custom-controls-stacked").find(".custom-radio").each(function () {
    $(this).find("input[type='radio']").change(function () {
        choice_str = $(this).val();
    });
});


var project_list = function prj_list_item(project_name, project_id) {
    var html = '<label class="custom-control custom-control-dark b-1 border-light p-2 custom-radio">' +
        '<input type="radio" class="custom-control-input" name="radio1" data-id="' + project_id + '">' +
        '<span class="custom-control-indicator"></span>' +
        '<span class="custom-control-description">' + project_name + '</span>' +
        '</label>';

    return html;
};

var avatar_list_str = function users_avatars(profile_img, profile_name) {
    var html = '<a class="avatar avatar-lg" href="javascript:void(0);" title="' + profile_name + '"><img src="' + profile_img + '"></a>';
    return html;
};


var dm_person_item = function requested_person_item(profile_name, position_title, profile_image, profile_location) {
    var html = '<div class="card card-body text-center py-20 shadow-0">' +
        '<a href="javascript:void(0);">' +
        '<img class="avatar avatar-xl" src="' + profile_image + '">' +
        '</a>' +
        '<h5 class="mt-3 mb-2 fs-18"><a class="hover-primary" href="javascript:void(0);">' + profile_name + '</a></h5>' +
        '<p class="text-fade text-capitalize fs-15">' + position_title + '</p>' +
        '<p class="text-fade text-capitalize fs-14">' + profile_location + '</p>' +
        '</div>';

    return html;
};

var dm_user_info = function user_info(profile_address, profile_point, profile_image, profile_name, profile_location, position_title) {
    var dataColor = "";
    if (profile_point <= 26) {
        dataColor = "#f96868";
    }
    else if (profile_point > 26 && profile_point < 100) {
        dataColor = "#f2a654";
    }
    else {
        dataColor = "#46be8a";
    }

    var html = '<div class="card card-body text-center py-25 shadow-0 b-0 mb-0">' +
        '<a href="' + profile_address + '">' +
        '<div data-provide="easypie" data-percent="' + profile_point + '" data-color="' + dataColor + '" data-scale-color="#465161" data-size="100" data-line-width="2">' +
        '<span class="easypie-data lead fw-bold">' +
        '<img class="avatar avatar-xxl" src="' + profile_image + '">' +
        '</span>' +
        '</div>' +
        '</a>' +
        '<h5 class="mt-3 mb-2"><a class="hover-primary text-default" href="' + profile_address + '">' + profile_name + '</a></h5>' +
        '<p class="text-capitalize fs-13">' + position_title + '</p>' +
        '<p class="text-fade fs-12 profile-location">' + profile_location + '</p>' +
        '</div>';

    return html;
};


var dm_project_item = function dm_project_item(project_title, project_content, project_address, project_logo, project_location, project_point) {
    var dataColor = "";
    if (project_point <= 26) {
        dataColor = "#f96868";
    }
    else if (project_point > 26 && project_point < 100) {
        dataColor = "#f2a654";
    }
    else {
        dataColor = "#46be8a";
    }


    var html = '<div class="card card-body text-center py-25 shadow-0 b-0">' +
        '<a href="' + project_address + '">' +
        '<div data-provide="easypie" data-percent="' + project_point + '" data-color="' + dataColor + '" data-scale-color="#465161" data-size="100" data-line-width="2">' +
        '<span class="easypie-data lead fw-bold">' +
        '<img class="avatar avatar-xxl" src="' + project_logo + '">' +
        '</span>' +
        '</div>' +
        '</a>' +
        '<h5 class="mt-3 mb-2"><a class="hover-primary text-default" href="' + project_address + '">' + project_title + '</a></h5>' +
        '<p class="text-fade fs-12">' + project_location + '</p>' +
        '<p class="text-fade lh-15 fs-14 project-content">' + project_content + '</p>' +
        '<div class="gap-items"><button type="button" class="btn btn-outline btn-danger">' + messageStr("dm-button-text") + '</button></div>' +
        '</div>';

    return html;
};

var user_support_container = function support_container(user_hours, investment_type, hours_value, investment_value) {
    var html = '<ul class="flexbox flex-justified text-center dm-user-supports my-35 bt-1 bb-1 py-25">' +
        '<li class="br-1 border-light">' +
        '<div class="fs-16 selected-hour" data-hour="' + hours_value + '">' + user_hours + ' </div>' +
        '<small>' + messageStr("user-hours") + '</small>' +
        '<div class="flexbox flex-justified text-center">' +
        '<a href="javascript:void(0);" class="btn btn-bold btn-pure btn-danger text-uppercase set-hours">' + messageStr("option-text") + '</a>' +
        '</div>' +
        '</li>' +
        '<li>' +
        '<div class="fs-16 text-capitalize selected-investment" data-investment="' + investment_value + '">' + investment_type + '</div>' +
        '<small>' + messageStr("user-supports") + '</small>' +
        '<div class="flexbox flex-justified text-center">' +
        '<a href="javascript:void(0);" class="btn btn-bold btn-pure btn-danger text-uppercase set-invests">' + messageStr("option-text") + '</a>' +
        '</div>' +
        '</li>' +
        '</ul>';

    return html;
};

var dm_sweet_popup = function dm_sweet_teamrequest(dm_id, modal_text, showCancelBtn, showCloseBtn, confirmBtnText, cancelBtnText, confirmMessageTitle, confirmMessageContent, cancelMessageTitle, cancelMessageContent) {
    var user_supports_html = "";
    swal({
        html: modal_text,
        showCancelButton: showCancelBtn,
        showCloseButton: showCloseBtn,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmBtnText,
        cancelButtonText: cancelBtnText,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-secondary',
        buttonsStyling: false,
        allowOutsideClick: false,
        onOpen: function () {
            // var selectBoxHtmlHours="";
            swal.disableConfirmButton();
            dm_get_project_supports(dm_id).done(function (data) {
                user_supports_html += user_support_container(data.project_hour, data.support_type, data.project_hour_id, data.support_type_id);

            });

            $("#swal2-content").find("button").on("click", function () {
                $("#swal2-content").find('.project-content').remove();
                $("#swal2-content").find('.gap-items').remove();
                $("#swal2-content").find(".card-body").after('<h3 class="card-title b-0 py-0 fs-16">' + messageStr("dm-support-header-title") + '</h3>');

                $("#swal2-content").append(user_supports_html).find(".dm-user-supports").find("li").each(function () {
                    $(this).find(".set-hours").click(function () {
                        dm_setup_project_hours().done(function (data) {

                            var selectBoxHtml = '';
                            var selectItems = '';
                            var lang = $("#language").val();

                            $.each(JSON.parse(data), function (index, value) {
                                switch (lang) {
                                    case "tr":
                                        selectItems += '<option value="' + value.id + '">' + value.title + '</option>';
                                        break;
                                    default:
                                        selectItems += '<option value="' + value.id + '">' + value.title_eng + '</option>';
                                        break;
                                }

                                selectBoxHtml = '<select data-provide="selectpicker" data-width="fit" class="my-4 mx-4" name="dm-hours" id="dm-hours">' + selectItems + '</select>';

                            });

                            $("#swal2-content").find(".dm-user-supports").find("li").find(".set-hours").parent().after(selectBoxHtml);

                            $("#dm-hours").on("change", function () {
                                dm_select_project_hours(dm_id, $(this).val()).done(function (data) {
                                    $("#swal2-content").find(".dm-user-supports").find("li").find(".btn-group").remove();
                                    $("#swal2-content").find(".dm-user-supports").find("li").find(".selected-hour").text(data.project_hours);
                                    $("#swal2-content").find(".dm-user-supports").find("li").find(".selected-hour").attr("data-hour", data.project_hours_id);
                                });
                            });


                        });

                    });

                    $(this).find(".set-invests").click(function () {
                        dm_setup_project_investments().done(function (data) {
                            var selectBoxHtml = '';
                            var selectItems = '';
                            var lang = $("#language").val();

                            $.each(JSON.parse(data), function (index, value) {
                                switch (lang) {
                                    case "tr":
                                        selectItems += '<option value="' + value.id + '">' + value.title + '</option>';
                                        break;
                                    default:
                                        selectItems += '<option value="' + value.id + '">' + value.title_eng + '</option>';
                                        break;
                                }

                                selectBoxHtml = '<select data-provide="selectpicker" data-width="fit" class="my-4 mx-4" name="dm-investments" id="dm-investments">' + selectItems + '</select>';

                            });

                            $("#swal2-content").find(".dm-user-supports").find("li").find(".set-invests").parent().after(selectBoxHtml);

                            $("#dm-investments").on("change", function () {
                                // select_investments($(this).val(), selected_post, page_type);
                                dm_select_investments(dm_id, $(this).val()).done(function (data) {

                                    $("#swal2-content").find(".dm-user-supports").find("li").find(".btn-group").remove();
                                    $("#swal2-content").find(".dm-user-supports").find("li").find(".selected-investment").text(data.investments);
                                    $("#swal2-content").find(".dm-user-supports").find("li").find(".selected-investment").attr("data-investment", data.investments_id);
                                    if (data.message !== "") {
                                        $("#swal2-content").find(".dm-user-supports").after("<p class='text-center result-message'>" + data.message + "</p>");
                                    }
                                    else {
                                        $("#swal2-content").find(".result-message").remove();
                                    }
                                });
                            });

                        });
                        //sistemden-invests
                    });
                });

                swal.enableConfirmButton();
            });
        }

    }).then(function (result) {
        if (result.value) {
            var selected_dm_investment = $("#swal2-content").find(".dm-user-supports").find("li").find(".selected-investment").attr("data-investment");

            var selected_dm_project_hour = $("#swal2-content").find(".dm-user-supports").find("li").find(".selected-hour").attr("data-hour");

            send_answer_to_dm_request(dm_id, selected_dm_investment, selected_dm_project_hour).done(function (data) {
                $("body").find(".preloader").fadeOut();
                switch (data.message_type) {
                    case "success":
                        swal(
                            confirmMessageTitle,
                            // confirmMessageContent,
                            data.message,
                            'success'
                        );
                        break;
                    case "error":
                        swal(
                            cancelMessageTitle,
                            // confirmMessageContent,
                            data.message,
                            'error'
                        );
                        break;
                }
            });
        }
    }, function (dismiss) {

    });
};


var classic_special_sweet_popup = function classic_popup(modal_title, modal_text, modal_type, showCancelBtn, showCloseBtn, confirmBtnText, cancelBtnText) {

    swal({
        title: modal_title,
        html: modal_text,
        type: modal_type,
        showCancelButton: showCancelBtn,
        showCloseButton: showCloseBtn,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmBtnText,
        cancelButtonText: cancelBtnText,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-secondary',
        buttonsStyling: false,
        allowOutsideClick: false,
        onOpen: function () {

        }
    }).then(function (result) {
        if (result.value) {


        }
    }, function (dismiss) {

    });
};


var info_sweet_popup = function classic_popup(modal_title, modal_text, modal_type, showCancelBtn, showCloseBtn, confirmBtnText, cancelBtnText, id, page_type) {
    switch (page_type) {
        case "delete":
            swal({
                title: modal_title,
                html: modal_text,
                type: modal_type,
                showCancelButton: showCancelBtn,
                showCloseButton: showCloseBtn,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: confirmBtnText,
                cancelButtonText: cancelBtnText,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                buttonsStyling: false,
                allowOutsideClick: false,
                onOpen: function () {
                    swal.disableConfirmButton();
                    selected_delete_member(id).done(function (data) {
                        $("#swal2-content").append(dm_user_info("javascript:void();", data.profile_point, data.profile_image, data.profile_name, data.profile_location, data.position_title));
                        swal.enableConfirmButton();
                    });
                }
            }).then(function (result) {
                if (result.value) {
                    delete_member_from_team(id).done(function (data) {
                        $("body").find(".preloader").fadeOut();
                        switch (data.message_type) {
                            case "success":
                                $("#my-team").find(".team-members-table").find("tbody").html('');
                                loadmore_myteam_table(1);
                                swal(
                                    messageStr("delete-message-success-title"),
                                    "<p class='fs-15 lh-15'>" + data.message + "</p>",
                                    'success'
                                );
                                break;
                            case "error":
                                swal(
                                    messageStr("delete-message-fail-title"),
                                    "<p class='fs-15 lh-15'>" + data.message + "</p>",
                                    'error'
                                );
                                break;
                        }
                    });

                }
            }, function (dismiss) {

            });
            break;
        default:
            swal({
                title: modal_title,
                html: modal_text,
                type: modal_type,
                showCancelButton: showCancelBtn,
                showCloseButton: showCloseBtn,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: confirmBtnText,
                cancelButtonText: cancelBtnText,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                buttonsStyling: false,
                allowOutsideClick: false,
                onOpen: function () {

                }
            }).then(function (result) {
                if (result.value) {


                }
            }, function (dismiss) {

            });
            break;
    }

};


var special_sweet_popup = function special_sweet_teamrequest(modal_text, requests, showCancelBtn, showCloseBtn, confirmBtnText, cancelBtnText, confirmMessageTitle, confirmMessageContent, cancelMessageTitle, cancelMessageContent) {
    var checked_option = 0;
    swal({
        // title: modal_title,
        html: modal_text,
        // type: modal_type,
        showCancelButton: showCancelBtn,
        showCloseButton: showCloseBtn,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmBtnText,
        cancelButtonText: cancelBtnText,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-secondary',
        buttonsStyling: false,
        allowOutsideClick: false,
        onOpen: function () {
            swal.disableConfirmButton();
            $("#swal2-content").append('<button class="btn btn-outline btn-danger mb-15 select-project">' + messageStr("dm-add-btn") + '</button>');
            $("#swal2-content").find(".select-project").on("click", function () {
                $(this).remove();
                $("#swal2-content").find(".card-body").removeClass("py-25").addClass("pt-0").addClass("pb-25");
                $("#swal2-content").find(".card-body").find(".profile-location").remove();
                $("#swal2-content").find(".card-body").after('<h3 class="card-title b-0 pt-0 fs-16">' + messageStr("dm-project-select-title") + '</h3>');
                dm__select_projects().done(function (data) {
                    var project_item = "";
                    var main_container = "";

                    $.each(data.projects, function (index, value) {
                        project_item += project_list(value.title, value.id);
                    });

                    main_container += '<div class="custom-controls-stacked pb-4">' + project_item + '</div>';

                    $("#swal2-content").append(main_container);

                    $("#swal2-content").find(".custom-controls-stacked").find(".custom-radio").unbind("change").change(function () {
                        checked_option = $(this).find(".custom-control-input").attr("data-id");
                        if (checked_option !== 0) {
                            swal.enableConfirmButton();
                        }
                    });


                });


            });


        }
    }).then(function (result) {
        if (result.value) {
            $("body").find(".preloader").fadeIn();
            dm_send_teamrequest(requests, checked_option).done(function (data) {
                $("body").find(".preloader").fadeOut();
                confirmMessageContent = data.message;
                switch (data.message_type) {
                    case "success":
                        swal(
                            confirmMessageTitle,
                            // confirmMessageContent,
                            "<p class='fs-15 lh-15'>" + data.message + "</p>",
                            'success'
                        );
                        break;
                    case "warning":
                        swal(
                            messageStr("dm-warning-send-teamrequest-title"),
                            // "<p class='fs-15 lh-15'>" + confirmMessageContent + "</p>",
                            "<p class='fs-15 lh-15'>" + data.message + "</p>",
                            'warning'
                        );
                        break;
                    case "error":
                        swal(
                            cancelMessageTitle,
                            // cancelMessageContent,
                            "<p class='fs-15 lh-15'>" + data.message + "</p>",
                            'error'
                        );
                        break;
                }
            });

        }
    }, function (dismiss) {

    });

};


var sweet_popup = function sweet_notification(modal_title, modal_text, requests, modal_type, showCancelBtn, showCloseBtn, confirmBtnText, cancelBtnText, confirmMessageTitle, confirmMessageContent, cancelMessageTitle, cancelMessageContent, id, page_type) {
    var checked_option = 0;
    if (page_type === "project-page") {
        swal({
            title: modal_title,
            // text: modal_text,
            html: '<div class="avatar-list avatar-list-overlap">' + modal_text + '</div>',
            type: modal_type,
            showCancelButton: showCancelBtn,
            showCloseButton: showCloseBtn,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirmBtnText,
            cancelButtonText: cancelBtnText,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-secondary',
            buttonsStyling: false,
            allowOutsideClick: false,
            onOpen: function () {
                if (modal_type === "error" || modal_type === "warning") {
                    swal(
                        cancelMessageTitle,
                        "<p class='fs-15 lh-15'>" + confirmMessageTitle + "</p>",
                        modal_type
                    );
                }

            }
        }).then(function (result) {
            if (result.value) {
                //eğer confirm tıklandıysa
                answers_teamrequests(id, requests, page_type, 0).done(function (data) {
                    $("body").find(".preloader").fadeOut();
                    switch (data.sub_context.message_type) {
                        case "success":
                            swal(
                                confirmMessageTitle,
                                "<p class='fs-15 lh-15'>" + data.sub_context.message + "</p>",
                                'success'
                            );
                            break;
                        case "error":
                            swal(
                                cancelMessageTitle,
                                "<p class='fs-15 lh-15'>" + data.sub_context.message + "</p>",
                                'error'
                            );
                            break;
                    }
                });

            }
        }, function (dismiss) {

        });
    }
    else {
        swal({
            title: modal_title,
            html: modal_text,
            // type: modal_type,
            showCancelButton: showCancelBtn,
            showCloseButton: showCloseBtn,
            showConfirmButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirmBtnText,
            cancelButtonText: cancelBtnText,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-secondary',
            buttonsStyling: false,
            allowOutsideClick: false,
            onOpen: function () {

                swal.disableConfirmButton();
                $("#swal2-content").find(".custom-controls-stacked").find(".custom-radio").unbind("change").change(function () {
                    checked_option = $(this).find(".custom-control-input").attr("data-id");
                });

                $(".swal2-content").find("button").click(function () {

                    var user_item_html = "";
                    $("#swal2-content").html('');
                    $("#swal2-title").find(".card-title").find("span").remove();
                    $("#swal2-title").find(".card-title").find("br").remove();

                    second_stageIdeas_users_profiles(requests, checked_option).done(function (data) {
                        if (data.users.length > 0) {
                            $.each(data.users, function (index, value) {
                                user_item_html += avatar_list_str(value.profile_img, value.profile_name);
                            });
                            $("#swal2-content").append('<div class="avatar-list avatar-list-overlap">' + user_item_html + '</div>');
                            swal.enableConfirmButton();
                        }
                        else {
                            swal(
                                cancelMessageTitle,
                                "<p class='fs-15 lh-15'>" + data.message + "</p>",
                                'error'
                            );
                        }
                    });

                });


            }
        }).then(function (result) {
            if (result.value) {
                // proje seçimini alıyorum -> checked_option
                answers_teamrequests(checked_option, requests, page_type, id).done(function (data) {

                    $("body").find(".preloader").fadeOut();
                    switch (data.sub_context.message_type) {
                        case "success":
                            swal(
                                confirmMessageTitle,
                                "<p class='fs-15 lh-15'>" + data.sub_context.message + "</p>",
                                'success'
                            );
                            break;
                        case "error":
                            swal(
                                cancelMessageTitle,
                                "<p class='fs-15 lh-15'>" + data.sub_context.message + "</p>",
                                'error'
                            );
                            break;
                    }
                });

            }
        }, function (dismiss) {

        });
    }

};


var notification_item = function not_item(hyperlink, icon_type, message_str, created_date, is_read, color_str, item_type, profile_name, id) {
    var is_read_str = "";
    var link_type = "";
    var answer_item = "";
    if (is_read) {
        is_read_str = "";
    }
    else {
        is_read_str = "media-new";
    }

    switch (item_type) {
        case "answer":
            answer_item = "item-answer";
            link_type = 'data-toggle="modal" data-target="#modal-quick-answer"';
            hyperlink = "javascript:void(0);";
            break;
        default:
            link_type = "";
            answer_item = "";
            break;
    }


    var html = '<a data-name="' + profile_name + '" data-answers="' + id + '" class="media item-post media-single ' + is_read_str + " " + answer_item + '" href="' + hyperlink + '" ' + link_type + '>' +
        '<span class="avatar avatar-sm ' + color_str + '"><i class="' + icon_type + '"></i></span>' +
        '<span class="title">' + message_str + '</span>' +
        '<time>' + created_date + '</time>' +
        '</a>';
    return html;
};

var teamrequest_item_str = function ntfc_teamrequest(profile_name, profile_id, profile_image) {
    var html = '<a href="#qv-user-details" data-toggle="quickview" data-provide="tooltip" data-placement="bottom" data-original-title="' + profile_name + '" class="profile-popup" data-id="' + profile_id + '">' +
        '<img class="avatar avatar-lg" src="' + profile_image + '" alt="' + profile_name + '">' +
        '</a>';

    return html;
};

var general_ntf_item = function gnrl_itm(created_date, profile_name, profile_id, title, message) {
    var html = '<li class="timeline-block">' +
        '<div class="timeline-point">' +
        '<span class="badge badge-ring badge-secondary"></span>' +
        '</div>' +
        '<div class="timeline-content">' +
        '<time>' + created_date + '</time>' +
        '<h5>' +
        '<small class="subtitle"><a href="#">' + title + '</a></small>' +
        '</h5>' +
        '<a href="#qv-user-details" data-toggle="quickview" data-provide="tooltip" data-placement="bottom" data-original-title="' + profile_name + '" class="profile-popup" data-id="' + profile_id + '">' +
        profile_name +
        '</a>&nbsp;' + message +
        '<hr>' +
        '</div>' +
        '</li>';

    return html;
};

var quick_answer_item = function q_answ_item(profile_name, created_date, profile_image, answer_content, count_like, count_answer) {
    var html = '<div class="media">' +
        '<span class="avatar">' +
        '<img src="' + profile_image + '" alt="' + profile_name + '">' +
        '</span>' +
        '<div class="media-body">' +
        '<p class="mb-2"><strong>' + profile_name + '</strong>' +
        '<time class="float-right">' + created_date + '</time>' +
        '</p>' + content_html(answer_content) +
        '<div class="btn-spacer mt-3">' +
        '<a class="text-lighter hover-danger like-btn mr-3" href="javascript:void(0);"><i class="fa fa-heart pr-1"></i>  ' + count_like + '</a>' +
        '<a class="text-lighter sub-answer-btn" href="javascript:void(0);"><i class="fa fa-comments-o pr-1"></i> ' + count_answer + '</a>' +
        '</div>' +
        '</div>' +
        '</div>';

    return html;
};

var id_return = function idReturn() {
    var url_str = document.location.pathname.split('/');
    var id_rtrn = url_str[url_str.length - 2];
    return id_rtrn;
};

var near_ideas_item = function near_ideas(profile_image, created_date, profile_name, content_str, post_id, profile_location, member_type) {

    var html = '<div class="media media-single post-item" data-target="#qv-post-detail" data-toggle="quickview" data-post="' + post_id + '">' +
        '<a class="avatar avatar-lg " href="javascript:void(0);">' +
        '<img src="' + profile_image + '" alt="' + profile_name + '">' +
        '</a>' +
        '<div class="media-body cursor-pointer" >' +
        '<p>' +
        '<a class="hover-primary" href="#!"><strong>' + profile_name + '</strong></a>' +
        '<time class="float-right">' + created_date + '</time>' +
        '</p>' +
        '<p class="text-capitalize"><small class="text-fader">' + profile_location + '</small></p>' +
        '<p>' + content_str + '</p>' +
        '</div>' +
        '</div>';

    return html;
};

var near_person_item = function near_persons(profile_name, profile_picture, position_title, profile_id, member_type) {

    var html = '<div class="media media-single">' +
        '<a href="#qv-user-details" data-toggle="quickview" class="profile-popup avatar" data-id="' + profile_id + '">' +
        '<img class="avatar" src="' + profile_picture + '" alt="' + profile_name + '">' +
        '</a>' +
        '<div class="media-body">' +
        '<h6 class="text-truncate"><a href="#qv-user-details" class="profile-popup" data-toggle="quickview" data-id="' + profile_id + '">' + profile_name + '</a></h6>' +
        '<small class="text-fader text-capitalize">' + position_title + '</small>' +
        '</div>' +
        '<div class="media-right">' +
        '<button class="btn btn-square btn-outline btn-dark mr-2 fs-18 send-team-request"><i class="ti-comments-smiley"></i></button>' +
        '<button class="btn btn-square btn-outline btn-secondary ml-2 fs-18 send-message"><i class="ti-comment-alt"></i></button>' +
        '</div>' +
        '</div>';

    return html;
};

var project_person = function prj_person(profile_name, profile_id, profile_image) {
    var html = '<a href="#qv-user-details" class="avatar avatar-md profile-popup" data-id="' + profile_id + '" data-toggle="quickview">' +
        '<img src="' + profile_image + '">' +
        '</a>';
    return html;
};

var send_dm_message = function sweetdm_message(modal_text, showCancelBtn, showCloseBtn, showConfirmBtn, confirmBtnText, cancelBtnText, profile_id) {

    swal({
        html: modal_text,
        // type: modal_type,
        showCancelButton: showCancelBtn,
        showCloseButton: showCloseBtn,
        showConfirmButton: showConfirmBtn,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmBtnText,
        cancelButtonText: cancelBtnText,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-secondary',
        buttonsStyling: false,
        allowOutsideClick: false,
        onOpen: function () {
            swal.disableConfirmButton();
            $("#swal2-content").append('<div class="spinner-circle-shadow mx-auto my-40"></div>');

            check_send_dm_message(profile_id).done(function (data) {
                $("#swal2-content").find(".spinner-circle-shadow").remove();
                switch (data.message_type) {
                    case "success":
                        if (data.users.length > 0) {

                            $.each(data.users, function (index, value) {
                                $("#swal2-content").append(dm_user_info("javascript:void(0);", value.profile_point, value.profile_image, value.profile_name, value.profile_location, value.position_title));
                            });

                            $("#swal2-content").find(".card-body").removeClass("py-25").addClass("py-0").addClass("pb-15");
                            $("#swal2-content").addClass("form-type-combine");
                            new_message_form_element($("#swal2-content"), "rich-text-area");
                            swal.enableConfirmButton();

                        }
                        break;
                    case "error":
                        swal(
                            messageStr("request-cancel-message-title"),
                            "<p class='fs-15 lh-15'>" + data.message + "</p>",
                            'error'
                        );
                        break;
                }

            });

        }
    }).then(function (result) {
        if (result.value) {
            // arka tarafta doluluklar kontrol edilecek
            if ($("#swal2-content").find("#newmessage-content").val().trim().length > 0) {
                var body_text = $("#swal2-content").find("#newmessage-content").val().trim();
                dm_message_send(profile_id, body_text).done(function (data) {

                    $("body").find(".preloader").fadeOut();
                    switch (data.message_type) {
                        case "success":
                            swal(
                                messageStr("request-successful-text-title"),
                                "<p class='fs-15 lh-15'>" + data.message + "</p>",
                                'success'
                            );
                            break;
                        case "error":
                            swal(
                                messageStr("request-cancel-message-title"),
                                "<p class='fs-15 lh-15'>" + data.message + "</p>",
                                'error'
                            );
                            break;
                    }

                });
            }
            else {
                swal(
                    messageStr("request-cancel-message-title"),
                    "<p class='fs-15 lh-15'>" + messageStr("dm-request-cancel-message-text-content") + "</p>",
                    'error'
                );
            }

        }
    }, function (dismiss) {

    });
};


var message_sweet_popup = function message_dm(modal_text, showCancelBtn, showCloseBtn, showConfirmBtn, confirmBtnText, cancelBtnText) {
    var selected_friend = 0;
    swal({
        html: modal_text,
        // type: modal_type,
        showCancelButton: showCancelBtn,
        showCloseButton: showCloseBtn,
        showConfirmButton: showConfirmBtn,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmBtnText,
        cancelButtonText: cancelBtnText,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-secondary',
        buttonsStyling: false,
        allowOutsideClick: false,
        onOpen: function () {
            swal.disableConfirmButton();
            $("#swal2-content").append('<div class="spinner-circle-shadow mx-auto my-40"></div>');

            if ($("#swal2-content").find(".media-list").length > 0) {
                $("#swal2-content").find(".media-list").remove();
            }
            // arkadaş listem => takımımdaki kişiler aynı zamanda arkadaşım
            myfriends().done(function (data) {

                switch (data.message_type) {
                    case "success":
                        var user_item_html = "";
                        $("#swal2-content").addClass("messenger-list");
                        $("#swal2-content").find(".spinner-circle-shadow").remove();

                        $.each(data.users, function (index, value) {
                            user_item_html += msg_user_item(value.profile_name, value.profile_image, value.position_title, value.profile_location, value.id);
                        });

                        var mainHtmlStr = '<div class="media-list media-list-hover media-list-divided scrollable ps-container ps-theme-default message-person-list" style="height:300px;">' + user_item_html + '</div>';

                        $(".messenger-list").append(mainHtmlStr);
                        $(".messenger-list").find(".media-list").perfectScrollbar();

                        if (data.has_next) {
                            loadmore_myfriends(data.page);
                        }


                        $(".messenger-list").find(".message-person-list").find(".media-single").unbind("click").on("click", function () {
                            selected_friend = $(this).attr("data-id");

                            $(".messenger-list").find('.message-person-list').remove();
                            $(".messenger-list").find('.card-title').remove();
                            dm_message_userinfo(selected_friend).done(function (data) {
                                $(".messenger-list").find('.spinner-circle-shadow').remove();
                                $(".messenger-list").append(dm_user_info("javascript:void(0);", data.profile_point, data.profile_image, data.profile_name, data.profile_location, data.position_title));
                                $(".messenger-list").find(".card-body").removeClass("py-25").addClass("py-0").addClass("pb-15");

                                $(".messenger-list").addClass("form-type-combine");
                                new_message_form_element($(".messenger-list"), "rich-text-area");

                            });
                            swal.enableConfirmButton();
                        });
                        break;
                    case "warning":
                        $("#swal2-content").find(".spinner-circle-shadow").remove();
                        // swala başlık koyacağız - arkadaşın yok uyarısı
                        swal(
                            "",
                            "<p class='fs-15 lh-15'>" + data.message + "</p>",
                            'warning'
                        );
                        break;
                }
            });

        }
    }).then(function (result) {
        if (result.value) {
            // arka tarafta doluluklar kontrol edilecek
            if ($(".messenger-list").find("#newmessage-content").val().trim().length > 0) {
                var body_text = $(".messenger-list").find("#newmessage-content").val().trim();
                dm_message_send(selected_friend, body_text).done(function (data) {

                    $("body").find(".preloader").fadeOut();
                    switch (data.message_type) {
                        case "success":
                            swal(
                                messageStr("request-successful-text-title"),
                                "<p class='fs-15 lh-15'>" + data.message + "</p>",
                                'success'
                            );
                            break;
                        case "error":
                            swal(
                                messageStr("request-cancel-message-title"),
                                "<p class='fs-15 lh-15'>" + data.message + "</p>",
                                'error'
                            );
                            break;
                    }

                });
            }
            else {
                swal(
                    messageStr("request-cancel-message-title"),
                    "<p class='fs-15 lh-15'>" + messageStr("dm-request-cancel-message-text-content") + "</p>",
                    'error'
                );
            }

        }
    }, function (dismiss) {

    });

};

var new_message_form_element = function new_message_forms(html_element, input_type) {
    switch (input_type) {
        case "normal-input":
            $(html_element).append('<div class="form-group text-left mb-0"><label for="input-placeholder">' + messageStr("new-message-subject-title") + '</label><input type="text" class="form-control" placeholder="' + messageStr("new-message-subject") + '" id="newmessage-subject"></div>');
            break;
        case "rich-text-area":
            $(html_element).append('<div class="bb-1 text-left"><textarea rows="5" name="newmessage-content" id="newmessage-content" class="form-control b-0"></textarea></div>');
            $(html_element).find("#newmessage-content").summernote({
                height: 200,
                toolbar: [["style", ["clear"]], ["insert", ["link"]], ["para", ["ul"]], ["insert", ["link", "picture", "video"]]],
                disableDragAndDrop: true,
                disableResizeEditor: true,
                dialogsInBody: false,
                dialogsFade: false,
                focus: true,
                placeholder: messageStr("new-message-focus-text")
            });
            $(html_element).find(".note-editor").addClass("b-0").addClass("fs-13");
            break;
        case "message-person":
            // $(html_element).append()
            break;
    }


};


var msg_user_item = function messages_item(profile_name, profile_image, position_title, profile_location, id) {
    var html = '<a class="media media-single text-left" href="javascript:void(0);" data-id="' + id + '">' +
        '<img class="avatar avatar-lg" src="' + profile_image + '" alt="' + profile_name + '">' +
        '<div class="media-body">' +
        '<h6 class="text-capitalize">' + profile_name + '</h6>' +
        '<small class="text-fade text-capitalize">' + position_title + '</small>' +
        '<small class="float-right"><i class="text-fader text-capitalize">' + profile_location + '</i></small>' +
        '</div>' +
        '</a>';

    return html;
};

function messages_menu() {
    $("#messages-menu").find(".nav-item").each(function () {
        $(this).removeClass("active");
        if ($(this).find(".nav-link").text().trim().toLowerCase() === id_return() && $(this).find(".nav-link").text().trim().toLowerCase() !== "") {
            $(this).addClass("active");
        }
    });
}

var item_mailmessage = function mail_message_item(profile_image, profile_name, message_id, title, message_content, sended_date, is_network, is_active) {
    var networkHtml = "";
    if (is_network === "kiyo") {
        networkHtml = '<span class="badge badge-ring fill badge-purple ml-3 mr-0"></span>';
    }
    else {
        if (is_network) {
            networkHtml = '<span class="badge badge-ring badge-success ml-3 mr-0"></span>';
        }
        else {
            networkHtml = '<span class="badge badge-ring badge-info ml-3 mr-0"></span>';
        }
    }

    var html = '<div class="media align-items-center ' + is_active + '">' +
        '<label class="custom-control custom-checkbox">' +
        '<input type="checkbox" class="custom-control-input" value="' + message_id + '">' +
        '<span class="custom-control-indicator"></span>' +
        '</label>' +
        networkHtml +
        '<a href="javascript:void(0);">' +
        '<img class="avatar" src="' + profile_image + '" alt="' + profile_name + '">' +
        '</a>' +
        '<a class="media-body text-truncate" href="/dashboard/messages/detail/' + message_id + '/">' +
        '<h6>' + profile_name + '</h6>' +
        '<span class="text-fade fs-12">' +
        '<strong>' + title + '</strong>&nbsp;' + message_content + ' </span>' +
        '</a>' +
        '<time>' + sended_date + '</time>' +
        '</div>';

    return html;
};

var selected_messages = [];

$(document).find("#messages-list").find(".media").each(function () {
    $(this).find(".custom-checkbox :checkbox").unbind("change").change(function () {

        if ($(this).is(":checked")) {
            if ($.inArray($(this).val(), selected_messages) === -1) {
                selected_messages.push(($(this).val()))
            }
        }
        else {
            selected_messages.splice(selected_messages.indexOf($(this).val()), 1);
        }
    });

});

var message_detail_item = function msg_detail(sender_name, profile_picture, sended_date, message_content) {
    var html = '<div class="card">' +
        '<header class="card-header cursor-pointer" data-toggle="collapse" data-target="#email-0">' +
        '<div class="card-title flexbox">' +
        '<img class="avatar" src="' + profile_picture + '" alt="' + sender_name + '">' +
        '<div>' +
        '<h6 class="mb-0 text-capitalize">' + sender_name + '</h6>' +
        '<small>' + sended_date + '</small>' +
        '</div>' +
        '</div>' +
        '</header>' +
        '<div class="collapse show" id="email-0">' +
        '<div class="card-body">' +
        content_html(message_content) +
        '</div>' +
        '</div>' +
        '</div>';

    return html;
};

var message_ntf_item = function msg_notification_item(profile_name, profile_image, sended_date, message_content, id, is_read) {
    var readCss = "";
    switch (is_read) {
        case true:
            readCss = "media-new1";
            break;
        case false:
            readCss = "media-new";
            break;
    }

    var html = '<a class="media ' + readCss + '" href="/dashboard/messages/detail/' + id + '/">' +
        '<span class="avatar mt-2">' +
        '<img src="' + profile_image + '" alt="' + profile_name + '">' +
        '</span>' +
        '<div class="media-body">' +
        '<p><strong>' + profile_name + '</strong>' +
        '<time class="float-right">' + sended_date + '</time>' +
        '</p>' +
        '<p class="text-truncate">' + message_content + '</p>' +
        '</div>' +
        '</a>';

    return html;
};

function leap_sidebar() {
    $("#vue-sidebar").find(".menu").find(".menu-item").removeClass("active");
    $("#vue-sidebar").find(".menu").find(".menu-item").each(function () {
        if ($(this).attr("data-item") === id_return()) {
            $(this).addClass("active");
        }
    });
}

var myteam_member_list = function team_member_list(profile_image) {
    var html = '<img class="avatar" src="' + profile_image + '">';
    return html;
};

var myteam_item = function myteam_item_html(project_image, project_title, project_describe, project_location, project_id, member_html) {
    var html = '<div class="card b-1 hover-shadow-2 mb-20">' +
        '<a class="media card-body" href="javascript:void(0);">' +
        '<div class="media-left pr-12">' +
        '<img class="avatar avatar-xl no-radius" src="' + project_image + '" alt="' + project_title + '">' +
        '</div>' +
        '<div class="media-body">' +
        '<div class="mb-2">' +
        '<span class="fs-20 pr-16 text-capitalize">' + project_title + '</span>' +
        '</div>' +
        '<p class="my-1 pr-4">' + project_describe + '</p>' +
        '</div>' +
        '<div class="media-right text-right d-none d-md-block">' +
        '<p class="text-fade">' +
        '<small><i class="fa fa-map-marker pr-1"></i> ' + project_location + '</small>' +
        '</p>' +
        '</div>' +
        '</a>' +
        '<footer class="card-footer flexbox align-items-center">' +
        member_html +
        '<div class="card-hover-show">' +
        '<a class="btn btn-xs fs-10 btn-bold btn-dark" href="/dashboard/myteams/detail/' + project_id + '/">' + button_text('detail-btn') + '</a>' +
        '</div>' +
        '</footer>' +
        '</div>';

    return html;
};

var myteam_row = function (profile_name, profile_image, support_type, project_hours, profile_points, profile_id, hour_cost, point_css) {
    var html = '<tr>' +
        '<td>' +
        '<a class="avatar avatar-pill avatar-sm profile-popup" href="#qv-user-details" data-toggle="quickview" data-id="' + profile_id + '">' +
        '<img src="' + profile_image + '">' +
        '<span class="text-capitalize text-truncate">' + profile_name + '</span>' +
        '</a>' +
        '</td>' +
        '<td class="support-info text-truncate">' + support_type + '</td>' +
        '<td class="prj-hours-info">' + project_hours + '</td>' +
        '<td class="hour-cost-info">' + hour_cost + '</td>' +
        '<td class="align-middle level-info">' +
        '<div class="progress mb-0">' +
        '<div class="progress-bar ' + point_css + '" role="progressbar" style="width: ' + profile_points + '%" aria-valuenow="' + profile_points + '" aria-valuemin="0" aria-valuemax="100"></div>' +
        '</div>' +
        '</td>' +
        '<td>' +
        '<nav class="nav no-gutters gap-2 fs-16">' +
        '<a class="nav-link hover-danger mx-auto delete-member" href="javascript:void(0);" data-delete="' + profile_id + '" data-provide="tooltip" title="" data-original-title="' + button_text("button_text") + '"><i class="ti-trash"></i></a>' +
        '</nav>' +
        '</td>' +
        '</tr>';

    return html;
};

var my_dm_request = function myteam_myrequests_table_row(profile_name, profile_image, support_type, project_hours, hour_cost, profile_id, is_convince, is_success, is_give_up, profile_points, point_css, request_id) {
    var rowCss = '';
    if (is_give_up === true) {
        rowCss = "bg-pale-danger";
    }
    else if (is_convince === true) {
        rowCss = "bg-pale-info";
    }
    else if (is_convince === false && is_give_up === false && is_success === false) {
        //success
        rowCss = "";
    }
    else {
        rowCss = "bg-pale-success";
    }


    var html = '<tr class="' + rowCss + '">' +
        '<td>' +
        '<a class="avatar avatar-pill avatar-sm profile-popup" href="#qv-user-details"data-toggle="quickview" data-id="' + profile_id + '">' +
        '<img src="' + profile_image + '">' +
        '<span class="text-capitalize text-truncate">' + profile_name + '</span>' +
        '</a>' +

        '</td>' +
        '<td class="support-info text-truncate">' + support_type + '</td>' +
        '<td class="prj-hours-info">' + project_hours + '</td>' +
        '<td class="hour-cost-info">' + hour_cost + '</td>' +
        '<td class="align-middle level-info">' +
        '<div class="progress mb-0">' +
        '<div class="progress-bar ' + point_css + '" role="progressbar" style="width: ' + profile_points + '%" aria-valuenow="' + profile_points + '" aria-valuemin="0" aria-valuemax="100"></div>' +
        '</div>' +
        '</td>' +
        '<td>' +
        '<nav class="nav no-gutters gap-2 fs-16">' +
        '<a class="nav-link hover-danger mx-auto delete-member" href="javascript:void(0);"data-delete="' + request_id + '" data-provide="tooltip" title="" data-original-title="Delete"><i class="ti-trash"></i></a>' +
        '</nav>' +
        '</td>' +
        '</tr>';

    return html;
};