/**
 * Created by kadircebel on 22.07.2018.
 */

//bu kısmı tekrardan gözden geçir
function more_load(choice_str, investment_choice, page) {

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#load-more-container").find(".result-message").remove();
            $("#load-more-container").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');

        },
        url: '/dashboard/profiles/more-person/',
        method: 'post',
        data: {
            "page": page,
            "filter_option": choice_str,
            "investment_option": investment_choice
        },
        success: function (data) {

            if (data.profiles.length > 0) {
                var profile_html = "";
                $.each(data.profiles, function (index, value) {
                    profile_html += list_html(value.id, value.profile_picture, value.profile_name, value.position_title, value.profile_location, value.profile_point, value.member_type);
                });

                $("#person-list").append(profile_html);
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-container").find(".spinner-circle-shadow").remove();
                        $("#load-more-container").find("button").show();
                        $("#load-more-container").find("button").attr("data-page", data_json.page);
                        break;
                    case false:

                        $("#load-more-container").find(".spinner-circle-shadow").remove();
                        $("#load-more-container").find("button").before('<p class="result-message">' + messageStr('list-result-message') + '</p>');
                        $("#load-more-container").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                $(document).find("#person-list").find(".card").each(function () {
                    $(this).find(".send-team-request").unbind("click").on("click", function () {
                        var profile_id = $(this).parent().parent().parent().find(".profile-popup").attr("data-id");
                        general_send_teamrequest_to_person(profile_id);
                    });
                });

            }
        }
    });
}

function more_load_ideas(city, country, unanswered, all, counter) {

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#load-more-ideas").find(".reuslt-message").remove();
            $("#load-more-ideas").find("button").before().before('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/ideas/more-ideas/',
        method: 'post',
        data: {
            "city": city,
            "country": country,
            "unanswered": unanswered,
            "all": all,
            "page": counter
        },
        success: function (data) {

            if (data.posts.length > 0) {
                var htmlStrIdeas = "";
                $.each(data.posts, function (index, value) {
                    htmlStrIdeas += ideas_list_item(value.post_id, value.profile_picture, value.profile_username, value.created_date, value.position_title, value.post_content, value.like_count, value.teamrequest_count, value.smilarproject_count, value.answer_count, value.profile_id);
                });

                $("#users-ideas-list").append(htmlStrIdeas);
            }


        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                $("#load-more-ideas").find(".spinner-circle-shadow").remove();
                var data_json = $.parseJSON(jqXHR.responseText);

                $(document).find(".post-item").on("click", function () {
                    var post = $(this).attr("data-post");
                    ideas_popup(post);
                });

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                switch (data_json.has_next) {
                    case true:
                        $("#load-more-ideas").find("button").show();
                        $("#load-more-ideas").find("button").attr("data-page", data_json.page);
                        break;
                    case false:

                        $("#load-more-ideas").find("button").hide();
                        $("#load-more-ideas").find("button").before('<p class="text-center result-message">' + messageStr('list-result-message') + '</p>');
                        break;
                }

            }
        }


    });
}

function more_load_sub_answers(answer, page, page_type) {

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $(".load-more-subanswers").parent().before('<div class="spinner-linear spinner-dark mb-15"><div class="line"></div></div>');
        },
        url: '/dashboard/ideas/more-sub-answers/',
        method: 'post',
        data: {
            "answer": answer,
            "page": page
        },
        success: function (data) {
            // $("#load-more-ideas").find(".spinner-circle").fadeOut();
            // $("#ideas-list").find(".list-result-message").html('');
            if (data.sub_answers.length > 0) {
                var sub_answers_html = "";
                $.each(data.sub_answers, function (index, value) {

                    sub_answers_html += post_sub_answers(value.profile_image, value.profile_name, value.created_date, value.position_title, value.answer_content, value.like_count, value.answers_count, value.sub_answer_id, value.profile_id, page_type, value.is_edit, value.ans_ans_id);
                });
            }


            if (page_type.indexOf("detail-page") > -1) {
                $("#post-answers-detail").find('[data-answers=' + answer + ']').find(".media:last-child").find(".spinner-linear").remove();
                $("#post-answers-detail").find('[data-answers=' + answer + ']').find(".media:last-child").before(sub_answers_html);
                $("#post-answers-detail").find('[data-answers=' + answer + ']').find(".media:last-child").find(".load-more-subanswers").attr("data-page", data.page);
            }
            else {
                $("#post-answers").find('[data-answers=' + answer + ']').find(".media:last-child").find(".spinner-linear").remove();
                $("#post-answers").find('[data-answers=' + answer + ']').find(".media:last-child").before(sub_answers_html);
                $("#post-answers").find('[data-answers=' + answer + ']').find(".media:last-child").find(".load-more-subanswers").attr("data-page", data.page);
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {

                var data_json = $.parseJSON(jqXHR.responseText);

                if (data_json.has_next === false) {
                    if (page_type.indexOf("detail-page") > -1) {
                        $("#post-answers-detail").find('[data-answers=' + answer + ']').find(".media:last-child").find(".media-body").find("p").html(messageStr('list-result-message'));
                    }
                    else {
                        $("#post-answers").find('[data-answers=' + answer + ']').find(".media:last-child").find(".media-body").find("p").html(messageStr('list-result-message'));
                    }
                }

                mainAndSubAnswerBtnContainer();
            }
        }


    });
}

//page type kısmında kaldın
function more_load_main_answers(post, page, page_type) {

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $(".load-more-mainanswers").parent().before('<div class="spinner-linear spinner-dark mb-15"><div class="line"></div></div>');

        },
        url: '/dashboard/ideas/more-main-answers/',
        method: 'post',
        data: {
            "post": post,
            "page": page
        },
        success: function (data) {

            switch (data.message_type) {
                case "success":
                    var sub_htmlStr = "";
                    var main_htmlStr = "";
                    var load_more_btn_html = "";

                    $.each(data.main_answers, function (index, value) {

                        if (value.sub_answers.length > 0) {

                            $.each(value.sub_answers, function (index_sub, value_sub) {

                                sub_htmlStr += post_sub_answers(value_sub.profile_image, value_sub.profile_name, value_sub.created_date, value_sub.position_title, value_sub.answer_content, value_sub.like_count, value_sub.answers_count, value_sub.sub_answer_id, value_sub.profile_id, page_type, value_sub.is_edit, value_sub.ans_ans_id);

                            });

                            if (value.sub_has_next === true) {
                                load_more_btn_html = '<div class="media">' +
                                    '<div class="media-body text-left">' +
                                    '<p class="text-lighter"><button class="fs-13 fs-13 btn btn-pure btn-secondary text-light fw-400 load-more-subanswers" data-page="2"><i class="fa fa-mail-forward mr-1 text-light"></i> ' + button_text("more-load") + '</button></p></div></div>';

                                if (page_type.indexOf("popup-page") > -1) {
                                    if ($("#post-answers").find('[data-answers=' + value.answer_id + ']').find(".media").length > 4) {
                                        $("#post-answers").find(".media-body").find(".media:last-child").after(load_more_btn_html);
                                    }
                                }
                                else {
                                    if ($("#post-answers-detail").find('[data-answers=' + value.answer_id + ']').find(".media").length > 4) {
                                        $("#post-answers-detail").find(".media-body").find(".media:last-child").after(load_more_btn_html);
                                    }
                                }
                                sub_htmlStr += load_more_btn_html;
                            }


                            main_htmlStr += post_main_answers(value.profile_image, value.profile_name, value.created_date, value.position_title, value.answer_content, value.like_count, value.answers_count, value.answer_id, sub_htmlStr, value.profile_id, page_type, value.is_edit);
                            sub_htmlStr = "";
                        }
                        else {
                            main_htmlStr += post_main_answers(value.profile_image, value.profile_name, value.created_date, value.position_title, value.answer_content, value.like_count, value.answers_count, value.answer_id, sub_htmlStr, value.profile_id, page_type, value.is_edit);

                        }
                    });

                    if (page_type.indexOf("detail-page") > -1) {
                        $("#post-answers-detail").find(".media:last-child").find(".spinner-linear").remove();
                        $("#post-answers-detail").find(".load-more-mainanswers").closest(".media").before(main_htmlStr);

                        $("#post-answers-detail").find(".media:last-child").find(".load-more-mainanswers").attr("data-page", data.page);
                    }
                    else {
                        $("#post-answers").find(".media:last-child").find(".spinner-linear").remove();
                        $("#post-answers").find(".load-more-mainanswers").closest(".media").before(main_htmlStr);

                        $("#post-answers").find(".media:last-child").find(".load-more-mainanswers").attr("data-page", data.page);


                    }


                    break;
                case "warning":
                    toastMessage(data.message, messageStr("ok-message"), "", "warning");
                    break;
                case "error":
                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                    break;
                default:
                    break;

            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {

                var data_json = $.parseJSON(jqXHR.responseText);

                if (data_json.has_next === false) {
                    if (page_type.indexOf("popup-page") > -1) {
                        $("#post-answers").find(".load-more-mainanswers").closest(".media").find(".media-body").find("p").html(messageStr('list-result-message'));
                    }
                    else {
                        $("#post-answers-detail").find(".load-more-mainanswers").closest(".media").find(".media-body").find("p").html(messageStr('list-result-message'));
                    }

                }

                if (page_type.indexOf("detail-page") > -1) {
                    if ($("#post-answers-detail").find(".load-more-subanswers").length > 0) {
                        $("#post-answers-detail").find(".load-more-subanswers").unbind().click(function () {
                            var answer_id = $(this).closest(".main-answers").attr("data-answers");
                            var page = $(this).attr("data-page");
                            more_load_sub_answers(answer_id, page, page_type);
                        });
                        $(document).find(".profile-popup").click(function () {
                            $("#qv-user-details").find(".quickview-body").scrollTop(0);
                            $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                            var id = $(this).attr("data-id");

                            $("#qv-user-details").attr("data-user-id", id);

                            var modal_body = $("#qv-user-details").find(".quickview-body");
                            user_popup(id, modal_body);

                        });
                    }
                }
                else {
                    if ($("#post-answers").find(".load-more-subanswers").length > 0) {
                        $("#post-answers").find(".load-more-subanswers").unbind().click(function () {
                            var answer_id = $(this).closest(".main-answers").attr("data-answers");
                            var page = $(this).attr("data-page");
                            more_load_sub_answers(answer_id, page, page_type);
                        });
                    }


                }


                mainAndSubAnswerBtnContainer();
            }
        }


    });
}

function loadmore_myfriends(page) {
    var divHtml = $(".messenger-list").find(".message-person-list");
    var hasNext = true;

    $(divHtml).scroll(function () {
        var hT = $(this).offset().top,
            hH = $(this).outerHeight(),
            wH = $(document).height(),
            wS = $(this).scrollTop();
        if (wS > (hT + hH - wH)) {
            if (hasNext === true) {
                $.getJSON("/dashboard/myprofile/loadmore-myfriends/?page=" + page, function (data) {
                    $(".messenger-list").find(".message-person-list").find(".media-single:last-child").after('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');
                    if (hasNext && data.page !== page) {
                        $.each(data.users, function (index, value) {
                            $(".messenger-list").find(".message-person-list").append(msg_user_item(value.profile_name, value.profile_image, value.position_title, value.profile_location, value.id));
                        });
                        page = data.page;
                    }
                }).done(function (data) {
                    $(".messenger-list").find(".message-person-list").find(".spinner-linear").remove();
                    hasNext = data.has_next;

                    $(".messenger-list").find(".message-person-list").find(".media-single").unbind("click").on("click", function () {

                        $(".messenger-list").find('.message-person-list').remove();
                    });
                });
            }
        }
    });
}

/*Teamrequest*/

function loadmore_teamrequest(page) {
    var divHtml = $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".dropdown-menu").find(".notification-content");
    var hasNext = true;

    $(divHtml).scroll(function () {
        var hT = $(this).offset().top,
            hH = $(this).outerHeight(),
            wH = $(document).height(),
            wS = $(this).scrollTop();

        if (wS > (hT + hH - wH)) {
            if (hasNext === true) {
                $.getJSON("/dashboard/loadmore-teamrequest/?page=" + page, function (data) {
                    $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".notification-content").find(".item-post").last().after('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');

                    if (hasNext && data.page !== page) {
                        $.each(data.notifications, function (index, value) {
                            $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".notification-content").append(notification_item_team(value.profile_image, value.profile_name, value.created_date, value.message, value.is_read, value.redirect_url));
                        });
                        page = data.page;
                    }

                }).done(function (data) {
                    $(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").find(".notification-content").find(".spinner-linear").remove();
                    dm_request_answer();
                    hasNext = data.has_next;
                });
            }


        }

    });

}

function loadmore_project_undecided_teamrequests(id, page) {
    //users_projects_id
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#load-more-undecided").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/myprojects/loadmore-undecided/' + id + "/",
        method: 'post',
        data: {
            // "id": id,
            "page": page
        },
        success: function (data) {
            if (data.requests.length > 0) {
                var requestHtmlStr = "";
                $.each(data.requests, function (index, value) {
                    requestHtmlStr += teamrequest_item(value.profile_img, value.profile_name, value.id, value.position_title, value.support_type, value.hour_info, value.hour_cost, value.location);

                });
                $("#undecided-requests").find(".undecided-list").append(requestHtmlStr);
            }
            else {
                $("#undecided-requests").find(".undecided-list").html('');
                $("#undecided-requests").find(".undecided-list").append('<div class="col-md-12 col-lg-12">' + alertBox(data.message, "danger") + '</div>');
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                $("#load-more-undecided").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-undecided").find(".result-message").remove();
                        $("#load-more-undecided").find("button").show();
                        $("#load-more-undecided").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#load-more-undecided").find("button").before('<p class="text-center result-message">' + data_json.message + '</p>');
                        $("#load-more-undecided").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

            }
        }
    });
}

/*Teamrequest*/

function load_more_projects(prj_option, prj_stage, page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $("#load-more-projects").find(".list-result-message").find("p").remove();
            $("#load-more-projects").find(".list-result-message").before('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/projects/loadmore-projects/',
        method: 'post',
        data: {
            "selected_option": prj_option,
            "selected_stage": prj_stage,
            "page": page
        },
        success: function (data) {
            if ($(data.project_list).length > 1) {
                var listHtml = '';
                $.each(data.project_list, function (index, value) {
                    switch (value.message_type) {
                        case "success":
                            listHtml += project_item_html(value.project_name, value.project_logo, value.project_location, value.project_id, value.is_teamrequest, value.is_feedback, value.business_stage);
                            break;
                        case "error":

                            break;
                    }
                });

                $("#projects-list").append(listHtml);
            }
            else {
                $("#load-more-projects").find(".list-result-message").append('<p>' + messageStr('project-no-item') + '</p>');
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                $("#load-more-projects").find(".spinner-circle-shadow").remove();
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-projects").find(".list-result-message").find("p").remove();
                        $("#load-more-projects").find(".list-result-message").find("button").show();
                        $("#load-more-projects").find(".list-result-message").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#load-more-projects").find(".list-result-message").append('<p>' + messageStr('project-result-message') + '</p>');
                        $("#load-more-projects").find(".list-result-message").find("button").hide();
                        break;
                }

            }
        }
    });
}

function loadmore_prj_teamrequests(id, page) {

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $("#load-more-requests").find(".result-message").remove();
            $("#load-more-requests").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');

        },
        url: '/dashboard/myprojects/loadmore-teamrequests/' + id + '/',
        method: 'post',
        data: {
            "page": page,
            "id": id
        },
        success: function (data) {
            var requestHtmlStr = "";
            if (data.requests.length > 0) {
                $.each(data.requests, function (index, value) {
                    requestHtmlStr += teamrequest_item(value.profile_img, value.profile_name, value.id, value.position_title, value.support_type, value.hour_info, value.hour_cost, value.location);

                });
                $("#users-requests").find(".project-requests-lists").append(requestHtmlStr);
            }
            else {
                $("#load-more-requests").find("button").before('<p class="text-center result-message">' + messageStr('request-no-item') + '</p>');
            }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                $("#load-more-requests").find(".spinner-circle-shadow").remove();
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-requests").find(".result-message").remove();
                        $("#load-more-requests").find("button").show();
                        $("#load-more-requests").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#load-more-requests").find("button").before('<p class="text-center result-message">' + messageStr('request-no-item') + '</p>');
                        $("#load-more-requests").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                myprj_requests();

            }
        }
    });
}

function loadmore_smilars(id, page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#load-more-similars").find("button").before('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/myideas/loadmore-similarprojects/' + id + "/",
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {

            if (data.similar_projects.length > 0) {
                var requestHtmlStr = "";
                $.each(data.similar_projects, function (index, value) {
                    requestHtmlStr += similarproject(value.profile_img, value.profile_id, value.created_date, value.similar_content, value.similar_content_link, value.profile_name);

                });
                $("#smilar-projects").append(requestHtmlStr);
            }
            // else {
            //     // $("#undecided-requests").find(".undecided-list").html('');
            //     // $("#smilar-projects").append('<div class="col-md-12 col-lg-12">' + alertBox(data.message, "danger") + '</div>');
            // }

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);

                $("#load-more-similars").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#load-more-similars").find(".result-message").remove();
                        $("#load-more-similars").find("button").show();
                        $("#load-more-similars").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#load-more-similars").find("button").before('<p class="text-center result-message">' + messageStr("similar-projects") + '</p>');
                        $("#load-more-similars").find("button").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

            }
        }
    });
}

function loadmore_get_general_notifications(page) {
    var divHtml = $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".dropdown-menu").find(".notification-content");
    var hasNext = true;

    $(divHtml).scroll(function () {
        var hT = $(this).offset().top,
            hH = $(this).outerHeight(),
            wH = $(document).height(),
            wS = $(this).scrollTop();

        if (wS > (hT + hH - wH)) {
            if (hasNext) {

                $.getJSON("/dashboard/notifications/loadmore-gnrl-notifications/?page=" + page, function (data) {
                    $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").find(".item-post").last().after('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');
                    if (hasNext && data.page !== page) {

                        $.each(data.notifications, function (index, value) {
                            $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").append(notification_item(value.redirect_url, value.icon, value.message, value.created_date, value.is_read, value.color, value.link_type, value.profile_name, value.id))
                        });
                        page = data.page;
                    }
                }).done(function (data) {

                    $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").find(".spinner-linear").remove();
                    hasNext = data.has_next;

                    $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").each(function () {
                        $(this).find(".item-answer").unbind("click").on("click", function () {
                            $("#quick-answer-content").summernote('code', '');
                            var p_user = $(this).attr("data-name");
                            var answer_id = $(this).attr("data-answers");


                            $("#modal-quick-answer").attr("data-answers", answer_id);
                            quick_answer(answer_id).done(function (data) {
                                $("#modal-quick-answer").find(".spinner-circle-shadow").remove();

                                $("#modal-quick-answer").find("hr").before(quick_answer_item(data.profile_name, data.created_date, data.profile_image, data.answer_content, data.like_count, data.answer_count));
                            });
                        });
                    });
                });
            }

        }

    });
}

function loadmore_ntfc_teamrequest(page) {
    var ntfc_item = "";
    $.getJSON("/dashboard/notifications/loadmore-teamrequestlist/?page=" + page, function (data) {
        $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").find(".item-post").last().after('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');
        $.each(data.teamrequests, function (index, value) {

            if (value.users.length > 0) {

                ntfc_item = "";
                $.each(value.users, function (indx, vlue) {
                    ntfc_item += teamrequest_item_str(vlue.profile_name, vlue.user_id, vlue.profile_img);
                });

                if (value.type.indexOf("post") > -1) {
                    $("#teamrequests-timeline").append(
                        '<li class="timeline-block"><div class="timeline-point">' + '<span class="badge badge-ring badge-secondary"></span></div>' +
                        '<div class="timeline-content"><time>' + value.created_date + '</time><p class="pb-0 mb-0 mt-2 mb-3">' + messageStr("team-ntfc-post-title") + '<br/><a href="' + value.redirect_url + '">' + value.title + '</a></p>' + ntfc_item + '<hr></div></li>');
                }
                else {
                    $("#teamrequests-timeline").append(
                        '<li class="timeline-block"><div class="timeline-point">' + '<span class="badge badge-ring badge-secondary"></span></div>' +
                        '<div class="timeline-content"><time>' + value.created_date + '</time><p class="pb-0 mb-0 mt-2 mb-3">' + messageStr("team-ntfc-project-title") + '<br/><a href="' + value.redirect_url + '">' + value.title + '</a></p>' + ntfc_item + '<hr></div></li>');
                }
            }
            else {
                ntfc_item = "";
            }
        });
    }).done(function (data) {
        $(document).find("header.topbar").find(".topbar-right").find(".general-notifications").find(".notification-content").find(".spinner-linear").remove();
        if (data.has_next != true) {
            $("#loadmore-ntf-requests").find("button").hide();
            $("#loadmore-ntf-requests").find("button").before('<p class="text-center result-message">' + messageStr("nomore-notification") + '</p>');
        }
        else {
            $("#loadmore-ntf-requests").find("button").show();
            $("#loadmore-ntf-requests").find("button").attr("data-page", data.page);
        }
        $(document).find(".profile-popup").click(function () {
            $("#qv-user-details").find(".quickview-body").scrollTop(0);
            $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
            var id = $(this).attr("data-id");
            $("#qv-user-details").attr("data-user-id", id);
            var modal_body = $("#qv-user-details").find(".quickview-body");
            user_popup(id, modal_body);
        });
    });
}

function loadmore_general_ntfc(page) {
    $.getJSON("/dashboard/notifications/loadmore-general/?page=" + page, function (data) {
        $("#loadmore-ntf-general").find(".result-message").remove();
        if (data.notifications.length > 0) {
            $.each(data.notifications, function (index, value) {
                $("#notifications-timeline").append(general_ntf_item(value.created_date, value.profile_name, value.profile_id, value.title, value.message))
            });
        }
        else {
            $("#loadmore-ntf-general").find("button").before('<p class="text-center result-message">' + messageStr("myproject-filter-no-item") + '</p>');
            $("#loadmore-ntf-general").find("button").hide();
        }
    }).done(function (data) {
        switch (data.has_next) {
            case true:
                $("#loadmore-ntf-general").find(".result-message").remove();
                $("#loadmore-ntf-general").find("button").attr("data-page", data.page);
                break;
            case false:
                $("#loadmore-ntf-general").find("button").before('<p class="text-center result-message">' + messageStr("myproject-filter-no-item") + '</p>');
                $("#loadmore-ntf-general").find("button").hide();
                break;
        }


    });
}

function loadmore_dashboard_near_members(page, has_next) {
    var divHtml = $("#near-persons").find(".media-list");
    var hasNext = has_next;

    $(divHtml).scroll(function () {
        var hT = $(this).offset().top,
            hH = $(this).outerHeight(),
            wH = $(document).height(),
            wS = $(this).scrollTop();

        if (wS > (hT + hH - wH)) {
            if (hasNext === true) {
                $.getJSON("/dashboard/loadmore-near-members/?page=" + page, function (data) {
                    $("#near-persons").find(".media-list").find(".media-single").last().after('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');
                    if (hasNext && data.page !== page) {

                        $.each(data.profiles, function (index, value) {
                            $("#near-persons").find(".media-list").append(near_person_item(value.profile_name, value.profile_picture, value.position_title, value.id));
                        });
                        page = data.page;
                    }
                }).done(function (data) {
                    $("#near-persons").find(".media-list").find(".spinner-linear").remove();
                    hasNext = data.has_next;
                    $(document).find(".profile-popup").click(function () {
                        $("#qv-user-details").find(".quickview-body").scrollTop(0);
                        $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                        var id = $(this).attr("data-id");
                        $("#qv-user-details").attr("data-user-id", id);
                        var modal_body = $("#qv-user-details").find(".quickview-body");
                        user_popup(id, modal_body);
                    });
                });

            }


        }
    });
}

function loadmore_inbox(page) {

    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $(document).find("#messages-list").append('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/messages/loadmore-inbox/',
        method: 'POST',
        data: {
            "page": page,
            "page_type": id_return()
        },
        success: function (data) {
            var htmlStr = "";
            $(document).find("#messages-list").find(".spinner-circle-shadow").remove();
            if (data.messages.length > 0) {

                $.each(data.messages, function (index, value) {
                    htmlStr += item_mailmessage(value.profile_image, value.sender_name, value.id, value.title, value.message_content, value.sended_date, value.is_network, value.is_readed);
                });

                $(document).find("#messages-list").append(htmlStr);
            }
            else {
                if ($(document).find("#messages-list").find(".media").length === 0) {
                    $(document).find("#messages-list").append('<div class="card-body text-center">' + data.message + '</div>');
                }
            }

        },
        complete: function (jqXHR, status) {

            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                if (data_json.has_next) {
                    $(document).find("#loadmore-inbox").show();
                    $(document).find("#loadmore-inbox").attr("data-page", data_json.page);
                }
                else {
                    $(document).find("#loadmore-inbox").hide();
                }
            }

            selected_messages = [];

            $(document).find("#messages-list").find(".media").each(function () {
                $(this).find(".custom-checkbox :checkbox").unbind("change").change(function () {

                    if ($(this).is(":checked")) {
                        if ($.inArray($(this).val(), selected_messages) === -1) {
                            selected_messages.push(($(this).val()))
                        }
                    }
                    else {
                        selected_messages.splice(selected_messages.indexOf($(this).val()), 1);
                    }
                });

            });
        }
    });
}


function loadmore_getmessages(page) {

    var divHtml = $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".dropdown-menu").find(".message-notification-content");
    var hasNext = true;

    $(divHtml).scroll(function () {
        var hT = $(this).offset().top,
            hH = $(this).outerHeight(),
            wH = $(document).height(),
            wS = $(this).scrollTop();

        if (wS > (hT + hH - wH)) {
            if (hasNext) {

                $.getJSON("/dashboard/messages/loadmore-notification-list/?page=" + page, function (data) {
                    if ($(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".spinner-linear").length === 0) {
                        $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".dropdown-footer").before('<div class="spinner-linear spinner-dark"><div class="line"></div></div>');
                    }

                    if (hasNext && data.page !== page) {

                        $.each(data.messages, function (index, value) {
                            $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".message-notification-content").append(message_ntf_item(value.sender_name, value.profile_image, value.sended_date, value.message_content, value.id, value.is_read));
                        });
                        page = data.page;
                    }

                }).done(function (data) {
                    $(document).find("header.topbar").find(".topbar-right").find(".notification-messages").find(".spinner-linear").remove();
                    hasNext = data.has_next;
                });
            }
        }

    });


}

function loadmore_message_detail(page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $(document).find("#detail-msg-content").append('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/messages/loadmore-detail/',
        method: 'post',
        data: {
            "page": page,
            "id": id_return()
        },
        success: function (data) {

            if (data.messages.length > 0) {
                $(document).find("#detail-msg-content").find(".spinner-circle-shadow").remove();
                $.each(data.messages, function (index, value) {
                    $("#detail-msg-content").append(message_detail_item(value.profile_name, value.profile_image, value.sended_date, value.message_content));
                });
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                if (data_json.has_next) {
                    $("#loadmore-msg-detail").attr("data-page", data_json.page);
                }
                else {
                    $("#loadmore-msg-detail").hide();
                }
            }


        }
    });
}

function loadmore_myteams(page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $(document).find("#detail-msg-content").append('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/myteams/loadmore-myteams/',
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {
            var projects_html = "";
            var team_member_html = "";

            if (data.my_projects.length > 0) {

                $.each(data.my_projects, function (index, value) {

                    if (value.project_members.length > 0) {
                        $.each(value.project_members, function (indx, vlue) {
                            team_member_html += myteam_member_list(vlue.profile_image);
                        });

                        team_member_html = '<div class="avatar-list">' + team_member_html + '</div>';

                    }
                    else {
                        team_member_html = "";
                        team_member_html = "<div><span>You don't have any team member yet.</span></div>";
                    }

                    // console.log(team_member_html);
                    projects_html += myteam_item(value.project_image, value.project_title, value.project_describe, value.project_location, value.project_id, team_member_html);


                });

                // $("#myteams-list").append(projects_html);
                $("#tab-founded").append(projects_html);
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.has_next) {
                    case true:
                        $("#loadmore-myteams").find("button").show();
                        $("#loadmore-myteams").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#loadmore-myteams").find("button").hide();
                        break;
                }

            }
        }
    });
}

function loadmore_myteam_table(page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $("#my-team").find(".loadmore-detail").before('<div class="spinner-circle-shadow mx-auto"></div>');
            $("#my-team").find(".result-message").remove();
        },
        url: '/dashboard/myteams/loadmore-details/',
        method: 'post',
        data: {
            "page": page,
            "id": id_return()
        },
        success: function (data) {
            console.log(data);
            if (data.team_members.length > 0) {
                $.each(data.team_members, function (index, value) {
                    $("#my-team").find(".team-members-table").find("tbody").append(myteam_row(value.profile_name, value.profile_image, value.support_type, value.project_hours, value.profile_points, value.profile_id, value.hour_cost, value.point_css));
                });
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                $("#my-team").find(".spinner-circle-shadow").remove();
                switch (data_json.has_next) {
                    case true:
                        $("#my-team").find(".result-message").remove();
                        $("#my-team").find(".loadmore-detail").show();
                        $("#my-team").find(".loadmore-detail").attr("data-page", data_json.page);
                        break;
                    case false:
                        $("#my-team").find(".loadmore-detail").after('<p class="result-message">' + messageStr("myteam-result-message") + '</p>');
                        $("#my-team").find(".loadmore-detail").hide();
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                $(document).find("#my-team").find(".team-members-table").find(".delete-member").on("click", function () {
                    var id = $(this).attr("data-delete");
                    $(document).find("#my-team").find(".team-members-table").find(".delete-member").on("click", function () {
                        var id = $(this).attr("data-delete");
                        info_sweet_popup(messageStr("delete-member-title"), messageStr("delete-member-content"), "", true, false, messageStr("option-yes"), messageStr("option-no"), id, "delete");
                    });
                });
            }
        }
    });
}

function loadmore_cofunded_projects_myteam(page) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
            $(document).find("#detail-msg-content").append('<div class="spinner-circle-shadow mx-auto"></div>');
        },
        url: '/dashboard/myteams/loadmore-cofunded-projects/',
        method: 'post',
        data: {
            "page": page
        },
        success: function (data) {
            var projects_html = "";
            var team_member_html = "";

            if (data.my_projects.length > 0) {
                $.each(data.my_projects, function (index, value) {
                    team_member_html = "";
                    if (value.project_members.length > 0) {
                        $.each(value.project_members, function (indx, vlue) {
                            team_member_html += myteam_member_list(vlue.profile_image);
                        });
                        team_member_html = '<div class="avatar-list">' + team_member_html + '</div>';
                    }
                    else {
                        team_member_html = "";
                        team_member_html = "<div><span>You don't have any team member yet.</span></div>";
                    }

                    projects_html += myteam_item(value.project_image, value.project_title, value.project_describe, value.project_location, value.project_id, team_member_html);

                });

                $("#tab-cofounded").append(projects_html);
            }
            else {
                $("#tab-cofounded").append('<p class="result-message text-center">' + data.message + '</p>');
            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                var btnHtml = '<div class="row" id="loadmore-cofunded-teams">' +
                    '<div class="col-md-12 col-12 text-center py-10">' +
                    '<button type="button" class="btn btn-w-md btn-round btn-primary my-30" data-page="">' + button_text("more-load") + '</button></div></div>';
                switch (data_json.has_next) {
                    case true:
                        if ($("#tab-cofunded").find("#loadmore-cofunded-teams").length < 1) {
                            $("#tab-cofounded").append(btnHtml);
                        }

                        $(document).find("#loadmore-cofunded-teams").find("button").show();
                        $(document).find("#loadmore-cofunded-teams").find("button").attr("data-page", data_json.page);
                        break;
                    case false:
                        $(document).find("#loadmore-cofunded-teams").find("button").hide();
                        break;
                }

                $(document).find("#loadmore-cofunded-teams").find("button").unbind("click").on("click", function () {
                    loadmore_cofunded_projects_myteam($(this).attr("data-page"));
                });

            }
        }
    });
}

function myteam_mydm_requests(page, id) {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }

            $(document).find("#my-requests").find(".dm-requests-table").find("tbody").append('<tr class="loading-spin"><td colspan="6"><div class="spinner-circle-shadow mx-auto"></div></td></tr>');
        },
        url: '/dashboard/myteams/my-dm-requests/',
        method: 'post',
        data: {
            "page": page,
            "id": id
        },
        success: function (data) {
            if ($(document).find("#my-requests").find(".dm-requests-table").find("tbody").find(".loading-spin").length > 0){
                $(document).find("#my-requests").find(".dm-requests-table").find("tbody").find(".loading-spin").remove();
            }

            if (data.requests.length > 0) {
                var rowHtmlStr = '';
                $.each(data.requests, function (index, value) {
                    rowHtmlStr += my_dm_request(value.profile_name, value.profile_image, value.support_type, value.project_hours, value.hour_cost, value.profile_id, value.is_convince, value.is_success, value.is_give_up, value.profile_points, value.profile_point_css,value.request_id);
                });

                $(document).find("#my-requests").find(".dm-requests-table").find("tbody").append(rowHtmlStr);

            }
        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                var btnHtml = '<div class="row" id="loadmore-dm-myrequests">' +
                    '<div class="col-md-12 col-12 text-center py-10">' +
                    '<button type="button" class="btn btn-w-md btn-round btn-primary my-30" data-page="">' + button_text("more-load") + '</button></div></div>';


                switch (data_json.has_next) {
                    case true:
                        //dm-requests-table
                        if ($(document).find("#my-requests").find("#loadmore-dm-myrequests").length > 0){
                            $(document).find("#my-requests").find("#loadmore-dm-myrequests").find("button").attr("data-page",data_json.page);
                        }
                        else{
                            $(document).find("#my-requests").find(".dm-requests-table").after(btnHtml);
                            $(document).find("#my-requests").find("#loadmore-dm-myrequests").find("button").attr("data-page",data_json.page);
                        }
                        break;
                    case false:
                        if ($(document).find("#my-requests").find("#loadmore-dm-myrequests").length > 0){
                            $(document).find("#my-requests").find("#loadmore-dm-myrequests").remove();
                        }
                        break;
                }

                $(document).find(".profile-popup").click(function () {
                    $("#qv-user-details").find(".quickview-body").scrollTop(0);
                    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");
                    var id = $(this).attr("data-id");
                    $("#qv-user-details").attr("data-user-id", id);
                    var modal_body = $("#qv-user-details").find(".quickview-body");
                    user_popup(id, modal_body);
                });

                $(document).find("#my-requests").find("#loadmore-dm-myrequests").find("button").unbind("click").on("click",function(){
                    myteam_mydm_requests($(this).attr("data-page"), id_return());
                });

                $(".delete-member").unbind("click").on("click",function(){
                    delete_dm_request($(this).attr("data-delete"));
                });
            }
        }
    });
}