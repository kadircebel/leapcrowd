/**
 * Created by kadircebel on 17.04.2018.
 */

window.fbAsyncInit = function () {
    FB.init({
        // appId: '104239337095491',
        appId:'225690941633654',
        cookie: true,
        xfbml: true,
        version: 'v3.1'
    });

    FB.AppEvents.logPageView();
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function fb_Login() {
    FB.login(function () {
        FB.api(
            '/me',
            'GET',
            {"fields": "id,name,email,last_name,picture{url}"},
            function (response) {
                adminfb_ajaxlogin(response);
            }
        );
    }, {scope: 'public_profile,email'});
}

function adminfb_ajaxlogin(data) {
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".entrepreneur-messages").html('');
                $("#main-loading").show();
            },
            url: '/login-social/',
            data: {
                'email': data.email
            },
            success: function (data) {
                if (data.message_type==="error") {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="mailto:kadir@leapcrowd.co" class="btn btn-lg btn-round btn-danger px-6 mt-5" ><i class="fa fa-minus"></i> '+data.button_label+'</a>');
                }
                else if (data.message_type==="warning") {
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    $("#main-loading").append('<a href="'+ data.redirect_url+'" class="btn btn-lg btn-round btn-warning px-6 mt-5" ><i class="fa fa-warning"></i> '+data.button_label+'</a>');
                }
                else {
                    //success
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    setTimeout(function () {
                            window.location.href = data.redirect_url;
                        }, 2000);
                }
            }
        }
    );
}



