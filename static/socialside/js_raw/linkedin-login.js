/**
 * Created by kadircebel on 18.04.2018.
 */
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

function onSuccess(data) {
    // console.log(data);
    get_linkedindata(data);

}

// Handle an error response from the API call
function onError(error) {
    // console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~:(email-address)?format=json").result(onSuccess).error(onError);
}

function get_linkedindata(data) {
    $.ajax({
            type: 'POST',
            beforeSend: function (xhr, settings) {
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
                $(".entrepreneur-messages").html('');
                $(".team-messages").html('');
                $(".teach-messages").html('');

                $("#main-loading").show();

            },
            url: '/login-social/',
            data: {
                'email': data.emailAddress
            },
            success: function (data) {
                // console.log(data);
                if (data.message_type === "error") {
                    $("#main-loading").hide();
                    $("#main-loading").html('<p>' + data.message + '</p>');
                    setTimeout(function () {
                            window.location.href = data.redirect_url;
                        }, 2000);
                }
                else if (data.message_type === "warning") {
                    $("#main-loading").hide();
                    $(".entrepreneur-messages").html('<p class="alert-warning">' + data.message + '</p>');

                    setTimeout(function () {
                            window.location.href = data.redirect_url;
                        }, 2000);
                }
                else {
                    //success
                    $("#main-loading").html('<p>'+ data.message + '</p>');
                        setTimeout(function () {
                            window.location.href = data.redirect_url;
                        }, 2000);
                }
            },
            error: function (data) {
                // console.log("hata");
            }
        }
    );
}


