/**
 * Created by kadircebel on 18.04.2018.
 */
$.getScript('/static/socialside/js/remodeling.js', function () {
});
$.getScript('/static/socialside/js/db_functions.js', function () {
});
$.getScript('/static/socialside/js/db_loadmore_functions.js', function () {
});
$.getScript('/static/socialside/js/messages_js.js', function () {
});
$.getScript('/static/socialside/js/member-notifications.js', function () {
});
$.getScript('/static/socialside/js/plugin_functions.js', function () {
});
$.getScript('/static/socialside/js/click_operations.js', function () {
});
$.getScript('/static/socialside/js/screen_size.js', function () {
});

app.ready(function () {

    if (location.pathname.indexOf("/login/") === -1) {
        general_size_load();
        general_resize_reload();
        membership_options();
        leap_sidebar();
        var rightMenuClicked = false;
        header_profil_img();
        requests_count();
        notifications_count();
        newmessages_count();
        get_update_count();




    }

    //bütün uyarı işlemleri burda çalışacak
    /*
     mesaj uyarıları
     * */

    if (location.pathname.indexOf("/dashboard/") > -1) {
        if (location.pathname.indexOf("/profiles/") > -1) {
            if (location.pathname.indexOf("/edit/") > -1) {
                if (rightMenuClicked === false) {
                    profileNavMenu(); // sayfa load olduğunda

                }
                else {
                    profileNavMenuClick();
                }
            }
            else if (location.pathname.indexOf("/details/") > -1) {
                get_profile_level();
                profile_experiences();
                profile_investments();
                get_profile_skills();
            }
            else {
                filter_options_profiles();
                // profile_investment_choice();
                // profile_filter_choice();
            }
        }
        else if (location.pathname.indexOf("/projects/") > -1) {
            if (location.pathname.indexOf("/detail/") > -1) {
                project_steps();
                project_teammates();
                get_profile_investments();
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/new-project/") > -1) {
                get_projectAreas();
                get_projectUsageTypes();

                if (rightMenuClicked === false) {
                    getProjectPoints();
                    projectNavMenu();

                }
                else {
                    getProjectPoints();
                    projectNavMenuClick();

                }
            }
            else if (location.pathname.indexOf("/edit/") > -1) {
                get_projectAreas();
                get_projectUsageTypes();
                upload_project_logo();
                if (rightMenuClicked === false) {
                    getProjectPoints();
                    projectNavMenu();
                }
                else {
                    getProjectPoints();
                    projectNavMenuClick();
                }
            }
            else if (location.pathname.indexOf("/requests/") > -1) {
                myprj_requests();
                myproject_requests_nav_click();

            }
            else {
                myproject_nav_menu_click();
                fundedProjectsCount();
            }
        }
        else if (location.pathname.indexOf("/updates/") > -1) {
            get_update_list();
        }
        else if (location.pathname.indexOf("/myideas/") > -1) {
            if (location.pathname.indexOf("/myideas/detail/") > -1) {
                resizeAndEdit();
                myideas_nav_click();
            }
            else {
                //my_ideas
            }
        }
        else if (location.pathname.indexOf("/myprofile/") > -1) {

            if (location.pathname.indexOf("/projects/") > -1) {

            }
            else {
                get_user_skills();
                get_profile_investments();
                get_user_level();
                get_profile_experiences();
            }

        }
        else if (location.pathname.indexOf("/ideas/") > -1) {
            resizeAndEdit();

            if (location.pathname.indexOf("/detail/") > -1) {

            }

        }
        else if (location.pathname.indexOf("/messages/") > -1) {
            $("#new-message-btn").fadeIn();
            messages_menu();
            if (location.pathname.indexOf("/detail/") > -1) {
                $("#new-message-btn").hide();
            }
            else if (location.pathname.indexOf("/sent/") > -1) {

                $(document).find("#messages-btn-group").find("button").attr("disabled", "disabled");
            }
            else if (location.pathname.indexOf("/trash/") > -1) {
                $(document).find("#messages-btn-group").find(".send-archive").attr("disabled", "disabled");
            }
            else if (location.pathname.indexOf("/archive/") > -1) {
                $(document).find("#messages-btn-group").find(".send-archive").attr("disabled", "disabled");
            }
            else {

            }

        }
        else if (location.pathname.indexOf("/myteams/") > -1) {

            if (location.pathname.indexOf("/myteams/detail/") > -1) {
                myteam_nav_click();
            }
            else {
                myteams_nav_menu_click();
            }

        }
        else {
            basic_options();
            get_profile_investments();

            dashboard_near_ideas();
            dashboard_near_members();

            $(document).find("#tag-type").attr('data-provide', '');
            $(document).find("#saved-experiences").hide();
            $("#new-message-btn").hide();
        }
    }

});