/**
 * Created by kadircebel on 23.04.2018.
 */
//if there is a missing part on new member
function basic_options() {

    $.getJSON("/dashboard/basic-options/", function (data) {
        $.each(data, function (index) {
            $(document).find("#option-messages").append(basic_messages_html(data[index]["content"], data[index]["hyperlink"], data[index]["linkText"], data[index]["type"]));
        });
    });
}

function basic_messages_html(messageContent, messageLink, messageLinkText, messageType) {
    var message = "";
    if (messageType === "error") {
        message = "";
        message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
        message += '<button type="button" class="close cursor-pointer" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        message += '<p class="mb-0 pr-3">' + messageContent + '<a href="' + messageLink + '" class="text-danger"><b>' + messageLinkText + '</b></a></p>';
    }
    else {
        message = "";
        message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
        message += '<button type="button" class="close cursor-pointer" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
        message += '<p class="mb-0 pr-3">' + messageContent + '<a href="' + messageLink + '" class="text-danger"><b>' + messageLinkText + '</b></a></p>';
    }

    return message;

}

function header_profil_img() {
    $.getJSON("/dashboard/myprofile/get-profile-photo/", function (data) {
        $("#profile-header-img").attr("src", data.profile_photo);
    });
}

function membership_options() {
    $.ajax({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        url: "/dashboard/memberships/membership-options/",
        method: 'post',
        // async:false,
        data: {},
        success: function (data) {

        },
        complete: function (jqXHR, status) {
            if (status === "success") {
                var data_json = $.parseJSON(jqXHR.responseText);
                switch (data_json.message_type) {
                    case "warning":
                        toastMessage(data_json.message, messageStr("ok-message"), "", "warning");
                        break;
                    case "error":
                        toastMessage(data_json.message, messageStr("ok-message"), "", "danger");
                        break;
                }
            }

        }


    });
}