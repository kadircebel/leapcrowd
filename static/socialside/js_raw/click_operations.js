/**
 * Created by kadircebel on 10.06.2018.
 */

function myteams_nav_menu_click() {
    $("#myteams-nav").find(".nav-tabs").find(".nav-item").on("click", function () {
        switch ($(this).index()) {
            case 0:
                break;
            case 1:
                $("#tab-cofounded").html('');
                loadmore_cofunded_projects_myteam(1);
                break;
        }
    });
}

function myproject_nav_menu_click() {
    $("#myproject-nav").find(".nav-tabs").find(".nav-item").on("click", function () {
        switch ($(this).index()) {
            case 0:
                $("#tab-projects").find(".project-list").html('');
                myproject_list(1);
                break;
            case 1:
                $("#tab-feedbacks").find(".project-list").html('');
                myproject_feedbacks_teamrequests(1);
                break;
            case 2:
                $("#tab-cofunded").find(".project-list").html('');
                coFunded_projects(1);
                break;
            case 3:
                break;

            default:
                break;
        }
    });
}

$(document).find("#loadmore-myprojects").find("button").on("click", function () {
    var page = $(this).attr("data-page");
    myproject_list(page);
});

$(document).find("#loadmore-cofunded").find("button").on("click", function () {
    var page = $(this).attr("data-page");
    coFunded_projects(page);
});

function myproject_requests_nav_click() {
    $("#project-requests-nav").find(".nav-tabs").find(".nav-item").on("click", function () {
        var id = $("#project-requests-nav").attr("data-id");
        switch ($(this).index()) {
            case 0:
                myprj_requests();

                if ($("#users-requests").find(".alert-info").length > 0) {
                    $("#users-requests").find(".alert-info").last().remove();
                }
                if ($("#users-requests").find(".alert-danger").length > 0) {
                    $("#users-requests").find(".alert-danger").last().remove();
                }
                break;
            case 1:
                project_undecided_teamrequests(id);
                break;
            case 2:
                $("#feedbacks").find(".feedback-list").html('');
                project_feedbacks(id, 1);
                break;
        }
    });
}


function profileNavMenuClick() {
    $("#profile-right-menu").find(".nav-tabs").find(".nav-item").click(function () {
        // $("#skill-list").hide();
        // $(".positions-result").hide().html('');
        // $("#position-title-list").hide();

        rightMenuClicked = true;
        switch ($(this).index()) {
            case 0:
                $(document).find("#saved-experiences").hide();
                // positionSearch();
                break;
            case 1:
                $("#saved-experiences").show();
                showExperiences();
                positionSearch();
                break;
            case 2:
                skillAddToProfile();
                showSkills();
                $(document).find("#saved-experiences").hide();
                if ($("#skill-container").css("display") === "block") {
                    $("#skills").val('');
                    $("#skill-container").hide();
                }
                break;
            case 3:
                $(document).find("#saved-experiences").hide();
                selectInvestments();
                selectProjectHours();
                selectStartupHistory();
                supportType();

                break;
            default:
                $("#saved-experiences").hide();
                break;
        }
    });

    if ($("#profile-right-menu").find(".nav-tabs").find(".nav-item").index() === 0) {
        $(document).find("#saved-experiences").hide();
    }
}

function projectNavMenuClick() {
    $("#project-nav-menu").find(".nav-tabs").find(".nav-item").click(function () {
        rightMenuClicked = true;
        switch ($(this).index()) {
            case 0:
                get_projectAreas();
                get_projectUsageTypes();
                upload_project_logo();
                // getSelectedUsageTypes();
                break;
            case 1:

                break;
            case 2:

                break;
            case 3:

                break;
            case 4:
                get_business_stages();
                break;
            case 5:
                get_project_options("give-feedback");
                get_project_options("send-teamrequest");
                get_project_options("publish-project");
                break;

            default:

                break;
        }
    });
}

$("#position-titles").unbind("keyup").bind("keydown", function () {
    title_text = $(this).val();
    position_title_list(title_text);
});

$(document).click(function () {

    if ($("#position-title-list").css("display") === "block") {
        $("#position-title-list").hide();
    }
    else if ($(".positions-result-container").css("display") === "block") {
        $(".positions-result-container").hide();
    }
    // else if ($("#skill-container").css("display") === "block"){
    //     // $("#skills").val('');
    //     // $("#skill-container").hide();
    // }
});

// $("#position-title-list").mouseleave(function () {
//     $("#position-titles").val('');
//     $(this).fadeOut();
// });

// $(".positions-result-container").mouseleave(function () {
//     $("#profil2").find(".position-title").val('');
//     $(this).fadeOut();
// });

// $("#skill-container").mouseleave(function () {
//     $("#skills").val('');
//     $(this).fadeOut();
// });

$(document).on("click", "#cover-photo", function () {
    change_cover_photo();
});

$(document).on("click", ".btn-facebook", function () {
    fb_Login();
});

$(document).on("click", ".btn-linkedin", function () {
    if (!IN.User.isAuthorized()) {
        var linkedbtn = $(".IN-widget > span > span");
        $(linkedbtn).click();
    }
    else {
        getProfileData();
    }
});

$(document).on("click", '#remember-btn', function () {
    if (isEmailValid($("#email").val()) && $("#email").val() !== "") {
        if ($("#email").hasClass("border-danger")) {
            $("#email").removeClass("border-danger");
        }
        requestChangePass($("#email").val());
    }
    else {
        $("#email").addClass("border-danger");
    }
});

$(document).on("click", "#login-form-btn", function () {
    emailInput = $("#login-form").find("#emailaddress").val();
    passInput = $("#login-form").find("#password").val();
    var formActive = false;

    if (isEmailValid($("#login-form").find("#emailaddress").val()) && $("#login-form").find("#emailaddress").val().length > 0 && $("#login-form").find("#emailaddress").val() !== "") {
        if ($("#login-form").find("#emailaddress").hasClass("border-danger")) {
            $("#login-form").find("#emailaddress").removeClass("border-danger");
        }
        formActive = true;
    }
    else {
        $("#login-form").find("#emailaddress").addClass("border-danger");
        formActive = false;
    }

    if ($("#login-form").find("#password").val() !== "" && $("#login-form").find("#emailaddress").val().length > 0) {
        if ($("#login-form").find("#password").hasClass("border-danger")) {
            $("#login-form").find("#password").removeClass("border-danger");
        }
        formActive = true;
    }
    else {
        $("#login-form").find("#password").addClass("border-danger");
        formActive = false;
    }

    if (formActive === true) {
        loginForm($("#login-form").find("#emailaddress").val(), $("#login-form").find("#password").val());
    }
});

$(document).on("click", "#change-pass-btn", function () {
    var pass1 = $(document).find("#change-pass-form").find("#password").val();
    var repass = $(document).find("#change-pass-form").find("#repassword").val();
    var user_id = getQerystringByName('user');
    var querypass = getQerystringByName('rp');

    if (pass1 === repass && pass1 !== "" && repass !== "" && pass1 !== " " && repass !== " " && pass1.length >= 8 && repass.length >= 8) {
        if ($(document).find("#change-pass-form").find("#password").hasClass("border-danger")) {
            $(document).find("#change-pass-form").find("#password").removeClass("border-danger");
        }
        if ($(document).find("#change-pass-form").find("#repassword").hasClass("border-danger")) {
            $(document).find("#change-pass-form").find("#repassword").removeClass("border-danger");
        }
        changePasswordSubmit(repass, user_id, querypass);
    }
    else {
        if (pass1 !== repass && pass1 === "" && pass1 === " " && pas1.length < 8) {
            $(document).find("#change-pass-form").find("#password").addClass("border-danger");
        }
        else if (repass !== pass1 && repass !== " " && repass !== "" && repass.length < 8) {
            $(document).find("#change-pass-form").find("#repassword").addClass("border-danger");
        }
    }

});

$(document).on("click", "#general-update", function () {
    generalInformationsSave();
});

var isStillThere = false;

$(document).on("change", '.still-there :checkbox', function () {
    if (isStillThere === true) {
        isStillThere = false;
        $("." + $(this).data("endyear-title")).fadeIn();
        $(this).find("input[type='checkbox']").prop("checked", isStillThere);

    }
    else {
        isStillThere = true;
        $("." + $(this).data("endyear-title")).fadeOut();
        $(this).find("input[type='checkbox']").prop("checked", isStillThere);
    }

});

$(document).find("#profil2").find("#experience-save").on("click", function () {
    var situation = $(this).data("situation");

    if (exlist().length > 0 && $("#experiences-container").find(".main-div").length === exlist().length) {
        $(".add-positions-result").fadeOut();
        switch (situation) {
            case "create":
                $.ajax({
                        type: 'POST',
                        beforeSend: function (xhr, settings) {
                            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                                // Only send the token to relative URLs i.e. locally.
                                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                            }
                            $(".preloader").fadeIn();
                        },
                        url: '/dashboard/profiles/save-experiences/',
                        data: {'list': JSON.stringify(exlist())},
                        success: function (data) {
                            $(".preloader").fadeOut();
                            switch (data.message_type) {
                                case "success":
                                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                                    removeExperiencesLatestAdded();
                                    showExperiences();
                                    getUserPoints();
                                    break;
                                case "error":
                                    toastMessage(data.message, messageStr("ok-message"), "", "danger");

                                    break;
                            }

                        },
                        error: function (data) {

                        }
                    }
                );
                break;
            case "update":
                var company_infos_id = $(this).data("id");
                $.ajax({
                        type: 'POST',
                        beforeSend: function (xhr, settings) {
                            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                                // Only send the token to relative URLs i.e. locally.
                                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                            }
                            $(".preloader").fadeIn();
                        },
                        url: '/dashboard/profiles/update-experience/',
                        data: {'list': JSON.stringify(exlist()), 'id': company_infos_id},
                        success: function (data) {
                            $(".preloader").fadeOut();
                            switch (data.message_type) {
                                case "success":
                                    toastMessage(data.message, messageStr("ok-message"), "", "success");
                                    removeExperiencesLatestAdded();
                                    showExperiences();
                                    getUserPoints();
                                    break;
                                case "error":
                                    toastMessage(data.message, messageStr("ok-message"), "", "danger");
                                    break;
                            }

                        },
                        error: function (data) {

                        }
                    }
                );
                break;
        }
    }
    else {
        $(".add-positions-result").fadeIn();
    }
});

$(document).on("click", "#save-skills", function () {
    saveSkills(allSkills);
});

$(document).on("click", "#save-basics", function () {
    var project_suply = "0";
    var hour_cost = "0";
    var project_hour = "0";
    var startup_history = "0";

    if ($("#project-supply")[0].selectedIndex !== 0) {
        project_suply = $("#project-supply").val();
    }
    if ($("#project-hour-cost").val().trim() !== "") {
        hour_cost = $("#project-hour-cost").val();
    }
    if ($("#project-hour")[0].selectedIndex !== 0) {
        project_hour = $("#project-hour").val();
    }
    if ($("#startup-history")[0].selectedIndex !== 0) {
        startup_history = $("#startup-history").val();
    }
    saveBasics(project_suply, hour_cost, project_hour, startup_history);

});

$(document).on("click", "#project-generals-save", function () {
    save_project_generalinformations();
});

$(document).on("click", "#projcts-business-save", function () {
    save_project_businessModel();
});

$(document).on("click", "#project-requirements-save", function () {
    save_project_requirementsModel();
});

$(document).on("click", "#project-competitors-save", function () {
    save_project_competitorsModel();
});

$(document).on("click", ".remove-logo", function () {
    removeProjectLogo();
});


$(document).on("change", '#give-feedback :checkbox', function () {
    var value_str = $(this).attr("data-value");
    var situation = $(this).attr("data-situation");
    if (situation.indexOf("false") > -1) {
        situation = "true";
    }
    else {
        situation = "false";
    }
    $(this).attr("data-situation", situation);
    set_project_options(value_str, situation);
    return false;
});

$(document).on("change", '#send-teamrequest :checkbox', function () {
    var value_str = $(this).attr("data-value");
    var situation = $(this).attr("data-situation");
    if (situation.indexOf("false") > -1) {
        situation = "true";
    }
    else {
        situation = "false";
    }
    $(this).attr("data-situation", situation);

    set_project_options(value_str, situation);
    return false;
});

$(document).on("change", '#publish-project :checkbox', function () {
    var value_str = $(this).attr("data-value");
    var situation = $(this).attr("data-situation");
    if (situation.indexOf("false") > -1) {
        situation = "true";
    }
    else {
        situation = "false";
    }
    $(this).attr("data-situation", situation);
    set_project_options(value_str, situation);
    // return false;
});

$(document).find(".profile-popup").click(function () {
    $("#qv-user-details").find(".quickview-body").scrollTop(0);
    $("#qv-user-details").find(".quickview-body").perfectScrollbar("update");

    var id = $(this).attr("data-id");

    $("#qv-user-details").attr("data-user-id", id);

    var modal_body = $("#qv-user-details").find(".quickview-body");
    user_popup(id, modal_body);

});

$("#share-quick-idea").click(function () {

    share_quick_idea();
});


$(document).find("#filter-results").on("click", function () {
    filter_ideas_list(city_str, country_str, unanswered_str, all_str);
});


$("#load-more-ideas").find("button").on("click", function () {
    var counter_ideas = $(this).attr("data-page");

    more_load_ideas(city_str, country_str, unanswered_str, all_str, counter_ideas);
});


$(document).find(".post-item").on("click", function () {
    var data_target;
    var data_toggle;
    if (location.pathname.indexOf("/myideas/detail/") > -1) {
        data_target = $(this).attr("data-target");
        data_toggle = $(this).attr("data-toggle");
    }
    else {
        data_target = $(this).find(".card-body").attr("data-target");
        data_toggle = $(this).find(".card-body").attr("data-toggle");
    }

    var post = $(this).attr("data-post");
    if (data_target.length > 0 && data_toggle.length > 0) {
        ideas_popup(post);
        get_post_answers(post, "popup-page");
    }
    else {
        ideas_detail(post);
    }


});


/*Detail Page*/

$(".ideas-nav-detail").find(".write-answer").on("click", function () {
    var p_user = $("#post-detail-page").attr("data-name");
    $("#modal-answer").find(".send-answer").attr("data-btn-role", "main-answer");
    $("#answer-content").summernote('code', '');
    $("#modal-answer").find(".modal-title").find("span").text(p_user);

});

$(".ideas-nav-detail").find(".smilar-project").on("click", function () {
    var post_id = $("#post-detail-page").attr("data-post");
    ideas_label_infos(post_id, "similar-project", "post-page");

});

$(".ideas-nav-detail").find(".send-teamrequest").on("click", function () {
    var post_id = $("#post-detail-page").attr("data-post");

    get_project_supports(post_id);
    ideas_label_infos(post_id, "team-request", "post-page");
});

/*Detail Page*/

$(document).find("#qv-post-detail").find(".write-answer").on("click", function () {

    var p_user = $(document).find("#qv-post-detail").find("#post-detail").attr("data-name");
    $("#modal-answer").find(".send-answer").attr("data-btn-role", "main-answer");
    $("#answer-content").summernote('code', '');
    $("#modal-answer").find(".modal-title").find("span").text(p_user);

    // bu kısımdan içerik yazılacak ve send butonuna tıklanılacak
});

/*Detail Page*/
$("#post-detail-page").find(".sub-answer-btn").click(function () {

    var p_user = $(this).closest(".media").attr("data-name");
    var answer_id = $(this).closest(".media").attr("data-answers");
    answer_text = $("#answer-content").val();

    $("#modal-answer").find(".modal-title").find("span").text(p_user);
    $("#modal-answer").find(".send-answer").attr("data-btn-role", "sub-answer");
    $("#modal-answer").find(".send-answer").attr("data-answer", answer_id);
});

$("#post-detail-page").find(".main-answer-btn").click(function () {

    var p_user = $("#post-detail-page").attr("data-name");
    $("#modal-answer").find(".modal-title").find("span").text(p_user);
    $("#modal-answer").find(".send-answer").attr("data-btn-role", "main-answer");
    $("#modal-answer").find(".send-answer").attr("data-answer", "");
});

$("#post-answers-detail").find(".load-more-subanswers").on("click", function () {

    if ($("#post-answers-detail").find(".main-answers").length <= 5) {
        var answer_id = $(this).closest(".main-answers").attr("data-answers");
        var page = $(this).attr("data-page");
        more_load_sub_answers(answer_id, page, "detail-page");
    }
});

$("#post-answers-detail").find(".load-more-mainanswers").on("click", function () {
    var post_id = $("#post-detail-page").attr("data-post");
    var page = $(this).attr("data-page");
    more_load_main_answers(post_id, page, "detail-page");

});

/*Detail Page*/
$(document).find("#modal-answer").find(".send-answer").click(function () {
    var btnRole = $(this).attr("data-btn-role");

    switch (btnRole) {
        case "main-answer":
            if (location.pathname.indexOf("/ideas/detail/") > -1) {
                var n_post_id = $("#post-detail-page").attr("data-post");
                var answer_text = $("#answer-content").val();
                write_answerToMainPost(n_post_id, answer_text, "detail-page");
            }
            else {
                var n_post_id = $(document).find("#qv-post-detail").find("#post-detail").attr("data-post");
                var answer_text = $("#answer-content").val();
                write_answerToMainPost(n_post_id, answer_text, "popup-page");
            }


            break;
        case "sub-answer":
            if (location.pathname.indexOf("/ideas/detail") > -1) {
                var n_answer_id = $(this).attr("data-answer");
                var answer_text = $("#answer-content").val();
                write_answerToAnswer(n_answer_id, answer_text, "detail-page");
            }
            else {
                var n_answer_id = $(this).attr("data-answer");
                var answer_text = $("#answer-content").val();
                write_answerToAnswer(n_answer_id, answer_text, "popup-page");
            }


            break;
    }

    $("#answer-content").summernote('code', '');

});
/*Detail Page*/


$(document).find("#qv-post-detail").find("aside").find(".smilar-project").on("click", function () {
    var post_id = $("#post-detail").attr("data-post");
    countCharacter("project-describe", 20, 350);
    ideas_label_infos(post_id, "similar-project", "post-page");
});

$(document).find("#qv-post-detail").find("aside").find(".send-teamrequest").on("click", function () {
    var post_id = $("#post-detail").attr("data-post");
    //burda pencere yeniden açıldığında selectboxları kaldırmak lazım
    get_project_supports(post_id);
    ideas_label_infos(post_id, "team-request", "post-page");
});

$(document).find("#send-feedback").on("click", function () {
    ideas_label_infos(id_return(), "send-feedback", "project-page");
});

$(document).find("#modal-feedback").find(".send-feedback-desc").on("click", function () {

    var txtContent = $("#feedback-desc").val();
    if (txtContent.trim().length > 0) {
        if (txtContent.trim().length >= 112) {
            send_feedbackToProject(txtContent, id_return());
            $("#feedback-desc").val('');
        }
        else {
            toastMessage(messageStr("feedback-content-less"), messageStr("ok-message"), "", "warning");
        }
    }
    else {
        toastMessage(messageStr("feedback-content-atleast"), messageStr("ok-message"), "", "danger");
    }
});

$(document).find("#send-prj-teamrequest").on("click", function () {
    get_users_supports(id_return());
    ideas_label_infos(id_return(), "send-teamrequest", "project-page");
});

$(document).find("#similar-confirm").on("click", function () {

    if ($("#project-link").val().trim().length === 0 && $("#project-describe").val().trim().length === 0) {
        toastMessage(messageStr("similar-project-send"), messageStr("ok-message"), "", "danger");
    }
    else {
        var post_id = 0;
        if (location.pathname.indexOf("/ideas/detail/") > -1) {
            post_id = $("#post-detail-page").attr("data-post");
        }
        else {
            post_id = $("#post-detail").attr("data-post");
        }
        $("#modal-smilar-project").find(".invalid-feedback").hide();
        //detail page
        add_smilar_project(post_id, $("#project-link").val().trim(), $("#project-describe").val().trim());

        $("#project-link").val("");
        $("#project-describe").val("");
    }

});

$(document).find("#modal-team-request").find(".set-hours").on("click", function () {
    $(this).hide();
    var post_id = 0;
    if (location.pathname.indexOf("/ideas/detail/") > -1) {
        post_id = $("#post-detail-page").attr("data-post");
        setup_project_hours(post_id, "post-page");
    }
    else if (location.pathname.indexOf("/projects/detail/") > -1) {
        setup_project_hours(id_return(), "project-page");
    }
    else {
        post_id = $("#post-detail").attr("data-post");
        setup_project_hours(post_id, "post-page");
    }


    $("#select-hours").show();
});

$(document).find("#modal-team-request").find(".set-support").on("click", function () {
    $(this).hide();
    var post_id = 0;
    if (location.pathname.indexOf("/ideas/detail/") > -1) {
        post_id = $("#post-detail-page").attr("data-post");
        setup_project_investments(post_id, "post-page");
    }
    else if (location.pathname.indexOf("/projects/detail/") > -1) {

        setup_project_investments(id_return(), "project-page");
    }
    else {
        post_id = $("#post-detail").attr("data-post");
        setup_project_investments(post_id, "post-page");
    }


    $("#select-investments").show();
});

$(document).find("#modal-team-request").find(".send-team-request").on("click", function () {
    var post_id = 0;
    if (location.pathname.indexOf("/ideas/detail/") > -1) {
        post_id = $("#post-detail-page").attr("data-post");
        send_user_teamrequest(post_id, "post-page");
    }
    else if (location.pathname.indexOf("/projects/detail/") > -1) {

        send_user_teamrequest(id_return(), "project-page");
    }
    else {
        post_id = $("#post-detail").attr("data-post");
        send_user_teamrequest(post_id, "post-page");
    }


});

$(document).find("#modal-idea").find(".send-idea").on("click", function () {

    var ideaTxt = $("#idea-content").val();

    if (ideaTxt.trim().length > 0) {
        share_idea(content_html(ideaTxt));
    }
    else {
        //mesaj koy

    }
});

$(document).find("#filter-list-projects").on("click", function () {
    filter_projects(project_option, project_stage);
});

$(document).find("#load-more-projects").find("button").on("click", function () {
    var pageStr = $(this).attr("data-page");
    load_more_projects(project_option, project_stage, pageStr);
});

$(document).find("header.topbar").find(".topbar-right").find(".notification-teamrequest").on("click", function () {
    get_teamrequests();
});

$(document).find("header.topbar").find(".topbar-right").find(".general-notifications").on("click", function () {
    get_general_notifications();
});

$(document).find("header.topbar").find(".topbar-right").find(".notification-messages").on("click", function () {
    get_messages();
});


$(document).find("#users-requests").find("#load-more-requests").find("button").on("click", function () {
    var prj_id = $(this).attr("data-id");
    var page = $(this).attr("data-page");
    loadmore_prj_teamrequests(prj_id, page);
});

$(document).find("#load-more-undecided").find("button").on("click", function () {
    var prj_id = $(this).attr("data-id");
    var page = $(this).attr("data-page");
    loadmore_project_undecided_teamrequests(prj_id, page);
});


$(document).find("#load-more-similars").find("button").on("click", function () {
    var id = $(this).attr("data-id");
    var page = $(this).attr("data-page");
    loadmore_smilars(id, page);
});

$(document).find("#loadmore-ideasrequest").find("button").on("click", function () {
    var id = $(this).attr("data-id");
    var page = $(this).attr("data-page");
    myideas_teamrequest(id, page, true);
});

function myteam_nav_click() {
    $("#my-teams-nav").find(".nav-tabs").find(".nav-item").on("click", function () {
        switch ($(this).index()) {
            case 0:
                break;
            case 1:
                myteam_mydm_requests(1, id_return());
                break;
        }
    })
}

function myideas_nav_click() {
    $("#myideas-nav").find(".nav-tabs").find(".nav-item").on("click", function () {
        var id = $("#myideas-nav").attr("data-id");

        switch ($(this).index()) {
            case 0:
                break;
            case 1:
                $("#tab-team").find(".requests-list").html('');
                $("#loadmore-ideasrequest").find(".result-message").remove();

                if ($("#tab-team").find(".alert-info").length > 0) {
                    $("#tab-team").find(".alert-info").last().remove();
                }
                if ($("#tab-team").find(".alert-danger").length > 0) {
                    $("#tab-team").find(".alert-danger").last().remove();
                }
                if ($("#tab-team").find("#current-city").length === 0) {
                    $("#tab-team").append('<input type="hidden" id="current-city" />');
                }
                else {
                    $("#tab-team").find("#current-city").remove();
                }

                if ($("#tab-team").find("#same-counter").length === 0) {
                    $("#tab-team").append('<input type="hidden" id="same-counter" />');
                }
                else {
                    $("#tab-team").find("#same-counter").remove();
                }

                myideas_teamrequest(id, 1);
                break;
            case 2:
                $("#tab-undecided").find(".undecided-list").html('');
                myideas_undecided_requests(id, 1);
                break;
        }
    });
}

$(document).find("#loadmore-feedbacks").find("button").on("click", function () {
    var id = $(this).attr("data-id");
    var page = $(this).attr("data-page");
    project_feedbacks(id, page);
});

$(document).find("#loadmore-ideas-undecided").find("button").on("click", function () {
    var id = $(this).attr("data-id");
    var page = $(this).attr("data-page");
    myideas_undecided_requests(id, page);
});

$(document).find("#fix-filter-menu").find(".fab-buttons").find("li").each(function () {
    $(this).find(".btn").unbind("click").click(function () {
        var data_str = $(this).attr("data-btn-type");
        var page = $(this).attr("data-page");
        var id = $(this).attr("data-id");
        fix_menu_btn_types(id, page, data_str, "project-page");
    });
});

$(document).find("#ideas-fix-filter").find(".fab-buttons").find("li").each(function () {
    $(this).find(".btn").unbind("click").click(function () {
        var data_str = $(this).attr("data-btn-type");
        var page = $(this).attr("data-page");
        var id = $(this).attr("data-id");
        fix_menu_btn_types(id, page, data_str, "ideas-page");
    });

});

$(document).find("#filter-profiles-btn").on("click", function () {
    var page = $(this).attr("data-page");
    filter_profiles(choice_str, investment_choice, page);
});

$(document).find("#loadmore-ntf-requests").find("button").on("click", function () {
    var page = $(this).attr("data-page");
    loadmore_ntfc_teamrequest(page);
});

$(document).find("#loadmore-ntf-general").find("button").on("click", function () {
    var page = $(this).attr("data-page");
    loadmore_general_ntfc(page)
});

$(document).find("#general-notifications").find(".notification-content").each(function () {
    $(this).find(".item-answer").unbind("click").on("click", function () {
        $("#quick-answer-content").summernote('code', '');
        var answer_id = $(this).attr("data-answers");
        $("#modal-quick-answer").attr("data-answers", answer_id);
        quick_answer(answer_id).done(function (data) {
            $("#modal-quick-answer").find(".spinner-circle-shadow").remove();
            $("#modal-quick-answer").find("hr").before(quick_answer_item(data.profile_name, data.created_date, data.profile_image, data.answer_content, data.like_count, data.answer_count));
        });
    });
});

$(document).find("#modal-quick-answer").find(".send-answer").on("click", function () {
    var answer_id = $("#modal-quick-answer").attr("data-answers");
    var answer_content = $("#quick-answer-content").val();
    write_quick_answer(answer_id, content_html(answer_content));
});

$("#load-more-container").find("button").on("click", function () {
    var page = $(this).attr("data-page");
    more_load(id_return(), investment_choice, page);
});


$(document).find("#new-message-btn").find("a").on("click", function () {
    message_sweet_popup("<h3 class='mb-8 card-title'>" + messageStr("send-message-title") + "</h3>", true, false, true, button_text("send-message"), button_text("cancel-btn"));
});

$(document).find("#send-reply-message").on("click", function () {
    body_text = $(document).find("#reply-form").find("#reply-content").val().trim();
    send_reply_message(body_text);
});

$(document).find("#loadmore-inbox").on("click", function () {
    var page = $(this).attr("data-page");
    loadmore_inbox(page);
});


$(document).find("#select-all-messages").find(".custom-checkbox :checkbox").unbind("change").on("change", function () {

    if ($(this).is(":checked")) {
        $(document).find("#messages-list").find(".media").find(".custom-checkbox :checkbox").each(function () {
            if ($.inArray($(this).val(), selected_messages) === -1) {
                selected_messages.push(($(this).val()));
            }
        });
    }
    else {
        $(document).find("#messages-list").find(".media").find(".custom-checkbox :checkbox").each(function () {
            selected_messages.splice(selected_messages.indexOf($(this).val()), 1);
        });
    }


});


$(document).find("#messages-btn-group").find(".send-archive").on("click", function () {
    send_archive_messages(selected_messages);
});

$(document).find("#messages-btn-group").find(".send-trash").on("click", function () {
    if (location.pathname.indexOf("/trash/") > -1) {
        send_delete_messages(selected_messages);
    }
    else {
        send_trash_messages(selected_messages);
    }

});

$(document).find("#loadmore-msg-detail").on("click", function () {
    var page = $(this).attr("data-page");
    loadmore_message_detail(page);
});

$(document).find("#person-list").find(".card").each(function () {
    $(this).find(".send-team-request").unbind("click").on("click", function () {
        var profile_id = $(this).parent().parent().parent().find(".profile-popup").attr("data-id");

        general_send_teamrequest_to_person(profile_id);
    });

    $(this).find(".send-dm-message").unbind("click").on("click", function () {
        var id = $(this).parent().parent().parent().find(".profile-popup").attr("data-id");
        send_dm_message(" ", true, false, true, button_text("send-message"), button_text("cancel-btn"), id);
    });
});

$(document).find("#loadmore-myteams").find("button").on("click", function () {
    var page = $(this).attr("data-page");
    loadmore_myteams(page);
});

$(document).find("#my-team").find(".loadmore-detail").on("click", function () {
    var page = $(this).attr("data-page");
    loadmore_myteam_table(page);
});

$(document).find("#my-team").find(".team-members-table").find(".delete-member").on("click", function () {
    var id = $(this).attr("data-delete");
    info_sweet_popup(messageStr("delete-member-title"), messageStr("delete-member-content"), "", true, false, messageStr("option-yes"), messageStr("option-no"), id, "delete");
});

// $("#notification-teamrequest").find(".media-list").find(".item-dm").on("click", function () {
//     console.log("click dosyasından");
//         var dm_id = $(this).attr("data-id");
//         requested_dm_project_info(dm_id).done(function (data) {
//             dm_sweet_popup(dm_id, dm_project_item(data.project_title, data.project_content, data.project_address, data.project_logo, data.project_location, data.project_point), true, false, messageStr("request-confirm-btn-text"), messageStr("request-cancel-btn-text"), messageStr("request-successful-text-title"), messageStr("request-successful-text-content"), messageStr("request-cancel-message-title"), messageStr("request-cancel-message-text-content"));
//         });
//
//     });