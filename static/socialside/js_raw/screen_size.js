/**
 * Created by kadircebel on 08.09.2018.
 */
function general_size_load() {

    if ($(window).width() <= 320) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        $(document).find("header.topbar").find(".show-on-mobile").find(".topbar-btns").find("li:last-child").addClass("d-none");

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().removeClass("order-sm-first order-first").addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }

        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");

        }

    }
    else if ($(window).width() <= 360) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");

        }

    }
    else if ($(window).width() <= 375) {

        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {

            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").addClass("fs-25");
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {

            }
        }
    }
    else if ($(window).width() <= 384) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").addClass("fs-25");
        }

    }
    else if ($(window).width() <= 400) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {

            }
        }

    }
    else if ($(window).width() <= 412) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");

        }

    }
    else if ($(window).width() <= 414) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").addClass("fs-25");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

    }
    else if ($(window).width() <= 480) {

        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {

            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").addClass("fs-30");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").addClass("fs-30");
        }

    }
    else if ($(window).width() <= 533) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
        }

    }
    else if ($(window).width() <= 568) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {

            }

        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }

    }
    else if ($(window).width() <= 600) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {

            }
        }

    }
    else if ($(window).width() <= 640) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {

            }
        }


        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }
    }
    else if ($(window).width() <= 667) {

        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {

            }
        }


        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }
    }
    else if ($(window).width() <= 684) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().removeClass("order-sm-first order-first").addClass("order-first");
            }
            else {

            }
        }
    }
    else if ($(window).width() <= 720) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }

    }
    else if ($(window).width() <= 732) {

        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            // console.log("????");
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }

    }
    else if ($(window).width() <= 736) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {

            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }

    }
    else if ($(window).width() <= 768) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {

        }

    }
    else if ($(window).width() <= 800) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");

        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }

    }
    else if ($(window).width() <= 812) {

        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");

        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {

        }

    }
    else if ($(window).width() <= 960) {

    }
    else if ($(window).width() <= 980) {

    }
    else if ($(window).width() < 1024) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }

    }
    else if ($(window).width() === 1024) {
        $(document).find("header.topbar").find(".show-on-mobile").hide();
        $(document).find("header.topbar").find(".hide-dsktp-right").show();
        if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $("body").addClass("sidebar-folded");
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").addClass("fs-40");
        }

    }
    else if ($(window).width() > 1024) {
        $("body").removeClass("sidebar-folded");
        $(document).find("header.topbar").find(".show-on-mobile").hide();
        $(document).find("header.topbar").find(".hide-dsktp-right").show();
        $(document).find(".show-on-mobile").hide();
        $(document).find(".hide-dsktp-right").show();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
    }
    else {
        $(document).find("header.topbar").find(".show-on-mobile").hide();
        $(document).find("header.topbar").find(".hide-dsktp-right").show();

        $(document).find(".hide-dsktp-right").show();
        $(document).find(".show-on-mobile").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
    }
}

function general_resize_works() {
    console.log($(this).width());
    console.log("**********");
    if ($(this).width() <= 320) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        $(document).find("header.topbar").find(".show-on-mobile").find(".topbar-btns").find("li:last-child").addClass("d-none");

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first").addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();

            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").addClass("fs-25");
        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }

    }
    else if ($(this).width() <= 360) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {

                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }

        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();

            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").addClass("fs-25");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

    }
    else if ($(this).width() <= 375) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {

                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().removeClass("order-first order-sm-first").addClass("order-first");

            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "overflow-x": "scroll",
                    "overflow-y": "hidden",
                    "flex-wrap": "nowrap"
                });
            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();

            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").addClass("fs-25");
        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {
                //         $("#my-team").find(".team-members-table").find("tbody").find("tr").find("td").find("a").find("span").addClass("text-truncate");
                // $("#my-team").find(".team-members-table").find("tbody").find("tr").find("td:nth-child(2)").addClass("text-truncate");
            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 384) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();

            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 400) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }
    }
    else if ($(this).width() <= 412) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").removeClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();

            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 414) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").removeClass("fs-30").addClass("fs-25");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length > 0 && $("#project-nav-menu").find(".flex-column").length > 0) {
                    $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").css({
                        "flex-wrap": "nowrap",
                        "overflow-y": "hidden",
                        "overflow-x": "scroll"
                    });
                }

                $("#project-nav-menu").parent().removeClass("order-sm-first").addClass("order-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }
    }

    else if ($(this).width() <= 480) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-40").removeClass("fs-25").addClass("fs-30");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-40").removeClass("fs-25").addClass("fs-30");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {
                $("#myproject-nav").find(".nav").css({
                    "overflow-y": "hidden",
                    "overflow-x": "scroll",
                    "flex-wrap": "nowrap"
                });
            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }
    }
    else if ($(this).width() <= 533) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 568) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }

        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 600) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 640) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 667) {

        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first").addClass("order-sm-first");
            }
            else {

            }
        }

        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }
    }
    else if ($(this).width() <= 684) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/requests/") > -1) {
                $(document).find("#fix-filter-menu").addClass("fab-dir-left");
            }
            else if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }

        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").remove("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 720) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 732) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().addClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 736) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").removeClass("nav-lg flex-column");
                $("#project-nav-menu").find(".nav-tabs").css({
                    "flex-wrap": "nowrap",
                    "overflow-y": "hidden",
                    "overflow-x": "scroll"
                });
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first").addClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 768) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                $("#project-nav-menu").parent().removeClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 800) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                $("#project-nav-menu").parent().removeClass("order-sm-first");
            }
            else {

            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else if ($(this).width() <= 812) {
        $(document).find("header.topbar").find(".show-on-mobile").show();
        $(document).find("header.topbar").find(".hide-dsktp-right").hide();

        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").addClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length < 1 && $("#project-nav-menu").find(".flex-column").length < 1) {
                    $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                }

                $("#project-nav-menu").parent().removeClass("order-sm-first order-first");

            }
            else {
                $("#myproject-nav").find(".nav").attr("style", "");
            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }
    }
    else if ($(this).width() <= 960) {
        if (location.pathname.indexOf("/projects/detail/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length < 1 && $("#project-nav-menu").find(".flex-column").length < 1) {
                    $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                }
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first");
            }
            else {

            }
        }

    }
    else if ($(this).width() <= 980) {
        if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length < 1 && $("#project-nav-menu").find(".flex-column").length < 1) {
                    $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                }
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first");
            }
            else {

            }
        }

    }
    else if ($(this).width() < 1024) {

        $("body").addClass("sidebar-folded");
        if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").hide();
            $(document).find(".show-on-mobile").show();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length < 1 && $("#project-nav-menu").find(".flex-column").length < 1) {
                    $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                }
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first");
            }
            else {
                // myprojects ana sayfa
            }
        }

    }
    else if ($(this).width() === 1024) {
        $("body").addClass("sidebar-folded");
        $(document).find("header.topbar").find(".show-on-mobile").hide();
        $(document).find("header.topbar").find(".hide-dsktp-right").show();

        if (location.pathname.indexOf("/ideas/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
            $(document).find("#post-smilarprojects").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");
            $(document).find("#post-teamrequests").find(".counter-text").removeClass("fs-30").removeClass("fs-25").addClass("fs-40");

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }

        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length < 1 && $("#project-nav-menu").find(".flex-column").length < 1) {
                    $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                }
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first");
            }
            else {
                $("#myproject-nav").find(".nav").attr("style", "");
            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "");
            }
        }


    }
    else if ($(this).width() > 1024) {
        $("body").removeClass("sidebar-folded");
        $(document).find("header.topbar").find(".show-on-mobile").hide();
        $(document).find("header.topbar").find(".hide-dsktp-right").show();
        $(document).find(".show-on-mobile").hide();
        $(document).find(".hide-dsktp-right").show();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/ideas/detail/") > -1) {

        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }

        else if (location.pathname.indexOf("/myprojects/") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length < 1 && $("#project-nav-menu").find(".flex-column").length < 1) {
                    $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                }
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first");
            }
            else {
                $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                $("#myproject-nav").find(".nav").attr("style", "");
            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }
    else {
        $(document).find("header.topbar").find(".show-on-mobile").hide();
        $(document).find("header.topbar").find(".hide-dsktp-right").show();

        $(document).find(".hide-dsktp-right").show();
        $(document).find(".show-on-mobile").hide();
        if (location.pathname.indexOf("/myideas/detail/") > -1) {
            $(document).find("#ideas-fix-filter").removeClass("fab-dir-left");
        }
        else if (location.pathname.indexOf("/projects/detail/") > -1) {
            $(document).find(".hide-dsktp-right").show();
            $(document).find(".show-on-mobile").hide();
        }
        else if (location.pathname.indexOf("/myprojects") > -1) {
            if (location.pathname.indexOf("/myprojects/edit/") > -1 || location.pathname.indexOf("/myprojects/new-project/") > -1) {
                if ($("#project-nav-menu").find(".nav-lg").length < 1 && $("#project-nav-menu").find(".flex-column").length < 1) {
                    $("#project-nav-menu").find(".nav-tabs").addClass("nav-lg flex-column");
                    $("#project-nav-menu").find(".nav-tabs").attr("style", "");
                }
                $("#project-nav-menu").parent().removeClass("order-sm-first order-first");
            }
            else {
                $("#myproject-nav").find(".nav").attr("style", "");
            }
        }
        else if (location.pathname.indexOf("/myteams/") > -1) {
            if (location.pathname.indexOf("/myteams/detail/") > -1) {

            }
            else {
                $("#myteams-nav").find(".nav").attr("style", "")
            }
        }

    }

}


var resizeTimer;

function general_resize_reload() {

    $(window).resize(function () {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            general_resize_works();
            // click_events_after_resize();
        }, 250);
    });

}


function click_events_after_resize() {
    if ($(document).find("header.topbar").find(".show-on-mobile").find(".general-notifications").find(".dropdown-menu").length < 1) {
        $(document).find("header.topbar").find(".show-on-mobile").find(".general-notifications").on("click", function () {
            document.location.href = "/dashboard/notifications/general/";
        });
    }
    else if ($(document).find("header.topbar").find(".show-on-mobile").find(".notification-messages").find(".dropdown-menu").length < 1) {
        $(document).find("header.topbar").find(".show-on-mobile").find(".notification-messages").on("click", function () {
            document.location.href = "/dashboard/notifications/general/";
        });
    }
    else if ($(document).find("header.topbar").find(".show-on-mobile").find(".notification-teamrequest").find(".dropdown-menu").length < 1) {
        $(document).find("header.topbar").find(".show-on-mobile").find(".notification-teamrequest").on("click", function () {
            document.location.href = "/dashboard/notifications/general/";
        });
    }
}



