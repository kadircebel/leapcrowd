from django.urls import path
from .views import *

app_name="faqs"

urlpatterns=[
    path('',index,name='faq-index'),
    path('detail/',detail,name='faq-detail'),
]