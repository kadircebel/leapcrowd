from django.shortcuts import render
from django.utils.translation import ugettext
# Create your views here.

def index(request):
    context={
        'message':ugettext("We will update this section soon. Thanks your patience."),
        "page_title":ugettext("Frequently Asked Questions"),
    }
    return render(request,'socialside/faq/index.html',context)

def detail(request):
    context={
        'data':'faq_index',
    }
    return render(request,'socialside/faq/faq_detail.html',context)