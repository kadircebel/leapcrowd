from django.db import models

class ProjectsPage(models.Model):
    system_projectspages = models.ForeignKey('mainadmins.SystemProjectpages', models.DO_NOTHING, blank=True)
    # system_projectspages=models.OneToOneField('mainadmins.SystemProjectpages',on_delete=models.CASCADE)
    title = models.CharField(max_length=500, blank=True)
    headline_text = models.CharField(max_length=500, blank=True)
    content_text = models.TextField(blank=True)
    timeline_date = models.DateField(blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    up_date = models.DateField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'projects_page'

class ProjectsPageCountdown(models.Model):
    projects_page = models.ForeignKey('ProjectsPage', models.DO_NOTHING, blank=True)
    # projects_page=models.OneToOneField('ProjectsPage',on_delete=models.CASCADE)
    created_date = models.DateField(auto_created=True)
    completed_date = models.DateField(auto_now_add=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'projects_page_countdown'

class ProjectsPageFollowers(models.Model):
    projects_page = models.ForeignKey('ProjectsPage', models.DO_NOTHING, blank=True)
    # users = models.ForeignKey('persons.Users', models.DO_NOTHING, blank=True)
    users_fullprofiles=models.ForeignKey('persons.UsersFullProfile',models.DO_NOTHING)
    is_follow = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'projects_page_followers'

class ProjectsPageSocialaddress(models.Model):
    projects_page = models.ForeignKey('ProjectsPage', models.DO_NOTHING, blank=True)
    social_address = models.ForeignKey('mainadmins.Socialaddress', models.DO_NOTHING, blank=True)
    social_url = models.CharField(max_length=500, blank=True)
    created_date = models.DateField(auto_created=True)
    is_active = models.DateField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'projects_page_socialaddress'

class Timeline(models.Model):
    title = models.CharField(max_length=500, blank=True)
    content_text = models.TextField(blank=True)
    created_date = models.DateField(auto_now_add=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'timeline'

class TimelineUsersProjects(models.Model):
    timeline = models.ForeignKey('Timeline', models.DO_NOTHING, blank=True)
    # timeline=models.OneToOneField('Timeline',on_delete=models.CASCADE)
    users_projects_page = models.ForeignKey('persons.UsersProjectsPage', models.DO_NOTHING, blank=True)
    # users_projects_page=models.OneToOneField('persons.UsersProjectsPage',on_delete=models.CASCADE)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'timeline_users_projects'




