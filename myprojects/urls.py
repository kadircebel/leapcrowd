from django.urls import path
from .views import *

app_name="myprojects"

urlpatterns=[
    path('',index,name='myprojects-index'),


    path('new-project/',project_options,name='addnew_project'),
    path('feedbacks/',project_feedbacks,name='myprojects-feedbacks'),
    path('requests/<int:id>/',project_team_requests,name='myprojects-requests'),
    path('detail/',project_custompages,name='myprojects-custompages'),
    path('edit/<int:id>/', project_edit, name='myproject-edit'),

    path('general-infos/', project_general_infos, name='project_general_infos'),
    path('business-model/', project_business_model, name='project_business_model'),
    path('requirements/', project_requirements_model, name='project_requirements_model'),
    path('competitors/', project_competitors_model, name='project_competitors_model'),
    path('get-project-points/', get_projectpoints, name='project_points'),
    path('project-business-stage/', project_business_stage_check, name='project_business_stage'),
    path('change-project-options/', project_options_change, name='project_options_change'),
    path('get-project-options/', get_project_options, name='project_options_get'),
    path('active-step/', active_step, name='active_step'),

    path('upload-project-logo/', upload_logo, name='project_logo'),
    path('delete-project-logo/', delete_logo, name='delete_logo'),
    path('selected-project-areas/', selected_project_areas, name='selected_project_areas'),
    path('selected-project-types/', selected_project_types, name='selected_project_types'),
    path('loadmore-teamrequests/<int:id>/', loadmore_teamrequests, name='loadmore_teamrequests'),
    path('undecided-requests/<int:id>/', undecided_teamrequests, name='undecided_teamrequests'),
    path('loadmore-undecided/<int:id>/', loadmore_undecided_requests, name='loadmore_requests'),
    path('feedbacks/<int:id>/', requests_feedbacks, name='feedbacks'),
    path('teamrequests/', project_teamrequests_and_feedbacks, name='teamrequests'),
    path('filter-teamrequests/<int:id>/', filter_teamrequests, name='filter_teamrequests'),
    path('response-teamrequests/', send_response_to_teamrequests, name='response_teamrequests'),
    path('response-profiles/', response_profiles, name='response_profiles'),
    path('funded-projects-count/', funded_projects_count, name='funded_projects_count'),

    path('myprojects-list/', myprojects_list, name='myprojects_list'),
    path('cofunded-projects-list/', cofunded_projects_list, name='cofunded_projects_list'),
    path('selected-person/', selected_person_for_team, name='selected_person'),
    path('selected-person-location/', selected_person_location_for_team, name='selected_person_location'),

    path('<slug:section>/',index,name='myprojects-index'),


]