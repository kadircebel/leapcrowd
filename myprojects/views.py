from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_protect
import json,requests
from django.utils.translation import ugettext
from myprofile.models import FullprofileLocation
from persons.models import UsersFullProfile, UsersProjects, UsersProjectsTeamrequests, UsersProjectsFeedbacks, UsersTeams, UsersMembershipTermsType,UsersIndustries
from projects.models import Projects, ProjectUsageTypes, ProjectPointsScores, ProjectPrjSectors, GeneralInfos, BusinessPlans, Competitors, Requirements, ProjectsSteps, ProjectsProjectOptions
from mainadmins.models import UsageTypes, PointScores, ProjectSector, ImageUploadSize, Steps, ProjectOptions
from utils.views import control_file_extention, check_file_size, content_projectlogo_filename, get_active_logo,delete_project_logo, get_profile_name_str, clean_hours_title, get_fulllocation_string, responded_teamrequest_mail, change_characters, select_teammembers_array,match_friends_in_system,is_it_same_location,new_signals_users_list,send_signals_to_projects_owner
from notifications.models import NotificationsUsersTeams
from teams.models import Teams
from memberships.models import MembershipTermsType
# from datetime import datetime
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.utils.timesince import timesince
from django.views.decorators.cache import never_cache, cache_page

# Create your views here.
@never_cache
def index(request, section=""):
    if 'user_fullprofile' in request.session:
        context_projects = []
        selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if "special-projects" in section:
            context = {
                "projects": context_projects,
                "nav_buttons": True,
                "section": section,
                "page_title":ugettext("Special projects"),
            }
            return render(request, 'socialside/myprojects/index.html', context)
        elif "teamrequest" in section:
            context = {
                "projects": context_projects,
                "nav_buttons": True,
                "section": section,
                "page_title": ugettext("Team requests"),
            }
            return render(request, 'socialside/myprojects/index.html', context)
        elif "cofunded" in section:
            try:
                cofunded_list = NotificationsUsersTeams.list_all(selected_user_fullprofile)
                for cofunded_project in cofunded_list[:5]:
                    # bu kısımda project listeleme yapacağız

                    if cofunded_project.is_read is False:
                        cofunded_project.read_and_update_notification(cofunded_project.id)

                    try:
                        if request.LANGUAGE_CODE is "tr":
                            steps = ProjectsSteps.select_steps(
                                cofunded_project.users_teams.teams.users_projects.projects).order_by("-steps__id")[
                                0].steps.step_name
                        else:
                            steps = ProjectsSteps.select_steps(
                                cofunded_project.users_teams.teams.users_projects.projects).order_by("-steps__id")[
                                0].steps.step_name_eng
                    except:
                        if request.LANGUAGE_CODE is "tr":
                            steps = "Henüz bir proje adımı bulunmuyor."
                        else:
                            steps = "No project steps are found yet."

                    item = {
                        "id": cofunded_project.users_teams.teams.users_projects.projects.id,
                        "project_logo": cofunded_project.users_teams.teams.users_projects.projects.general_infos.project_logo,
                        "title": cofunded_project.users_teams.teams.users_projects.projects.general_infos.title,
                        "steps": steps,
                        "project_locations": cofunded_project.users_teams.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + cofunded_project.users_teams.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                        "project_options": ProjectsProjectOptions.select_options(
                            cofunded_project.users_teams.teams.users_projects.projects),
                        "isOnline": ProjectsProjectOptions.isItOnline(
                            cofunded_project.users_teams.teams.users_projects.projects,
                            ProjectOptions.select("publish-project")),

                    }
                    context_projects.append(item)

                if len(cofunded_list) > 5:
                    has_next = True
                else:
                    has_next = False

                context = {
                    "projects": context_projects,
                    "nav_buttons": True,
                    "section": section,
                    "message_type": "success",
                    "has_next": has_next,
                    "page_title": ugettext("Co-funded projects"),
                }
            except:
                context = {
                    "projects": context_projects,
                    "nav_buttons": True,
                    "section": section,
                    "message_type": "error",
                    "message": ugettext("There is not co-funded project yet."),
                    "page_title": ugettext("Co-funded projects"),
                }

            return render(request, 'socialside/myprojects/index.html', context)
        else:
            # ana sayfa - ilk tab

            projects = UsersProjects.list_all(selected_user_fullprofile)
            locations = FullprofileLocation.select(selected_user_fullprofile.full_profile)

            for project in projects[:5]:
                if request.LANGUAGE_CODE == "tr":
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[0].steps.step_name
                    except:
                        steps = "Henüz bir proje adımı bulunmuyor."
                    item = {
                        "id": project.projects.id,
                        "project_logo": project.projects.general_infos.project_logo,
                        "title": project.projects.general_infos.title,
                        "steps": steps,
                        "project_locations": locations.location.city + ", " + locations.location.country,
                        "project_options": ProjectsProjectOptions.select_options(project.projects),
                        "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                      ProjectOptions.select("publish-project")),
                    }
                else:
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[
                            0].steps.step_name_eng
                    except:
                        steps = "No project steps are found yet."

                    item = {
                        "id": project.projects.id,
                        "project_logo": project.projects.general_infos.project_logo,
                        "title": project.projects.general_infos.title,
                        "steps": steps,
                        "project_locations": locations.location.city + ", " + locations.location.country,
                        "project_options": ProjectsProjectOptions.select_options(project.projects),
                        "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                      ProjectOptions.select("publish-project")),
                    }

                context_projects.append(item)

            if projects.count() > 5:
                has_next = True
            else:
                has_next = False

            context = {
                "projects": context_projects,
                "nav_buttons": True,
                "has_next": has_next,
                "section": section,
                "page_title":ugettext("My projects"),
            }
            return render(request, 'socialside/myprojects/index.html', context)
    else:
        return HttpResponseRedirect("/login/")

@never_cache
@csrf_protect
def myprojects_list(request):
    if request.method == "POST":
        context_projects = []
        page = request.POST.get("page")
        selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        projects = UsersProjects.list_all(selected_user_fullprofile)
        locations = FullprofileLocation.select(selected_user_fullprofile.full_profile)

        for project in projects:

            if request.LANGUAGE_CODE == "tr":
                try:
                    steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[0].steps.step_name
                except:
                    steps = "Henüz bir proje adımı bulunmuyor."
                item = {
                    "id": project.projects.id,
                    "project_logo": project.projects.general_infos.project_logo.name,
                    "project_name": project.projects.general_infos.title,
                    "steps": steps,
                    "project_locations": locations.location.city + ", " + locations.location.country,
                    "is_teamrequest": ProjectsProjectOptions.isItOnline(project.projects,
                                                                        ProjectOptions.select("send-teamrequest")),
                    "is_feedback": ProjectsProjectOptions.isItOnline(project.projects,
                                                                     ProjectOptions.select("give-feedback")),
                    "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                  ProjectOptions.select("publish-project")),
                }
            else:
                try:
                    steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[0].steps.step_name_eng
                except:
                    steps = "No project steps are found yet."

                item = {
                    "id": project.projects.id,
                    "project_logo": project.projects.general_infos.project_logo.name,
                    "project_name": project.projects.general_infos.title,
                    "steps": steps,
                    "project_locations": locations.location.city + ", " + locations.location.country,
                    "is_teamrequest": ProjectsProjectOptions.isItOnline(project.projects,
                                                                        ProjectOptions.select("send-teamrequest")),
                    "is_feedback": ProjectsProjectOptions.isItOnline(project.projects,
                                                                     ProjectOptions.select("give-feedback")),
                    "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                  ProjectOptions.select("publish-project")),
                }

            context_projects.append(item)

        paginator = Paginator(context_projects, 5)

        try:
            projects_list = paginator.page(page)
        except PageNotAnInteger:
            projects_list = paginator.page(1)
        except EmptyPage:
            projects_list = paginator.page(paginator.num_pages)

        page = str(int(page) + 1)

        output_data = {
            "project_list": list(projects_list),
            "has_next": projects_list.has_next(),
            "page": page,
        }

        return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
@csrf_protect
def cofunded_projects_list(request):
    if request.method == "POST":
        page = request.POST.get("page")
        context_projects = []

        selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

        cofunded_list = NotificationsUsersTeams.list_all(selected_user_fullprofile)
        try:
            for cofunded_project in cofunded_list:
                # bu kısımda project listeleme yapacağız
                if cofunded_project.is_read is False:
                    cofunded_project.read_and_update_notification(cofunded_project.id)

                try:
                    if request.LANGUAGE_CODE is "tr":
                        steps = ProjectsSteps.select_steps(
                            cofunded_project.users_teams.teams.users_projects.projects).order_by("-steps__id")[
                            0].steps.step_name
                    else:
                        steps = ProjectsSteps.select_steps(
                            cofunded_project.users_teams.teams.users_projects.projects).order_by("-steps__id")[
                            0].steps.step_name_eng
                except:
                    if request.LANGUAGE_CODE is "tr":
                        steps = "Henüz bir proje adımı bulunmuyor."
                    else:
                        steps = "No project steps are found yet."

                item = {
                    "id": cofunded_project.users_teams.teams.users_projects.projects.id,
                    "project_logo": cofunded_project.users_teams.teams.users_projects.projects.general_infos.project_logo.name,
                    "project_name": cofunded_project.users_teams.teams.users_projects.projects.general_infos.title,

                    "steps": steps,

                    "project_locations": cofunded_project.users_teams.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + cofunded_project.users_teams.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                    "is_teamrequest": ProjectsProjectOptions.isItOnline(
                        cofunded_project.users_teams.teams.users_projects.projects,
                        ProjectOptions.select("send-teamrequest")),
                    "is_feedback": ProjectsProjectOptions.isItOnline(
                        cofunded_project.users_teams.teams.users_projects.projects,
                        ProjectOptions.select("give-feedback")),
                    "isOnline": ProjectsProjectOptions.isItOnline(
                        cofunded_project.users_teams.teams.users_projects.projects,
                        ProjectOptions.select("publish-project")),

                }
                context_projects.append(item)
        except:
            pass

        paginator = Paginator(context_projects, 5)

        try:
            projects_list = paginator.page(page)
        except PageNotAnInteger:
            projects_list = paginator.page(1)
        except EmptyPage:
            projects_list = paginator.page(paginator.num_pages)

        page = str(int(page) + 1)

        output_data = {
            "project_list": list(projects_list),
            "has_next": projects_list.has_next(),
            "page": page,
        }

        return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
def project_options(request):

    if 'user_fullprofile' in request.session:
        user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

        if UsersMembershipTermsType.isOver_limitsize(user_fullprofile, "add-project",user_fullprofile.users.users_membershiptypes.name_code) == True:
            # mevcut üyelikte limit aşıldıysa

            if "beginner" == user_fullprofile.users.users_membershiptypes.name_code:
                context = {
                    "message_type": "error",
                    "message": ugettext(
                        "You can only open 1 project in your membership package. If you'd like to open more than 1 project, you should benefit our paid membership packages."),
                    "nav_buttons": False,
                    'ph_title': ugettext("New Project"),
                    "po_location": get_fulllocation_string(
                        user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                        user_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + user_fullprofile.full_profile.fullprofilelocation_set.get().location.country,
                    "project_situation": ugettext("Draft"),
                    "page_title":ugettext("Building new project"),
                    "project_logo": "/media/projects/default_logo.png",
                }
            else:
                context = {
                    "message_type": "error",
                    "message": ugettext(
                        "You appear to have completed of the limit for creating new project. You can benefit our paid membership packages."),
                    "nav_buttons": False,
                    'ph_title': ugettext("New Project"),
                    "po_location": get_fulllocation_string(
                        user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                        user_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + user_fullprofile.full_profile.fullprofilelocation_set.get().location.country,
                    "project_situation": ugettext("Draft"),
                    "page_title": ugettext("Building new project"),
                    "project_logo": "/media/projects/default_logo.png",
                }
        else:
            context = {
                    "message_type": "success",
                    "message": ugettext(
                        "You can only open 1 project in your membership package. If you'd like to open more than 1 project, you should benefit our paid membership packages."),
                    "nav_buttons": False,
                    'ph_title': ugettext("New Project"),
                    "po_location": get_fulllocation_string(
                        user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                        user_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + user_fullprofile.full_profile.fullprofilelocation_set.get().location.country,
                    "project_situation": ugettext("Draft"),
                    "page_title": ugettext("Building new project"),
                    "project_logo":"/media/projects/default_logo.png",
                }

        return render(request, 'socialside/myprojects/project_options.html', context)

    else:
        return HttpResponseRedirect("/login/")

@never_cache
def project_edit(request, id):
    if 'user_fullprofile' in request.session:
        selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if Projects.isThereProject(id):
            if UsersProjects.isProjectForThisUser(selected_user_fullprofile, Projects.select(id)):
                selected_project = Projects.select(id)
                try:
                    context = {
                        "message_type": "success",
                        "messages": ugettext("Edit your project informations"),
                        "project_id": id,
                        "title": selected_project.general_infos.title,
                        "summary": selected_project.general_infos.describe,
                        "website": selected_project.general_infos.project_website,
                        "project_logo": selected_project.general_infos.project_logo,
                        "target_group": selected_project.business_plans.target_group_content,
                        "revenue_model": selected_project.business_plans.revenue_model,
                        "existing_requirements": selected_project.requirements.existing_requirements,
                        "needed_requirements": selected_project.requirements.needed_requirements,
                        "competitor_content": selected_project.competitors.competitor_content,
                        "project_situation": ugettext("Edit"),
                        'ph_title': ugettext("Edit"),
                        "nav_buttons": False,
                        "po_location": get_fulllocation_string(
                            selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.country,
                        "page_title":selected_project.general_infos.title + " update",
                    }
                except:
                    context = {
                        "message_type": "success",
                        "messages": ugettext("Edit your project informations"),
                        "project_id": id,
                        "title": selected_project.general_infos.title,
                        "summary": selected_project.general_infos.describe,
                        "website": selected_project.general_infos.project_website,
                        "project_logo": selected_project.general_infos.project_logo,
                        "project_situation": ugettext("Edit"),
                        'ph_title': ugettext("Edit"),
                        "nav_buttons": False,
                        "po_location": get_fulllocation_string(
                            selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.country,
                        "page_title": selected_project.general_infos.title + " update",
                    }

                return render(request, 'socialside/myprojects/project_options.html', context)
            else:
                return HttpResponseRedirect("/dashboard/myprojects/")
        else:
            return HttpResponseRedirect("/dashboard/myprojects/")
    else:
        return HttpResponseRedirect("/login/")

@never_cache
def selected_project_areas(request):
    if request.method == "GET":
        project_id = request.GET.get("id")
        try:
            if Projects.isThereProject(project_id):
                selected_project = Projects.select(project_id)
                context = {
                    "id": ProjectPrjSectors.select(selected_project).project_sectors.id,
                }
            else:
                context = {
                    "id": 0,
                }
        except:
            context = {
                "id": 0,
            }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def selected_project_types(request):
    context = []
    if request.method == "GET":
        project_id = request.GET.get("id")
        selected_project = Projects.select(project_id)
        for usageType in ProjectUsageTypes.select_list_by_project(selected_project):
            item = {"id": usageType.usage_types.id}
            context.append(item)

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def project_feedbacks(request):
    context = {
        'data': 'my_projects page',
    }
    return render(request, 'socialside/myprojects/feedbacks_detail.html', context)

@never_cache
def filter_teamrequests(request, id):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            filter_option = request.POST.get("filter_choice")
            page = request.POST.get("page")
            context_request = []
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            selected_requests = UsersProjectsTeamrequests.select_requests(
                UsersProjects.select_by_id(id, users_fullprofile))

            if selected_requests.count() > 0:
                for requests in selected_requests:
                    if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = requests.users_fullprofiles.users.member_type
                    else:
                        if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = requests.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                            # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                    if request.LANGUAGE_CODE == "tr":
                        support_type = requests.teamrequest.users_request_investments.investments.title
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title,
                            "saat")
                    else:
                        support_type = requests.teamrequest.users_request_investments.investments.title_eng
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title_eng, "hour")

                    if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                        try:
                            hour_cost = requests.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + "" + requests.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                        except:
                            hour_cost = ""
                    else:
                        hour_cost = ""

                    if filter_option in requests.teamrequest.users_request_investments.investments.title_code:
                        item_request = {
                            "profile_img": profile_img,
                            "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                                                                 requests.users_fullprofiles.full_profile.first_name),
                            "position_title": position_title,
                            "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                            "support_type": support_type,
                            "hour_cost": hour_cost,
                            "hour_info": hour_info,
                            "id": requests.users_fullprofiles.id,
                        }

                        context_request.append(item_request)
                    elif "show-all" in filter_option:
                        item_request = {
                            "profile_img": profile_img,
                            "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                                                                 requests.users_fullprofiles.full_profile.first_name),
                            "position_title": position_title,
                            "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                            "support_type": support_type,
                            "hour_cost": hour_cost,
                            "hour_info": hour_info,
                            "id": requests.users_fullprofiles.id,
                        }

                        context_request.append(item_request)

            paginator = Paginator(context_request, 6)

            try:
                requests_list = paginator.page(page)
            except PageNotAnInteger:
                requests_list = paginator.page(1)
            except EmptyPage:
                requests_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "requests": list(requests_list),
                "has_next": requests_list.has_next(),
                "page": page,
                "message_type": "success",
            }

        else:
            context = {
                "message_type": "warning",
            }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def project_team_requests(request, id):
    if 'user_fullprofile' in request.session:
        context_request = []
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        owner_city = change_characters(users_fullprofile.full_profile.fullprofilelocation_set.get().location.city)
        # membership takım ekleme özelliklerimi almalıyım
        team_person_limit = MembershipTermsType.select_terms_type(
            users_fullprofile.users.users_membershiptypes.name_code, "person-to-team").limit_count
        extra_person_limit = MembershipTermsType.select_terms_type(
            users_fullprofile.users.users_membershiptypes.name_code, "person-to-team-location").limit_count
        # membership takım ekleme özelliklerimi almalıyım

        create_team_message = ""
        # daha önceden takım oluşturdum mu?
        if Teams.is_there_team_by_project(UsersProjects.select_by_id(id, users_fullprofile)):

            selected_team = Teams.select_team(UsersProjects.select_by_id(id, users_fullprofile))

            if UsersTeams.my_location_teammembers_count(selected_team.users_projects,users_fullprofile.id) < extra_person_limit:
                same_mate_counter = extra_person_limit - UsersTeams.my_location_teammembers_count(selected_team.users_projects,users_fullprofile.id)
            elif UsersTeams.my_location_teammembers_count(selected_team.users_projects,users_fullprofile.id) == extra_person_limit:
                same_mate_counter = UsersTeams.my_location_teammembers_count(selected_team.users_projects,users_fullprofile.id)
            else:
                same_mate_counter = 0

            if same_mate_counter == extra_person_limit and (selected_team.users_projects.teams_set.get().usersteams_set.count() - 1) < team_person_limit:
                different_mate_counter = selected_team.users_projects.teams_set.get().usersteams_set.count() - 1

            elif (selected_team.users_projects.teams_set.get().usersteams_set.count() - 1) < team_person_limit:
                different_mate_counter = team_person_limit - same_mate_counter
            else:
                different_mate_counter = 0

            if same_mate_counter == extra_person_limit and different_mate_counter < team_person_limit:
                if request.LANGUAGE_CODE == "tr":
                    create_team_message = selected_team.users_projects.projects.general_infos.title + " projesine daha önceden takım oluşturmuşsunuz ve istediğiniz kişileri seçmişsiniz. Bulunduğunuz üyelik paketinde takımınıza " + str(different_mate_counter) + " kişi/kişiler seçebilirsiniz. Eğer daha fazla kişiyi takımınıza dahil etmek isterseniz ücretli üyelik paketlerimizden faydalanabilirsiniz."
                else:
                    create_team_message = "You have already teamed up for the project named " + selected_team.users_projects.projects.general_infos.title + ". You can add " + str(different_mate_counter) + " person/people to your team. If you would like to select more people, you should benefit our paid membership packages."

            elif same_mate_counter > 0 or different_mate_counter > 0:
                if request.LANGUAGE_CODE == "tr":
                    create_team_message = selected_team.users_projects.projects.general_infos.title + " projesine daha önceden takım oluşturmuşsunuz ve istediğiniz kişi/kişileri seçmişsiniz. Kendi konumunuzdan " + str(same_mate_counter) + " kişi, kendi konumunuz dışından " + str(different_mate_counter) + " kişi/kişiler seçebilirsiniz."
                else:
                    create_team_message = "You have already teamed up for the project named " + selected_team.users_projects.projects.general_infos.title + ". You can add " + str(same_mate_counter) + " person/people from your own location and you can add " + str(different_mate_counter) + " person/people from outside your location."


        else:
            create_team_message = ""

        ph_title = UsersProjects.select_by_id(id, users_fullprofile).projects.general_infos.title

        if ProjectsProjectOptions.po_situation(UsersProjects.select_by_id(id, users_fullprofile).projects,
                                               ProjectOptions.select("publish-project")):
            project_situation = ugettext("Online")
        else:
            project_situation = ""

        po_location = get_fulllocation_string(
            users_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
            users_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + users_fullprofile.full_profile.fullprofilelocation_set.get().location.country

        selected_requests = UsersProjectsTeamrequests.select_requests(UsersProjects.select_by_id(id, users_fullprofile))

        if selected_requests.count() > 0:

            for requests in selected_requests:
                if requests.is_read is False:
                    requests.readthis_request(requests.teamrequest)

                if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                    profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title="Girişimci"
                    else:
                        position_title = requests.users_fullprofiles.users.member_type
                else:
                    if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = requests.users_fullprofiles.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                        # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                if request.LANGUAGE_CODE == "tr":
                    support_type = requests.teamrequest.users_request_investments.investments.title
                    hour_info = clean_hours_title(requests.teamrequest.users_request_projecthours.project_hours.title,
                                                  "saat")
                else:
                    support_type = requests.teamrequest.users_request_investments.investments.title_eng
                    hour_info = clean_hours_title(
                        requests.teamrequest.users_request_projecthours.project_hours.title_eng, "hour")

                if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                    try:
                        hour_cost = requests.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + "" + requests.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                    except:
                        hour_cost = ""
                else:
                    hour_cost = ""

                item_request = {
                    "profile_img": profile_img,
                    "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                                                         requests.users_fullprofiles.full_profile.first_name),
                    "position_title": position_title,
                    # "skills": requests.users_fullprofiles.usersskills_set.all()[:5],
                    "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                    "support_type": support_type,
                    "hour_cost": hour_cost,
                    "hour_info": hour_info,

                    "id": requests.users_fullprofiles.id,
                }

                context_request.append(item_request)

            if len(context_request) > 6:
                has_next = True
                message = ""
            else:
                has_next = False
                message = ugettext("You are viewing all team requests.")

            context = {
                "message_type": "success",
                "requests": context_request[:6],
                "has_next": has_next,
                "message": message,
                "id": id,
                "ph_title": ph_title,
                "project_situation": project_situation,
                "po_location": po_location,
                "nav_buttons": True,
                "team_message": create_team_message,
                "page_title":ugettext("Incoming team requests"),
            }
        else:
            context = {
                "message_type": "warning",
                "message": ugettext("You have not received a team request yet."),
                "requests": context_request,
                "ph_title": ph_title,
                "project_situation": project_situation,
                "po_location": po_location,
                "id": id,
                "nav_buttons": True,
                "team_message": create_team_message,
                "page_title": ugettext("Incoming team requests"),
            }

        return render(request, 'socialside/myprojects/team_requests.html', context)

    else:
        return HttpResponseRedirect("/login/")

@never_cache
@csrf_protect
def loadmore_teamrequests(request, id):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            page = request.POST.get("page")
            context_request = []
            selected_requests = UsersProjectsTeamrequests.select_requests(
                UsersProjects.select_by_id(id, users_fullprofile))

            for requests in selected_requests:

                if requests.is_read is False:
                    requests.readthis_request(requests.teamrequest)

                if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                    profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title = "Girişimci"
                    else:
                        position_title = requests.users_fullprofiles.users.member_type
                else:
                    if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = requests.users_fullprofiles.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                        # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                if request.LANGUAGE_CODE == "tr":
                    support_type = requests.teamrequest.users_request_investments.investments.title
                    hour_info = requests.teamrequest.users_request_projecthours.project_hours.title
                else:
                    support_type = requests.teamrequest.users_request_investments.investments.title_eng
                    hour_info = requests.teamrequest.users_request_projecthours.project_hours.title_eng

                if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                    try:
                        hour_cost = requests.users_fullprofiles.users.usershourcosts_set.get().hour_costs.cost_code + " " + requests.users_fullprofiles.users.usershourcosts_set.get().hour_cost_amount
                    except:
                        hour_cost = ""
                else:
                    hour_cost = ""

                item_request = {
                    "profile_img": profile_img,
                    "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                                                         requests.users_fullprofiles.full_profile.first_name),
                    "position_title": position_title,
                    "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                    "support_type": support_type,
                    "hour_cost": hour_cost,
                    "hour_info": hour_info,
                    "id": requests.users_fullprofiles.id,
                }

                context_request.append(item_request)

            paginator = Paginator(context_request, 6)

            try:
                requests_list = paginator.page(page)
            except PageNotAnInteger:
                requests_list = paginator.page(2)
            except EmptyPage:
                requests_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "requests": list(requests_list),
                "has_next": requests_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@cache_page(60 * 10)
@csrf_protect
def undecided_teamrequests(request, id):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            context_request = []
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            selected_requests = UsersProjectsTeamrequests.undecided_requests(UsersProjects.select_by_id(id, users_fullprofile))

            if selected_requests.count() > 0:

                for requests in selected_requests:

                    if requests.is_read is False:
                        requests.readthis_request(requests.teamrequest)

                    if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                    if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = requests.users_fullprofiles.users.member_type
                    else:
                        if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = requests.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                            # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                    if request.LANGUAGE_CODE == "tr":
                        support_type = requests.teamrequest.users_request_investments.investments.title
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title,
                            "saat")
                    else:
                        support_type = requests.teamrequest.users_request_investments.investments.title_eng
                        hour_info = clean_hours_title(
                            requests.teamrequest.users_request_projecthours.project_hours.title_eng, "hour")

                    if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                        try:
                            hour_cost = requests.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + "" + requests.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                        except:
                            hour_cost = ""
                    else:
                        hour_cost = ""

                    item_request = {
                        "profile_img": profile_img,
                        "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                                                             requests.users_fullprofiles.full_profile.first_name),
                        "position_title": position_title,
                        "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                        "support_type": support_type,
                        "hour_cost": hour_cost,
                        "hour_info": hour_info,
                        "id": requests.users_fullprofiles.id,
                    }

                    context_request.append(item_request)

                if len(context_request) > 6:
                    has_next = True
                    message = ugettext(
                        "In this section, you will see the people who gave up the offer. You can reach them by sending a message and try to change their minds.")
                else:
                    has_next = False
                    message = ugettext(
                        "In this section, you will see the people who gave up the offer. You can reach them by sending a message and try to change their minds.")



                if "beginner" in users_fullprofile.users.users_membershiptypes.name_code:
                    if request.LANGUAGE_CODE == "tr":
                        message = str(len(context_request)) + " adet kararsız takım isteğiniz mevcut. Takım isteklerini görebilmeniz için ücretli üyelik paketlerinden birine sahip olmalısınız."
                    else:
                        message="You have " + str(len(context_request)) + " undecided requests. You should benefit our paid membership if you want to see these requests."

                    context = {
                        "membership_type": "beginner",
                        # "requests": len(context_request),
                        "message_type": "success",
                        "message": message,
                        "id": id,
                        "has_next": False,
                    }
                else:
                    context = {
                        "membership_type": users_fullprofile.users.users_membershiptypes.name_code,
                        "requests": context_request[:6],
                        "message_type": "success",
                        "message": message,
                        "id": id,
                        "has_next": has_next,
                    }
            else:
                context = {
                    "requests": context_request,
                    "message_type": "success",
                    "message": ugettext("You have not an undecided team request yet."),
                    "id": id,
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(60 * 5)
@csrf_protect
def loadmore_undecided_requests(request, id):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            context_request = []
            page = request.POST.get("page")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            selected_requests = UsersProjectsTeamrequests.undecided_requests(
                UsersProjects.select_by_id(id, users_fullprofile))

            for requests in selected_requests:

                if requests.is_read is False:
                    requests.readthis_request(requests.teamrequest)

                if not "default" in requests.users_fullprofiles.full_profile.picture_url.name:
                    profile_img = requests.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_img = requests.users_fullprofiles.full_profile.linkedin_picture_url

                if "entrepreneur" in requests.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title = "Girişimci"
                    else:
                        position_title = requests.users_fullprofiles.users.member_type
                else:
                    if len(requests.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = requests.users_fullprofiles.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(requests.users_fullprofiles).industries.description
                        # position_title = requests.users_fullprofiles.usersındustries_set.get().industries.description

                if request.LANGUAGE_CODE == "tr":
                    support_type = requests.teamrequest.users_request_investments.investments.title
                    hour_info = clean_hours_title(
                        requests.teamrequest.users_request_projecthours.project_hours.title,
                        "saat")
                else:
                    support_type = requests.teamrequest.users_request_investments.investments.title_eng
                    hour_info = clean_hours_title(
                        requests.teamrequest.users_request_projecthours.project_hours.title_eng, "hour")

                if "paid-support" in requests.teamrequest.users_request_investments.investments.title_code:
                    try:
                        hour_cost = requests.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + "" + requests.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                    except:
                        hour_cost = ""
                else:
                    hour_cost = ""

                item_request = {
                    "profile_img": profile_img,
                    "profile_name": get_profile_name_str(requests.users_fullprofiles.full_profile.last_name,
                                                         requests.users_fullprofiles.full_profile.first_name),
                    "position_title": position_title,
                    "location": requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + requests.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                    "support_type": support_type,
                    "hour_cost": hour_cost,
                    "hour_info": hour_info,
                    "id": requests.users_fullprofiles.id,
                }

                context_request.append(item_request)

            paginator = Paginator(context_request, 6)

            try:
                requests_list = paginator.page(page)
            except PageNotAnInteger:
                requests_list = paginator.page(2)
            except EmptyPage:
                requests_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "requests": list(requests_list),
                "has_next": requests_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
@csrf_protect
def requests_feedbacks(request, id):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            page = request.POST.get("page")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            project_feedback = UsersProjectsFeedbacks.list_feedbacks(UsersProjects.select_by_id(id, users_fullprofile))
            context_feedback = []

            for feedback in project_feedback:
                feedback.notificationsprojectsfeedbacks_set.get().read_this(feedback.notificationsprojectsfeedbacks_set.get().id)
                feedback.read_feedbacks(feedback.id)


                if not "default" in feedback.users_fullprofiles.full_profile.picture_url.name:
                    profile_img = feedback.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_img = feedback.users_fullprofiles.full_profile.linkedin_picture_url

                item = {
                    "profile_img": profile_img,
                    "profile_name": str(get_profile_name_str(feedback.users_fullprofiles.full_profile.last_name,
                                                             feedback.users_fullprofiles.full_profile.first_name)).lower(),
                    "profile_id": feedback.users_fullprofiles.id,
                    "created_date": timesince(feedback.created_date),
                    "content_text": feedback.describe
                }

                context_feedback.append(item)

                pass

            paginator = Paginator(context_feedback, 6)

            try:
                feedback_list = paginator.page(page)
            except PageNotAnInteger:
                feedback_list = paginator.page(1)
            except EmptyPage:
                feedback_list = paginator.page(paginator.num_pages)

            if len(page) > 0:
                page = str(int(page) + 1)

            output_data = {
                "feedbacks": list(feedback_list),
                "has_next": feedback_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
def project_custompages(request):
    context = {
        'data': 'my_projects page',
    }
    return render(request, 'socialside/myprojects/project_options.html', context)

@never_cache
@csrf_protect
def get_projectpoints(request):
    # bu kısımda post olarak project id alınacak
    # alınan id ye göre proje seçilecek
    if request.method == "GET":
        project_id = request.GET.get("id")
        try:
            selected_project = Projects.select(project_id)
            scores = ProjectPointsScores.calculate_points(selected_project)
        except:
            scores = 0
        context = {
            'score': scores,
        }
        return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def delete_logo(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            project_id = request.POST.get("project_id")

            selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            if Projects.select(project_id):
                if UsersProjects.isProjectForThisUser(selected_user_fullprofile, Projects.select(project_id)):
                    selected_project = Projects.select(project_id)

                    if delete_project_logo(selected_project.general_infos.project_logo.name):
                        GeneralInfos.delete_project_logo(selected_project.general_infos.id,"/media/projects/default_logo.png")
                        context = {
                            "message_type": "success",
                            "message": ugettext("Project logo successfully removed."),
                            "default_logo": "/media/projects/default_logo.png",
                        }
                    else:
                        context = {
                            "message_type": "error",
                            "message": ugettext("An error was encountered while deleting the project logo."),
                        }
                else:
                    context = {
                        "message_type": "error",
                        "message": ugettext("An error was encountered while deleting the project log."),
                    }
            else:
                context = {
                    "message_type": "error",
                    "message": ugettext("This project is not your project."),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def upload_logo(request):
    if 'user_fullprofile' in request.session:
        pic = request.FILES['myfile']
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        project_id = request.POST.get("project_id")
        # eğer general kaydı yoksa yeni kayıt açılacak ve general id geri dönecek
        # eğer general kaydı var ise

        if control_file_extention(pic):
            if check_file_size(pic) > int(ImageUploadSize.select_mb("2,5")):
                context = {
                    'message_type': 'warning',
                    'message': ugettext("Your logo have to small than 2.5 mb. Thanks."),
                    'redirect_url': '/myprojects/new-project/',
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

                # böyle bir proje kaydı var mı
                if Projects.isThereProject(project_id):
                    if UsersProjects.isProjectForThisUser(selected_user_fullprofile, Projects.select(project_id)):
                        selected_general_infos = GeneralInfos.objects.filter(
                            id=Projects.select(project_id).general_infos.id).get()

                        # selected_general_infos.objects.update_or_create(
                        #     project_logo=content_projectlogo_filename(selected_user_fullprofile.full_profile.id, pic))

                        selected_general_infos.create_selected_project_logo(selected_general_infos.id,content_projectlogo_filename(selected_user_fullprofile.full_profile.id, pic))

                        ProjectPointsScores.create(Projects.select(project_id), PointScores.select("project-logo"))

                        # proje var ise editle
                        context = {
                            'message_type': 'success',
                            'message': ugettext("Your project logo has been changed. Thanks."),
                            'redirect_url': "/myprojects/new-project/",
                            'project_logo': str(get_active_logo(int(selected_general_infos.id))),
                            'project_id': project_id,
                        }
                    else:
                        # yok ise yeni oluştur
                        # general_infos oluşturulacak
                        general_infos = GeneralInfos.objects.update_or_create(
                            project_logo=content_projectlogo_filename(selected_user_fullprofile.full_profile.id, pic))
                        # projects oluşturulacak
                        projects = Projects.create_generals(general_infos, project_id)
                        # users_projects oluşturulacak
                        created_user_projects = UsersProjects.create(selected_user_fullprofile, projects,0)
                        # projects_pointscores oluşturulacak
                        ProjectPointsScores.create(projects, PointScores.select("project-logo"))

                        context = {
                            'message_type': 'success',
                            'message': ugettext("Your project logo has been changed. Thanks."),
                            'redirect_url': "/myprojects/new-project/",
                            'project_id': projects.id,
                            'project_logo': str(get_active_logo(int(created_user_projects.projects.general_infos.id))),
                        }
                    return HttpResponse(json.dumps(context), content_type='application/json')
                else:
                    # bu kısımda yine membership olayına bakılması lazım

                    general_infos = GeneralInfos.create_only_logo(
                        content_projectlogo_filename(selected_user_fullprofile.full_profile.id, pic))
                    # projects oluşturulacak

                    projects = Projects.create_generals(general_infos, project_id)
                    # users_projects oluşturulacak
                    created_user_projects = UsersProjects.create(selected_user_fullprofile, projects,0)
                    # projects_pointscores oluşturulacak
                    ProjectPointsScores.create(projects, PointScores.select("project-logo"))

                    UsersMembershipTermsType.create(users_fullprofiles, "add-project",users_fullprofiles.users.users_membershiptypes.name_code)

                    context = {
                        'message_type': 'success',
                        'message': ugettext("Your project logo has been changed. Thanks."),
                        'redirect_url': "/myprojects/new-project/",
                        'project_id': projects.id,
                        'project_logo': str(get_active_logo(int(created_user_projects.projects.general_infos.id))),
                    }
                return HttpResponse(json.dumps(context), content_type='application/json')

        else:
            context = {
                'message_type': 'error',
                'message': ugettext("Your image file should be contains this extensions: *.jpg, *.bmp, *.png"),
                'redirect_url': '/myprojects/new-project/',
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        context = {
            'message_type': 'error',
            'message': ugettext("Your web session seems to be ended. You've to login again. Thanks."),
            'redirect_url': "/login/",
        }
        return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def project_general_infos(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            # try:
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            summary = request.POST.get("summary")
            sector = request.POST.get("sector")
            usage_types = json.loads(request.POST.get("usage_types"))
            website_url = request.POST.get("website")
            project_id = request.POST.get("project_id")
            title = request.POST.get("title")

            if Projects.isThereProject(project_id):
                if UsersProjects.isProjectForThisUser(users_fullprofiles, Projects.select(project_id)):
                    # proje var ve proje bu kullanıcının ise
                    # ozaman proje editlenecek
                    selected_project = Projects.select(project_id)
                    try:
                        general_infos = GeneralInfos.create(title, summary, website_url,
                                                            selected_project.general_infos.id)
                    except:
                        general_infos = GeneralInfos.create(title, summary, website_url, 0)

                    projects = Projects.create_generals(general_infos, project_id)
                    ProjectPrjSectors.create(projects, ProjectSector.select(sector))

                    for usageTypes in usage_types:
                        ProjectUsageTypes.create(projects, UsageTypes.select(usageTypes))

                    if len(summary) >= 120:
                        ProjectPointsScores.create(projects, PointScores.select("project-summary"))

                    else:
                        ProjectPointsScores.delete_point(projects, PointScores.select("project-summary"))

                    ProjectPointsScores.create(projects, PointScores.select("project-sector"))
                    ProjectPointsScores.create(projects, PointScores.select("usage-type"))

                    if ProjectPointsScores._check_scores(projects, PointScores.select(
                            "project-summary")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                        "project-sector")) and ProjectPointsScores._check_scores(projects,
                                                                                 PointScores.select("usage-type")):
                        ProjectsSteps.create(projects, Steps.select("idea"))

                        if ProjectPointsScores._check_scores(projects, PointScores.select(
                                "target-group")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                            "revenue-model")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                            "existing-requirements")) and ProjectPointsScores._check_scores(projects,
                                                                                            PointScores.select(
                                                                                                "needed-requirements")) and ProjectPointsScores._check_scores(
                            projects, PointScores.select("competitors")):
                            ProjectsSteps.create(projects, Steps.select("business-model"))

                    else:
                        ProjectsSteps.delete_steps(projects, Steps.select("idea"))
                        ProjectsSteps.delete_steps(projects, Steps.select("business-model"))

                    context = {
                        "message_type": "success",
                        "message": ugettext(
                            "General Information was successfully edited. Next, you have to define Business Model of your project."),
                        "project_id": projects.id,
                        "redirect_url": "",
                    }

                else:
                    # bu kısımda hata mesajı çıkartalım!!! eğer proje kullanıcının değilse
                    # zaten html tarafından oynamış demektir.
                    context = {
                        "message_type": "warning",
                        "message": ugettext(
                            "I think, you are expecting a back door. We're sorry for this. You have to create a new project."),
                        "redirect_url": "/dashboard/myprojects/new-project/",
                    }

            else:
                # proje yok
                # yeni proje oluşturulacak
                general_infos = GeneralInfos.create(title, summary, website_url, 0)
                projects = Projects.create_generals(general_infos, 0)
                ProjectPrjSectors.create(projects, ProjectSector.select(sector))
                UsersProjects.create(users_fullprofiles, projects,0)
                for usageTypes in usage_types:
                    ProjectUsageTypes.create(projects, UsageTypes.select(usageTypes))

                if len(summary) >= 120:
                    ProjectPointsScores.create(projects, PointScores.select("project-summary"))

                ProjectPointsScores.create(projects, PointScores.select("project-sector"))
                ProjectPointsScores.create(projects, PointScores.select("usage-type"))

                if ProjectPointsScores._check_scores(projects, PointScores.select(
                        "project-summary")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "project-sector")) and ProjectPointsScores._check_scores(projects,
                                                                             PointScores.select("usage-type")):
                    ProjectsSteps.create(projects, Steps.select("idea"))
                else:
                    ProjectsSteps.delete_steps(projects, Steps.select("idea"))

                UsersMembershipTermsType.create(users_fullprofiles, "add-project",
                                                users_fullprofiles.users.users_membershiptypes.name_code)

                context = {
                    "message_type": "success",
                    "message": ugettext(
                        "General Information was successfully saved. Next, you have to define Business Model of your project."),
                    "project_id": projects.id,
                    "redirect_url": "",
                }
            # except:
            #     context = {
            #         "message_type": "error",
            #         "message": ugettext("An error was encountered during the save phase. Please, try again later."),
            #         "project_id": 0,
            #         "redirect_url": "",
            #     }

            return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def project_business_model(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            project_id = request.POST.get("project_id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            target_group = request.POST.get("target-group")
            revenue_model = request.POST.get("revenue-model")
            if Projects.isThereProject(project_id):
                # böyle bir proje mevcutmu?
                if UsersProjects.isProjectForThisUser(users_fullprofiles, Projects.select(project_id)):
                    # mevcut olan proje bu kullanıcıya mı ait?
                    try:
                        business_plan = BusinessPlans.create(target_group, "", revenue_model,
                                                             Projects.select(project_id).business_plans.id)
                    except:
                        business_plan = BusinessPlans.create(target_group, "", revenue_model, 0)

                    # bu kısımda business stage - iş planı hazır işaretlenecek

                    projects = Projects.create_business(business_plan, project_id)

                    if len(target_group) > 70:
                        ProjectPointsScores.create(projects, PointScores.select("target-group"))
                    else:
                        ProjectPointsScores.delete_point(projects, PointScores.select("target-group"))

                    if len(revenue_model) > 70:
                        ProjectPointsScores.create(projects, PointScores.select("revenue-model"))
                    else:
                        ProjectPointsScores.delete_point(projects, PointScores.select("revenue-model"))

                    if ProjectPointsScores._check_scores(projects, PointScores.select(
                            "target-group")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                        "revenue-model")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                        "existing-requirements")) and ProjectPointsScores._check_scores(projects,
                                                                                        PointScores.select(
                                                                                            "needed-requirements")) and ProjectPointsScores._check_scores(
                        projects, PointScores.select("competitors")):
                        ProjectsSteps.create(projects, Steps.select("business-model"))
                    else:
                        ProjectsSteps.delete_steps(projects, Steps.select("business-model"))

                    context = {
                        "message_type": "success",
                        "message": ugettext(
                            "Business model was successfully saved. Next, you have to define Requirements of your project."),
                        "project_id": projects.id,
                        "redirect_url": "",
                    }
                else:
                    # mevcut olan proje bu kullanıcıya ait değilse
                    context = {
                        "message_type": "warning",
                        "message": ugettext(
                            "I think, you are expecting a back door. We're sorry for this. You have to create a new project."),
                        "redirect_url": "/dashboard/myprojects/new-project/",
                    }
            else:
                # böyle bir proje mevcut değilse yeni proje oluştur
                business_plan = BusinessPlans.create(target_group, "", revenue_model, 0)
                projects = Projects.create_business(business_plan, 0)

                if len(target_group) > 70:
                    ProjectPointsScores.create(projects, PointScores.select("target-group"))

                if len(revenue_model) > 70:
                    ProjectPointsScores.create(projects, PointScores.select("revenue-model"))

                # bu kısımda business stage - iş planı hazır işaretlenecek

                if ProjectPointsScores._check_scores(projects, PointScores.select(
                        "target-group")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "revenue-model")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "existing-requirements")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "needed-requirements")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "competitors")):

                    ProjectsSteps.create(projects, Steps.select("business-model"))

                else:
                    ProjectsSteps.delete_steps(projects, Steps.select("business-model"))

                # olurda işlem kesintiye uğrarsa ve yeni proje yaratılırsa
                UsersMembershipTermsType.create(users_fullprofiles, "add-project",
                                                users_fullprofiles.users.users_membershiptypes.name_code)

                context = {
                    "message_type": "success",
                    "message": ugettext(
                        "Business model was successfully saved. Next, you have to define Requirements of your project."),
                    "project_id": projects.id,
                    "redirect_url": "",
                }

            return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def project_requirements_model(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            project_id = request.POST.get("project_id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            existing_requiremets = request.POST.get("existing-requirements")
            needed_requirements = request.POST.get("needed-requirements")

            if Projects.isThereProject(project_id):
                if UsersProjects.isProjectForThisUser(users_fullprofiles, Projects.select(project_id)):
                    try:
                        requirements = Requirements.create(existing_requiremets, needed_requirements,
                                                           Projects.select(project_id).requirements.id)
                    except:
                        requirements = Requirements.create(existing_requiremets, needed_requirements, 0)

                    projects = Projects.create_requirements(requirements, project_id)
                    if len(existing_requiremets) > 70:
                        ProjectPointsScores.create(projects, PointScores.select("existing-requirements"))
                    else:
                        ProjectPointsScores.delete_point(projects, PointScores.select("existing-requirements"))

                    if len(needed_requirements) > 70:
                        ProjectPointsScores.create(projects, PointScores.select("needed-requirements"))
                    else:
                        ProjectPointsScores.delete_point(projects, PointScores.select("needed-requirements"))

                    if ProjectPointsScores._check_scores(projects, PointScores.select(
                            "existing-requirements")) and ProjectPointsScores._check_scores(projects,
                                                                                            PointScores.select(
                                                                                                "needed-requirements")) and ProjectPointsScores._check_scores(
                        projects, PointScores.select("target-group")) and ProjectPointsScores._check_scores(
                        projects, PointScores.select("revenue-model")) and ProjectPointsScores._check_scores(
                        projects, PointScores.select("competitors")):

                        ProjectsSteps.create(projects, Steps.select("business-model"))

                    else:
                        ProjectsSteps.delete_steps(projects, Steps.select("business-model"))

                    context = {
                        "message_type": "success",
                        "message": ugettext(
                            "Requirements was successfully saved. Next, you have to define Competitors of your project."),
                        "project_id": projects.id,
                        "redirect_url": "",
                    }
                else:
                    context = {
                        "message_type": "warning",
                        "message": ugettext(
                            "I think, you are expecting a back door. We're sorry for this. You have to create a new project."),
                        "redirect_url": "/dashboard/myprojects/new-project/",
                    }

            else:
                # proje yoksa yeni bir proje oluştur
                requirements = Requirements.create(existing_requiremets, needed_requirements, 0)
                projects = Projects.create_requirements(requirements, 0)

                if len(existing_requiremets) > 70:
                    ProjectPointsScores.create(projects, PointScores.select("existing-requirements"))

                if len(needed_requirements) > 70:
                    ProjectPointsScores.create(projects, PointScores.select("needed-requirements"))

                if ProjectPointsScores._check_scores(projects, PointScores.select(
                        "existing-requirements")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "needed-requirements")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "target-group")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "revenue-model")) and ProjectPointsScores._check_scores(projects,
                                                                            PointScores.select("competitors")):
                    ProjectsSteps.create(projects, Steps.select("business-model"))
                else:
                    ProjectsSteps.delete_steps(projects, Steps.select("business-model"))

                UsersMembershipTermsType.create(users_fullprofiles, "add-project",
                                                users_fullprofiles.users.users_membershiptypes.name_code)

                context = {
                    "message_type": "success",
                    "message": ugettext(
                        "Requirements was successfully saved. Next, you have to define Competitors of your project."),
                    "project_id": projects.id,
                    "redirect_url": "",
                }

            return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def project_competitors_model(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            project_id = request.POST.get("project_id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            competitor_content = request.POST.get("competitor")

            if Projects.isThereProject(project_id):
                if UsersProjects.isProjectForThisUser(users_fullprofiles, Projects.select(project_id)):
                    try:
                        competitors = Competitors.create(competitor_content, Projects.select(project_id).competitors.id)
                    except:
                        competitors = Competitors.create(competitor_content, 0)

                    projects = Projects.create_competitors(competitors, project_id)

                    if len(competitor_content) > 15:
                        ProjectPointsScores.create(projects, PointScores.select("competitors"))
                    else:
                        ProjectPointsScores.delete_point(projects, PointScores.select("competitors"))

                    if ProjectPointsScores._check_scores(projects, PointScores.select(
                            "target-group")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                        "revenue-model")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                        "existing-requirements")) and ProjectPointsScores._check_scores(projects,
                                                                                        PointScores.select(
                                                                                            "needed-requirements")) and ProjectPointsScores._check_scores(
                        projects, PointScores.select("competitors")):

                        ProjectsSteps.create(projects, Steps.select("business-model"))

                    else:
                        ProjectsSteps.delete_steps(projects, Steps.select("business-model"))

                    context = {
                        "message_type": "success",
                        "message": ugettext(
                            "Competitor Information was successfully saved. Next, you have to enter Business Stage of your project."),
                        "project_id": projects.id,
                        "redirect_url": "",
                    }
                else:
                    context = {
                        "message_type": "warning",
                        "message": ugettext(
                            "I think, you are expecting a back door. We're sorry for this. You have to create a new project."),
                        "redirect_url": "/dashboard/myprojects/new-project/",
                    }
            else:
                competitors = Competitors.create(competitor_content, 0)
                projects = Projects.create_competitors(competitors, 0)

                if len(competitor_content) > 15:
                    ProjectPointsScores.create(projects, PointScores.select("competitors"))

                if ProjectPointsScores._check_scores(projects, PointScores.select(
                        "target-group")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "revenue-model")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "existing-requirements")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "needed-requirements")) and ProjectPointsScores._check_scores(projects, PointScores.select(
                    "competitors")):

                    ProjectsSteps.create(projects, Steps.select("business-model"))

                else:
                    ProjectsSteps.delete_steps(projects, Steps.select("business-model"))

                UsersMembershipTermsType.create(users_fullprofiles, "add-project",
                                                users_fullprofiles.users.users_membershiptypes.name_code)

                context = {
                    "message_type": "success",
                    "message": ugettext(
                        "Competitor Information was successfully saved. Next, you have to enter Business Stage of your project."),
                    "project_id": projects.id,
                    "redirect_url": "",
                }

            return HttpResponse(json.dumps(context), content_type='application/json')

@never_cache
@csrf_protect
def project_business_stage_check(request):
    if 'user_fullprofile' in request.session:
        context = []
        if request.method == "GET":
            project_id = request.GET.get("project_id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            if Projects.isThereProject(project_id):
                if UsersProjects.isProjectForThisUser(users_fullprofiles, Projects.select(project_id)):
                    selected_project = Projects.select(project_id)

                    if ProjectsSteps.isThereThisStep(selected_project, Steps.select("idea")):
                        item = {"name": "idea", "situation": "enabled"}
                        context.append(item)
                    else:
                        item = {"name": "idea", "situation": "disabled"}
                        context.append(item)

                    if ProjectsSteps.isThereThisStep(selected_project,
                                                     Steps.select("business-model")) and ProjectsSteps.isThereThisStep(
                        selected_project, Steps.select("idea")):
                        item = {"name": "business-model", "situation": "enabled"}
                        context.append(item)
                    else:
                        item = {"name": "business-model", "situation": "disabled"}
                        context.append(item)

                    if ProjectsSteps.isThereThisStep(selected_project,
                                                     Steps.select("teammates")) and ProjectsSteps.isThereThisStep(
                        selected_project, Steps.select("business-model")) and ProjectsSteps.isThereThisStep(
                        selected_project, Steps.select("idea")):
                        item = {"name": "teammates", "situation": "enabled"}
                        context.append(item)
                    else:
                        item = {"name": "teammates", "situation": "disabled"}
                        context.append(item)

                    if ProjectsSteps.isThereThisStep(selected_project,
                                                     Steps.select("teammates")) and ProjectsSteps.isThereThisStep(
                        selected_project, Steps.select("business-model")) and ProjectsSteps.isThereThisStep(
                        selected_project, Steps.select("idea")):

                        # item = {"name": "active-options", "situation": "enabled"}
                        # context.append(item)

                        if ProjectsSteps.isThereThisStep(selected_project, Steps.select("mvp-product")):
                            item = {"name": "mvp-product", "situation": "enabled"}
                            context.append(item)
                        else:
                            item = {"name": "mvp-product", "situation": "disabled", "is_open": True}
                            context.append(item)

                        if ProjectsSteps.isThereThisStep(selected_project,
                                                         Steps.select("mvp-product")) and ProjectsSteps.isThereThisStep(
                            selected_project, Steps.select("paying-customers")):
                            item = {"name": "paying-customers", "situation": "enabled"}
                            context.append(item)
                        else:
                            item = {"name": "paying-customers", "situation": "disabled", "is_open": True}
                            context.append(item)



                    else:
                        item = {"name": "mvp-product", "situation": "disabled", "is_open": False}
                        context.append(item)
                        item = {"name": "paying-customers", "situation": "disabled", "is_open": False}
                        context.append(item)

                        # item = {"name": "active-options", "situation": "disabled"}
                        # context.append(item)

                    return HttpResponse(json.dumps(context), content_type="application/json")
            else:
                if request.LANGUAGE_CODE == "tr":
                    message="Öncelikle diğer adımları tamamlamalısınız."
                else:
                    message = "You have to complete other steps, first."

                context = {
                    "message_type": "error",
                    "message": message,
                }
                return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def project_options_change(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            value_code = request.GET.get("value")
            project_id = request.GET.get("project_id")
            if "true" in request.GET.get("situation"):
                situation = True
            else:
                situation = False
            try:
                users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
                if Projects.isThereProject(project_id):
                    if UsersProjects.isProjectForThisUser(users_fullprofiles, Projects.select(project_id)):
                        if ProjectPointsScores.calculate_points(Projects.select(project_id)) >= 95 and ProjectsSteps.isThereThisStep(Projects.select(project_id), Steps.select("idea")) and ProjectsSteps.isThereThisStep(Projects.select(project_id), Steps.select("business-model")):
                            ProjectsProjectOptions.create(Projects.select(project_id),ProjectOptions.select(value_code), situation)

                            if "give-feedback" in value_code or "send-teamrequest" in value_code:
                                if situation == True and (ProjectsProjectOptions.select_option(Projects.select(project_id),ProjectOptions.select("give-feedback")).situation == False or ProjectsProjectOptions.select_option(Projects.select(project_id),ProjectOptions.select("send-teamrequest")).situation == False):
                                    context = {
                                        "message_type": "success",
                                        "message": ugettext(
                                            "Your project is online now. I hope, you can collabrate with persons. If we are faced with meaningless narration, we could take your project offline."),
                                        "value_str": value_code,
                                        "trigger_this": True,
                                    }
                                else:
                                    context = {
                                        "message_type": "success",
                                        "message": ugettext(
                                            "Your project is online now. I hope, you can collabrate with persons. If we are faced with meaningless narration, we could take your project offline."),
                                        "value_str": value_code,
                                        "trigger_this": False,
                                    }

                                notification_message_tr = get_profile_name_str(Projects.select(
                                    project_id).usersprojects_set.get().users_fullprofiles.full_profile.last_name,Projects.select(project_id).usersprojects_set.get().users_fullprofiles.full_profile.first_name) + " yeni bir proje oluşturdu."
                                notification_message_en = get_profile_name_str(Projects.select(project_id).usersprojects_set.get().users_fullprofiles.full_profile.last_name,Projects.select(project_id).usersprojects_set.get().users_fullprofiles.full_profile.first_name) + " created a new project."

                                payload = {
                                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                    "include_player_ids": new_signals_users_list(Projects.select(project_id).usersprojects_set.get().users_fullprofiles.id),
                                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                                    "url": "/dashboard/projects/detail/" + str(Projects.select(project_id).usersprojects_set.get().id) + "/"
                                }

                                requests.post("https://onesignal.com/api/v1/notifications",
                                              headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))

                                return HttpResponse(json.dumps(context), content_type="application/json")
                            else:
                                context = {
                                    "message_type": "success",
                                    "message": ugettext(
                                        "Your project is online now. I hope, you can collabrate with persons. If we are faced with meaningless narration, we could take your project offline."),
                                    "value_str": value_code,
                                    "trigger_this": False,
                                }

                                notification_message_tr = get_profile_name_str(Projects.select(
                                    project_id).usersprojects_set.get().users_fullprofiles.full_profile.last_name,Projects.select(project_id).usersprojects_set.get().users_fullprofiles.full_profile.first_name) + " yeni bir proje oluşturdu."

                                notification_message_en = get_profile_name_str(
                                    Projects.select(project_id).usersprojects_set.get().users_fullprofiles.full_profile.last_name,Projects.select(project_id).usersprojects_set.get().users_fullprofiles.full_profile.first_name) + " created a new project."

                                payload = {
                                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                    "include_player_ids": new_signals_users_list(Projects.select(project_id).usersprojects_set.get().users_fullprofiles.id),
                                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                                    "url": "/dashboard/projects/detail/" + str(Projects.select(project_id).usersprojects_set.get().id) + "/"
                                }

                                requests.post("https://onesignal.com/api/v1/notifications",headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))

                                return HttpResponse(json.dumps(context), content_type="application/json")

                        else:
                            context = {
                                "message_type": "error",
                                "message": ugettext(
                                    "Your project score should 95 point at least and you have to fill all fields if you need feedback or build team."),
                            }
                            return HttpResponse(json.dumps(context), content_type="application/json")
                    else:
                        context = {
                            "message_type": "warning",
                            "message": ugettext(
                                "I think, you are expecting a back door. We're sorry for this. You have to create a new project."),
                        }
                        return HttpResponse(json.dumps(context), content_type="application/json")


                else:
                    if request.LANGUAGE_CODE == "tr":
                        message = "Öncelikle diğer adımları tamamlamalısınız."
                    else:
                        message = "You have to complete other steps, first."

                    context = {
                        "message_type": "error",
                        "message": message,
                    }
                    return HttpResponse(json.dumps(context), content_type="application/json")
            except:
                context = {
                    "message_type": "error",
                    "message": ugettext(
                        "An error was encountered during the save phase. Try again later, please. Thanks"),
                }
                return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def get_project_options(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            context = []
            value_code = request.GET.get("value")
            project_id = request.GET.get("project_id")
            if Projects.isThereProject(project_id):
                try:
                    selected_option = ProjectsProjectOptions.select_option(Projects.select(project_id),
                                                                           ProjectOptions.select(value_code))
                    item = {"title_code": value_code, "situation": selected_option.situation}
                    context.append(item)

                    return HttpResponse(json.dumps(context), content_type="application/json")
                except:
                    ProjectsProjectOptions.create(Projects.select(project_id), ProjectOptions.select(value_code), False)
                    item = {"title_code": value_code, "situation": False}
                    context.append(item)
                    return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def active_step(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            context = []
            step_code = request.GET.get("name")
            project_id = request.GET.get("id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            try:
                if Projects.isThereProject(project_id):
                    if UsersProjects.isProjectForThisUser(users_fullprofiles, Projects.select(project_id)):
                        if ProjectsSteps.isThereThisStep(Projects.select(project_id), Steps.select(step_code)):
                            if "mvp-product" in step_code:
                                ProjectsSteps.delete_steps(Projects.select(project_id), Steps.select(step_code))
                                ProjectsSteps.delete_steps(Projects.select(project_id),
                                                           Steps.select("paying-customers"))
                            else:
                                ProjectsSteps.delete_steps(Projects.select(project_id), Steps.select(step_code))

                            item = {
                                "message_type": "success",
                                "message": ugettext("Project steps is saved. Thanks"),
                            }
                            context.append(item)
                        else:
                            if "paying-customers" in step_code:
                                if ProjectsSteps.isThereThisStep(Projects.select(project_id),
                                                                 Steps.select("mvp-product")):
                                    ProjectsSteps.create(Projects.select(project_id), Steps.select(step_code))
                                    item = {
                                        "message_type": "success",
                                        "message": ugettext("Project steps is saved. Thanks"),
                                    }
                                    context.append(item)
                                else:
                                    item = {
                                        "message_type": "error",
                                        "message": ugettext(
                                            "This step was not saved. You've to mark Mvp Product first."),
                                    }
                                    context.append(item)
                            else:
                                ProjectsSteps.create(Projects.select(project_id), Steps.select(step_code))
                                item = {
                                    "message_type": "success",
                                    "message": ugettext("Project steps is saved. Thanks"),
                                }
                                context.append(item)
                    else:
                        item = {
                            "message_type": "warning",
                            "message": ugettext(
                                "I think, you are expecting a back door. We're sorry for this. You have to create a new project."),
                        }
                        context.append(item)
            except:
                item = {
                    "message_type": "error",
                    "message": ugettext(
                        "There was a problem during the saving process. Try again later, please."),
                }
                context.append(item)

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def project_teamrequests_and_feedbacks(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            context_projects = []
            page = request.POST.get("page")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            projects = UsersProjects.list_all(users_fullprofiles).order_by("-id")
            locations = FullprofileLocation.select(users_fullprofiles.full_profile)

            for project in projects:

                if request.LANGUAGE_CODE == "tr":
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[0].steps.step_name
                    except:
                        steps = "Henüz bir proje adımı bulunmuyor."

                    if UsersProjectsTeamrequests.isThere_any_request_to_project(project) or UsersProjectsFeedbacks.isThere_any_feedbacks(project):

                        item = {
                            "id": project.id,
                            "project_logo": project.projects.general_infos.project_logo.name,
                            "title": project.projects.general_infos.title,
                            "steps": steps,
                            "project_locations": locations.location.city + ", " + locations.location.country,
                            "project_name": project.projects.general_infos.title,
                            "is_teamrequest": ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select("send-teamrequest")),
                            "is_feedback": ProjectsProjectOptions.isItOnline(project.projects,
                                                                             ProjectOptions.select("give-feedback")),
                            "is_online": ProjectsProjectOptions.isItOnline(project.projects,
                                                                           ProjectOptions.select("publish-project")),

                        }
                        context_projects.append(item)

                else:
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[
                            0].steps.step_name_eng
                    except:
                        steps = "No project steps are found yet."

                    if ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select("publish-project")):
                        item = {
                            "id": project.id,
                            "project_logo": project.projects.general_infos.project_logo.name,
                            "title": project.projects.general_infos.title,
                            "steps": steps,
                            "project_locations": locations.location.city + ", " + locations.location.country,
                            "project_name": project.projects.general_infos.title,
                            "is_teamrequest": ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select(
                                "send-teamrequest")),
                            "is_feedback": ProjectsProjectOptions.isItOnline(project.projects,
                                                                             ProjectOptions.select("give-feedback")),
                            "is_online": ProjectsProjectOptions.isItOnline(project.projects,
                                                                           ProjectOptions.select("publish-project")),

                        }

                        context_projects.append(item)

            paginator = Paginator(context_projects, 5)

            try:
                projects_list = paginator.page(page)
            except PageNotAnInteger:
                projects_list = paginator.page(1)
            except EmptyPage:
                projects_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "project_list": list(projects_list),
                "has_next": projects_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")


@never_cache
def response_profiles(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

        team_person_limit = MembershipTermsType.select_terms_type(
            users_fullprofiles.users.users_membershiptypes.name_code, "person-to-team").limit_count
        extra_person_limit = MembershipTermsType.select_terms_type(
            users_fullprofiles.users.users_membershiptypes.name_code, "person-to-team-location").limit_count
        team_person_count = 0
        extra_person_count = 0

        if request.method == "POST":
            # kullanıcının membership ayarları
            users_fullprofile_list = request.POST.getlist("users[]")
            users_projects_id=request.POST.get("id")
            context_users = []

            # print(users_fullprofile_list)

            if UsersProjects.isProjectForThisUser(users_fullprofiles, UsersProjects.select_by_id(users_projects_id,users_fullprofiles).projects):
                # proje bizim
                selected_users_projects = UsersProjects.select_by_id(users_projects_id, users_fullprofiles)
                if Teams.is_there_team_by_project(selected_users_projects):

                    # proje için kurulmuş mevcut bir takım var
                    selected_team = Teams.select_team(selected_users_projects)
                    team_person_count = (selected_team.usersteams_set.count() - 1)
                    extra_person_count = UsersTeams.my_location_teammembers_count(selected_team.users_projects,users_fullprofiles.id)

                    for user_item in select_teammembers_array(users_fullprofile_list,users_fullprofiles.id,team_person_limit, extra_person_limit):
                        # teklif gönderen kişileri alıyoruz
                        if team_person_limit == -1:
                            requested_user = UsersFullProfile.select_user(user_item)

                            if not "default" in requested_user.full_profile.picture_url.name:
                                profile_img = requested_user.full_profile.picture_url.url
                            else:
                                profile_img = requested_user.full_profile.linkedin_picture_url

                            item_user = {
                                "profile_img": profile_img,
                                "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                                                                     requested_user.full_profile.first_name),
                            }
                            context_users.append(item_user)
                            team_person_count += 1
                        else:
                            if is_it_same_location(selected_team.users_projects.users_fullprofiles.id,UsersFullProfile.select_user(user_item).id) == False:
                                # farklı konumda
                                # print(team_person_count)
                                # print(team_person_limit)
                                # print(extra_person_count)
                                if team_person_count < team_person_limit:

                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_profile_name_str(requested_user.full_profile.last_name,requested_user.full_profile.first_name),
                                    }
                                    context_users.append(item_user)
                                    team_person_count += 1

                            else:
                                # aynı konumda
                                # print(team_person_count)
                                # print(team_person_limit)
                                # print(extra_person_count)

                                if extra_person_count < extra_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_profile_name_str(requested_user.full_profile.last_name,requested_user.full_profile.first_name),
                                    }
                                    context_users.append(item_user)
                                    extra_person_count += 1
                                elif team_person_count + extra_person_count < team_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                                                                             requested_user.full_profile.first_name),
                                    }
                                    context_users.append(item_user)
                                    team_person_count += 1



                    sub_context = {
                        "message_type": "success",
                        "message": "",
                    }
                else:
                    # proje için yeni bir takım kurulacak
                    team_person_count = 0
                    extra_person_count = 0
                    for user_item in select_teammembers_array(users_fullprofile_list,users_fullprofiles.id,team_person_limit, extra_person_limit):
                        if team_person_limit == -1:
                            requested_user = UsersFullProfile.select_user(user_item)

                            if not "default" in requested_user.full_profile.picture_url.name:
                                profile_img = requested_user.full_profile.picture_url.url
                            else:
                                profile_img = requested_user.full_profile.linkedin_picture_url

                            item_user = {
                                "profile_img": profile_img,
                                "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                                                                     requested_user.full_profile.first_name),
                            }
                            context_users.append(item_user)
                            team_person_count += 1
                        else:
                            if is_it_same_location(selected_users_projects.users_fullprofiles.id,UsersFullProfile.select_user(user_item).id) == False:
                                if team_person_count < team_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_profile_name_str(requested_user.full_profile.last_name,requested_user.full_profile.first_name),
                                    }
                                    context_users.append(item_user)
                                    team_person_count += 1

                            else:
                                if extra_person_count < extra_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_profile_name_str(requested_user.full_profile.last_name,requested_user.full_profile.first_name),
                                    }
                                    context_users.append(item_user)
                                    extra_person_count += 1
                                elif team_person_count + extra_person_count < team_person_limit:
                                    requested_user = UsersFullProfile.select_user(user_item)

                                    if not "default" in requested_user.full_profile.picture_url.name:
                                        profile_img = requested_user.full_profile.picture_url.url
                                    else:
                                        profile_img = requested_user.full_profile.linkedin_picture_url

                                    item_user = {
                                        "profile_img": profile_img,
                                        "profile_name": get_profile_name_str(requested_user.full_profile.last_name,
                                                                             requested_user.full_profile.first_name),
                                    }
                                    context_users.append(item_user)
                                    team_person_count += 1

                    sub_context = {
                        "message_type": "success",
                        "message": "",
                    }

            else:
                # proje bize ait değil
                sub_context = {
                    "message_type": "error",
                    "message": ugettext("This project doesn't belong to you"),
                }

            if len(context_users) == 0:

                if team_person_count < team_person_limit:
                    if request.LANGUAGE_CODE == "tr":
                        message_type="warning"
                        message="Konumunuz dışından " + str(team_person_limit) + " adet kişi ekleyebilirsiniz  veya üyelik paketinizi yükselterek daha fazla kişi ekleyebilirsiniz."
                    else:
                        message = "You can add " + str(team_person_limit) + " person/people from outside your location. Or you should benefit our paid membership if you want to include more team member.",
                        message_type = "warning"


                elif extra_person_count < extra_person_limit:
                    if request.LANGUAGE_CODE == "tr":
                        message_type="error",
                        message="Mevcut projenize konumunuzdan sadece " + str(extra_person_limit) +" adet kişi ekleyebilirsiniz. Daha fazla kişi eklemek isterseniz eğer mevcut üyelik paketini yükseltmeniz gerekmektedir."
                    else:
                        message= "You can't include a new team member for this project from your outside your location. You can add " + str(extra_person_limit) + " person/people from your location. Or you should benefit our paid membership if you want to include more team member.",
                        message_type = "error"
                else:

                    if team_person_count == team_person_limit:
                        message_type="error"
                        if request.LANGUAGE_CODE == "tr":
                            message="Mevcut projenize daha fazla kişiyi takıma ekleyemezsiniz. Daha fazla kişi eklemek için mevcut üyelik paketinizi yükseltmelisiniz."

                        else:
                            message="You can't include a new team member for your project. You should benefit our paid membership if you want to include more team member."


                    elif extra_person_count == extra_person_limit:
                        message_type="error"
                        if request.LANGUAGE_CODE == "tr":
                            message = "Mevcut projenize daha fazla kişiyi takıma ekleyemezsiniz. Daha fazla kişi eklemek için mevcut üyelik paketinizi yükseltmelisiniz."
                        else:
                            message = "You can't include a new team member for your project. You should benefit our paid membership if you want to include more team member."

                    else:
                        message_type = "warning"
                        if request.LANGUAGE_CODE == "tr":
                            message = "Takımınız için en az bir kişi seçmelisiniz."
                        else:
                            message = "You should choose at least one person for your team."




                context={
                    "message_type":message_type,
                    "message":message,
                    "users":context_users,
                    "sub_context": sub_context,
                }
            else:
                context = {
                    "message_type": "success",
                    "message": "",
                    "users":context_users,
                    "sub_context": sub_context,
                }

            # context = {
            #     "sub_context": sub_context,
            # }

            return HttpResponse(json.dumps(context), content_type="application/json")


@csrf_protect
@never_cache
def send_response_to_teamrequests(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            context_mail = []
            users_fullprofile_list = request.POST.getlist("users[]")
            users_projects_id = request.POST.get("id")

            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            # üyelik paketimizin özellikleri
            team_person_limit = MembershipTermsType.select_terms_type(
                users_fullprofiles.users.users_membershiptypes.name_code, "person-to-team").limit_count
            extra_person_limit = MembershipTermsType.select_terms_type(
                users_fullprofiles.users.users_membershiptypes.name_code, "person-to-team-location").limit_count
            # üyelik paketimizin özellikleri
            # extra_person_count = 0
            # team_person_count = 0

            if UsersProjects.isThereProjectForThisUser_by_id(users_projects_id, users_fullprofiles):
                # proje bize ait
                selected_users_projects = UsersProjects.select_by_id(users_projects_id, users_fullprofiles)

                if Teams.is_there_team_by_project(selected_users_projects):
                    # bu projeye ait bir takım var
                    selected_team = Teams.select_team(selected_users_projects)
                    team_person_count = (selected_team.usersteams_set.count() - 1)
                    extra_person_count = UsersTeams.my_location_teammembers_count(selected_users_projects,users_fullprofiles.id)

                    for user_item in select_teammembers_array(users_fullprofile_list,users_fullprofiles.id,team_person_limit, extra_person_limit):
                        # teklif veren kullanıcıları alıyoruz
                        if NotificationsUsersTeams.isThere_notification_by_project(UsersFullProfile.select_user(user_item),selected_team.users_projects) is False:
                            # daha önceden teklif değerlendirilmediyse
                            if team_person_limit == -1:
                                created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),selected_team)
                                # UsersFriend.create(users_fullprofiles, UsersFullProfile.select_user(user_item))
                                NotificationsUsersTeams.create_notification(created_users_teams)
                                UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_users_projects)

                                item_mail = {
                                    "first_name": UsersFullProfile.select_user(
                                        user_item).full_profile.first_name,
                                    "email_address": UsersFullProfile.select_user(
                                        user_item).users.email_address,
                                    "id": UsersFullProfile.select_user(user_item).id,
                                }
                                context_mail.append(item_mail)

                                UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_team.users_projects)
                                # **************** WORKER KODLARI ********************
                                notification_message_tr = get_profile_name_str(
                                    users_fullprofiles.full_profile.last_name,
                                    users_fullprofiles.full_profile.first_name) +"," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                notification_message_en = get_profile_name_str(
                                    users_fullprofiles.full_profile.last_name,
                                    users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                payload = {
                                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                    "include_player_ids": send_signals_to_projects_owner(UsersFullProfile.select_user(user_item).id),
                                    "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                }

                                requests.post("https://onesignal.com/api/v1/notifications",
                                              headers=settings.ONESIGNAL_HEADER,
                                              data=json.dumps(payload))

                                # **************** WORKER KODLARI ********************

                                team_person_count += 1
                            else:
                                if is_it_same_location(selected_team.users_projects.users_fullprofiles.id,UsersFullProfile.select_user(user_item).id) == False:
                                    # benimle farklı konumda
                                    if team_person_count < team_person_limit:

                                        created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),selected_team)
                                        # UsersFriend.create(users_fullprofiles, UsersFullProfile.select_user(user_item))
                                        NotificationsUsersTeams.create_notification(created_users_teams)
                                        UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item), selected_users_projects)
                                        item_mail = {
                                            "first_name": UsersFullProfile.select_user(
                                                user_item).full_profile.first_name,
                                            "email_address": UsersFullProfile.select_user(
                                                user_item).users.email_address,
                                            "id":UsersFullProfile.select_user(user_item).id,
                                        }
                                        context_mail.append(item_mail)

                                        UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_team.users_projects)

                                        # **************** WORKER KODLARI ********************
                                        notification_message_tr = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                        notification_message_en = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                        payload = {
                                            "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                            "include_player_ids": send_signals_to_projects_owner(
                                                UsersFullProfile.select_user(user_item).id),
                                            "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                        }

                                        requests.post("https://onesignal.com/api/v1/notifications",
                                                      headers=settings.ONESIGNAL_HEADER,
                                                      data=json.dumps(payload))

                                        # **************** WORKER KODLARI ********************

                                        team_person_count+=1

                                else:
                                    # benimle aynı konumda
                                    if extra_person_count < extra_person_limit:
                                        created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),selected_team)
                                        # UsersFriend.create(users_fullprofiles, UsersFullProfile.select_user(user_item))
                                        NotificationsUsersTeams.create_notification(created_users_teams)
                                        UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item), selected_users_projects)
                                        item_mail = {
                                            "first_name": UsersFullProfile.select_user(
                                                user_item).full_profile.first_name,
                                            "email_address": UsersFullProfile.select_user(
                                                user_item).users.email_address,
                                            "id": UsersFullProfile.select_user(user_item).id,
                                        }

                                        context_mail.append(item_mail)

                                        UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_team.users_projects)

                                        # **************** WORKER KODLARI ********************
                                        notification_message_tr = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                        notification_message_en = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                        payload = {
                                            "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                            "include_player_ids": send_signals_to_projects_owner(
                                                UsersFullProfile.select_user(user_item).id),
                                            "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                        }

                                        requests.post("https://onesignal.com/api/v1/notifications",
                                                      headers=settings.ONESIGNAL_HEADER,
                                                      data=json.dumps(payload))

                                        # **************** WORKER KODLARI ********************

                                        extra_person_count += 1
                                    elif team_person_count < team_person_limit:
                                        created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),selected_team)

                                        NotificationsUsersTeams.create_notification(created_users_teams)
                                        UsersProjectsTeamrequests.confirm_respond(
                                            UsersFullProfile.select_user(user_item), selected_users_projects)
                                        item_mail = {
                                            "first_name": UsersFullProfile.select_user(
                                                user_item).full_profile.first_name,
                                            "email_address": UsersFullProfile.select_user(
                                                user_item).users.email_address,
                                            "id": UsersFullProfile.select_user(user_item).id,
                                        }

                                        context_mail.append(item_mail)

                                        UsersProjectsTeamrequests.confirm_respond(
                                            UsersFullProfile.select_user(user_item), selected_team.users_projects)

                                        # **************** WORKER KODLARI ********************
                                        notification_message_tr = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                        notification_message_en = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                        payload = {
                                            "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                            "include_player_ids": send_signals_to_projects_owner(
                                                UsersFullProfile.select_user(user_item).id),
                                            "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                        }

                                        requests.post("https://onesignal.com/api/v1/notifications",
                                                      headers=settings.ONESIGNAL_HEADER,
                                                      data=json.dumps(payload))

                                        # **************** WORKER KODLARI ********************

                                        team_person_count += 1

                    if team_person_count > 0 or extra_person_count > 0:
                        ProjectsSteps.create(selected_team.users_projects.projects, Steps.select("teammates"))

                    if len(context_mail) > 0:
                        if responded_teamrequest_mail(context_mail,users_fullprofiles.id,selected_team.users_projects.projects.general_infos.title):
                            message = ugettext("You have successfully responded to team requests. The step of your current project has been successfully changed.")
                            # kişileri arkadaş olarak eşleştir => bu fonksiyon True-False dönüyor
                            #bu kısımda id yollaman lazım
                            match_friends_in_system(selected_team.users_projects)

                        else:
                            message = ugettext("You have successfully responded to team requests but there was a problem with sendin email to team members. The step of your current project has been successfully changed.")
                    else:
                        message = ugettext("You have already sent notifications to persons for your new project.")



                    sub_context = {
                        "message_type": "success",
                        "message": message,
                    }

                else:
                    # bu projeye ait bir takım yok - yeni takım kurulacak
                    if request.LANGUAGE_CODE == "tr":
                        team_title = selected_users_projects.projects.general_infos.title.capitalize() + " takımı"
                    else:
                        team_title = selected_users_projects.projects.general_infos.title.capitalize() + " team"

                    created_team = Teams.create(team_title, "", selected_users_projects)
                    UsersTeams.create(users_fullprofiles, created_team)
                    team_person_count = 0
                    extra_person_count = 0
                    for user_item in select_teammembers_array(users_fullprofile_list,users_fullprofiles.id,team_person_limit, extra_person_limit):
                        # teklif veren kullanıcıları çekiyoruz
                        if team_person_limit == -1:
                            created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)
                            # UsersFriend.create(users_fullprofiles, UsersFullProfile.select_user(user_item))
                            NotificationsUsersTeams.create_notification(created_users_teams)
                            UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_users_projects)
                            item_mail = {
                                "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                "id": UsersFullProfile.select_user(user_item).id,
                            }
                            context_mail.append(item_mail)

                            # **************** WORKER KODLARI ********************
                            notification_message_tr = get_profile_name_str(
                                users_fullprofiles.full_profile.last_name,
                                users_fullprofiles.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                            notification_message_en = get_profile_name_str(
                                users_fullprofiles.full_profile.last_name,
                                users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                            payload = {
                                "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                "include_player_ids": send_signals_to_projects_owner(
                                    UsersFullProfile.select_user(user_item).id),
                                "contents": {"en": notification_message_en, "tr": notification_message_tr}
                            }

                            requests.post("https://onesignal.com/api/v1/notifications",
                                          headers=settings.ONESIGNAL_HEADER,
                                          data=json.dumps(payload))

                            # **************** WORKER KODLARI ********************

                            team_person_count += 1
                        else:
                            if is_it_same_location(selected_users_projects.users_fullprofiles.id,UsersFullProfile.select_user(user_item).id) == False:
                                # benimle farklı konumda
                                if team_person_count < team_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)
                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_users_projects)
                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id": UsersFullProfile.select_user(user_item).id,
                                    }
                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        users_fullprofiles.full_profile.last_name,
                                        users_fullprofiles.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        users_fullprofiles.full_profile.last_name,
                                        users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    team_person_count += 1

                            else:
                                # benimle aynı konumda
                                if extra_person_count < extra_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)
                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_users_projects)

                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id":UsersFullProfile.select_user(user_item).id,
                                    }
                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        users_fullprofiles.full_profile.last_name,
                                        users_fullprofiles.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        users_fullprofiles.full_profile.last_name,
                                        users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    extra_person_count += 1
                                elif team_person_count < team_person_limit:
                                    created_users_teams = UsersTeams.create(UsersFullProfile.select_user(user_item),created_team)
                                    NotificationsUsersTeams.create_notification(created_users_teams)
                                    UsersProjectsTeamrequests.confirm_respond(UsersFullProfile.select_user(user_item),selected_users_projects)

                                    item_mail = {
                                        "first_name": UsersFullProfile.select_user(user_item).full_profile.first_name,
                                        "email_address": UsersFullProfile.select_user(user_item).users.email_address,
                                        "id": UsersFullProfile.select_user(user_item).id,
                                    }
                                    context_mail.append(item_mail)

                                    # **************** WORKER KODLARI ********************
                                    notification_message_tr = get_profile_name_str(
                                        users_fullprofiles.full_profile.last_name,
                                        users_fullprofiles.full_profile.first_name) + "," + selected_users_projects.projects.general_infos.title + " projesi için takım isteğinizi yanıtladı."

                                    notification_message_en = get_profile_name_str(
                                        users_fullprofiles.full_profile.last_name,
                                        users_fullprofiles.full_profile.first_name) + " replied your team request for " + selected_users_projects.projects.general_infos.title + " project."

                                    payload = {
                                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                        "include_player_ids": send_signals_to_projects_owner(
                                            UsersFullProfile.select_user(user_item).id),
                                        "contents": {"en": notification_message_en, "tr": notification_message_tr}
                                    }

                                    requests.post("https://onesignal.com/api/v1/notifications",
                                                  headers=settings.ONESIGNAL_HEADER,
                                                  data=json.dumps(payload))

                                    # **************** WORKER KODLARI ********************

                                    team_person_count += 1

                    if team_person_count > 0 or extra_person_count > 0:
                        ProjectsSteps.create(created_team.users_projects.projects, Steps.select("teammates"))

                    if len(context_mail) > 0:
                        if responded_teamrequest_mail(context_mail,users_fullprofiles.id,created_team.users_projects.projects.general_infos.title):
                            message = ugettext("You have successfully responded to team requests. The step of your current project has been successfully changed.")
                            #bu kısımda id yollaman lazım
                            match_friends_in_system(created_team.users_projects)
                        else:
                            message = ugettext("You have successfully responded to team requests but there was a problem with sendin email to team members. The step of your current project has been successfully changed.")
                    else:
                        message = ugettext("You have already sent notifications to persons for your new project.")

                    # bu fonksiyon True-False dönüyor


                    sub_context = {
                        "message_type": "success",
                        "message": message,
                    }

            else:
                # proje bizim değil
                sub_context = {
                    "sub_context": "error",
                    "message": ugettext("This project doesn't belong to you."),
                }



            context={
                "sub_context":sub_context,
            }


            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def funded_projects_count(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            funded_count = NotificationsUsersTeams.funded_projects_count(users_fullprofiles)
            # print(funded_count)
            context = {
                "funded_count": funded_count,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def selected_person_location_for_team(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_project_id=request.POST.get("id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            user=request.POST.get("user")
            selected_user=UsersFullProfile.select_user(user)
            selected_project=UsersProjects.select_by_id(users_project_id,users_fullprofiles)

            if is_it_same_location(selected_project.users_fullprofiles.id,selected_user.id):
                is_it_same=True
            else:
                is_it_same=False

            context={
                # "location_name":change_characters(selected_user.full_profile.fullprofilelocation_set.get().location.city),
                "is_it":is_it_same,
            }

            return HttpResponse(json.dumps(context),content_type="application/json")

@never_cache
@csrf_protect
def selected_person_for_team(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_project_id = request.POST.get("id")
            selected_users_list = request.POST.getlist("users[]")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            # owner_city = change_characters(users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city)
            same_user_counter = 0

            for member in selected_users_list:
                if is_it_same_location(users_fullprofiles.id,UsersFullProfile.select_user(member).id):
                    same_user_counter += 1

            context = {
                "location_counter": same_user_counter
            }



            return HttpResponse(json.dumps(context), content_type="application/json")
