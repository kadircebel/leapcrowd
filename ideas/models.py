from django.db import models


class Posts(models.Model):
    content_text = models.TextField(blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField()
    up_date = models.DateTimeField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'posts'

    @classmethod
    def create_post(cls,content_text):
        new_record = cls(content_text=content_text, is_active=True)
        new_record.save()
        return new_record

    @classmethod
    def is_there(cls,id):
        try:
            if cls.objects.filter(id=id).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def update_post(cls,content_text,id):
        selected=cls.objects.filter(id=id).get()
        selected.content_text=content_text
        selected.is_active=True
        selected.save()
        return selected


    @classmethod
    def select_post(cls,id):
        return cls.objects.filter(id=id).get()





class PostsFiles(models.Model):
    id = models.BigAutoField(primary_key=True)
    posts=models.ForeignKey('Posts',models.DO_NOTHING,blank=True)
    file_content = models.FileField(upload_to="posts/", default=None)
    is_active = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed=True
        db_table='posts_files'


class PostsComplained(models.Model):
    posts = models.ForeignKey('Posts', models.DO_NOTHING, blank=True)
    # posts=models.OneToOneField('Posts',on_delete=models.CASCADE)
    complained_options = models.ForeignKey('mainadmins.ComplainedOptions', models.DO_NOTHING, blank=True)
    # complained_options=models.OneToOneField('mainadmins.ComplainedOptions',on_delete=models.CASCADE)
    is_complained = models.NullBooleanField()
    content_text = models.TextField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'posts_complained'

    @classmethod
    def isThere_complained(cls,posts):
        try:
            if cls.objects.filter(posts=posts).count() > 0:
                return True
            else:
                return False
        except:
            return False

class PostsPostComplained(models.Model):
    users = models.ForeignKey('persons.Users', models.DO_NOTHING, blank=True)
    # users=models.OneToOneField('persons.Users',on_delete=models.CASCADE)
    post_complained = models.ForeignKey('PostsComplained', models.DO_NOTHING, blank=True)
    # post_complained=models.OneToOneField('PostsComplained',on_delete=models.CASCADE)
    is_read = models.NullBooleanField()
    is_active = models.NullBooleanField()
    created_date = models.DateTimeField(auto_created=True)
    up_date = models.DateTimeField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'posts_post_complained'

class PostsSmilarprojects(models.Model):
    posts = models.ForeignKey('Posts', models.DO_NOTHING, blank=True)
    link_address = models.CharField(max_length=250, blank=True)
    describe = models.CharField(max_length=500, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'posts_smilarprojects'

    @classmethod
    def post_smilarprojects_count(cls,posts):
        try:
            return cls.objects.filter(posts=posts).count()
        except:
            return 0

    @classmethod
    def create(cls,posts,link_address,describe):
        new_record=cls(posts=posts,link_address=link_address,describe=describe)
        new_record.save()
        return new_record





