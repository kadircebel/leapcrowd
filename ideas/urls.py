from django.urls import path
from .views import *



app_name="ideas"

urlpatterns=[
    path('',index,name='ideas'),

    path('detail/<int:id>/', detail, name='ideas_detail'),

    path('more-ideas/',lazy_load_ideas,name='more_ideas'),
    path('filter-ideas/', load_filter, name='filter_ideas'),
    path('get-post/', get_this_post, name='get_post'),
    path('write-answer/', write_answer_mainpost, name='write_answer'),
    path('write-answer-to/', writer_answer_answer, name='write_answer_to'),
    path('post-answers/', get_post_answers, name='post_answer'),
    path('edit-getpost/', edit_get_post, name='edit_get_post'),
    path('edit-mainpost/', edit_main_post, name='edit_main_post'),
    path('edit-getanswer/', edit_get_answer, name='edit_get_answer'),
    path('edit-mainanswer/', edit_main_answer, name='edit_main_answer'),
    path('edit-get-subanswer/', edit_get_subanswer, name='edit_get_subanswer'),
    path('edit-save-subanswer/', edit_save_subanswer, name='edit_save_subanswer'),

    path('more-sub-answers/', load_more_sub_answers, name='more_sub_answer'),
    path('more-main-answers/', load_more_main_answers, name='more_main_answer'),
    path('post-smilar-project/', add_smilar_project, name='post_smilar_project'),
    path('user-infos/', user_infos, name='user_infos'),
    path('project-supports/', users_project_supports, name='users_project_supports'),
    path('save-project-hours/', save_project_hours, name='save_project_hours'),
    path('save-support-types/', save_support_type, name='save_support_type'),
    path('send-teamrequest/', send_teamrequest, name='send_teamrequest'),
    path('share-idea/', share_idea, name='share_idea'),
    path('get-answer/', get_answer, name='get_answer'),
    path('write-quick-answers/', write_quick_answer, name='quick_answers'),

]