from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.conf import settings
from .models import Posts, PostsSmilarprojects
from persons.models import UsersPosts, UsersAnswers, UsersFullProfile, UsersAnswersAnswers,UsersPostsSmilarprojects, UsersProjectHours, UsersInvestments, UsersRequestInvestments, UsersHourCosts, UsersRequestProjectHours, Teamrequests, UsersPostsTeamrequests,UsersMembershipTermsType,UsersIndustries
from answers.models import Answers
from mainadmins.models import ProjectHours, Investments
from utils.views import get_profile_name_str, clean_hours_title,get_person_profile_name,is_it_same_location,is_it_same_country,get_post_answers_users_list,new_signals_users_list,send_signals_to_projects_owner
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from notifications.models import NotificationsAnswers, NotificationsSmilarProjects
import json,requests
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import ugettext
from django.utils.timesince import timesince

from django.views.decorators.cache import cache_page,never_cache


@never_cache
def index(request):


    if 'user_fullprofile' in request.session:
        context_ideas = []
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        location_city = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city
        location_country = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country

        ideas_list = UsersPosts.objects.filter(posts__is_active=True).all().order_by("-id")

        counter = 0
        for post in ideas_list:
            if counter <= 10:
                likes_count = post.users_fullprofiles.userspostslikes_set.count()
                teamrequest_count = post.userspoststeamrequests_set.count()
                smilar_projects_count = post.posts.postssmilarprojects_set.count()
                answers_count = post.usersanswers_set.count()

                if "entrepreneur" in post.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title = "Girişimci"
                    else:
                        position_title = post.users_fullprofiles.users.member_type
                else:
                    if len(post.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = post.users_fullprofiles.full_profile.position_title
                    else:
                        position_title = UsersIndustries.select(post.users_fullprofiles).industries.description
                        # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                item = {
                    "like_count": likes_count,
                    "teamrequest_count": teamrequest_count,
                    "smilarproject_count": smilar_projects_count,
                    "answer_count": answers_count,
                    "post_content": post.posts.content_text,
                    "post_id": post.id,
                    "profile_picture": profile_picture,
                    # "username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,
                    #                                  post.users_fullprofiles.full_profile.first_name),
                    "username": get_person_profile_name(users_fullprofiles.id, post.users_fullprofiles.id),
                    "position_title": position_title,
                    "created_date": post.posts.created_date,
                    "profile_id": post.users_fullprofiles.id,
                }

                context_ideas.append(item)
                counter += 1

        if len(ideas_list) > 10:
            has_next = True
        else:
            has_next = False

        context = {
            "posts": context_ideas,
            "user_city": location_city,
            "user_country": location_country,
            "option_country":users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood,
            "has_next": has_next,
            "page_title":ugettext("Ideas"),
        }

        return render(request, 'socialside/ideas/index.html', context)
    else:
        return HttpResponseRedirect("/login/")

@never_cache
def detail(request, id=0):
    if 'user_fullprofile' in request.session:
        if id != 0:
            context_ans = []
            selected_user_post = UsersPosts.select_this(id)
            profile_username = get_profile_name_str(selected_user_post.users_fullprofiles.full_profile.last_name,selected_user_post.users_fullprofiles.full_profile.first_name)

            if "entrepreneur" in selected_user_post.users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    position_title = "Girişimci"
                else:
                    position_title = selected_user_post.users_fullprofiles.users.member_type
            else:
                if len(selected_user_post.users_fullprofiles.full_profile.position_title) > 0:
                    position_title = selected_user_post.users_fullprofiles.full_profile.position_title
                else:
                    position_title=UsersIndustries.select(selected_user_post.users_fullprofiles).industries.description
                    # position_title = selected_user_post.users_fullprofiles.usersındustries_set.get().industries.description
            # print(selected_user_post.users_fullprofiles.full_profile.picture_url.name)
            if not "default" in selected_user_post.users_fullprofiles.full_profile.picture_url.name:
                profile_image = selected_user_post.users_fullprofiles.full_profile.picture_url.url
            else:
                profile_image = selected_user_post.users_fullprofiles.full_profile.linkedin_picture_url

            main_answers = UsersAnswers.answers_by_post(selected_user_post)[:6]

            if main_answers.count() > 0:
                for ans in main_answers[:5]:
                    # cevabın altındaki cevaplar
                    context_ans_ans = []
                    if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 2:
                        sub_has_next = True
                    else:
                        sub_has_next = False

                    # cevabın alt cevabı var ise
                    if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 0:

                        selected_ans_answers = ans.usersanswersanswers_set.filter(is_active=True, is_block=False).all().order_by("id")[:5]
                        for ans_ans in selected_ans_answers:

                            if "entrepreneur" in ans_ans.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title ="Girişimci"
                                else:
                                    position_title=ans_ans.users_fullprofiles.users.member_type
                            else:
                                if len(ans_ans.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = ans_ans.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(ans_ans.users_fullprofiles).industries.description
                                    # position_title = ans_ans.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in ans_ans.users_fullprofiles.full_profile.picture_url.name:
                                profile_image_sub_sub = ans_ans.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_image_sub_sub = ans_ans.users_fullprofiles.full_profile.linkedin_picture_url

                            item_ans_ans = {
                                "profile_image": profile_image_sub_sub,
                                "position_title": position_title,
                                "created_date": timesince(ans_ans.answers.created_date),
                                "like_count": ans_ans.answers.usersanswerslike_set.count(),
                                "answers_count": ans.answers.usersanswersanswers_set.count(),
                                "profile_name": get_profile_name_str(ans_ans.users_fullprofiles.full_profile.last_name,
                                                                     ans_ans.users_fullprofiles.full_profile.first_name),
                                "sub_answer_id": ans_ans.users_answers.id,
                                "answer_content": ans_ans.answers.answer_text,
                                "profile_id": ans_ans.users_fullprofiles.id,
                                "ans_ans_id": ans_ans.id,

                            }
                            context_ans_ans.append(item_ans_ans)

                    if "entrepreneur" in ans.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = ans.users_fullprofiles.users.member_type
                    else:
                        if len(ans.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = ans.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(ans.users_fullprofiles).industries.description


                    if not "default" in ans.users_fullprofiles.full_profile.picture_url.name:
                        profile_image_sub = ans.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_image_sub = ans.users_fullprofiles.full_profile.linkedin_picture_url

                    if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 0:

                        item_ans = {
                            "profile_image": profile_image_sub,
                            "position_title": position_title,
                            "profile_name": get_profile_name_str(ans.users_fullprofiles.full_profile.last_name,
                                                                 ans.users_fullprofiles.full_profile.first_name),
                            "like_count": ans.answers.usersanswerslike_set.count(),
                            "answers_count": ans.usersanswersanswers_set.count(),
                            "answer_content": ans.answers.answer_text,
                            "answer_id": ans.id,
                            "created_date": timesince(ans.answers.created_date),
                            "sub_answers": context_ans_ans,
                            "sub_hasnext": sub_has_next,
                            "profile_id": ans.users_fullprofiles.id,
                        }
                        context_ans.append(item_ans)
                    else:
                        context_ans_ans = []

                        if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() == 0 and ans.answers.usersanswersanswers_set.count() == 0:
                            item_ans = {
                                "profile_image": profile_image_sub,
                                "position_title": position_title,
                                "profile_name": get_profile_name_str(ans.users_fullprofiles.full_profile.last_name,
                                                                     ans.users_fullprofiles.full_profile.first_name),
                                "like_count": ans.answers.usersanswerslike_set.count(),
                                "answers_count": ans.usersanswersanswers_set.count(),
                                "answer_content": ans.answers.answer_text,
                                "answer_id": ans.id,
                                "created_date": timesince(ans.answers.created_date),
                                "sub_answers": context_ans_ans,
                                "sub_hasnext": sub_has_next,
                                "profile_id": ans.users_fullprofiles.id,
                            }
                            # print(item_ans)
                            context_ans.append(item_ans)

            if main_answers.count() > 5:
                main_hasnext = True
            else:
                main_hasnext = False

            context = {
                "smilarproject_count": selected_user_post.posts.postssmilarprojects_set.count(),
                "teamrequest_count": selected_user_post.userspoststeamrequests_set.count(),
                "like_count": selected_user_post.posts.userspostslikes_set.count(),
                "answer_count": selected_user_post.usersanswers_set.count(),
                "profile_position_title": position_title,
                "profile_username": profile_username,
                "post_content": selected_user_post.posts.content_text,
                "created_date": timesince(selected_user_post.posts.created_date),
                "profile_image": profile_image,
                "main_answers": context_ans,
                "post_id": selected_user_post.id,
                "main_hasnext": main_hasnext,
                "profile_id": selected_user_post.users_fullprofiles.id,
                "page_title":ugettext("Idea detail"),
            }

            # print(context)

            return render(request, "socialside/ideas/detail.html", context)
        else:
            return HttpResponseRedirect("/dashboard/ideas/")

    else:
        return HttpResponseRedirect("/login/")

@cache_page(60 * 5)
def lazy_load_ideas(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            context_ideas = []
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            location_city = request.POST.get("city")
            location_country = request.POST.get("country")
            is_answer = request.POST.get("unanswered")
            is_all = request.POST.get("all")

            page = request.POST.get("page")

            if len(location_city) < 1 and len(is_all) < 1 and len(location_country) < 1 and len(is_answer) < 1:
                location_city = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city
                # pass

            ideas_list = UsersPosts.objects.filter(posts__is_active=True).all().order_by("-id")

            for post in ideas_list:
                if len(location_city) > 0:
                    # şehir seçiliyse
                    if len(is_answer) > 0:
                        # seçilen şehre göre cevapsızları
                        if is_it_same_location(users_fullprofiles.id,post.users_fullprofiles.id) and post.usersanswers_set.count() == 0:
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()

                            if "entrepreneur" in post.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title="Girişimci"
                                else:
                                    position_title=post.users_fullprofiles.users.member_type
                            else:
                                if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = post.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                    # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            # if len(post.users_fullprofiles.full_profile.linkedin_picture_url) > 0:
                            #     profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url
                            # else:
                            #     profile_picture = post.users_fullprofiles.full_profile.picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }
                            context_ideas.append(item)
                    else:
                        # seçilen şehre göre tümünü
                        if is_it_same_location(users_fullprofiles.id,post.users_fullprofiles.id):
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()

                            if "entrepreneur" in post.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title = "Girişimci"
                                else:
                                    position_title = post.users_fullprofiles.users.member_type
                            else:
                                if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = post.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                    # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                # "created_date": str(post.posts.created_date),
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }
                            # print(item)
                            context_ideas.append(item)
                elif len(location_country) > 0:
                    if len(is_answer) > 0:
                        if is_it_same_country(users_fullprofiles.id,post.users_fullprofiles.id) and post.usersanswers_set.count() == 0:
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()

                            if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                position_title = post.users_fullprofiles.full_profile.position_title
                            else:
                                position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }

                            context_ideas.append(item)
                    else:
                        if is_it_same_country(users_fullprofiles.id,post.users_fullprofiles.id):
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()

                            if "entrepreneur" in post.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title ="Girişimci"
                                else:
                                    position_title=post.users_fullprofiles.users.member_type
                            else:
                                if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = post.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                    # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }

                            context_ideas.append(item)
                elif len(is_answer) > 0:
                    if post.usersanswers_set.count() == 0:
                        likes_count = post.users_fullprofiles.userspostslikes_set.count()
                        teamrequest_count = post.userspoststeamrequests_set.count()
                        smilar_projects_count = post.posts.postssmilarprojects_set.count()
                        answers_count = post.usersanswers_set.count()

                        if "entrepreneur" in post.users_fullprofiles.users.member_type:
                            if request.LANGUAGE_CODE == "tr":
                                position_title = "Girişimci"
                            else:
                                position_title=post.users_fullprofiles.users.member_type
                        else:
                            if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                position_title = post.users_fullprofiles.full_profile.position_title
                            else:
                                position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                        if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                        item = {
                            "like_count": likes_count,
                            "teamrequest_count": teamrequest_count,
                            "smilarproject_count": smilar_projects_count,
                            "answer_count": answers_count,
                            "post_content": post.posts.content_text,
                            "post_id": post.id,
                            # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,
                            #                                          post.users_fullprofiles.full_profile.first_name),
                            "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                            "position_title": position_title,
                            "created_date": timesince(post.posts.created_date),
                            "profile_picture": profile_picture,
                            "profile_id": post.users_fullprofiles.id,
                        }

                        context_ideas.append(item)
                elif len(is_all) > 0:
                    likes_count = post.users_fullprofiles.userspostslikes_set.count()
                    teamrequest_count = post.userspoststeamrequests_set.count()
                    smilar_projects_count = post.posts.postssmilarprojects_set.count()
                    answers_count = post.usersanswers_set.count()

                    if "entrepreneur" in post.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title ="Girişimci"
                        else:
                            position_title=post.users_fullprofiles.users.member_type
                    else:
                        if len(post.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = post.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                            # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                    if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                    item = {
                        "like_count": likes_count,
                        "teamrequest_count": teamrequest_count,
                        "smilarproject_count": smilar_projects_count,
                        "answer_count": answers_count,
                        "post_content": post.posts.content_text,
                        "post_id": post.id,
                        # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,
                        #                                          post.users_fullprofiles.full_profile.first_name),
                        "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                        "position_title": position_title,
                        "created_date": timesince(post.posts.created_date),
                        "profile_picture": profile_picture,
                        "profile_id": post.users_fullprofiles.id,
                    }

                    context_ideas.append(item)
                else:
                    likes_count = post.users_fullprofiles.userspostslikes_set.count()
                    teamrequest_count = post.userspoststeamrequests_set.count()
                    smilar_projects_count = post.posts.postssmilarprojects_set.count()
                    answers_count = post.usersanswers_set.count()

                    if "entrepreneur" in post.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = post.users_fullprofiles.users.member_type
                    else:
                        if len(post.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = post.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                            # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                    if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                    item = {
                        "like_count": likes_count,
                        "teamrequest_count": teamrequest_count,
                        "smilarproject_count": smilar_projects_count,
                        "answer_count": answers_count,
                        "post_content": post.posts.content_text,
                        "post_id": post.id,
                        # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,
                        #                                          post.users_fullprofiles.full_profile.first_name),
                        "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                        "position_title": position_title,
                        "created_date": timesince(post.posts.created_date),
                        "profile_picture": profile_picture,
                        "profile_id": post.users_fullprofiles.id,
                    }

                    context_ideas.append(item)

            paginator = Paginator(context_ideas, 10)

            try:
                posts_list = paginator.page(page)
            except PageNotAnInteger:
                posts_list = paginator.page(2)
            except EmptyPage:
                posts_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "posts": list(posts_list),
                "has_next": posts_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@cache_page(60 * 5)
@csrf_protect
def load_filter(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            context_ideas = []
            location_city = request.POST.get("city")
            location_country = request.POST.get("country")
            is_answer = request.POST.get("unanswered")
            is_all = request.POST.get("all")
            # page=request.POST.get("page")
            if len(location_city) < 1 and len(location_country) < 1 and len(is_answer) < 1 and len(is_all) < 1:
                location_city = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city

            ideas_list = UsersPosts.objects.filter(posts__is_active=True).all().order_by("-id")

            for post in ideas_list:
                if len(location_city) > 0:
                    # şehir seçiliyse
                    if len(is_answer) > 0:
                        # seçilen şehre göre cevapsızları
                        if is_it_same_location(users_fullprofiles.id,post.users_fullprofiles.id) and post.usersanswers_set.count() == 0:
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()

                            if "entrepreneur" in post.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title="Girişimci"
                                else:
                                    position_title=post.users_fullprofiles.users.member_type
                            else:
                                if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = post.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                    # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }
                            context_ideas.append(item)
                    else:
                        # seçilen şehre göre tümünü
                        # print(post.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city)
                        if is_it_same_location(users_fullprofiles.id,post.users_fullprofiles.id):
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()

                            if "entrepreneur" in post.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title="Girişimci"
                                else:
                                    position_title=post.users_fullprofiles.users.member_type
                            else:
                                if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = post.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                    # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }

                            context_ideas.append(item)
                elif len(location_country) > 0:
                    if len(is_answer) > 0:
                        if is_it_same_country(users_fullprofiles.id,post.users_fullprofiles.id) and post.usersanswers_set.count() == 0:
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()


                            if "entrepreneur" in post.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title = "Girişimci"
                                else:
                                    position_title=post.users_fullprofiles.users.member_type
                            else:
                                if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = post.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                    # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }

                            context_ideas.append(item)
                    else:
                        if is_it_same_country(users_fullprofiles.id,post.users_fullprofiles.id):
                            likes_count = post.users_fullprofiles.userspostslikes_set.count()
                            teamrequest_count = post.userspoststeamrequests_set.count()
                            smilar_projects_count = post.posts.postssmilarprojects_set.count()
                            answers_count = post.usersanswers_set.count()

                            if "entrepreneur" in post.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title = "Girişimci"
                                else:
                                    position_title = post.users_fullprofiles.users.member_type
                            else:
                                if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = post.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                    # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description
                            if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                            item = {
                                "like_count": likes_count,
                                "teamrequest_count": teamrequest_count,
                                "smilarproject_count": smilar_projects_count,
                                "answer_count": answers_count,
                                "post_content": post.posts.content_text,
                                "post_id": post.id,
                                # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,post.users_fullprofiles.full_profile.first_name),
                                "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                                "position_title": position_title,
                                "created_date": timesince(post.posts.created_date),
                                "profile_picture": profile_picture,
                                "profile_id": post.users_fullprofiles.id,
                            }

                            context_ideas.append(item)
                elif len(is_answer) > 0:
                    if post.usersanswers_set.count() == 0:
                        # print(UsersAnswers.post_answers_count(post))
                        likes_count = post.users_fullprofiles.userspostslikes_set.count()
                        teamrequest_count = post.userspoststeamrequests_set.count()
                        smilar_projects_count = post.posts.postssmilarprojects_set.count()
                        answers_count = post.usersanswers_set.count()

                        if "entrepreneur" in post.users_fullprofiles.users.member_type:
                            if request.LANGUAGE_CODE == "tr":
                                position_title ="Girişimci"
                            else:
                                position_title = post.users_fullprofiles.users.member_type
                        else:
                            if len(post.users_fullprofiles.full_profile.position_title) > 0:
                                position_title = post.users_fullprofiles.full_profile.position_title
                            else:
                                position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                                # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                        if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                        item = {
                            "like_count": likes_count,
                            "teamrequest_count": teamrequest_count,
                            "smilarproject_count": smilar_projects_count,
                            "answer_count": answers_count,
                            "post_content": post.posts.content_text,
                            "post_id": post.id,
                            # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,
                            #                                          post.users_fullprofiles.full_profile.first_name),
                            "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                            "position_title": position_title,
                            "created_date": timesince(post.posts.created_date),
                            "profile_picture": profile_picture,
                            "profile_id": post.users_fullprofiles.id,
                        }

                        context_ideas.append(item)

                elif len(is_all) > 0:
                    likes_count = post.users_fullprofiles.userspostslikes_set.count()
                    teamrequest_count = post.userspoststeamrequests_set.count()
                    smilar_projects_count = post.posts.postssmilarprojects_set.count()
                    answers_count = post.usersanswers_set.count()

                    if "entrepreneur" in post.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title="Girişimci"
                        else:
                            position_title=post.users_fullprofiles.users.member_type
                    else:
                        if len(post.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = post.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                            # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                    if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                    item = {
                        "like_count": likes_count,
                        "teamrequest_count": teamrequest_count,
                        "smilarproject_count": smilar_projects_count,
                        "answer_count": answers_count,
                        "post_content": post.posts.content_text,
                        "post_id": post.id,
                        # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,
                        #                                          post.users_fullprofiles.full_profile.first_name),
                        "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                        "position_title": position_title,
                        "created_date": timesince(post.posts.created_date),
                        "profile_picture": profile_picture,
                        "profile_id": post.users_fullprofiles.id,
                    }

                    context_ideas.append(item)
                else:
                    likes_count = post.users_fullprofiles.userspostslikes_set.count()
                    teamrequest_count = post.userspoststeamrequests_set.count()
                    smilar_projects_count = post.posts.postssmilarprojects_set.count()
                    answers_count = post.usersanswers_set.count()

                    if "entrepreneur" in post.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title="Girişimci"
                        else:
                            position_title = post.users_fullprofiles.users.member_type
                    else:
                        if len(post.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = post.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(post.users_fullprofiles).industries.description
                            # position_title = post.users_fullprofiles.usersındustries_set.get().industries.description

                    if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                    item = {
                        "like_count": likes_count,
                        "teamrequest_count": teamrequest_count,
                        "smilarproject_count": smilar_projects_count,
                        "answer_count": answers_count,
                        "post_content": post.posts.content_text,
                        "post_id": post.id,
                        # "profile_username": get_profile_name_str(post.users_fullprofiles.full_profile.last_name,
                        #                                          post.users_fullprofiles.full_profile.first_name),
                        "profile_username":get_person_profile_name(users_fullprofiles.id,post.users_fullprofiles.id),
                        "position_title": position_title,
                        "created_date": timesince(post.posts.created_date),
                        "profile_picture": profile_picture,
                        "profile_id": post.users_fullprofiles.id,
                    }

                    context_ideas.append(item)

            paginator = Paginator(context_ideas, 10)

            try:
                posts_list = paginator.page(1)
            except PageNotAnInteger:
                posts_list = paginator.page(1)
            except EmptyPage:
                posts_list = paginator.page(paginator.num_pages)

            output_data = {
                "posts": list(posts_list),
                "has_next": posts_list.has_next(),
                "page": 2,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")


@never_cache
@csrf_protect
def get_this_post(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            user_post_id = request.GET.get("id")
            selected_user_post = UsersPosts.select_this(user_post_id)
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            if selected_user_post.users_fullprofiles.id is users_fullprofiles.id:
                is_edit = True
            else:
                is_edit = False

            # profile_username = get_profile_name_str(selected_user_post.users_fullprofiles.full_profile.last_name,
            #                                         selected_user_post.users_fullprofiles.full_profile.first_name)
            profile_username = get_person_profile_name(users_fullprofiles.id,selected_user_post.users_fullprofiles.id)

            if "entrepreneur" in selected_user_post.users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    position_title="Girişimci"
                else:
                    position_title=selected_user_post.users_fullprofiles.users.member_type
            else:
                if len(selected_user_post.users_fullprofiles.full_profile.position_title) > 0:
                    position_title = selected_user_post.users_fullprofiles.full_profile.position_title
                else:
                    position_title=UsersIndustries.select(selected_user_post.users_fullprofiles).industries.description
                    # position_title = selected_user_post.users_fullprofiles.usersındustries_set.get().industries.description

            if not "default" in selected_user_post.users_fullprofiles.full_profile.picture_url.name:
                profile_image = selected_user_post.users_fullprofiles.full_profile.picture_url.url
            else:
                profile_image = selected_user_post.users_fullprofiles.full_profile.linkedin_picture_url

            context = {
                "smilarproject_count": selected_user_post.posts.postssmilarprojects_set.count(),
                "teamrequest_count": selected_user_post.userspoststeamrequests_set.count(),
                "like_count": selected_user_post.posts.postssmilarprojects_set.count(),
                "answer_count": selected_user_post.usersanswers_set.count(),
                "profile_position_title": position_title,
                "profile_username": profile_username,
                "post_content": selected_user_post.posts.content_text,
                "created_date": timesince(selected_user_post.posts.created_date),
                "profile_image": profile_image,
                "post_id": selected_user_post.id,
                "is_edit": is_edit,
            }

            return HttpResponse(json.dumps(context), content_type="applicaton/json")

@never_cache
@csrf_protect
def write_answer_mainpost(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            users_post_id = request.POST.get("post")
            answer_text = request.POST.get("answer")

            if len(answer_text) > 0:

                users_post = UsersPosts.select_this(users_post_id)
                new_answer = Answers.create_answer(answer_text)
                new_users_answers = UsersAnswers.create(new_answer, users_post, users_fullprofiles)
                NotificationsAnswers.create_notification(new_users_answers, users_post.users_fullprofiles)
                # selected_post_answer=UsersAnswers.select_post_answer(users_post,users_fullprofiles)

                if "entrepreneur" in new_users_answers.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title="Girişimci"
                    else:
                        position_title=new_users_answers.users_fullprofiles.users.member_type
                else:
                    if len(new_users_answers.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = new_users_answers.users_fullprofiles.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(new_users_answers.users_fullprofiles).industries.description
                        # position_title = new_users_answers.users_fullprofiles.usersındustries_set.get().industries.description

                if not "default" in new_users_answers.users_fullprofiles.full_profile.picture_url.name:
                    profile_image = new_users_answers.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_image = new_users_answers.users_fullprofiles.full_profile.linkedin_picture_url

                context_answer = {
                    "profile_image": profile_image,
                    "position_title": position_title,
                    "created_date": timesince(new_users_answers.answers.created_date),
                    "profile_username":get_person_profile_name(users_fullprofiles.id,new_users_answers.users_fullprofiles.id),
                    "answer_content": new_users_answers.answers.answer_text,
                    "like_count": new_users_answers.answers.usersanswerslike_set.count(),
                    "answer_count": new_users_answers.answers.usersanswersanswers_set.count(),
                    "users_answers_id": new_users_answers.id,
                }



                context = {
                    "message_type": "success",
                    "message": ugettext("Your answer is sent."),
                    "user_answer": context_answer,
                }

                # requests.post("https://onesignal.com/api/v1/notifications",headers={"Authorization": "Basic OGY3NDZmNDEtMzgzZS00OTI2LTgwNzUtNzE0YThkZDg2ZTEx"},json=json.dumps(data))


                notification_message_tr= get_profile_name_str(users_fullprofiles.full_profile.last_name,users_fullprofiles.full_profile.first_name) + " takip ettiğiniz bir fikre cevap yazdı."

                notification_message_en = get_profile_name_str(users_fullprofiles.full_profile.last_name,users_fullprofiles.full_profile.first_name) + " wrote an answer which an idea you followed."

                payload = {
                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                    "include_player_ids": get_post_answers_users_list(users_post_id, users_fullprofiles.id),
                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                    "url":"/dashboard/ideas/detail/" + users_post_id + "/"
                }

                requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,data=json.dumps(payload))


                return HttpResponse(json.dumps(context), content_type="application/json")
            else:
                context = {
                    "message_type": "warning",
                    "message": ugettext("Your answer is not sent."),
                }

                return HttpResponse(json.dumps(context), content_type="application/json")


    else:
        context = {
            "message_type": "error",
            "message": ugettext("Your session seems to be ended. You should login for send answer."),

        }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def get_answer(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            usersanswers_id = request.GET.get("id")
            users_answers = UsersAnswers.select_answer(usersanswers_id)
            NotificationsAnswers.read_this_by_usersanswers(UsersAnswers.select_answer(usersanswers_id))
            # profile_name = get_profile_name_str(users_answers.users_fullprofiles.full_profile.last_name,
            #                                     users_answers.users_fullprofiles.full_profile.first_name)
            profile_name=get_person_profile_name(users_fullprofiles.id,users_answers.users_fullprofiles.id)

            if not "default" in users_answers.users_fullprofiles.full_profile.picture_url.name:
                profile_image = users_answers.users_fullprofiles.full_profile.picture_url.url
            else:
                profile_image = users_answers.users_fullprofiles.full_profile.linkedin_picture_url

            context = {
                "profile_name": profile_name,
                "created_date": timesince(users_answers.answers.created_date),
                "answer_content": users_answers.answers.answer_text,
                "like_count": users_answers.answers.usersanswerslike_set.filter(is_like=True).count(),
                "answer_count": 0,
                "profile_image": profile_image,
            }
            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def write_quick_answer(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_answers_id = request.POST.get("answer")
            answer_text = request.POST.get("answer-content")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            if len(answer_text) > 0:
                selected_ans_ans = UsersAnswers.select_answer(users_answers_id)
                new_answer_to_answer = Answers.create_answer(answer_text)

                users_answers = UsersAnswers.create(new_answer_to_answer, selected_ans_ans.users_posts,
                                                    users_fullprofiles)
                UsersAnswersAnswers.create_ans_ans(new_answer_to_answer, selected_ans_ans, users_fullprofiles)
                NotificationsAnswers.create_notification(users_answers, selected_ans_ans.users_fullprofiles)

                context = {
                    "message_type": "success",
                    "message": ugettext("Your answer is sent."),
                }

                notification_message_tr = get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                            users_fullprofiles.full_profile.first_name) + " takip ettiğiniz bir fikre cevap yazdı."

                notification_message_en = get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                            users_fullprofiles.full_profile.first_name) + " wrote an answer which an idea you followed."

                payload = {
                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                    "include_player_ids": get_post_answers_users_list(selected_ans_ans.users_posts.id, users_fullprofiles.id),
                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                    "url": "/dashboard/ideas/detail/" + str(selected_ans_ans.users_posts.id) + "/"
                }

                requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,data=json.dumps(payload))

            else:
                context = {
                    "message_type": "warning",
                    "message": ugettext("Your answer is not sent."),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def writer_answer_answer(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_answers_id = request.POST.get("answer")  # users_answers_id : 1 - 21
            answer_text = request.POST.get("answer-content")
            if len(answer_text) > 0:
                users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
                selected_ans_ans = UsersAnswers.select_answer(users_answers_id)
                new_answer_to_answer = Answers.create_answer(answer_text)

                # burda bunu yapmamızın sebebi :> Eğer cevaba karşılık verilen cevabada bir cevap yazılırsa
                users_answers = UsersAnswers.create(new_answer_to_answer, selected_ans_ans.users_posts,
                                                    users_fullprofiles)

                NotificationsAnswers.create_notification(users_answers, users_answers.users_fullprofiles)

                created_new_answer = UsersAnswersAnswers.create_ans_ans(new_answer_to_answer, selected_ans_ans,
                                                                        users_fullprofiles)

                if "entrepreneur" in created_new_answer.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title="Girişimci"
                    else:
                        position_title=created_new_answer.users_fullprofiles.users.member_type
                else:
                    if len(created_new_answer.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = created_new_answer.users_fullprofiles.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(created_new_answer.users_fullprofiles).industries.description
                        # position_title = created_new_answer.users_fullprofiles.usersındustries_set.get().industries.description

                if not "default" in created_new_answer.users_fullprofiles.full_profile.picture_url.name:
                    profile_image = created_new_answer.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_image = created_new_answer.users_fullprofiles.full_profile.linkedin_picture_url

                context_answer = {
                    "profile_image": profile_image,
                    "position_title": position_title,
                    "created_date": timesince(created_new_answer.answers.created_date),
                    # "profile_username": get_profile_name_str(
                    #     created_new_answer.users_fullprofiles.full_profile.last_name,
                    #     created_new_answer.users_fullprofiles.full_profile.first_name),
                    "profile_username":get_person_profile_name(users_fullprofiles.id,created_new_answer.users_fullprofiles.id),
                    "answer_content": created_new_answer.answers.answer_text,
                    "like_count": created_new_answer.answers.usersanswerslike_set.count(),
                    "answer_count": created_new_answer.answers.usersanswersanswers_set.count(),
                    "selected_answer_count": selected_ans_ans.usersanswersanswers_set.count(),
                    # bu kısım cevap attığımız ana cevaba yazılacak
                    "selected_answer_id": selected_ans_ans.id,
                    "users_answers_id": users_answers.id,
                }

                notification_message_tr = get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                               users_fullprofiles.full_profile.first_name) + " takip ettiğiniz bir fikre cevap yazdı."

                notification_message_en = get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                               users_fullprofiles.full_profile.first_name) + " wrote an answer which an idea you followed."

                payload = {
                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                    "include_player_ids": get_post_answers_users_list(selected_ans_ans.users_posts.id,
                                                                      users_fullprofiles.id),
                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                    "url": "/dashboard/ideas/detail/" + str(selected_ans_ans.users_posts.id) + "/"
                }

                requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,
                              data=json.dumps(payload))

                context = {
                    "message_type": "success",
                    "message": ugettext("Your answer is sent."),
                    "user_answer": context_answer,
                }



                return HttpResponse(json.dumps(context), content_type="application/json")

            else:
                context = {
                    "message_type": "warning",
                    "message": ugettext("Your answer is not sent."),
                }

                return HttpResponse(json.dumps(context), content_type="application/json")

    else:
        context = {
            "message_type": "error",
            "message": ugettext("Your session seems to be ended. You should login for send answer."),
        }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def get_post_answers(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            context_ans = []
            post_id = request.POST.get("post")
            selected_users_post = UsersPosts.select_this(post_id)
            main_answers = UsersAnswers.answers_by_post(selected_users_post)
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            if main_answers.count() > 0:
                for ans in main_answers[:5]:
                    context_ans_ans = []
                    if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 5:
                        sub_has_next = True
                    else:
                        sub_has_next = False

                    if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 0:

                        # selected_ans_answers = UsersAnswersAnswers.answers_by_answer(ans)[:5]
                        selected_ans_answers = ans.usersanswersanswers_set.filter(is_active=True, is_block=False).all().order_by("id")[:5]
                        # print(ans.usersanswersanswers_set.count())
                        for ans_ans in selected_ans_answers:
                            if ans_ans.users_fullprofiles.id is users_fullprofiles.id:
                                is_edit = True
                            else:
                                is_edit = False

                            if "entrepreneur" in ans_ans.users_fullprofiles.users.member_type:
                                if request.LANGUAGE_CODE == "tr":
                                    position_title="Girişimci"
                                else:
                                    position_title=ans_ans.users_fullprofiles.users.member_type
                            else:
                                if len(ans_ans.users_fullprofiles.full_profile.position_title) > 0:
                                    position_title = ans_ans.users_fullprofiles.full_profile.position_title
                                else:
                                    position_title=UsersIndustries.select(ans_ans.users_fullprofiles).industries.description
                                    # position_title = ans_ans.users_fullprofiles.usersındustries_set.get().industries.description

                            if not "default" in ans_ans.users_fullprofiles.full_profile.picture_url.name:
                                profile_image_sub_sub = ans_ans.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_image_sub_sub = ans_ans.users_fullprofiles.full_profile.linkedin_picture_url

                            item_ans_ans = {
                                "profile_image": profile_image_sub_sub,
                                "position_title": position_title,
                                "created_date": timesince(ans_ans.answers.created_date),
                                "like_count": ans_ans.answers.usersanswerslike_set.count(),
                                "answers_count": ans.answers.usersanswersanswers_set.count(),
                                # "profile_name": get_profile_name_str(ans_ans.users_fullprofiles.full_profile.last_name,ans_ans.users_fullprofiles.full_profile.first_name),
                                "profile_name":get_person_profile_name(users_fullprofiles.id,ans_ans.users_fullprofiles.id),
                                "sub_answer_id": ans.id,
                                "answer_content": ans_ans.answers.answer_text,
                                "profile_id": ans_ans.users_fullprofiles.id,
                                "is_edit": is_edit,
                                "ans_ans_id": ans_ans.id,
                            }
                            context_ans_ans.append(item_ans_ans)

                    if "entrepreneur" in ans.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title="Girişimci"
                        else:
                            position_title=ans.users_fullprofiles.users.member_type
                    else:
                        if len(ans.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = ans.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(ans.users_fullprofiles).industries.description
                            # position_title = ans.users_fullprofiles.usersındustries_set.get().industries.description

                    if not "default" in ans.users_fullprofiles.full_profile.picture_url.name:
                        profile_image = ans.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_image = ans.users_fullprofiles.full_profile.linkedin_picture_url

                    if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 0:

                        if ans.users_fullprofiles.id is users_fullprofiles.id:
                            is_edit = True
                        else:
                            is_edit = False

                        item_ans = {
                            "profile_image": profile_image,
                            "position_title": position_title,
                            # "profile_name": get_profile_name_str(ans.users_fullprofiles.full_profile.last_name,
                            #                                      ans.users_fullprofiles.full_profile.first_name),
                            "profile_name":get_person_profile_name(users_fullprofiles.id,ans.users_fullprofiles.id),
                            "like_count": ans.answers.usersanswerslike_set.count(),
                            "answers_count": ans.usersanswersanswers_set.count(),
                            "answer_content": ans.answers.answer_text,
                            "answer_id": ans.id,
                            "created_date": timesince(ans.answers.created_date),
                            "sub_answers": context_ans_ans,
                            "sub_has_next": sub_has_next,
                            "profile_id": ans.users_fullprofiles.id,
                            "is_edit": is_edit,
                        }
                        context_ans.append(item_ans)
                    else:
                        context_ans_ans = []

                        if ans.usersanswersanswers_set.filter(is_active=True,is_block=False).count() == 0 and ans.answers.usersanswersanswers_set.count() == 0:
                            if ans.users_fullprofiles.id is users_fullprofiles.id:
                                is_edit = True
                            else:
                                is_edit = False

                            item_ans = {
                                "profile_image": profile_image,
                                "position_title": position_title,
                                "profile_name":get_person_profile_name(users_fullprofiles.id,ans.users_fullprofiles.id),
                                "like_count": ans.answers.usersanswerslike_set.count(),
                                "answers_count": ans.usersanswersanswers_set.count(),
                                "answer_content": ans.answers.answer_text,
                                "answer_id": ans.id,
                                "created_date": timesince(ans.answers.created_date),
                                "sub_answers": context_ans_ans,
                                "sub_has_next": sub_has_next,
                                "profile_id": ans.users_fullprofiles.id,
                                "is_edit": is_edit,
                            }

                            context_ans.append(item_ans)


                if UsersAnswers.main_answers_hasNext(selected_users_post) > 5:
                    has_next = True
                else:
                    has_next=False

                # if len(main_answers) > 5:
                #     has_next = True
                # else:
                #     has_next = False

                context = {
                    "message_type": "success",
                    "message": ugettext("Success"),
                    "main_answers": context_ans,
                    "has_next": has_next,
                }
            else:
                context = {
                    "message_type": "warning",
                    "message": ugettext("There is no answer yet"),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        context = {
            "message_type": "error",
            "message": ugettext("Your session seems to be ended. You should login for send answer."),
        }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def load_more_sub_answers(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            answer_id = request.POST.get("answer")
            page = request.POST.get("page")
            context_ans_ans = []
            answers = UsersAnswersAnswers.answers_by_answer(UsersAnswers.select_answer(answer_id))

            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            for ans_ans in answers:

                if "entrepreneur" in ans_ans.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title="Girişimci"
                    else:
                        position_title=ans_ans.users_fullprofiles.users.member_type
                else:
                    if len(ans_ans.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = ans_ans.users_fullprofiles.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(ans_ans.users_fullprofiles).industries.description
                        # position_title = ans_ans.users_fullprofiles.usersındustries_set.get().industries.description

                if not "default" in ans_ans.users_fullprofiles.full_profile.picture_url.name:
                    profile_image = ans_ans.users_fullprofiles.full_profile.picture_url.url

                else:
                    profile_image = ans_ans.users_fullprofiles.full_profile.linkedin_picture_url

                if ans_ans.users_fullprofiles.id is users_fullprofiles.id:
                    is_edit = True
                else:
                    is_edit = False

                item_ans_ans = {
                    "profile_image": profile_image,
                    "position_title": position_title,
                    "created_date": timesince(ans_ans.answers.created_date),
                    "like_count": ans_ans.answers.usersanswerslike_set.count(),
                    "answers_count": ans_ans.answers.usersanswersanswers_set.count(),
                    "profile_name":get_person_profile_name(users_fullprofiles.id,ans_ans.users_fullprofiles.id),
                    # "sub_answer_id": ans_ans.answers.usersanswers_set.get().id,
                    "sub_answer_id": ans_ans.users_answers.id,
                    "answer_content": ans_ans.answers.answer_text,
                    "profile_id": ans_ans.users_fullprofiles.id,
                    "is_edit": is_edit,
                    "ans_ans_id": ans_ans.id,
                }
                context_ans_ans.append(item_ans_ans)

            paginator = Paginator(context_ans_ans, 5)



            try:
                subanswers_list = paginator.page(page)
            except PageNotAnInteger:
                subanswers_list = paginator.page(2)
            except EmptyPage:
                subanswers_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "sub_answers": list(subanswers_list),
                "has_next": subanswers_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
@csrf_protect
def load_more_main_answers(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            post_id = request.POST.get("post")
            page = request.POST.get("page")
            context_ans = []
            selected_users_post = UsersPosts.select_this(post_id)
            main_answers = UsersAnswers.answers_by_post(selected_users_post)
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            for ans in main_answers:
                context_ans_ans = []
                if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 5:
                    sub_has_next = True
                else:
                    sub_has_next = False

                if ans.usersanswersanswers_set.count() > 0:

                    # selected_ans_answers = UsersAnswersAnswers.answers_by_answer(ans)[:5]
                    selected_ans_answers = ans.usersanswersanswers_set.filter(is_active=True, is_block=False).all()[:5]
                    # print(ans.usersanswersanswers_set.count())
                    for ans_ans in selected_ans_answers:

                        if ans_ans.users_fullprofiles.id is users_fullprofiles.id:
                            is_edit = True
                        else:
                            is_edit = False

                        if "entrepreneur" in ans_ans.users_fullprofiles.users.member_type:
                            if request.LANGUAGE_CODE == "tr":
                                position_title="Girişimci"
                            else:
                                position_title = ans_ans.users_fullprofiles.users.member_type
                        else:
                            if len(ans_ans.users_fullprofiles.full_profile.position_title) > 0:
                                position_title = ans_ans.users_fullprofiles.full_profile.position_title
                            else:
                                position_title=UsersIndustries.select(ans_ans.users_fullprofiles).industries.description
                                # position_title = ans_ans.users_fullprofiles.usersındustries_set.get().industries.description

                        if not "default" in ans_ans.users_fullprofiles.full_profile.picture_url.name:
                            profile_image = ans_ans.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_image = ans_ans.users_fullprofiles.full_profile.linkedin_picture_url

                        item_ans_ans = {
                            "profile_image": profile_image,
                            "position_title": position_title,
                            "created_date": timesince(ans_ans.answers.created_date),
                            "like_count": ans_ans.answers.usersanswerslike_set.count(),
                            "answers_count": ans.answers.usersanswersanswers_set.count(),
                            # "profile_name": get_profile_name_str(ans_ans.users_fullprofiles.full_profile.last_name,
                            #                                      ans_ans.users_fullprofiles.full_profile.first_name),
                            "profile_name":get_person_profile_name(users_fullprofiles.id,ans_ans.users_fullprofiles.id),
                            "sub_answer_id": ans.id,
                            "answer_content": ans_ans.answers.answer_text,
                            "profile_id": ans_ans.users_fullprofiles.id,
                            "is_edit": is_edit,
                            "ans_ans_id": ans_ans.id,
                        }
                        context_ans_ans.append(item_ans_ans)


                if "entrepreneur" in ans.users_fullprofiles.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title="Girişimci"
                    else:
                        position_title=ans.users_fullprofiles.users.member_type
                else:
                    if len(ans.users_fullprofiles.full_profile.position_title) > 0:
                        position_title = ans.users_fullprofiles.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(ans.users_fullprofiles).industries.description
                        # position_title = ans.users_fullprofiles.usersındustries_set.get().industries.description

                if not "default" in ans.users_fullprofiles.full_profile.picture_url.name:
                    profile_image = ans.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_image = ans.users_fullprofiles.full_profile.linkedin_picture_url

                if ans.usersanswersanswers_set.filter(is_active=True, is_block=False).count() > 0:
                    if ans.users_fullprofiles.id is users_fullprofiles.id:
                        is_edit = True
                    else:
                        is_edit = False

                    item_ans = {
                        "profile_image": profile_image,
                        "position_title": position_title,
                        "profile_name":get_person_profile_name(users_fullprofiles.id,ans.users_fullprofiles.id),
                        "like_count": ans.answers.usersanswerslike_set.count(),
                        "answers_count": ans.usersanswersanswers_set.count(),
                        "answer_content": ans.answers.answer_text,
                        "answer_id": ans.id,
                        "created_date": timesince(ans.answers.created_date),
                        "sub_answers": context_ans_ans,
                        "sub_has_next": sub_has_next,
                        "profile_id": ans.users_fullprofiles.id,
                        "is_edit": is_edit
                    }
                    context_ans.append(item_ans)
                else:
                    context_ans_ans = []
                    if ans.usersanswersanswers_set.filter(is_active=True,is_block=False).count() == 0 and ans.answers.usersanswersanswers_set.count() == 0:

                        if ans.users_fullprofiles.id is users_fullprofiles.id:
                            is_edit = True
                        else:
                            is_edit = False

                        item_ans = {
                            "profile_image": profile_image,
                            "position_title": position_title,
                            "profile_name":get_person_profile_name(users_fullprofiles.id,ans.users_fullprofiles.id),
                            "like_count": ans.answers.usersanswerslike_set.count(),
                            "answers_count": ans.usersanswersanswers_set.count(),
                            "answer_content": ans.answers.answer_text,
                            "answer_id": ans.id,
                            "created_date": timesince(ans.answers.created_date),
                            "sub_answers": context_ans_ans,
                            "sub_has_next": sub_has_next,
                            "profile_id": ans.users_fullprofiles.id,
                            "is_edit": is_edit,
                        }

                        context_ans.append(item_ans)

            paginator = Paginator(context_ans, 5)

            try:
                mainanswers_list = paginator.page(page)
            except PageNotAnInteger:
                mainanswers_list = paginator.page(2)
            except EmptyPage:
                mainanswers_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)
            # print(page)
            output_data = {
                "main_answers": list(mainanswers_list),
                "has_next": mainanswers_list.has_next(),
                "page": page,
                "message_type": "success",
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")
    else:
        context = {
            "message_type": "error",
            "message": ugettext("Your session seems to be ended. You should login for send answer."),
        }
        return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(60 * 10)
def user_infos(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            post_id = request.GET.get("post_id")
            users_posts = UsersPosts.select_this(post_id)

            if "entrepreneur" in users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    current_user_position_title="Girişimci"
                else:
                    current_user_position_title=users_fullprofiles.users.member_type
            else:
                if len(users_fullprofiles.full_profile.position_title) > 0:
                    current_user_position_title = users_fullprofiles.full_profile.position_title
                else:
                    current_user_position_title=UsersIndustries.select(users_fullprofiles).industries.description
                    # current_user_position_title = users_fullprofiles.usersındustries_set.get().industries.description

            if not "default" in users_fullprofiles.full_profile.picture_url.name:
                current_user_profile_image = users_fullprofiles.full_profile.picture_url.url
            else:
                current_user_profile_image = users_fullprofiles.full_profile.linkedin_picture_url

            if "entrepreneur" in users_posts.users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    requested_user_position_title = "Girişimci"
                else:
                    requested_user_position_title = users_posts.users_fullprofiles.users.member_type
            else:
                if len(users_posts.users_fullprofiles.full_profile.position_title) > 0:
                    requested_user_position_title = users_posts.users_fullprofiles.full_profile.position_title
                else:
                    requested_user_position_title=UsersIndustries.select(users_posts.users_fullprofiles).industries.description
                    # requested_user_position_title = users_posts.users_fullprofiles.usersındustries_set.get().industries.description

            if not "default" in users_posts.users_fullprofiles.full_profile.picture_url.name:
                requested_user_profile_image = users_posts.users_fullprofiles.full_profile.picture_url.url
            else:
                requested_user_profile_image = users_posts.users_fullprofiles.full_profile.linkedin_picture_url

            context = {
                "current_user_profile_img": current_user_profile_image,
                "current_user_position_title": current_user_position_title,
                "current_user_profilename": get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                                 users_fullprofiles.full_profile.first_name),
                # "requested_user_profilename": get_profile_name_str(
                #     users_posts.users_fullprofiles.full_profile.last_name,
                #     users_posts.users_fullprofiles.full_profile.first_name),
                "requested_user_profilename":get_person_profile_name(users_fullprofiles.id,users_posts.users_fullprofiles.id),
                "requested_user_profile_img": requested_user_profile_image,
                "requested_user_position_title": requested_user_position_title,
                "message_type": "success",
            }

            return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        context = {
            "message_type": "error",
        }

        return HttpResponse(json.dumps(context), content_type="application/json")


# burda kaldım
@never_cache
@csrf_protect
def add_smilar_project(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            post_id = request.POST.get("post")
            link_text = request.POST.get("link_text")
            describe_text = request.POST.get("describe_text")

            if len(describe_text) >= 20:

                if UsersPosts.is_there(post_id):
                    if UsersPosts.select_this(post_id).users_fullprofiles.id == request.session["user_fullprofile"]:
                        context = {
                            "message_type": "error",
                            "message": ugettext("You can't send yourself similar project info."),
                        }

                    else:
                        selected_post = UsersPosts.select_this(post_id).posts
                        smilar_project = PostsSmilarprojects.create(selected_post, link_text, describe_text)
                        created_userssimilar = UsersPostsSmilarprojects.create(users_fullprofiles, smilar_project)
                        NotificationsSmilarProjects.create_notification(created_userssimilar)

                        context = {
                            "message_type": "success",
                            "message": ugettext("Successfully sent. Thanks"),
                            "smilar_project_count": selected_post.postssmilarprojects_set.count(),
                        }

                else:
                    context = {
                        "message_type": "error",
                        "message": ugettext(
                            "An error occured during the sending process. the idea may have been removed."),
                    }
            else:
                context = {
                    "message_type": "error",
                    "message": ugettext("You've to type more than 20 characters in the description field.")
                }

            return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        context = {
            "message_type": "warning",
            "message": ugettext("Your session seems to be ended. You should login for send answer."),
        }
        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def save_project_hours(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            ph_id = request.GET.get("id")
            users_post_id = request.GET.get("post_id")
            users_posts = UsersPosts.select_this(users_post_id)

            if users_posts.users_fullprofiles.id == request.session["user_fullprofile"]:
                context = {
                    "message_type": "error",
                    "message": ugettext("You can't send team request to yourself."),

                }
            else:
                if UsersPostsTeamrequests.isThere_request(users_fullprofiles, users_posts):
                    # yapılan bir teamrequest var ise
                    project_hours = ProjectHours.select_project_hours(ph_id)
                    selected_upt = UsersPostsTeamrequests.select_this(users_fullprofiles, users_posts)

                    if selected_upt.teamrequest.users_request_projecthours is None:
                        users_request_projecthours = UsersRequestProjectHours.create(project_hours, 0)
                        Teamrequests.create(0, users_request_projecthours, selected_upt.teamrequest.id)
                    else:
                        users_request_projecthours = UsersRequestProjectHours.create(project_hours,
                                                                                     selected_upt.teamrequest.users_request_projecthours.id)

                else:
                    # project_hours = UsersProjectHours.select_user_projecthours(users_fullprofiles)
                    project_hours = ProjectHours.select_project_hours(ph_id)
                    users_request_projecthours = UsersRequestProjectHours.create(project_hours, 0)
                    teamrequests = Teamrequests.create(0, users_request_projecthours, 0)
                    UsersPostsTeamrequests.create(users_fullprofiles, users_posts, teamrequests)

                if request.LANGUAGE_CODE == "tr":
                    selected_project_hours = clean_hours_title(users_request_projecthours.project_hours.title, "saat")
                else:
                    selected_project_hours = clean_hours_title(users_request_projecthours.project_hours.title_eng,"hours")

                context = {
                    "message_type": "success",
                    "selected_project_hours": selected_project_hours,
                    "data_project_hours": users_request_projecthours.project_hours.id,
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def save_support_type(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            st_id = request.GET.get("id")
            post_id = request.GET.get("post_id")
            users_posts = UsersPosts.select_this(post_id)

            if users_posts.users_fullprofiles.id == request.session["user_fullprofile"]:
                context = {
                    "message_type": "error",
                    "message": ugettext("You can't send team request to yourself."),
                }
            else:

                if UsersPostsTeamrequests.isThere_request(users_fullprofiles, users_posts):
                    if "paid-support" in Investments.select_invest_type(st_id).title_code:
                        if UsersHourCosts._isUsersHourCostsClear(users_fullprofiles):
                            investments = Investments.select_invest_type(st_id)
                        else:
                            investments = Investments.select_by_code("equal-paid")
                    else:
                        investments = Investments.select_invest_type(st_id)

                    selected_upt = UsersPostsTeamrequests.select_this(users_fullprofiles, users_posts)
                    # daha önceden yaptığım bir invest var mı

                    if selected_upt.teamrequest.users_request_investments is None:
                        users_request_investments = UsersRequestInvestments.create(investments, 0)
                        Teamrequests.create(users_request_investments, 0, selected_upt.teamrequest.id)
                    else:
                        users_request_investments = UsersRequestInvestments.create(investments,
                                                                                   selected_upt.teamrequest.users_request_investments.id)

                else:
                    # investments=Investments.select_invest_type(st_id)
                    if "paid-support" in Investments.select_invest_type(st_id).title_code:
                        if UsersHourCosts._isUsersHourCostsClear(users_fullprofiles):
                            investments = Investments.select_invest_type(st_id)
                        else:
                            investments = Investments.select_by_code("equal-paid")
                    else:
                        investments = Investments.select_invest_type(st_id)

                    users_request_investments = UsersRequestInvestments.create(investments, 0)
                    teamrequest = Teamrequests.create(users_request_investments, 0, 0)
                    UsersPostsTeamrequests.create(users_fullprofiles, users_posts, teamrequest)

                if request.LANGUAGE_CODE == "tr":
                    selected_investments = users_request_investments.investments.title
                else:
                    selected_investments = users_request_investments.investments.title_eng

                context = {
                    "message_type": "success",
                    "investment": selected_investments,
                    "data_investment": users_request_investments.investments.id,
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def users_project_supports(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_post_id = request.GET.get("post")
            users_posts = UsersPosts.select_this(users_post_id)
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            if UsersPostsTeamrequests.isThere_request(users_fullprofiles, users_posts):
                selected_teamrequest = UsersPostsTeamrequests.select_this(users_fullprofiles, users_posts)

                if selected_teamrequest.teamrequest.users_request_investments is None:
                    users_investments = UsersInvestments.select_users_investment(users_fullprofiles)
                else:
                    users_investments = selected_teamrequest.teamrequest.users_request_investments

                if selected_teamrequest.teamrequest.users_request_projecthours is None:
                    users_project_hours = UsersProjectHours.select_user_projecthours(users_fullprofiles)
                else:
                    users_project_hours = selected_teamrequest.teamrequest.users_request_projecthours

                if request.LANGUAGE_CODE == "tr":
                    investment = users_investments.investments.title
                    project_hours = clean_hours_title(users_project_hours.project_hours.title, "saat")
                else:
                    investment = users_investments.investments.title_eng
                    project_hours = clean_hours_title(users_project_hours.project_hours.title_eng, "hours")

                context = {
                    "message_type": "success",
                    "selected_investment": investment,
                    "selected_project_hours": project_hours,
                    "data_project_hours": users_project_hours.project_hours.id,
                    "data_investments": users_investments.investments.id,
                }
            else:
                users_investments = UsersInvestments.select_users_investment(users_fullprofiles)
                users_project_hours = UsersProjectHours.select_user_projecthours(users_fullprofiles)

                if request.LANGUAGE_CODE == "tr":
                    investment = users_investments.investments.title
                    selected_project_hours = clean_hours_title(users_project_hours.project_hours.title, "saat")
                else:
                    investment = users_investments.investments.title_eng
                    selected_project_hours = clean_hours_title(users_project_hours.project_hours.title_eng, "hours")

                context = {
                    "message_type": "success",
                    "selected_investment": investment,
                    "selected_project_hours": selected_project_hours,
                    "data_project_hours": users_project_hours.project_hours.id,
                    "data_investments": users_investments.investments.id,
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_teamrequest(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_post_id = request.POST.get("post")

            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_posts = UsersPosts.select_this(users_post_id)

            if users_posts.users_fullprofiles.id == request.session["user_fullprofile"]:
                context = {
                    "message_type": "error",
                    "message": ugettext("You can't send teamrequest to yourself."),
                }
            else:

                if not UsersPostsTeamrequests.isThere_request(users_fullprofiles, users_posts):
                    request_ph = UsersRequestProjectHours.create(
                        UsersProjectHours.select_user_projecthours(users_fullprofiles).project_hours, 0)
                    request_pi = UsersRequestInvestments.create(
                        UsersInvestments.select_users_investment(users_fullprofiles).investments, 0)
                    created_teamrequest = Teamrequests.create(request_pi, request_ph, 0)
                    UsersPostsTeamrequests.create(users_fullprofiles, users_posts,
                                                  created_teamrequest).confirm_teamrequest(users_fullprofiles,
                                                                                           users_posts)

                    context = {
                        "message_type": "success",
                        "message": ugettext("Your team request has been successfully sent."),
                        "teamrequest_count": users_posts.userspoststeamrequests_set.count(),
                    }

                else:
                    # eğer request var fakat investment or hour belirtilmemiş ise ya da sadece biri belirtilmişse
                    selected_post_teamrequest = UsersPostsTeamrequests.select_this(users_fullprofiles, users_posts)

                    if selected_post_teamrequest.teamrequest.users_request_investments is None:
                        request_investments = UsersRequestInvestments.create(UsersInvestments.select_users_investment(
                            selected_post_teamrequest.users_fullprofiles).investments, 0)
                        Teamrequests.create(request_investments, 0, selected_post_teamrequest.teamrequest.id)

                    if selected_post_teamrequest.teamrequest.users_request_projecthours is None:
                        request_projecthors = UsersRequestProjectHours.create(
                            UsersProjectHours.select_user_projecthours(
                                selected_post_teamrequest.users_fullprofiles).project_hours, 0)
                        Teamrequests.create(0, request_projecthors, selected_post_teamrequest.teamrequest.id)

                    UsersPostsTeamrequests.confirm_teamrequest(users_fullprofiles, users_posts)
                    context = {
                        "message_type": "success",
                        "message": ugettext("Your team request has been successfully sent."),
                        "teamrequest_count": users_posts.userspoststeamrequests_set.count(),
                    }

                notification_message_tr = get_profile_name_str(users_fullprofiles.full_profile.last_name,users_fullprofiles.full_profile.first_name) + " fikrinize yeni bir takım isteği gönderdi."
                notification_message_en = get_profile_name_str(users_fullprofiles.full_profile.last_name,
users_fullprofiles.full_profile.first_name) + " sent a team request to your idea."

                payload = {
                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                    "include_player_ids": send_signals_to_projects_owner(
                        users_posts.users_fullprofiles.id),
                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                    "url": "/dashboard/myideas/detail/" + str(users_posts.id) + "/"
                }

                requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,
                              data=json.dumps(payload))

            return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        context = {
            "message_type": "warning",
            "message": ugettext("Your session seems to be ended. You should login for send answer."),
        }
        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def share_idea(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            ideaTxt = request.POST.get("idea")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            created_post = Posts.create_post(ideaTxt)
            created_usersposts=UsersPosts.create_users_posts(users_fullprofiles, created_post)
            UsersMembershipTermsType.create(users_fullprofiles,"open-new-post",users_fullprofiles.users.users_membershiptypes.name_code)

            context = {
                "message_type": "success",
                "message": ugettext("Your idea was shared with crowd. Now you should be patient."),
                "redirect_url": "/dasboard/ideas/",
            }

            notification_message_tr = get_profile_name_str(created_usersposts.users_fullprofiles.full_profile.last_name,
                                                           created_usersposts.users_fullprofiles.full_profile.first_name) + " yeni bir fikir paylaştı."

            notification_message_en = get_profile_name_str(created_usersposts.users_fullprofiles.full_profile.last_name,
                                                           created_usersposts.users_fullprofiles.full_profile.first_name) + " wrote an idea."

            payload = {
                "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                "include_player_ids": new_signals_users_list(users_fullprofiles.id),
                "contents": {"en": notification_message_en, "tr": notification_message_tr},
                "url": "/dashboard/ideas/detail/" + str(created_usersposts.id) + "/"
            }

            requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,
                          data=json.dumps(payload))

            return HttpResponse(json.dumps(context), content_type="application/json")

    else:
        context = {
            "message_type": "warning",
            "message": ugettext("Your session seems to be ended. You should login for send answer."),
            "redirect_url": "/login/",
        }

        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def edit_get_post(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            post_id = request.GET.get("id")
            selected_post = UsersPosts.select_this(post_id)
            context = {
                "post_content": selected_post.posts.content_text,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def edit_get_subanswer(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            ans_ans_id = request.GET.get("id")
            selected_ans_ans = UsersAnswersAnswers.select_by_id(ans_ans_id)

            context = {
                "answer_text": selected_ans_ans.answers.answer_text,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def edit_save_subanswer(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            ans_ans_id = request.POST.get("id")
            answer_text = request.POST.get("content_text")
            try:
                selected_ans_ans = UsersAnswersAnswers.select_by_id(ans_ans_id)
                Answers.update_answer(selected_ans_ans.answers.id, answer_text)

                context = {
                    "message_type": "success",
                    "message": ugettext("You've successfully updated."),
                }
            except:
                context = {
                    "message_type": "error",
                    "message": ugettext("A problem was encountered during the update. Please, try again later."),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def edit_get_answer(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            answer_id = request.GET.get("id")
            selected_answer = Answers.select_answer(answer_id)

            context = {
                "answer_text": selected_answer.answer_text,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def edit_main_answer(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            answer_id = request.POST.get("id")
            context_text = request.POST.get("content_text")
            try:
                Answers.update_answer(answer_id, context_text)

                context = {
                    "message_type": "success",
                    "message": ugettext("You've successfully updated."),
                }
            except:
                context={
                    "message_type":"error",
                    "message": ugettext("A problem was encountered during the update. Please, try again later."),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def edit_main_post(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            post_id = request.POST.get("id")
            content_text = request.POST.get("content_text")
            try:
                selected_userspost = UsersPosts.select_this(post_id)
                Posts.update_post(content_text, selected_userspost.posts.id)
                context = {
                    "message_type": "success",
                    "message": ugettext("You've successfully updated."),
                }
            except:
                context = {
                    "message_type": "error",
                    "message": ugettext("A problem was encountered during the update. Please, try again later."),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")