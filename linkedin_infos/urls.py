from django.urls import path
from .views import *

app_name="linkedin-infos"

urlpatterns=[
    path('get-skills/',get_skills,name='skill-list'),
]