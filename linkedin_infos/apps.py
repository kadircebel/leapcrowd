from django.apps import AppConfig


class LinkedinInfosConfig(AppConfig):
    name = 'linkedin_infos'
