from django.db import models

# Create your models here.

class Certifications(models.Model):
    name = models.CharField(max_length=250, blank=True)
    authority = models.CharField(max_length=250, blank=True)
    number = models.IntegerField(blank=True)
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'certifications'

class CompanyInfos(models.Model):
    name=models.CharField(max_length=600, blank=True)
    title = models.CharField(max_length=250, blank=True)
    location_name = models.CharField(max_length=250, blank=True)
    start_date_month=models.CharField(max_length=2,blank=True)
    start_date_year=models.CharField(max_length=5,blank=True)
    end_date_month = models.CharField(max_length=2, blank=True)
    end_date_year = models.CharField(max_length=5, blank=True)
    is_current = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        managed = True
        db_table = 'company_infos'

    @classmethod
    def create(cls,name,title,location_name,start_month,start_year,is_current,end_month,end_year):
        company=cls(name=name,title=title,location_name=location_name,start_date_month=start_month,start_date_year=start_year,is_current=is_current,end_date_month=end_month,end_date_year=end_year)
        company.save()
        return company
    @classmethod
    def select(cls,id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def update(cls,name,title,location_name,start_month,start_year,is_current,end_month,end_year,id):
        selected=cls.objects.get(id=id)
        selected.name=name
        selected.title=title
        selected.location_name=location_name
        selected.start_date_month=start_month
        selected.start_date_year=start_year
        selected.is_current=is_current
        selected.end_date_month=end_month
        selected.end_date_year=end_year
        selected.save()
        return selected

class Courses(models.Model):
    name = models.CharField(max_length=200, blank=True)
    number = models.IntegerField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'courses'

class Educations(models.Model):
    scholl_name = models.CharField(max_length=200, blank=True)
    field_of_study = models.CharField(max_length=500, blank=True)
    start_date = models.DateField(blank=True)
    degree = models.CharField(max_length=10, blank=True)
    activities = models.TextField(blank=True)
    notes = models.TextField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'educations'

class HonorsAwards(models.Model):
    title = models.CharField(max_length=250, blank=True)
    describtion = models.TextField(blank=True)
    hw_date = models.DateField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'honors_awards'

class Industries(models.Model):
    code = models.CharField(max_length=250, blank=True)
    groups = models.CharField(max_length=250, blank=True)
    description = models.CharField(max_length=350, blank=True)
    id = models.BigAutoField(primary_key=True)

    def __str__(self):
        return '{}'.format(self.description)

    class Meta:
        managed = True
        db_table = 'industries'

    @classmethod
    def create(cls,code,groups,description):
        ind=cls(code=code,groups=groups,description=description)
        ind.save()
        return ind


class LanguagesKnown(models.Model):
    language_name = models.CharField(max_length=150, blank=True)
    proficiency_code = models.CharField(max_length=150, blank=True)
    proficiency_name = models.CharField(max_length=150, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'languages_known'

class LinkedinRecommendations(models.Model):
    recommendation_type = models.CharField(max_length=250, blank=True)
    recommendation_text = models.TextField(blank=True)
    recommender = models.CharField(max_length=150, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'linkedin_recommendations'

    @classmethod
    def create_signal_id(cls,recommendation_type,recommendation_text):
        new_record=cls(recommendation_type=recommendation_type,recommendation_text=recommendation_text)
        new_record.save()
        return new_record

class Skills(models.Model):
    title = models.CharField(max_length=150, blank=True)
    id = models.BigAutoField(primary_key=True)


    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        managed = True
        db_table = 'skills'

    @classmethod
    def create(cls,title):
        skill=cls(title=title)
        skill.save()
        return skill
    @classmethod
    def select(cls,id):
        return cls.objects.filter(id=id).get()

