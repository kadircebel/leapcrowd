from django.http import JsonResponse
from .models import Skills
import json
from django.views.decorators.cache import cache_page
# Create your views here.

@cache_page(60*60)
def get_skills(request):
    skill_list = []
    keyword=request.GET.get("keyword")
    finding=Skills.objects.filter(title__contains=keyword.lower()).all()[:10]
    for k in finding:
        s = {'id': k.id, 'title': k.title}
        skill_list.append(s)

    return JsonResponse(json.dumps(skill_list),safe=False)