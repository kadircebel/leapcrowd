from django.contrib import admin
from .models import Skills
# Register your models here.

class SkillsAdmin(admin.ModelAdmin):
    list_display = ["title"]

    class Meta:
        model = Skills

admin.site.register(Skills,SkillsAdmin)