from django.urls import path
from .views import *
from django.views.decorators.cache import cache_page

app_name="my-teams"

urlpatterns=[
    path('',index,name='index'),
    path('detail/<int:id>/',detail,name='detail'),
    path('loadmore-myteams/', loadmore_myteam_projects, name='loadmore_myteams'),
    path('loadmore-details/', loadmore_detail, name='loadmore_details'),
    path('loadmore-cofunded-projects/', co_funded_projects, name='co_funded_projects'),
    path('delete-member/', delete_member, name='delete_member'),
    path('selected-member/', selected_member, name='selected_member'),
    path('my-dm-requests/', myrequests, name='my_requests'),
    path('delete-dm/', delete_dm_request, name='delete_dm_request'),

]