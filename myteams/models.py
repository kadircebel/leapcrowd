from django.db import models

class TeamPages(models.Model):
    system_teampages = models.ForeignKey('mainadmins.SystemTeampages', models.DO_NOTHING, blank=True)
    users_teampages = models.ForeignKey('persons.UsersTeamPages', models.DO_NOTHING, blank=True)
    describe = models.TextField(blank=True)
    created_date = models.DateField(auto_created=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'team_pages'