from django.shortcuts import render, HttpResponse, redirect
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import ugettext
import json
from utils.views import get_fulllocation_string, get_profile_name_str, remove_html_tags, change_characters, clean_hours_title,get_person_profile_name,profile_points_css
from persons.models import UsersFullProfile, UsersProjects, UsersPointScores, UsersInvestments, UsersTeams, UsersFriend, UsersMembershipTermsType, UsersDmProjectsTeamrequests,UsersIndustries
from teams.models import Teams
from memberships.models import MembershipTermsType
from datetime import datetime
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

from django.views.decorators.cache import never_cache,cache_page

# Create your views here.
# @cache_page(60*5)
@never_cache
def index(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        users_projects = UsersProjects.list_all(users_fullprofiles)
        context_projects = []

        for prj in users_projects:
            if Teams.is_there_team_by_project(prj):
                context_members = []
                for member in prj.teams_set.get().usersteams_set.filter(is_active=True).all():
                    if not "default" in member.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = member.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = member.users_fullprofiles.full_profile.linkedin_picture_url

                    item_member = {
                        "profile_image": profile_img,
                    }

                    context_members.append(item_member)


            else:

                context_members = []

            item = {
                "project_image": prj.projects.general_infos.project_logo,
                "project_title": prj.projects.general_infos.title,
                "project_describe": remove_html_tags(prj.projects.general_infos.describe),
                "project_location": get_fulllocation_string(
                    prj.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                    prj.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + prj.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                "project_members": context_members,
                "project_id": prj.id,
            }
            context_projects.append(item)

        paginator = Paginator(context_projects, 5)

        page = 1

        try:
            projects_list = paginator.page(page)
        except PageNotAnInteger:
            projects_list = paginator.page(1)
        except EmptyPage:
            projects_list = paginator.page(paginator.num_pages)

        page = str(int(page) + 1)

        context = {
            "my_projects": list(projects_list),
            "has_next": projects_list.has_next(),
            "page": page,
            "page_title":ugettext("My teams"),
            "cofunded_count":str(UsersTeams.cofunded_projects_count(users_fullprofiles.id)),
        }

        return render(request, 'socialside/myteams/index.html', context)
    else:
        return redirect("/login/")

@never_cache
def loadmore_myteam_projects(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_projects = UsersProjects.list_all(users_fullprofiles)
            page = request.POST.get("page")

            context_projects = []

            for prj in users_projects:
                if Teams.is_there_team_by_project(prj):
                    context_members = []
                    for member in prj.teams_set.get().usersteams_set.all():
                        if not "default" in member.users_fullprofiles.full_profile.picture_url.name:
                            profile_img = member.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_img = member.users_fullprofiles.full_profile.linkedin_picture_url

                        item_member = {
                            "profile_image": profile_img,
                        }

                        context_members.append(item_member)
                else:

                    context_members = []

                if len(remove_html_tags(prj.projects.general_infos.describe)) > 140:
                    project_describe = str(remove_html_tags(prj.projects.general_infos.describe))[:140] + "..."
                else:
                    project_describe = remove_html_tags(prj.projects.general_infos.describe)

                item = {
                    "project_image": prj.projects.general_infos.project_logo.name,
                    "project_title": prj.projects.general_infos.title,
                    "project_describe": project_describe,
                    "project_location": get_fulllocation_string(
                        prj.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                        prj.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + prj.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                    "project_members": list(context_members),
                    "project_id": prj.id,
                }
                context_projects.append(item)

            paginator = Paginator(context_projects, 5)

            try:
                projects_list = paginator.page(page)
            except PageNotAnInteger:
                projects_list = paginator.page(1)
            except EmptyPage:
                projects_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "my_projects": list(projects_list),
                "has_next": projects_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def selected_member(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            user_id = request.POST.get("id")
            selected_user = UsersFullProfile.select_user(user_id)

            if not "default" in selected_user.full_profile.picture_url.name:
                profile_img = selected_user.full_profile.picture_url.url
            else:
                profile_img = selected_user.full_profile.linkedin_picture_url

            if "entrepreneur" in selected_user.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    position_title = "Girişimci"
                else:
                    position_title = selected_user.users.member_type
            else:
                if len(selected_user.full_profile.position_title) > 0:
                    position_title = selected_user.full_profile.position_title
                else:
                    position_title=UsersIndustries.select(selected_user).industries.description
                    # position_title = selected_user.usersındustries_set.get().industries.description

            context = {
                "profile_name": get_profile_name_str(selected_user.full_profile.last_name,
                                                     selected_user.full_profile.first_name),
                "profile_image": profile_img,
                "profile_location": get_fulllocation_string(
                    selected_user.full_profile.fullprofilelocation_set.get().location.district,
                    selected_user.full_profile.fullprofilelocation_set.get().location.city) + " - " + selected_user.full_profile.fullprofilelocation_set.get().location.country,
                "position_title": position_title,
                "profile_point": UsersPointScores.calculate_points(selected_user),
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def detail(request, id):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        context_team = []
        project_name=""

        if UsersProjects.is_blong_this_user(users_fullprofiles, id):
            is_owner=True
            users_project = UsersProjects.select_by_id(id, users_fullprofiles)
            project_name=users_project.projects.general_infos.title

            if not users_project.posts is None:
                total_requests=users_project.posts.usersposts_set.get().userspoststeamrequests_set.count() + users_project.usersprojectsteamrequests_set.count()

            else:
                total_requests = users_project.usersprojectsteamrequests_set.count()

            total_hours = 0
            total_paid_supports = 0

            if Teams.is_there_team_by_project(users_project):

                selected_team = Teams.select_team(users_project)

                for member in selected_team.usersteams_set.filter(is_active=True).all():

                    if users_project.users_fullprofiles.id == member.users_fullprofiles.id:
                        is_project_owner = True
                    else:
                        is_project_owner = False

                    if not "default" in member.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = member.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = member.users_fullprofiles.full_profile.linkedin_picture_url

                    profile_name = get_profile_name_str(member.users_fullprofiles.full_profile.last_name,
                                                        member.users_fullprofiles.full_profile.first_name)

                    if request.LANGUAGE_CODE == "tr":

                        if member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                            # eğer projeye request yaptıysam - projenin sahibi değilim demektir.

                            member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title

                            if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                total_paid_supports+=1

                                hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                            else:
                                # teklif verirken ücretli olarak belirtmemiş fakat normalde sisteme kendi ücretini
                                # nasıl belirlemiş - bunu almamızın sebebi girişimcinin ücret bazında ne kadar destek                                aldığını göstermemiz. Bu şekilde ben sana şunu yaptım - şu kadar para gerekiyor
                                # tartışmalarını sonlandırmak
                                if member.users_fullprofiles.usershourcosts_set.exists():
                                    hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code +" " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = " - "


                            if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                            else:
                                member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")

                            total_hours+=int(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)

                        # direk projeye request yapılmadıysa eğer => projenin bağlandığı post var mı
                        elif not member.teams.users_projects.posts is None:

                            if users_project.users_fullprofiles.id != member.users_fullprofiles.id and member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():

                                member_investments=member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title

                                if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    total_paid_supports+=1
                                    hour_cost=member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code  + " " + member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost =member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost=""

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(
                                        member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")

                                total_hours+=int(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)
                            else:
                                member_investments=UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title
                                if "paid-support" in UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title_code:
                                    total_paid_supports+=1
                                    hour_cost=users_project.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + users_project.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost=""

                                # proje sahibi olduğum için bunu konum ayrımı yapma
                                member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"saat")
                                total_hours+=int(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)

                        else:
                            # eğer projeye request yapmayıp - takımda isem takımın sahibiyim
                            # bu kısımda post tarafından da teklif yapmışmıyım ona bakmalıyım

                            member_investments = UsersInvestments.select_users_investment(
                                users_project.users_fullprofiles).investments.title

                            if "paid-support" in UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title_code:
                                total_paid_supports+=1
                                if users_project.users_fullprofiles.usershourcosts_set.exists():
                                    hour_cost =  users_project.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " "+ users_project.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = " - "

                            else:
                                hour_cost = " - "

                                if users_project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"hours")




                            total_hours+=int(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)

                    else:
                        # ingilizce
                        if member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                            # eğer projeye request yaptıysam - projenin sahibi değilim demektir.
                            member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                            if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                total_paid_supports+= 1
                                hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                            else:
                                # teklif verirken ücretli olarak belirtmemiş fakat normalde sisteme kendi ücretini
                                # nasıl belirlemiş - bunu almamızın sebebi girişimcinin ücret bazında ne kadar destek                                aldığını göstermemiz. Bu şekilde ben sana şunu yaptım - şu kadar para gerekiyor
                                # tartışmalarını sonlandırmak
                                if member.users_fullprofiles.usershourcosts_set.exists():
                                    hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = " - "

                            if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")

                            else:

                                member_project_hours = clean_hours_title(
                                    member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")
                            total_hours += int(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)

                        # direk projeye request yapılmadıysa eğer => projenin bağlandığı post var mı
                        elif not member.teams.users_projects.posts is None:
                            if users_project.users_fullprofiles.id != member.users_fullprofiles.id and member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():

                                member_investments = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                                if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    total_paid_supports+= 1
                                    hour_cost = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                else:
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(
                                        member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(
                                        member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"hours")

                                total_hours += int(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)

                            else:
                                member_investments = UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title_eng

                                if "paid-support" in UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title_code:
                                    total_paid_supports+= 1
                                    hour_cost =users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +  users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    if users_project.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = users_project.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +users_project.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if users_project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                                else:
                                    member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng, "hours")

                                total_hours += int(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)


                        else:
                            # proje sahibiyim
                            member_investments = UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title_eng

                            if "paid-support" in UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title_code:
                                total_paid_supports+= 1
                                hour_cost = users_project.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +users_project.users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                            else:
                                hour_cost = " - "


                            if users_project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"saat")
                            else:
                                member_project_hours = clean_hours_title(
                                users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title,
                                "hours")

                            # total_hours += int(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=users_project.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)
                            total_hours += int(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)



                    users_points = UsersPointScores.calculate_points(member.users_fullprofiles)

                    if users_points < 26:
                        point_css = "bg-danger"
                    elif users_points > 26 and users_points < 100:
                        point_css = "bg-warning"
                    else:
                        point_css = "bg-success"


                    item = {
                        "profile_image": profile_img,
                        "profile_name": profile_name,
                        "support_type": member_investments,
                        "hour_cost": hour_cost,
                        "project_hours": member_project_hours,
                        "profile_points": users_points,
                        "profile_id": member.users_fullprofiles.id,
                        "point_css": point_css,
                        "project_owner":is_project_owner,

                    }

                    context_team.append(item)

                paginator = Paginator(context_team, 5)

                page = 1
                try:
                    members_list = paginator.page(page)
                except PageNotAnInteger:
                    members_list = paginator.page(2)
                except EmptyPage:
                    members_list = paginator.page(paginator.num_pages)

                page = str(int(page) + 1)

                context = {
                    "team_members": members_list,
                    "has_next": members_list.has_next(),
                    "page": page,
                    "is_owner": is_owner,
                    "total_paid_supports": total_paid_supports,
                    "total_hours": total_hours,
                    "total_requests":total_requests,
                    "project_name":project_name,
                    "page_title":project_name + ugettext(" team members"),
                }
            else:
                context = {
                    "team_members": context_team,
                    "has_next": False,
                    "page": "",
                    "is_owner": is_owner,
                    "total_paid_supports": total_paid_supports,
                    "total_hours": total_hours,
                    "total_requests": total_requests,
                    "project_name": project_name,
                    "page_title":project_name + ugettext(" team members"),
                }
            return render(request, 'socialside/myteams/detail.html', context)

        elif UsersTeams.am_i_included_this_project(users_fullprofiles, UsersProjects.select_by_only_id(id)):

            is_owner = False

            users_project = UsersProjects.select_by_only_id(id)
            project_name = users_project.projects.general_infos.title

            if not users_project.posts is None:
                total_requests=users_project.posts.usersposts_set.get().userspoststeamrequests_set.count() + users_project.usersprojectsteamrequests_set.count()

            else:
                total_requests = users_project.usersprojectsteamrequests_set.count()

            total_hours = 0
            total_paid_supports = 0

            if Teams.is_there_team_by_project(users_project):
                selected_team = Teams.select_team(users_project)

                for member in selected_team.usersteams_set.filter(is_active=True).all():

                    if users_project.users_fullprofiles.id == member.users_fullprofiles.id:
                        is_project_owner = True
                    else:
                        is_project_owner = False

                    if not "default" in member.users_fullprofiles.full_profile.picture_url.name:
                        profile_img = member.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = member.users_fullprofiles.full_profile.linkedin_picture_url

                    profile_name = get_profile_name_str(member.users_fullprofiles.full_profile.last_name,
                                                        member.users_fullprofiles.full_profile.first_name)

                    # buraya eklenecek
                    if request.LANGUAGE_CODE == "tr":

                        if member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                            # eğer projeye request yaptıysam - projenin sahibi değilim demektir.
                            # is_project_owner=False
                            member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title

                            if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                total_paid_supports+=1
                                hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                            else:
                                # teklif verirken ücretli olarak belirtmemiş fakat normalde sisteme kendi ücretini
                                # nasıl belirlemiş - bunu almamızın sebebi girişimcinin ücret bazında ne kadar destek                                aldığını göstermemiz. Bu şekilde ben sana şunu yaptım - şu kadar para gerekiyor
                                # tartışmalarını sonlandırmak
                                if member.users_fullprofiles.usershourcosts_set.exists():
                                    hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = " - "

                            if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                            else:
                                member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"hours")



                            total_hours+=int(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)

                        # direk projeye request yapılmadıysa eğer => projenin bağlandığı post var mı
                        elif not member.teams.users_projects.posts is None:

                            if member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():

                                member_investments=member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title

                                if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    total_paid_supports+=1
                                    hour_cost=member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                else:
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost=""
                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"hours")

                                total_hours+=int(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)

                            else:
                                member_investments=UsersInvestments.select_users_investment(member.users_fullprofiles).investments.title
                                if "paid-support" in UsersInvestments.select_users_investment(member.users_fullprofiles).investments.title_code:
                                    total_paid_supports+=1
                                    hour_cost=member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost=""

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(member.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"hours")

                                total_hours+=int(member.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)



                        else:
                            # eğer projeye request yapmayıp - takımda isem takımın sahibiyim
                            # bu kısımda post tarafından da teklif yapmışmıyım ona bakmalıyım
                            member_investments = UsersInvestments.select_users_investment(member.users_fullprofiles).investments.title

                            if "paid-support" in UsersInvestments.select_users_investment(member.users_fullprofiles).investments.title_code:
                                total_paid_supports += 1
                                hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                            else:
                                if member.users_fullprofiles.usershourcosts_set.exists():
                                    hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = " - "


                            if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                member_project_hours = clean_hours_title(member.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                            else:
                                member_project_hours = clean_hours_title(member.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "hours")

                            total_hours+=int(member.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)

                    else:
                        # ingilizce
                        if member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                            # eğer projeye request yaptıysam - projenin sahibi değilim demektir.
                            member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                            if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                total_paid_supports += 1
                                hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                            else:
                                # teklif verirken ücretli olarak belirtmemiş fakat normalde sisteme kendi ücretini
                                # nasıl belirlemiş - bunu almamızın sebebi girişimcinin ücret bazında ne kadar destek                                aldığını göstermemiz. Bu şekilde ben sana şunu yaptım - şu kadar para gerekiyor
                                # tartışmalarını sonlandırmak
                                if member.users_fullprofiles.usershourcosts_set.exists():
                                    hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = " - "
                            if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")

                            else:

                                member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")

                            total_hours += int(member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)

                            # direk projeye request yapılmadıysa eğer => projenin bağlandığı post var mı
                        elif not member.teams.users_projects.posts is None:
                            if selected_team.users_projects.users_fullprofiles.id != member.users_fullprofiles.id and member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():

                                member_investments = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                                if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    total_paid_supports += 1
                                    hour_cost = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                else:
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "
                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"hours")

                                total_hours += int(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.value_code)

                            else:
                                member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_eng

                                if "paid-support" in UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_code:
                                    total_paid_supports += 1
                                    hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " "+selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    if selected_team.users_projects.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " "+selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                                else:
                                    member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng, "hours")

                                total_hours += int(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)


                        else:
                            # proje sahibiyim
                            member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_eng

                            if "paid-support" in UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_code:
                                total_paid_supports += 1
                                hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " "+selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                            else:
                                hour_cost = " - "

                            if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                member_project_hours = clean_hours_title(member.teams.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"saat")
                            else:
                                member_project_hours = clean_hours_title(member.teams.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,"hours")

                            total_hours += int(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.value_code)

                    # buraya eklenecek

                    users_points = UsersPointScores.calculate_points(member.users_fullprofiles)

                    item = {
                        "profile_image": profile_img,
                        "profile_name": profile_name,
                        "support_type": member_investments,
                        "hour_cost": hour_cost,
                        "project_hours": member_project_hours,
                        "profile_points": users_points,
                        "profile_id": member.users_fullprofiles.id,
                        "point_css": profile_points_css(users_points),
                        "project_owner":is_project_owner,

                    }

                    context_team.append(item)

                paginator = Paginator(context_team, 5)

                page = 1
                try:
                    members_list = paginator.page(page)
                except PageNotAnInteger:
                    members_list = paginator.page(2)
                except EmptyPage:
                    members_list = paginator.page(paginator.num_pages)

                page = str(int(page) + 1)

                context = {
                    "team_members": members_list,
                    "has_next": members_list.has_next(),
                    "page": page,
                    "is_owner":is_owner,
                    "total_paid_supports":total_paid_supports,
                    "total_requests":total_requests,
                    "total_hours":total_hours,
                    "project_name": project_name,
                    "page_title": project_name + ugettext(" team members"),
                }
            else:
                context = {
                    "team_members": context_team,
                    "has_next": False,
                    "page": "",
                    "is_owner": is_owner,
                    "total_paid_supports": total_paid_supports,
                    "total_requests": total_requests,
                    "total_hours": total_hours,
                    "project_name": project_name,
                    "page_title": project_name + ugettext(" team members"),
                }
            return render(request, 'socialside/myteams/detail.html', context)

        else:
            # proje bize ait değil
            return redirect("/dashboard/myteams/")
    else:
        return redirect("/login/")

@never_cache
def loadmore_detail(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "POST":
            users_project_id = request.POST.get("id")
            page = request.POST.get("page")

            context_team = []

            if UsersProjects.is_blong_this_user(users_fullprofiles, users_project_id):

                users_project = UsersProjects.select_by_id(users_project_id, users_fullprofiles)

                if Teams.is_there_team_by_project(users_project):
                    selected_team = Teams.select_team(users_project)
                    # print("seçilen takım : ",selected_team.usersteams_set.count())
                    for member in selected_team.usersteams_set.filter(is_active=True).all():
                        # print("users_profile : ",member.users_fullprofiles.id)
                        # print("***************")
                        if users_project.users_fullprofiles.id == member.users_fullprofiles.id:
                            is_project_owner = True
                        else:
                            is_project_owner = False

                        if not "default" in member.users_fullprofiles.full_profile.picture_url.name:
                            profile_img = member.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_img = member.users_fullprofiles.full_profile.linkedin_picture_url

                        profile_name = get_profile_name_str(member.users_fullprofiles.full_profile.last_name,
                                                            member.users_fullprofiles.full_profile.first_name)

                        if request.LANGUAGE_CODE == "tr":

                            if member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).exists():
                                # eğer projeye request yaptıysam
                                member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title

                                if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                else:
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")




                            elif not member.teams.users_projects.posts is None:

                                if member.teams.users_projects.users_fullprofiles.id != member.users_fullprofiles.id and member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                                    member_investments = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title
                                    if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                        hour_cost = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        if member.users_fullprofiles.usershourcosts_set.exists():
                                            hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                        else:
                                            hour_cost = ""

                                    if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                    else:
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"hours")


                                else:
                                    member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title
                                    if "paid-support" in UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_code:
                                        hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = ""

                                    if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                                    else:
                                        member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"hours")

                            else:
                                # eğer projeye request yapmayıp - takımda isem takımın sahibiyim
                                member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title

                                if selected_team.users_projects.users_fullprofiles.usershourcosts_set.exists():
                                    hour_cost = users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = " - "

                                if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                                else:
                                    member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,"hours")


                        else:
                            # ingilizce
                            if member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                                member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                                if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                else:
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")

                            elif not member.teams.users_projects.posts is None:
                                if selected_team.users_projects.users_fullprofiles.id != member.users_fullprofiles.id and member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                                    member_investments = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                                    if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:

                                        hour_cost = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " "+ member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                    else:

                                        if member.users_fullprofiles.usershourcosts_set.exists():
                                            hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                        else:
                                            hour_cost = " - "

                                    if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                    else:
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,
                                            "hours")

                                else:
                                    member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_eng

                                    if "paid-support" in UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_code:
                                        hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        if selected_team.users_projects.users_fullprofiles.usershourcosts_set.exists():
                                            hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                        else:
                                            hour_cost = " - "

                                    if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                                    else:
                                        member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,"hours")


                            else:
                                member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_eng

                                if "paid-support" in UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_code:
                                    hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    if selected_team.users_projects.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(
                                        selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title,
                                        "saat")
                                else:
                                    member_project_hours = clean_hours_title(
                                        selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,
                                        "hours")


                        users_points = UsersPointScores.calculate_points(member.users_fullprofiles)

                        if users_points < 26:
                            point_css = "bg-danger"
                        elif users_points > 26 and users_points < 100:
                            point_css = "bg-warning"
                        else:
                            point_css = "bg-success"

                        item = {
                            "profile_image": profile_img,
                            "profile_name": profile_name,
                            "support_type": member_investments,
                            "hour_cost": hour_cost,
                            "project_hours": member_project_hours,
                            "profile_points": users_points,
                            "profile_id": member.users_fullprofiles.id,
                            "point_css": point_css,
                            "project_owner":is_project_owner,
                        }

                        context_team.append(item)

                paginator = Paginator(context_team, 5)

                try:
                    members_list = paginator.page(page)
                except PageNotAnInteger:
                    members_list = paginator.page(2)
                except EmptyPage:
                    members_list = paginator.page(paginator.num_pages)

                page = str(int(page) + 1)

                context = {
                    "team_members": list(members_list),
                    "has_next": members_list.has_next(),
                    "page": page
                }

                return HttpResponse(json.dumps(context), content_type="application/json")

            elif UsersTeams.am_i_included_this_project(users_fullprofiles,UsersProjects.select_by_only_id(users_project_id)):
                users_project = UsersProjects.select_by_only_id(users_project_id)

                if Teams.is_there_team_by_project(users_project):
                    selected_team = Teams.select_team(users_project)

                    for member in selected_team.usersteams_set.filter(is_active=True).all():

                        if not "default" in member.users_fullprofiles.full_profile.picture_url.name:
                            profile_img = member.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_img = member.users_fullprofiles.full_profile.linkedin_picture_url

                        profile_name = get_profile_name_str(member.users_fullprofiles.full_profile.last_name,
                                                            member.users_fullprofiles.full_profile.first_name)

                        if request.LANGUAGE_CODE == "tr":

                            if member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).exists():
                                # eğer projeye request yaptıysam
                                is_project_owner = False
                                member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                    users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title

                                if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    # teklif verirken ücretli olarak belirtmemiş fakat normalde sisteme kendi ücretini
                                    # nasıl belirlemiş - bunu almamızın sebebi girişimcinin ücret bazında ne kadar destek                                aldığını göstermemiz. Bu şekilde ben sana şunu yaptım - şu kadar para gerekiyor
                                    # tartışmalarını sonlandırmak
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")


                            elif not member.teams.users_projects.posts is None:
                                if users_project.users_fullprofiles.id != member.users_fullprofiles.id and member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                                    is_project_owner=False
                                    member_investments = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title

                                    if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:

                                        hour_cost = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        if member.users_fullprofiles.usershourcosts_set.exists():
                                            hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code +" " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                        else:
                                            hour_cost = ""

                                    if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                    else:
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")
                                else:
                                    # proje sahibiyim
                                    is_project_owner=True
                                    member_investments = UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title
                                    if "paid-support" in UsersInvestments.select_users_investment(users_project.users_fullprofiles).investments.title_code:
                                        hour_cost = users_project.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + users_project.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = ""

                                    if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title,"saat")
                                    else:
                                        member_project_hours = clean_hours_title(users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,"hours")

                            else:
                                # eğer projeye request yapmayıp - takımda isem takımın sahibiyim
                                is_project_owner = True
                                member_investments = UsersInvestments.select_users_investment(
                                    users_project.users_fullprofiles).investments.title
                                if "paid-support" in UsersInvestments.select_users_investment(
                                        users_project.users_fullprofiles).investments.title_code:
                                    hour_cost = users_project.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + users_project.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    hour_cost = ""

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(
                                        users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title,
                                        "saat")
                                else:
                                    member_project_hours = clean_hours_title(
                                        users_project.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,
                                        "hours")

                        else:
                            # ingilizce
                            if member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                                is_project_owner=False
                                member_investments = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                                if "paid-support" in member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:
                                    hour_cost = member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.teams.users_projects.usersprojectsteamrequests_set.filter(
                                        users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                else:
                                    if member.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " +  member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                else:
                                    member_project_hours = clean_hours_title(member.teams.users_projects.usersprojectsteamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,"hours")

                            elif not member.teams.users_projects.posts is None:
                                if selected_team.users_projects.users_fullprofiles.id != member.users_fullprofiles.id and member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).exists():
                                    is_project_owner=False
                                    member_investments = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_eng

                                    if "paid-support" in member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_investments.investments.title_code:

                                        hour_cost = member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().users_fullprofiles.usershourcosts_set.get().hour_cost_amount

                                    else:

                                        if member.users_fullprofiles.usershourcosts_set.exists():
                                            hour_cost = member.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + member.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                        else:
                                            hour_cost = " - "

                                    if member.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title,"saat")
                                    else:
                                        member_project_hours = clean_hours_title(member.teams.users_projects.posts.usersposts_set.get().userspoststeamrequests_set.filter(users_fullprofiles=member.users_fullprofiles).get().teamrequest.users_request_projecthours.project_hours.title_eng,
                                            "hours")

                                else:
                                    # proje sahibiyim
                                    is_project_owner=True
                                    member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_eng

                                    if "paid-support" in UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_code:
                                        hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        if selected_team.users_projects.users_fullprofiles.usershourcosts_set.exists():
                                            hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                        else:
                                            hour_cost = " - "

                                    if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                        member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                                    else:
                                        member_project_hours = clean_hours_title(selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,"hours")


                            else:
                                # proje sahibiyim
                                is_project_owner=True
                                member_investments = UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_eng

                                if "paid-support" in UsersInvestments.select_users_investment(selected_team.users_projects.users_fullprofiles).investments.title_code:
                                    hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                else:
                                    if selected_team.users_projects.users_fullprofiles.usershourcosts_set.exists():
                                        hour_cost = selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + selected_team.users_projects.users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                                    else:
                                        hour_cost = " - "

                                if selected_team.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                                    member_project_hours = clean_hours_title(
                                        selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title,
                                        "saat")
                                else:
                                    member_project_hours = clean_hours_title(
                                        selected_team.users_projects.users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng,
                                        "hours")



                        users_points = UsersPointScores.calculate_points(member.users_fullprofiles)

                        if users_points < 26:
                            point_css = "bg-danger"
                        elif users_points > 26 and users_points < 100:
                            point_css = "bg-warning"
                        else:
                            point_css = "bg-success"

                        item = {
                            "profile_image": profile_img,
                            "profile_name": profile_name,
                            "support_type": member_investments,
                            "hour_cost": hour_cost,
                            "project_hours": member_project_hours,
                            "profile_points": users_points,
                            "profile_id": member.users_fullprofiles.id,
                            "point_css": point_css,
                            "project_owner":is_project_owner,
                        }

                        context_team.append(item)

                paginator = Paginator(context_team, 5)

                try:
                    members_list = paginator.page(page)
                except PageNotAnInteger:
                    members_list = paginator.page(2)
                except EmptyPage:
                    members_list = paginator.page(paginator.num_pages)

                page = str(int(page) + 1)

                context = {
                    "team_members": list(members_list),
                    "has_next": members_list.has_next(),
                    "page": page
                }

                return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def delete_member(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        user_id = request.POST.get("id")
        project_id = request.POST.get("project")
        if UsersProjects.is_blong_this_user(users_fullprofiles, project_id):
            users_project = UsersProjects.select_by_id(project_id, users_fullprofiles)
            selected_team = Teams.select_team(users_project)

            # bu kısımda member haklar girecek - bunu yapmamızın asıl nedeni: eğer kullanıcı açtığı bir projeyi
            # ismini değiştirip tekrardan takım isteği arar ise doğal olarak önceki takımdaki kişileri silmek isteyecek ve üyelik başına (her ay) sadece belli bir miktar silebilecek. Bu oran ekstra-kişi-ekleme ile bir tutuldu. Her ay sıfırlanacak

            if UsersMembershipTermsType.isOver_limitsize(users_fullprofiles, "remove-team-member",users_fullprofiles.users.users_membershiptypes.name_code) is False:
                if users_fullprofiles.id != UsersFullProfile.select_user(user_id).id:
                    if UsersTeams.delete_member(UsersFullProfile.select_user(user_id), selected_team):
                        UsersMembershipTermsType.create(users_fullprofiles, "remove-team-member",
                                                        users_fullprofiles.users.users_membershiptypes.name_code)

                        if UsersFriend.isThis_my_friend(users_fullprofiles, UsersFullProfile.select_user(user_id)):
                            UsersFriend.block_friend(users_fullprofiles, UsersFullProfile.select_user(user_id))

                        context = {
                            "message_type": "success",
                            "message": ugettext("You have successfully removed this person from your team.")
                        }
                    else:
                        context = {
                            "message_type": "error",
                            "message": ugettext("An error occurred during the process. Try again later, please.")
                        }
                else:
                    if users_fullprofiles.id == UsersFullProfile.select_user(user_id).id:
                        message = ugettext("You can't remove yourself from your team.")
                    else:
                        message = ugettext("An error occurred during the process. Try again later, please.")

                    context = {
                        "message_type": "error",
                        "message": message,
                    }
            else:
                if request.LANGUAGE_CODE == "tr":
                    message = "Takımınızda büyük bir revizyon gerçekleştirmek istediğini görüyoruz ve anlıyoruz. Ayrıca, başarıyı yakalamak için ne gerekirse yapacağının da farkındayız. Bu yüzden size ücretli üyeliklerimizden faydalanmanızı öneriyoruz veya limitinizin sıfırlanması için bir ay bekleyebilirsiniz."
                else:
                    message = "We understand that you want to perform a major revision on your team. We also understand that you will do whatever it takes to catch the success. So we want you to take advantage of our paid memberships. Or you have to wait about one month for your limit to be renewed."

                context = {
                    "message_type": "error",
                    "message": message
                }
        else:
            context = {
                "message_type": "error",
                "message": ugettext("This project is not belong to you. You are only a team member.")
            }

        return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(60 * 5)
def co_funded_projects(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        context_projects = []
        context_members = []

        if request.method == "POST":
            page = request.POST.get("page")

            if UsersTeams.is_there_anyproject_included(users_fullprofiles):
                cofunded_projects = UsersTeams.get_teams_in_projects(users_fullprofiles)
                # bu kısımda proje sahibini ayırmamız lazım. Proje sahibi dışındakiler co-funded
                for prj in cofunded_projects:
                    if prj.teams.users_projects.users_fullprofiles.id != users_fullprofiles.id:
                        for member in UsersTeams.get_teammembers_by_teams(prj.teams):
                            if not "default" in member.users_fullprofiles.full_profile.picture_url.name:
                                profile_img = member.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_img = member.users_fullprofiles.full_profile.linkedin_picture_url

                            item_member = {
                                "profile_image": profile_img,
                            }

                            context_members.append(item_member)

                        if len(remove_html_tags(prj.teams.users_projects.projects.general_infos.describe)) > 140:
                            project_describe= remove_html_tags(prj.teams.users_projects.projects.general_infos.describe)[:140] + "..."
                        else:
                            project_describe = remove_html_tags(prj.teams.users_projects.projects.general_infos.describe)

                        item = {
                            "project_image": prj.teams.users_projects.projects.general_infos.project_logo.name,
                            "project_title": prj.teams.users_projects.projects.general_infos.title,
                            "project_describe": project_describe,
                            "project_location": get_fulllocation_string(prj.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,prj.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + prj.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                            "project_members": context_members,
                            "project_id": prj.teams.users_projects.id,
                        }

                        context_projects.append(item)
                        context_members = []

            paginator = Paginator(context_projects, 5)

            if len(context_projects) > 0:
                message = ""
            else:
                message = ugettext("You do not have a project that you are a co-funded.")

            try:
                projects_list = paginator.page(page)
            except PageNotAnInteger:
                projects_list = paginator.page(1)
            except EmptyPage:
                projects_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "my_projects": list(projects_list),
                "has_next": projects_list.has_next(),
                "page": page,
                "message": message,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        return redirect("/login/")

@never_cache
def myrequests(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        context_requests=[]
        if request.method == "POST":
            page=request.POST.get("page")
            id=request.POST.get("id")

            if UsersProjects.is_blong_this_user(users_fullprofiles,id):
                users_project=UsersProjects.select_by_id(id,users_fullprofiles)
                today=datetime.now().date()
                # is_give_up=False
                # is_convince=False
                # is_success=False

                # ÖNEMLİ => BU KISIMDA SADECE BİZİM BU PROJE İÇİN GÖNDERDİĞİMİZ ÖZEL REQUEST LERİ LİSTELEYECEĞİZ
                #        => KULLANICI BAŞKA BİRİNE ÖZEL REQUEST GÖNDERMEK İSTEDİĞİMİZDE EĞER LİMİTİ DOLDUYSA
                #        => BURDAN İŞİNE YARAMAYAN REQUEST'LERİ (CEVAP VERMEYEN KİŞİLERİ) KALDIRIP BAŞKA REQUEST'LER
                #        => ATABİLECEK


                for requests in UsersDmProjectsTeamrequests.list_sent_requests(users_fullprofiles,users_project):

                    profile_points=UsersPointScores.calculate_points(requests.to_users_fullprofiles)



                    if request.LANGUAGE_CODE == "tr":

                        investments=UsersInvestments.select_users_investment(requests.to_users_fullprofiles).investments.title
                    else:
                        investments = UsersInvestments.select_users_investment(requests.to_users_fullprofiles).investments.title_eng

                    if "paid-support" in UsersInvestments.select_users_investment(requests.to_users_fullprofiles).investments.title_code:
                        hour_cost=requests.to_users_fullprofiles.usershourcosts_set.get().hour_costs.cost_code + " " + requests.to_users_fullprofiles.usershourcosts_set.get().hour_cost_amount
                    else:
                        hour_cost=" - "

                    if requests.to_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood == "TR":
                        project_hours = clean_hours_title(requests.to_users_fullprofiles.usersprojecthours_set.get().project_hours.title,"saat")
                    else:
                        project_hours = clean_hours_title(requests.to_users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng, "hours")

                    if not "default" in requests.to_users_fullprofiles.full_profile.picture_url.name:
                        profile_img = requests.to_users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_img = requests.to_users_fullprofiles.full_profile.linkedin_picture_url

                    if requests.is_read == False:

                        if ((today)-(requests.created_date.date())).days >= 5:
                            # bu isteğin üzerinden 5 gün geçmiş veya 5. gününde
                            is_give_up=True # kırmızı renk
                            is_success=False
                            is_convince=False
                        else:
                            is_give_up=False
                            is_success=False
                            is_convince=False

                    elif requests.is_response == False:
                        if ((today) - requests.readed_date.date()).days == 3:
                            is_convince=True # info renk
                            is_give_up = False
                            is_success = False
                        elif ((today) - requests.readed_date.date()).days >=7:
                            is_convince = False # kırmızı renk
                            is_give_up = True
                            is_success = False
                        else:
                            is_give_up=True
                            is_convince=False
                            is_success=False


                    else:
                        is_give_up=False
                        is_success=True
                        is_convince=False


                    profile_name = get_person_profile_name(users_fullprofiles.id, requests.to_users_fullprofiles.id)

                    item= {
                        "profile_name":profile_name,
                        "profile_image":profile_img,
                        "profile_points":profile_points,
                        "profile_point_css":profile_points_css(profile_points),
                        "support_type":investments,
                        "hour_cost":hour_cost,
                        "project_hours":project_hours,
                        "profile_id":requests.to_users_fullprofiles.id,
                        "request_id":requests.id,
                        "is_give_up":is_give_up,
                        "is_convince":is_convince,
                        "is_success":is_success,
                    }

                    context_requests.append(item)

                message_type="success"
                message=""
            else:
                # proje bu kullanıcıya ait değil
                message_type="error"
                message=ugettext("This project doesn't belong you")

            paginator = Paginator(context_requests, 5)

            try:
                requests_list = paginator.page(page)
            except PageNotAnInteger:
                requests_list = paginator.page(1)
            except EmptyPage:
                requests_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "requests": list(requests_list),
                "has_next": requests_list.has_next(),
                "page": page,
                "message":message,
                "message_type":message_type,

            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def delete_dm_request(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        #silme haklıarını kontrol ederek sil
        remove_dm_count = 0
        if request.method == "POST":
            request_id=request.POST.get("id")
            selected_request=UsersDmProjectsTeamrequests.select_this_by_id(request_id)

            if selected_request.is_response:
                # bu cevap verilmiş bir istek ise silinemez
                context={
                    "message_type":"error",
                    "message":ugettext("This request answered to you. You can't delete this.")
                }
            else:
                # silinebilir
                remove_dm_limit = MembershipTermsType.select_terms_type(users_fullprofiles.users.users_membershiptypes.name_code, "remove-dm-request").limit_count

                if UsersMembershipTermsType.isOver_limitsize(users_fullprofiles,"remove-dm-request",users_fullprofiles.users.users_membershiptypes.name_code) is False:
                    if remove_dm_count < remove_dm_limit:
                        UsersDmProjectsTeamrequests.delete_this_request(request_id)
                        UsersMembershipTermsType.create(users_fullprofiles,"remove-dm-request",users_fullprofiles.users.users_membershiptypes.name_code)
                        remove_dm_count+=1
                        context={
                            "message_type":"success",
                            "message":ugettext("Successfully deleted.")
                        }
                    else:
                        context = {
                            "message_type": "error",
                            "message": ugettext("You have reached the limit of deletion in your current membership package. You have to wait about one month for your limit to be renewed."),
                        }
                else:
                    context={
                        "message_type":"error",
                        "message":ugettext("You have reached the limit of deletion in your current membership package. You have to wait about one month for your limit to be renewed."),
                    }

            return HttpResponse(json.dumps(context),content_type="application/json")