from django.contrib import admin
from .models import *
# Register your models here.

class MembershipTermsAdmin(admin.ModelAdmin):
    list_display = ['term_title','term_text','term_code','is_active']

    class Meta:
        model=MembershipTerms


class MembershipTypesAdmin(admin.ModelAdmin):
    list_display = ['title', 'name', 'name_code', 'is_active']

    class Meta:
        model = MembershipTypes

class MembershipTermsTypeAdmin(admin.ModelAdmin):
    list_display = ['membership_types', 'membership_terms', 'limit_count']
    list_filter = ['membership_types']
    class Meta:
        model = MembershipTermsType

admin.site.register(MembershipTerms,MembershipTermsAdmin)
admin.site.register(MembershipTypes,MembershipTypesAdmin)
admin.site.register(MembershipTermsType,MembershipTermsTypeAdmin)