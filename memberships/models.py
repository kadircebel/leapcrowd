from django.db import models

class MembershipTypes(models.Model):
    title = models.CharField(max_length=150, blank=True)
    name = models.CharField(max_length=150, blank=True)
    name_code = models.CharField(max_length=150, blank=True)
    created_date = models.DateField(auto_created=True)
    created_user = models.IntegerField(blank=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        managed = True
        db_table = 'membership_types'

    @classmethod
    def select(cls,name_code):
        return cls.objects.filter(name_code=name_code).get()

    @classmethod
    def select_all_package(cls):
        return cls.objects.all()




class MembershipTerms(models.Model):
    term_title = models.CharField(max_length=150, blank=True)
    term_text = models.TextField(blank=True)
    term_code = models.CharField(max_length=150, blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    created_user = models.IntegerField(blank=True)
    id = models.BigAutoField(primary_key=True)

    def __str__(self):
        return '{}'.format(self.term_title)

    class Meta:
        managed = True
        db_table = 'membership_terms'

    @classmethod
    def select(cls,term_code):
        return cls.objects.filter(term_code=term_code).get()

class MembershipTermsType(models.Model):
    membership_types=models.ForeignKey('MembershipTypes',models.DO_NOTHING)
    membership_terms=models.ForeignKey('MembershipTerms',models.DO_NOTHING)
    limit_count = models.IntegerField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'membership_terms_type'

    @classmethod
    def is_there_benefit(cls,membership_type_code,term_code):
        try:
            selected_membership_types = MembershipTypes.select(membership_type_code)
            selected_term = MembershipTerms.select(term_code)
            if cls.objects.filter(membership_types=selected_membership_types,membership_terms=selected_term).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def package_limitsize(cls,membership_type_code,term_code):
        selected_membership_types=MembershipTypes.select(membership_type_code)
        selected_term=MembershipTerms.select(term_code)
        return cls.objects.filter(membership_types=selected_membership_types,membership_terms=selected_term).get().limit_count

    @classmethod
    def select_terms_type(cls,membership_type_code,term_code):
        selected_membership_types = MembershipTypes.select(membership_type_code)
        selected_term = MembershipTerms.select(term_code)
        return cls.objects.filter(membership_types=selected_membership_types,membership_terms=selected_term).get()