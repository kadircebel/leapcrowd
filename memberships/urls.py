from django.urls import path
from .views import *

app_name="memberships"

urlpatterns=[
    path('',index,name='index'),
    path('membership-options/', control_membership_limit, name='control_membership_limit'),

]