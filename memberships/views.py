from django.shortcuts import render,redirect,HttpResponse
# from .models import *
from persons.models import UsersFullProfile,UsersMembershipTermsType
import json,datetime

from django.views.decorators.csrf import csrf_protect
from django.utils.translation import ugettext
from django.views.decorators.cache import cache_page,never_cache

@cache_page(60*15)
def index(request):
    if 'user_fullprofile' in request.session:
        context={
            "page_title":ugettext("Our paid memberships"),
        }
        return render(request,"socialside/memberships/index.html",context)
    else:
        return redirect("/login/")


@never_cache
@csrf_protect
def control_membership_limit(request):
    if 'user_fullprofile' in request.session:
        user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        membership_terms=["remove-team-member","send-direct-teamrequest","send-message","remote-watching","remove-dm-request"]

        if UsersMembershipTermsType.is_there_any_terms(user_fullprofile):
            successful_counter=0
            if "beginner" in user_fullprofile.users.users_membershiptypes.name_code:
                if user_fullprofile.users.memberships_renew_date is None:
                    start_date = user_fullprofile.users.joined_date.date()
                else:
                    start_date=user_fullprofile.users.memberships_renew_date.date()

                today_date=datetime.date.today()
                result_date=(today_date-start_date).days

                if result_date > 30:
                    # bu kısımda sıfırlama işlemleri
                    for term_code in membership_terms:
                        if UsersMembershipTermsType.reset_membership_terms(user_fullprofile,term_code,user_fullprofile.users.users_membershiptypes.name_code):
                            successful_counter+=1

                    if successful_counter > 0:
                        user_fullprofile.users.update_membership_dates(user_fullprofile.users.id)
                        message_type="warning"
                        message=ugettext("Your membership package limits have been renewed.")
                    else:
                        message_type="error"
                        message=ugettext("Your membership package limits couldn't be renewed. If you report this problem to us [hello@leapcrowd.co], we will immediately take care of it.")
                else:
                    message_type="success",
                    message=""

                context={
                    "message_type":message_type,
                    "message":message
                }
            else:
                # bu kısma ayrı bir tablo yapılacak ve bu tabloda aldığı ücretli üyeliğe göre
                # sıfırlanması yapılacak
                context = {
                    "message_type": "success",
                    # "message": ugettext("We're working new membership models.")
                    "message":""
                }

        else:
            context={
                "message_type":"success",
                "message":"",
            }

        return HttpResponse(json.dumps(context), content_type="application/json")