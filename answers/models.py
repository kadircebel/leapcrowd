from django.db import models

# Create your models here.
class Answers(models.Model):
    answer_text = models.TextField(blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(blank=True,auto_now_add=True)
    # posts_id = models.IntegerField(blank=True)
    # posts=models.OneToOneField('ideas.Posts',on_delete=models.CASCADE)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'answers'

    @classmethod
    def select_answer(cls,id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def create_answer(cls,answer_text):
        new_record=cls(answer_text=answer_text)
        new_record.save()
        return new_record

    @classmethod
    def update_answer(cls,id,answer_text):
        selected=cls.objects.filter(id=id).get()
        selected.answer_text=answer_text
        selected.save()
        return selected


class AnswersComplained(models.Model):
    answers = models.ForeignKey('Answers', models.DO_NOTHING, blank=True)
    complained_options = models.ForeignKey('mainadmins.ComplainedOptions', models.DO_NOTHING, blank=True)
    is_complained = models.NullBooleanField()
    content_text = models.TextField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'answers_complained'

    @classmethod
    def isThis_complained(cls,answers):
        try:
            if cls.objects.filter(answers=answers).count() > 0:
                return True
            else:
                return False
        except:
            return False