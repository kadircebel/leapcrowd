from django.apps import AppConfig


class VueappsConfig(AppConfig):
    name = 'vueapps'
