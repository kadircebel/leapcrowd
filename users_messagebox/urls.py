from django.urls import path
from .views import *


app_name = "messages"

urlpatterns = [
    path('', index, name='index'),
    path('loadmore-inbox/', loadmore_inbox, name='loadmore_inbox'),
    path('send-archive/', send_archive, name='send_archive'),
    path('send-trash/', send_trash, name='send_trash'),
    path('send-delete/', send_delete, name='send_delete'),
    path('notification-count/', notification_count, name='notification_count'),
    path('notification-list/', notification_list, name='notification_list'),
    path('loadmore-notification-list/', loadmore_notification_list, name='loadmore_notification_list'),
    path('dm-person-info/', dm_person_info, name='dm_person_info'),

    path('send-reply/', send_reply_message, name='send_reply'),
    path('send-message/', send_message_to_person, name='send_message_to_person'),
    path('loadmore-detail/', loadmore_detail, name='loadmore_detail'),
    path('detail/<int:id>/', detail, name='messages_detail'),

    path('<slug:type_name>/', index, name='index'),

]
