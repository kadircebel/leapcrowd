from django.apps import AppConfig


class UsersMessageboxConfig(AppConfig):
    name = 'users_messagebox'
