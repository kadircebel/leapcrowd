from django.shortcuts import render, redirect, HttpResponse
from persons.models import UsersFullProfile, UsersFriend, UsersMembershipTermsType, UsersMessages,UsersPointScores,UsersIndustries
from .models import *
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.decorators.csrf import csrf_protect
from utils.views import get_profile_name_str, remove_html_tags,get_fulllocation_string,get_person_profile_name,is_it_same_location,new_message_notification
import json
from django.utils.translation import ugettext
from django.utils.timesince import timesince
from django.views.decorators.cache import cache_page,never_cache

@never_cache
def index(request, type_name="inbox"):
    # type_name = inbox,trash,archive
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        unread_messages_count=Messages.unread_messages_count(users_fullprofiles)

        if "archive" in type_name:
            msg_list = Messages.archived_messages_list(True, users_fullprofiles, request.LANGUAGE_CODE)

            paginator = Paginator(msg_list, 10)

            page = 1

            try:
                messages_list = paginator.page(page)
            except PageNotAnInteger:
                messages_list = paginator.page(2)
            except EmptyPage:
                messages_list = paginator.page(paginator.num_pages)

            context = {
                "messages": messages_list,
                "has_next": messages_list.has_next(),
                "page": str(int(page) + 1),
                "page_type": type_name,
                "unread_count": unread_messages_count,
                "page_title":ugettext("Archive"),
            }
        elif "trash" in type_name:
            try:

                msg_list = Messages.trash_messages_list(True, users_fullprofiles, request.LANGUAGE_CODE)

                paginator = Paginator(msg_list, 10)

                page = 1

                try:
                    messages_list = paginator.page(page)
                except PageNotAnInteger:
                    messages_list = paginator.page(2)
                except EmptyPage:
                    messages_list = paginator.page(paginator.num_pages)

                context = {
                    "messages": messages_list,
                    "has_next": messages_list.has_next(),
                    "page": str(int(page) + 1),
                    "page_type": type_name,
                    "unread_count": unread_messages_count,
                    "page_title":ugettext("Trash"),
                }

            except:
                context = {
                    "message_type": "error",
                    "message": ugettext("You have not sent any messages yet."),
                    "page_title":ugettext("Trash"),
                }
        elif "sent" in type_name:
            try:

                msg_list = UsersMessages.sended_messages_list(users_fullprofiles, request.LANGUAGE_CODE)

                paginator = Paginator(msg_list, 10)

                page = 1

                try:
                    messages_list = paginator.page(page)
                except PageNotAnInteger:
                    messages_list = paginator.page(2)
                except EmptyPage:
                    messages_list = paginator.page(paginator.num_pages)

                context = {
                    "messages": messages_list,
                    "has_next": messages_list.has_next(),
                    "page": str(int(page) + 1),
                    "page_type": type_name,
                    "unread_count": unread_messages_count,
                    "page_title":ugettext("Sent"),
                }

            except:
                context = {
                    "message_type": "error",
                    "message": ugettext("You have not sent any messages yet."),
                    "page_type": type_name,
                    "unread_count": unread_messages_count,
                    "page_title":ugettext("Sent"),
                }

        else:
            # inbox
            msg_list = Messages.inbox_messages_list(True, users_fullprofiles, request.LANGUAGE_CODE)

            paginator = Paginator(msg_list, 10)

            page = 1

            try:
                messages_list = paginator.page(page)
            except PageNotAnInteger:
                messages_list = paginator.page(2)
            except EmptyPage:
                messages_list = paginator.page(paginator.num_pages)

            context = {
                "messages": messages_list,
                "has_next": messages_list.has_next(),
                "page": str(int(page) + 1),
                "page_type": type_name,
                "unread_count":unread_messages_count,
                "page_title":ugettext("Inbox")
            }

        return render(request, "socialside/messagebox/index.html", context)

@never_cache
@csrf_protect
def loadmore_inbox(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            page = request.POST.get("page")
            page_type = request.POST.get("page_type")
            message = ""
            if "inbox" in page_type:
                msg_list = Messages.inbox_messages_list(True, users_fullprofiles, request.LANGUAGE_CODE)
                if len(msg_list) == 0:
                    message = ugettext("You have not received a message yet.")

            elif "sent" in page_type:
                msg_list = UsersMessages.sended_messages_list(users_fullprofiles, request.LANGUAGE_CODE)
                if len(msg_list) == 0:
                    message = ugettext("You have not sent a message yet.")
            elif "trash" in page_type:
                msg_list = Messages.trash_messages_list(True, users_fullprofiles, request.LANGUAGE_CODE)
                if len(msg_list) == 0:
                    message = ugettext("You have not deleted a message yet.")
            else:
                msg_list = Messages.archived_messages_list(True, users_fullprofiles, request.LANGUAGE_CODE)
                if len(msg_list) == 0:
                    message = ugettext("You have not archived a message yet.")

            paginator = Paginator(msg_list, 10)

            try:
                messages_list = paginator.page(page)
            except PageNotAnInteger:
                messages_list = paginator.page(2)
            except EmptyPage:
                messages_list = paginator.page(paginator.num_pages)

            context = {
                "messages": list(messages_list),
                "has_next": messages_list.has_next(),
                "page": str(int(page) + 1),
                "message": message,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def detail(request, id):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        # ilk gönderilen ana mail
        context_messages = []
        unread_messages_count = Messages.unread_messages_count(users_fullprofiles)

        try:
            selected_message = UsersMessages.select_message(Messages.select_message(id))
            selected_message.messages.read_this_message(id, users_fullprofiles)

            if len(selected_message.messages.title) > 0:
                message_title = selected_message.messages.title
            else:
                if len(str(remove_html_tags(selected_message.messages.body_text))) > 15:
                    message_title = str(remove_html_tags(selected_message.messages.body_text))[:15] + "..."
                else:
                    message_title = str(remove_html_tags(selected_message.messages.body_text))

            # bu kısımda benim network'ümde olan kişiyi sorduğum sorunun sahibine göre ayırıyoruz
            if users_fullprofiles.id == selected_message.users_fullprofiles.id:
                is_network = UsersFriend.isThis_my_friend(selected_message.users_fullprofiles,
                                                          selected_message.messages.to_users_fullprofiles)
            else:
                is_network = UsersFriend.isThis_my_friend(users_fullprofiles, selected_message.users_fullprofiles)

            can_reply = True

            if selected_message.parent_message is None:
                # eğer mesaja bağlı ana bir mesaj yok ise
                # bu bölümdeki senaryo : bize bir mesaj geldiyse ve ve biz bu mesaja cevap attıysak

                if not "default" in selected_message.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = selected_message.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = selected_message.users_fullprofiles.full_profile.linkedin_picture_url

                profile_name = get_person_profile_name(users_fullprofiles.id, selected_message.users_fullprofiles.id)

                item = {
                    "profile_image": profile_picture,
                    "profile_name": profile_name,
                    "message_content": selected_message.messages.fk_messages.get().messages.body_text,
                    "sended_date": timesince(selected_message.messages.fk_messages.get().messages.send_date),
                    "id": selected_message.messages.fk_messages.get().messages.id,
                }

                context_messages.append(item)

                # bu mesaja cevap yazdıysam cevabı da getir

                if selected_message.messages.fk_parent_messages is not None:
                    for msg in UsersMessages.get_sub_messages(selected_message.messages.fk_messages.get().messages):
                        print("users messages : ", msg)
                        print("messages : ", msg.messages)

                        if not "default" in msg.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = msg.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = msg.users_fullprofiles.full_profile.linkedin_picture_url

                        sub_profile_name = get_profile_name_str(msg.users_fullprofiles.full_profile.last_name,
                                                                msg.users_fullprofiles.full_profile.first_name)

                        item = {
                            "profile_image": profile_picture,
                            "profile_name": sub_profile_name,
                            "message_content": msg.messages.body_text,
                            "sended_date": timesince(msg.messages.send_date),
                            "id": msg.messages.id,
                        }

                        context_messages.append(item)
            elif selected_message.parent_message is not None:

                # eğer mesaj başka bir mesaja bağlıysa
                # bu kısım da ilk önce ana mesaj
                if not "default" in selected_message.parent_message.fk_messages.get().users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = selected_message.parent_message.fk_messages.get().users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = selected_message.parent_message.fk_messages.get().users_fullprofiles.full_profile.linkedin_picture_url

                profile_name = get_person_profile_name(users_fullprofiles.id,
                                                       selected_message.parent_message.fk_messages.get().users_fullprofiles.id)

                item = {
                    "profile_image": profile_picture,
                    "profile_name": profile_name,
                    "message_content": selected_message.parent_message.fk_messages.get().messages.body_text,
                    "sended_date": timesince(selected_message.parent_message.fk_messages.get().messages.send_date),
                    "id": selected_message.parent_message.fk_messages.get().messages.id,
                }

                context_messages.append(item)

                for msg in UsersMessages.get_sub_messages(selected_message.parent_message.fk_messages.get().messages):
                    msg.messages.read_this_message(msg.messages.id, users_fullprofiles)

                    if not "default" in msg.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = msg.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = msg.users_fullprofiles.full_profile.linkedin_picture_url

                    sub_profile_name = get_profile_name_str(msg.users_fullprofiles.full_profile.last_name,
                                                            msg.users_fullprofiles.full_profile.first_name)

                    item = {
                        "profile_image": profile_picture,
                        "profile_name": sub_profile_name,
                        "message_content": msg.messages.body_text,
                        "sended_date": timesince(msg.messages.send_date),
                        "id": msg.messages.id,
                    }

                    context_messages.append(item)
            else:
                if not "default" in selected_message.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = selected_message.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = selected_message.users_fullprofiles.full_profile.linkedin_picture_url

                profile_name = get_profile_name_str(selected_message.users_fullprofiles.full_profile.last_name,
                                                    selected_message.users_fullprofiles.full_profile.first_name)

                item = {
                    "profile_image": profile_picture,
                    "profile_name": profile_name,
                    "message_content": selected_message.messages.body_text,
                    "sended_date": timesince(selected_message.messages.send_date),
                    "id": selected_message.messages.id,
                }

                context_messages.append(item)


        except:
            selected_message = Messages.select_message(id)
            selected_message.read_this_message(id, users_fullprofiles)
            item = {
                "profile_image": "/media/leapcrowd/profile.jpg",
                "profile_name": "Leapcrowd",
                "message_content": selected_message.body_text,
                "sended_date": timesince(selected_message.send_date),
                "id": selected_message.id,
            }

            context_messages.append(item)

            is_network="kiyo"
            message_title=selected_message.title
            can_reply=False


        page = 1
        paginator = Paginator(sorted(context_messages, key=lambda item: item["id"], reverse=True), 5)

        try:
            messages_list = paginator.page(page)
        except PageNotAnInteger:
            messages_list = paginator.page(1)
        except EmptyPage:
            messages_list = paginator.page(paginator.num_pages)

        page = str(int(page) + 1)

        context = {
            "messages": messages_list,
            "has_next": messages_list.has_next(),
            "page": page,
            "is_network": is_network,
            "title": message_title,
            "can_reply":can_reply,
            "unread_count": unread_messages_count,
        }


        return render(request, "socialside/messagebox/detail.html", context)

@never_cache
@csrf_protect
def loadmore_detail(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            page=request.POST.get("page")
            id=request.POST.get("id")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            context_messages = []

            try:
                selected_message = UsersMessages.select_message(Messages.select_message(id))
                selected_message.messages.read_this_message(id, users_fullprofiles)

                if len(selected_message.messages.title) > 0:
                    message_title = selected_message.messages.title
                else:
                    if len(str(remove_html_tags(selected_message.messages.body_text))) > 15:
                        message_title = str(remove_html_tags(selected_message.messages.body_text))[:15] + "..."
                    else:
                        message_title = str(remove_html_tags(selected_message.messages.body_text))

                # bu kısımda benim network'ümde olan kişiyi sorduğum sorunun sahibine göre ayırıyoruz
                if users_fullprofiles.id == selected_message.users_fullprofiles.id:
                    is_network = UsersFriend.isThis_my_friend(selected_message.users_fullprofiles,
                                                              selected_message.messages.to_users_fullprofiles)
                else:
                    is_network = UsersFriend.isThis_my_friend(users_fullprofiles, selected_message.users_fullprofiles)

                can_reply = True

                if selected_message.parent_message is None:
                    # eğer mesaja bağlı ana bir mesaj yok ise
                    # bu bölümdeki senaryo : bize bir mesaj geldiyse ve ve biz bu mesaja cevap attıysak

                    if not "default" in selected_message.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = selected_message.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = selected_message.users_fullprofiles.full_profile.linkedin_picture_url

                    profile_name = get_person_profile_name(users_fullprofiles.id,
                                                           selected_message.users_fullprofiles.id)

                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "message_content": selected_message.messages.fk_messages.get().messages.body_text,
                        "sended_date": timesince(selected_message.messages.fk_messages.get().messages.send_date),
                        "id": selected_message.messages.fk_messages.get().messages.id,
                    }

                    context_messages.append(item)

                    # bu mesaja cevap yazdıysam cevabı da getir

                    if selected_message.messages.fk_parent_messages is not None:
                        for msg in UsersMessages.get_sub_messages(selected_message.messages.fk_messages.get().messages):
                            print("users messages : ", msg)
                            print("messages : ", msg.messages)

                            if not "default" in msg.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = msg.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = msg.users_fullprofiles.full_profile.linkedin_picture_url

                            sub_profile_name = get_profile_name_str(msg.users_fullprofiles.full_profile.last_name,
                                                                    msg.users_fullprofiles.full_profile.first_name)

                            item = {
                                "profile_image": profile_picture,
                                "profile_name": sub_profile_name,
                                "message_content": msg.messages.body_text,
                                "sended_date": timesince(msg.messages.send_date),
                                "id": msg.messages.id,
                            }

                            context_messages.append(item)
                elif selected_message.parent_message is not None:

                    # eğer mesaj başka bir mesaja bağlıysa
                    # bu kısım da ilk önce ana mesaj
                    if not "default" in selected_message.parent_message.fk_messages.get().users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = selected_message.parent_message.fk_messages.get().users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = selected_message.parent_message.fk_messages.get().users_fullprofiles.full_profile.linkedin_picture_url

                    profile_name = get_person_profile_name(users_fullprofiles.id,
                                                           selected_message.parent_message.fk_messages.get().users_fullprofiles.id)

                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "message_content": selected_message.parent_message.fk_messages.get().messages.body_text,
                        "sended_date": timesince(selected_message.parent_message.fk_messages.get().messages.send_date),
                        "id": selected_message.parent_message.fk_messages.get().messages.id,
                    }

                    context_messages.append(item)

                    for msg in UsersMessages.get_sub_messages(
                            selected_message.parent_message.fk_messages.get().messages):
                        msg.messages.read_this_message(msg.messages.id, users_fullprofiles)

                        if not "default" in msg.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = msg.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = msg.users_fullprofiles.full_profile.linkedin_picture_url

                        sub_profile_name = get_profile_name_str(msg.users_fullprofiles.full_profile.last_name,
                                                                msg.users_fullprofiles.full_profile.first_name)

                        item = {
                            "profile_image": profile_picture,
                            "profile_name": sub_profile_name,
                            "message_content": msg.messages.body_text,
                            "sended_date": timesince(msg.messages.send_date),
                            "id": msg.messages.id,
                        }

                        context_messages.append(item)
                else:
                    if not "default" in selected_message.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = selected_message.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = selected_message.users_fullprofiles.full_profile.linkedin_picture_url

                    profile_name = get_profile_name_str(selected_message.users_fullprofiles.full_profile.last_name,
                                                        selected_message.users_fullprofiles.full_profile.first_name)

                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "message_content": selected_message.messages.body_text,
                        "sended_date": timesince(selected_message.messages.send_date),
                        "id": selected_message.messages.id,
                    }

                    context_messages.append(item)


            except:
                selected_message = Messages.select_message(id)
                selected_message.read_this_message(id, users_fullprofiles)
                item = {
                    "profile_image": "/media/leapcrowd/profile.jpg",
                    "profile_name": "Leapcrowd",
                    "message_content": selected_message.body_text,
                    "sended_date": timesince(selected_message.send_date),
                    "id": selected_message.id,
                }

                context_messages.append(item)

                # is_network = "kiyo"
                # message_title = selected_message.title
                # can_reply = False


            paginator = Paginator(sorted(context_messages, key=lambda item: item["id"], reverse=True), 5)

            try:
                messages_list = paginator.page(page)
            except PageNotAnInteger:
                messages_list = paginator.page(1)
            except EmptyPage:
                messages_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "messages": list(messages_list),
                "has_next": messages_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(context),content_type="application/json")

@cache_page(60 * 5)
@csrf_protect
def dm_person_info(request):
    if "user_fullprofile" in request.session:
        context_users = []
        if request.method == "POST":
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_id = request.POST.get("id")
            selected_user=UsersFullProfile.select_user(users_id)

            if is_it_same_location(users_fullprofile.id,selected_user.id) or UsersFriend.isThis_my_friend(users_fullprofile, selected_user) == True:

                if not "default" in selected_user.full_profile.picture_url.name:
                    profile_picture = selected_user.full_profile.picture_url.url
                else:
                    profile_picture = selected_user.full_profile.linkedin_picture_url

                if "entrepreneur" in selected_user.users.member_type:
                    if request.LANGUAGE_CODE == "tr":
                        position_title = "Girişimci"
                    else:
                        position_title = selected_user.users.member_type
                else:

                    if len(selected_user.full_profile.position_title) > 10:
                        position_title = selected_user.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(selected_user).industries.description
                        # position_title = selected_user.usersındustries_set.get().industries.description

                profile_name = get_person_profile_name(users_fullprofile.id,selected_user.id)

                profile_location = get_fulllocation_string(selected_user.full_profile.fullprofilelocation_set.get().location.district,selected_user.full_profile.fullprofilelocation_set.get().location.city) + " - " + selected_user.full_profile.fullprofilelocation_set.get().location.country

                item = {
                    "profile_image": profile_picture,
                    "profile_name": profile_name,
                    "position_title": position_title,
                    "id": selected_user.id,
                    "profile_location": profile_location,
                    "profile_point":UsersPointScores.calculate_points(selected_user),
                }

                context_users.append(item)

                context={
                    "users":list(context_users),
                    "message_type":"success",
                    "message":"",
                }
            else:
                if UsersMembershipTermsType.isOver_limitsize(users_fullprofile, "send-message",users_fullprofile.users.users_membershiptypes.name_code):
                    # limit aşılmış mesajı

                    if users_fullprofile.users.users_membershiptypes.name_code == "beginner":

                        if request.LANGUAGE_CODE == "tr":
                            message = "Bulunduğunuz üyelik paketinde konumunuz dışından birine özel mesaj gönderemezsiniz."
                        else:
                            message = "You can't send any private message outside your location. You should benefit our paid membership packages."
                    else:
                        if request.LANGUAGE_CODE == "tr":
                            message = "Bulunduğunuz üyelik paketinde özel mesaj gönderme limitine ulaştınız. Bir sonraki ay mevcut paketinizde mesaj limiti sıfırlanacaktır."
                        else:
                            message = "You've reached the limit of sending messages to someone outside your location. You should upgrade your membership or you can wait a month."

                    context={
                            "message_type":"error",
                            "message":message,
                            "users":list(context_users),
                        }
                else:

                    if not "default" in selected_user.full_profile.picture_url.name:
                        profile_picture = selected_user.full_profile.picture_url.url
                    else:
                        profile_picture = selected_user.full_profile.linkedin_picture_url

                    if "entrepreneur" in selected_user.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = selected_user.users.member_type
                    else:

                        if len(selected_user.full_profile.position_title) > 10:
                            position_title = selected_user.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(selected_user).industries.description
                            # position_title = selected_user.usersındustries_set.get().industries.description

                    profile_name = get_person_profile_name(users_fullprofile.id, selected_user.id)

                    profile_location = get_fulllocation_string(
                        selected_user.full_profile.fullprofilelocation_set.get().location.district,
                        selected_user.full_profile.fullprofilelocation_set.get().location.city) + " - " + selected_user.full_profile.fullprofilelocation_set.get().location.country

                    item = {
                        "profile_image": profile_picture,
                        "profile_name": profile_name,
                        "position_title": position_title,
                        "id": selected_user.id,
                        "profile_location": profile_location,
                        "profile_point": UsersPointScores.calculate_points(selected_user),
                    }

                    context_users.append(item)
                    context = {
                        "users": list(context_users),
                        "message_type": "success",
                        "message": "",
                    }

            return HttpResponse(json.dumps(context),content_type="application/json")

@never_cache
@csrf_protect
def send_message_to_person(request):
    if "user_fullprofile" in request.session:
        if request.method == "POST":
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            if request.is_secure():
                host_address = "https://" + request.get_host() + "/"
            else:
                host_address = "http://" + request.get_host() + "/"

            users_id = request.POST.get("id")
            body_text = request.POST.get("body_text")

            selected_user_projefile = UsersFullProfile.select_user(users_id)
            # burda 2 durum söz konusu: konum içi mesajları ve arkadaşlarımıza mesajı limitsiz atacağız
            # konumdışı arkadaşımız olmayanlara atamayacağız
            if is_it_same_location(users_fullprofile.id,selected_user_projefile.id) or UsersFriend.isThis_my_friend(users_fullprofile, selected_user_projefile):

                # eğer aynı lokasyondaysak
                try:
                    if len(body_text) > 0:
                        created_message = Messages.new_message(UsersFullProfile.select_user(users_id), "", body_text)
                        UsersMessages.new_message(users_fullprofile, created_message, None)

                        new_message_notification(users_fullprofile.id,created_message.to_users_fullprofiles.id,request.LANGUAGE_CODE,host_address)

                        context = {
                            "message_type": "success",
                            "message": ugettext("Your message has been successfully sent.")
                        }
                    else:
                        context = {
                            "message_type": "error",
                            "message": ugettext("You should enter a content.")
                        }
                except:
                    context = {
                        "message_type": "error",
                        "message": ugettext(
                            "An error was encountered while sending the message. Try again later, please.")
                    }

            else:
                # konum dışı ve arkadaşım değilse - bu sefer mesaj gönderme limitine bakıyorum
                if UsersMembershipTermsType.isOver_limitsize(users_fullprofile, "send-message",users_fullprofile.users.users_membershiptypes.name_code):
                    # limiti aştım
                    context = {
                        "message_type": "error",
                        "message": ugettext(
                            "You have reached the limit of sending messages to someone outside your location. You should upgrade your membership, if package is not enough.")
                    }

                else:
                    # limitim var
                    if len(body_text) > 0:
                        created_message = Messages.new_message(UsersFullProfile.select_user(users_id), "", body_text)
                        UsersMessages.new_message(users_fullprofile, created_message, None)

                        UsersMembershipTermsType.create(users_fullprofile, "send-message",users_fullprofile.users.users_membershiptypes.name_code)

                        new_message_notification(users_fullprofile.id, created_message.to_users_fullprofiles.id,request.LANGUAGE_CODE, host_address)

                        context = {
                            "message_type": "success",
                            "message": ugettext("Your message has been successfully sent.")
                        }
                    else:
                        context = {
                            "message_type": "error",
                            "message": ugettext("You should enter a content.")
                        }

            # mail uyarısı eklenecek
            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_reply_message(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":

            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            reply_content = request.POST.get("body_text")
            message_id = request.POST.get("message")

            if request.is_secure():
                host_address = "https://" + request.get_host() + "/"
            else:
                host_address = "http://" + request.get_host() + "/"

            parent_message_title = "re: " + str(remove_html_tags(Messages.select_message(message_id).body_text))[:8] + "..."

            # to_users = Messages.select_message(message_id).fk_messages.get().users_fullprofiles
            if Messages.select_message(message_id).fk_messages.get().parent_message is not None:
                # cevap verildiyse hiçbir kullanıcı için mesaj limiti arttırılmayacak
                to_users = Messages.select_message(message_id).fk_messages.get().users_fullprofiles

                # print("cevap atarken : ", to_users.id)
            else:
                # daha önceden gönderilen mesaja cevap gönderilmediyse ve ben tekrar mesajı devam
                # ettiriyorsam
                # bu kısımda kullanıcı cevapsız olan mesajı devam ettiriyorsa Özel mesaj limiti arttırılacak
                if users_fullprofiles.id == Messages.select_message(message_id).fk_messages.get().users_fullprofiles.id:
                    to_users = Messages.select_message(message_id).to_users_fullprofiles
                else:
                    to_users = Messages.select_message(message_id).fk_messages.get().users_fullprofiles
                # to_users = Messages.select_message(message_id).to_users_fullprofiles
                UsersMembershipTermsType.create(users_fullprofiles, "send-message",users_fullprofiles.users.users_membershiptypes.name_code)



            try:
                if Messages.select_message(message_id).fk_messages.get().parent_message is not None:
                    parent_message = Messages.select_message(message_id).fk_messages.get().parent_message
                else:
                    # parent_message = None
                    parent_message=Messages.select_message(message_id)

                created_message = Messages.new_message(to_users, parent_message_title, reply_content)
                sended_message = UsersMessages.new_message(users_fullprofiles, created_message, parent_message)

                profile_name = get_profile_name_str(sended_message.users_fullprofiles.full_profile.last_name,
                                                    sended_message.users_fullprofiles.full_profile.first_name)

                if not "default" in sended_message.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = sended_message.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = sended_message.users_fullprofiles.full_profile.linkedin_picture_url

                new_message_notification(users_fullprofiles.id, to_users.id,request.LANGUAGE_CODE, host_address)

                context = {
                    "message_type": "success",
                    "message": ugettext("Your message successfully sent."),
                    "message_content":sended_message.messages.body_text,
                    "profile_name": profile_name,
                    "profile_image": profile_picture,
                    "sended_date": timesince(sended_message.messages.send_date),
                }
            except:
                context = {
                    "message_type": "error",
                    "message": ugettext("An error was encountered while sending the message. Please, try again later."),
                }
            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_archive(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            messages_list = request.POST.getlist("messages[]")
            error_counter = 0
            for item in messages_list:
                # print(Messages.select_message(item))

                archived_message = Messages.save_to_archive(item, users_fullprofiles)
                # archived_message=UsersMessages.save_to_archive(Messages.select_message(item),users_fullprofiles)
                if archived_message is not True:
                    error_counter += 1

            if error_counter > 0:
                message = ugettext("An error occurred during the process.")
                message_type = "error"
            else:
                message = ugettext("Your messages have been successfully archived.")
                message_type = "success"

            context = {
                "message_type": message_type,
                "message": message
            }
            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_trash(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            messages_list = request.POST.getlist("messages[]")
            error_counter = 0
            for item in messages_list:
                # print(Messages.select_message(item))

                trashed_message = Messages.save_to_trash(item, users_fullprofiles)
                # archived_message=UsersMessages.save_to_archive(Messages.select_message(item),users_fullprofiles)
                if trashed_message is not True:
                    error_counter += 1

            if error_counter > 0:
                message = ugettext("An error occurred during the process.")
                message_type = "error"
            else:
                message = ugettext("Your messages have been successfully trashed.")
                message_type = "success"

            context = {
                "message_type": message_type,
                "message": message
            }
            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_delete(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            messages_list = request.POST.getlist("messages[]")
            error_counter = 0
            for item in messages_list:
                # print(Messages.select_message(item))

                deleted_message = Messages.save_to_delete(item, users_fullprofiles)
                # archived_message=UsersMessages.save_to_archive(Messages.select_message(item),users_fullprofiles)
                if deleted_message is not True:
                    error_counter += 1

            if error_counter > 0:
                message = ugettext("An error occurred during the process.")
                message_type = "error"
            else:
                message = ugettext("Your messages have been successfully deleted.")
                message_type = "success"

            context = {
                "message_type": message_type,
                "message": message
            }
            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def notification_count(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            context={
                "messages_count":Messages.unread_messages_count(users_fullprofiles),
            }
            return HttpResponse(json.dumps(context),content_type="application/json")

@never_cache
@csrf_protect
def notification_list(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            page=1
            paginator = Paginator(Messages.notification_messages(users_fullprofiles), 5)

            try:
                messages_list = paginator.page(page)
            except PageNotAnInteger:
                messages_list = paginator.page(1)
            except EmptyPage:
                messages_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "messages": list(messages_list),
                "has_next": messages_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(context),content_type="application/json")

@never_cache
@csrf_protect
def loadmore_notification_list(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            page=request.GET.get("page")
            paginator = Paginator(Messages.notification_messages(users_fullprofiles), 5)

            try:
                messages_list = paginator.page(page)
            except PageNotAnInteger:
                messages_list = paginator.page(2)
            except EmptyPage:
                messages_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            context = {
                "messages": list(messages_list),
                "has_next": messages_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(context),content_type="application/json")

# @csrf_protect
# def send_dm_message(request):
#     if 'user_fullprofile' in request.session:
#         if request.method == "POST":
#             users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
#             users_profiles_id=request.POST.get("id")
#
#
#             selected_users=UsersFullProfile.select_user(users_profiles_id)
#
#             # önce arkadaş mıyız bu kişiyle ona bakalım
#             # sonra bizim lokasyonu muzda mi?
#             # bunların ikiside değilse mesaj gönderme limitimiz var mı?
#
#             if change_characters(users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) == change_characters(selected_users.full_profile.fullprofilelocation_set.get().location.city) or UsersFriend.isThis_my_friend(users_fullprofiles,selected_users):
#                 # aynı lokasyondayız veya arkadaşım ise
#
#                 pass
#             else:
#                 # mesaj gönderme limitimiz var mı
#                 if UsersMembershipTermsType.isOver_limitsize(users_fullprofiles,"send-message",users_fullprofiles.users.users_membershiptypes.name_code) is False:
#                     # gönderebilirim
#                     pass
#                 else:
#                     # gönderemiyorum
#                     pass
#
#
#     pass