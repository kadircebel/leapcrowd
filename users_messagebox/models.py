from django.db import models


class Messages(models.Model):
    to_users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    title = models.CharField(max_length=500, blank=True)
    body_text = models.TextField(blank=True)
    send_date = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    read_date = models.DateTimeField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_archive = models.BooleanField(default=False)
    is_trash = models.BooleanField(default=False)
    is_delete = models.BooleanField(default=False)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'messages'

    @classmethod
    def notification_messages(cls,to_users_fullprofiles):
        from utils.views import get_profile_name_str, remove_html_tags
        from django.utils.timesince import timesince
        context_messages=[]
        for msg in cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles,is_active=True,is_archive=False,is_trash=False,is_delete=False).all().order_by("-id"):
            # msg.fk_messages.get().users_fullprofiles
            if not msg.fk_messages.exists():
                if len(msg.body_text) > 30:
                    message_content = str(remove_html_tags(msg.body_text))[:30] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                item = {
                    "profile_image": "/media/leapcrowd/profile.jpg",
                    "is_read": msg.is_read,
                    "sender_name": "Leapcrowd",
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                }
            else:

                if not "default" in msg.fk_messages.get().users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = msg.fk_messages.get().users_fullprofiles.full_profile.picture_url.url

                else:
                    profile_picture = msg.fk_messages.get().users_fullprofiles.full_profile.linkedin_picture_url

                sender_name = get_profile_name_str(msg.fk_messages.get().users_fullprofiles.full_profile.last_name,msg.fk_messages.get().users_fullprofiles.full_profile.first_name)

                if len(msg.body_text) > 30:
                    message_content = str(remove_html_tags(msg.body_text))[:30] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                item = {
                    "profile_image": profile_picture,
                    "is_read": msg.is_read,
                    "sender_name": sender_name,
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                }
            context_messages.append(item)

        return context_messages

    @classmethod
    def unread_messages_count(cls,to_users_fullprofiles):
        count=0
        try:
            for msg in cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles,is_active=True,is_delete=False,is_trash=False,is_archive=False).all():
                if msg.is_read is False:
                    count+=1

            return count
        except:
            return count

    @classmethod
    def save_to_archive(cls, id, users_fullprofiles):
        try:
            if cls.objects.filter(id=id).get().to_users_fullprofiles == users_fullprofiles:
                if cls.objects.filter(id=id).get().is_archive == False:
                    selected = cls.objects.filter(id=id).get()
                    selected.is_archive = True
                    selected.save()
                    return True
            else:
                return False
        except:
            return False

    @classmethod
    def save_to_trash(cls, id, users_fullprofiles):
        try:
            if cls.objects.filter(id=id).get().to_users_fullprofiles == users_fullprofiles:
                if cls.objects.filter(id=id).get().is_trash == False:
                    selected = cls.objects.filter(id=id).get()
                    selected.is_trash = True
                    selected.save()
                    return True
            else:
                return False
        except:
            return False

    @classmethod
    def save_to_delete(cls,id,users_fullprofiles):
        try:
            if cls.objects.filter(id=id).get().to_users_fullprofiles == users_fullprofiles:
                if cls.objects.filter(id=id).get().is_delete == False:
                    selected = cls.objects.filter(id=id).get()
                    selected.is_delete = True
                    selected.save()
                    return True
            else:
                return False
        except:
            return False

    @classmethod
    def read_this_message(cls, id, users_fullprofiles):
        from datetime import datetime
        try:
            if cls.objects.filter(id=id).get().to_users_fullprofiles == users_fullprofiles:
                if cls.objects.filter(id=id).get().is_read == False:
                    selected = cls.objects.filter(id=id, to_users_fullprofiles=users_fullprofiles).get()
                    selected.is_read = True
                    selected.read_date = datetime.now()
                    selected.save()
                    return selected
        except:
            pass


    @classmethod
    def select_message(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def new_message(cls, to_users_fullprofiles, title, body_text):
        new_record = cls(to_users_fullprofiles=to_users_fullprofiles, title=title, body_text=body_text)
        new_record.save()
        return new_record

    @classmethod
    def archived_messages_list(cls, is_active, to_users_fullprofiles, language):
        from persons.models import UsersFriend
        from utils.views import get_profile_name_str, remove_html_tags,get_person_profile_name
        from django.utils.timesince import timesince
        message_list = cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, is_active=is_active,
                                          is_archive=True, is_trash=False,is_delete=False).all().order_by("-id")

        context_messages = []
        for msg in message_list:

            if not msg.fk_messages.exists():
                if len(msg.body_text) > 30:
                    message_content = str(remove_html_tags(msg.body_text))[:30] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                if msg.is_read is False:
                    is_readed = "active"
                else:
                    is_readed = ""

                item = {
                    "profile_image": "/media/leapcrowd/profile.jpg",
                    "is_network": "kiyo",
                    "is_readed": is_readed,
                    "sender_name": "Leapcrowd",
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                    "title": msg.title.capitalize(),
                }
                context_messages.append(item)

            else:

                if not "default" in Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.linkedin_picture_url


                if UsersFriend.isThis_my_friend(msg.to_users_fullprofiles, Messages.select_message(msg.id).fk_messages.get().users_fullprofiles) :
                    is_my_network = True
                else:
                    is_my_network = False

                sender_name = get_person_profile_name(Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.id,Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.id)

                if msg.is_read is False:
                    is_readed = "active"
                else:
                    is_readed = ""

                if len(msg.title) > 0:
                    title = msg.title
                else:
                    if language == "tr":
                        title = "yeni mesaj: " + str(remove_html_tags(msg.body_text))[:15] + "..."
                    else:

                        title = "new message: " + str(remove_html_tags(msg.body_text))[:15] + "..."

                # bu kısımda eğer benim network'ümün dışında ise ismini gizli olarak göstereceğim
                if len(msg.body_text) > 100:
                    message_content = str(remove_html_tags(msg.body_text))[:150] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                item = {
                    "profile_image": profile_picture,
                    "is_network": is_my_network,
                    "is_readed": is_readed,
                    "sender_name": sender_name,
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                    "title": title,
                }
                context_messages.append(item)

        return context_messages

    @classmethod
    def trash_messages_list(cls, is_active, to_users_fullprofiles, language):
        from persons.models import UsersFriend
        from utils.views import get_profile_name_str, remove_html_tags,get_person_profile_name
        from django.utils.timesince import timesince
        message_list = cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, is_active=is_active,
                                          is_trash=True,is_delete=False).all().order_by("-id")

        context_messages = []
        for msg in message_list:

            if not msg.fk_messages.exists():
                if len(msg.body_text) > 30:
                    message_content = str(remove_html_tags(msg.body_text))[:30] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                if msg.is_read is False:
                    is_readed = "active"
                else:
                    is_readed = ""

                item = {
                    "profile_image": "/media/leapcrowd/profile.jpg",
                    "is_network": "kiyo",
                    "is_readed": is_readed,
                    "sender_name": "Leapcrowd",
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                    "title": msg.title.capitalize(),
                }
                context_messages.append(item)

            else:

                if not "default" in Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.linkedin_picture_url

                # mesajı gönderen benim arkadaşım mı?
                if UsersFriend.isThis_my_friend(msg.to_users_fullprofiles, Messages.select_message(msg.id).fk_messages.get().users_fullprofiles):
                    is_my_network = True
                    sender_name = get_profile_name_str(Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.last_name,Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.full_profile.first_name)
                else:
                    is_my_network = False
                    sender_name=get_person_profile_name(Messages.select_message(msg.id).fk_messages.get().users_fullprofiles.id,msg.fk_messages.get().users_fullprofiles.id)



                if msg.is_read is False:
                    is_readed = "active"
                else:
                    is_readed = ""

                if len(msg.title) > 0:
                    title = msg.title
                else:
                    if language == "tr":
                        title = "yeni mesaj: " + str(remove_html_tags(msg.body_text))[:15] + "..."
                    else:

                        title = "new message: " + str(remove_html_tags(msg.body_text))[:15] + "..."

                # bu kısımda eğer benim network'ümün dışında ise ismini gizli olarak göstereceğim
                if len(msg.body_text) > 100:
                    message_content = str(remove_html_tags(msg.body_text))[:150] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                item = {
                    "profile_image": profile_picture,
                    "is_network": is_my_network,
                    "is_readed": is_readed,
                    "sender_name": sender_name,
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                    "title": title,
                }
                context_messages.append(item)

        return context_messages

    @classmethod
    def inbox_messages_list(cls, is_active, to_users_fullprofiles, language):
        from persons.models import UsersFriend
        from utils.views import get_profile_name_str, remove_html_tags,get_person_profile_name
        from django.utils.timesince import timesince
        message_list = cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles,is_active=is_active, is_archive=False, is_trash=False,is_delete=False).all().order_by("-id")
        context_messages = []
        for msg in message_list:
            if not msg.fk_messages.exists():
                if len(msg.body_text) > 30:
                    message_content = str(remove_html_tags(msg.body_text))[:30] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                if msg.is_read is False:
                    is_readed = "active"
                else:
                    is_readed = ""

                item = {
                    "profile_image": "/media/leapcrowd/profile.jpg",
                    "is_readed": is_readed,
                    "is_network": "kiyo",
                    "title":msg.title.capitalize(),
                    "sender_name": "Leapcrowd",
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                }

            else:

                if not "default" in msg.fk_messages.get().users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = msg.fk_messages.get().users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = msg.fk_messages.get().users_fullprofiles.full_profile.linkedin_picture_url

                # mesajı gönderen benim arkadaşım mı?
                if UsersFriend.isThis_my_friend(to_users_fullprofiles, msg.fk_messages.get().users_fullprofiles):
                    is_my_network = True
                else:
                    is_my_network = False

                sender_name = get_profile_name_str(msg.fk_messages.get().users_fullprofiles.full_profile.last_name,msg.fk_messages.get().users_fullprofiles.full_profile.first_name)


                if msg.is_read is False:
                    is_readed = "active"
                else:
                    is_readed = ""

                if len(msg.title) > 0:
                    title = msg.title
                else:
                    if language == "tr":
                        title = "yeni mesaj: " + str(remove_html_tags(msg.body_text))[:15] + "..."
                    else:

                        title = "new message: " + str(remove_html_tags(msg.body_text))[:15] + "..."

                # bu kısımda eğer benim network'ümün dışında ise ismini gizli olarak göstereceğim
                if len(msg.body_text) > 100:
                    message_content = str(remove_html_tags(msg.body_text))[:150] + "..."
                else:
                    message_content = remove_html_tags(msg.body_text)

                item = {
                    "profile_image": profile_picture,
                    "is_network": is_my_network,
                    "is_readed": is_readed,
                    "sender_name": sender_name,
                    "message_content": message_content,
                    "sended_date": timesince(msg.send_date),
                    "id": msg.id,
                    "title": title,
                }
            context_messages.append(item)

        return context_messages


class ReplyMessages(models.Model):
    main_users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    title = models.CharField(max_length=500, blank=True)
    body_text = models.TextField(blank=True)
    send_date = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    read_date = models.DateTimeField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    messages = models.ForeignKey(Messages, models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'reply_messages'




    @classmethod
    def select_message_will_reply(cls, message, users_fullprofiles):
        from persons.models import UsersMessages
        try:
            if UsersMessages.is_there_active_message(message, users_fullprofiles):
                return UsersMessages.select_coming_message(message, users_fullprofiles)
            else:
                pass
        except:
            pass
