from django.urls import path
from .views import *


app_name="persons"

urlpatterns=[
    path('users-popup/', popup_window, name='userspopup_summary'),
    path('edit/',edit,name='edit'),
    path('edit/<int:id>/',edit,name='edit'),
    path('general-info/', general_informations, name='general-info'),
    path('user-points/', get_userpoints, name='points'),
    path('profile-points/', get_profilepoints, name='profile_points'),
    path('profile-experiences/', get_profile_experiences, name='profile_experiences'),
    path('profile-investments/', get_profile_investments, name='profile_investments'),
    path('membership-types/', profile_membership_types, name='membership_types'),
    path('select-membertype/', member_type, name='member_type'),
    path('selected-membertype/', selected_member_type, name='selected_member_type'),
    path('delete-avatar/', delete_avatar, name='delete_avatar'),

    path('upload-image/', upload_profile_img, name='upload-image'),
    path('save-experiences/', save_experiences, name='save-experiences'),
    path('all-experiences/', show_experiences, name='all-experiences'),
    path('delete-experience/', delete_experience, name='delete-experience'),
    path('edit-experience/', edit_experience, name='edit-experience'),
    path('update-experience/', update_experience, name='update-experience'),
    path('save-skills/', save_skills, name='save-skills'),
    path('get-skills/', get_profile_skills, name='get-skills'),
    path('remove-skill/', remove_skill, name='remove-skill'),
    path('save-basics/', save_basic_informations, name='save-basics'),
    path('selected-invest/', selected_investmentType, name='selected-investmentType'),
    path('selected-hours/', selectedHours, name='selected-hours'),
    path('selected-hourcost/', selected_hourcost, name='selected-hourcost'),
    path('selected-history/', selected_startupHistory, name='selected-startupHistory'),
    path('position-title/', position_title, name='position_title'),
    path('more-person/', lazy_load_profiles, name='lazy_load_persons'),
    path('filter-profiles/', filter_profiles, name='filter_profiles'),
    path('dm-teamrequest/', dm_request_person, name='dm_teamrequest'),
    path('dm-projects/', request_persons_projects, name='dm_projects'),
    path('dm-requested-person/', dm_requested_person_info, name='dm_requested_person'),
    path('dm-send-request/', dm_send_team_request, name='dm_send_request'),
    path('requested-dm-project/', request_dm_project, name='request_dm_project'),
    path('dm-support-type/', dm_project_supports, name='dm_support_type'),
    path('save-projecthours/', save_projecthours, name='dm_save_projecthours'),
    path('save-investments/', save_investments, name='dm_save_investments'),
    path('send-answert-dm/', send_asnwer_dm_teamrequest, name='send_answer_dm'),
    path('dmmessage-person-info/', dmmessage_person_info, name='dmmessage_person_info'),

    path('logout/', logout, name='logout'),

    path('',index,name='persons'),
    path('<slug:members>/',index,name='persons'), # default olarak girişimcileri alıyor

    path('details/<int:id>/',details,name='details'),
    path('projects/<int:id>/',projects,name='projects'),

    path('cofunded-projects/<int:id>/', cofunded_projects, name='cofunded_projects'),

]