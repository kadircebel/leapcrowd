from django.contrib import admin
from .models import UsersFullProfile,Users

# Register your models here.

class UsersAdmin(admin.ModelAdmin):
    list_display = ["email_address","member_type","joined_date","is_active"]

    class Meta:
        model=Users

admin.site.register(Users,UsersAdmin)



class UsersFullprofilesAdmin(admin.ModelAdmin):
    list_display = ["user_fullname","email_address","member_type","membership_type","location_info","users_is_active"]




    def user_fullname(self,instance):
        return instance.full_profile.first_name + " " + instance.full_profile.last_name

    def email_address(self,instance):
        return instance.users.email_address

    def member_type(self,instance):
        return instance.users.member_type

    def membership_type(self,instance):
        return instance.users.users_membershiptypes.name_code

    def location_info(self,instance):
        return instance.full_profile.fullprofilelocation_set.get().location.city + " - " + instance.full_profile.fullprofilelocation_set.get().location.country

    def users_is_active(self,instance):
        return instance.users.is_active



    class Meta:
        model=UsersFullProfile


admin.site.register(UsersFullProfile,UsersFullprofilesAdmin)