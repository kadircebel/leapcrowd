from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.views.decorators.csrf import csrf_protect
from django.conf import settings
from django.http import JsonResponse
from .models import UsersFullProfile, UsersPointScores, UsersProjectHours, UsersCompanyInfos, UsersSkills, \
    UsersInvestments, UsersHourCosts, UsersStartupHistory, UsersProjects, UsersTeams, UsersDmProjectsTeamrequests, \
    Teamrequests, UsersRequestProjectHours, UsersRequestInvestments, UsersProjectsTeamrequests, \
    UsersMembershipTermsType, UsersIndustries, UsersFriend,Users
from notifications.models import NotificationsUsersTeams
from memberships.models import MembershipTermsType
from teams.models import Teams
from myprofile.models import FullprofileLocation, Locations, FullProfile
from django.utils.translation import ugettext, ngettext
from mainadmins.models import PointScores, ImageUploadSize, HourCosts, Investments, ProjectHours, StartupHistory, \
    JobTitles, ProjectOptions
from utils.views import user_location, content_file_name, control_file_extention, check_file_size, get_active_avatar, \
    get_fulllocation_string, get_profile_name_str, clean_hours_title, change_characters, dm_request_teamrequest_mail, \
    dm_response_to_owner_mail, get_person_profile_name, delete_project_logo, is_it_same_location, \
    send_signals_to_projects_owner
from linkedin_infos.models import CompanyInfos, Skills
from projects.models import ProjectPointsScores, ProjectsProjectOptions, ProjectsSteps
import json, requests,datetime
# from datetime import datetime
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.decorators.cache import never_cache, cache_page

@never_cache
def member_type(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            selected_user = UsersFullProfile.select_user(request.session["user_fullprofile"])
            today_date = datetime.date.today()
            result_date = (today_date - selected_user.users.joined_date.date()).days
            if result_date <= 30:
                if request.LANGUAGE_CODE == "tr":
                    message = "Üyelik tipinizi ayda 1 defa değiştirebilirsiniz."
                else:
                    message = "You can change your member type once a month."
            else:
                message=""

            context={
                "message":message
            }

            return HttpResponse(json.dumps(context),content_type="application/json")

@never_cache
def selected_member_type(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            context={
                "member_type":UsersFullProfile.select_user(request.session["user_fullprofile"]).users.member_type,
            }
            return HttpResponse(json.dumps(context),content_type="application/json")


# Create your views here.
@cache_page(60 * 10)
def index(request, members="entrepreneur"):
    if 'user_fullprofile' in request.session:
        context_profiles = []
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        counter_member_list = 0
        member_list = UsersFullProfile.objects.filter(users__member_type=members, users__is_active=True).all().order_by(
            "-id")

        for profile in member_list:

            if users_fullprofiles.id != profile.id:

                if counter_member_list < 10:
                    if not "default" in profile.full_profile.picture_url.name:
                        profile_picture = profile.full_profile.picture_url.url
                    else:
                        profile_picture = profile.full_profile.linkedin_picture_url

                    profile_location = get_fulllocation_string(
                        profile.full_profile.fullprofilelocation_set.get().location.district,
                        profile.full_profile.fullprofilelocation_set.get().location.city) + " - " + profile.full_profile.fullprofilelocation_set.get().location.country

                    profile_points = UsersPointScores.calculate_points(profile)
                    member_type = profile.users.member_type
                    # profile_name = get_profile_name_str(profile.full_profile.last_name, profile.full_profile.first_name)
                    profile_name = get_person_profile_name(users_fullprofiles.id, profile.id)

                    if "entrepreneur" in profile.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = profile.users.member_type
                    else:
                        if len(profile.full_profile.position_title) > 0:
                            position_title = profile.full_profile.position_title
                        else:
                            position_title = UsersIndustries.select(profile).industries.description
                            # position_title = profile.usersındustries_set.get().industries.description

                    item = {
                        "profile_picture": profile_picture,
                        "profile_location": profile_location,
                        "profile_point": profile_points,
                        "member_type": member_type,
                        "profile_name": profile_name,
                        "position_title": position_title,
                        "id": profile.id,
                    }

                    context_profiles.append(item)
                    counter_member_list += 1

        if len(member_list) > 10:
            has_next = True
        else:
            has_next = False

        context = {
            "profiles": context_profiles,
            "member_type": members,
            "has_next": has_next,
            "page_title": ugettext("Profiles"),
        }

        return render(request, 'socialside/persons/index.html', context)

    else:
        return HttpResponseRedirect("/login/")


@cache_page(60 * 10)
@csrf_protect
def filter_profiles(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "POST":
            page = request.POST.get("page")
            profile_type = request.POST.get("filter_option")
            support_type = request.POST.get("investment_option")
            context_profiles = []
            if len(profile_type) < 1:
                profile_type = "entrepreneur"

            members_list = UsersFullProfile.objects.filter(users__member_type=profile_type,
                                                           users__is_active=True).all().order_by("-id")

            for profile in members_list:
                if users_fullprofiles.id != profile.id:
                    if not "default" in profile.full_profile.picture_url.name:
                        profile_picture = profile.full_profile.picture_url.url
                    else:
                        profile_picture = profile.full_profile.linkedin_picture_url

                    profile_points = UsersPointScores.calculate_points(profile)
                    member_type = profile.users.member_type
                    # profile_name = get_profile_name_str(profile.full_profile.last_name, profile.full_profile.first_name)

                    profile_name = get_person_profile_name(users_fullprofiles.id, profile.id)

                    if "entrepreneur" in profile.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = profile.users.member_type
                    else:
                        if len(profile.full_profile.position_title) > 0:
                            position_title = profile.full_profile.position_title
                        else:
                            position_title = UsersIndustries.select(profile).industries.description
                            # position_title = profile.usersındustries_set.get().industries.description

                    profile_location = get_fulllocation_string(
                        profile.full_profile.fullprofilelocation_set.get().location.district,
                        profile.full_profile.fullprofilelocation_set.get().location.city) + " - " + profile.full_profile.fullprofilelocation_set.get().location.country

                    if len(support_type) > 0 and "0" not in support_type and UsersInvestments._isInvestmentsClear(
                            profile):
                        if UsersInvestments.select_users_investment(
                                profile).investments.title_code in Investments.select_invest_type(
                                support_type).title_code:
                            item = {
                                "profile_picture": profile_picture,
                                "profile_location": profile_location,
                                "profile_point": profile_points,
                                "member_type": member_type,
                                "profile_name": profile_name,
                                "position_title": position_title,
                                "id": profile.id,
                            }

                            context_profiles.append(item)
                    else:
                        item = {
                            "profile_picture": profile_picture,
                            "profile_location": profile_location,
                            "profile_point": profile_points,
                            "member_type": member_type,
                            "profile_name": profile_name,
                            "position_title": position_title,
                            "id": profile.id,
                        }

                        context_profiles.append(item)

            paginator = Paginator(context_profiles, 10)

            try:
                profile_list = paginator.page(page)
            except PageNotAnInteger:
                profile_list = paginator.page(1)
            except EmptyPage:
                profile_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "profiles": list(profile_list),
                "has_next": profile_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")


@cache_page(60 * 10)
@csrf_protect
def lazy_load_profiles(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "POST":
            page = request.POST.get("page")
            profile_type = request.POST.get("filter_option")
            support_type = request.POST.get("investment_option")
            context_profiles = []

            if len(profile_type) < 1:
                profile_type = "entrepreneur"

            members_list = UsersFullProfile.objects.filter(users__is_active=True,
                                                           users__member_type=profile_type).all().order_by("-id")

            for profile in members_list:

                if users_fullprofiles.id != profile.id:

                    if not "default" in profile.full_profile.picture_url.name:
                        profile_picture = profile.full_profile.picture_url.url
                    else:
                        profile_picture = profile.full_profile.linkedin_picture_url

                    profile_points = UsersPointScores.calculate_points(profile)
                    member_type = profile.users.member_type

                    profile_name = get_person_profile_name(users_fullprofiles.id, profile.id)

                    if "entrepreneur" in profile.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = profile.users.member_type
                    else:
                        if len(profile.full_profile.position_title) > 0:
                            position_title = profile.full_profile.position_title
                        else:
                            position_title = UsersIndustries.select(profile).industries.description
                            # position_title = profile.usersındustries_set.get().industries.description

                    profile_location = get_fulllocation_string(
                        profile.full_profile.fullprofilelocation_set.get().location.district,
                        profile.full_profile.fullprofilelocation_set.get().location.city) + " - " + profile.full_profile.fullprofilelocation_set.get().location.country

                    if len(support_type) > 0 and "0" not in support_type:
                        if UsersInvestments.select_users_investment(
                                profile).investments.title_code in Investments.select_invest_type(
                            support_type).title_code:
                            item = {
                                "profile_picture": profile_picture,
                                "profile_location": profile_location,
                                "profile_point": profile_points,
                                "member_type": member_type,
                                "profile_name": profile_name,
                                "position_title": position_title,
                                "id": profile.id,
                            }

                            context_profiles.append(item)
                    else:
                        item = {
                            "profile_picture": profile_picture,
                            "profile_location": profile_location,
                            "profile_point": profile_points,
                            "member_type": member_type,
                            "profile_name": profile_name,
                            "position_title": position_title,
                            "id": profile.id,
                        }

                        context_profiles.append(item)

            paginator = Paginator(context_profiles, 10)
            # print(context_profiles)
            try:
                profile_list = paginator.page(page)
            except PageNotAnInteger:
                profile_list = paginator.page(1)
            except EmptyPage:
                profile_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "profiles": list(profile_list),
                "has_next": profile_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")


# @cache_page(60 * 10)
@never_cache
@csrf_protect
def popup_window(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            user_id = request.GET.get("id")
            selected_user = UsersFullProfile.select_user(user_id)
            context_companies = []
            context_skills = []
            context_projects = []
            context_memberships = []

            if UsersCompanyInfos.isThereExperience(selected_user):
                for company in UsersCompanyInfos.select_all(selected_user).order_by("-id")[:1]:
                    item_companies = {"position_title": company.company_infos.title,
                                      "company_name": company.company_infos.name}
                    context_companies.append(item_companies)
            else:
                item_companies = {"position_title": "", "company_name": ugettext("There is no experience yet.")}
                context_companies.append(item_companies)

            if UsersSkills.count_skills(selected_user) > 0:
                for skll in UsersSkills.select_all(selected_user).order_by("-id")[:5]:
                    item_skills = {"title": skll.skills.title}
                    context_skills.append(item_skills)
            else:
                item_skills = {"title": ugettext("There is no skill yet.")}
                context_skills.append(item_skills)

            if UsersInvestments._isInvestmentsClear(selected_user):
                if request.LANGUAGE_CODE == "tr":
                    invest_type = UsersInvestments.select_users_investment(selected_user).investments.title
                else:
                    invest_type = UsersInvestments.select_users_investment(selected_user).investments.title_eng

                if "paid-support" in UsersInvestments.select_users_investment(selected_user).investments.title_code:
                    if UsersHourCosts._isUsersHourCostsClear(selected_user):
                        hour_cost = UsersHourCosts.select_user_hourcost(
                            selected_user).hour_costs.cost_code + " " + UsersHourCosts.select_user_hourcost(
                            selected_user).hour_cost_amount
                    else:
                        hour_cost = ""

                else:
                    hour_cost = ""
            else:
                invest_type = "-"
                hour_cost = ""
            print("proje var mı : ", UsersProjects.isThereProject(selected_user))
            print("users : ", selected_user)
            if UsersProjects.isThereProject(selected_user):
                print("buraya girdi")
                for prj in UsersProjects.list_all(selected_user).order_by("-id"):
                    if ProjectsProjectOptions.isItOnline(prj.projects, ProjectOptions.select("publish-project")):
                        item_project = {"title": prj.projects.general_infos.title,
                                        "points": ProjectPointsScores.calculate_points(prj.projects),
                                        "project_url": "/dashboard/projects/detail/" + str(prj.projects.id) + "/"}
                        context_projects.append(item_project)

                        break
            else:
                item_project = {"title": ugettext("There is no project yet"), "points": 0,
                                "project_url": "javascript:void(0);"}
                context_projects.append(item_project)

            if not "default" in selected_user.full_profile.picture_url.name:
                profile_picture = selected_user.full_profile.picture_url.url
            else:
                profile_picture = selected_user.full_profile.linkedin_picture_url

            profile_location = get_fulllocation_string(
                selected_user.full_profile.fullprofilelocation_set.get().location.district,
                selected_user.full_profile.fullprofilelocation_set.get().location.city) + " - " + selected_user.full_profile.fullprofilelocation_set.get().location.country
            profile_points = UsersPointScores.calculate_points(selected_user)

            # users membership paketine bakılmalı
            # eğer beginner paket ise profil detayı görüntülenmemeli uyarı çıkartılmalı

            if "entrepreneur" in selected_user.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    position_title = "Girişimci"
                else:
                    position_title = selected_user.users.member_type
            else:
                if len(selected_user.full_profile.position_title) > 0:
                    position_title = selected_user.full_profile.position_title
                else:
                    position_title = UsersIndustries.select(selected_user).industries.description
                    # position_title = selected_user.usersındustries_set.get().industries.description

            if UsersStartupHistory._isStartupHistoryClear(selected_user):
                if request.LANGUAGE_CODE == "tr":
                    startup_history = selected_user.usersstartuphistory_set.get().startup_history.title
                else:
                    startup_history = selected_user.usersstartuphistory_set.get().startup_history.title_eng
            else:
                startup_history = ""

            # print(users_fullprofiles.full_profile.first_name)
            # print("*********")
            # print(users_fullprofiles.users.users_membershiptypes.name_code)

            if "beginner" not in users_fullprofiles.users.users_membershiptypes.name_code:
                item_membership = {
                    "message_type": "success",
                    "url": "/dashboard/profiles/details/" + str(selected_user.id) + "/",
                    "message": "",
                }
                context_memberships.append(item_membership)
            else:
                item_membership = {
                    "message_type": "warning",
                    "url": "",
                    "message": ugettext(
                        "Your membership is not enough for profile detail. You can see only profile summary. There is another way. You can ask your question in your mind and you can get answers or team requests."),
                }
                context_memberships.append(item_membership)

            context = {
                "profile_name": get_person_profile_name(users_fullprofiles.id, selected_user.id),
                "position_title": position_title,
                "profile_picture": profile_picture,
                "profile_points": profile_points,
                "profile_location": profile_location,
                "projects": context_projects,
                "skills": context_skills,
                "invest_type": invest_type,
                "hour_cost": hour_cost,
                "companies": context_companies,
                "startup_history": startup_history,
                "profile_cover": str(selected_user.full_profile.cover_photo),
                "membership_type": context_memberships,
                "profile_id": selected_user.id,
                # "member_type":selected_user.users.member_type,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def edit(request, id=0):
    if 'user_fullprofile' in request.session:
        if id is 0:
            return redirect("/dashboard/profiles/edit/" + str(request.session["user_fullprofile"]) + "/")
        else:
            if not id is request.session["user_fullprofile"]:
                return redirect("/dashboard/profiles/edit/" + str(request.session["user_fullprofile"]) + "/")
            else:
                users_fullprofiles = UsersFullProfile.select_user(id)
                isThereLocation = True
                profile_score = UsersPointScores.calculate_points(users_fullprofiles.id)
                address_text = ""
                location_country = change_characters(
                    users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country)

                if profile_score <= 26:
                    point_score_text = ugettext("NOT COMPLETED")
                    point_score_css = "bg-danger"
                elif profile_score > 26 and profile_score < 100:
                    point_score_text = ugettext("NOT COMPLETED")
                    point_score_css = "bg-warning"
                else:
                    point_score_text = ugettext("COMPLETED")
                    point_score_css = "bg-success"

                if FullprofileLocation._isLocationClear(users_fullprofiles.full_profile) is False:
                    isThereLocation = False
                else:
                    address_text = user_location(users_fullprofiles.id)

                if not "default" in users_fullprofiles.full_profile.picture_url.name:
                    picture_url = users_fullprofiles.full_profile.picture_url.url
                else:
                    picture_url = users_fullprofiles.full_profile.linkedin_picture_url

                if "entrepreneur" in users_fullprofiles.users.member_type and len(
                        users_fullprofiles.full_profile.position_title) < 1:
                    if request.LANGUAGE_CODE == "tr":
                        position_title = "Girişimci"
                    else:
                        position_title = users_fullprofiles.users.member_type
                else:
                    if len(users_fullprofiles.full_profile.position_title) > 0:
                        position_title = users_fullprofiles.full_profile.position_title
                    else:
                        position_title = UsersIndustries.select(users_fullprofiles).industries.description

                context = {
                    'pic_name': users_fullprofiles.full_profile.first_name,
                    'picture': picture_url,
                    'summary': users_fullprofiles.full_profile.summary,
                    'position_title': position_title,
                    'headline': users_fullprofiles.full_profile.head_line,
                    'isThereLocation': isThereLocation,
                    'address_text': address_text,
                    'profile_score': UsersPointScores.calculate_points(users_fullprofiles.id),
                    'point_score_text': point_score_text,
                    'point_score_css': point_score_css,
                    'hour_cost': HourCosts.select(request.LANGUAGE_CODE, location_country).cost_code,
                    'page_title': ugettext("My profile edit"),
                }
                return render(request, 'socialside/persons/edit.html', context)
    else:
        return redirect("/login/")


@cache_page(60 * 5)
def cofunded_projects(request, id):
    context_projects = []

    if 'user_fullprofile' in request.session:
        my_users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        selected_user_fullprofile = UsersFullProfile.select_user(id)

        if UsersMembershipTermsType.isOver_limitsize(my_users_fullprofiles, "remote-watching",
                                                     my_users_fullprofiles.users.users_membershiptypes.name_code):
            if "beginner" in my_users_fullprofiles.users.users_membershiptypes.name_code:
                context = {
                    "message_type": "warning",
                    "message": ugettext("Your membership package is inadequate for this transaction."),

                }
            else:
                context = {
                    "message_type": "error",
                    "message": ugettext(
                        "You appear to have completed of the limit for see the profile detail. You can benefit our paid membership packages."),
                    "page_title": ugettext("Profile detail"),
                }

            return render(request, "socialside/persons/cofunded_projects.html", context)

        else:
            profile_name = get_person_profile_name(my_users_fullprofiles.id, selected_user_fullprofile.id)

            if "entrepreneur" in selected_user_fullprofile.users.member_type and len(
                    selected_user_fullprofile.full_profile.position_title) < 1:
                if request.LANGUAGE_CODE == "tr":
                    position_title = "Girişimci"
                else:
                    position_title = selected_user_fullprofile.users.member_type
            else:
                if len(selected_user_fullprofile.full_profile.position_title) > 0:
                    position_title = selected_user_fullprofile.full_profile.position_title
                else:
                    position_title = UsersIndustries.select(selected_user_fullprofile).industries.description

            if not "default" in selected_user_fullprofile.full_profile.picture_url.name:
                profile_img = selected_user_fullprofile.full_profile.picture_url.url
            else:
                profile_img = selected_user_fullprofile.full_profile.linkedin_picture_url

            try:
                cofunded_list = NotificationsUsersTeams.list_all(selected_user_fullprofile)

                for cofunded_project in cofunded_list:
                    try:
                        if request.LANGUAGE_CODE is "tr":
                            steps = ProjectsSteps.select_steps(
                                cofunded_project.users_teams.teams.users_projects.projects).order_by("-steps__id")[
                                0].steps.step_name
                        else:
                            steps = ProjectsSteps.select_steps(
                                cofunded_project.users_teams.teams.users_projects.projects).order_by("-steps__id")[
                                0].steps.step_name_eng
                    except:
                        if request.LANGUAGE_CODE is "tr":
                            steps = "Henüz bir proje adımı bulunmuyor."
                        else:
                            steps = "No project steps are found yet."

                    item = {
                        "id": cofunded_project.users_teams.teams.users_projects.projects.id,
                        "project_logo": cofunded_project.users_teams.teams.users_projects.projects.general_infos.project_logo.name,
                        "title": cofunded_project.users_teams.teams.users_projects.projects.general_infos.title,

                        "steps": steps,

                        "project_locations": cofunded_project.users_teams.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + cofunded_project.users_teams.teams.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,

                        "project_options": ProjectsProjectOptions.select_options(
                            cofunded_project.users_teams.teams.users_projects.projects),

                        "isOnline": ProjectsProjectOptions.isItOnline(
                            cofunded_project.users_teams.teams.users_projects.projects,
                            ProjectOptions.select("publish-project")),
                    }

                    context_projects.append(item)

                paginator = Paginator(context_projects, 5)

                page = 1

                try:
                    projects_list = paginator.page(page)
                except PageNotAnInteger:
                    projects_list = paginator.page(1)
                except EmptyPage:
                    projects_list = paginator.page(paginator.num_pages)

                page = str(int(page) + 1)

                output_data = {
                    "message_type": "success",
                    "project_list": projects_list,
                    "has_next": projects_list.has_next(),
                    "page": page,
                    "profile_image": profile_img,
                    "headline": selected_user_fullprofile.full_profile.head_line,
                    "cover_photo": selected_user_fullprofile.full_profile.cover_photo,
                    "industry": position_title,
                    "profile_name": profile_name,
                    "profile_location": get_fulllocation_string(
                        selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                        selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.city),
                    "header_generals": "/dashboard/profiles/details/" + str(selected_user_fullprofile.id),
                    "header_projects": "/dashboard/profiles/projects/" + str(selected_user_fullprofile.id),
                    "header_cofunded": "/dashboard/profiles/cofunded-projects/" + str(
                        selected_user_fullprofile.id) + "/",
                    "section": "cofunded",
                    "page_title": profile_name + ugettext(" | Co-funded projects"),
                }
            except:
                projects_list = []
                output_data = {
                    "message_type": "success",
                    "project_list": projects_list,
                    "has_next": False,
                    "page": 0,
                    "profile_image": profile_img,
                    "headline": selected_user_fullprofile.full_profile.head_line,
                    "cover_photo": selected_user_fullprofile.full_profile.cover_photo,
                    "industry": position_title,
                    "profile_name": profile_name,
                    "profile_location": get_fulllocation_string(
                        selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                        selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.city),
                    "header_generals": "/dashboard/profiles/details/" + str(selected_user_fullprofile.id),
                    "header_projects": "/dashboard/profiles/projects/" + str(selected_user_fullprofile.id),
                    "header_cofunded": "/dashboard/profiles/cofunded-projects/" + str(
                        selected_user_fullprofile.id) + "/",
                    "section": "cofunded",
                    "page_title": profile_name + ugettext(" | Co-funded projects"),
                }

            return render(request, "socialside/persons/cofunded_projects.html", output_data)
    else:
        return redirect("/login/")


@cache_page(60 * 5)
def projects(request, id):
    context_projects = []

    if 'user_fullprofile' in request.session:
        my_users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

        if UsersMembershipTermsType.isOver_limitsize(my_users_fullprofiles, "remote-watching",
                                                     my_users_fullprofiles.users.users_membershiptypes.name_code):
            if "beginner" in my_users_fullprofiles.users.users_membershiptypes.name_code:
                context = {
                    "message_type": "warning",
                    "message": ugettext("Your membership package is inadequate for this transaction."),
                    "page_title": ugettext("Profile projects")
                }
            else:
                context = {
                    "message_type": "error",
                    "message": ugettext(
                        "You appear to have completed of the limit for see the profile detail. You can benefit our paid membership packages."),
                    "page_title": ugettext("Profile detail"),
                }

            return render(request, 'socialside/persons/projects.html', context)

        else:
            selected_user_fullprofile = UsersFullProfile.select_user(id)
            projects = UsersProjects.list_all(selected_user_fullprofile)
            locations = FullprofileLocation.select(selected_user_fullprofile.full_profile)

            for project in projects:
                if request.LANGUAGE_CODE == "tr":
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[0].steps.step_name
                    except:
                        steps = "Henüz bir proje adımı bulunmuyor."

                    item = {
                        "id": project.projects.id,
                        "project_logo": project.projects.general_infos.project_logo,
                        "title": project.projects.general_infos.title,
                        "steps": steps,
                        "project_locations": locations.location.city + ", " + locations.location.country,
                        "project_options": ProjectsProjectOptions.select_options(project.projects),
                        "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                      ProjectOptions.select("publish-project")),
                    }
                else:
                    try:
                        steps = ProjectsSteps.select_steps(project.projects).order_by("-steps__id")[
                            0].steps.step_name_eng
                    except:
                        steps = "No project steps are found yet."

                    item = {
                        "id": project.projects.id,
                        "project_logo": project.projects.general_infos.project_logo,
                        "title": project.projects.general_infos.title,
                        "steps": steps,
                        "project_locations": locations.location.city + ", " + locations.location.country,
                        "project_options": ProjectsProjectOptions.select_options(project.projects),
                        "isOnline": ProjectsProjectOptions.isItOnline(project.projects,
                                                                      ProjectOptions.select("publish-project")),
                    }

                context_projects.append(item)

            if not "default" in selected_user_fullprofile.full_profile.picture_url.name:
                profile_picture = selected_user_fullprofile.full_profile.picture_url.url
            else:
                profile_picture = selected_user_fullprofile.full_profile.linkedin_picture_url

            profile_name = get_person_profile_name(my_users_fullprofiles.id, selected_user_fullprofile.id)

            if "entrepreneur" in selected_user_fullprofile.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    position_title = "Girişimci"
                else:
                    position_title = selected_user_fullprofile.users.member_type
            else:
                if len(selected_user_fullprofile.full_profile.position_title) > 0:
                    position_title = selected_user_fullprofile.full_profile.position_title
                else:
                    position_title = UsersIndustries.select(selected_user_fullprofile).industries.description
                    # position_title = selected_user_fullprofile.usersındustries_set.get().industries.description

            context = {
                "message_type": "success",
                "projects": context_projects,
                "project_count": UsersProjects.projects_count(selected_user_fullprofile),
                "profile_image": profile_picture,
                "headline": selected_user_fullprofile.full_profile.head_line,
                "cover_photo": selected_user_fullprofile.full_profile.cover_photo,
                "industry": position_title,
                "profile_name": profile_name,
                "profile_location": get_fulllocation_string(
                    selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                    selected_user_fullprofile.full_profile.fullprofilelocation_set.get().location.city),

                "header_generals": "/dashboard/profiles/details/" + str(selected_user_fullprofile.id),
                "header_projects": "/dashboard/profiles/projects/" + str(selected_user_fullprofile.id),
                "header_cofunded": "/dashboard/profiles/cofunded-projects/" + str(selected_user_fullprofile.id) + "/",
                "section": "project",
                "page_title": profile_name + ugettext(" | Projects"),
            }
            return render(request, 'socialside/persons/projects.html', context)



    else:
        return HttpResponseRedirect("/login/")


@cache_page(60 * 10)
def details(request, id):
    if 'user_fullprofile' in request.session:
        my_users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

        if UsersMembershipTermsType.isOver_limitsize(my_users_fullprofiles, "remote-watching",
                                                     my_users_fullprofiles.users.users_membershiptypes.name_code):
            if "beginner" in my_users_fullprofiles.users.users_membershiptypes.name_code:
                context = {
                    "message_type": "error",
                    "message": ugettext(
                        "You can't see profile detail in this membership package. You can only see profile summary."),
                    "page_title": ugettext("Profile detail"),
                }
            else:
                context = {
                    "message_type": "error",
                    "message": ugettext(
                        "You appear to have completed of the limit for see the profile detail. You can benefit our paid membership packages."),
                    "page_title": ugettext("Profile detail"),
                }
        else:
            UsersMembershipTermsType.create(my_users_fullprofiles, "remote-watching",
                                            my_users_fullprofiles.users.users_membershiptypes.name_code)

            startup_history = ""
            users_fullprofiles = UsersFullProfile.select_user(id)
            # get profile image
            if not "default" in users_fullprofiles.full_profile.picture_url.name:
                profile_picture = users_fullprofiles.full_profile.picture_url.url
            else:
                profile_picture = users_fullprofiles.full_profile.linkedin_picture_url

            profile_name = get_person_profile_name(my_users_fullprofiles.id, users_fullprofiles.id)

            if UsersStartupHistory._isStartupHistoryClear(users_fullprofiles):
                if request.LANGUAGE_CODE == "tr":
                    startup_history = UsersStartupHistory.select_user_history(users_fullprofiles).startup_history.title
                else:
                    startup_history = UsersStartupHistory.select_user_history(
                        users_fullprofiles).startup_history.title_eng

            if "entrepreneur" in users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    industry = "Girişimci"
                else:
                    industry = users_fullprofiles.users.member_type
            else:
                if len(users_fullprofiles.full_profile.position_title) > 0:
                    industry = users_fullprofiles.full_profile.position_title
                else:
                    industry = UsersIndustries.select(users_fullprofiles).industries.description
                    # industry = users_fullprofiles.usersındustries_set.get().industries.description

            # bu kısımda sadece üyelik paketine bakıldı
            # ayrıca üyelik paketinin limitleri kontrol edilecek
            context = {
                "message_type": "success",
                "profile_image": profile_picture,
                "profile_name": profile_name,
                "profile_location": get_fulllocation_string(
                    FullprofileLocation.select(users_fullprofiles.full_profile).location.district,
                    FullprofileLocation.select(users_fullprofiles.full_profile).location.city),
                "headline": users_fullprofiles.full_profile.head_line,
                "summary": users_fullprofiles.full_profile.summary,
                "startup_history": startup_history,
                "industry": industry,
                "cover_photo": users_fullprofiles.full_profile.cover_photo,
                "header_generals": "/dashboard/profiles/details/" + str(users_fullprofiles.id),
                "header_projects": "/dashboard/profiles/projects/" + str(users_fullprofiles.id),
                "header_cofunded": "/dashboard/profiles/cofunded-projects/" + str(users_fullprofiles.id) + "/",
                "section": "general",
                "page_title": profile_name + ugettext(" | Profile detail"),
            }

        return render(request, "socialside/persons/detail.html", context)

    else:
        return HttpResponseRedirect("/login/")


@never_cache
@csrf_protect
def remove_skill(request):
    if "user_fullprofile" in request.session:
        try:
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            id = request.GET.get("id")
            UsersSkills.remove(users_fullprofile, Skills.select(id))
            context = {
                "message_type": "success",
                "message": ugettext("Successfully removed."),
            }
        except:
            context = {
                "message_type": "error",
                "message": ugettext("An error was encountered. Try again later, please."),
            }

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def get_userpoints(request):
    scores = UsersPointScores.calculate_points(request.session['user_fullprofile'])
    context = {
        'score': scores,
    }
    return HttpResponse(json.dumps(context), content_type='application/json')


@cache_page(60 * 10)
@csrf_protect
def get_profilepoints(request):
    if request.method == "GET":
        user_id = request.GET.get("user_id")
        scores = UsersPointScores.calculate_points(user_id)
        context = {
            'score': scores,
        }
        return HttpResponse(json.dumps(context), content_type='application/json')


@cache_page(60 * 10)
@csrf_protect
def get_profile_experiences(request):
    if request.method == "GET":
        context = []
        user_id = request.GET.get("user_id")

        users_fullprofiles = UsersFullProfile.select_user(user_id)
        if UsersCompanyInfos.isThereExperience(users_fullprofiles):
            for cmp in UsersCompanyInfos.select_all(users_fullprofiles):
                if cmp.company_infos.is_current:
                    item = {"message_type": "success", "name": cmp.company_infos.name,
                            "title": cmp.company_infos.title,
                            'start_date': cmp.company_infos.start_date_month + "." + cmp.company_infos.start_date_year,
                            "end_date": ugettext("Still Working")}
                else:
                    item = {"message_type": "success", "name": cmp.company_infos.name,
                            "title": cmp.company_infos.title, 'is_current': "Not working",
                            'start_date': cmp.company_infos.start_date_month + "." + cmp.company_infos.start_date_year,
                            'end_date': cmp.company_infos.end_date_month + "." + cmp.company_infos.end_date_year}

                context.append(item)
        else:
            item = {"message_type": "warning", "message": ugettext("User has not yet added an experience.")}
            context.append(item)

        return HttpResponse(json.dumps(context), content_type='application/json')


@cache_page(60 * 10)
@csrf_protect
def get_profile_investments(request):
    if request.method == "GET":
        users_id = request.GET.get("user_id")
        users_fullprofiles = UsersFullProfile.select_user(users_id)
        hour_cost = ""

        if UsersInvestments._isInvestmentsClear(users_fullprofiles):
            users_investments = UsersInvestments.select_users_investment(users_fullprofiles)

            if "paid-support" in users_investments.investments.title_code:
                hour_cost = UsersHourCosts.select_user_hourcost(
                    users_fullprofiles).hour_costs.cost_code + " " + UsersHourCosts.select_user_hourcost(
                    users_fullprofiles).hour_cost_amount

            if request.LANGUAGE_CODE == "tr":
                invest_type = users_investments.investments.title
            else:
                invest_type = users_investments.investments.title_eng

            if request.LANGUAGE_CODE == "tr":
                project_hours = clean_hours_title(
                    UsersProjectHours.select_user_projecthours(users_fullprofiles).project_hours.title, "saat")
            else:
                project_hours = clean_hours_title(
                    UsersProjectHours.select_user_projecthours(users_fullprofiles).project_hours.title_eng,
                    "hours")

            context = {
                "message_type": "success",
                "message": ugettext("User investments type is successful."),
                "project_hours": project_hours,
                "hour_cost": hour_cost,
                "invest_type": invest_type,
            }
        else:
            context = {
                "message_type": "warning",
                "message": ugettext("User has not yet specified the type of investment."),
            }

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def get_profile_skills(request):
    if request.method == "GET":
        context = []
        user_id = request.GET.get("id")
        users_fullprofile = UsersFullProfile.select_user(user_id)
        for skill in UsersSkills.select_all(users_fullprofile):
            item = {"id": skill.skills.id, "title": skill.skills.title}
            context.append(item)

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def upload_profile_img(request):
    if 'user_fullprofile' in request.session:
        pic = request.FILES['myfile']

        if control_file_extention(pic):
            if check_file_size(pic) > int(ImageUploadSize.select_mb("2,5")):
                context = {
                    'message_type': 'warning',
                    'message': ugettext("Your image have to small than 2.5 mb. Thanks."),
                    'redirect_url': '/profiles/edit/',
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
            else:
                selected_user_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
                selected_user = FullProfile.objects.filter(id=selected_user_fullprofile.full_profile.id).get()

                selected_user.picture_url = content_file_name(selected_user.id, pic)
                # selected_user.linkedin_picture_url = ""
                selected_user.save()
                UsersPointScores.create(selected_user_fullprofile, PointScores.select("profile-image"))

                context = {
                    'message_type': 'success',
                    'message': ugettext("Your profile image has been changed. Thanks."),
                    'redirect_url': "/profiles/edit/",
                    'profile_img': str(get_active_avatar(int(request.session["user_fullprofile"]))),
                }
                return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            context = {
                'message_type': 'error',
                'message': ugettext("your image file should be contains this extensions: *.jpg, *.bmp, *.png"),
                'redirect_url': '/profiles/edit/',
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        context = {
            'message_type': 'error',
            'message': ugettext("Your web session seems to be ended. You've to login again. Thanks."),
            'redirect_url': "/login/",
        }
        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def general_informations(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            summary = request.POST.get("summary")
            headline = request.POST.get("headline")
            city = request.POST.get("city")
            country = request.POST.get("country")
            district = request.POST.get("district")
            neighborhood = request.POST.get("neighborhood")
            lat_positions = request.POST.get("lat")
            lng_positions = request.POST.get("lng")
            position_title = request.POST.get("position-title")

            summary_message = ""
            try:
                users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

                FullProfile.objects.filter(id=users_fullprofiles.full_profile.id).update(summary=summary)
                FullProfile.objects.filter(id=users_fullprofiles.full_profile.id).update(head_line=headline)
                UsersPointScores.create(users_fullprofiles, PointScores.select("profile-headline"))

                if len(position_title) > 10:
                    FullProfile.objects.filter(id=users_fullprofiles.full_profile.id).update(
                        position_title=position_title)
                    UsersPointScores.create(users_fullprofiles, PointScores.select("position-title"))
                else:
                    UsersPointScores.delete_point(users_fullprofiles, PointScores.select("position-title"))

                # summary puanı ekleyeceksin
                if len(summary) > 80:
                    UsersPointScores.create(users_fullprofiles, PointScores.select("profile-summary"))
                else:
                    summary_message = ugettext("You should enter a narration of at least 80 characters.")
                    UsersPointScores.delete_point(users_fullprofiles, PointScores.select("profile-summary"))

                # locations kısmı eğer kayıt aşamasında sekteye uğradıysa.
                if not FullprofileLocation._isLocationClear(users_fullprofiles.full_profile):
                    locations = Locations.create(district, neighborhood, city, country, lat_positions, lng_positions)
                    fullprofile_location = FullprofileLocation.create_profileLocation(users_fullprofiles.full_profile,
                                                                                      locations)
                    UsersPointScores.create(users_fullprofiles, PointScores.select("location"))

                    context = {
                        'message_type': 'success',
                        'profile_image': '',
                        'summary': users_fullprofiles.full_profile.summary,
                        'district': fullprofile_location.location.district,
                        'neighborhood': fullprofile_location.location.neighborhood,
                        'city': fullprofile_location.location.city,
                        'country': fullprofile_location.location.country,
                        'position_title': users_fullprofiles.full_profile.position_title,
                        'message': summary_message,
                    }
                else:
                    context = {
                        'message_type': 'success',
                        'profile_image': '',
                        'summary': users_fullprofiles.full_profile.summary,
                        'position_title': users_fullprofiles.full_profile.position_title,
                        'message': ugettext('Your information has been updated. Thanks.'),
                    }
            except:
                context = {
                    'message_type': 'error',
                    'message': ugettext('Encountered an error. You can try later. Thanks'),
                }

            return HttpResponse(json.dumps(context), content_type='application/json')

    else:
        context = {
            'message_type': 'warning',
            'redirect_url': '/login/',
            'message': ugettext('Your session has timed out. You should login. Thanks.'),
        }
        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
def show_experiences(request):
    context = []

    if 'user_fullprofile' in request.session:
        user_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UsersCompanyInfos.isThereExperience(user_fullprofiles):
            for company in UsersCompanyInfos.select_all(user_fullprofiles):
                context_item = {'name': company.company_infos.name, 'title': company.company_infos.title,
                                'id': company.id,
                                'delete_btn': ugettext("Delete"), 'edit_btn': ugettext("Edit"),
                                'start_month': company.company_infos.start_date_month,
                                'start_year': company.company_infos.start_date_year,
                                'end_month': company.company_infos.end_date_month,
                                'end_year': company.company_infos.end_date_year}
                context.append(context_item)

            return HttpResponse(json.dumps(context), content_type='application/json')
        else:
            return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def update_experience(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            experiencelist = json.loads(request.POST.get("list"))
            # print(experiencelist)
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_company_infos_id = request.POST.get("id")
            # print(users_company_infos_id)
            selected = UsersCompanyInfos.select(users_company_infos_id)
            # try:
            for experience in experiencelist:
                if int(experience["end-year"]) > datetime.date.today().year or int(
                        experience["end-month"]) > datetime.date.today().month:
                    experience["end-year"] = " "
                    experience["end-month"] = " "

                if int(experience["start-year"]) > datetime.date.today().year or int(
                        experience["start-month"]) > datetime.date.today().month:
                    experience["start-year"] = " "
                    experience["start-month"] = " "

                CompanyInfos.update(experience["company-title"], experience["position"], "",
                                    experience["start-month"], experience["start-year"],
                                    experience["is-current"], experience["end-month"],
                                    experience["end-year"], selected.company_infos.id)
            context = {
                'message_type': 'success',
                'message': ugettext("Your experience was updated. Thanks."),
                'message_button': "-",
            }
            # except:
            #     context = {
            #         'message_type': 'error',
            #         'message': ugettext(
            #             "Encountered an error adding your experiences. You can try later or you can send mail to us."),
            #         'message_button': ugettext("Send Error"),
            #         'ufp_id': users_fullprofile.id,
            #         'error_type': ugettext("updating experiences error"),
            #     }

            return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def save_skills(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            skilllist = json.loads(request.POST.get("list"))
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            try:
                for skill in skilllist:
                    # print(Skills.select(skill).title)
                    UsersSkills.create(users_fullprofile, Skills.select(skill))
                    # 15 adet'ten fazla girilmemeli

                UsersPointScores.create(users_fullprofile, PointScores.select("skill-item"))

                context = {
                    'message_type': 'success',
                    'message': ugettext("Your skills was added to the leapcrowd. Thanks."),
                    'message_button': "-",
                }
            except:
                context = {
                    'message_type': 'error',
                    'message': ugettext(
                        "Encountered an error adding your skills. You can try later or you can send mail to us."),
                    'message_button': ugettext("Send Error"),
                    'ufp_id': users_fullprofile.id,
                    'error_type': ugettext("adding experiences error"),
                }

            return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def save_experiences(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            experiencelist = json.loads(request.POST.get("list"))
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            # try:
            for experience in experiencelist:
                if experience["end-year"] != "-" and experience["end-month"] != "-":
                    if (int(experience["end-year"]) > datetime.date.today().year and int(
                            experience["end-month"]) > datetime.date.today().month) or int(experience["end-year"]) < int(
                        experience["start-year"]):
                        experience["end-year"] = " "
                        experience["end-month"] = " "

                if experience["start-year"] != "-" and experience["start-month"] != "-":
                    if (int(experience["start-year"]) > datetime.date.today().year and int(
                            experience["start-month"]) > datetime.date.today().month):
                        experience["start-year"] = " "
                        experience["start-month"] = " "

                comp_info = CompanyInfos.create(experience["company-title"], experience["position"], "",
                                                experience["start-month"], experience["start-year"],
                                                experience["is-current"], experience["end-month"],
                                                experience["end-year"])

                UsersCompanyInfos.create(users_fullprofile, comp_info)

            # eğer hiç eklenmediyse point ata
            if UsersCompanyInfos.isThereExperience(users_fullprofile):
                # UsersPointScores.create(users_fullprofile, "profile-experience")
                UsersPointScores.create(users_fullprofile, PointScores.select("profile-experience"))

            context = {
                'message_type': 'success',
                'message': ugettext("Your experience was added to the leapcrowd. Thanks."),
                'message_button': "-",
            }

            # except:
            #     context = {
            #         'message_type': 'error',
            #         'message': ugettext(
            #             "Encountered an error adding your experiences. You can try later or you can send mail to us."),
            #         'message_button': ugettext("Send Error"),
            #         'ufp_id': users_fullprofile.id,
            #         'error_type': ugettext("adding experiences error"),
            #     }
            return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def delete_experience(request):
    if 'user_fullprofile' in request.session:
        try:

            id = request.GET.get("id")
            selected_companyinfos = UsersCompanyInfos.select(id).company_infos
            UsersCompanyInfos.delete(UsersCompanyInfos.select(id))
            CompanyInfos.delete(selected_companyinfos)
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

            # bu kısımda puan kontrolu yapmalıyım eğer hiç tecrübe yoksa
            # print(UsersCompanyInfos.isThereExperience(users_fullprofile))

            if UsersCompanyInfos.isThereExperience(users_fullprofile) is False:
                UsersPointScores.delete_point(users_fullprofile, PointScores.select("profile-experience"))

            context = {
                'message_type': 'success',
                'message': ugettext("Your record has been successfully removed."),
            }
            return HttpResponse(json.dumps(context), content_type='application/json')
        except:
            context = {
                'message_type': 'error',
                'message': ugettext("An error was encountered. Try again later, please."),
            }
            return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def edit_experience(request):
    if 'user_fullprofile' in request.session:
        id = request.GET.get("id")
        selected = UsersCompanyInfos.select(id)
        end_date = ""
        if len(selected.company_infos.end_date_month) == 0 and len(selected.company_infos.end_date_year) == 0:
            end_date = ""
        else:
            end_date = selected.company_infos.end_date_month + "/" + selected.company_infos.end_date_year

        context = {
            'name': selected.company_infos.name,
            'title': selected.company_infos.title,
            'start_date': selected.company_infos.start_date_month + "/" + selected.company_infos.start_date_year,
            'end_date': end_date,
            'is_current': selected.company_infos.is_current,
        }

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def get_users_skills(request):
    if 'user_fullprofile' in request.session:
        context = []
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        for skill in UsersSkills.select_all(users_fullprofile):
            item = {"id": skill.skills.id, "title": skill.skills.title}
            context.append(item)

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
@csrf_protect
def save_basic_informations(request):
    if "user_fullprofile" in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "POST":

            support_type = request.POST.get("support_type")
            project_hour = request.POST.get("project_hour")
            hour_cost = request.POST.get("hour_cost")
            startup_history = request.POST.get("startup_history")
            member_type = request.POST.get("member_type")


            try:

                if "0" != support_type and not "0" != hour_cost:
                    # investment=Investments.select_invest_type(support_type)
                    UsersInvestments.create(Investments.select_invest_type(support_type), users_fullprofile)
                    UsersPointScores.create(users_fullprofile, PointScores.select("support-type"))

                if "0" != support_type:
                    UsersInvestments.create(Investments.select_invest_type(support_type), users_fullprofile)
                    UsersPointScores.create(users_fullprofile, PointScores.select("support-type"))

                if "0" != project_hour:
                    UsersProjectHours.create(users_fullprofile, ProjectHours.select_project_hours(project_hour))
                    UsersPointScores.create(users_fullprofile, PointScores.select("hours"))

                if "0" != hour_cost:
                    UsersHourCosts.create(users_fullprofile, HourCosts.select(request.LANGUAGE_CODE, change_characters(
                        users_fullprofile.full_profile.fullprofilelocation_set.get().location.country)), hour_cost)
                    UsersPointScores.create(users_fullprofile, PointScores.select("hourly-cost"))

                if "0" != startup_history:
                    UsersStartupHistory.create(users_fullprofile, StartupHistory.select(startup_history))
                    UsersPointScores.create(users_fullprofile, PointScores.select("startup-history"))

                if "0" != member_type:
                    result_date = (datetime.date.today() - users_fullprofile.users.joined_date.date()).days
                    if result_date > 30 and member_type != users_fullprofile.users.member_type:
                        Users.update_user_membertype(users_fullprofile.users.id, member_type)

                context = {
                    "message_type": "success",
                    "message": ugettext("Your basic informations was added to the leapcrowd. Thanks."),
                }

            except:
                context = {
                    "message_type": "error",
                    'message': ugettext(
                        "Encountered an error adding your informations. You can try later or you can send mail to us."),
                    'message_button': ugettext("Send Error"),
                    'ufp_id': users_fullprofile.id,
                    'error_type': ugettext("adding experiences error"),
                }

            return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
def selected_investmentType(request):
    if "user_fullprofile" in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UsersInvestments._isInvestmentsClear(users_fullprofile):
            selected = UsersInvestments.select_users_investment(users_fullprofile)
            context = {
                'message_type': "success",
                'id': selected.investments.id,
            }
        else:
            context = {
                'message_type': "warning",
                'id': 0,
            }

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
def selectedHours(request):
    if "user_fullprofile" in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UsersProjectHours._isProjectHoursClear(users_fullprofile):
            selected = UsersProjectHours.select_user_projecthours(users_fullprofile)
            context = {
                "message_type": "success",
                "id": selected.project_hours.id,
            }
        else:
            context = {
                "message_type": "warning",
                "id": 0,
            }

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
def selected_startupHistory(request):
    if "user_fullprofile" in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UsersStartupHistory._isStartupHistoryClear(users_fullprofile):
            selected = UsersStartupHistory.select_user_history(users_fullprofile)
            context = {
                "message_type": "success",
                "id": selected.startup_history.id,
            }

        else:
            context = {
                "message_type": "warning",
                "id": 0,
            }

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
def selected_hourcost(request):
    if "user_fullprofile" in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UsersHourCosts._isUsersHourCostsClear(users_fullprofile):
            selected = UsersHourCosts.select_user_hourcost(users_fullprofile)
            context = {
                "message_type": "success",
                "id": selected.hour_cost_amount,
            }

        else:
            context = {
                "message_type": "warning",
                "id": 0,
            }

        return HttpResponse(json.dumps(context), content_type='application/json')


@never_cache
def logout(request):
    if request.method == "GET":
        if "user_fullprofile" in request.session:
            del request.session["user_fullprofile"]
            context = {
                "message_type": "success",
                "message": ugettext("You have successfully ended your session."),
                "redirect_url": "/login/",
            }
        else:
            context = {
                "message_type": "error",
                "message": ugettext("You are already ended your session."),
                "redirect_url": "/login/",
            }

        # print(request.session["user_fullprofile"])
        return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def position_title(request):
    if "user_fullprofile" in request.session:
        title_list = []
        # users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            keyword = request.GET.get("title")
            finding = JobTitles.objects.filter(title__contains=keyword.lower()).all()[:10]
            for k in finding:
                s = {'id': k.id, 'title': k.title}
                title_list.append(s)

            return JsonResponse(json.dumps(title_list), safe=False)


@never_cache
@csrf_protect
def dm_request_person(request):
    if "user_fullprofile" in request.session:
        if request.method == "POST":
            user_id = request.POST.get("user_id")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            requested_users_fullprofile = UsersFullProfile.select_user(user_id)

            if is_it_same_location(users_fullprofile.id,
                                   requested_users_fullprofile.id) == True or UsersFriend.isThis_my_friend(
                    users_fullprofile, requested_users_fullprofile) == True:
                # aynı şehirdeyim ve projem var mı?

                if users_fullprofile.usersprojects_set.count() > 0:

                    if "entrepreneur" in requested_users_fullprofile.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title = "Girişimci"
                        else:
                            position_title = requested_users_fullprofile.users.member_type
                    else:
                        if len(requested_users_fullprofile.full_profile.position_title) > 10:
                            position_title = requested_users_fullprofile.full_profile.position_title
                        else:
                            position_title = UsersIndustries.select(requested_users_fullprofile).industries.description
                            # position_title = requested_users_fullprofile.usersındustries_set.get().industries.description

                    if not "default" in requested_users_fullprofile.full_profile.picture_url.name:
                        profile_picture = requested_users_fullprofile.full_profile.picture_url.url
                    else:
                        profile_picture = requested_users_fullprofile.full_profile.linkedin_picture_url

                    profile_location = get_fulllocation_string(
                        requested_users_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                        requested_users_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + requested_users_fullprofile.full_profile.fullprofilelocation_set.get().location.country

                    profile_point = UsersPointScores.calculate_points(requested_users_fullprofile)

                    context = {
                        "profile_image": profile_picture,
                        "profile_point": profile_point,
                        "position_title": position_title,
                        "profile_address": "/dashboard/profiles/details/" + str(requested_users_fullprofile.id) + "/",
                        "profile_location": profile_location,
                        # "profile_name":get_profile_name_str(requested_users_fullprofile.full_profile.last_name,requested_users_fullprofile.full_profile.first_name),
                        "profile_name": get_person_profile_name(users_fullprofile.id, requested_users_fullprofile.id),
                        "message_type": "success",
                        "message": "",
                    }
                else:
                    # projem yok ise önce proje oluştur uyarısı
                    context = {
                        "message_type": "warning",
                        "message": ugettext(
                            "If you want to send team request directly to the selected person, you have to create a project first."),
                    }
            else:
                # aynı şehirde değilsem - üyelik paketim teklif vermeyi karşılıyor mu
                if "beginner" == users_fullprofile.users.users_membershiptypes.name_code:
                    # karşılamıyorsa
                    context = {
                        "message_type": "warning",
                        "message": ugettext(
                            "Your membership is not enough for sending team request. You can only send team request for the nearby users. Or you can benefit from our paid memberships."),
                    }
                else:
                    if users_fullprofile.usersprojects_set.count() > 0:

                        if "entrepreneur" in requested_users_fullprofile.users.member_type:
                            if request.LANGUAGE_CODE == "tr":
                                position_title = "Girişimci"
                            else:
                                position_title = requested_users_fullprofile.users.member_type
                        else:
                            if len(requested_users_fullprofile.full_profile.position_title) > 10:
                                position_title = requested_users_fullprofile.full_profile.position_title
                            else:
                                position_title = UsersIndustries.select(
                                    requested_users_fullprofile).industries.description
                                # position_title = requested_users_fullprofile.usersındustries_set.get().industries.description

                        if not "default" in requested_users_fullprofile.full_profile.picture_url.name:
                            profile_picture = requested_users_fullprofile.full_profile.picture_url.url
                        else:
                            profile_picture = requested_users_fullprofile.full_profile.linkedin_picture_url

                        profile_location = get_fulllocation_string(
                            requested_users_fullprofile.full_profile.fullprofilelocation_set.get().location.district,
                            requested_users_fullprofile.full_profile.fullprofilelocation_set.get().location.city) + " - " + requested_users_fullprofile.full_profile.fullprofilelocation_set.get().location.country

                        profile_point = UsersPointScores.calculate_points(requested_users_fullprofile)

                        context = {
                            "profile_image": profile_picture,
                            "profile_point": profile_point,
                            "position_title": position_title,
                            "profile_address": "/dashboard/profiles/details/" + str(
                                requested_users_fullprofile.id) + "/",
                            "profile_location": profile_location,
                            # "profile_name": get_profile_name_str(requested_users_fullprofile.full_profile.last_name,requested_users_fullprofile.full_profile.first_name),
                            "profile_name": get_person_profile_name(users_fullprofile.id,
                                                                    requested_users_fullprofile.id),
                            "message_type": "success",
                            "message": "",
                        }
                    else:
                        context = {
                            "message_type": "warning",
                            "message": ugettext(
                                "If you want to send team request directly to the selected person, you have to create a project first."),
                        }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
@csrf_protect
def request_persons_projects(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_projects = UsersProjects.select_active_projects_by_user(users_fullprofile)
            context_projects = []

            for prj in users_projects:
                item = {
                    "title": prj.projects.general_infos.title,
                    "id": prj.id,
                }
                context_projects.append(item)

            context = {
                "projects": list(context_projects),
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@cache_page(60 * 5)
@csrf_protect
def dm_requested_person_info(request):
    if "user_fullprofile" in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            user_id = request.GET.get("id")
            requested_users_fullprofiles = UsersFullProfile.select_user(user_id)

            if not "default" in requested_users_fullprofiles.full_profile.picture_url.name:
                profile_picture = requested_users_fullprofiles.full_profile.picture_url.url

            else:
                profile_picture = requested_users_fullprofiles.full_profile.linkedin_picture_url

            if "entrepreneur" in requested_users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    position_title = "Girişimci"
                else:
                    position_title = requested_users_fullprofiles.users.member_type
            else:
                if len(requested_users_fullprofiles.full_profile.position_title) > 0:
                    position_title = requested_users_fullprofiles.full_profile.position_title
                else:
                    position_title = UsersIndustries.select(requested_users_fullprofiles).industries.description
                    # position_title = requested_users_fullprofiles.usersındustries_set.get().industries.description

            context = {
                # "profile_name": get_profile_name_str(requested_users_fullprofiles.full_profile.last_name,
                #                                      requested_users_fullprofiles.full_profile.first_name),
                "profile_name": get_person_profile_name(users_fullprofile.id, requested_users_fullprofiles.id),
                "profile_image": profile_picture,
                "position_title": position_title,
                "profile_location": get_fulllocation_string(
                    requested_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                    requested_users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city),
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
@csrf_protect
def dm_send_team_request(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            user_id = request.GET.get("id")
            users_projects_id = request.GET.get("project")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            # context_mail=[]
            # owner_city = change_characters(users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city)
            # print("Üyelik Paketim : ", users_fullprofiles.users.users_membershiptypes.name_code)
            team_person_limit = MembershipTermsType.select_terms_type(
                users_fullprofiles.users.users_membershiptypes.name_code, "person-to-team").limit_count
            extra_person_limit = MembershipTermsType.select_terms_type(
                users_fullprofiles.users.users_membershiptypes.name_code, "person-to-team-location").limit_count

            # Seçilen Projede oluşturduğumuz bir takım var mı
            if UsersProjects.isProjectForThisUser(users_fullprofiles, UsersProjects.select_by_id(users_projects_id,
                                                                                                 users_fullprofiles).projects):
                # projemiz var ve bize ait
                # kurduğumuz bir takım var mı - yok mu
                if Teams.is_there_team_by_project(UsersProjects.select_by_id(users_projects_id, users_fullprofiles)):
                    # projeme ait daha önceden kurulmuş bir takımım var mı
                    selected_team = Teams.select_team(UsersProjects.select_by_id(users_projects_id, users_fullprofiles))
                    team_person_count = (selected_team.usersteams_set.count() - 1)
                    # eğer takımımdaki aynı lokasyonda yer alan kişi sayısı 0 ise
                    # print("aynı lokasyondaki kişi sayısı : ",UsersTeams.my_location_teammembers_count(selected_team.users_projects,users_fullprofiles.id))



                    if UsersTeams.my_location_teammembers_count(selected_team.users_projects,
                                                                users_fullprofiles.id) == 0:
                        # özel teklif gönderdiğim kişiler arasunda aynı lokasyonda olan kişilerin sayısı
                        extra_person_count = UsersDmProjectsTeamrequests.same_location_count_requested(
                            selected_team.users_projects, users_fullprofiles.id)
                        team_person_count += extra_person_count
                        sended_extra = True
                    else:

                        extra_person_count = UsersTeams.my_location_teammembers_count(selected_team.users_projects,
                                                                                      users_fullprofiles.id) + UsersDmProjectsTeamrequests.same_location_count_requested(
                            selected_team.users_projects, users_fullprofiles.id)
                        team_person_count += extra_person_count
                        sended_extra = False

                    # print("lokasyondaki toplam kişi sayısı : ", extra_person_count)
                    # print("özel istek gönderdiklerimin sayısı - aynı lokasyon : ", UsersDmProjectsTeamrequests.same_location_count_requested(selected_team.users_projects, users_fullprofiles.id))

                    if UsersDmProjectsTeamrequests.is_there_created_request(UsersFullProfile.select_user(user_id),
                                                                            selected_team.users_projects) == False or UsersProjectsTeamrequests.selected_user_has_request(
                            UsersFullProfile.select_user(user_id), selected_team.users_projects) == False:
                        # daha önceden istek göndermemişim

                        if team_person_limit == -1:
                            try:
                                created_teamrequest = Teamrequests.create_for_dm(0)
                                UsersDmProjectsTeamrequests.create_request(created_teamrequest, users_fullprofiles,
                                                                           UsersFullProfile.select_user(user_id),
                                                                           selected_team.users_projects)
                                UsersMembershipTermsType.create(users_fullprofiles, "send-direct-teamrequest",
                                                                users_fullprofiles.users.users_membershiptypes.name_code)

                                team_person_count += 1

                                if dm_request_teamrequest_mail(
                                        UsersFullProfile.select_user(user_id).users.email_address, user_id,
                                        get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                             users_fullprofiles.full_profile.first_name),
                                        selected_team.users_projects.projects.general_infos.title):
                                    message = ugettext("Your team request has been successfully sent.")
                                else:
                                    message = ugettext(
                                        "Your team request has been successfully sent but there was a problem sending an email to user.")

                                context = {
                                    "message_type": "success",
                                    "message": message,
                                }

                                notification_message_tr = get_profile_name_str(
                                    users_fullprofiles.full_profile.last_name,
                                    users_fullprofiles.full_profile.first_name) + " size özel takım isteği gönderdi. Takım isteğini bildirimleriniz kısmından cevaplayabilirsiniz."
                                notification_message_en = get_profile_name_str(
                                    users_fullprofiles.full_profile.last_name,
                                    users_fullprofiles.full_profile.first_name) + " sent a private team request. You can send an answer from your notifications area."

                                payload = {
                                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                    "include_player_ids": send_signals_to_projects_owner(user_id),
                                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                                    "url": "/dashboard/"
                                }

                                requests.post("https://onesignal.com/api/v1/notifications",
                                              headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))


                            except:
                                context = {
                                    "message_type": "error",
                                    "message": ugettext("There was a problem in sending team request.")
                                }
                        else:
                            if is_it_same_location(users_fullprofiles.id, user_id) == False:

                                # teklif göndereceğim kişi benimle farklı lokasyonda
                                # doğrudan istek gönderme hakkım var mı
                                if UsersMembershipTermsType.isOver_limitsize(users_fullprofiles,
                                                                             "send-direct-teamrequest",
                                                                             users_fullprofiles.users.users_membershiptypes.name_code) == False:
                                    # doğrudan istek gönderebilirim
                                    if team_person_count < team_person_limit:
                                        # ekleyebilirim
                                        # print("buraya girmemesi lazım : ",team_person_count)
                                        # print("team_person_limit : ", team_person_limit)
                                        try:
                                            created_teamrequest = Teamrequests.create_for_dm(0)
                                            UsersDmProjectsTeamrequests.create_request(created_teamrequest,
                                                                                       users_fullprofiles,
                                                                                       UsersFullProfile.select_user(
                                                                                           user_id),
                                                                                       selected_team.users_projects)
                                            UsersMembershipTermsType.create(users_fullprofiles,
                                                                            "send-direct-teamrequest",
                                                                            users_fullprofiles.users.users_membershiptypes.name_code)

                                            team_person_count += 1

                                            if dm_request_teamrequest_mail(
                                                    UsersFullProfile.select_user(user_id).users.email_address, user_id,
                                                    get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                                         users_fullprofiles.full_profile.first_name),
                                                    selected_team.users_projects.projects.general_infos.title):
                                                message = ugettext("Your team request has been successfully sent.")
                                            else:
                                                message = ugettext(
                                                    "Your team request has been successfully sent but there was a problem sending an email to user.")

                                            context = {
                                                "message_type": "success",
                                                "message": message,
                                                # "message":"ok eklendi " + str(team_person_count) + " - " + str(team_person_limit),
                                            }

                                            notification_message_tr = get_profile_name_str(
                                                users_fullprofiles.full_profile.last_name,
                                                users_fullprofiles.full_profile.first_name) + " size özel takım isteği gönderdi. Takım isteğini bildirimleriniz kısmından cevaplayabilirsiniz."
                                            notification_message_en = get_profile_name_str(
                                                users_fullprofiles.full_profile.last_name,
                                                users_fullprofiles.full_profile.first_name) + " sent a private team request. You can send an answer from your notifications area."

                                            payload = {
                                                "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                                "include_player_ids": send_signals_to_projects_owner(user_id),
                                                "contents": {"en": notification_message_en,
                                                             "tr": notification_message_tr},
                                                "url": "/dashboard/"
                                            }

                                            requests.post("https://onesignal.com/api/v1/notifications",
                                                          headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))


                                        except:
                                            context = {
                                                "message_type": "error",
                                                "message": ugettext("There was a problem in sending team request.")
                                            }

                                    else:
                                        # print("buraya girmesi lazım")
                                        if extra_person_count < extra_person_limit:

                                            if request.LANGUAGE_CODE == "tr":
                                                message = "Şuan kurulmuş olan takımınıza konum dışından birini ekleme limitiniz olmadığı için, sadece konum içinden " + str(
                                                    abs(
                                                        extra_person_limit - extra_person_count)) + " adet kişi ekleyebilirsiniz."
                                            else:
                                                message_1 = "You can not include a person in outside your location. "
                                                message = ngettext(
                                                    message_1 + "You can only include %(count)d person in your location",
                                                    "You can only include %(count)d people in your location",
                                                    abs(extra_person_limit - extra_person_count) % {
                                                        "count": abs(extra_person_limit - extra_person_count)})

                                            context = {
                                                "message_type": "warning",
                                                "message": message,
                                            }

                                        else:
                                            # hiç bir şekilde kimseyi ekleyeymem mesajı
                                            if "beginner" in users_fullprofiles.users.users_membershiptypes.name_code:
                                                context = {
                                                    "message_type": "error",
                                                    "message": "You have " + str(
                                                        selected_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit from our paid memberships.",
                                                }
                                            else:
                                                if request.LANGUAGE_CODE == "tr":
                                                    message = "Bulunduğunuz üyelik paketinde takımınıza başka bir kişiyi dahil edemezsiniz. Şu an projenizde yer alan takımda sizinle birlikte " + str(
                                                        selected_team.usersteams_set.count()) + " adet kişi bulunuyor. Projenize daha fazla kişi eklemek için bir üst üyelik paketinden faydalanmanız gerekmektedir."
                                                else:
                                                    message = "You have " + str(
                                                        selected_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit our 'Growing' membership package"

                                                context = {
                                                    "message_type": "error",
                                                    "message": message,
                                                }

                                else:
                                    # doğrudan istek gönderemem - konum içi gönderebilir miyim
                                    # bilgi mesajları
                                    if extra_person_count < extra_person_limit:
                                        if request.LANGUAGE_CODE == "tr":
                                            message = "Şuan kurulmuş olan takımınıza konum dışından birini ekleme limitiniz olmadığı için, sadece konum içinden " + str(
                                                abs(
                                                    extra_person_limit - extra_person_count)) + " adet kişi ekleyebilirsiniz."
                                        else:
                                            message_1 = "You can not include a person in outside your location. "
                                            message = ngettext(
                                                message_1 + "You can only include %(count)d person in your location",
                                                "You can only include %(count)d people in your location",
                                                abs(extra_person_limit - extra_person_count) % {
                                                    "count": abs(extra_person_limit - extra_person_count)})

                                        context = {
                                            "message_type": "warning",
                                            "message": message,
                                        }

                                    elif UsersMembershipTermsType.isOver_limitsize(users_fullprofiles,
                                                                                   "send-direct-teamrequest",
                                                                                   users_fullprofiles.users.users_membershiptypes.name_code) == True:
                                        if request.LANGUAGE_CODE == "tr":
                                            message = "Bulunduğunuz üyelik paketinde takımınıza; konumunuz dışından veya arkadaşınız olmayan birini ekleyemezsiniz. Projenize daha fazla kişi eklemek için bir üst üyelik paketinden faydalanmanız gerekmektedir."
                                        else:
                                            message = "You can't add any person your team from outside your location. You should benefit our paid membership packages."

                                        context = {
                                            "message_type": "error",
                                            "message": message,
                                        }

                                    else:
                                        # hiç bir şekilde kimseyi ekleyeymem mesajı
                                        if sended_extra is True:
                                            if request.LANGUAGE_CODE == "tr":
                                                message = "Özel takım isteği gönderdiğiniz kişiler içinde " + str(
                                                    extra_person_count) + " adet kişi bulunuyor. Eğer daha fazla kişiye teklif göndermek istiyorsanız, gönderdiğiniz özel teklifleri <u>takımlarım</u> bölümünden iptal edebilirsiniz veya ücretli üyeliklerimizden yararlanabilirsiniz."
                                            else:
                                                message = "If you want to send more team requests to the people in your location, you can cancel your previously submitted team requests from <u>my teams</u> section. Or you can benefit our paid memberships."
                                        else:
                                            if request.LANGUAGE_CODE == "tr":
                                                message = "Bulunduğunuz üyelik paketinde takımınıza başka bir kişiyi dahil edemezsiniz. Şu an projenizde yer alan takımda sizinle birlikte " + str(
                                                    selected_team.usersteams_set.count()) + " adet kişi bulunuyor. Projenize daha fazla kişi eklemek için bir üst üyelik paketinden faydalanmanız gerekmektedir."
                                            else:
                                                message = "You have " + str(
                                                    selected_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit our 'Growing' membership package"

                                        context = {
                                            "message_type": "error",
                                            "message": message,
                                        }

                            else:
                                # teklif göndereceğim kişi benimle aynı lokasyonda
                                if extra_person_count < extra_person_limit and sended_extra == True:
                                    # ekleyebiliyorum
                                    try:
                                        created_teamrequest = Teamrequests.create_for_dm(0)
                                        UsersDmProjectsTeamrequests.create_request(created_teamrequest,
                                                                                   users_fullprofiles,
                                                                                   UsersFullProfile.select_user(
                                                                                       user_id),
                                                                                   selected_team.users_projects)

                                        extra_person_count += 1

                                        if dm_request_teamrequest_mail(
                                                UsersFullProfile.select_user(user_id).users.email_address, user_id,
                                                get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                                     users_fullprofiles.full_profile.first_name),
                                                selected_team.users_projects.projects.general_infos.title):
                                            message = ugettext("Your team request has been successfully sent.")
                                        else:
                                            message = ugettext(
                                                "Your team request has been successfully sent but there was a problem sending an email to user.")

                                        context = {
                                            "message_type": "success",
                                            "message": message,
                                            # "message":"ok eklendi" + str(extra_person_count) + " - " + str(extra_person_limit),
                                        }

                                        notification_message_tr = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + " size özel takım isteği gönderdi. Takım isteğini bildirimleriniz kısmından cevaplayabilirsiniz."
                                        notification_message_en = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + " sent a private team request. You can send an answer from your notifications area."

                                        payload = {
                                            "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                            "include_player_ids": send_signals_to_projects_owner(user_id),
                                            "contents": {"en": notification_message_en, "tr": notification_message_tr},
                                            "url": "/dashboard/"
                                        }

                                        requests.post("https://onesignal.com/api/v1/notifications",
                                                      headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))

                                    except:
                                        context = {
                                            "message_type": "error",
                                            "message": ugettext("There was a problem in sending team request.")
                                        }

                                else:
                                    # aynı lokasyondan da ekleyemiyorum
                                    if sended_extra is True:
                                        if request.LANGUAGE_CODE == "tr":
                                            message = "Özel takım isteği gönderdiğiniz kişiler içinde " + str(
                                                extra_person_count) + " adet kişi bulunuyor. Eğer daha fazla kişiye teklif göndermek istiyorsanız, gönderdiğiniz özel teklifleri <u>takımlarım</u> bölümünden iptal edebilirsiniz veya ücretli üyeliklerimizden yararlanabilirsiniz."
                                        else:
                                            message = "If you want to send more team requests to the people in your location, you can cancel your previously submitted team requests from <u>my teams</u> section. Or you can benefit our paid memberships."
                                    else:
                                        if request.LANGUAGE_CODE == "tr":
                                            message = "Bulunduğunuz üyelik paketinde takımınıza başka bir kişiyi dahil edemezsiniz. Şu an projenizde yer alan takımda sizinle birlikte " + str(
                                                selected_team.usersteams_set.count()) + " adet kişi bulunuyor. Projenize daha fazla kişi eklemek için bir üst üyelik paketinden faydalanmanız gerekmektedir."
                                        else:
                                            message = "You have " + str(
                                                selected_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit our 'Growing' membership package"

                                    context = {
                                        "message_type": "error",
                                        "message": message,
                                    }



                    else:
                        # daha önceden istek göndermişim
                        context = {
                            "message_type": "success",
                            "message": ugettext(
                                "You had already sent a team request this person. You should be patient or you can send a message."),
                        }

                else:
                    # ilk defa takım kurulacak
                    users_projects = UsersProjects.select_by_id(users_projects_id, users_fullprofiles)

                    if request.LANGUAGE_CODE == "tr":
                        team_title = users_projects.projects.general_infos.title.capitalize() + " takımı"
                    else:
                        team_title = users_projects.projects.general_infos.title.capitalize() + " team"

                    created_team = Teams.create(team_title, "", users_projects)
                    UsersTeams.create(users_fullprofiles, created_team)

                    team_person_count = 0

                    if UsersTeams.my_location_teammembers_count(users_projects, users_fullprofiles.id) == 0:
                        # özel teklif gönderdiğim kişiler arasunda aynı lokasyonda olan kişilerin sayısı
                        extra_person_count = UsersDmProjectsTeamrequests.same_location_count_requested(users_projects,
                                                                                                       users_fullprofiles.id)
                        sended_extra = True
                    else:
                        extra_person_count = UsersTeams.my_location_teammembers_count(users_projects,
                                                                                      users_fullprofiles.id) + UsersDmProjectsTeamrequests.same_location_count_requested(
                            users_projects, users_fullprofiles.id)
                        sended_extra = False

                    if UsersDmProjectsTeamrequests.is_there_created_request(UsersFullProfile.select_user(user_id),
                                                                            created_team.users_projects) is False:
                        # daha önceden istek göndermemişim
                        if team_person_limit == -1:
                            try:
                                created_teamrequest = Teamrequests.create_for_dm(0)
                                UsersDmProjectsTeamrequests.create_request(created_teamrequest, users_fullprofiles,
                                                                           UsersFullProfile.select_user(user_id),
                                                                           created_team.users_projects)
                                UsersMembershipTermsType.create(users_fullprofiles, "send-direct-teamrequest",
                                                                users_fullprofiles.users.users_membershiptypes.name_code)
                                team_person_count += 1
                                if dm_request_teamrequest_mail(
                                        UsersFullProfile.select_user(user_id).users.email_address, user_id,
                                        get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                             users_fullprofiles.full_profile.first_name),
                                        created_team.users_projects.projects.general_infos.title):
                                    message = ugettext("Your team request has been successfully sent.")
                                else:
                                    message = ugettext(
                                        "Your team request has been successfully sent but there was a problem sending an email to user.")

                                context = {
                                    "message_type": "success",
                                    "message": message,
                                }

                                notification_message_tr = get_profile_name_str(
                                    users_fullprofiles.full_profile.last_name,
                                    users_fullprofiles.full_profile.first_name) + " size özel takım isteği gönderdi. Takım isteğini bildirimleriniz kısmından cevaplayabilirsiniz."
                                notification_message_en = get_profile_name_str(
                                    users_fullprofiles.full_profile.last_name,
                                    users_fullprofiles.full_profile.first_name) + " sent a private team request. You can send an answer from your notifications area."

                                payload = {
                                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                    "include_player_ids": send_signals_to_projects_owner(user_id),
                                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                                    "url": "/dashboard/"
                                }

                                requests.post("https://onesignal.com/api/v1/notifications",
                                              headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))

                            except:
                                context = {
                                    "message_type": "error",
                                    "message": ugettext("There was a problem in sending team request.")
                                }

                        else:
                            if is_it_same_location(users_fullprofiles.id,
                                                   UsersFullProfile.select_user(user_id).id) == False:
                                # benimle farklı konumda
                                if UsersMembershipTermsType.isOver_limitsize(users_fullprofiles,
                                                                             "send-direct-teamrequest",
                                                                             users_fullprofiles.users.users_membershiptypes.name_code) is False:
                                    # doğrudan takım isteği gönderebiliyorum
                                    if team_person_count < team_person_limit:
                                        try:
                                            created_teamrequest = Teamrequests.create_for_dm(0)
                                            UsersDmProjectsTeamrequests.create_request(created_teamrequest,
                                                                                       users_fullprofiles,
                                                                                       UsersFullProfile.select_user(
                                                                                           user_id),
                                                                                       created_team.users_projects)
                                            UsersMembershipTermsType.create(users_fullprofiles,
                                                                            "send-direct-teamrequest",
                                                                            users_fullprofiles.users.users_membershiptypes.name_code)
                                            team_person_count += 1
                                            if dm_request_teamrequest_mail(
                                                    UsersFullProfile.select_user(user_id).users.email_address, user_id,
                                                    get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                                         users_fullprofiles.full_profile.first_name),
                                                    created_team.users_projects.projects.general_infos.title):
                                                message = ugettext("Your team request has been successfully sent.")
                                            else:
                                                message = ugettext(
                                                    "Your team request has been successfully sent but there was a problem sending an email to user.")

                                            context = {
                                                "message_type": "success",
                                                "message": message,
                                                # "message":"ok eklendi " + str(team_person_count) + " - " + str(team_person_limit),
                                            }

                                            notification_message_tr = get_profile_name_str(
                                                users_fullprofiles.full_profile.last_name,
                                                users_fullprofiles.full_profile.first_name) + " size özel takım isteği gönderdi. Takım isteğini bildirimleriniz kısmından cevaplayabilirsiniz."
                                            notification_message_en = get_profile_name_str(
                                                users_fullprofiles.full_profile.last_name,
                                                users_fullprofiles.full_profile.first_name) + " sent a private team request. You can send an answer from your notifications area."

                                            payload = {
                                                "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                                "include_player_ids": send_signals_to_projects_owner(user_id),
                                                "contents": {"en": notification_message_en,
                                                             "tr": notification_message_tr},
                                                "url": "/dashboard/"
                                            }

                                            requests.post("https://onesignal.com/api/v1/notifications",
                                                          headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))

                                        except:
                                            context = {
                                                "message_type": "error",
                                                "message": ugettext("There was a problem in sending team request.")
                                            }
                                    else:
                                        if extra_person_count < extra_person_limit:
                                            if request.LANGUAGE_CODE == "tr":
                                                message = "Şuan kurulmuş olan takımınıza konum dışından birini ekleme limitiniz olmadığı için, sadece konum içinden " + str(
                                                    abs(
                                                        extra_person_limit - extra_person_count)) + " adet kişi ekleyebilirsiniz."
                                            else:
                                                message_1 = "You can not include a person in outside your location. "
                                                message = ngettext(
                                                    message_1 + "You can only include %(count)d person in your location",
                                                    "You can only include %(count)d people in your location",
                                                    (extra_person_limit - extra_person_count) % {
                                                        "count": (extra_person_limit - extra_person_count)})

                                            context = {
                                                "message_type": "warning",
                                                "message": message,
                                            }

                                        else:
                                            # hiç bir şekilde kimseyi ekleyeymem mesajı
                                            if "beginner" in users_fullprofiles.users.users_membershiptypes.name_code:
                                                context = {
                                                    "message_type": "error",
                                                    "message": "You have " + str(
                                                        created_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit from our paid memberships.",
                                                }
                                            else:
                                                if request.LANGUAGE_CODE == "tr":
                                                    message = "Bulunduğunuz üyelik paketinde takımınıza başka bir kişiyi dahil edemezsiniz. Şu an projenizde yer alan takımda sizinle birlikte " + str(
                                                        created_team.usersteams_set.count()) + " adet kişi bulunuyor. Projenize daha fazla kişi eklemek için bir üst üyelik paketinden faydalanmanız gerekmektedir."
                                                else:
                                                    message = "You have " + str(
                                                        created_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit our 'Growing' membership package"

                                                context = {
                                                    "message_type": "error",
                                                    "message": message,
                                                }

                                else:
                                    # doğrudan takım isteği gönderemiyorum
                                    if extra_person_count < extra_person_limit:
                                        if request.LANGUAGE_CODE == "tr":
                                            message = "Şuan kurulmuş olan takımınıza konum dışından birini ekleme limitiniz olmadığı için, sadece konum içinden " + str(
                                                abs(
                                                    extra_person_limit - extra_person_count)) + " adet kişi ekleyebilirsiniz."
                                        else:
                                            message_1 = "You can not include a person in outside your location. "
                                            message = ngettext(
                                                message_1 + "You can only include %(count)d person in your location",
                                                "You can only include %(count)d people in your location",
                                                str(abs(extra_person_limit - extra_person_count)) % {
                                                    "count": abs(extra_person_limit - extra_person_count)})

                                        context = {
                                            "message_type": "warning",
                                            "message": message,
                                        }

                                    else:
                                        # hiç bir şekilde kimseyi ekleyeymem mesajı
                                        if sended_extra is True:
                                            if request.LANGUAGE_CODE == "tr":
                                                message = "Özel takım isteği gönderdiğiniz kişiler içinde " + str(
                                                    extra_person_count) + " adet kişi bulunuyor. Eğer daha fazla kişiye teklif göndermek istiyorsanız, gönderdiğiniz özel teklifleri <u>takımlarım</u> bölümünden iptal edebilirsiniz veya ücretli üyeliklerimizden yararlanabilirsiniz."
                                            else:
                                                message = "If you want to send more team requests to the people in your location, you can cancel your previously submitted team requests from <u>my teams</u> section. Or you can benefit our paid memberships."
                                        else:
                                            if request.LANGUAGE_CODE == "tr":
                                                message = "Bulunduğunuz üyelik paketinde takımınıza başka bir kişiyi dahil edemezsiniz. Şu an projenizde yer alan takımda sizinle birlikte " + str(
                                                    created_team.usersteams_set.count()) + " adet kişi bulunuyor. Projenize daha fazla kişi eklemek için bir üst üyelik paketinden faydalanmanız gerekmektedir."
                                            else:
                                                message = "You have " + str(
                                                    created_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit our 'Growing' membership package"

                                        context = {
                                            "message_type": "error",
                                            "message": message,
                                        }
                            else:
                                # benimle aynı konumda
                                # print(extra_person_count)
                                # print("***************")
                                # print(extra_person_limit)
                                if extra_person_count < extra_person_limit:
                                    try:
                                        created_teamrequest = Teamrequests.create_for_dm(0)
                                        UsersDmProjectsTeamrequests.create_request(created_teamrequest,
                                                                                   users_fullprofiles,
                                                                                   UsersFullProfile.select_user(
                                                                                       user_id),
                                                                                   created_team.users_projects)

                                        extra_person_count += 1

                                        if dm_request_teamrequest_mail(
                                                UsersFullProfile.select_user(user_id).users.email_address, user_id,
                                                get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                                     users_fullprofiles.full_profile.first_name),
                                                created_team.users_projects.projects.general_infos.title):
                                            message = ugettext("Your team request has been successfully sent.")
                                        else:
                                            message = ugettext(
                                                "Your team request has been successfully sent but there was a problem sending an email to user.")

                                        context = {
                                            "message_type": "success",
                                            "message": message,
                                            # "message":"ok eklendi " + str(extra_person_count) + " - " + str(extra_person_limit),
                                        }

                                        notification_message_tr = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + " size özel takım isteği gönderdi. Takım isteğini bildirimleriniz kısmından cevaplayabilirsiniz."
                                        notification_message_en = get_profile_name_str(
                                            users_fullprofiles.full_profile.last_name,
                                            users_fullprofiles.full_profile.first_name) + " sent a private team request. You can send an answer from your notifications area."

                                        payload = {
                                            "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                                            "include_player_ids": send_signals_to_projects_owner(user_id),
                                            "contents": {"en": notification_message_en, "tr": notification_message_tr},
                                            "url": "/dashboard/"
                                        }

                                        requests.post("https://onesignal.com/api/v1/notifications",
                                                      headers=settings.ONESIGNAL_HEADER, data=json.dumps(payload))

                                    except:
                                        context = {
                                            "message_type": "error",
                                            "message": ugettext("There was a problem in sending team request.")
                                        }

                                else:
                                    # ekstra kişi de ekleyemiyorum
                                    if sended_extra is True:
                                        if request.LANGUAGE_CODE == "tr":
                                            message = "Eğer daha fazla kişiye teklif göndermek istiyorsanız, gönderdiğiniz özel teklifleri <u>takımlarım</u> bölümünden iptal edebilirsiniz veya ücretli üyeliklerimizden yararlanabilirsiniz."
                                        else:
                                            message = "If you want to send more team requests to the people in your location, you can cancel your previously submitted team requests from <u>my teams</u> section. Or you can benefit our paid memberships."
                                    else:
                                        if request.LANGUAGE_CODE == "tr":
                                            message = "Bulunduğunuz üyelik paketinde takımınıza başka bir kişiyi dahil edemezsiniz. Şu an projenizde yer alan takımda sizinle birlikte " + str(
                                                created_team.usersteams_set.count()) + " adet kişi bulunuyor. Projenize daha fazla kişi eklemek için bir üst üyelik paketinden faydalanmanız gerekmektedir."
                                        else:
                                            message = "You have " + str(
                                                created_team.usersteams_set.count()) + " people on your project with you and you can't include another person in this membership. You should benefit our 'Growing' membership package"

                                    context = {
                                        "message_type": "error",
                                        "message": message,
                                    }

                    else:
                        # daha önceden istek göndermişim
                        context = {
                            "message_type": "success",
                            "message": ugettext(
                                "You had already sent a team request this person. You should be patient or you can send a message."),
                        }

                return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def request_dm_project(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            dm_id = request.GET.get("id")
            selected_dm_reuqest = UsersDmProjectsTeamrequests.select_this_by_id(dm_id)
            UsersDmProjectsTeamrequests.read_this_request_by_id(dm_id)  # read
            project_logo = selected_dm_reuqest.users_projects.projects.general_infos.project_logo.name
            project_title = selected_dm_reuqest.users_projects.projects.general_infos.title
            if len(selected_dm_reuqest.users_projects.projects.general_infos.describe) > 150:
                project_content = selected_dm_reuqest.users_projects.projects.general_infos.describe[:150] + "..."
            else:
                project_content = selected_dm_reuqest.users_projects.projects.general_infos.describe

            project_location = get_fulllocation_string(
                selected_dm_reuqest.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                selected_dm_reuqest.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + " - " + selected_dm_reuqest.users_projects.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country

            project_point = ProjectPointsScores.calculate_points(selected_dm_reuqest.users_projects.projects)

            context = {
                "project_title": project_title,
                "project_logo": project_logo,
                "project_content": project_content,
                "project_address": "/dashboard/projects/detail/" + str(
                    selected_dm_reuqest.users_projects.projects.id) + "/",
                "project_location": project_location,
                "project_point": project_point,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def dm_project_supports(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            dm_id = request.GET.get("id")
            selected_dm = UsersDmProjectsTeamrequests.select_this_by_id(dm_id)
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

            if request.LANGUAGE_CODE == "tr":
                if not selected_dm.teamrequests.users_request_projecthours is None:
                    project_hours = clean_hours_title(
                        selected_dm.teamrequests.users_request_projecthours.project_hours.title, "saat")
                else:
                    project_hours = clean_hours_title(users_fullprofile.usersprojecthours_set.get().project_hours.title,
                                                      "saat")

                if not selected_dm.teamrequests.users_request_investments is None:
                    investments = selected_dm.teamrequests.users_request_investments.investments.title
                else:
                    investments = UsersInvestments.select_users_investment(users_fullprofile).investments.title
            else:

                if not selected_dm.teamrequests.users_request_projecthours is None:
                    project_hours = clean_hours_title(
                        selected_dm.teamrequests.users_request_projecthours.project_hours.title_eng, "hours")
                else:
                    project_hours = clean_hours_title(
                        users_fullprofile.usersprojecthours_set.get().project_hours.title_eng, "hours")

                if not selected_dm.teamrequests.users_request_investments is None:
                    investments = selected_dm.teamrequests.users_request_investments.investments.title_eng
                else:
                    investments = UsersInvestments.select_users_investment(users_fullprofile).investments.title_eng

            context = {
                "project_hour": project_hours,
                "project_hour_id": users_fullprofile.usersprojecthours_set.get().project_hours.id,
                "support_type": investments,
                "support_type_id": UsersInvestments.select_users_investment(users_fullprofile).investments.id,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def save_projecthours(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            dm_id = request.GET.get("id")
            prj_hours_id = request.GET.get("selected_value")

            selected_dm = UsersDmProjectsTeamrequests.select_this_by_id(dm_id)
            selected_project_hours = ProjectHours.select_project_hours(prj_hours_id)

            if selected_dm.teamrequests.users_request_projecthours is None:
                created_request_project_hours = UsersRequestProjectHours.create(selected_project_hours, 0)
            else:
                created_request_project_hours = UsersRequestProjectHours.create(selected_project_hours,
                                                                                selected_dm.teamrequests.users_request_projecthours.id)

            created_teamrequest = Teamrequests.create(0, created_request_project_hours, selected_dm.teamrequests.id)

            UsersProjectsTeamrequests.create(selected_dm.to_users_fullprofiles, selected_dm.users_projects,
                                             created_teamrequest)

            if request.LANGUAGE_CODE == "tr":
                project_hours = clean_hours_title(created_request_project_hours.project_hours.title, "saat")
            else:
                project_hours = clean_hours_title(created_request_project_hours.project_hours.title_eng, "hours")

            context = {
                "project_hours": project_hours,
                "project_hours_id": created_request_project_hours.project_hours.id,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def save_investments(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            dm_id = request.GET.get("id")
            investments_id = request.GET.get("selected_value")
            selected_dm = UsersDmProjectsTeamrequests.select_this_by_id(dm_id)
            selected_investments = Investments.select_invest_type(investments_id)
            message = ""

            if "paid-support" in selected_investments.title_code and UsersHourCosts._isUsersHourCostsClear(
                    selected_dm.to_users_fullprofiles) is False:
                message = ugettext("You should set your hourly rate from profile settings.")
                selected_investments = Investments.select_by_code("work-support")

            if selected_dm.teamrequests.users_request_investments is None:
                created_request_investments = UsersRequestInvestments.create(selected_investments, 0)
            else:
                created_request_investments = UsersRequestInvestments.create(selected_investments,
                                                                             selected_dm.teamrequests.users_request_investments.id)

            created_teamrequest = Teamrequests.create(created_request_investments, 0, selected_dm.teamrequests.id)
            UsersProjectsTeamrequests.create(selected_dm.to_users_fullprofiles, selected_dm.users_projects,
                                             created_teamrequest)

            if request.LANGUAGE_CODE == "tr":
                project_investments = created_request_investments.investments.title
            else:
                project_investments = created_request_investments.investments.title_eng

            context = {
                "investments": project_investments,
                "investments_id": created_request_investments.investments.id,
                "message": message,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def send_asnwer_dm_teamrequest(request):
    if "user_fullprofile" in request.session:
        if request.method == "POST":
            dm_id = request.POST.get("id")
            investment_id = request.POST.get("investment")
            project_hour_id = request.POST.get("project_hour")
            selected_dm = UsersDmProjectsTeamrequests.select_this_by_id(dm_id)

            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            # try:
            if selected_dm.teamrequests.users_request_investments is None:
                created_request_investments = UsersRequestInvestments.create(
                    Investments.select_invest_type(investment_id), 0)
                created_teamrequest = Teamrequests.create(created_request_investments, 0,
                                                          selected_dm.teamrequests.id)
                UsersProjectsTeamrequests.create(selected_dm.to_users_fullprofiles, selected_dm.users_projects,
                                                 created_teamrequest)

            if selected_dm.teamrequests.users_request_projecthours is None:
                created_request_project_hours = UsersRequestProjectHours.create(
                    ProjectHours.select_project_hours(project_hour_id), 0)
                created_teamrequest = Teamrequests.create(0, created_request_project_hours,
                                                          selected_dm.teamrequests.id)
                UsersProjectsTeamrequests.create(selected_dm.to_users_fullprofiles, selected_dm.users_projects,
                                                 created_teamrequest)

            UsersProjectsTeamrequests.confirm_teamrequest(users_fullprofile, selected_dm.users_projects)
            UsersDmProjectsTeamrequests.response_this_request_by_usersprojects(users_fullprofile,
                                                                               selected_dm.users_projects)

            # bu kısımda email bilgilendirme karşı tarafa - bu kısımda bize gönderilen özel isteğe
            # koşullarımızı belirterek özel isteğe  cevap atıyoruz
            # bize mailde gerekenler : proje adı, teklifi değerlendirenin adı-soyadı
            if dm_response_to_owner_mail(users_fullprofile.id,
                                         selected_dm.users_projects.projects.general_infos.title,
                                         selected_dm.users_fullprofiles.id):
                # email gönderildi
                message = ugettext("You have successfully submitted your offer.")
            else:
                # email gönderiminde bir hata meydana geldi
                message = ugettext(
                    "You have successfully submitted your offer but there was a problem during email notification to user.")

            context = {
                "message_type": "success",
                "message": message,
            }

            # burdayım

            notification_message_tr = get_profile_name_str(users_fullprofile.full_profile.last_name,users_fullprofile.full_profile.first_name) + " gönderdiğiniz özel takım isteğini, kendi şartlarını belirterek cevapladı. Takım istekleri bölümünden onaylayabilirsiniz."

            notification_message_en = get_profile_name_str(users_fullprofile.full_profile.last_name,users_fullprofile.full_profile.first_name) + " replied your private team request. You can confirm the sended team request from your team requests section."

            payload = {
                "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                "include_player_ids": send_signals_to_projects_owner(selected_dm.users_projects.users_fullprofiles.id),
                "contents": {"en": notification_message_en, "tr": notification_message_tr},
                "url": "/dashboard/myprojects/requests/" + str(selected_dm.users_projects.id) + "/"
            }

            requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,
                          data=json.dumps(payload))

        # except:
        #     context = {
        #         "message_type": "error",
        #         "message": ugettext("Encountered an error .You can try later or you can send mail to us."),
        #     }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
def profile_membership_types(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            # burda bize lazım olacak olanlar - hem posttan hem projeden takım seçimi yapılırken
            # 1-  membership paketim
            # 2 - takıma alacağım kişi sayılarının limiti
            team_person_limit = MembershipTermsType.select_terms_type(
                users_fullprofile.users.users_membershiptypes.name_code, "person-to-team").limit_count
            extra_person_limit = MembershipTermsType.select_terms_type(
                users_fullprofile.users.users_membershiptypes.name_code, "person-to-team-location").limit_count

            if team_person_limit == -1:
                if request.LANGUAGE_CODE == "tr":
                    message = "Bulunduğunuz üyelik paketinde oluşturacağınız takıma istediğiniz kadar kişi seçebilirsiniz."
                else:
                    message = "You can choose as many person as you for your team."
            else:
                if request.LANGUAGE_CODE == "tr":
                    message = "Bulunduğunuz üyelik paketinde oluşturacağınız takıma en fazla " + str(
                        team_person_limit) + " kişi seçebilirsiniz. Fakat hedeflerimiz içerisinde; projenin bulunduğu konumda yer alan kişilerin de başarıya ortak olmasını istediğimiz için, eğer seçtiğiniz kişiler içerisinde kendi konumunuzdan biri yok ise kendi konumunuzda yer alan " + str(
                        extra_person_limit) + " kişiyi takımınıza dahil edebilirsiniz. Eğer kendi konumuzdan size gelen bir takım isteği yok ise; konumunuzda olan kişilere siz özel takım isteği gönderebilirsiniz."
                else:
                    message = "You can include up to " + str(
                        team_person_limit) + " people in your current membership package. However, we want to discover more successful people in your location. Therefore, if you don't have choose a person in your location, you can include " + str(
                        extra_person_limit) + " more person from at your location to your team. So, you can choose " + str(
                        (
                            team_person_limit + extra_person_limit)) + " people while creating a team. If anybody didn't send a team request in your location, you can send a private team request to the people in your location."

            context = {
                "membership_name": users_fullprofile.users.users_membershiptypes.name_code,
                "team_person": team_person_limit,
                "more_person": extra_person_limit,
                "my_location": change_characters(
                    users_fullprofile.full_profile.fullprofilelocation_set.get().location.city),
                "message": message,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@cache_page(60 * 5)
def dmmessage_person_info(request):
    if "user_fullprofile" in request.session:
        if request.method == "GET":
            # users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            users_id = request.GET.get("id")
            selected_user_projefile = UsersFullProfile.select_user(users_id)

            if not "default" in selected_user_projefile.full_profile.picture_url.name:
                profile_picture = selected_user_projefile.full_profile.picture_url.url

            else:
                profile_picture = selected_user_projefile.full_profile.linkedin_picture_url

            if "entrepreneur" in selected_user_projefile.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    position_title = "Girişimci"
                else:
                    position_title = selected_user_projefile.users.member_type
            else:
                if len(selected_user_projefile.full_profile.position_title) > 0:
                    position_title = selected_user_projefile.full_profile.position_title
                else:
                    position_title = UsersIndustries.select(selected_user_projefile).industries.description
                    # position_title = selected_user_projefile.usersındustries_set.get().industries.description

            context = {
                "profile_name": get_profile_name_str(selected_user_projefile.full_profile.last_name,
                                                     selected_user_projefile.full_profile.first_name),
                "profile_image": profile_picture,
                "position_title": position_title,
                "profile_location": get_fulllocation_string(
                    selected_user_projefile.full_profile.fullprofilelocation_set.get().location.district,
                    selected_user_projefile.full_profile.fullprofilelocation_set.get().location.city),
                "profile_point": UsersPointScores.calculate_points(selected_user_projefile),
            }

            return HttpResponse(json.dumps(context), content_type="application/json")


@never_cache
@csrf_protect
def delete_avatar(request):
    if "user_fullprofile" in request.session:
        if request.method == "POST":
            id = request.POST.get("id")
            selected_user = UsersFullProfile.select_user(id)

            if UsersFullProfile.select_user(request.session["user_fullprofile"]).id != selected_user.id:
                context = {
                    "message_type": "error",
                    "message": ugettext("This profile doesn't belong to you"),
                }
            else:
                if delete_project_logo("/media" + selected_user.full_profile.picture_url.name):
                    selected_user.full_profile.delete_avatar_image(selected_user.full_profile.id)
                    context = {
                        "message_type": "success",
                        "message": ugettext("Profile image removed succesfully."),
                        "default_avatar": "/media/profiles/default.png",
                    }
                else:
                    context = {
                        "message_type": "error",
                        "message": ugettext("An error was encountered while deleting the profile image."),
                    }

            return HttpResponse(json.dumps(context), content_type="application/json")
