from django.db import models
from django.contrib.auth.hashers import make_password, check_password
from django.db.models import Sum

class Users(models.Model):
    username = models.CharField(max_length=250, blank=True)
    users_membershiptypes = models.ForeignKey('memberships.MembershipTypes', models.DO_NOTHING, blank=True)
    email_address = models.CharField(max_length=350, blank=True)
    member_type = models.CharField(max_length=100, blank=True)
    joined_date = models.DateTimeField(auto_now_add=True)
    login_date = models.DateTimeField(auto_now_add=True)
    memberships_renew_date=models.DateTimeField(null=True)
    is_active = models.NullBooleanField()
    password = models.CharField(max_length=128)
    random_pass = models.CharField(max_length=128, blank=True)
    change_req = models.BooleanField()
    id = models.BigAutoField(primary_key=True)

    def __str__(self):
        return self.email_address

    class Meta:
        managed = True
        db_table = 'users'

    @classmethod
    def update_user_membertype(cls,id,member_type):
        import datetime
        selected=cls.objects.get(id=id)
        selected.member_type = member_type
        selected.joined_date = datetime.datetime.now()
        selected.save()
        return selected

    @classmethod
    def update_membership_dates(cls,id):
        import datetime
        selected=cls.objects.filter(id=id).get()
        selected.memberships_renew_date=datetime.datetime.now()
        selected.save()
        return selected

    @classmethod
    def create(cls, username, users_membershiptypes, email_address, member_type, is_active, password):
        user = cls(username=username, users_membershiptypes=users_membershiptypes, email_address=email_address,
                   member_type=member_type, is_active=is_active, password=password, change_req=False, random_pass="")
        user.save()
        return user

    @classmethod
    def create_randompass(cls, random_pass, id):
        user = cls.objects.filter(id=id).get()
        user.random_pass = random_pass
        user.change_req = True
        user.save()
        return user

    @classmethod
    def _isPasswordChangeRequest(cls, random_pass, id):
        if cls.objects.filter(id=id).exists():
            # user = cls.objects.filter(id=id).get()
            if (cls.objects.filter(id=id, random_pass=random_pass, change_req=True).exists()):
                return True
            else:
                return False
        else:
            return False

    @classmethod
    def change_password(cls, user_id, password):
        user = cls.objects.filter(id=user_id).get()
        user.password = make_password(password=password, salt=None, hasher='pbkdf2_sha1')
        user.change_req = False
        user.random_pass = ""
        user.save()
        return user

    @classmethod
    def list_of_users_membertype(cls, member_type):
        return cls.objects.filter(member_type=member_type, is_active=True).count()

    @classmethod
    def list_of_nearusers_membertype(cls, member_type, user_id):
        from utils.views import is_it_same_location
        counter_users = 0
        try:
            users_list = cls.objects.filter(member_type=member_type, is_active=True).all()
            for user in users_list:
                if user_id != user.usersfullprofile_set.get().id:
                    if is_it_same_location(user_id,user.usersfullprofile_set.get().id):
                        counter_users += 1
        except:
            pass

        return counter_users




# bu kısım otomatik alınamıyor
class UsersCertifications(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    certifications = models.ForeignKey('linkedin_infos.Certifications', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_certifications'


# position kısmı var ise doldurulacak
class UsersCompanyInfos(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    company_infos = models.ForeignKey('linkedin_infos.CompanyInfos', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_company_infos'

    @classmethod
    def create(cls, users_fullprofiles, company_infos):
        users_company = cls(users_fullprofiles=users_fullprofiles, company_infos=company_infos)
        users_company.save()
        return users_company

    @classmethod
    def isThereExperience(cls, users_fullprofiles):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
            return True
        else:
            return False

    @classmethod
    def select_all(cls, users_fullprofiles):
        users_company = cls.objects.filter(users_fullprofiles=users_fullprofiles).all()
        return users_company

    @classmethod
    def select(cls, id):
        selected = cls.objects.filter(id=id).get()
        return selected


# bu kısım dolduralamıyor
class UsersCourses(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    courses = models.ForeignKey('linkedin_infos.Courses', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_courses'


# bu kısım dolduralamıyor
class UsersEducations(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    educations = models.ForeignKey('linkedin_infos.Educations', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_educations'


# bu kısım dolduralamıyor
class UsersHonorsAwards(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    honors_awards = models.ForeignKey('linkedin_infos.HonorsAwards', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_honors_awards'


class UsersIndustries(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    industries = models.ForeignKey('linkedin_infos.Industries', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_industries'

    @classmethod
    def create(cls, users_fullprofiles, industries):
        user_industry = cls(users_fullprofiles=users_fullprofiles, industries=industries)
        user_industry.save()
        return user_industry

    @classmethod
    def select(cls, users_fullprofiles):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles).get()


# bu kısım dolduralamıyor
class UsersLanguagesKnown(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    languages_known = models.ForeignKey('linkedin_infos.LanguagesKnown', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_languages_known'


# bu kısım dolduralamıyor
class UsersLinkedinRecommendations(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    linkedin_recommendations = models.ForeignKey('linkedin_infos.LinkedinRecommendations', models.DO_NOTHING,
                                                 blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_linkedin_recommendations'

    @classmethod
    def is_there_signal_id(cls,users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def select_users_onesignal(cls,users_fullprofiles):
        try:
            if UsersLinkedinRecommendations.is_there_signal_id(users_fullprofiles) == True:
                return cls.objects.filter(users_fullprofiles=users_fullprofiles).order_by("-id").get()
        except:
            return False


    @classmethod
    def create_users_onesignal(cls,users_fullprofiles,linkedin_recommendations):
        new_record = cls(users_fullprofiles=users_fullprofiles, linkedin_recommendations=linkedin_recommendations)
        new_record.save()
        return new_record


# bu kısım daha sonra statik olarak doldurulacak
class UsersSkills(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    skills = models.ForeignKey('linkedin_infos.Skills', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_skills'

    @classmethod
    def create(cls, users_profiles, skills):
        if cls.objects.filter(users_fullprofiles=users_profiles, skills=skills).exists():
            select = cls.objects.filter(users_fullprofiles=users_profiles, skills=skills).get()
            select.skills = skills
            select.save()
            return select
        else:
            skills = cls.objects.create(users_fullprofiles=users_profiles, skills=skills)
            return skills

    @classmethod
    def remove(cls, users_fullprofiles, skills):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles, skills=skills).exists():
            return cls.objects.filter(users_fullprofiles=users_fullprofiles, skills=skills).delete()

    @classmethod
    def count_skills(cls, users_fullprofiles):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles).count()

    @classmethod
    def select_all(cls, users_fullprofiles):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles).all()


class UsersFullProfile(models.Model):
    users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    full_profile = models.ForeignKey('myprofile.FullProfile', models.DO_NOTHING, blank=True)
    crypto_user_name=models.CharField(max_length=10, blank=True)
    id = models.BigAutoField(primary_key=True)

    def __str__(self):
        return ("%s %s" % (self.full_profile.first_name,self.full_profile.last_name))
        # return '%s %s {}'.format(self.full_profile.first_name, self.full_profile.last_name)

    class Meta:
        managed = True
        db_table = 'users_full_profile'

    @classmethod
    def get_users(cls, member_type):
        try:
            return cls.objects.filter(users__is_active=True,users__member_type=member_type).all()[:4]
        except:
            pass

    @classmethod
    def all_active_users(cls):
        return cls.objects.filter(users__is_active=True).all()


    @classmethod
    def near_users(cls, users_fullprofiles, language_code):
        from utils.views import is_it_same_location,get_person_profile_name
        users_list = cls.objects.filter(users__is_active=True).all().order_by("-id")
        members_list = []
        for profile in users_list:
            if is_it_same_location(users_fullprofiles,profile.id) and profile.id is not users_fullprofiles:
                if not "default" in profile.full_profile.picture_url.name:
                    profile_picture = profile.full_profile.picture_url.url
                else:
                    profile_picture = profile.full_profile.linkedin_picture_url
                    # print(profile_picture)

                profile_name=get_person_profile_name(users_fullprofiles,profile.id)
                # profile_name = get_profile_name_str(profile.full_profile.last_name, profile.full_profile.first_name)

                if "entrepreneur" in profile.users.member_type:
                    if language_code == "tr":
                        position_title = "Girişimci"
                    else:
                        position_title = profile.users.member_type
                else:
                    if len(profile.full_profile.position_title) > 0:
                        position_title = profile.full_profile.position_title
                    else:
                        position_title=UsersIndustries.select(profile).industries.description


                item = {
                    "profile_picture": profile_picture,
                    "profile_name": profile_name,
                    "position_title": position_title,
                    "id": profile.id,
                    "member_type": profile.users.member_type,
                }

                members_list.append(item)
        return members_list

    @classmethod
    def _emailAndPassIsOk(cls, emailaddress, password):
        if cls.objects.filter(users__email_address=emailaddress).exists():
            selected = cls.objects.filter(users__email_address=emailaddress).get()
            if check_password(password, selected.users.password):
                return True
            else:
                return False
        else:
            return False

    @classmethod
    def create(cls, users, full_profile,crypto_user_name):
        user = cls(users=users, full_profile=full_profile,crypto_user_name=crypto_user_name)
        user.save()
        return user

    @classmethod
    def selectUsersFullProfileByUserId(cls, user_id):
        user = cls.objects.filter(users_id=user_id).get()
        return user

    @classmethod
    def select_user(cls, id):
        user = cls.objects.filter(id=id).get()
        return user

    @classmethod
    def _userIsActive(cls, id):
        if cls.objects.filter(id=id, users__is_active=True).exists():
            return True
        else:
            return False

    @classmethod
    def get_user_byemail(cls, emailaddress):
        user = cls.objects.filter(users__email_address=emailaddress).get()
        return user

    @classmethod
    def is_there_user(cls, emailaddress):
        if cls.objects.filter(users__email_address=emailaddress).exists():
            return True
        else:
            return False

    @classmethod
    def is_there_user_by_cookie(cls,id):
        try:
            if cls.objects.filter(id=id).exists():
                return True
            else:
                return False
        except:
            return False


class UsersMembershipTermsType(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    mtt = models.ForeignKey('memberships.MembershipTermsType', models.DO_NOTHING, blank=True)
    limit_count = models.IntegerField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_membership_terms_type'

    @classmethod
    def create(cls, users_fullprofiles, term_code, membership_type_name_code):
        from memberships.models import MembershipTermsType
        # kayıt aşaması :
        # 1 - şu kullanıcı şu pakete ait şu term'i ni limit sayısını girerek oluştur
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles,mtt=MembershipTermsType.select_terms_type(membership_type_name_code,term_code)).exists():
                selected = cls.objects.filter(users_fullprofiles=users_fullprofiles,
                                              mtt=MembershipTermsType.select_terms_type(membership_type_name_code,
                                                                                        term_code)).get()
                selected.limit_count += 1
                selected.save()
                return selected
            else:
                new_record = cls(users_fullprofiles=users_fullprofiles,
                                 mtt=MembershipTermsType.select_terms_type(membership_type_name_code, term_code),
                                 limit_count=1)
                new_record.save()
                return new_record
        except:
            new_record = cls(users_fullprofiles=users_fullprofiles,
                             mtt=MembershipTermsType.select_terms_type(membership_type_name_code, term_code),
                             limit_count=1)
            new_record.save()
            return new_record

    @classmethod
    def user_package_limitsize(cls, users_fullprofiles, term_code, membership_type_code):
        from memberships.models import MembershipTermsType
        try:
            return cls.objects.filter(mtt=MembershipTermsType.select_terms_type(membership_type_code, term_code),
                                      users_fullprofiles=users_fullprofiles).get().limit_count
        except:
            return cls.objects.filter(mtt=MembershipTermsType.select_terms_type(membership_type_code, term_code),
                                      users_fullprofiles=users_fullprofiles).aggregate(Sum("limit_count"))

    @classmethod
    def reset_membership_terms(cls,users_fullprofiles,term_code,membership_type_name):
        from memberships.models import MembershipTermsType,MembershipTypes
        # bu kısımda mantıksal bir hatam olmuş veritabanı eşleştirme kısmında.
        # hata : eğer kullanıcı daha önceden growing paket ise ve limitler buna göre kayıt edildiyse,
        # daha sonradan connected paket alındysa - mevcut pakete göre limitleri sıfırlıyor
        # fakat biz daha önceden alınan üyeliklerin de limitlerini sıfırlamalıyız.
        # örn : growing paketteki üyelerin detayını görme özelliğinin id verisi ile connected paketin üyeleri görme
        # id numarası farklı olduğu için hepsini sıfırlamalıyız
        membership_types_list = MembershipTypes.select_all_package()
        successful_count = 0
        try:
            for membership_type in membership_types_list:
                if cls.objects.filter(mtt=MembershipTermsType.select_terms_type(membership_type.name_code, term_code),users_fullprofiles=users_fullprofiles).exists():
                    if membership_type_name != membership_type.name_code:
                        cls.objects.filter(mtt=MembershipTermsType.select_terms_type(membership_type.name_code, term_code),users_fullprofiles=users_fullprofiles).delete()

                    else:
                        selected = cls.objects.filter(mtt=MembershipTermsType.select_terms_type(membership_type.name_code, term_code),users_fullprofiles=users_fullprofiles).get()
                        selected.limit_count = 0
                        selected.save()
                        successful_count += 1




            if successful_count > 0:
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def select_all(cls,users_fullprofiles):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles).all()

    @classmethod
    def isOver_limitsize(cls, users_fullprofiles, term_code, membership_type_name_code):
        # sistem tarafından belirtilen limit aşıldı mı?
        # kullanıcının membership_types'ı alınıyor.
        from memberships.models import MembershipTermsType

        try:
            if -1 == MembershipTermsType.select_terms_type(membership_type_name_code, term_code).limit_count:
                # limitsiz anlamına geliyor
                # print("limitsiz")
                return False
            elif 0 == MembershipTermsType.select_terms_type(membership_type_name_code, term_code).limit_count:
                return True
            else:
                if MembershipTermsType.select_terms_type(membership_type_name_code,term_code).limit_count == cls.objects.filter(mtt=MembershipTermsType.select_terms_type(membership_type_name_code, term_code),users_fullprofiles=users_fullprofiles).get().limit_count:
                    return True
                else:
                    return False
        except:
            return False

    @classmethod
    def is_there_any_terms(cls,users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False


class UsersMembershipsTermsAreas(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    mtt = models.ForeignKey('memberships.MembershipTermsType', models.DO_NOTHING, blank=True)
    area_path = models.CharField(max_length=150, blank=True)
    active_size = models.CharField(max_length=500, blank=True)
    total_size = models.CharField(max_length=500, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(blank=True)

    class Meta:
        managed = True
        db_table = 'users_memberships_terms_areas'


class UsersOptions(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    options = models.ForeignKey('mainadmins.Options', models.DO_NOTHING, blank=True)
    status = models.NullBooleanField()
    created_date = models.DateTimeField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_options'


class UsersAnswers(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    users_posts = models.ForeignKey('UsersPosts', models.DO_NOTHING, blank=True)
    answers = models.ForeignKey('answers.Answers', models.DO_NOTHING, blank=True)
    is_block = models.BooleanField(auto_created=False)
    is_active = models.BooleanField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_answers'

    @classmethod
    def post_answers_count(cls, posts):
        try:
            return cls.objects.filter(users_posts__posts=posts).count()
        except:
            return 0

    @classmethod
    def create(cls, answers, users_posts, users_fullprofiles):
        new_record = cls(answers=answers, users_posts=users_posts, users_fullprofiles=users_fullprofiles,
                         is_block=False, is_active=True)
        new_record.save()

        return new_record

    @classmethod
    def answers_by_post(cls, users_posts):
        return cls.objects.filter(users_posts=users_posts, is_block=False, is_active=True).all().order_by("id")

    @classmethod
    def select_answer(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def main_answers_count(cls):
        main_answers = cls.objects.filter(is_active=True, is_block=False).all()[:10]
        counter = 0
        try:
            for item in main_answers:
                if not UsersAnswersAnswers.isThereSubAnswer_by_answer(item.answers):
                    counter += 1
            return counter
        except:
            return counter

    @classmethod
    def main_answers_hasNext(cls, users_posts):
        main_answers = cls.objects.filter(is_active=True, is_block=False, users_posts=users_posts).all()[:10]
        counter = 0
        try:
            for item in main_answers:
                if not UsersAnswersAnswers.isThereSubAnswer_by_answer(item.answers):
                    counter += 1
            return counter
        except:
            return counter


# bir post'ta cevaba cevap yazılması işlemi
class UsersAnswersAnswers(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    answers = models.ForeignKey('answers.Answers', models.DO_NOTHING, blank=True)
    users_answers = models.ForeignKey('UsersAnswers', models.DO_NOTHING, blank=True)
    is_block = models.BooleanField(auto_created=False)
    is_active = models.BooleanField(auto_created=True)

    class Meta:
        managed = True
        db_table = "users_answers_answers"

    @classmethod
    def select_by_id(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def isThere(cls, users_answers):
        try:
            if cls.objects.filter(users_answers=users_answers, is_active=True, is_block=False).count() > 0:
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def answers_by_answer(cls, users_answers):
        return cls.objects.filter(users_answers=users_answers, is_block=False, is_active=True).all().order_by("id")

    @classmethod
    def create_ans_ans(cls, answers, users_answers, users_fullprofiles):
        new_ans = cls(answers=answers, users_answers=users_answers, users_fullprofiles=users_fullprofiles,
                      is_active=True, is_block=False)
        new_ans.save()
        return new_ans

    @classmethod
    def isThereSubAnswer_by_answer(cls, answers):
        try:
            if cls.objects.filter(answers=answers, is_active=True, is_block=False).exists():
                return True
            else:
                return False
        except:
            return False


class UsersAnswersComplained(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    answers_complained = models.ForeignKey('answers.AnswersComplained', models.DO_NOTHING, blank=True)
    is_read = models.NullBooleanField()
    admin_answer_text = models.TextField(blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateTimeField(auto_created=True)
    up_date = models.DateTimeField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_answers_complained'


class UsersAnswerslike(models.Model):
    answers = models.ForeignKey('answers.Answers', models.DO_NOTHING, blank=True)
    # user = models.ForeignKey('Users', models.DO_NOTHING)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    is_like = models.NullBooleanField()
    created_date = models.DateTimeField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_answerslike'


class UsersFriend(models.Model):
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True,
                                           related_name='main_users_fkey')
    to_users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True,
                                              related_name='to_users_fkey')
    created_date = models.DateTimeField(auto_now_add=True)
    is_block = models.NullBooleanField(default=False)
    # block_date = models.DateTimeField(blank=True,null=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_friend'

    @classmethod
    def are_these_friends(cls,users_fullprofiles_id,to_users_fullprofiles_id):
        owner_user=UsersFullProfile.select_user(users_fullprofiles_id)
        other_user=UsersFullProfile.select_user(to_users_fullprofiles_id)
        if owner_user.id == other_user.id:
            return True
        elif cls.objects.filter(users_fullprofiles=owner_user, to_users_fullprofiles=other_user).exists():
            return True
        else:
            return False
        # try:
        #     if owner_user.id == other_user.id:
        #         return True
        #     elif cls.objects.filter(users_fullprofiles=owner_user,to_users_fullprofiles=other_user).exists():
        #         return True
        #     else:
        #         return False
        # except:
        #     return False


    @classmethod
    def make_friends(cls, users_fullprofiles_id, to_users_fullprofiles_id):
        owner_user = UsersFullProfile.select_user(users_fullprofiles_id)
        other_user = UsersFullProfile.select_user(to_users_fullprofiles_id)
        # if not cls.objects.filter(users_fullprofiles=owner_user, to_users_fullprofiles=other_user).exists():
        #     new_record = cls.objects.bulk_create(users_fullprofiles=owner_user, to_users_fullprofiles=other_user)
        #     # new_record = cls(users_fullprofiles=owner_user, to_users_fullprofiles=other_user)
        #     new_record.save()
        #     return new_record
        try:
            if not cls.objects.filter(users_fullprofiles=owner_user,to_users_fullprofiles=other_user).exists():
                new_record = cls(users_fullprofiles=owner_user, to_users_fullprofiles=other_user)
                new_record.save()
                return new_record
            else:
                # selected=cls.objects.filter(users_fullprofiles=users_fullprofiles,to_users_fullprofiles=to_users_fullprofiles).get()
                # return selected
                pass
        except:
            new_record = cls(users_fullprofiles=owner_user, to_users_fullprofiles=other_user)
            new_record.save()
            return new_record

    @classmethod
    def block_friend(cls, users_fullprofiles, to_users_fullprofiles):
        try:
            from datetime import datetime
            if cls.objects.filter(users_fullprofiles=users_fullprofiles,to_users_fullprofiles=to_users_fullprofiles).exists():
                selected = cls.objects.filter(users_fullprofiles=users_fullprofiles,
                                              to_users_fullprofiles=to_users_fullprofiles).get()
                selected.is_block = True
                selected.block_date = datetime.now()
                selected.save()
                return selected
            else:
                selected=cls.objects.filter(users_fullprofiles=to_users_fullprofiles,to_users_fullprofiles=users_fullprofiles).get()
                selected = cls.objects.filter(users_fullprofiles=users_fullprofiles,
                                              to_users_fullprofiles=to_users_fullprofiles).get()
                selected.is_block = True
                selected.block_date = datetime.now()
                selected.save()
                return selected
        except:
            # return False
            pass

    @classmethod
    def is_there_any_friend(cls, users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, is_block=False).count() > 0:
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def is_there_any_friend_toadded(cls, users_fullprofiles):
        try:
            if cls.objects.filter(is_block=False, to_users_fullprofiles=users_fullprofiles).count() > 0:
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def friend_list(cls, users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, is_block=False).exists():
                return cls.objects.filter(users_fullprofiles=users_fullprofiles, is_block=False).all().order_by(
                    "-created_date")
            else:
                return cls.objects.filter(to_users_fullprofiles=users_fullprofiles, is_block=False).all().order_by("-created_date")
        except:
            pass

    @classmethod
    def isThis_my_friend(cls, users_fullprofiles, to_users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, to_users_fullprofiles=to_users_fullprofiles,
                                  is_block=False).count() > 0 or cls.objects.filter(
                users_fullprofiles=to_users_fullprofiles, to_users_fullprofiles=users_fullprofiles,
                is_block=False).count() > 0:
                return True
            else:
                return False
        except:
            return False


class UsersMessages(models.Model):
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    messages = models.ForeignKey('users_messagebox.Messages', models.DO_NOTHING, blank=True, related_name="fk_messages")
    parent_message = models.ForeignKey('users_messagebox.Messages', models.DO_NOTHING, blank=True,
                                       related_name="fk_parent_messages", null=True)

    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_messages'



    @classmethod
    def save_to_trash(cls,id,users_fullprofiles):
        try:
            if cls.objects.filter(id=id).get().users_fullprofiles == users_fullprofiles:
                if cls.objects.filter(id=id).get().is_trash == False:
                    selected= cls.objects.filter(id=id).get()
                    selected.is_trash=True
                    selected.save()
                    return selected
        except:
            pass

    @classmethod
    def is_there_active_message(cls, message, users_fullprofiles):
        try:
            if cls.objects.filter(messages__is_active=True, messages__to_users_fullprofiles=users_fullprofiles,
                                  messages=message,messages__is_delete=False,messages__is_trash=False).exists():
                # bu mesaj bana mı gelmiş ve hala aktif mi
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def new_message(cls, users_fullprofiles, messages, parent_message):
        if parent_message is None:
            new_record = cls(users_fullprofiles=users_fullprofiles, messages=messages, parent_message=None)
        else:
            new_record = cls(users_fullprofiles=users_fullprofiles, messages=messages, parent_message=parent_message)
        new_record.save()
        return new_record

    @classmethod
    def select_message(cls, messages):
        return cls.objects.filter(messages=messages).get()

    # @classmethod
    # def get_sub_messages(cls, messages):
    #     return cls.objects.filter(messages=messages).all().order_by("-id")
    @classmethod
    def get_sub_messages(cls, parent_message):
        return cls.objects.filter(parent_message=parent_message).all().order_by("-id")

    @classmethod
    def is_there_sub_message(cls, parent_message):
        try:
            if cls.objects.filter(parent_message=parent_message).count() > 0:
                return True
            else:
                return False
        except:
            # print("buraya gelmemesi lazım")
            return False

    @classmethod
    def selected_message_all_flow(cls, message):
        return cls.objects.filter(messages=message).all().order_by("-id")

    @classmethod
    def sended_messages_list(cls, users_fullprofiles,language):
        # from persons.models import UsersFriend, UsersMessages
        from utils.views import get_profile_name_str, remove_html_tags,get_person_profile_name
        from django.utils.timesince import timesince
        message_list = cls.objects.filter(users_fullprofiles=users_fullprofiles,messages__is_archive=False,messages__is_trash=False,messages__is_delete=False).all().order_by("-messages__send_date")
        context_messages = []
        for msg in message_list:

            if not "default" in msg.messages.to_users_fullprofiles.full_profile.picture_url.name:
                profile_picture = msg.messages.to_users_fullprofiles.full_profile.picture_url.url

            else:
                profile_picture = msg.messages.to_users_fullprofiles.full_profile.linkedin_picture_url

            if UsersFriend.isThis_my_friend(users_fullprofiles, msg.messages.to_users_fullprofiles):
                is_my_network = True
            else:
                is_my_network = False

            # sended_name = get_profile_name_str(msg.messages.to_users_fullprofiles.full_profile.last_name,
            #                                    msg.messages.to_users_fullprofiles.full_profile.first_name)
            sended_name=get_person_profile_name(msg.users_fullprofiles.id,msg.messages.to_users_fullprofiles.id)

            # bu kısımda eğer benim network'ümün dışında ise ismini gizli olarak göstereceğim
            if len(msg.messages.body_text) > 100:
                message_content = str(remove_html_tags(msg.messages.body_text))[:150] + "..."
            else:
                message_content = remove_html_tags(msg.messages.body_text)

            if len(msg.messages.title) > 0:
                title = msg.messages.title
            else:
                if language == "tr":
                    title = "yeni mesaj: " + str(remove_html_tags(msg.messages.body_text))[:15] + "..."
                else:
                    title="new message: " + str(remove_html_tags(msg.messages.body_text))[:15] + "..."

            item = {
                "profile_image": profile_picture,
                "is_network": is_my_network,
                "sender_name": sended_name,
                "message_content": message_content,
                "sended_date": timesince(msg.messages.send_date),
                "id": msg.messages.id,
                "title":title,
                # "is_readed":True,
            }
            context_messages.append(item)

        return context_messages
        # return cls.objects.filter(users_fullprofiles=users_fullprofiles).all().order_by("-id")


class UsersMessagesReplyMessages(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    reply_messages = models.ForeignKey('users_messagebox.ReplyMessages', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_messages_reply_messages'


class UsersTeamPages(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    team_pages = models.ForeignKey('myteams.TeamPages', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_team_pages'


# şu kullanıcı şu yatırım tipi ile belirtilen saat veya ücret karşılığında takım isteği attı
class Teamrequests(models.Model):
    users_request_investments = models.ForeignKey('UsersRequestInvestments', models.DO_NOTHING, null=True)
    users_request_projecthours = models.ForeignKey('UsersRequestProjectHours', models.DO_NOTHING, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(null=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'teamrequests'

    @classmethod
    def create_for_dm(cls, id):
        try:
            if not cls.objects.filter(id=id).exists():
                new_record = cls()
                new_record.save()
                return new_record
        except:
            new_record = cls()
            new_record.save()
            return new_record

    @classmethod
    def create(cls, users_request_investments, users_request_projecthours, id):
        try:
            from django.utils import timezone
            if cls.objects.filter(id=id).filter().exists():
                selected = cls.objects.filter(id=id).get()

                if users_request_investments is 0:
                    selected.users_request_projecthours = users_request_projecthours
                elif users_request_projecthours is 0:
                    selected.users_request_investments = users_request_investments
                else:
                    selected.users_request_investments = users_request_investments
                    selected.users_request_projecthours = users_request_projecthours

                selected.up_date = timezone.now()
                selected.save()
                return selected
            else:
                if users_request_investments == 0:
                    new_record = cls(users_request_projecthours=users_request_projecthours)
                elif users_request_projecthours == 0:
                    new_record = cls(users_request_investments=users_request_investments)
                else:
                    new_record = cls(users_request_investments=users_request_investments,
                                     users_request_projecthours=users_request_projecthours)

                new_record.save()
                return new_record
        except:
            if users_request_investments == 0:
                new_record = cls(users_request_projecthours=users_request_projecthours)
                pass
            elif users_request_projecthours == 0:
                new_record = cls(users_request_investments=users_request_investments)
                pass
            else:
                new_record = cls(users_request_investments=users_request_investments,
                                 users_request_projecthours=users_request_projecthours)

            new_record.save()
            return new_record

            # şu kullanıcı şu post'a  takım isteği yolladı

    @classmethod
    def select(cls, users_request_investments, users_request_projecthours):
        return cls.objects.filter(users_request_investments=users_request_investments,
                                  users_request_projecthours=users_request_projecthours)


class UsersProjectsTeamrequests(models.Model):
    teamrequest = models.ForeignKey('Teamrequests', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    users_projects = models.ForeignKey('UsersProjects', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    is_response = models.BooleanField(default=False)
    is_request = models.BooleanField(default=True)
    is_active = models.BooleanField(default=False)
    response_date = models.DateTimeField(null=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_projects_teamrequests'

    @classmethod
    def confirm_respond(cls, users_fullprofiles, users_projects):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofiles, users_projects=users_projects).get()
        selected.is_response = True
        selected.save()
        return selected

    @classmethod
    def create(cls, users_fullprofiles, users_projects, teamrequest):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, teamrequest=teamrequest).exists():
                selected = cls.objects.filter(users_fullprofiles=users_fullprofiles, teamrequest=teamrequest).get()
                selected.users_fullprofiles = users_fullprofiles
                selected.users_projects = users_projects
                selected.teamrequest = teamrequest
                selected.save()
                return selected
            else:
                new_record = cls(users_fullprofiles=users_fullprofiles, users_projects=users_projects,
                                 teamrequest=teamrequest)
                new_record.save()
                return new_record

        except:
            new_record = cls(users_fullprofiles=users_fullprofiles, users_projects=users_projects,
                             teamrequest=teamrequest)
            new_record.save()
            return new_record

    @classmethod
    def confirm_teamrequest(cls, users_fullprofiles, users_projects):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofiles, users_projects=users_projects).get()
        selected.is_active = True
        selected.save()
        return selected

    @classmethod
    def request_counter(cls, users_projects):
        try:
            return cls.objects.filter(users_projects=users_projects).count()
        except:
            return 0

    @classmethod
    def select(cls, users_fullprofiles, users_projects):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_projects=users_projects).exists():
                return cls.objects.filter(users_fullprofiles=users_fullprofiles, users_projects=users_projects).get()
        except:
            pass



    @classmethod
    def isThere_any_request_to_project(cls, users_projects):
        try:
            if cls.objects.filter(users_projects=users_projects).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def isThereRequest(cls, users_fullprofiles, users_projects):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_projects=users_projects).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def selected_user_has_request(cls, users_fullprofiles, users_projects):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_projects=users_projects,
                                  is_active=True).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def users_request_count(cls, users_fullprofiles):
        request_count = 0
        try:
            for requests in cls.objects.filter(is_active=True, is_read=False).all():
                if users_fullprofiles == requests.users_projects.users_fullprofiles:
                    request_count += 1
            return request_count
        except:
            return request_count

    @classmethod
    def select_requests(cls, users_projects):
        return cls.objects.filter(is_active=True, users_projects=users_projects).all().order_by("-id")

    @classmethod
    def undecided_requests(cls, users_projects):
        return cls.objects.filter(is_active=False, users_projects=users_projects).all().order_by("id")

    @classmethod
    def readthis_request(cls, teamrequest):
        selected = cls.objects.filter(teamrequest=teamrequest).get()
        selected.is_read = True
        selected.save()


class UsersPostsTeamrequests(models.Model):
    teamrequest = models.ForeignKey('Teamrequests', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    users_posts = models.ForeignKey('UsersPosts', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    is_response = models.BooleanField(default=False)
    is_request = models.BooleanField(default=True)
    is_active = models.BooleanField(default=False)
    response_date = models.DateTimeField(null=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_posts_teamrequests'

    @classmethod
    def readthis_request(cls, teamrequest):
        selected = cls.objects.filter(teamrequest=teamrequest).get()
        selected.is_read = True
        selected.save()
        return selected

    @classmethod
    def create(cls, users_fullprofiles, users_posts, teamrequest):
        new_record = cls(users_fullprofiles=users_fullprofiles, users_posts=users_posts, teamrequest=teamrequest)
        new_record.save()
        return new_record

    @classmethod
    def confirm_teamrequest(cls, users_fullprofiles, users_posts):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).get()
        selected.is_active = True
        selected.save()
        return selected

    @classmethod
    def response_teamrequest(cls,users_fullprofiles,users_posts):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).get()
        selected.is_response=True
        selected.save()
        return selected
    # bu kısım sıçacak başka yerde
    @classmethod
    def post_teamrequests_count(cls, users_posts):
        try:
            return cls.objects.filter(users_posts=users_posts).count()
        except:
            return 0

    @classmethod
    def select_this(cls, users_fullprofiles, users_posts):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).get()

    @classmethod
    def isThere_request(cls, users_fullprofiles, users_posts):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def post_requests(cls, users_posts, users_fullprofiles):
        # bu kısımda okunmayan bildirimleri çekeceğiz
        return cls.objects.filter(users_posts=users_posts, users_fullprofiles=users_fullprofiles,
                                  is_read=False).all().order_by("-id")

    @classmethod
    def users_request_count(cls, users_fullprofiles):
        request_count = 0
        try:
            for requests in cls.objects.filter(is_active=True, is_read=False).all():
                if users_fullprofiles == requests.users_posts.users_fullprofiles:
                    request_count += 1

            return request_count

        except:
            return request_count

    @classmethod
    def select_requests(cls, users_posts):
        return cls.objects.filter(is_active=True, users_posts=users_posts).all().order_by("-id")

    @classmethod
    def undediced_requests(cls, users_posts):
        return cls.objects.filter(is_active=False, users_posts=users_posts).all().order_by("-id")

    @classmethod
    def isThere_Investments(cls, users_fullprofiles, users_posts):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).exists():
                selected = cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).get()
                # if selected.teamrequest.users_request_investments is None
            pass
        except:
            return False


# eğer teknik kişi -> ücretli desteği seçmiş ise bu tabloya girişmaliyetini yazacak
class UsersHourCosts(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    hour_costs = models.ForeignKey('mainadmins.HourCosts', models.DO_NOTHING, blank=True)
    hour_cost_amount = models.CharField(max_length=150, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'users_hourcosts'

    @classmethod
    def create(cls, users_fullprofiles, hour_costs, hour_cost_amount):
        if not cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
            u_hcosts = cls(users_fullprofiles=users_fullprofiles, hour_costs=hour_costs,
                           hour_cost_amount=hour_cost_amount)
            u_hcosts.save()
        else:
            u_hcosts = cls.objects.filter(users_fullprofiles=users_fullprofiles).get()
            u_hcosts.hour_costs = hour_costs
            u_hcosts.hour_cost_amount = hour_cost_amount
            u_hcosts.save()

        return u_hcosts

    @classmethod
    def _isUsersHourCostsClear(cls, users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def select_user_hourcost(cls, users_fullprofile):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofile).get()
        return selected


class UsersProjectsPage(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    projects_page = models.ForeignKey('myprojects.ProjectsPage', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_projects_page'


class UsersProjects(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    projects = models.ForeignKey('projects.Projects', models.DO_NOTHING, blank=True)
    posts=models.ForeignKey('ideas.Posts',models.DO_NOTHING,blank=True,null=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_projects'

    @classmethod
    def same_location_projects_count(cls,users_fullprofiles_id):
        from utils.views import is_it_same_location
        selected_user=UsersFullProfile.select_user(users_fullprofiles_id)
        counter=0
        for project in cls.objects.all():
            if counter < 11:
                if is_it_same_location(selected_user.id,project.users_fullprofiles.id):
                    counter+=1

        return counter

    @classmethod
    def create(cls, users_fullprofiles, projects,posts):
        if posts == 0:
            new_record = cls(users_fullprofiles=users_fullprofiles, projects=projects)
        else:
            new_record = cls(users_fullprofiles=users_fullprofiles, projects=projects,posts=posts)

        new_record.save()
        return new_record

    @classmethod
    def connect_post_to_project(cls,users_fullprofiles,projects,posts):
        selected=cls.objects.filter(users_fullprofiles=users_fullprofiles,projects=projects).get()
        selected.posts=posts
        selected.save()
        return selected

    @classmethod
    def isProjectForThisUser(cls, users_fullprofiles, projects):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles, projects=projects).exists():
            return True
        else:
            return False

    @classmethod
    def is_blong_this_user(cls,users_fullprofiles,id):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles,id=id).exists():
            return True
        else:
            return False

    @classmethod
    def select_active_projects_by_user(cls, users_fullprofiles):
        return cls.objects.filter(projects__is_active=True, users_fullprofiles=users_fullprofiles).all().order_by("-id")

    @classmethod
    def list_all(cls, users_fullprofiles):
        list_all = cls.objects.filter(users_fullprofiles=users_fullprofiles).all()
        return list_all

    @classmethod
    def projects_count(cls, users_fullprofiles):
        from projects.models import Projects, ProjectsProjectOptions
        from mainadmins.models import ProjectOptions
        count = 0
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
                for project in cls.objects.filter(users_fullprofiles=users_fullprofiles).all():
                    if ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select("publish-project")):
                        count += 1
        except:
            count = 0

        return count

    @classmethod
    def select(cls, projects):
        return cls.objects.filter(projects=projects).get()

    @classmethod
    def select_by_only_id(cls, id):
        try:
            return cls.objects.filter(id=id).get()
        except:
            pass

    @classmethod
    def isThereProject(cls, users_fullprofiles):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
            return True
        else:
            return False

    @classmethod
    def select_last_project(cls, users_fullprofiles):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles).last()

    @classmethod
    def general_projects_count(cls):
        try:
            from projects.models import ProjectsProjectOptions, ProjectPointsScores
            from mainadmins.models import ProjectOptions
            counter=0
            for prj in UsersProjects.objects.all().order_by("-id"):
                if ProjectPointsScores.calculate_points(prj.projects) >= 95 and ProjectsProjectOptions.isItOnline(prj.projects, ProjectOptions.select("publish-project")):
                    counter+=1

            return counter
        except:
            return 0

    @classmethod
    def isThere_activeProject(cls, projects):
        try:
            from projects.models import ProjectsProjectOptions, ProjectPointsScores
            from mainadmins.models import ProjectOptions

            if ProjectPointsScores.calculate_points(projects) >= 95 and ProjectsProjectOptions.isItOnline(projects, ProjectOptions.select("publish-project")):
                return True
            else:
                return False

        except:
            return False

    @classmethod
    def isThereProject_by_project(cls, projects):
        try:
            if cls.objects.filter(projects=projects).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def select_by_id(cls, id, users_fullprofiles):
        return cls.objects.filter(id=id, users_fullprofiles=users_fullprofiles).get()

    @classmethod
    def isThereProjectForThisUser_by_id(cls, id, users_fullprofiles):
        try:
            if cls.objects.filter(id=id, users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False


# şu kullanıcı şu projeye feedback yazdı
class UsersProjectsFeedbacks(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    users_projects = models.ForeignKey('UsersProjects', models.DO_NOTHING, blank=True)
    describe = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_read = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'users_projectsfeedbacks'

    @classmethod
    def create(cls, users_fullprofiles, users_projects, describe):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_projects=users_projects).exists():
                selected = cls.objects.filter(users_fullprofiles=users_fullprofiles,
                                              users_projects=users_projects).get()
                selected.describe = describe
                selected.save()
                return selected
            else:
                new_record = cls(users_fullprofiles=users_fullprofiles, users_projects=users_projects,
                                 describe=describe)
                new_record.save()
                return new_record
        except:
            new_record = cls(users_fullprofiles=users_fullprofiles, users_projects=users_projects, describe=describe)
            new_record.save()
            return new_record

    @classmethod
    def isThere_any_feedbacks(cls,users_projects):
        try:
            if cls.objects.filter(users_projects=users_projects).count() > 0:
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def list_feedbacks(cls, users_projects):
        return cls.objects.filter(users_projects=users_projects, is_active=True).all().order_by("-id")

    @classmethod
    def read_feedbacks(cls,id):
        selected=cls.objects.filter(id=id).get()
        selected.is_read=True
        selected.save()
        return selected


class UsersSystemMessages(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    system_messages = models.ForeignKey('mainadmins.SystemMessages', models.DO_NOTHING, blank=True)
    is_read = models.NullBooleanField()
    read_date = models.DateTimeField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_system_messages'


class UsersPosts(models.Model):
    posts = models.ForeignKey('ideas.Posts', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_posts'

    @classmethod
    def create_users_posts(cls, users_fullprofiles, posts):
        new_record = cls(users_fullprofiles=users_fullprofiles, posts=posts)
        new_record.save()
        return new_record

    @classmethod
    def is_there(cls, id):
        try:
            if cls.objects.filter(id=id).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def select_this(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def select_by_post(cls,posts):
        return cls.objects.filter(posts=posts).get()

    @classmethod
    def count_by_location(cls, city):
        counter = 0
        for idea in cls.objects.all()[:12]:
            if counter > 10:
                break
            else:
                if city in idea.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city:
                    counter += 1

        return counter

    @classmethod
    def getposts_byuser(cls, users_fullprofiles):
        try:
            return cls.objects.filter(users_fullprofiles=users_fullprofiles).all().order_by("-id")
        except:
            pass

    @classmethod
    def post_list(cls, users_fullprofiles):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles, posts__is_active=True).all().order_by("-id")

    @classmethod
    def isThere_post_by_user(cls, users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def select_this_byuser(cls, id, users_fullprofiles):
        return cls.objects.filter(id=id, users_fullprofiles=users_fullprofiles).get()

    @classmethod
    def isThere_my_post(cls,users_fullprofiles,posts):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles,posts=posts).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def general_post_count(cls):
        return UsersPosts.objects.count()


class UsersRequestProjectHours(models.Model):
    id = models.BigAutoField(primary_key=True)
    project_hours = models.ForeignKey('mainadmins.ProjectHours', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'users_request_project_hours'

    @classmethod
    def create(cls, project_hours, id):
        try:
            if cls.objects.filter(id=id).exists():
                selected = cls.objects.filter(id=id).get()
                selected.project_hours = project_hours
                selected.is_active = True
                selected.save()
                return selected
            else:
                new_record = cls(project_hours=project_hours)
                new_record.save()
                return new_record

        except:
            new_record = cls(project_hours=project_hours)
            new_record.save()
            return new_record

    @classmethod
    def select_projecthour(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def countby_userspost(cls, users_fullprofiles, users_posts):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).exists():
                return cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).count()
            else:
                return 0
        except:
            return 0

    @classmethod
    def isThere(cls, id):
        try:
            if cls.objects.filter(id=id).exists():
                return True
            else:
                return False
        except:
            return False


class UsersPostsSmilarprojects(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    posts_smilarprojects = models.ForeignKey('ideas.PostsSmilarprojects', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)
    is_read = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'users_posts_smilarprojects'

    @classmethod
    def create(cls, users_fullprofiles, post_smilarprojects):
        new_record = cls(users_fullprofiles=users_fullprofiles, posts_smilarprojects=post_smilarprojects)
        new_record.save()
        return new_record

    @classmethod
    def select_by_posts(cls, posts):
        return cls.objects.filter(posts_smilarprojects__posts=posts).all()


class UsersPostslikes(models.Model):
    posts = models.ForeignKey('ideas.Posts', models.DO_NOTHING, blank=True)
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    is_like = models.NullBooleanField()
    liked_date = models.DateTimeField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_postslikes'

    @classmethod
    def post_likes_count(cls, posts):
        try:
            return cls.objects.filter(posts=posts, is_like=True).count()
        except:
            return 0


class UsersPostsnotifications(models.Model):
    posts = models.ForeignKey('ideas.Posts', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    is_alarm = models.NullBooleanField()
    created_date = models.DateTimeField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_postsnotifications'


class UsersPostssave(models.Model):
    posts = models.ForeignKey('ideas.Posts', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    is_save = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_postssave'


class UsersProjectsPageFollowers(models.Model):
    # users = models.ForeignKey('Users', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    projects_page_followers = models.ForeignKey('myprojects.ProjectsPageFollowers', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'users_projects_page_followers'


class UsersRequestInvestments(models.Model):
    id = models.BigAutoField(primary_key=True)
    investments = models.ForeignKey('mainadmins.Investments', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'users_request_investments'

    @classmethod
    def create(cls, investments, id):
        try:
            if cls.objects.filter(id=id).exists():
                selected = cls.objects.filter(id=id).get()
                selected.investments = investments
                selected.is_active = True
                selected.save()
                return selected
            else:
                new_record = cls(investments=investments)
                new_record.save()
                return new_record
        except:
            new_record = cls(investments=investments)
            new_record.save()
            return new_record

    @classmethod
    def select_investment(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def isThere(cls, id):
        try:
            if cls.objects.filter(id=id).exists():
                return True
            else:
                return False
        except:
            return False



            # @classmethod
            # def count_by_user_and_post(cls, users_fullprofiles, users_posts):
            #     try:
            #         if cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).exists():
            #             return cls.objects.filter(users_fullprofiles=users_fullprofiles, users_posts=users_posts).count()
            #         else:
            #             return 0
            #     except:
            #         return 0

    @classmethod
    def select_by_investment(cls, investments):
        return cls.objects.filter(investments=investments).get()


class UsersInvestments(models.Model):
    id = models.BigAutoField(primary_key=True)
    investments = models.ForeignKey('mainadmins.Investments', models.DO_NOTHING, blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)

    @classmethod
    def create(cls, investments, users_fullprofiles):
        try:
            if not cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
                u_invest = cls(investments=investments, users_fullprofiles=users_fullprofiles)
                u_invest.save()
            else:
                u_invest = cls.objects.filter(users_fullprofiles=users_fullprofiles).get()
                u_invest.investments = investments
                u_invest.save()
        except:
            u_invest = cls(investments=investments, users_fullprofiles=users_fullprofiles)
            u_invest.save()

        return u_invest

    @classmethod
    def _isInvestmentsClear(cls, users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def select_users_investment(cls, users_fullprofiles):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofiles).get()
        return selected

    @classmethod
    def _investmentsType(cls, users_fullprofiles):
        investments_type = cls.objects.filter(users_fullprofiles=users_fullprofiles).get().investments.title_code
        return investments_type

    class Meta:
        managed = True
        db_table = 'users_investments'


class UsersProjectHours(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    project_hours = models.ForeignKey('mainadmins.ProjectHours', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'UsersProjectHours'

    @classmethod
    def create(cls, users_fullprofiles, project_hours):
        if not cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
            u_projecthours = cls(users_fullprofiles=users_fullprofiles, project_hours=project_hours)
            u_projecthours.save()
        else:
            u_projecthours = cls.objects.filter(users_fullprofiles=users_fullprofiles).get()
            u_projecthours.project_hours = project_hours
            u_projecthours.save()

        return u_projecthours

    @classmethod
    def _isProjectHoursClear(cls, users_fullprofiles):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles).exists():
            return True
        else:
            return False

    @classmethod
    def select_user_projecthours(cls, users_fullprofiles):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofiles).get()
        return selected


class users_generalnotifications(models.Model):
    id = models.BigAutoField(primary_key=True)
    # users=models.ForeignKey('Users',models.DO_NOTHING,blank=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    general_notifications = models.ForeignKey('mainadmins.general_notifications', models.DO_NOTHING)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'users_generalnotifications'


class UsersBlocklist(models.Model):
    id = models.BigAutoField(primary_key=True)
    # users=models.ForeignKey('Users',models.DO_NOTHING,blank=True,related_name='main_usersfkey')
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True,
                                           related_name='main_usersfkey')
    # to_users=models.ForeignKey('Users',models.DO_NOTHING,blank=True,related_name='block_usersfkey')
    to_users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True,
                                              related_name='block_usersfkey')
    describe = models.TextField()
    is_active = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'users_blockeduser'

    @classmethod
    def create(cls, users_fullprofiles, to_users_fullprofiles, describe, is_active):
        blocked_user = cls(users_fullprofiles=users_fullprofiles, to_users_fullprofiles=to_users_fullprofiles,
                           describe=describe, is_active=is_active)
        blocked_user.save()
        return blocked_user


class UsersTeamEmailRequests(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    team_requests = models.ForeignKey('mainadmins.TeamEmailRequests', models.DO_NOTHING)
    is_active = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'users_teamemailrequests'

    @classmethod
    def create(cls, users_fullprofiles, team_requests, is_active):
        u_teamrequest = cls(users_fullprofiles=users_fullprofiles, team_requests=team_requests, is_active=is_active)
        u_teamrequest.save()
        return u_teamrequest

    @classmethod
    def _isThereInvitation(cls, users_fullprofiles_id):

        if cls.objects.filter(users_fullprofiles_id=users_fullprofiles_id).exists():
            return True
        else:
            return False


class UsersPointScores(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    pointscores = models.ForeignKey('mainadmins.PointScores', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'users_pointscores'

    @classmethod
    def create(cls, users_fullprofiles, pointsscores):
        users_pointscores = cls.objects.update_or_create(users_fullprofiles=users_fullprofiles,
                                                         pointscores=pointsscores)
        return users_pointscores

    @classmethod
    def __check_scores(cls, users_fullprofiles, point_scores):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles, pointscores=point_scores).exists():
            return True
        else:
            return False

    @classmethod
    def __check_skill_count(cls, users_fullprofiles):
        from mainadmins.models import PointScores
        return cls.objects.filter(users_fullprofiles=users_fullprofiles,
                                  pointscores=PointScores.select("skill-item")).count()

    @classmethod
    def calculate_points(cls, users_fullprofiles):
        all_points = 0
        skill_points = 0

        for score in cls.objects.filter(users_fullprofiles=users_fullprofiles).all():

            # skill eklemiş ise => sayısı kadar skill puanı ile çarp
            if "skill-item" in score.pointscores.point_code:
                skill_points = int(UsersSkills.count_skills(users_fullprofiles)) * int(score.pointscores.point_value)
                all_points += int(skill_points)
            else:
                all_points += int(score.pointscores.point_value)

        return all_points

    @classmethod
    def delete_point(cls, users_fullprofiles, pointscores):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles, pointscores=pointscores).delete()


class UsersStartupHistory(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    startup_history = models.ForeignKey('mainadmins.StartupHistory', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'users_startuphistory'

    @classmethod
    def create(cls, users_fullprofile, startup_history):
        if not cls.objects.filter(users_fullprofiles=users_fullprofile).exists():
            s_history = cls(users_fullprofiles=users_fullprofile, startup_history=startup_history)
            s_history.save()
        else:
            s_history = cls.objects.filter(users_fullprofiles=users_fullprofile).get()
            s_history.startup_history = startup_history
            s_history.save()

        return s_history

    @classmethod
    def _isStartupHistoryClear(cls, users_fullprofiles):
        if cls.objects.filter(users_fullprofiles_id=users_fullprofiles).exists():
            return True
        else:
            return False

    @classmethod
    def select_user_history(cls, users_fullprofiles):
        selected = cls.objects.filter(users_fullprofiles=users_fullprofiles).get()
        return selected


class UsersUpdate_Infos(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('UsersFullProfile', models.DO_NOTHING, blank=True)
    update_main_infos = models.ForeignKey("updates.UpdatesMain_UpdatesInfo", models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = "users_update_infos"

    @classmethod
    def read_this_update(cls, users_fullprofiles, update_main_infos):
        return cls.objects.update_or_create(users_fullprofiles=users_fullprofiles, update_main_infos=update_main_infos)

    @classmethod
    def _isReadThisLog(cls, users_fullprofiles, update_main_infos):
        if cls.objects.filter(users_fullprofiles=users_fullprofiles, update_main_infos=update_main_infos).exists():
            return True
        else:
            return False

    @classmethod
    def read_notification(cls, users_fullprofiles, update_main_infos):
        return cls.objects.update_or_create(users_fullprofiles=users_fullprofiles, update_main_infos=update_main_infos)


class UsersTeams(models.Model):
    users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True)
    teams = models.ForeignKey('teams.Teams', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'users_teams'

    @classmethod
    def my_location_teammembers_count(cls, users_projects, users_fullprofile_id):
        from utils.views import is_it_same_location
        same_counter = 0
        if cls.objects.filter(teams__users_projects=users_projects).exists():
            for user_item in cls.objects.filter(teams__users_projects=users_projects).all():
                if user_item.users_fullprofiles.id != users_fullprofile_id:
                    if is_it_same_location(user_item.users_fullprofiles.id,users_fullprofile_id):
                        same_counter += 1
        else:
            same_counter = 0

        return same_counter

    # @classmethod
    # def teams_member_location_by_mylocation_count(cls, users_fullprofiles):
    #     from utils.views import change_characters,is_it_same_location
    #     selected_user = cls.objects.filter(users_fullprofiles=users_fullprofiles).get()
    #     count = 0
    #     # if is_it_same_location()
    #     if change_characters(
    #             selected_user.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) == change_characters(
    #         str(owner_city)):
    #         count += 1
    #
    #     return count

    @classmethod
    def isThere_user_this_team(cls, users_fullprofiles, teams):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles, teams=teams).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def create(cls, users_fullprofiles, teams):
        try:
            if not cls.objects.filter(users_fullprofiles=users_fullprofiles, teams=teams).exists():
                new_record = cls(users_fullprofiles=users_fullprofiles, teams=teams)
                new_record.save()
                return new_record
            else:
                selected = cls.objects.filter(users_fullprofiles=users_fullprofiles, teams=teams).get()
                selected.users_fullprofiles = users_fullprofiles
                selected.save()
                return selected
        except:
            new_record = cls(users_fullprofiles=users_fullprofiles, teams=teams)
            new_record.save()
            return new_record

    @classmethod
    def get_teammembers_by_teams(cls, teams):
        return cls.objects.filter(teams=teams, is_active=True).all().order_by("id")

    @classmethod
    def delete_member(cls,users_fullprofiles,teams):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles,teams=teams).exists():
                selected=cls.objects.filter(users_fullprofiles=users_fullprofiles,teams=teams).get()
                selected.is_active = False
                selected.save()
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def is_there_anyproject_included(cls,users_fullprofiles):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles,is_active=True).exclude(teams__users_projects__users_fullprofiles=users_fullprofiles).count() > 0:
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def am_i_included_this_project(cls,users_fullprofiles,users_projects):
        try:
            if cls.objects.filter(users_fullprofiles=users_fullprofiles,teams__users_projects=users_projects).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def get_teams_in_projects(cls,users_fullprofiles):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles, is_active=True).all().order_by("-id")

    @classmethod
    def cofunded_projects_count(cls,users_fullprofiles_id):
        cofunded_count=0
        try:
            for prj in cls.objects.filter(users_fullprofiles=UsersFullProfile.select_user(users_fullprofiles_id), is_active=True).all():
                if users_fullprofiles_id != prj.teams.users_projects.users_fullprofiles.id:
                    print("+")
                    cofunded_count+=1

            return cofunded_count
        except:
            return cofunded_count

    @classmethod
    def select_teammembers_by_id(cls,users_projects):
        return cls.objects.filter(teams__users_projects=users_projects)

class UsersDmProjectsTeamrequests(models.Model):
    id = models.BigAutoField(primary_key=True)
    users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True,
                                           related_name='main_fkey')
    to_users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING, blank=True,
                                              related_name='other_users_key')
    teamrequests = models.ForeignKey('Teamrequests', models.DO_NOTHING, blank=True)
    users_projects = models.ForeignKey('UsersProjects', models.DO_NOTHING, blank=True)

    created_date = models.DateTimeField(auto_now_add=True)
    readed_date = models.DateTimeField(null=True, blank=True)
    response_date = models.DateTimeField(null=True, blank=True)
    is_response = models.BooleanField(default=False)
    is_request = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    is_read = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = "users_dm_projects_teamrequests"

    @classmethod
    def create_request(cls, teamrequests, users_fullprofiles, to_users_fullprofiles, users_projects):
        try:
            if not cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles,
                                      users_projects=users_projects).exists():
                new_record = cls(users_fullprofiles=users_fullprofiles, to_users_fullprofiles=to_users_fullprofiles,
                                 teamrequests=teamrequests, users_projects=users_projects)
                new_record.save()
                return new_record
            else:
                pass
        except:
            new_record = cls(users_fullprofiles=users_fullprofiles, to_users_fullprofiles=to_users_fullprofiles,
                             teamrequests=teamrequests, users_projects=users_projects)
            new_record.save()
            return new_record

    @classmethod
    def select_this(cls, to_users_fullprofiles, users_projects):
        return cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, users_projects=users_projects).get()

    @classmethod
    def select_this_by_id(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def same_location_count_requested(cls,users_projects,owner_users_id):
        from utils.views import is_it_same_location
        selected_requests=cls.objects.filter(users_projects=users_projects).all()
        # owner_users=UsersFullProfile.select_user(owner_users_id)
        counter=0
        for requested_person in selected_requests:
            if is_it_same_location(owner_users_id,requested_person.to_users_fullprofiles.id):
                counter+=1

        return counter

    @classmethod
    def request_count(cls, to_users_fullprofiles, is_read):
        try:
            return cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, is_active=True,
                                      is_read=is_read).count()
        except:
            return 0

    @classmethod
    def list_requests(cls, to_users_fullprofiles):
        return cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, is_active=True).all().order_by("-id")

    @classmethod
    def read_this_request_by_usersprojects(cls, to_users_fullprofiles, users_projects):
        from datetime import datetime
        selected = cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, users_projects=users_projects).get()
        selected.is_read = True
        selected.readed_date = datetime.now()
        selected.save()
        return selected

    @classmethod
    def read_this_request_by_id(cls, id):
        from datetime import datetime
        selected = cls.objects.filter(id=id).get()
        selected.is_read = True
        selected.readed_date = datetime.now()
        selected.save()
        return selected

    @classmethod
    def response_this_request_by_id(cls, id):
        from datetime import datetime
        selected = cls.objects.filter(id=id).get()
        selected.is_response = True
        selected.response_date = datetime.now()
        selected.save()
        return selected

    @classmethod
    def response_this_request_by_usersprojects(cls, to_users_fullprofiles, users_projects):
        from datetime import datetime
        selected = cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, users_projects=users_projects).get()
        selected.is_response = True
        selected.response_date = datetime.now()
        selected.save()
        return selected

    @classmethod
    def is_there_created_request(cls, to_users_fullprofiles, users_projects):
        try:
            if cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, users_projects=users_projects).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def is_there_dm_requests(cls, to_users_fullprofiles):
        try:
            if cls.objects.filter(to_users_fullprofiles=to_users_fullprofiles, is_active=True).exists():
                return True
            else:
                return False
        except:
            return False

    @classmethod
    def list_sent_requests(cls,users_fullprofiles,users_projects):
        return cls.objects.filter(users_fullprofiles=users_fullprofiles,users_projects=users_projects,is_request=True,is_active=True).all().order_by("-id")

    @classmethod
    def delete_this_request(cls,id):
        selected=cls.objects.filter(id=id).get()
        selected.is_active=False
        selected.save()
        return selected
