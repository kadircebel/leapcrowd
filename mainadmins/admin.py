from django.contrib import admin
from .models import Investments, ProjectHours, general_notifications, PointScores, ImageUploadSize, HourCosts, StartupHistory,  UsageTypes, ProjectOptions, ProjectSector,Steps, JobTitles


class StepsAdmin(admin.ModelAdmin):
    list_display = ["step_name","step_name_eng","step_code","created_date","up_date"]

    class Meta:
        model=Steps

class ProjectSectorAdmin(admin.ModelAdmin):
    list_display = ['title', 'title_eng', 'is_active', 'created_date']

    class Meta:
        model = ProjectSector


class UsageTypesAdmin(admin.ModelAdmin):
    list_display = ['title', 'title_eng', 'is_active', 'created_date']

    class Meta:
        model = UsageTypes


# Register your models here.
class InvestmentAdmin(admin.ModelAdmin):
    list_display = ['title', 'isactive', 'created_date']

    class Meta:
        model = Investments


class ProjectHoursAdmin(admin.ModelAdmin):
    list_display = ['title', 'value_code', 'created_date', 'is_active']

    class Meta:
        model = Investments


class GeneralNotificationsAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_date', 'is_active']

    class Meta:
        model = general_notifications


class PointScoresAdmin(admin.ModelAdmin):
    list_display = ['point_name', 'point_code', 'point_value', 'is_active', 'created_date']

    class Meta:
        model = PointScores


class ImageUploadSizesAdmin(admin.ModelAdmin):
    list_display = ['byte_value', 'mb_value', 'created_date', 'is_active']

    class Meta:
        model = ImageUploadSize


class HourCostsAdmin(admin.ModelAdmin):
    list_display = ['cost_type', 'cost_code', 'created_date', 'is_active']

    class Meta:
        model = HourCosts


class StartupHistoryAdmin(admin.ModelAdmin):
    list_display = ['title', 'title_code', 'created_date', 'is_active']

    class Meta:
        model = StartupHistory


class ProjectOptionsAdmin(admin.ModelAdmin):
    list_display = ['title','title_code','situation', 'created_date']

    class Meta:
        model = ProjectOptions

class JobTitlesAdmin(admin.ModelAdmin):
    list_display = ['title']

    class Meta:
        model = JobTitles

admin.site.register(JobTitles,JobTitlesAdmin)
admin.site.register(ProjectOptions,ProjectOptionsAdmin)
admin.site.register(StartupHistory, StartupHistoryAdmin)
admin.site.register(ImageUploadSize, ImageUploadSizesAdmin)
admin.site.register(Investments, InvestmentAdmin)
admin.site.register(ProjectHours, ProjectHoursAdmin)
admin.site.register(general_notifications, GeneralNotificationsAdmin)
admin.site.register(PointScores, PointScoresAdmin)
admin.site.register(HourCosts, HourCostsAdmin)
admin.site.register(UsageTypes, UsageTypesAdmin)
admin.site.register(ProjectSector, ProjectSectorAdmin)
admin.site.register(Steps, StepsAdmin)
# admin.site.register(ComplainedUsers,ComplainedUsersAdmin)
