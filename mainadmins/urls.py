from django.urls import path
from .views import *

app_name="mainadmins"

urlpatterns=[
    path('job-titles/',get_job_titles,name='job-titles'),
    path('select-invest/',select_investments,name='select_invest'),
    path('select-hours/', select_project_hours, name='select_hours'),
    path('select-startuphistory/', select_startup_history, name='select_startuphistory'),
    path('select-project-areas/', project_areas, name='project_areas'),
    path('select-project-types/', project_types, name='project_types'),
]