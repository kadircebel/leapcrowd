from django.db import models


class ComplainedOptions(models.Model):
    title = models.CharField(max_length=500, blank=True)
    complain_code = models.CharField(max_length=250, blank=True)
    created_date = models.DateField(auto_created=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'complained_options'


class Options(models.Model):
    title = models.CharField(max_length=250, blank=True)
    title_code = models.CharField(max_length=250, blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'options'


class SystemMessages(models.Model):
    title = models.CharField(max_length=500, blank=True)
    body_text = models.TextField(blank=True)
    send_date = models.DateField(blank=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'system_messages'


class SystemProjectpages(models.Model):
    page_title = models.CharField(max_length=500, blank=True)
    page_code = models.CharField(max_length=250, blank=True)
    file_str = models.CharField(max_length=700, blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'system_projectpages'


class SystemTeampages(models.Model):
    page_title = models.CharField(max_length=500, blank=True)
    page_code = models.CharField(max_length=250, blank=True)
    file_str = models.CharField(max_length=700, blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'system_teampages'


class Steps(models.Model):
    id = models.BigAutoField(primary_key=True)
    step_name = models.CharField(max_length=500, blank=True)
    step_name_eng = models.CharField(max_length=500, blank=True)
    step_code = models.CharField(max_length=500, blank=True)
    created_date = models.DateField(auto_created=True)
    up_date = models.DateField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'steps'

    def __str__(self):
        return '{}'.format(self.step_name)

    @classmethod
    def select(cls, step_code):
        return cls.objects.filter(step_code=step_code).get()

    @classmethod
    def select_by_id(cls,step_id):
        return cls.objects.filter(id=step_id).get()

    @classmethod
    def select_list(cls):
        return cls.objects.all()


class Socialaddress(models.Model):
    title = models.CharField(max_length=500, blank=True)
    slug = models.CharField(max_length=500, blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateField(auto_created=True)
    created_user = models.IntegerField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'socialaddress'


class FriendsRequests(models.Model):
    users = models.ForeignKey('persons.Users', models.DO_NOTHING, blank=True, related_name='main_users_fk')
    to_users = models.ForeignKey('persons.Users', models.DO_NOTHING, blank=True, related_name='to_user_fk')
    is_request = models.NullBooleanField()
    content_text = models.TextField(blank=True)
    created_date = models.DateField(auto_created=True)
    is_active = models.NullBooleanField()
    is_accept = models.NullBooleanField()
    accept_date = models.DateField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'friends_requests'


class City(models.Model):
    title = models.CharField(max_length=200, blank=True)
    slug = models.CharField(max_length=200, blank=True)
    is_active = models.NullBooleanField()
    created_date = models.DateField(blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'city'


class Country(models.Model):
    title = models.CharField(max_length=200, blank=True)
    slug = models.CharField(max_length=200, blank=True)
    created_date = models.DateField(blank=True)
    is_active = models.NullBooleanField()
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'country'


class CountryCity(models.Model):
    country = models.ForeignKey('Country', models.DO_NOTHING, blank=True)
    city = models.ForeignKey('City', models.DO_NOTHING, blank=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'country_city'


class Investments(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=500, blank=True)
    title_eng = models.CharField(max_length=500, blank=True)
    title_code = models.CharField(max_length=500, blank=True)
    created_date = models.DateField(auto_created=True)
    isactive = models.NullBooleanField(db_column='isActive')  # Field name made lowercase.

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        managed = True
        db_table = 'Investments'

    @classmethod
    def select_list_all(cls,lang_code):
        context=[]
        if lang_code == "tr":
            context.append({"id": 0, "title": "Seçiniz"})

            for ins in cls.objects.all().order_by("id"):
                context.append({"id":ins.id,"title":ins.title})

        else:
            context.append({"id": 0, "title": "Select"})
            for ins in cls.objects.all().order_by("id"):
                context.append({"id":ins.id,"title":ins.title_eng})

        return context
    @classmethod
    def list_all(cls):
        return cls.objects.all().order_by("id")


    @classmethod
    def select_invest_type(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def select_by_code(cls,title_code):
        return cls.objects.filter(title_code=title_code).get()


class ProjectHours(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=True)
    title_eng = models.CharField(max_length=100, blank=True)
    value_code = models.CharField(max_length=100, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField()

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        managed = True
        db_table = 'ProjectHours'

    @classmethod
    def select_list_all(cls,lang_code):
        context=[]
        if lang_code == "tr":
            context.append({"id": 0, "title": "Seçiniz"})
            for hrs in cls.objects.all().order_by("id"):
                context.append({"id":hrs.id,"title":hrs.title})
        else:
            context.append({"id": 0, "title": "Select"})
            for hrs in cls.objects.all().order_by("id"):
                context.append({"id":hrs.id,"title":hrs.title})

        return context

    @classmethod
    def list_all(cls):
        return cls.objects.all().order_by("id")


    @classmethod
    def select_project_hours(cls, id):
        selected = cls.objects.get(id=id)
        return selected


class general_notifications(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=350, blank=True)
    title_code = models.CharField(max_length=350, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField()

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        managed = True
        db_table = 'general_notifications'


class ComplainedUsers(models.Model):
    id = models.BigAutoField(primary_key=True)
    complained_options = models.ForeignKey('ComplainedOptions', models.DO_NOTHING)
    users_fullprofiles = models.ForeignKey('persons.UsersFullProfile', models.DO_NOTHING)
    is_active = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s %s {}'.format(self.users_fullprofiles.full_profile.first_name,
                                 self.users_fullprofiles.full_profile.last_name)

    class Meta:
        managed = True
        db_table = 'ComplainedUsers'

    @classmethod
    def _isBlocked(cls, email):
        if cls.objects.filter(users_fullprofiles__users__email_address=email).exists():
            return True
        else:
            return False


class TeamEmailRequests(models.Model):
    id = models.BigAutoField(primary_key=True)
    request_password = models.CharField(max_length=100, blank=True)
    request_email = models.CharField(max_length=250, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'team_emailrequests'

    @classmethod
    def create(cls, request_email, request_password):
        request = cls(request_email=request_email, request_password=request_password)
        request.save()

    @classmethod
    def isThereInvitationCode(cls, request_password):
        if cls.objects.filter(request_password=request_password).exists():
            return True
        else:
            return False


# bu tablo yönetim tarafından belirlenen bir tablo
# bu kısımda ücretin türleri girilecek $,tl,vs.
class HourCosts(models.Model):
    id = models.BigAutoField(primary_key=True)
    cost_type = models.CharField(max_length=80, blank=True)
    cost_code = models.CharField(max_length=30, blank=True)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'hour_costs'

    @classmethod
    def select(cls, lang_code,country):
        if lang_code == "tr":
            return cls.objects.filter(cost_type="Türk Lirası").get()
        else:
            if country != "turkiye" and country != "türkiye" and country != "turkey":
                return cls.objects.filter(cost_type="Dollar").get()
            else:
                return cls.objects.filter(cost_type="Türk Lirası").get()

    @classmethod
    def select_by_id(cls, id):
        return cls.objects.filter(id=id).get()


# yönetim tarafından belirlenen proje alan seçimi - projenin kullanım tipi - laptopp -mobile
class UsageTypes(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=250, blank=True)
    title_eng = models.CharField(max_length=250, blank=True)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'usage_types'

    def __str__(self):
        return '{}'.format(self.title)

    @classmethod
    def list_all(cls, lang_code):
        if lang_code == "tr":
            usage_types = cls.objects.filter(is_active=True).values("id", "title")
        else:
            usage_types = cls.objects.filter(is_active=True).values("id", "title_eng")

        return usage_types

    @classmethod
    def select(cls, id):
        return cls.objects.filter(id=id).get()


class ProjectSector(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=250, blank=True)
    title_eng = models.CharField(max_length=250, blank=True)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = "project_sector"

    def __str__(self):
        return '{}'.format(self.title)

    @classmethod
    def list_all(cls, lang_code):
        if lang_code == "tr":
            project_sectors = cls.objects.filter(is_active=True).values("id", "title")
        else:
            project_sectors = cls.objects.filter(is_active=True).values("id", "title_eng")

        return project_sectors

    @classmethod
    def select(cls, id):
        return cls.objects.filter(id=id).get()


# bu kısımda menü isimleri yönetim tarafından giriliecek
class ProjectOptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=500, blank=True)
    title_eng = models.CharField(max_length=500, blank=True)
    title_code = models.CharField(max_length=500, blank=True)
    name = models.CharField(max_length=500, blank=True)
    name_eng = models.CharField(max_length=500, blank=True)
    situation = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'project_options'

    def __str__(self):
        return '{}'.format(self.title)

    @classmethod
    def select(cls, title_code):
        return cls.objects.filter(title_code=title_code).get()


class PointScores(models.Model):
    id = models.BigAutoField(primary_key=True)
    point_name = models.CharField(max_length=500, blank=True)
    point_code = models.CharField(max_length=500, blank=True)
    point_value = models.CharField(max_length=4, blank=True)
    is_active = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'pointscores'

    @classmethod
    def select(cls, point_code):
        selected = cls.objects.filter(point_code=point_code).get()
        return selected


class ImageUploadSize(models.Model):
    id = models.BigAutoField(primary_key=True)
    byte_value = models.CharField(max_length=500, blank=True)
    mb_value = models.CharField(max_length=200, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField()

    class Meta:
        managed = True
        db_table = "image_uploadsize"

    @classmethod
    def select_mb(cls, mb_value):
        selected = cls.objects.filter(mb_value=mb_value, is_active=True).get().byte_value
        return selected


class JobTitles(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=500, blank=True)
    title_tr=models.CharField(max_length=500,blank=True)

    class Meta:
        managed = True
        db_table = "job_titles"

    def __str__(self):
        return '{}'.format(self.title)


class StartupHistory(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=True)
    title_eng = models.CharField(max_length=100, blank=True)
    title_code = models.CharField(max_length=100, blank=True)
    is_active = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'startuphistory'

    @classmethod
    def list_all(cls, lang_code):
        if lang_code == "tr":
            startup_history = cls.objects.values("id", "title")
        else:
            startup_history = cls.objects.values("id", "title_eng")

        return startup_history

    @classmethod
    def select(cls, id):
        return cls.objects.filter(id=id).get()
