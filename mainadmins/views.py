from django.http import JsonResponse,HttpResponse
from .models import JobTitles,Investments,ProjectHours,StartupHistory,UsageTypes,ProjectSector
import json
from django.views.decorators.cache import cache_page,never_cache
# from django.core import serializers
# Create your views here.
@cache_page(60*60)
def get_job_titles(request):
    title_list = []
    keyword = request.GET.get("keyword")
    finding = JobTitles.objects.filter(title__contains=keyword.lower()).all()[:10]
    for k in finding:
        s = {'id': k.id, 'title': k.title}
        title_list.append(s)

    return JsonResponse(json.dumps(title_list), safe=False)

@cache_page(60*60)
def select_investments(request):
    context=[]
    if request.LANGUAGE_CODE == "tr":
        context.append({"id": "0", "title": "Seçiniz"})
        for chooice in Investments.list_all().order_by("id"):
            context.append({"id": chooice.id, "title": chooice.title})
    else:
        context.append({"id": "0", "title": "Select"})
        for chooice in Investments.list_all().order_by("id"):
            context.append({"id": chooice.id, "title": chooice.title_eng})

    return JsonResponse(json.dumps(context), safe=False)

@cache_page(60*60)
def select_project_hours(request):
    if request.LANGUAGE_CODE == "tr":
        context = []
        context.append({"id": "0", "title": "Seçiniz"})
        for chooice in ProjectHours.list_all().order_by("id"):
            context.append({ "id": chooice.id, "title": chooice.title })
    else:
        context = []
        context.append({"id": "0", "title": "Select"})
        for chooice in ProjectHours.list_all().order_by("id"):
            context.append({ "id":chooice.id, "title":chooice.title_eng })

    # return context
    return JsonResponse(json.dumps(context), safe=False)

@cache_page(60*60)
def select_startup_history(request):
    context = []
    if request.LANGUAGE_CODE == "tr":
        context.append({"id": "0", "title": "Seçiniz"})
        for chooice in StartupHistory.list_all(request.LANGUAGE_CODE):
            context.append(chooice)
    else:
        context.append({"id": "0", "title_eng": "Select"})
        for chooice in StartupHistory.list_all(request.LANGUAGE_CODE):
            context.append(chooice)

    return JsonResponse(json.dumps(context), safe=False)

@cache_page(60*60)
def project_areas(request):
    context = []
    if request.LANGUAGE_CODE == "tr":
        context.append({"id": "0", "title": "Seçiniz"})
        for chooice in ProjectSector.list_all(request.LANGUAGE_CODE).order_by("id"):
            context.append(chooice)
    else:
        context.append({"id": "0", "title_eng": "Select"})
        for chooice in ProjectSector.list_all(request.LANGUAGE_CODE).order_by("id"):
            context.append(chooice)

    return JsonResponse(json.dumps(context), safe=False)

@cache_page(60*60)
def project_types(request):
    context = []
    if request.LANGUAGE_CODE == "tr":
        for chooice in UsageTypes.list_all(request.LANGUAGE_CODE).order_by("id"):
            context.append(chooice)
    else:
        for chooice in UsageTypes.list_all(request.LANGUAGE_CODE).order_by("id"):
            context.append(chooice)

    return JsonResponse(json.dumps(context), safe=False)