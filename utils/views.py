from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext, ngettext
from django.utils.text import slugify
from persons.models import UsersFullProfile, Users, UsersFriend, UsersTeams, UsersAnswers, UsersPosts,UsersLinkedinRecommendations,UsersIndustries
from users_messagebox.models import Messages
from myprofile.models import FullprofileLocation
from projects.models import GeneralInfos
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives, BadHeaderError
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import re, uuid




def get_community_list(language_code):
    context_person = []

    for entrepreneurs in UsersFullProfile.get_users('entrepreneur'):

        if language_code == "tr":
            position_title = "Girişimci"
        else:
            position_title = "Entrepreneur"

        if not "default" in entrepreneurs.full_profile.picture_url.name:
            profile_img = entrepreneurs.full_profile.picture_url.url
        else:
            profile_img = entrepreneurs.full_profile.linkedin_picture_url

        item = {
            'profile_name': get_profile_name_str(entrepreneurs.full_profile.last_name,
                                                 entrepreneurs.full_profile.first_name),
            'location_name': entrepreneurs.full_profile.fullprofilelocation_set.get().location.city,
            'profile_image': profile_img,
            'position_title': position_title,
            'member_type':"entrepreneur",
        }

        context_person.append(item)

    for tech_persons in UsersFullProfile.get_users('technical'):

        if len(tech_persons.full_profile.position_title) > 0:
            position_title = tech_persons.full_profile.position_title
        else:
            position_title = UsersIndustries.select(tech_persons).industries.description

        if not "default" in tech_persons.full_profile.picture_url.name:
            profile_img = tech_persons.full_profile.picture_url.url
        else:
            profile_img = tech_persons.full_profile.linkedin_picture_url

        item = {
            'profile_name': get_profile_name_str(tech_persons.full_profile.last_name,
                                                 tech_persons.full_profile.first_name),
            'location_name': tech_persons.full_profile.fullprofilelocation_set.get().location.city,
            'profile_image': profile_img,
            'position_title': position_title,
            'member_type':'technical',
        }

        context_person.append(item)

    return context_person



def create_user_password(password):
    hashed_pass = make_password(password=password, salt=None, hasher='pbkdf2_sha1')
    return hashed_pass


def forgetten_password_mail(emailAddress, random_pass, host_address):
    user_fullprofile = UsersFullProfile.get_user_byemail(emailAddress)
    emailaddressList = [user_fullprofile.users.email_address]

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = host_address + "static/frontside/bg/4.jpg"
    message_button = ugettext("Recover my password")
    newsletter_title = ugettext("Recover Password")
    reset_hyperlink = '<a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="' + host_address + '/change-password/?rp=' + random_pass + '&user={}"'.format(
        user_fullprofile.users.id) + '>' + message_button + '</a>'
    module1message = ugettext(
        "Hello,<br/> you were requested reset password. If you are not this person, please do not consider incoming e-mail.")
    message_title = ugettext("You are requesting to reset password")
    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + 'thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + 'img-01.jpg" style="vertical-align:top;" width="600" height="306" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f">' + reset_hyperlink + '</td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'

    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'

    # subject = user_fullprofile.full_profile.first_name + " " + user_fullprofile.full_profile.last_name + ugettext(" sent a team request invitation.")
    subject = ugettext("You requested a password reset.")
    from_email = settings.EMAIL_HOST_USER
    to = emailaddressList

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'  # send_mail(

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True

    except BadHeaderError:
        return False

def new_message_notification(owner_users_id,selected_users_id,lang_code,host_address):
    owner_users_fullprofiles=UsersFullProfile.select_user(owner_users_id)
    selected_users_fullprofiles=UsersFullProfile.select_user(selected_users_id)
    emailaddressList = [selected_users_fullprofiles.users.email_address]

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = host_address + "static/frontside/bg/4.jpg"
    if lang_code == "tr":
        message_button = "Mesajlarım"
        newsletter_title = "Yeni bir mesaj aldınız."
        module1message= "Merhabalar,<br/>" + get_profile_name_str(owner_users_fullprofiles.full_profile.last_name,owner_users_fullprofiles.full_profile.first_name) + " isimli kişi size bir mesaj gönderdi."
        message_title = "Yeni bir mesaj aldınız."

    else:
        message_button = "My Messages"
        newsletter_title = "You've received a new message."
        module1message = "Hello,<br/> You've received a new message from " + get_profile_name_str(owner_users_fullprofiles.full_profile.last_name,owner_users_fullprofiles.full_profile.first_name)
        message_title = "You've received a new message."

    reset_hyperlink = '<a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="' + host_address + 'dashboard/messages/inbox/">' + message_button + '</a>'

    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + 'thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + 'img-01.jpg" style="vertical-align:top;" width="600" height="306" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f">' + reset_hyperlink + '</td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'

    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'

    # subject = user_fullprofile.full_profile.first_name + " " + user_fullprofile.full_profile.last_name + ugettext(" sent a team request invitation.")
    subject = message_title
    from_email = settings.EMAIL_HOST_USER
    to = emailaddressList

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'  # send_mail(

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True

    except BadHeaderError:
        return False


def team_request_mail(emailaddressList, user_fullprofile_id, request_pass):
    user_fullprofile = UsersFullProfile.select_user(user_fullprofile_id)

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    # address_hyperlink = request.get_raw_uri() + "static/frontside/img_newsletters/img-01.jpg"
    address_hyperlink = "https://" + Site.objects.get_current().domain + "/static/frontside/bg/4.jpg"

    module1message = ugettext(
        "Hello,<br/> The user name of " + user_fullprofile.full_profile.first_name + " " + user_fullprofile.full_profile.last_name + ", has sent you a team request invitation from leapcrowd.co website.")
    message_title = ugettext("There is a teamrequest from leapcrowd.co")
    newsletter_title = ugettext("Team Request Invitation")
    message_button = ugettext("Accept team request invitation")
    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + 'thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + 'img-01.jpg" style="vertical-align:top;" width="600" height="306" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f"><a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="https://' + Site.objects.get_current().domain + '/?teampass=' + request_pass + '">' + message_button + '</a></td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'
    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'
    subject = user_fullprofile.full_profile.first_name + " " + user_fullprofile.full_profile.last_name + ugettext(
        " sent a team request invitation.")
    from_email = settings.EMAIL_HOST_USER
    to = emailaddressList

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'  # send_mail(
    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True
    except BadHeaderError:
        return False


def send_message_to_new_member(user_id,lang_code):
    selected_user=UsersFullProfile.select_user(user_id)
    country=change_characters(selected_user.full_profile.fullprofilelocation_set.get().location.country)

    if (country == "turkey" or country == "turkiye" or country == "türkiye") and lang_code == "tr":
        #turkce
        message_title="Leapcrowd'a üye olduğunuz için teşekkür ederiz."
        body_text="<p>"+"Leapcrowd'a üye olduğun için teşekkür ederiz. Size özet olarak Leapcrowd platformundan bahsetmek isteriz."+"</p>"
        body_text+="<p>"+ "Leapcrowd platformunun hedefi; girişimci kişilerin hayata geçirmek istedikleri proje veya projeler için kendi ekiplerini oluşturmaya yardımcı olmaktır. Bu zamana kadar dünya genelinde buna benzer uygulamalar olmuştur fakat ingilizce bilmeyen arkadaşlar genellikle bu uygulamalardan faydalanamamıştır. Aynı zamanda; dünyada yer alan genel uygulamaları incelediğimiz zaman bizim oluşturduğumuz yapıya benzer bir yapı ile ekip arkadaşı bulma ve ekip oluşturma üzerine bir uygulama ile biz karşılaşmadık." +"</p>"
        body_text+="<p>"+ "Bu platformu yazılımsal olarak kullanıma sunarken; kendi ekibimizi oluşturma adımlarından faydalandık. Leapcrowd ekibi öncelikle tek kişi olarak başladı. Daha sonrasında 1,5 yıl boyunca ekip arkadaşı arayışını sürdürdü. Bu süreç kapsamında freelance iş sitelerine ilanlar verildi ve bu sitelerde 'troll' diyebileceğimiz kişiler ile uğraşıldı. Bütün bunların sonucunda ekibimiz farklı lokasyonlarda olmak üzere toplam 4 kişiye ulaştı. Yeni bir projeyi hayata geçirmek için 4 kişilik bir ekip oldukça yeterlidir. Özellikle ekip oluştururken en zorlanılan konuların başında güven duygusu öne çıkmaktadır ve diğer bir konu ise kişilerin farklı lokasyonda olmasıdır. Biz bu yöntemleri kendimiz üzerinde test ederek başarıya ulaştığını ispatladık ve ispatı da bu projenin yayına alınmasıdır. :)" +"</p>"

        body_text+="<p>"+ "Dünya'nın düzenine baktığımız zaman artık yavaş yavaş ofis ortamları terk edilmektedir ve çoğu ekip uzaktan (remote) çalışmaktadır. Bu şekilde ekip kurulumu için leapcrowd platformunu oldukça ideal görüyorum. Şimdilik; güven problemini bir nebze olsun aşabilmek için kullanıcı profillerine puanlar ekledik. Yani eğer bir kişinin profil puanı düşük ise ve bu kişi size bir takım isteği gönderdiyse; profil puanını baz alarak ve profilin doğruluğunu inceleyerek anlayabilirsiniz. Bu kısımda proje sahibine biraz iş düşebilir. Size teklif gönderen bir kişinin en son çalıştığı yere bir e-posta atabilir ve kişinin durumunu öğrenmek isteyebilirsiniz. Bu yapıyı ilerleyen zamanlarda yapay zeka yardımıyla anlamak hedeflerimiz arasındadır. Örnek olarak; bir kişi profilinde kendini özet olarak anlatırken; kurduğu cümleden ne kadar ciddi ve gerçekçi olduğunu anlamak istiyoruz fakat bizim bu adımları gerçekleştirmemiz platformun ne kadar fazla tercih edildiğine bağlı olacaktır." +"</p>"

        body_text+="<p>"+ "Paylaşımcı olan bir kişi platformdan sonsuza kadar ücretsiz olarak yararlanabilir. Leapcrowd platformundan 2 şekilde takım isteği toplayabilirsiniz: 1- fikrinizi özet olarak belirtip gelen takım isteklerini değerlendirebilirsiniz. 2-Projeniz hazırsa sisteme proje ekleyebilir ve bunu leapcrowd'a üye olan kişilere açarak gelen takım isteklerini değerlendirebilirsiniz. Normal şartlarda takım 3 kişilik ekip dahilinde oluşturuluyor fakat biz ekstradan 4.kişiyi kendi lokasyonunuzdan eklemenizi istiyoruz ve toplamda 4 kişilik bir ekip oluşturabiliyorsunuz. Eğer lokasyonunuz dışından bir kişiye direk takım isteği veya özel mesaj atmak isterseniz bunun için ücret ödemek zorunda kalacaksınız. Leapcrowd platformu olarak dürüstlüğe ve topluluğun gücüne inandığımız için ücretli üyelik satın almadan da projeniz için ekip oluşturabilirsiniz. Kendi lokasyonunuzdaki kişilere sınırsız olarak özel mesaj gönderebilirsiniz fakat özel takım isteğini sadece kendi lokasyonunuzdan ekleyeceğiniz kişi sayısı kadar atabilirsiniz. Özel takım isteği atma limiti her ay sıfırlanan bir haktır. Yani açtığınız bir projede; bir kişiye özel takım isteği attınız ve cevap alamadınız diyelim. Bu özel isteği 'Takımlarım' kısmından kaldırabilirsiniz fakat bir sonraki ay tekrardan 1 adet takım atma isteği hakkınız doğacaktır. Bunun haricinde zaten fikrinizin özetini post olarak paylaştığınız zaman veya projenizi platformda paylaştığınız zaman gelen takım isteklerini değerlendirebilirsiniz. Bu durum size kalmış bir durumdur." +"</p>"

        body_text+="<p>" + "Bunun haricine üyelik paketlerimize hiç bir zaman dolar kurunu yansıtmayacağız. Üyelik paketlerinin ücreti farklı ülkelere göre sadece birim olarak aynıdır."  + "</p>"

        body_text+="<p>" + "Şuan bu platformun hazırlık sayfasında toplamda 140 kişi bulunmaktadır. Platformda ağırlıklı olarak yabancı kişi sayısı fazladır. Yani eğer fikrinizi veya proje anlatımınızı ingilizce dilinde yazarsanız bu sizin için daha yararlı olacaktır. Doğrudan global olarak bir proje başlatmak için şansınız var demektir ki krizin olduğu dönemde bu gerçekten çok işinize yarayacaktır. " + "</p>"

        body_text+="<p>"+ "Biz şuan mvp projesi olarak başladığımız için elbetteki eksikliklerimiz var ve bunların farkındayız. Bu durumları zamana yayarak halledeceğiz. :) Hedefimiz sadece üretimi arttırmak üzerinedir. " +"</p>"

        body_text+="<p>" + "Daha iyi haberler ile görüşmek üzere," + "</p>"

    elif (country == "turkey" or country == "turkiye" or country == "türkiye") and lang_code == "en":
        #ingilizce
        message_title = "Thank you for joining Leapcrowd."
        body_text = "<p>"+ "Thank you for joining Leapcrowd. We would like to talk about the Leapcrowd platform." +"</p>"
        body_text+="<p>"+ "The main goal of the Leapcrowd platform is to help entrepreneurs build their own project teams. You can build your team with 2 different methods in this platform. 1- You can share your idea and evaluate incoming team requests. 2- If your project is ready but you don't have any team members, you can add your project to the leapcrowd and when you open team request option, you can evaluate incoming team requests." +"</p>"

        body_text+="<p>"+ "All members are included in the beginner membership package for now. Which this means; you can build your team with 3 persons. But if you want to add person in your location, you could add one more person to your team. You can directly send message or team request to people in your location. But when you directly want to send team request to people in your location you can only send 1 time. This limit will vary depending on the membership package. In beginner package, you can only send 1 time a month and only send to in your location. In growing package, 2 time a month and in growing package, you can send without limit. You can view membership package features from paid membership packages page. A person who likes to  share can benefit from the platform for free, forever." +"</p>"

        body_text+="<p>"+ "We are currently a mvp project now and we will deploy other membership packages next days. So, you can easily get the membership package you want." +"</p>"

        body_text+="<p>" + "We hope, we can benefit to build teams for your project. You can send an email to us any time. (hello@leapcrowd.co)" + "</p>"

        body_text+="<p>"+ "See your soon with good news," +"</p>"

    else:
        # ingilizce
        message_title = "Thank you for joining Leapcrowd."
        body_text = "<p>" + "Thank you for joining Leapcrowd. We would like to talk about the Leapcrowd platform." + "</p>"
        body_text += "<p>" + "The main goal of the Leapcrowd platform is to help entrepreneurs build their own project teams. You can build your team with 2 different methods in this platform. 1- You can share your idea and evaluate incoming team requests. 2- If your project is ready but you don't have any team members, you can add your project to the leapcrowd and when you open team request option, you can evaluate incoming team requests." + "</p>"

        body_text += "<p>" + "All members are included in the beginner membership package for now. Which this means; you can build your team with 3 persons. But if you want to add person in your location, you could add one more person to your team. You can directly send message or team request to people in your location. But when you directly want to send team request to people in your location you can only send 1 time. This limit will vary depending on the membership package. In beginner package, you can only send 1 time a month and only send to in your location. In growing package, 2 time a month and in growing package, you can send without limit. You can view membership package features from paid membership packages page. A person who likes to  share can benefit from the platform for free, forever." + "</p>"

        body_text += "<p>" + "We are currently a mvp project now and we will deploy other membership packages next days. So, you can easily get the membership package you want." + "</p>"

        body_text += "<p>" + "We hope, we can benefit to build teams for your project. You can send an email to us any time. (hello@leapcrowd.co)" + "</p>"

        body_text += "<p>" + "See your soon with good news," + "</p>"


    Messages.new_message(selected_user, message_title, body_text)

    return True


def send_message_to_teammate(user_id, owner_user_id, project_name):
    selected_users_fullprofile = UsersFullProfile.select_user(user_id)
    selected_owner_user = UsersFullProfile.select_user(owner_user_id)

    if change_characters(selected_users_fullprofile.full_profile.fullprofilelocation_set.get().location.country) != "turkey" or change_characters(selected_users_fullprofile.full_profile.fullprofilelocation_set.get().location.country) != "turkiye" or change_characters(selected_users_fullprofile.full_profile.fullprofilelocation_set.get().location.country) != "türkiye":
        # eğer kullanıcı türkiye dışından ise ingilizce mesaj
        body_text = "<p>" + get_profile_name_str(selected_owner_user.full_profile.last_name,
                                                 selected_owner_user.full_profile.first_name) + " accepted the team request you sent and added you to the " + project_name + " project as a teammate. We believe that you will have a successful project as a team. As Leapcrowd, we have build our team by following these steps and we started our project." + "</p>"
        body_text += "<p>" + "We recommend that you never fear to trust each other and finish your mvp product and test it as soon as possible. While describing your project to the target group, you should be honest. If at least 100 people support your project from your target group, don't give up and keep moving! Also, read a lot of book. :) " + "</p>"

        body_text += "<p>" + "You can also contact with " + get_profile_name_str(
            selected_owner_user.full_profile.last_name,
            selected_owner_user.full_profile.first_name) + " from " + selected_owner_user.users.email_address + ". We also recommend that you add each other through linkedin and add the project you are working on linkedin. " + "</p>"

        body_text += "<p>We will surprise for successful teams in next months. We can only help with our own efforts right now, unfortunately</p>"
        body_text += "<p>See you,<br/>Leapcrowd Team</p>"
        message_title = "Team request is answered."
    else:
        # eğer türkiyeden ise türkçe mesaj
        message_title = "Takım isteğinize yanıt verildi."
        body_text = "<p>" + get_profile_name_str(selected_owner_user.full_profile.last_name,
                                                 selected_owner_user.full_profile.first_name) + ", göndermiş olduğunuz takım isteğini kabul etti ve sizi " + project_name + " projesine takım arkadaşı olarak ekledi. Takım olarak başarılı bir projeyi hayata geçireceğinize inanıyoruz. Leapcrowd ekibi olarak, biz de aynı bu adımları uygulayarak takımımızı kurduk ve projemize başladık. Bir projeyi hayata geçirmek için, takım olarak birbirinize güvenmekten asla korkmamanızı tavsiye ederiz ve mvp projenizi bir an önce hayata geçirerek test etmenizi öneririz. Mvp projenizi hedef kitlenize açıklarken dürüst olmaktan korkmamanızı öneririz ve eğer mvp projenize hedef kitlenizden en az 100 kişi ilgi gösterir ise pes etmemenizi, devam etmenizi kesinlikle öneririz. Projeniz için öncelikle yatırıma değil, hedef kitlenizin problemini en az minimum koşullarda çözmenizi öneririz." + "</p>"
        body_text += "<p>" + get_profile_name_str(selected_owner_user.full_profile.last_name,
                                                  selected_owner_user.full_profile.first_name) + " ile ayrıca " + selected_owner_user.users.email_address + " e-posta adresinden iletişime geçebilirsin." + "</p>"

        body_text += "<p>Linkedin üzerinden birbirinizi ekleyebilir ve çalıştığınız projeyi linkedin üzerinde belirtip daha çok kişiye ulaşma durumunuz olabilir. Bu yüzden bu adımı da ayrıca düşünmenizi öneriririz.</p>"

        body_text += "<p>İlerleyen zamanlarda, mvp projelerini başarılı bir şekilde hayata geçirmiş takımlar için sürprizlerimiz olacaktır fakat şimdilik kendi emeklerimiz ile ancan bu kadar yardımcı olabiliyoruz.</p>"

        body_text += "<p>Görüşmek üzere,<br/>Leapcrowd Ekibi</p>"

        # pass

    Messages.new_message(UsersFullProfile.select_user(user_id), message_title, body_text)
    # UsersMessages.new_message(None, created_message, None)

    return True

    # try:
    #     created_message = Messages.new_message(UsersFullProfile.select_user(user_id), "", body_text)
    #     UsersMessages.new_message(None, created_message, None)
    #     return True
    # except:
    #     return False

def send_message_to_leapcrowd(message_subject,emailAddress,name,message_content):
    emailaddressList = ["kadircebel@gmail.com","hello@leapcrowd.co"]
    subject = message_subject
    from_email = settings.EMAIL_HOST_USER
    to = emailaddressList

    body_content="name : " + name + "<br/>email:" + emailAddress + "<br/>message:" + message_content

    try:
        msg = EmailMultiAlternatives(subject=subject, body=body_content, from_email=from_email, to=to)
        msg.attach_alternative(body_content, "text/html")
        msg.send()
        return True

    except BadHeaderError:
        return False

def responded_teamrequest_mail(users_list, owner_user_id, project_name):
    # bu kısımdan ekstradan item["id"] geliyor
    message_count = 0

    try:
        for item in users_list:
            responded_teamrequest_mail_content(item["first_name"], item["email_address"], item["id"])
            # bu kısımda özel mesaj atmalıyız karşı tarafa
            if send_message_to_teammate(item["id"], owner_user_id, project_name) is True:
                message_count += 1

        if message_count > 0:
            return True
        else:
            return False
    except:
        return False


def dm_request_teamrequest_mail(email_address, user_id, owner_name, project_name):
    selected_user = UsersFullProfile.select_user(user_id)
    selected_user_country = change_characters(selected_user.full_profile.fullprofilelocation_set.get().location.country)

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = "https://" + Site.objects.get_current().domain + "/static/frontside/bg/4.jpg"
    # print("takım isteği gönderilen kişinin ülkesi : ", selected_user_country)
    if selected_user_country != "turkiye" and selected_user_country != "türkiye" and selected_user_country != "turkey":
        module1message = "Hello " + selected_user.full_profile.first_name.capitalize() + "," + "<br/>The person named " + str(
            owner_name).capitalize() + " sent you a team request to be involved in the " + project_name + " project. You can respond to team request by specifying your own conditions."
        message_title = "You recieved a new team request."
        message_button = "Respond team request invitation"
    else:
        module1message = "Merhaba " + selected_user.full_profile.first_name.capitalize() + "," + "<br/>" + str(
            owner_name).capitalize() + " isimli kişi, " + project_name + " projesine katılman için size takım isteği gönderdi. Gönderilen takım isteğine kendi koşullarını belirleyerek cevap verebilirsin ve birlikte yeni bir projeyi hayata geçirebilirsiniz."
        message_title = "Yeni bir takım isteği aldınız."
        message_button = "Takım isteğine cevap ver"

    newsletter_title = message_title

    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + 'thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + 'img-01.jpg" style="vertical-align:top;" width="600" height="306" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f"><a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="https://' + Site.objects.get_current().domain + '/dashboard/">' + message_button + '</a></td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'
    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'

    subject = "leapcrowd: " + message_title
    from_email = settings.EMAIL_HOST_USER
    to = [email_address]

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True
    except BadHeaderError:
        return False


# özel takım isteğine atılan cevap
def dm_response_to_owner_mail(requested_user_id, project_name, owner_user_id):
    selected_user = UsersFullProfile.select_user(requested_user_id)
    selected_owner_user = UsersFullProfile.select_user(owner_user_id)
    selected_country = change_characters(
        selected_owner_user.full_profile.fullprofilelocation_set.get().location.country)
    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = "https://" + Site.objects.get_current().domain + "/static/frontside/bg/4.jpg"

    if selected_country != "turkey" and selected_country != "turkiye" and selected_country != "türkiye":
        # yabancı
        module1message = "Hello " + selected_owner_user.full_profile.first_name.capitalize() + "," + "<br/>" + selected_user.full_profile.first_name.capitalize() + " sent you reply for your team request. If " + selected_user.full_profile.first_name.capitalize() + "'s conditions are right for you, you can approve him and include on your team."

        message_title = "Your team request were replied for the " + project_name + " project."
        message_button = "Respond your team request"

    else:
        # türkçe
        module1message = "Merhaba " + selected_owner_user.full_profile.first_name.capitalize() + ", <br/>" + "<strong>" + project_name + "</strong>" + " projesine katılması için takım isteği gönderdiğin " + selected_user.full_profile.first_name.capitalize() + " isimli kişiden cevap geldi. <br/>Eğer koşullar senin için de uygunsa, gelen isteği onaylayıp " + selected_user.full_profile.first_name.capitalize() + " isimli kişiyi takımına dahil edebilirsin."

        message_title = project_name + " projesi için takım isteğinize cevap verildi."
        message_button = "Takım isteğine cevap ver"

    newsletter_title = message_title

    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + 'thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + 'img-01.jpg" style="vertical-align:top;" width="600" height="306" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f"><a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="https://' + Site.objects.get_current().domain + '/dashboard/">' + message_button + '</a></td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'

    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'

    subject = "leapcrowd: " + message_title
    from_email = settings.EMAIL_HOST_USER
    to = [selected_owner_user.users.email_address]

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True
    except BadHeaderError:
        return False


def responded_teamrequest_mail_content(first_name, email_address, responded_user_id):
    user_country = change_characters(
        UsersFullProfile.select_user(responded_user_id).full_profile.fullprofilelocation_set.get().location.country)

    style_content = '<style type="text/css">* {-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%;}a{outline:none;color:#40aceb;text-decoration:underline;}a:hover{text-decoration:none !important;}.nav a:hover{text-decoration:underline !important;}.title a:hover{text-decoration:underline !important;}.title-2 a:hover{text-decoration:underline !important;}.btn:hover{opacity:0.8;}.btn a:hover{text-decoration:none !important;}.btn{-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;transition:all 0.3s ease;}table td {border-collapse: collapse !important;}.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}@media only screen and (max-width:500px) {table[class="flexible"]{width:100% !important;}table[class="center"]{float:none !important;margin:0 auto !important;}*[class="hide"]{display:none !important;width:0 !important;height:0 !important;padding:0 !important;font-size:0 !important;line-height:0 !important;}td[class="img-flex"] img{width:100% !important;height:auto !important;}td[class="aligncenter"]{text-align:center !important;}th[class="flex"]{display:block !important;width:100% !important;}td[class="wrapper"]{padding:0 !important;}td[class="holder"]{padding:30px 15px 20px !important;}td[class="nav"]{padding:20px 0 0 !important;text-align:center !important;}td[class="h-auto"]{height:auto !important;}td[class="description"]{padding:30px 20px !important;}td[class="i-120"] img{width:120px !important;height:auto !important;}td[class="footer"]{padding:5px 20px 20px !important;}td[class="footer"] td[class="aligncenter"]{line-height:25px !important;padding:20px 0 0 !important;}tr[class="table-holder"]{display:table !important;width:100% !important;}th[class="thead"]{display:table-header-group !important; width:100% !important;}th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}}</style>'
    address_hyperlink = "https://" + Site.objects.get_current().domain + "/static/frontside/bg/4.jpg"

    if user_country != "turkiye" and user_country != "türkiye" and user_country != "turkey":
        # yabancı
        module1message = "Hello " + first_name + " " + ",<br/> we are happy to inform you that you were accepted for the project you want to join as a teammate."
        message_title = "There is a respond from leapcrowd.co for your team request."
        newsletter_title = "Team request was answered."
        message_button = "Accept team request"

    else:
        # türkçe
        module1message = "Merhaba " + first_name + " " + ",<br/>takım arkadaşı olarak katılmak istediğiniz projeye kabul edildiğinizi bildirmekten mutluluk duyarız."
        message_title = "Gönderdiğiniz takım isteği için cevap verildi."

        newsletter_title = "Takım isteğiniz cevaplandı."
        message_button = "Takım isteğini kabul et"

    # module1message = ugettext(
    #     "Hello " + first_name + " " + ",<br/> we are happy to inform you that you were accepted for the project you want to join as a teammate.")
    # message_title = ugettext("There is a respond from leapcrowd.co for your team request.")
    # newsletter_title = ugettext("Team request was answered.")
    # message_button = ugettext("Accept team request invitation")

    table_module1 = '<table data-module="module-2" data-thumb="' + address_hyperlink + 'thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td class="img-flex"><img src="' + address_hyperlink + 'img-01.jpg" style="vertical-align:top;" width="600" height="306" alt=""></td></tr><tr><td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">' + newsletter_title + '</td></tr><tr><td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">' + module1message + '</td></tr><tr><td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f"><a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="https://' + Site.objects.get_current().domain + '/dashboard/">' + message_button + '</a></td></tr></table></td></tr></table></td></tr><tr><td height="28"></td></tr></table>'
    table_content = '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced"><tr><td class="hide"><table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;"><tr><td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td></tr></table></td></tr><tr><td class="wrapper" style="padding:0 10px;"><table data-module="module-1" data-thumb="thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0"><tr><td data-bgcolor="bg-module" bgcolor="#eaeced"><table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0"><tr><td style="padding:29px 0 30px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><th class="flex" width="113" align="left" style="padding:0;"><table class="center" cellpadding="0" cellspacing="0"><tr><td style="line-height:0;"></td></tr></table></th><th lass="flex" align="left" style="padding:0;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;"></td></tr></table></th></tr></table></td></tr></table></td></tr></table>' + table_module1 + '</td></tr></table>'
    subject = "leapcrowd: " + ugettext(" team request was answered.")
    from_email = settings.EMAIL_HOST_USER
    # to = emailaddressList
    to = [email_address]

    html_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>' + message_title + '</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />' + style_content + '<body style="margin:0; padding:0;" bgcolor="#eaeced">' + table_content + '</body></head></html>'

    try:
        msg = EmailMultiAlternatives(subject=subject, body=html_content, from_email=from_email, to=to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return True
    except BadHeaderError:
        return False


def user_location(users_fullprofiles_id):
    district = ""
    city = ""
    neighborhood = ""
    country = ""
    address_text = ""
    user_fullprofile = UsersFullProfile.select_user(users_fullprofiles_id)
    location = FullprofileLocation.select(user_fullprofile.full_profile)
    if not location.location.neighborhood in [None, '']:
        neighborhood = location.location.neighborhood + ","
    if not location.location.district in [None, '']:
        district = location.location.district + ","
    if not location.location.city in [None, '']:
        city = location.location.city + ","
    if not location.location.country in [None, '']:
        country = location.location.country

    address_text = neighborhood + district + city + country

    return address_text


def control_file_extention(filename):
    ext = str(filename).split('.')[-1]
    orj_exts = ["jpg", "jpeg", "bmp", "png"]
    if ext in orj_exts:
        return True
    else:
        return False


def content_file_name(profile_id, filename):
    import os, uuid
    orj_file = filename
    ext = str(filename).split('.')[-1]
    new_name = uuid.uuid4().hex[:6].upper()
    filename = "%s_%s.%s" % (new_name, profile_id, ext)
    fs = FileSystemStorage()

    fs.save(os.path.join("profiles/", filename), orj_file, max_length=None)
    return os.path.join("/profiles/", filename)


def content_coverphoto_name(profile_id, filename):
    import os, uuid
    orj_file = filename
    ext = str(filename).split('.')[-1]
    new_name = uuid.uuid4().hex[:6].upper()
    filename = "%s_%s.%s" % (new_name, profile_id, ext)
    fs = FileSystemStorage()

    fs.save(os.path.join("covers/", filename), orj_file, max_length=None)
    return os.path.join("/media/covers/", filename)


def delete_project_logo(filename):
    import os
    from django.conf import settings
    mypath = settings.BASE_DIR + filename
    print(mypath)
    os.remove(mypath)
    return True
    # try:
    #     mypath = settings.BASE_DIR + filename
    #     os.remove(mypath)
    #     return True
    # except:
    #     return False


def content_projectlogo_filename(profile_id, filename):
    import os, uuid
    orj_file = filename
    ext = str(filename).split('.')[-1]
    new_name = uuid.uuid4().hex[:6].upper()
    filename = "%s_%s.%s" % (new_name, profile_id, ext)
    fs = FileSystemStorage()
    fs.save(os.path.join("projects/", filename), orj_file, max_length=None)
    return os.path.join("/media/projects/", filename)


def check_file_size(filename):
    import os
    filename.seek(0, os.SEEK_END)
    file_length = filename.tell()
    return file_length


def get_active_avatar(user_fullprofiles_id):
    selected = UsersFullProfile.select_user(user_fullprofiles_id)
    return selected.full_profile.picture_url.url


def get_active_logo(general_informations_id):
    selected = GeneralInfos.select(general_informations_id)
    return selected.project_logo


def get_active_cover(user_fullprofiles_id):
    selected = UsersFullProfile.select_user(user_fullprofiles_id)
    return selected.full_profile.cover_photo


def get_profile_name(lastname, name):
    if lastname in name:
        return True
    else:
        return False


def get_fulllocation_string(district, city):
    # print("uzunluk : ",len(district))
    # print("içerik : ", district)
    if len(city) > 0 and len(str(district).strip()) > 0 and district is not None:

        return district + ", " + city
    elif len(city) > 0:
        return city


def clean_hours_title(title, cleaning_str):
    if cleaning_str in title:
        title = str(title).replace(cleaning_str, "")
        return title
    else:
        title = str(title).replace("hour", "")
        return title


def get_profile_name_str(lastname, firstname):
    if lastname in firstname:
        return firstname
    else:
        return firstname + " " + lastname


def change_characters(str_value):
    str_value = str(str_value).lower().replace('ç', 'c')
    str_value = str(str_value).lower().replace('ı', 'i')
    str_value = str(str_value).lower().replace('ü', 'u')
    str_value = str(str_value).lower().replace('ş', 's')
    str_value = str(str_value).lower().replace('ğ', 'g')
    str_value = str(str_value).lower().replace('_', '-')
    str_value = str(str_value).lower().replace(' ', '-')
    str_value = str(str_value).lower().replace('ş', 's')
    str_value = str(str_value).lower().replace('&', '-')
    str_value = str(str_value).lower().replace('|', '-')
    str_value = str(str_value).lower().replace('@', '')
    str_value = str(str_value).lower().replace('~', '-')
    str_value = str(str_value).lower().replace('€', '')
    str_value = str(str_value).lower().replace('ª', '')
    str_value = str(str_value).lower().replace('`', '')
    str_value = str(str_value).lower().replace('<', '')
    str_value = str(str_value).lower().replace('>', '')
    str_value = str(str_value).lower().replace('µ', 'm')
    str_value = str(str_value).lower().replace('”', '')
    str_value = str(str_value).lower().replace(';', '')
    str_value = str(str_value).lower().replace('î', 'i')
    str_value = str(str_value).lower().replace('û', 'u')
    str_value = str(str_value).lower().replace('ɲ', 'n')
    str_value = str(str_value).lower().replace('¢', 'c')
    str_value = str(str_value).lower().replace('»', '')
    str_value = str(str_value).lower().replace('«', '')
    str_value = str(str_value).lower().replace('ô', 'o')
    str_value = str(str_value).lower().replace('ö', 'o')
    str_value = str(str_value).lower().replace('§', 's')
    str_value = str(str_value).lower().replace('â', 'a')
    str_value = str(str_value).lower().replace('™', '')
    str_value = str(str_value).lower().replace('¶', '')
    str_value = str(str_value).lower().replace('#', '')
    str_value = str(str_value).lower().replace('$', '')
    str_value = str(str_value).lower().replace('{', '')
    str_value = str(str_value).lower().replace('}', '')
    str_value = str(str_value).lower().replace(']', '')
    str_value = str(str_value).lower().replace('[', '')
    # str_value = str(str_value).lower().replace('.', '-')
    # str_value = str(str_value).lower().replace(':', '')

    return str(str_value)


def content_or_blank(str_value):
    try:
        if len(str_value) > 0:
            content_text = str_value
        else:
            content_text = ""
    except:
        content_text = ""

    return content_text


def remodel_typerlink(content_str):
    if "http://" in content_str or "https://" in content_str:
        remodel_hyp = "<a href='" + content_str + "'>" + content_str + "</a>"
    else:
        remodel_hyp = "<a href='http://" + content_str + "'>" + "http://" + content_str + "</a>" + " - " + "<a href='https://" + content_str + "'>" + "https://" + content_str + "</a>"

    return remodel_hyp


def remove_html_tags(content_str):
    # p = re.compile(r'<img.*?/>')
    # return p.sub('', content_str)
    p = re.compile(r'<.*?>')
    return p.sub('', content_str)


def download_image_from_social_account(url_item,users_fullprofile_id):
    import requests
    import tempfile
    from django.core import files

    if len(url_item) > 0 and not "default" in url_item:
        image_urls=[url_item]

        for image_url in image_urls:
            # Steam the image from the url
            request = requests.get(image_url, stream=True)

            # Was the request OK?
            if request.status_code != requests.codes.ok:
                # Nope, error handling, skip file etc etc etc
                continue

            # Get the filename from the url, used for saving later
            file_name = image_url.split('/')[-1]

            # Create a temporary file
            lf = tempfile.NamedTemporaryFile()

            # Read the streamed image in sections
            for block in request.iter_content(1024 * 8):

                # If no more file then stop
                if not block:
                    break

                # Write image block to temporary file
                lf.write(block)

            # Create the model you want to save the image to
            # image = Image()
            selected_user=UsersFullProfile.select_user(users_fullprofile_id)
            selected_user.full_profile.picture_url.save(file_name,files.File(lf))

            # Save the temporary image to the model#
            # This saves the model so be sure that is it valid
            # image.image.save(file_name, files.File(lf))


def get_member_statics(member_type,user_id):
    main_count = Users.list_of_users_membertype(member_type)
    near_count = Users.list_of_nearusers_membertype(member_type, user_id)
    if main_count == 0 or near_count == 0:
        percent_value = 0
    else:
        percent_value = (near_count * 100) // main_count

    item = {
        "main_count": main_count,
        "near_count": near_count,
        "percent_value": percent_value,
        "member_type": member_type,
        "message": ngettext("There is %(near_count)d person close to you",
                            "There are %(near_count)d people close to you", near_count) % {"near_count": near_count}
    }

    return item


def select_teammembers_array(users_list, owner_user_id, team_person_limit, extra_person_limit):
    general_users = []
    user_same_count = 0
    user_different_count = 0

    for user in users_list:
        if team_person_limit == -1:
            general_users.append(user)
            user_different_count += 1
        else:
            if is_it_same_location(owner_user_id,UsersFullProfile.select_user(user).id):
                if user_same_count <= extra_person_limit:
                    general_users.append(user)
                    user_same_count += 1
            else:
                if user_different_count <= team_person_limit:
                    general_users.append(user)
                    user_different_count += 1

    return general_users


def create_crypto_name():
    random_name = uuid.uuid4().hex[:6].upper()
    if not UsersFullProfile.objects.filter(crypto_user_name=random_name).exists():
        return str(random_name)
    else:
        create_crypto_name()


def get_person_profile_name(users_fullprofiles, to_users_fullprofiles):
    owner_users_fullprofiles = UsersFullProfile.select_user(users_fullprofiles)
    selected_users_fullprofiles = UsersFullProfile.select_user(to_users_fullprofiles)
    return get_profile_name_str(selected_users_fullprofiles.full_profile.last_name,
                                selected_users_fullprofiles.full_profile.first_name)
    # if UsersFriend.isThis_my_friend(owner_users_fullprofiles,selected_users_fullprofiles):
    #     return get_profile_name_str(selected_users_fullprofiles.full_profile.last_name,selected_users_fullprofiles.full_profile.first_name)
    # else:
    #     return selected_users_fullprofiles.crypto_user_name


def url_profilename(users_fullprofiles, to_users_fullprofiles):
    owner_users_fullprofiles = UsersFullProfile.select_user(users_fullprofiles)
    selected_users_fullprofiles = UsersFullProfile.select_user(to_users_fullprofiles)
    return inside_url_profilename(selected_users_fullprofiles.full_profile.first_name,
                                  selected_users_fullprofiles.full_profile.last_name)
    # if UsersFriend.isThis_my_friend(owner_users_fullprofiles, selected_users_fullprofiles):
    #     return inside_url_profilename(selected_users_fullprofiles.full_profile.first_name,
    #                                   selected_users_fullprofiles.full_profile.last_name)
    #
    # else:
    #     return str(slugify(selected_users_fullprofiles.crypto_user_name.lower(), allow_unicode=True))


def inside_url_profilename(firstname, lastname):
    if get_profile_name(lastname, firstname):
        new_str = str(firstname).replace(lastname, '') + lastname
    else:
        new_str = firstname + " " + lastname

    new_s = slugify(new_str, allow_unicode=True)

    return change_characters(new_s)


def profile_points_css(users_points):
    if int(users_points) <= 26:
        point_css = "bg-danger"
    elif int(users_points) > 26 and int(users_points) < 100:
        point_css = "bg-warning"
    else:
        point_css = "bg-success"

    return point_css


def match_friends_in_system(users_projects):
    users_teams = UsersTeams.select_teammembers_by_id(users_projects)
    context_member = []
    # member => 43,75,71,74,61,69,64
    for team_member in users_teams:
        context_member.append(team_member.users_fullprofiles.id)

    try:
        for member in context_member:
            for other_member in users_teams:

                if UsersFriend.are_these_friends(member,other_member.users_fullprofiles.id) is False:

                    UsersFriend.make_friends(member,other_member.users_fullprofiles.id)

        return True

    except:

        return False


def is_it_same_location(users_fullprofiles_id,to_users_fullprofiles_id):
    selected_user=UsersFullProfile.select_user(users_fullprofiles_id)
    to_selected_user=UsersFullProfile.select_user(to_users_fullprofiles_id)
    if selected_user.id != to_selected_user.id:

        if selected_user.full_profile.fullprofilelocation_set.get().location.lat == to_selected_user.full_profile.fullprofilelocation_set.get().location.lat and selected_user.full_profile.fullprofilelocation_set.get().location.lng == to_selected_user.full_profile.fullprofilelocation_set.get().location.lng:
            return  True
        elif change_characters(selected_user.full_profile.fullprofilelocation_set.get().location.city) == change_characters(to_selected_user.full_profile.fullprofilelocation_set.get().location.city):

            return True
        else:

            return False
    else:
        return False


def is_it_same_country(users_fullprofiles_id,to_users_fullprofiles_id):
    selected_user = UsersFullProfile.select_user(users_fullprofiles_id)
    to_selected_user = UsersFullProfile.select_user(to_users_fullprofiles_id)
    if selected_user.id != to_selected_user.id:
        if selected_user.full_profile.fullprofilelocation_set.get().location.neighborhood == to_selected_user.full_profile.fullprofilelocation_set.get().location.neighborhood:
            return  True
        else:
            return False

    else:
        return False



def send_signals_to_projects_owner(users_fullprofiles_id):
    current_user = UsersFullProfile.select_user(users_fullprofiles_id)
    context_users_signals=[]
    if UsersLinkedinRecommendations.is_there_signal_id(current_user):
        if current_user.userslinkedinrecommendations_set.get().linkedin_recommendations.recommendation_text not in context_users_signals:
            context_users_signals.append(current_user.userslinkedinrecommendations_set.get().linkedin_recommendations.recommendation_text)

    print(context_users_signals)
    return context_users_signals


def get_post_answers_users_list(users_posts_id,users_fullprofiles_id):
    users_post= UsersPosts.select_this(users_posts_id)
    users_answers_list=UsersAnswers.answers_by_post(users_post)
    context_users_signals=[]

    for pst in users_answers_list:
        if UsersLinkedinRecommendations.is_there_signal_id(pst.users_fullprofiles) and pst.users_fullprofiles.id != users_fullprofiles_id:
            if pst.users_fullprofiles.userslinkedinrecommendations_set.get().linkedin_recommendations.recommendation_text not in context_users_signals:
                context_users_signals.append(pst.users_fullprofiles.userslinkedinrecommendations_set.get().linkedin_recommendations.recommendation_text)

    return context_users_signals

def new_signals_users_list(users_fullprofiles_id):
    context_users_signals = []
    for usr in UsersFullProfile.all_active_users():
        if usr.id != users_fullprofiles_id:
            if UsersLinkedinRecommendations.is_there_signal_id(usr):
                if usr.userslinkedinrecommendations_set.get().linkedin_recommendations.recommendation_text not in context_users_signals:
                    context_users_signals.append(usr.userslinkedinrecommendations_set.get().linkedin_recommendations.recommendation_text)


    return context_users_signals