from django.urls import path
from .views import *

app_name="projects"

urlpatterns=[
    path('',index,name='index'),
    path('detail/<int:id>/',detail,name='projects_detail'),
    path('project-steps/', project_steps_infos, name='projects_steps'),
    path('business-choices/', business_stage_choices, name='business_choices'),
    path('filter-projects/', filter_projects, name='filter_projects'),
    path('user-infos/', user_infos, name='user_infos'),
    path('loadmore-projects/', load_more_projects, name='loadmore_projects'),
    path('update-projecthours/', update_projecthours, name='update_projecthours'),
    path('update-investments/', update_investments, name='update_investments'),
    path('users-supports/', get_users_supports, name='users_supports'),
    path('send-teamrequest/', send_teamrequest, name='send_teamrequest'),
    path('send-feedback/', send_feedback, name='send_feedback'),
    path('project-teammates/', projects_users_teams, name='project_teammates'),

]