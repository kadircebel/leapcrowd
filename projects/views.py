from django.shortcuts import render, HttpResponse, redirect
from django.conf import settings
from persons.models import UsersFullProfile, UsersProjects, UsersProjectsTeamrequests, UsersInvestments, \
    UsersRequestProjectHours, UsersRequestInvestments, Teamrequests, UsersProjectHours, UsersProjectsFeedbacks, \
    UsersHourCosts, UsersTeams,UsersIndustries
from mainadmins.models import ProjectOptions, Steps, ProjectHours, Investments
from projects.models import Projects, ProjectsProjectOptions, ProjectUsageTypes, ProjectsSteps, ProjectPointsScores
# from myprofile.models import FullprofileLocation
from teams.models import Teams
from utils.views import get_fulllocation_string, url_profilename, change_characters, content_or_blank, \
    get_profile_name_str, clean_hours_title,get_person_profile_name,is_it_same_country,is_it_same_location,send_signals_to_projects_owner
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.decorators.csrf import csrf_protect
import json,requests
from django.utils.translation import ugettext
from notifications.models import NotificationsProjectsFeedbacks
from django.views.decorators.cache import cache_page,never_cache

# Create your views here.
@never_cache
def index(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        location_city = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city
        location_country = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country

        counter_projects = UsersProjects.same_location_projects_count(users_fullprofiles.id)

        if counter_projects > 10:
            has_next = True
        else:
            has_next = False

        project_list = UsersProjects.objects.filter().all().order_by("-id")

        # bu kısımda ve diğer filtreleme sayfalarında kişinin kendi şehrinde bir proje yoksa mesaj çıkacak

        context_projects = []

        counter = 0
        for project in project_list:
            if ProjectPointsScores.calculate_points(project.projects) >= 95 and ProjectsProjectOptions.isItOnline(
                    project.projects, ProjectOptions.select("publish-project")):
                if counter <= 10:
                    item_project = {
                        "project_name": project.projects.general_infos.title,
                        "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                        "business_stage": ProjectsSteps.current_steps(project.projects, request.LANGUAGE_CODE),
                        "project_logo": project.projects.general_infos.project_logo,
                        "is_feedback": ProjectsProjectOptions.select_option(project.projects, ProjectOptions.select(
                            "give-feedback")).situation,
                        "is_teamrequest": ProjectsProjectOptions.select_option(project.projects, ProjectOptions.select(
                            "send-teamrequest")).situation,
                        "project_id": project.projects.id,
                    }

                    context_projects.append(item_project)
                    counter += 1
                else:
                    continue

        # business-stage-options
        context_stage = []
        if request.LANGUAGE_CODE == "tr":
            context_stage.append({"id": 0, "title": "Seçiniz"})
            for chooice in Steps.select_list().order_by("id"):
                item = {
                    "id": chooice.id,
                    "title": chooice.step_name,
                }
                context_stage.append(item)
        else:
            context_stage.append({"id": 0, "title": "Select"})
            for chooice in Steps.select_list().order_by("id"):
                item = {
                    "id": chooice.id,
                    "title": chooice.step_name_eng,
                }
                context_stage.append(item)
        # business-stage-options

        context = {
            "projects": context_projects,
            "user_city": location_city,
            "user_country": location_country,
            "option_country":users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood,
            "steps": context_stage,
            "has_next": has_next,
            "page_title":ugettext("Projects"),
        }

        return render(request, "socialside/projects/index.html", context)
    else:
        return redirect("/login/")

@cache_page(60 * 10)
@csrf_protect
def filter_projects(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            selected_opt = request.POST.get("selected_option")
            selected_stg = request.POST.get("selected_stage")
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            context_project = []
            project_list = UsersProjects.objects.filter().all().order_by("-id")

            if len(selected_stg) < 1 or "0" in selected_stg:
                selected_stg = str(Steps.select("idea").id)

            if len(selected_opt) < 1:
                selected_opt = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city

            for project in project_list:
                if ProjectPointsScores.calculate_points(project.projects) >= 95 and ProjectsProjectOptions.isItOnline(
                        project.projects, ProjectOptions.select("publish-project")):
                    if not "all" in selected_opt:

                        if selected_opt == project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood:
                            # eğer projenin adım'ı seçildiyse
                            if len(selected_stg) > 0:
                                try:
                                    if ProjectsSteps.isThereThisStep(project.projects,
                                                                     Steps.select_by_id(selected_stg)):
                                        item_project = {
                                            "message_type": "success",
                                            "project_name": project.projects.general_infos.title,
                                            "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                            "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                          request.LANGUAGE_CODE),
                                            "project_logo": project.projects.general_infos.project_logo.name,
                                            "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                                ProjectOptions.select(
                                                                                                    "give-feedback")).situation,
                                            "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                                   ProjectOptions.select(
                                                                                                       "send-teamrequest")).situation,
                                            "project_id": project.projects.id,
                                        }
                                        context_project.append(item_project)
                                except:
                                    pass

                            else:
                                item_project = {
                                    "message_type": "success",
                                    "project_name": project.projects.general_infos.title,
                                    "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                    "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                  request.LANGUAGE_CODE),
                                    "project_logo": project.projects.general_infos.project_logo.name,
                                    "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                        ProjectOptions.select(
                                                                                            "give-feedback")).situation,
                                    "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                           ProjectOptions.select(
                                                                                               "send-teamrequest")).situation,
                                    "project_id": project.projects.id,
                                }
                                context_project.append(item_project)

                        elif is_it_same_location(users_fullprofiles.id,project.users_fullprofiles.id):
                            if len(selected_stg) > 0:
                                try:
                                    if ProjectsSteps.isThereThisStep(project.projects,
                                                                     Steps.select_by_id(selected_stg)):
                                        item_project = {
                                            "message_type": "success",
                                            "project_name": project.projects.general_infos.title,
                                            "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                            "business_stage": ProjectsSteps.current_steps(project.projects,request.LANGUAGE_CODE),
                                            "project_logo": project.projects.general_infos.project_logo.name,
                                            "is_feedback": ProjectsProjectOptions.select_option(project.projects,ProjectOptions.select("give-feedback")).situation,
                                            "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,ProjectOptions.select("send-teamrequest")).situation,
                                            "project_id": project.projects.id,
                                        }
                                        context_project.append(item_project)
                                except:
                                    pass

                            else:
                                item_project = {
                                    "message_type": "success",
                                    "project_name": project.projects.general_infos.title,
                                    "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                    "business_stage": ProjectsSteps.current_steps(project.projects,request.LANGUAGE_CODE),
                                    "project_logo": project.projects.general_infos.project_logo.name,
                                    "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                        ProjectOptions.select("give-feedback")).situation,
                                    "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                           ProjectOptions.select("send-teamrequest")).situation,
                                    "project_id": project.projects.id,
                                }
                                context_project.append(item_project)

                    else:
                        # eğer hepsi seçeneği seçildiyse
                        if len(selected_stg) > 0:
                            try:
                                if ProjectsSteps.isThereThisStep(project.projects, Steps.select_by_id(selected_stg)):
                                    item_project = {
                                        "message_type": "success",
                                        "project_name": project.projects.general_infos.title,
                                        "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                        "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                      request.LANGUAGE_CODE),
                                        "project_logo": project.projects.general_infos.project_logo.name,
                                        "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                            ProjectOptions.select(
                                                                                                "give-feedback")).situation,
                                        "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                               ProjectOptions.select(
                                                                                                   "send-teamrequest")).situation,
                                        "project_id": project.projects.id,
                                    }
                                    context_project.append(item_project)
                            except:
                                pass
                        else:
                            item_project = {
                                "message_type": "success",
                                "project_name": project.projects.general_infos.title,
                                "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                "business_stage": ProjectsSteps.current_steps(project.projects, request.LANGUAGE_CODE),
                                "project_logo": project.projects.general_infos.project_logo.name,
                                "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                    ProjectOptions.select(
                                                                                        "give-feedback")).situation,
                                "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                       ProjectOptions.select(
                                                                                           "send-teamrequest")).situation,
                                "project_id": project.projects.id,
                            }
                            context_project.append(item_project)

            # print("Listenin Sayısı : ", len(context_project))
            #
            # print(context_project)

            paginator = Paginator(context_project, 10)

            page = 1
            try:
                filter_projects_list = paginator.page(page)
            except PageNotAnInteger:
                filter_projects_list = paginator.page(2)
            except EmptyPage:
                filter_projects_list = paginator.page(paginator.num_pages)

            output_data = {
                "project_list": list(filter_projects_list),
                "has_next": filter_projects_list.has_next(),
            }
            return HttpResponse(json.dumps(output_data), content_type="application/json")

@cache_page(60 * 10)
@csrf_protect
def load_more_projects(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            selected_opt = request.POST.get("selected_option")
            selected_stg = request.POST.get("selected_stage")
            page = request.POST.get("page")

            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            context_project = []
            project_list = UsersProjects.objects.filter().all().order_by("-id")

            if len(selected_opt) < 1:
                selected_opt = users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city

            for project in project_list:
                if ProjectPointsScores.calculate_points(
                        project.projects) >= 95 and ProjectsProjectOptions.isItOnline(project.projects,
                                                                                      ProjectOptions.select(
                                                                                          "publish-project")):
                    if not "all" in selected_opt:
                        if selected_opt == project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.neighborhood:

                            # eğer projenin adım'ı seçildiyse
                            if len(selected_stg) > 0:
                                try:
                                    if ProjectsSteps.select(project.projects, Steps.select_by_id(selected_stg)):
                                        item_project = {
                                            "message_type": "success",
                                            "project_name": project.projects.general_infos.title,
                                            "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                            "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                          request.LANGUAGE_CODE),
                                            "project_logo": project.projects.general_infos.project_logo.name,
                                            "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                                ProjectOptions.select(
                                                                                                    "give-feedback")).situation,
                                            "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                                   ProjectOptions.select(
                                                                                                       "send-teamrequest")).situation,
                                            "project_id": project.projects.id,
                                        }

                                        context_project.append(item_project)
                                except:
                                    item_project = {
                                        "message_type": "error",
                                        "message": ugettext("There is no project of the selected stage."),
                                    }
                                    context_project.append(item_project)
                                    break

                            else:
                                item_project = {
                                    "message_type": "success",
                                    "project_name": project.projects.general_infos.title,
                                    "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                    "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                  request.LANGUAGE_CODE),
                                    "project_logo": project.projects.general_infos.project_logo.name,
                                    "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                        ProjectOptions.select(
                                                                                            "give-feedback")).situation,
                                    "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                           ProjectOptions.select(
                                                                                               "send-teamrequest")).situation,
                                    "project_id": project.projects.id,
                                }
                                context_project.append(item_project)
                        elif is_it_same_location(users_fullprofiles.id,project.users_fullprofiles.id):

                            if len(selected_stg) > 0:
                                try:
                                    if ProjectsSteps.select(project.projects, Steps.select_by_id(selected_stg)):
                                        item_project = {
                                            "message_type": "success",
                                            "project_name": project.projects.general_infos.title,
                                            "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                            "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                          request.LANGUAGE_CODE),
                                            "project_logo": project.projects.general_infos.project_logo.name,
                                            "is_feedback": ProjectsProjectOptions.select_option(project.projects,ProjectOptions.select("give-feedback")).situation,
                                            "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,ProjectOptions.select("send-teamrequest")).situation,
                                            "project_id": project.projects.id,
                                        }

                                        context_project.append(item_project)
                                except:
                                    item_project = {
                                        "message_type": "error",
                                        "message": ugettext("There is no project of the selected stage."),
                                    }
                                    context_project.append(item_project)
                                    break

                            else:
                                item_project = {
                                    "message_type": "success",
                                    "project_name": project.projects.general_infos.title,
                                    "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                    "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                  request.LANGUAGE_CODE),
                                    "project_logo": project.projects.general_infos.project_logo.name,
                                    "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                        ProjectOptions.select("give-feedback")).situation,
                                    "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                           ProjectOptions.select("send-teamrequest")).situation,
                                    "project_id": project.projects.id,
                                }
                                context_project.append(item_project)

                    else:
                        # eğer hepsi seçeneği seçildiyse
                        if len(selected_stg) > 0:
                            try:
                                if ProjectsSteps.select(project.projects, Steps.select_by_id(selected_stg)):
                                    item_project = {
                                        "message_type": "success",
                                        "project_name": project.projects.general_infos.title,
                                        "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                        "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                                      request.LANGUAGE_CODE),
                                        "project_logo": project.projects.general_infos.project_logo.name,
                                        "is_feedback": ProjectsProjectOptions.select_option(project.projects,ProjectOptions.select("give-feedback")).situation,
                                        "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,ProjectOptions.select("send-teamrequest")).situation,
                                        "project_id": project.projects.id,
                                    }
                                    context_project.append(item_project)
                            except:
                                item_project = {
                                    "message_type": "error",
                                    "message": ugettext("There is no project of the selected stage."),
                                }
                                context_project.append(item_project)
                                break

                        else:
                            item_project = {
                                "message_type": "success",
                                "project_name": project.projects.general_infos.title,
                                "project_location": project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city + ", " + project.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                                "business_stage": ProjectsSteps.current_steps(project.projects,
                                                                              request.LANGUAGE_CODE),
                                "project_logo": project.projects.general_infos.project_logo.name,
                                "is_feedback": ProjectsProjectOptions.select_option(project.projects,
                                                                                    ProjectOptions.select(
                                                                                        "give-feedback")).situation,
                                "is_teamrequest": ProjectsProjectOptions.select_option(project.projects,
                                                                                       ProjectOptions.select(
                                                                                           "send-teamrequest")).situation,
                                "project_id": project.projects.id,
                            }
                            context_project.append(item_project)

            paginator = Paginator(context_project, 10)

            try:
                filter_projects_list = paginator.page(page)
            except PageNotAnInteger:
                filter_projects_list = paginator.page(2)
            except EmptyPage:
                filter_projects_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "project_list": list(filter_projects_list),
                "has_next": filter_projects_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@cache_page(60 * 60)
def business_stage_choices(request):
    context = []
    if request.LANGUAGE_CODE == "tr":
        context.append({"id": 0, "title": "Seçiniz"})
        for chooice in Steps.select_list().order_by("id"):
            item = {
                "id": chooice.id,
                "title": chooice.step_name,
            }
            context.append(item)
    else:
        context.append({"id": 0, "title": "Select"})
        for chooice in Steps.select_list().order_by("id"):
            item = {
                "id": chooice.id,
                "title": chooice.step_name_eng,
            }
            context.append(item)

    return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(60 * 10)
def detail(request, id):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        selected_project = Projects.select(id)

        try:

            if UsersProjectsTeamrequests.isThereRequest(users_fullprofiles,selected_project.usersprojects_set.get()) or UsersTeams.isThere_user_this_team(users_fullprofiles, Teams.select_team(selected_project.usersprojects_set.get())):
                # eğer teklif aldıysam ve takımda isem
                context_sectors = []
                context_usagetypes = []
                context_steps = []

                if selected_project.usersprojects_set.get().users_fullprofiles.id == users_fullprofiles.id:
                    button_grp = False
                else:
                    button_grp = True

                # print(button_grp)

                if ProjectsProjectOptions.po_situation(Projects.select(id), ProjectOptions.select("send-teamrequest")):
                    teamrequest = ugettext("COLLECTING TEAM REQUESTS")
                else:
                    teamrequest = ""

                if ProjectsProjectOptions.po_situation(Projects.select(id), ProjectOptions.select("give-feedback")):
                    feedback = ugettext("COLLECTING FEEDBACKS")
                else:
                    feedback = ""

                for sector in selected_project.projectprjsectors_set.all():
                    if request.LANGUAGE_CODE == "tr":
                        item = {"title": sector.project_sectors.title}
                    else:
                        item = {"title": sector.project_sectors.title_eng}

                    context_sectors.append(item)

                for usage_types in ProjectUsageTypes.select_list_by_project(selected_project):
                    if request.LANGUAGE_CODE == "tr":
                        item_types = {"title": usage_types.usage_types.title}
                    else:
                        item_types = {"title": usage_types.usage_types.title_eng}

                    context_usagetypes.append(item_types)

                summary = content_or_blank(selected_project.general_infos.describe)
                website = content_or_blank(selected_project.general_infos.project_website)
                if selected_project.business_plans is None:
                    target_group=""
                    revenue_model=""
                else:
                    target_group = content_or_blank(selected_project.business_plans.target_group_content)
                    revenue_model = content_or_blank(selected_project.business_plans.revenue_model)

                if selected_project.requirements is None:
                    existing_requirements=""
                    needed_requirements=""
                else:
                    existing_requirements = content_or_blank(selected_project.requirements.existing_requirements)
                    needed_requirements = content_or_blank(selected_project.requirements.needed_requirements)

                if selected_project.competitors is None:
                    competitors=""
                else:
                    competitors = content_or_blank(selected_project.competitors.competitor_content)

                context = {
                    "profile_location": get_fulllocation_string(
                        selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                        selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + ", " + selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                    "project_title": selected_project.general_infos.title,
                    "teamrequest": teamrequest,
                    "feedback": feedback,
                    "url_username": url_profilename(users_fullprofiles.id,selected_project.usersprojects_set.get().users_fullprofiles.id),
                    "url_project_title": change_characters(selected_project.general_infos.title),
                    "project_sectors": context_sectors,
                    "usage_types": context_usagetypes,
                    "project_summary": summary,
                    "website": website,
                    "target_group": target_group,
                    "revenue_model": revenue_model,
                    "existing_requirements": existing_requirements,
                    "needed_requirements": needed_requirements,
                    "competitors": competitors,
                    "project_steps": context_steps,
                    "project_id": id,
                    "profile_id": users_fullprofiles.id,
                    "button_grp": button_grp,
                }
                return render(request, 'socialside/projects/detail.html', context)
            else:
                #teklif almadıysam veya takımda değilsem proje aktif mi
                if UsersProjects.isThere_activeProject(selected_project):
                    context_sectors = []
                    context_usagetypes = []
                    context_steps = []



                    if selected_project.usersprojects_set.get().users_fullprofiles.id is users_fullprofiles.id:
                        button_grp = False
                    else:
                        button_grp = True


                    if ProjectsProjectOptions.po_situation(Projects.select(id), ProjectOptions.select("send-teamrequest")):
                        teamrequest = ugettext("COLLECTING TEAM REQUESTS")
                    else:
                        teamrequest = ""

                    if ProjectsProjectOptions.po_situation(Projects.select(id), ProjectOptions.select("give-feedback")):
                        feedback = ugettext("COLLECTING FEEDBACKS")
                    else:
                        feedback = ""

                    for sector in selected_project.projectprjsectors_set.all():
                        if request.LANGUAGE_CODE == "tr":
                            item = {"title": sector.project_sectors.title}
                        else:
                            item = {"title": sector.project_sectors.title_eng}

                        context_sectors.append(item)

                    for usage_types in ProjectUsageTypes.select_list_by_project(selected_project):
                        if request.LANGUAGE_CODE == "tr":
                            item_types = {"title": usage_types.usage_types.title}
                        else:
                            item_types = {"title": usage_types.usage_types.title_eng}

                        context_usagetypes.append(item_types)

                    summary = content_or_blank(selected_project.general_infos.describe)
                    website = content_or_blank(selected_project.general_infos.project_website)
                    target_group = content_or_blank(selected_project.business_plans.target_group_content)
                    revenue_model = content_or_blank(selected_project.business_plans.revenue_model)
                    existing_requirements = content_or_blank(selected_project.requirements.existing_requirements)
                    needed_requirements = content_or_blank(selected_project.requirements.needed_requirements)
                    competitors = content_or_blank(selected_project.competitors.competitor_content)

                    context = {
                        "profile_location": get_fulllocation_string(
                            selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                            selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + ", " + selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                        "project_title": selected_project.general_infos.title,
                        "teamrequest": teamrequest,
                        "feedback": feedback,
                        "url_username": url_profilename(
                            selected_project.usersprojects_set.get().users_fullprofiles.full_profile.first_name,
                            selected_project.usersprojects_set.get().users_fullprofiles.full_profile.last_name),
                        "url_project_title": change_characters(selected_project.general_infos.title),
                        "project_sectors": context_sectors,
                        "usage_types": context_usagetypes,
                        "project_summary": summary,
                        "website": website,
                        "target_group": target_group,
                        "revenue_model": revenue_model,
                        "existing_requirements": existing_requirements,
                        "needed_requirements": needed_requirements,
                        "competitors": competitors,
                        "project_steps": context_steps,
                        "project_id": id,
                        "profile_id": selected_project.usersprojects_set.get().users_fullprofiles.id,
                        "button_grp": button_grp,
                    }
                    return render(request, 'socialside/projects/detail.html', context)
                else:
                    #proje aktif değilse projeler sayfasına yönlendir
                    return redirect("/dashboard/projects/")

        except:
            if UsersProjects.isThere_activeProject(selected_project):
                context_sectors = []
                context_usagetypes = []
                context_steps = []

                if selected_project.usersprojects_set.get().users_fullprofiles.id is users_fullprofiles.id:
                    button_grp = False
                else:
                    button_grp = True

                if ProjectsProjectOptions.po_situation(Projects.select(id), ProjectOptions.select("send-teamrequest")):
                    teamrequest = ugettext("COLLECTING TEAM REQUESTS")
                else:
                    teamrequest = ""

                if ProjectsProjectOptions.po_situation(Projects.select(id), ProjectOptions.select("give-feedback")):
                    feedback = ugettext("COLLECTING FEEDBACKS")
                else:
                    feedback = ""

                for sector in selected_project.projectprjsectors_set.all():
                    if request.LANGUAGE_CODE == "tr":
                        item = {"title": sector.project_sectors.title}
                    else:
                        item = {"title": sector.project_sectors.title_eng}

                    context_sectors.append(item)

                for usage_types in ProjectUsageTypes.select_list_by_project(selected_project):
                    if request.LANGUAGE_CODE == "tr":
                        item_types = {"title": usage_types.usage_types.title}
                    else:
                        item_types = {"title": usage_types.usage_types.title_eng}

                    context_usagetypes.append(item_types)

                summary = content_or_blank(selected_project.general_infos.describe)
                website = content_or_blank(selected_project.general_infos.project_website)
                target_group = content_or_blank(selected_project.business_plans.target_group_content)
                revenue_model = content_or_blank(selected_project.business_plans.revenue_model)
                existing_requirements = content_or_blank(selected_project.requirements.existing_requirements)
                needed_requirements = content_or_blank(selected_project.requirements.needed_requirements)
                competitors = content_or_blank(selected_project.competitors.competitor_content)

                context = {
                    "profile_location": get_fulllocation_string(
                        selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,
                        selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city) + ", " + selected_project.usersprojects_set.get().users_fullprofiles.full_profile.fullprofilelocation_set.get().location.country,
                    "project_title": selected_project.general_infos.title,
                    "teamrequest": teamrequest,
                    "feedback": feedback,
                    "url_username": url_profilename(users_fullprofiles.id,selected_project.usersprojects_set.get().users_fullprofiles.id),
                    "url_project_title": change_characters(selected_project.general_infos.title),
                    "project_sectors": context_sectors,
                    "usage_types": context_usagetypes,
                    "project_summary": summary,
                    "website": website,
                    "target_group": target_group,
                    "revenue_model": revenue_model,
                    "existing_requirements": existing_requirements,
                    "needed_requirements": needed_requirements,
                    "competitors": competitors,
                    "project_steps": context_steps,
                    "project_id": id,
                    "profile_id": selected_project.usersprojects_set.get().users_fullprofiles.id,
                    "button_grp": button_grp,
                }
                return render(request, 'socialside/projects/detail.html', context)
            else:
                # proje aktif değilse projeler sayfasına yönlendir
                return redirect("/dashboard/projects/")

    else:
        return redirect("/login/")

@cache_page(60 * 10)
@csrf_protect
def project_steps_infos(request):
    if 'user_fullprofile' in request.session:
        context_step = []
        # users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            project_id = request.GET.get("project_id")
            selected_project = Projects.select(project_id)

            for step in Steps.objects.all().order_by("id"):
                if ProjectsSteps.isThereThisStep(selected_project, step):
                    if request.LANGUAGE_CODE == "tr":
                        item_step = {"title": step.step_name, "situation": "complete", "code": step.step_code}
                    else:
                        item_step = {"title": step.step_name_eng, "situation": "complete", "code": step.step_code}
                else:
                    if request.LANGUAGE_CODE == "tr":
                        item_step = {"title": step.step_name, "situation": "processing", "code": step.step_code}
                    else:
                        item_step = {"title": step.step_name_eng, "situation": "processing", "code": step.step_code}

                context_step.append(item_step)

        return HttpResponse(json.dumps(context_step), content_type="application/json")

@cache_page(60 * 10)
@csrf_protect
def projects_users_teams(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            project_id = request.GET.get("project_id")
            selected_project = Projects.select(project_id)
            context_team_mates = []
            try:
                selected_team = Teams.teammates_by_usersprojects(selected_project.usersprojects_set.get())
                selected_project_team = UsersTeams.get_teammembers_by_teams(selected_team)

                for team_member in selected_project_team:
                    profile_name=get_person_profile_name(users_fullprofiles.id,team_member.users_fullprofiles.id)
                    # profile_name = get_profile_name_str(team_member.users_fullprofiles.full_profile.last_name,
                    #                                     team_member.users_fullprofiles.full_profile.first_name)

                    if not "default" in team_member.users_fullprofiles.full_profile.picture_url.name:
                        profile_image = team_member.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_image = team_member.users_fullprofiles.full_profile.linkedin_picture_url

                    item = {
                        "profile_name": profile_name,
                        "profile_image": profile_image,
                        "profile_id": team_member.users_fullprofiles.id,
                    }
                    context_team_mates.append(item)

                context = {
                    "team_mates": list(context_team_mates),
                }
            except:
                context={
                    "team_mates":list(context_team_mates)
                }




            return HttpResponse(json.dumps(context), content_type="application/json")

@cache_page(60 * 5)
def user_infos(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            project_id = request.GET.get("project_id")
            users_projects = UsersProjects.select(Projects.select(project_id))



            if "entrepreneur" in users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    current_user_position_title = "Girişimci"
                else:
                    current_user_position_title=users_fullprofiles.users.member_type
            else:
                if len(users_fullprofiles.full_profile.position_title) > 0:
                    current_user_position_title = users_fullprofiles.full_profile.position_title
                else:
                    current_user_position_title=UsersIndustries.select(users_fullprofiles).industries.description
                    # current_user_position_title = users_fullprofiles.usersındustries_set.get().industries.description

            if not "default" in users_fullprofiles.full_profile.picture_url.name:
                current_user_profile_image = users_fullprofiles.full_profile.picture_url.url
            else:
                current_user_profile_image = users_fullprofiles.full_profile.linkedin_picture_url

            if "entrepreneur" in users_projects.users_fullprofiles.users.member_type:
                if request.LANGUAGE_CODE == "tr":
                    requested_user_position_title="Girişimci"
                else:
                    requested_user_position_title=users_projects.users_fullprofiles.users.member_type
            else:
                if len(users_projects.users_fullprofiles.full_profile.position_title) > 0:
                    requested_user_position_title = users_projects.users_fullprofiles.full_profile.position_title
                else:
                    requested_user_position_title=UsersIndustries.select(users_projects.users_fullprofiles).industries.description
                    # requested_user_position_title = users_projects.users_fullprofiles.usersındustries_set.get().industries.description

            if not "default" in users_projects.users_fullprofiles.full_profile.picture_url.name:
                requested_user_profile_image = users_projects.users_fullprofiles.full_profile.picture_url.url
            else:
                requested_user_profile_image = users_projects.users_fullprofiles.full_profile.linkedin_picture_url

            context = {
                "current_user_profile_img": current_user_profile_image,
                "current_user_position_title": current_user_position_title,
                "current_user_profilename": get_profile_name_str(users_fullprofiles.full_profile.last_name,
                                                                 users_fullprofiles.full_profile.first_name),
                # "requested_user_profilename": get_profile_name_str(
                #     users_projects.users_fullprofiles.full_profile.last_name,
                #     users_projects.users_fullprofiles.full_profile.first_name),
                "requested_user_profilename":get_person_profile_name(users_fullprofiles.id,users_projects.users_fullprofiles.id),
                "requested_user_profile_img": requested_user_profile_image,
                "requested_user_position_title": requested_user_position_title,
                "message_type": "success",
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def get_users_supports(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            projects_id = request.GET.get("projects_id")
            selected_projects = UsersProjects.select(Projects.select(projects_id))
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])

            if request.LANGUAGE_CODE == "tr":
                if UsersProjectsTeamrequests.isThereRequest(users_fullprofiles, selected_projects):
                    selected_projectrequest = UsersProjectsTeamrequests.select(users_fullprofiles, selected_projects)

                    project_hours = clean_hours_title(
                        selected_projectrequest.teamrequest.users_request_projecthours.project_hours.title, "saat")
                    project_investments = selected_projectrequest.teamrequest.users_request_investments.investments.title
                    data_project_hours = selected_projectrequest.teamrequest.users_request_projecthours.project_hours.id
                    data_investments = selected_projectrequest.teamrequest.users_request_investments.investments.id

                else:
                    project_hours = clean_hours_title(
                        users_fullprofiles.usersprojecthours_set.get().project_hours.title, "saat")
                    project_investments = UsersInvestments.select_users_investment(users_fullprofiles).investments.title
                    data_project_hours = users_fullprofiles.usersprojecthours_set.get().project_hours.id
                    data_investments = UsersInvestments.select_users_investment(users_fullprofiles).investments.id

            else:
                if UsersProjectsTeamrequests.isThereRequest(users_fullprofiles, selected_projects):
                    selected_projectrequest = UsersProjectsTeamrequests.select(users_fullprofiles, selected_projects)

                    project_hours = clean_hours_title(
                        selected_projectrequest.teamrequest.users_request_projecthours.project_hours.title_eng, "hours")
                    project_investments = selected_projectrequest.teamrequest.users_request_investments.investments.title_eng
                    data_project_hours = selected_projectrequest.teamrequest.users_request_projecthours.project_hours.id
                    data_investments = selected_projectrequest.teamrequest.users_request_investments.investments.id

                else:
                    project_hours = clean_hours_title(
                        users_fullprofiles.usersprojecthours_set.get().project_hours.title_eng, "hours")
                    project_investments = UsersInvestments.select_users_investment(
                        users_fullprofiles).investments.title_eng
                    data_project_hours = users_fullprofiles.usersprojecthours_set.get().project_hours.id
                    data_investments = UsersInvestments.select_users_investment(users_fullprofiles).investments.id

            context = {
                "project_hours": project_hours,
                "support_type": project_investments,
                "data_support_type": data_investments,
                "data_project_hours": data_project_hours,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def update_projecthours(request):
    if 'user_fullprofile' in request.session:
        users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if request.method == "GET":
            project_id = request.GET.get("project_id")
            ph_id = request.GET.get("id")
            users_projects = UsersProjects.select(Projects.select(project_id))

            if UsersProjectsTeamrequests.isThereRequest(users_fullprofiles, users_projects):
                project_hours = ProjectHours.select_project_hours(ph_id)
                selected_upt = UsersProjectsTeamrequests.select(users_fullprofiles, users_projects)

                if selected_upt.teamrequest.users_request_projecthours is None:
                    users_request_projecthours = UsersRequestProjectHours.create(project_hours, 0)
                    Teamrequests.create(0, users_request_projecthours, selected_upt.teamrequest.id)
                else:
                    users_request_projecthours = UsersRequestProjectHours.create(project_hours,
                                                                                 selected_upt.teamrequest.users_request_projecthours.id)

            else:
                # eğer yeni request yapıyorsam
                project_hours = ProjectHours.select_project_hours(ph_id)
                users_request_projecthours = UsersRequestProjectHours.create(project_hours, 0)
                teamrequests = Teamrequests.create(0, users_request_projecthours, 0)
                UsersProjectsTeamrequests.create(users_fullprofiles, users_projects, teamrequests)

            if request.LANGUAGE_CODE == "tr":
                selected_project_hours = clean_hours_title(users_request_projecthours.project_hours.title, "saat")
            else:
                selected_project_hours = clean_hours_title(users_request_projecthours.project_hours.title_eng, "hours")

            context = {
                "message_type": "success",
                "selected_project_hours": selected_project_hours,
                "data_project_hours": users_request_projecthours.project_hours.id,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def update_investments(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            st_id = request.GET.get("id")
            # print(st_id)
            project_id = request.GET.get("project_id")
            users_projects = UsersProjects.select(Projects.select(project_id))

            if UsersProjectsTeamrequests.isThereRequest(users_fullprofiles, users_projects):
                if "paid-support" in Investments.select_invest_type(st_id).title_code:
                    if UsersHourCosts._isUsersHourCostsClear(users_fullprofiles):
                        investments = Investments.select_invest_type(st_id)
                    else:
                        investments = Investments.select_by_code("equal-paid")
                else:
                    investments = Investments.select_invest_type(st_id)

                selected_upt = UsersProjectsTeamrequests.select(users_fullprofiles, users_projects)
                # daha önceden yaptığım bir invest var mı

                if selected_upt.teamrequest.users_request_investments is None:
                    users_request_investments = UsersRequestInvestments.create(investments, 0)
                    Teamrequests.create(users_request_investments, 0, selected_upt.teamrequest.id)
                else:
                    users_request_investments = UsersRequestInvestments.create(investments,
                                                                               selected_upt.teamrequest.users_request_investments.id)

            else:
                if "paid-support" in Investments.select_invest_type(st_id).title_code:
                    if UsersHourCosts._isUsersHourCostsClear(users_fullprofiles):
                        investments = Investments.select_invest_type(st_id)
                    else:
                        investments = Investments.select_by_code("equal-paid")
                else:
                    investments = Investments.select_invest_type(st_id)

                users_request_investments = UsersRequestInvestments.create(investments, 0)
                teamrequest = Teamrequests.create(users_request_investments, 0, 0)
                UsersProjectsTeamrequests.create(users_fullprofiles, users_projects, teamrequest)

            if request.LANGUAGE_CODE == "tr":
                selected_investments = users_request_investments.investments.title
            else:
                selected_investments = users_request_investments.investments.title_eng

            context = {
                "message_type": "success",
                "investment": selected_investments,
                "data_investment": users_request_investments.investments.id,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_teamrequest(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            projects_id = request.POST.get("post")
            users_projects = UsersProjects.select(Projects.select(projects_id))
            # print("***************")
            # print(UsersProjectsTeamrequests.isThereRequest(users_fullprofiles, users_projects))
            # print("***************")
            if UsersProjectsTeamrequests.isThereRequest(users_fullprofiles, users_projects) is False:
                request_ph = UsersRequestProjectHours.create(
                    UsersProjectHours.select_user_projecthours(users_fullprofiles).project_hours, 0)
                request_pi = UsersRequestInvestments.create(
                    UsersInvestments.select_users_investment(users_fullprofiles).investments, 0)
                created_teamrequest = Teamrequests.create(request_pi, request_ph, 0)
                UsersProjectsTeamrequests.create(users_fullprofiles, users_projects,
                                                 created_teamrequest).confirm_teamrequest(users_fullprofiles,
                                                                                          users_projects)

                context = {
                    "message_type": "success",
                    "message": ugettext("Your team request has been successfully sent."),
                }

            else:

                selected_porject_request = UsersProjectsTeamrequests.select(users_fullprofiles, users_projects)

                if selected_porject_request.teamrequest.users_request_investments is None:
                    request_investments = UsersRequestInvestments.create(UsersInvestments.select_users_investment(
                        selected_porject_request.users_fullprofiles).investments, 0)
                    Teamrequests.create(request_investments, 0, selected_porject_request.teamrequest.id)

                if selected_porject_request.teamrequest.users_request_projecthours is None:
                    request_projecthours = UsersRequestProjectHours.create(UsersProjectHours.select_user_projecthours(
                        selected_porject_request.users_fullprofiles).project_hours, 0)
                    Teamrequests.create(0, request_projecthours, selected_porject_request.teamrequest.id)

                UsersProjectsTeamrequests.confirm_teamrequest(users_fullprofiles, users_projects)
                context = {
                    "message_type": "success",
                    "message": ugettext("Your team request has been successfully sent."),
                }



                notification_message_tr = get_profile_name_str(users_fullprofiles.full_profile.last_name,users_fullprofiles.full_profile.first_name) + " size takım isteği gönderdi."
                notification_message_en = get_profile_name_str(users_fullprofiles.full_profile.last_name,
users_fullprofiles.full_profile.first_name) + " sent a team request."

                payload = {
                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                    "include_player_ids": send_signals_to_projects_owner(
                        users_projects.users_fullprofiles.id),
                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                    "url": "/dashboard/myprojects/requests/" + str(
                        users_projects.id) + "/"
                }

                requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,data=json.dumps(payload))

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
@csrf_protect
def send_feedback(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofiles = UsersFullProfile.select_user(request.session["user_fullprofile"])
            projects_id = request.POST.get("project_id")
            feedback_content = request.POST.get("feedback")


            if UsersProjects.isThereProject_by_project(Projects.select(projects_id)):

                users_projects = UsersProjects.select(Projects.select(projects_id))

                if len(feedback_content) >= 120:
                    created_feedback = UsersProjectsFeedbacks.create(users_fullprofiles, users_projects,
                                                                     feedback_content)
                    NotificationsProjectsFeedbacks.create_notification(created_feedback)

                    context = {
                        "message_type": "success",
                        "message": ugettext("Your feedback has been forwarded to the project owner. Thanks."),
                    }

                    notification_message_tr = get_profile_name_str(
                        created_feedback.users_fullprofiles.full_profile.last_name,
                        created_feedback.users_fullprofiles.full_profile.first_name) + " projenize bir geri bildirimde bulundu."
                    notification_message_en = get_profile_name_str(
                        created_feedback.users_fullprofiles.full_profile.last_name,
                        created_feedback.users_fullprofiles.full_profile.first_name) + " wrote a feedback to your project."

                    payload = {
                        "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                        "include_player_ids": send_signals_to_projects_owner(created_feedback.users_fullprofiles.id),
                        "contents": {"en": notification_message_en, "tr": notification_message_tr},
                        "url": "http://localhost:8000/dashboard/myprojects/requests/" + str(
                            users_projects.id) + "/"
                    }

                    requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,data=json.dumps(payload))

                else:
                    context = {
                        "message_type": "warning",
                        "message": ugettext(
                            "We can't send your feedback because it's empty. You've to enter at least 120 characters."),
                    }

                return HttpResponse(json.dumps(context), content_type="application/json")
