from django.db import models


class Projects(models.Model):
    id = models.BigAutoField(primary_key=True)
    general_infos = models.ForeignKey('GeneralInfos', models.DO_NOTHING, blank=True, null=True)
    business_plans = models.ForeignKey('BusinessPlans', models.DO_NOTHING, blank=True, null=True)
    requirements = models.ForeignKey('Requirements', models.DO_NOTHING, blank=True, null=True)
    competitors = models.ForeignKey('Competitors', models.DO_NOTHING, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(auto_created=True)

    class Meta:
        managed = True
        db_table = 'projects'

    @classmethod
    def create(cls, general_infos, business_plans, requirements, competitors, id):
        if Projects.isThereProject(id):
            selected = Projects.select(id)
            selected.general_infos = general_infos
            selected.business_plans = business_plans
            selected.requirements = requirements
            selected.competitors = competitors
            selected.is_active = True
            selected.save()
            return selected
        else:
            return cls.objects.update_or_create(general_infos, business_plans, requirements, competitors, True)

    @classmethod
    def create_generals(cls, general_infos, id):

        if Projects.isThereProject(id):
            selected = Projects.select(id)
            selected.general_infos = general_infos
            selected.is_active = True
            selected.save()
            return selected
        else:
            new_record = cls(general_infos=general_infos, is_active=True)
            new_record.save()
            return new_record

    @classmethod
    def create_business(cls, business_plans, id):
        if Projects.isThereProject(id):
            selected = Projects.select(id)
            selected.business_plans = business_plans
            selected.save()
            return selected
        else:
            new_record = cls(business_plans=business_plans)
            new_record.save()
            return new_record

    @classmethod
    def create_requirements(cls, requirements, id):
        if Projects.isThereProject(id):
            selected = Projects.select(id)
            selected.requirements = requirements
            selected.save()
            return selected
        else:
            new_record = cls(requirements=requirements)
            new_record.save()
            return new_record

    @classmethod
    def create_competitors(cls, competitors, id):
        if Projects.isThereProject(id):
            selected = Projects.select(id)
            selected.competitors = competitors
            selected.save()
            return selected
        else:
            new_record = cls(competitors=competitors)
            new_record.save()
            return new_record

    @classmethod
    def select(cls, id):
        if Projects.isThereProject(id):
            new_record = cls.objects.filter(id=id).get()
            return new_record

    @classmethod
    def isThereProject(cls, id):
        try:
            if cls.objects.filter(id=id).exists():
                return True
            else:
                return False
        except:
            return False

class ProjectsSteps(models.Model):
    projects = models.ForeignKey('Projects', models.DO_NOTHING, blank=True)
    steps = models.ForeignKey('mainadmins.Steps', models.DO_NOTHING, blank=True)
    created_date = models.DateField(auto_now_add=True)
    up_date = models.DateField(auto_now_add=True)
    id = models.BigAutoField(primary_key=True)

    class Meta:
        managed = True
        db_table = 'projects_steps'

    @classmethod
    def create(cls, projects, steps):
        projects_steps = cls.objects.update_or_create(projects=projects, steps=steps)
        return projects_steps

    @classmethod
    def create_step(cls, projects, steps):
        projects_steps = cls.objects.update_or_create(projects=projects, steps=steps)
        return projects_steps

    @classmethod
    def delete_steps(cls, projects, steps):
        if ProjectsSteps.isThereThisStep(projects, steps):
            return ProjectsSteps.select(projects, steps).delete()

    @classmethod
    def select(cls, projects, steps):
        return cls.objects.filter(projects=projects, steps=steps).get()

    @classmethod
    def select_steps(cls, projects):
        return cls.objects.filter(projects=projects).all()

    @classmethod
    def isThereThisStep(cls, projects, steps):
        if cls.objects.filter(projects=projects, steps=steps).exists():
            return True
        else:
            return False

    @classmethod
    def current_steps(cls,projects,language):
        try:
            if language=="tr":
                return cls.objects.filter(projects=projects).all().order_by("-steps_id")[:1].get().steps.step_name
            else:
                return cls.objects.filter(projects=projects).all().order_by("-steps_id")[:1].get().steps.step_name_eng
        except:
            return ""


class ProjectsProjectOptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    projects = models.ForeignKey('Projects', models.DO_NOTHING, blank=True)
    project_options = models.ForeignKey('mainadmins.ProjectOptions', models.DO_NOTHING, blank=True)
    situation = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'projects_projectoptions'

    @classmethod
    def create(cls, projects, project_options, situation):
        if cls.objects.filter(projects=projects, project_options=project_options).exists():
            selected = cls.objects.filter(projects=projects, project_options=project_options).get()
            selected.situation = situation
            selected.save()
            return selected
        else:
            new_record = cls(projects=projects, project_options=project_options, situation=situation)
            new_record.save()
            return new_record

    @classmethod
    def select_options(cls, projects):
        if cls.objects.filter(projects=projects).exists():
            return cls.objects.filter(projects=projects).all()

    @classmethod
    def select_option(cls, projects, project_options):
        
        if cls.objects.filter(projects=projects, project_options=project_options).exists():
            return cls.objects.filter(projects=projects, project_options=project_options).get()

    @classmethod
    def isItOnline(cls,projects,project_options):
        try:
            if cls.objects.filter(projects=projects, project_options=project_options).exists():
                return cls.objects.filter(projects=projects, project_options=project_options).get().situation
            else:
                return False
        except:
            return False



    @classmethod
    def po_situation(cls,projects,project_options):
        if cls.objects.filter(projects=projects, project_options=project_options).exists():
            return cls.objects.filter(projects=projects, project_options=project_options).get().situation
        else:
            return False

    # @classmethod
    # def select_option_bycode(cls,title_code,language):
    #     try:
    #         if language=="tr":
    #             return cls.objects.filter(project_options__title_code=title_code).get().project_options.title
    #         else:
    #             return cls.objects.filter(project_options__title_code=title_code).get().project_options.title_eng
    #     except:
    #         return ""


# yeni tablolar - yukardakiler editlenecek
class GeneralInfos(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=500, blank=True)
    describe = models.TextField()
    project_logo = models.FileField(upload_to="projects/", default="/media/projects/default_logo.png")
    project_system_url = models.CharField(max_length=500, blank=True)
    project_website = models.CharField(max_length=500, blank=True)

    class Meta:
        managed = True
        db_table = "project_generalinfos"

    @classmethod
    def create(cls, title, describe, project_website, id):
        if GeneralInfos.isThereGeneralInfos(id):
            selected = GeneralInfos.select(id)
            selected.title = title
            selected.describe = describe
            selected.project_website = project_website
            selected.save()
            return selected
        else:
            new_recod = cls(title=title, describe=describe, project_website=project_website)
            new_recod.save()
            return new_recod

    @classmethod
    def create_only_logo(cls, project_logo):
        new_record = cls(project_logo=project_logo)
        new_record.save()
        return new_record

    @classmethod
    def create_selected_project_logo(cls,general_infos_id,project_logo):
        selected = GeneralInfos.select(general_infos_id)
        selected.project_logo = project_logo
        selected.save()
        return selected

    @classmethod
    def delete_project_logo(cls, generals_infos_id, project_logo):
        selected = GeneralInfos.select(generals_infos_id)
        selected.project_logo = project_logo
        selected.save()
        return selected

    @classmethod
    def isThereGeneralInfos(cls, id):
        if cls.objects.filter(id=id).exists():
            return True
        else:
            return False

    @classmethod
    def select(cls, id):
        return cls.objects.filter(id=id).get()


class BusinessPlans(models.Model):
    id = models.BigAutoField(primary_key=True)
    target_group_content = models.TextField()
    # bu kısmı şimdilik aktif etme
    market_size = models.CharField(max_length=800, blank=True)
    revenue_model = models.TextField()

    class Meta:
        managed = True
        db_table = "project_businessplan"

    @classmethod
    def create(cls, target_group_content, market_size, revenue_model, id):
        if BusinessPlans.isThereBussPlan(id):
            selected = BusinessPlans.select(id)
            selected.target_group_content = target_group_content
            selected.market_size = market_size
            selected.revenue_model = revenue_model
            selected.save()
            return selected
        else:
            new_record = cls(target_group_content=target_group_content, market_size=market_size,
                             revenue_model=revenue_model)
            new_record.save()
            return new_record

    @classmethod
    def select(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def isThereBussPlan(cls, id):
        if cls.objects.filter(id=id).exists():
            return True
        else:
            return False


class Requirements(models.Model):
    id = models.BigAutoField(primary_key=True)
    existing_requirements = models.TextField()
    needed_requirements = models.TextField()

    class Meta:
        managed = True
        db_table = "project_requirements"

    @classmethod
    def create(cls, existing_requirements, needed_requirements, id):

        if Requirements.isThereRequirement(id):
            selected = Requirements.select(id)
            selected.existing_requirements = existing_requirements
            selected.needed_requirements = needed_requirements
            selected.save()
            return selected
        else:
            new_record = cls(existing_requirements=existing_requirements, needed_requirements=needed_requirements)
            new_record.save()
            return new_record

    @classmethod
    def select(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def isThereRequirement(cls, id):
        if cls.objects.filter(id=id).exists():
            return True
        else:
            return False


class Competitors(models.Model):
    id = models.BigAutoField(primary_key=True)
    competitor_content = models.TextField()

    class Meta:
        managed = True
        db_table = "project_competitors"

    @classmethod
    def create(cls, competitor_content, id):
        if Competitors.isThereRequirement(id):
            selected = Competitors.select(id)
            selected.competitor_content = competitor_content
            selected.save()
            return selected
        else:
            new_record = cls(competitor_content=competitor_content)
            new_record.save()
            return new_record

    @classmethod
    def select(cls, id):
        return cls.objects.filter(id=id).get()

    @classmethod
    def isThereRequirement(cls, id):
        if cls.objects.filter(id=id).exists():
            return True
        else:
            return False


class ProjectPointsScores(models.Model):
    id = models.BigAutoField(primary_key=True)
    projects = models.ForeignKey('Projects', models.DO_NOTHING, blank=True)
    pointscores = models.ForeignKey('mainadmins.PointScores', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = "project_pointscores"

    @classmethod
    def create(cls, projects, pointsscores):
        projects_pointscores = cls.objects.update_or_create(projects=projects,pointscores=pointsscores)
        return projects_pointscores

    @classmethod
    def _check_scores(cls, projects, pointscores):
        if cls.objects.filter(projects=projects, pointscores=pointscores).exists():
            return True
        else:
            return False

    @classmethod
    def calculate_points(cls, projects):
        all_points = 0

        for score in cls.objects.filter(projects=projects).all():
            all_points += int(score.pointscores.point_value)

        return all_points

    @classmethod
    def delete_point(cls, projects, pointscores):
        return cls.objects.filter(projects=projects, pointscores=pointscores).delete()


class ProjectUsageTypes(models.Model):
    id = models.BigAutoField(primary_key=True)
    projects = models.ForeignKey('Projects', models.DO_NOTHING, blank=True)
    usage_types = models.ForeignKey('mainadmins.UsageTypes', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = "project_usagetypes"

    @classmethod
    def create(cls, projects, usage_types):
        if ProjectUsageTypes.isThereProjectUsageTypes(projects, usage_types):
            selected = ProjectUsageTypes.select(projects, usage_types)
            selected.projects = projects
            selected.usage_types = usage_types
            selected.save()
            return selected
        else:
            new_record = cls(projects=projects, usage_types=usage_types)
            new_record.save()
            return new_record

    @classmethod
    def select(cls, projects, usage_types):
        return cls.objects.filter(projects=projects, usage_types=usage_types).get()

    @classmethod
    def isThereProjectUsageTypes(cls, projects, usage_types):
        if cls.objects.filter(projects=projects, usage_types=usage_types).exists():
            return True
        else:
            return False

    @classmethod
    def select_list_by_project(cls, projects):
        selected_list = cls.objects.filter(projects=projects).all()
        return selected_list

    @classmethod
    def select_usage_list(cls, projects):
        return cls.objects.filter(projects=projects).all()


class ProjectPrjSectors(models.Model):
    id = models.BigAutoField(primary_key=True)
    projects = models.ForeignKey('Projects', models.DO_NOTHING, blank=True)
    project_sectors = models.ForeignKey('mainadmins.ProjectSector', models.DO_NOTHING, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = "projects_prj_sectors"

    @classmethod
    def create(cls, projects, project_sectors):
        new_record = cls.objects.filter(projects=projects).update_or_create(projects=projects,
                                                                            project_sectors=project_sectors)

        return new_record

    @classmethod
    def select(cls, projects):
        return cls.objects.filter(projects=projects).get()
