from django.shortcuts import render, HttpResponseRedirect
from django.http import HttpResponse
from .models import UpdatesMain_UpdatesInfo
from django.utils.translation import ugettext
from django.utils.dateformat import format
from persons.models import UsersFullProfile,UsersUpdate_Infos
import json
from django.views.decorators.cache import never_cache,cache_page
# from datetime import datetime


# Create your views here.
@cache_page(60 * 30)
def index(request):
    try:
        if 'user_fullprofile' in request.session:
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            for log in UpdatesMain_UpdatesInfo.select_all():
                UsersUpdate_Infos.read_this_update(users_fullprofile, log)
            context = {
                "message_type": "success",
                "message": ugettext("All updates successfully read."),
                "message_class":"success",
                "page_title":ugettext("Leapcrowd Changelog"),
            }
            return render(request, 'socialside/updates/index.html', context)
        else:
            return HttpResponseRedirect("/login/")
    except:
        context = {
            "message_type": "error",
            "message": ugettext("An error occurred during the update list. Try again later, please."),
            "message_class": "danger",
            "page_title": ugettext("Leapcrowd Changelog"),
        }
        return render(request, 'socialside/updates/index.html', context)

@never_cache
def get_update_logs_count(request):
    if 'user_fullprofile' in request.session:
        context=[]
        count=0
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UpdatesMain_UpdatesInfo._isThereLog():
            for log in UpdatesMain_UpdatesInfo.select_all():
                if not UsersUpdate_Infos._isReadThisLog(users_fullprofile,log):
                    count+=1

        item={"update_count":count}
        context.append(item)

        return HttpResponse(json.dumps(context),content_type='application/json')

@cache_page(60 * 30)
def get_update_logs(request):
    if 'user_fullprofile' in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        if UpdatesMain_UpdatesInfo._isThereLog():
            for log in UpdatesMain_UpdatesInfo.select_all():
                #bu güncelleştirme okundu mu
                if UsersUpdate_Infos._isReadThisLog(users_fullprofile,log):
                    pass
                else:
                    pass
            pass
        else:
            pass


    pass

@cache_page(60 * 30)
def update_list(request):
    if 'user_fullprofile' in request.session:
        updates = UpdatesMain_UpdatesInfo.select_all()
        context = []
        if request.LANGUAGE_CODE == "tr":
            for update in updates:
                item = {"id":update.updates_main.id,"version": update.updates_main.title, "title": update.updates_info.title,
                        "title_content": update.updates_info.title_content, "date": format(update.updates_info.created_date,'d.m.Y'),
                        "up_date": format(update.updates_info.up_date,'d.m.Y'), "is_done": update.updates_info.isThis_done,
                        "is_cancel": update.updates_info.isThis_cancel}
                context.append(item)
        else:
            for update in updates:
                item = {"id":update.updates_main.id,"version": update.updates_main.title_eng, "title": update.updates_info.title_eng,
                        "title_content": update.updates_info.title_eng_content,
                        "date":format(update.updates_info.created_date,'d b, Y'), "up_date": format(update.updates_info.up_date,'d b, Y'),
                        "is_done": update.updates_info.isThis_done, "is_cancel": update.updates_info.isThis_cancel}
                context.append(item)

        print(context)

        return HttpResponse(json.dumps(context), content_type='application/json')
