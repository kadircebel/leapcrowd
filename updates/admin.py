from django.contrib import admin
from .models import UpdatesInfo,UpdatesMain,UpdatesMain_UpdatesInfo


# Register your models here.

class UpdatesMain_UpdatesInfosAdmin(admin.ModelAdmin):
    list_display = ["updates_main","updates_info"]

    class Meta:
        model=UpdatesMain_UpdatesInfo


class UpdatesMainAdmin(admin.ModelAdmin):
    list_display = ["title","title_eng","created_date","is_active"]

    class Meta:
        model=UpdatesMain


class UpdatesInfoAdmin(admin.ModelAdmin):
    list_display = ['title', 'title_eng', 'isThis_done', 'isThis_cancel', 'created_date','up_date']

    class Meta:
        model = UpdatesInfo

admin.site.register(UpdatesMain_UpdatesInfo,UpdatesMain_UpdatesInfosAdmin)
admin.site.register(UpdatesMain,UpdatesMainAdmin)
admin.site.register(UpdatesInfo,UpdatesInfoAdmin)

