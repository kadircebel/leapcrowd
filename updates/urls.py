from django.urls import path
from .views import *
from django.views.decorators.cache import cache_page

app_name="updates"

urlpatterns=[
    path('',index,name='updates-index'),
    path('update-list/', update_list, name='updates-info'),
    path('update-count/', get_update_logs_count, name='updates-count'),

]