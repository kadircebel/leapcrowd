from django.db import models


# Create your models here.

class UpdatesMain(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=50, blank=True)
    title_eng = models.CharField(max_length=50, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField()

    class Meta:
        managed = True
        db_table = "updates_main"

    def __str__(self):
        return '{}'.format(self.title)


class UpdatesInfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    version_title = models.CharField(max_length=50, blank=True)
    title = models.CharField(max_length=300, blank=True)
    title_eng = models.CharField(max_length=300, blank=True)
    title_content = models.TextField()
    title_eng_content = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    up_date = models.DateTimeField()
    isThis_done = models.BooleanField(default=False)
    isThis_cancel = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = "update_infos"

    def __str__(self):
        return '{}'.format(self.title)


class UpdatesMain_UpdatesInfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    updates_main=models.ForeignKey("UpdatesMain",models.DO_NOTHING,blank=True)
    updates_info=models.ForeignKey("UpdatesInfo",models.DO_NOTHING,blank=True)

    class Meta:
        managed=True
        db_table="updates_main_infos"

    @classmethod
    def select_all(cls):
        return cls.objects.all()

    @classmethod
    def _isThereLog(cls):
        if cls.objects.count()>0:
            return True
        else:
            return False


