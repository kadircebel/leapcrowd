from django.shortcuts import render, HttpResponseRedirect, HttpResponse,redirect
from django.conf import settings
from myprofile.models import FullprofileLocation
from persons.models import UsersFullProfile, UsersInvestments, UsersProjectHours, UsersHourCosts, UsersStartupHistory, UsersPosts, UsersPostsTeamrequests, UsersProjectsTeamrequests,Users,UsersDmProjectsTeamrequests,UsersMembershipTermsType,UsersIndustries,UsersProjects,UsersLinkedinRecommendations
from ideas.models import Posts
from utils.views import get_profile_name_str,get_member_statics,remove_html_tags,change_characters,get_person_profile_name,is_it_same_location,new_signals_users_list
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from projects.models import ProjectsProjectOptions
from mainadmins.models import ProjectOptions
from linkedin_infos.models import LinkedinRecommendations
# from memberships.models import MembershipTypes,MembershipTerms,MembershipTermsType
from django.utils.translation import ugettext
from notifications.models import NotificationsUsersTeams
from django.views.decorators.csrf import csrf_protect
from django.utils.timesince import timesince
import json,requests
from django.views.decorators.cache import cache_page,never_cache


# @never_cache
# def get_stickers(request):
#     sticker_list=["hello","fine","no"]
#     context_stc=[]
#     for stc in sticker_list:
#         url = "https://stickeroid.com/bot?k=YueBYy&s=" + stc
#         r = requests.request(method="GET",url=url)
#         item={
#             "sticker":r.json()
#         }
#         context_stc.append(item)
#     return HttpResponse(json.dumps(context_stc),content_type="application/json")

# @never_cache
# def get_sticker_from_user(request):
#     if 'user_fullprofile' in request.session:
#         if request.method == "GET":
#             search_text=request.GET.get("key")
#             url = "https://stickeroid.com/bot?k=YueBYy&s=" + search_text
#             r = requests.request(method="GET", url=url)
#
#             return HttpResponse(json.dumps(r.json()),content_type="application/json")


@never_cache
def entrepreneur_statics(request):
    if 'user_fullprofile' in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

        # city=users_fullprofile.full_profile.fullprofilelocation_set.get().location.city
        if request.method=="GET":
            entrepreneur_count=Users.list_of_users_membertype("entrepreneur")
            near_entrepreneur_count=Users.list_of_nearusers_membertype("entrepreneur",users_fullprofile.id)
            percent_value=(entrepreneur_count // near_entrepreneur_count)

            context={
                "main_count":entrepreneur_count,
                "near_count":near_entrepreneur_count,
                "percent_value":percent_value,
            }

            return HttpResponse(json.dumps(context),content_type="application/json")

@never_cache
def technicals_statics(request):
    if 'user_fullprofile' in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        # city = users_fullprofile.full_profile.fullprofilelocation_set.get().location.city
        if request.method == "GET":
            tech_count = Users.list_of_users_membertype("technical")
            near_tech_count = Users.list_of_nearusers_membertype("technical", users_fullprofile.id)

            percent_value=(tech_count // near_tech_count)


            context = {
                "main_count": tech_count,
                "near_count": near_tech_count,
                "percent_value":percent_value,
            }

            return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
# @cache_page(60 * 10)
def dashboard(request):
    # bu kısımda önce session'a bakacağız yoksa login sayfasına yönlendireceğiz
    context_static=[]

    if not 'user_fullprofile' in request.session:
        return redirect("/login/")
    else:


        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        context_static.append(get_member_statics("entrepreneur",users_fullprofile.id))
        context_static.append(get_member_statics("technical",users_fullprofile.id))

        context = {
            'member_statics': context_static,
            "page_title":ugettext("Dashboard"),
            "projects_count": UsersProjects.general_projects_count(),
            "ideas_count":UsersPosts.general_post_count()
        }
        return render(request, 'socialside/dashboard/index.html', context)

@cache_page(60*5)
@csrf_protect
def near_ideas(request):
    if 'user_fullprofile' in request.session:
        if request.method=="POST":
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            city = change_characters(users_fullprofile.full_profile.fullprofilelocation_set.get().location.city)
            context_ideas=[]
            ideas_list = UsersPosts.objects.filter(posts__is_active=True).all().order_by("-id")
            counter_idea=0
            for post in ideas_list:
                if is_it_same_location(users_fullprofile.id,post.users_fullprofiles.id):
                    if "entrepreneur" in post.users_fullprofiles.users.member_type:
                        if request.LANGUAGE_CODE == "tr":
                            position_title="Girişimci"
                        else:
                            position_title=post.users_fullprofiles.users.member_type
                    else:
                        if len(post.users_fullprofiles.full_profile.position_title) > 0:
                            position_title = post.users_fullprofiles.full_profile.position_title
                        else:
                            position_title=UsersIndustries.select(post.users_fullprofiles).industries.description

                    if not "default" in post.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = post.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = post.users_fullprofiles.full_profile.linkedin_picture_url

                    if len(remove_html_tags(post.posts.content_text)) > 120:
                        post_content=remove_html_tags(post.posts.content_text)[:120] + "..."
                    else:
                        post_content=remove_html_tags(post.posts.content_text)

                    if counter_idea < 7:
                        #profile_location kısmını front-tarafta ellemedim burda position_title olarak ele alınacak
                        item = {
                            "post_content": post_content,
                            # "profile_location":get_fulllocation_string(post.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.district,post.users_fullprofiles.full_profile.fullprofilelocation_set.get().location.city),
                            "profile_location":position_title,
                            "post_id": post.id,
                            "profile_username":get_person_profile_name(users_fullprofile.id,post.users_fullprofiles.id),
                            "member_type":post.users_fullprofiles.users.member_type,
                            "created_date": timesince(post.posts.created_date),
                            "profile_picture": profile_picture,
                            "profile_id": post.users_fullprofiles.id,
                        }
                        context_ideas.append(item)
                        counter_idea+=1

            return HttpResponse(json.dumps(context_ideas),content_type="application/json")

@cache_page(60 * 10)
@csrf_protect
def near_members(request):
    if 'user_fullprofile' in request.session:
        if request.method=="POST":
            # page=request.POST.get("page")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

            near_users=UsersFullProfile.near_users(users_fullprofile.id,request.LANGUAGE_CODE)

            paginator = Paginator(near_users, 6)

            page=1

            try:
                profiles = paginator.page(page)
            except PageNotAnInteger:
                profiles = paginator.page(1)
            except EmptyPage:
                profiles = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "profiles": list(profiles),
                "has_next": profiles.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data),content_type="application/json")

@cache_page(60 * 10)
@csrf_protect
def loadmore_near_members(request):
    if 'user_fullprofile' in request.session:
        if request.method=="GET":
            page=request.GET.get("page")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

            near_users = UsersFullProfile.near_users(users_fullprofile.id,request.LANGUAGE_CODE)

            paginator = Paginator(near_users, 6)

            try:
                profiles = paginator.page(page)
            except PageNotAnInteger:
                profiles = paginator.page(2)
            except EmptyPage:
                profiles = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "profiles": list(profiles),
                "has_next": profiles.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")


# bu kısım json respons olarak dönmektedir
@never_cache
def member_basic_options(request):
    option_list = []
    if 'user_fullprofile' in request.session:
        users_fullprofile_id = request.session["user_fullprofile"]
        users_fullprofile = UsersFullProfile.select_user(users_fullprofile_id)
        # bu kısım genelde hazırlık aşamasında kayıt olmuş kullanıcılar için
        # geçerli olacaktır
        # Kişinin lokasyon bilgisi yoksa girmesini sağla
        if not FullprofileLocation._isLocationClear(users_fullprofile.full_profile):
            message_type = "location"
            message_content = ugettext(
                "<b>Your location information</b> isn't configure on your profile. You should configure your location for using this platform. This setting is just needed once.")
            message_link_text = ugettext('Click to configure')
            message_link = "/dashboard/profiles/edit/"
            options = {"type": message_type, "content": message_content, "linkText": message_link_text,
                       "hyperlink": message_link}
            option_list.append(options)

        if not UsersStartupHistory._isStartupHistoryClear(users_fullprofile):
            message_type = "startup-history"
            message_content = ugettext(
                "<b>Your startup information</b> isn't configure on your profile. You should configure your startup experience information for using this platform. This setting is just needed once.")
            message_link_text = ugettext('Click to configure')
            message_link = "/dashboard/profiles/edit/"
            options = {"type": message_type, "content": message_content, "linkText": message_link_text,
                       "hyperlink": message_link}
            option_list.append(options)

        # kişinin investmentes ayarı yapılmamış ise yapmasını iste
        if not UsersInvestments._isInvestmentsClear(users_fullprofile):
            message_type = "investments"
            message_content = ugettext(
                "<b>Your support type</b> isn't configure on your profile. You should configure your support type for using this platform. Also, You can update this setting when bidding on a project.")
            message_link_text = ugettext('Click to configure')
            message_link = "/dashboard/profiles/edit/"
            options = {"type": message_type, "content": message_content, "linkText": message_link_text,
                       "hyperlink": message_link}
            option_list.append(options)

        # eğer proje saati belli değilse ayarlamasını ise
        if not UsersProjectHours._isProjectHoursClear(users_fullprofile):
            message_type = "projectHours"
            message_content = ugettext(
                "Your <b>project hour</b> isn't configure on your profile. You should configure your project hour for using this platform. Also, You can update this setting when bidding on a project.")
            message_link_text = ugettext('Click to configure')
            message_link = "/dashboard/profiles/edit/"
            options = {"type": message_type, "content": message_content, "linkText": message_link_text,
                       "hyperlink": message_link}
            option_list.append(options)
        else:
            if not UsersHourCosts._isUsersHourCostsClear(users_fullprofile):
                if UsersInvestments._investmentsType(users_fullprofile) == "paid-support":
                    message_type = "hourCosts"
                    message_content = ugettext(
                        "Your <b>hour cost</b> isn't configure on your profile. You should configure your project hour for using this platform. Also, You can update this setting when bidding on a project.")
                    message_link_text = ugettext('Click to configure')
                    message_link = "/dashboard/profiles/edit/"
                    options = {"type": message_type, "content": message_content, "linkText": message_link_text,
                               "hyperlink": message_link}
                    option_list.append(options)
    else:
        message_type = "error"
        message_content = ugettext("You must login first!")
        message_link_text = ugettext('Click to login')
        message_link = "/login/"
        options = {"type": message_type, "content": message_content, "linkText": message_link_text,
                   "hyperlink": message_link}
        option_list.append(options)

    return HttpResponse(json.dumps(option_list), content_type='application/json')

@never_cache
def quick_share_idea(request):
    if 'user_fullprofile' in request.session:
        if request.method == "POST":
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            idea_txt = request.POST.get("idea-text")

            if len(idea_txt) > 35:
                post = Posts.create_post(idea_txt)
                created_usersposts = UsersPosts.create_users_posts(users_fullprofile, post)
                UsersMembershipTermsType.create(users_fullprofile, "open-new-post",users_fullprofile.users.users_membershiptypes.name_code)
                context = {
                    "message_type": "success",
                    "message": ugettext("Your idea was successfully shared.")
                }

                notification_message_tr = get_profile_name_str(created_usersposts.users_fullprofiles.full_profile.last_name,created_usersposts.users_fullprofiles.full_profile.first_name) + " yeni bir fikir paylaştı."

                notification_message_en = get_profile_name_str(created_usersposts.users_fullprofiles.full_profile.last_name,created_usersposts.users_fullprofiles.full_profile.first_name) + " wrote an idea."

                payload = {
                    "app_id": "513c37fe-e03f-4a72-8eb2-b4810a7c15d1",
                    "include_player_ids": new_signals_users_list(users_fullprofile.id),
                    "contents": {"en": notification_message_en, "tr": notification_message_tr},
                    "url": "/dashboard/ideas/detail/" + str(created_usersposts.id) + "/"
                }

                requests.post("https://onesignal.com/api/v1/notifications", headers=settings.ONESIGNAL_HEADER,data=json.dumps(payload))

            else:
                context = {
                    "message_type": "warning",
                    "message": ugettext("You kept your idea is too short. You should explain a little more."),
                }

            return HttpResponse(json.dumps(context), content_type="application/json")

#notifications teamrequests
@never_cache
def get_teamrequests(request):
    if 'user_fullprofile' in request.session:
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
        project_list = users_fullprofile.usersprojects_set.all()
        post_list = users_fullprofile.usersposts_set.all()
        page = 1
        general_request = []
        for project in project_list:
            # arkadaş toplama ve publis proje mi
            if project.usersprojectsteamrequests_set.count() > 0:

               for team_request in project.usersprojectsteamrequests_set.all().order_by("-created_date")[:5]:
                        if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url

                        item_project_request = {
                            "profile_img": profile_picture,
                            "profile_name": get_profile_name_str(team_request.users_fullprofiles.full_profile.last_name,team_request.users_fullprofiles.full_profile.first_name),
                            "id":"",
                            "created_date": team_request.created_date,
                            "redirect_url": "/dashboard/myprojects/requests/" + str(team_request.users_projects.id),
                            "is_read": team_request.is_read,
                            "message":ugettext("sent you a team request."),
                            "type":"project-teamrequest",
                        }

                        general_request.append(item_project_request)

        if UsersDmProjectsTeamrequests.is_there_dm_requests(users_fullprofile):
            for dm_request in UsersDmProjectsTeamrequests.list_requests(users_fullprofile)[:5]:

                if not "default" in dm_request.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = dm_request.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = dm_request.users_fullprofiles.full_profile.linkedin_picture_url

                if request.LANGUAGE_CODE is "tr":
                    message= dm_request.users_projects.projects.general_infos.title + " projesi için size takım isteği gönderdi."
                else:
                    message = "sent you a team request for "+ dm_request.users_projects.projects.general_infos.title + "."

                item_dm = {
                    "profile_img": profile_picture,
                    "profile_name": get_profile_name_str(dm_request.users_fullprofiles.full_profile.last_name,dm_request.users_fullprofiles.full_profile.first_name),
                    "created_date": dm_request.created_date,
                    "redirect_url": "javascript:void(0);",
                    "is_read": dm_request.is_read,
                    "message": message,
                    "type":"dm-teamrequest",
                    "id":dm_request.id,
                }

                general_request.append(item_dm)

        if NotificationsUsersTeams.isThere_notification(users_fullprofile):
            for responded in NotificationsUsersTeams.list_all(users_fullprofile)[:5]:

                if not "default" in responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.picture_url.name:
                    profile_picture = responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.picture_url.url
                else:
                    profile_picture = responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.linkedin_picture_url

                if request.LANGUAGE_CODE is "tr":
                    message= responded.users_teams.teams.users_projects.projects.general_infos.title + " projesi için takım isteğiniz yanıtlandı."
                else:
                    message = "team request is answered for  "+responded.users_teams.teams.users_projects.projects.general_infos.title+" project."

                item_responded={
                    "profile_img":profile_picture,
                    "profile_name":get_profile_name_str(responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.last_name,responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.first_name),
                    "id":"",
                    "created_date":responded.created_date,
                    "redirect_url":"/dashboard/myprojects/cofunded/",
                    "is_read":responded.is_read,
                    "message":message,
                    "type":"answer-teamrequest",
                }

                general_request.append(item_responded)

        for post in post_list:
            if post.userspoststeamrequests_set.count() > 0:
                for team_request in post.userspoststeamrequests_set.all().order_by("-created_date")[:5]:
                    if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url



                    item_post_request = {
                        "profile_img": profile_picture,
                        "profile_name": get_profile_name_str(team_request.users_fullprofiles.full_profile.last_name,
                                                             team_request.users_fullprofiles.full_profile.first_name),
                        "id":"",
                        "created_date": team_request.created_date,
                        "redirect_url": "/dashboard/myideas/detail/" + str(team_request.users_posts.id),
                        "is_read": team_request.is_read,
                        "message": ugettext("sent you a team request."),
                        "type":"post-teamrequest",
                    }
                    general_request.append(item_post_request)

        context_notifications = []

        for general in sorted(general_request, key=lambda item: item["created_date"], reverse=True):
            item = {
                "profile_image": general["profile_img"],
                "profile_name": general["profile_name"],
                "created_date": timesince(general["created_date"]),
                "redirect_url": general["redirect_url"],
                "is_read": general["is_read"],
                "message":general["message"],
                "type":general["type"],
                "id":general["id"],
            }
            context_notifications.append(item)

        paginator = Paginator(context_notifications, 5)

        try:
            notification_list = paginator.page(page)
        except PageNotAnInteger:
            notification_list = paginator.page(2)
        except EmptyPage:
            notification_list = paginator.page(paginator.num_pages)

        page = str(int(page) + 1)

        output_data = {
            "notifications": list(notification_list),
            "has_next": notification_list.has_next(),
            "page": page,
        }

        return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
def teamrequest_count(request):
    if 'user_fullprofile' in request.session:
        # if request.method=="GET":
        users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

        request_count = UsersProjectsTeamrequests.users_request_count(users_fullprofile) + UsersPostsTeamrequests.users_request_count(users_fullprofile) + NotificationsUsersTeams.count_responded_teamrequests(users_fullprofile) + UsersDmProjectsTeamrequests.request_count(users_fullprofile,False)



        # print(NotificationsUsersTeams.count_responded_teamrequests(users_fullprofile))

        context = {
            "request_count": request_count,
        }
        return HttpResponse(json.dumps(context), content_type="application/json")

@never_cache
def loadmore_teamrequest(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            page = request.GET.get("page")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])
            project_list = users_fullprofile.usersprojects_set.all()
            post_list = users_fullprofile.usersposts_set.all()

            general_request = []
            for project in project_list:
                if ProjectsProjectOptions.isItOnline(project.projects, ProjectOptions.select(
                        "publish-project")) and ProjectsProjectOptions.isItOnline(project.projects,ProjectOptions.select("send-teamrequest")):
                    # arkadaş toplama ve publis proje mi
                    if project.usersprojectsteamrequests_set.count() > 0:
                        for team_request in project.usersprojectsteamrequests_set.all().order_by("-created_date")[:5]:
                            if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                                profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url
                            else:
                                profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url

                            # if len(team_request.users_fullprofiles.full_profile.position_title) > 0:
                            #     position_title = team_request.users_fullprofiles.full_profile.position_title
                            # else:
                            #     position_title = team_request.users_fullprofiles.usersındustries_set.get().industries.description

                            item_project_request = {
                                "profile_img": profile_picture,
                                "profile_name": get_profile_name_str(
                                    team_request.users_fullprofiles.full_profile.last_name,
                                    team_request.users_fullprofiles.full_profile.first_name),
                                # "position_title":position_title,
                                "message": ugettext("sent you a team request."),
                                "created_date": team_request.created_date,
                                "redirect_url": "/dashboard/myprojects/requests/" + str(team_request.users_projects.id),
                                "is_read": team_request.is_read,
                            }

                            general_request.append(item_project_request)

            if NotificationsUsersTeams.isThere_notification(users_fullprofile):
                for responded in NotificationsUsersTeams.list_all(users_fullprofile)[:5]:

                    if not "default" in responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.picture_url.name:
                        profile_picture = responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.picture_url.url
                    else:
                        profile_picture = responded.users_teams.teams.users_projects.users_fullprofiles.linkedin_picture_url

                    if request.LANGUAGE_CODE is "tr":
                        message = get_profile_name_str(
                            responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.last_name,
                            responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.first_name) + " takım isteğinizi yanıtladı."
                    else:
                        message = get_profile_name_str(
                            responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.last_name,
                            responded.users_teams.teams.users_projects.users_fullprofiles.full_profile.first_name) + " responded to your team request."

                    item_responded = {
                        "profile_img": profile_picture,
                        "profile_name": get_profile_name_str(
                            responded.users_teams.users_fullprofiles.full_profile.last_name,
                            responded.users_teams.users_fullprofiles.full_profile.first_name),
                        "created_date": responded.created_date,
                        "redirect_url": "/dashboard/myprojects/cofunded/",
                        "is_read": responded.is_read,
                        "message": message,
                    }

                    general_request.append(item_responded)

            for post in post_list:
                if post.userspoststeamrequests_set.count() > 0:
                    for team_request in post.userspoststeamrequests_set.all().order_by("-created_date")[:5]:
                        if not "default" in team_request.users_fullprofiles.full_profile.picture_url.name:
                            profile_picture = team_request.users_fullprofiles.full_profile.picture_url.url
                        else:
                            profile_picture = team_request.users_fullprofiles.full_profile.linkedin_picture_url

                        # if len(team_request.users_fullprofiles.full_profile.position_title) > 0:
                        #     position_title = team_request.users_fullprofiles.full_profile.position_title
                        # else:
                        #     position_title = team_request.users_fullprofiles.usersındustries_set.get().industries.description

                        item_post_request = {
                            "profile_img": profile_picture,
                            "profile_name": get_profile_name_str(team_request.users_fullprofiles.full_profile.last_name,team_request.users_fullprofiles.full_profile.first_name),
                            # "position_title":position_title,
                            "message": ugettext("sent you a team request."),
                            "created_date": team_request.created_date,
                            "redirect_url": "/dashboard/myideas/detail/" + str(team_request.users_posts.id),
                            "is_read": team_request.is_read,
                        }
                        general_request.append(item_post_request)

            context_notifications = []

            for general in sorted(general_request, key=lambda item: item["created_date"], reverse=True):
                item = {
                    "profile_image": general["profile_img"],
                    # "position_title": general["position_title"],
                    "profile_name": general["profile_name"],
                    "created_date": timesince(general["created_date"]),
                    "redirect_url": general["redirect_url"],
                    "is_read": general["is_read"],
                    "message":general["message"],
                }
                context_notifications.append(item)

            paginator = Paginator(context_notifications, 5)

            try:
                notification_list = paginator.page(page)
            except PageNotAnInteger:
                notification_list = paginator.page(2)
            except EmptyPage:
                notification_list = paginator.page(paginator.num_pages)

            page = str(int(page) + 1)

            output_data = {
                "notifications": list(notification_list),
                "has_next": notification_list.has_next(),
                "page": page,
            }

            return HttpResponse(json.dumps(output_data), content_type="application/json")

@never_cache
def save_signal_id(request):
    if 'user_fullprofile' in request.session:
        if request.method == "GET":
            signal_id=request.GET.get("signal_id")
            users_fullprofile = UsersFullProfile.select_user(request.session["user_fullprofile"])

            if (UsersLinkedinRecommendations.is_there_signal_id(users_fullprofile) == False):
                created_signal=LinkedinRecommendations.create_signal_id("onesignal",signal_id)
                UsersLinkedinRecommendations.create_users_onesignal(users_fullprofile,created_signal)

            context={
                "message_type":"success"
            }

            return HttpResponse(json.dumps(context),content_type="application/json")