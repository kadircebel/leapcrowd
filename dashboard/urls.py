from django.urls import path
from .views import *


app_name="dashboard"

urlpatterns=[
    path('',dashboard,name='dahsboard'),

    path('basic-options/',member_basic_options,name='memberBasicOptions'),
    path('quick-idea/', quick_share_idea, name='quick-idea'),
    path('notification-teamrequest/', get_teamrequests, name='notification_teamrequest'),
    path('loadmore-teamrequest/', loadmore_teamrequest, name='loadmore_teamrequest'),
    path('teamrequest-count/', teamrequest_count, name='notification_teamrequest'),
    path('near-ideas/', near_ideas, name='near_ideas'),
    path('near-members/', near_members, name='near_members'),
    path('loadmore-near-members/', loadmore_near_members, name='loadmore_near_members'),
    path('create-signal/', save_signal_id, name='create_signal'),
    # path('get-stickers/', get_stickers, name='get_stickers'),
    # path('get-sticker-user/', get_sticker_from_user, name='get_sticker_from_user'),
    # path('entrepreneur-static/', entrepreneur_statics, name='entrepreneur_statics'),
    # path('technical-static/', technicals_statics, name='technicals_statics'),

]